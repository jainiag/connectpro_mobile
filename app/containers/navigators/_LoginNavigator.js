import { createAppContainer, createStackNavigator } from 'react-navigation';
import DrawerNavigator from './_DrawerNavigator';
import forgotPassword from '../screens/AuthenticationScreens/forgotPassword';
import LoginScreen from '../screens/AuthenticationScreens/loginScreen';
import RegisterScreen from '../screens/AuthenticationScreens/registerScreen';
import resetPassword from '../screens/AuthenticationScreens/resetPassword';
import * as colors from '../../assets/styles/color';

const LoginStackNavigator = createStackNavigator(
  {
    LoginScreen: {
      screen: LoginScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
      },
    },
    RegisterScreen: {
      screen: RegisterScreen,
      navigationOptions: {
        header: null,
        gesturesEnabled: true,
      },
    },
    forgotPassword: {
      screen: forgotPassword,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
      },
    },
    resetPassword: {
      screen: resetPassword,
      navigationOptions: ({ navigation }) => ({
        headerStyle: {
          backgroundColor: colors.warmBlue,
          borderBottomColor: 'transparent',
          borderBottomWidth: 0,
        },
        headerTransparent: true,
        headerLeft: null,
      }),
    },
    DrawerNavigator: {
      screen: DrawerNavigator,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
      },
    },
   
  },
  {
    initialRouteName: 'LoginScreen',
    navigationOptions: {
      gesturesEnabled: false,
    },
  }
);

export default createAppContainer(LoginStackNavigator);
