import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import LoginStackNavigator from './_LoginNavigator';
import DrawerNavigator from './_DrawerNavigator';
import FirstScreen from '../screens/AuthenticationScreens/firstScreen';

const AuthStack = createSwitchNavigator(
  {
    AuthFirst: FirstScreen,
    App: DrawerNavigator,
    Auth: LoginStackNavigator,
  },
  {
    initialRouteName: 'AuthFirst',
  }
);

export default createAppContainer(AuthStack);
