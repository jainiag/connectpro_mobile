import { createDrawerNavigator, createStackNavigator, createAppContainer } from 'react-navigation';
import * as globals from '../../utils/globals';
import * as colors from '../../assets/styles/color';
import SideMenu from '../screens/SideMenu';
import GLOBAL_EVENT_SCREEN from '../screens/GlobalEvents/globalEvents/index';
import DASHBOARD_SCREEN from '../screens/Dashboard/MemberDashboard';
import GLOBAL_MEMBER_SCREEN from '../screens/GlobalMember';
import GLOBAL_COMMUNITIES_SCREEN from '../screens/GlobalCommunities';
import NOTIFICATION_SCREEN from '../screens/Notifications';
import CHANGE_PASSWORD_SCREEN from '../screens/ChangePassword';
import GLOBAL_EVENT_SEARCH_SCREEN from '../screens/GlobalEvents/searchscreen';
import MYEVENTS_SCREEN from '../screens/Dashboard/Events';
import LOCATION_SCREEN from '../screens/Dashboard/Events/Locationscreen';
import VIEW_MAP from '../screens/Dashboard/Events/Viewmapscreen';
import GLOBAL_EVENT_SCREEN_DASHBOARD from '../screens/Dashboard/Events/event_Dahboard/index';
// import DEMO from '../screens/swipeleftdemo';
import DIRECTORYDEMO from '../screens/Directoryscreen';
import Attendes from '../screens/Dashboard/Events/attendes';
import EDIT_PROFILE from '../screens/Profile/editableScreens/editProfile';
import EVENT_REGISTRATION from '../screens/Dashboard/Events/eventRegistrations/registerEvent';
import ATTENDEES_DETAIL2 from '../screens/Dashboard/Events/eventRegistrations/attendeeDetails2';
import AboutEvent from '../screens/Dashboard/Events/aboutEvents/index';
import AgendaScreen from '../screens/Dashboard/Events/agenda/index';
import SPEAKER_SCREEN from '../screens/Dashboard/Events/speakers/index';
import SHAREEVENT_SCREEN from '../screens/Dashboard/Events/shareEvent/index';
import SPONSORS_SCREEN from '../screens/Dashboard/Events/sponsors/index';
import ADD_PLAYER_INFORMATION from '../screens/Dashboard/Events/eventRegistrations/addPlayerinformation';
import MY_PROFILEDEMO from '../screens/Profile/index';
import PROFILE_CATEGORY from '../screens/Profile/editableScreens/profileCategory';
import CASH_DETAILS from '../screens/Dashboard/Events/eventRegistrations/cashDetails';
import CHEQUE_DETAILS from '../screens/Dashboard/Events/eventRegistrations/chequeDetails';
import SUCCESS_PAYMENT from '../screens/Dashboard/Events/eventRegistrations/successPayment';
import ATTENDEES_DETAILS from '../screens/Dashboard/Events/eventRegistrations/attendeesDetails';
import BOOKING_INFORMATION from '../screens/Dashboard/Events/eventRegistrations/bookingInformation';
import BookingReceipt from '../screens/Dashboard/Events/eventRegistrations/bookingReceipt';
import Matches from '../screens/Dashboard/Matches/index';
import Connections from '../screens/Dashboard/Connections/index';
import MyConnection from '../screens/Dashboard/Connections/myConnection';
import NewConnection from '../screens/Dashboard/Connections/newConnection';
import ResourcesTab from '../screens/Dashboard/Events/resources/resourcesTab';
import makePayment from '../screens/Dashboard/Events/eventRegistrations/makePayment';
import AuthorizeNetWebview from '../screens/Dashboard/Events/eventRegistrations/authorizeNetWebview';
import PdfView from '../screens/Dashboard/Events/resources/pdfView';
import discussionPost from '../screens/Dashboard/Communities/Community_Dashboard/Disscussion/discussionPost';
import EditBiusnessCategory from '../screens/Profile/companyProfileTab/editBusinessCategory';
import EditcompanyProfile from '../screens/Profile/companyProfileTab/editCompanyProfile';
import AddPortfolio from '../screens/Profile/editableScreens/addPortfolio';
import EditLinkedinMyProfile from '../screens/Profile/editableScreens/editLinkedinMyProfile';
import EditLinkedinProfile from '../screens/Profile/companyProfileTab/editLinkedinProfile';
import MessagesThread from '../screens/Dashboard/Messages/messageThreads';
import NewMessageListSearch from '../screens/Dashboard/Messages/newMessageListSearch';
import ChatScreen from '../screens/Dashboard/Messages/chatScreen';
import AddSkill from '../screens/Profile/editableScreens/addSkill';
import AddInterests from '../screens/Profile/editableScreens/addInterests';
import CommunityDashboard from '../screens/Dashboard/Communities/Community_Dashboard/communityDashboard';
import AboutCommunity from '../screens/Dashboard/Communities/Community_Dashboard/AboutCommunity/index'
import Communities from '../screens/Dashboard/Communities/mycommunityList';
import CommunityMembers from '../screens/Dashboard/Communities/Community_Dashboard/Members/index';
import Resources from '../screens/Dashboard/Communities/Community_Dashboard/Resources/index';
import Admins from '../screens/Dashboard/Communities/Community_Dashboard/Admins/index';
import MemberSearch from '../screens/Dashboard/Communities/Community_Dashboard/Members/MemberSearch/index';
import Events from '../screens/Dashboard/Communities/Community_Dashboard/Events/index';
import CreateCommunity from '../screens/Dashboard/Communities/CreateCommunity/index';
import CommunityGroups from '../screens/Dashboard/Communities/Community_Dashboard/Groups/index';
import EndorsementsTab from '../screens/Dashboard/Endorsements';
import ViewCompanyProfilePicture from '../screens/Profile/viewCompanyProfilePicture';
import viewMyProfilePicture from '../screens/Profile/viewMyProfilePicture';
import Groups from '../screens/Dashboard/Groups/groupListing/index'
import GroupDashboard from '../screens/Dashboard/Groups/groupDashboard/index'
import GroupAdmins from '../screens/Dashboard/Groups/groupDashboard/Admins/index'
import ResourcesGroups from '../screens/Dashboard/Groups/groupDashboard/Resources/index';
import GroupMembers from '../screens/Dashboard/Groups/groupDashboard/Members/index';
import OtherProfile from '../screens/OtherProfiles/index';
import PushController from '../screens/Dashboard/MemberDashboard/pushControler'
import GlobalMemberSearch from '../screens/GlobalMember/searchScreen';
import groupDiscussion from '../screens/Dashboard/Groups/groupDashboard/Discussion/groupDiscussion';
import ManageEmails from '../screens/Dashboard/ManageEmails';
import EmailDetails from '../screens/Dashboard/ManageEmails/sentEmails/detailScreen';
import ComposeEmails from '../screens/Dashboard/ManageEmails/composeEmails/index';
import EmailListScreen from '../screens/Dashboard/ManageEmails/statisticalAnalysis/emailListScreen';
import StatisticalEmailDetail from '../screens/Dashboard/ManageEmails/statisticalAnalysis/statisticalEmailDetail';

const DashboardStack = createStackNavigator({
  DASHBOARD_SCREEN: {
    screen: DASHBOARD_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PushController: {
    screen: PushController,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MYEVENTS_SCREEN: {
    screen: MYEVENTS_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Attendes: {
    screen: Attendes,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  LOCATION_SCREEN: {
    screen: LOCATION_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  VIEW_MAP: {
    screen: VIEW_MAP,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  // DEMO: {
  //   screen: DEMO,
  //   navigationOptions: ({ navigation }) => ({}),
  // },
  DIRECTORYDEMO: {
    screen: DIRECTORYDEMO,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  EVENT_REGISTRATION: {
    screen: EVENT_REGISTRATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ATTENDEES_DETAIL2: {
    screen: ATTENDEES_DETAIL2,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ADD_PLAYER_INFORMATION: {
    screen: ADD_PLAYER_INFORMATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  GLOBAL_EVENT_SCREEN_DASHBOARD: {
    screen: GLOBAL_EVENT_SCREEN_DASHBOARD,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AboutEvent: {
    screen: AboutEvent,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AgendaScreen: {
    screen: AgendaScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CASH_DETAILS: {
    screen: CASH_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CHEQUE_DETAILS: {
    screen: CHEQUE_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SUCCESS_PAYMENT: {
    screen: SUCCESS_PAYMENT,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ATTENDEES_DETAILS: {
    screen: ATTENDEES_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  BOOKING_INFORMATION: {
    screen: BOOKING_INFORMATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SPEAKER_SCREEN: {
    screen: SPEAKER_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  ResourcesTab: {
    screen: ResourcesTab,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PdfView: {
    screen: PdfView,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  BookingReceipt: {
    screen: BookingReceipt,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  SPONSORS_SCREEN: {
    screen: SPONSORS_SCREEN,
    navigationOptions: ({ navigation }) => ({
    }),
  },
  SHAREEVENT_SCREEN: {
    screen: SHAREEVENT_SCREEN,
    navigationOptions: ({ navigation }) => ({
    }),
  },
  Matches: {
    screen: Matches,
    navigationOptions: ({ navigation }) => ({
    }),
  },
  Connections: {
    screen: Connections,
    navigationOptions: ({ navigation }) => ({

    }),
  },
  MyConnection: {
    screen: MyConnection,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  NewConnection: {
    screen: NewConnection,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  makePayment: {
    screen: makePayment,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AuthorizeNetWebview: {
    screen: AuthorizeNetWebview,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  discussionPost: {
    screen: discussionPost,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  MessagesThread: {
    screen: MessagesThread,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  NewMessageListSearch: {
    screen: NewMessageListSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  NewMessageListSearch: {
    screen: NewMessageListSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ChatScreen: {
    screen: ChatScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditLinkedinProfile: {
    screen: EditLinkedinProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AboutCommunity: {
    screen: AboutCommunity,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CommunityDashboard: {
    screen: CommunityDashboard,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Communities: {
    screen: Communities,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CommunityMembers: {
    screen: CommunityMembers,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Resources: {
    screen: Resources,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  Admins: {
    screen: Admins,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MemberSearch: {
    screen: MemberSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Events: {
    screen: Events,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CreateCommunity: {
    screen: CreateCommunity,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CommunityGroups: {
    screen: CommunityGroups,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EndorsementsTab: {
    screen: EndorsementsTab,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Groups: {
    screen: Groups,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GroupDashboard: {
    screen: GroupDashboard,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GroupAdmins: {
    screen: GroupAdmins,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ResourcesGroups: {
    screen: ResourcesGroups,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GroupMembers: {
    screen: GroupMembers,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  OtherProfile: {
    screen: OtherProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  groupDiscussion: {
    screen: groupDiscussion,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ManageEmails: {
    screen: ManageEmails,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EmailDetails: {
    screen: EmailDetails,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ComposeEmails: {
    screen: ComposeEmails,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EmailListScreen: {
    screen: EmailListScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  StatisticalEmailDetail: {
    screen: StatisticalEmailDetail,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MY_PROFILEDEMO: {
    screen: MY_PROFILEDEMO,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EDIT_PROFILE: {
    screen: EDIT_PROFILE,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddPortfolio: {
    screen: AddPortfolio,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PROFILE_CATEGORY: {
    screen: PROFILE_CATEGORY,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddSkill: {
    screen: AddSkill,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddInterests: {
    screen: AddInterests,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },


});


const GlobalEventStack = createStackNavigator({
  GLOBAL_EVENT_SCREEN: {
    screen: GLOBAL_EVENT_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Attendes: {
    screen: Attendes,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  LOCATION_SCREEN: {
    screen: LOCATION_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  VIEW_MAP: {
    screen: VIEW_MAP,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GLOBAL_EVENT_SEARCH_SCREEN: {
    screen: GLOBAL_EVENT_SEARCH_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GLOBAL_EVENT_SCREEN_DASHBOARD: {
    screen: GLOBAL_EVENT_SCREEN_DASHBOARD,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AboutEvent: {
    screen: AboutEvent,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EVENT_REGISTRATION: {
    screen: EVENT_REGISTRATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  AgendaScreen: {
    screen: AgendaScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ResourcesTab: {
    screen: ResourcesTab,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PdfView: {
    screen: PdfView,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ADD_PLAYER_INFORMATION: {
    screen: ADD_PLAYER_INFORMATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CASH_DETAILS: {
    screen: CASH_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CHEQUE_DETAILS: {
    screen: CHEQUE_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SUCCESS_PAYMENT: {
    screen: SUCCESS_PAYMENT,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ATTENDEES_DETAILS: {
    screen: ATTENDEES_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ATTENDEES_DETAIL2: {
    screen: ATTENDEES_DETAIL2,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  BOOKING_INFORMATION: {
    screen: BOOKING_INFORMATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SPEAKER_SCREEN: {
    screen: SPEAKER_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),

  },

  BookingReceipt: {
    screen: BookingReceipt,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  SPONSORS_SCREEN: {
    screen: SPONSORS_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SHAREEVENT_SCREEN: {
    screen: SHAREEVENT_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  makePayment: {
    screen: makePayment,
    navigationOptions: ({ navigation }) => ({

      gesturesEnabled: false,
    }),
  },
  AuthorizeNetWebview: {
    screen: AuthorizeNetWebview,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditBiusnessCategory: {
    screen: EditBiusnessCategory,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Admins: {
    screen: Admins,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MemberSearch: {
    screen: MemberSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  OtherProfile: {
    screen: OtherProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MY_PROFILEDEMO: {
    screen: MY_PROFILEDEMO,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EDIT_PROFILE: {
    screen: EDIT_PROFILE,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddPortfolio: {
    screen: AddPortfolio,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PROFILE_CATEGORY: {
    screen: PROFILE_CATEGORY,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddSkill: {
    screen: AddSkill,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddInterests: {
    screen: AddInterests,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

});

const GlobalMemberStack = createStackNavigator({
  GLOBAL_MEMBER_SCREEN: {
    screen: GLOBAL_MEMBER_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MY_PROFILEDEMO: {
    screen: MY_PROFILEDEMO,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EDIT_PROFILE: {
    screen: EDIT_PROFILE,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddPortfolio: {
    screen: AddPortfolio,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PROFILE_CATEGORY: {
    screen: PROFILE_CATEGORY,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddSkill: {
    screen: AddSkill,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddInterests: {
    screen: AddInterests,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GlobalMemberSearch: {
    screen: GlobalMemberSearch,
    navigationOptions: ({ navigation }) => ({}),
  },
  MemberSearch: {
    screen: MemberSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  OtherProfile: {
    screen: OtherProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
});

const GlobalCommunitiesStack = createStackNavigator({
  GLOBAL_COMMUNITIES_SCREEN: {
    screen: GLOBAL_COMMUNITIES_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),

  },
  Attendes: {
    screen: Attendes,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  LOCATION_SCREEN: {
    screen: LOCATION_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  VIEW_MAP: {
    screen: VIEW_MAP,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GLOBAL_EVENT_SCREEN_DASHBOARD: {
    screen: GLOBAL_EVENT_SCREEN_DASHBOARD,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AboutEvent: {
    screen: AboutEvent,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EVENT_REGISTRATION: {
    screen: EVENT_REGISTRATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  AgendaScreen: {
    screen: AgendaScreen,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ResourcesTab: {
    screen: ResourcesTab,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PdfView: {
    screen: PdfView,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ADD_PLAYER_INFORMATION: {
    screen: ADD_PLAYER_INFORMATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CASH_DETAILS: {
    screen: CASH_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CHEQUE_DETAILS: {
    screen: CHEQUE_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SUCCESS_PAYMENT: {
    screen: SUCCESS_PAYMENT,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ATTENDEES_DETAILS: {
    screen: ATTENDEES_DETAILS,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ATTENDEES_DETAIL2: {
    screen: ATTENDEES_DETAIL2,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  BOOKING_INFORMATION: {
    screen: BOOKING_INFORMATION,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SPEAKER_SCREEN: {
    screen: SPEAKER_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  BookingReceipt: {
    screen: BookingReceipt,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  SPONSORS_SCREEN: {
    screen: SPONSORS_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  SHAREEVENT_SCREEN: {
    screen: SHAREEVENT_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  makePayment: {
    screen: makePayment,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AuthorizeNetWebview: {
    screen: AuthorizeNetWebview,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditBiusnessCategory: {
    screen: EditBiusnessCategory,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Admins: {
    screen: Admins,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MemberSearch: {
    screen: MemberSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  OtherProfile: {
    screen: OtherProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GroupDashboard: {
    screen: GroupDashboard,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GroupAdmins: {
    screen: GroupAdmins,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ResourcesGroups: {
    screen: ResourcesGroups,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  GroupMembers: {
    screen: GroupMembers,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  groupDiscussion: {
    screen: groupDiscussion,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CommunityDashboard: {
    screen: CommunityDashboard,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AboutCommunity: {
    screen: AboutCommunity,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Events: {
    screen: Events,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  Admins: {
    screen: Admins,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CommunityGroups: {
    screen: CommunityGroups,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  discussionPost: {
    screen: discussionPost,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CommunityMembers: {
    screen: CommunityMembers,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  CreateCommunity: {
    screen: CreateCommunity,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

  Resources: {
    screen: Resources,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MemberSearch: {
    screen: MemberSearch,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  OtherProfile: {
    screen: OtherProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AboutEvent: {
    screen: AboutEvent,
    navigationOptions: ({ navigation }) => ({
    }),
  },
  PdfView: {
    screen: PdfView,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  MY_PROFILEDEMO: {
    screen: MY_PROFILEDEMO,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EDIT_PROFILE: {
    screen: EDIT_PROFILE,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddPortfolio: {
    screen: AddPortfolio,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PROFILE_CATEGORY: {
    screen: PROFILE_CATEGORY,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddSkill: {
    screen: AddSkill,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddInterests: {
    screen: AddInterests,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },

});

const NotificationStack = createStackNavigator({
  NOTIFICATION_SCREEN: {
    screen: NOTIFICATION_SCREEN,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
});

const ChangePasswordStack = createStackNavigator({
  CHANGE_PASSWORD_SCREEN: {
    screen: CHANGE_PASSWORD_SCREEN,
    navigationOptions: ({ navigation }) => ({
      
      headerStyle: {
        backgroundColor: colors.warmBlue,
        borderBottomColor: 'transparent',
        borderBottomWidth: 0,
      },
      headerTransparent: true,
    }),
  },
});

const MyProfileStack = createStackNavigator({
  MY_PROFILEDEMO: {
    screen: MY_PROFILEDEMO,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  PROFILE_CATEGORY: {
    screen: PROFILE_CATEGORY,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditBiusnessCategory: {
    screen: EditBiusnessCategory,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditcompanyProfile: {
    screen: EditcompanyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddPortfolio: {
    screen: AddPortfolio,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddSkill: {
    screen: AddSkill,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  AddInterests: {
    screen: AddInterests,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditLinkedinProfile: {
    screen: EditLinkedinProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EditLinkedinMyProfile: {
    screen: EditLinkedinMyProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  EDIT_PROFILE: {
    screen: EDIT_PROFILE,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  viewMyProfilePicture: {
    screen: viewMyProfilePicture,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  ViewCompanyProfilePicture: {
    screen: ViewCompanyProfilePicture,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
  OtherProfile: {
    screen: OtherProfile,
    navigationOptions: ({ navigation }) => ({
      gesturesEnabled: false,
    }),
  },
});

const Drawer = createDrawerNavigator(
  {
    DASHBOARD_SCREEN: {
      screen: DashboardStack,
    },
    GLOBAL_EVENT_SCREEN: {
      screen: GlobalEventStack,
    },
    GLOBAL_MEMBER_SCREEN: {
      screen: GlobalMemberStack,
    },
    GLOBAL_COMMUNITIES_SCREEN: {
      screen: GlobalCommunitiesStack,
    },
    NOTIFICATION_SCREEN: {
      screen: NotificationStack,
    },
    MYPROFILE_SCREEN: {
      screen: MyProfileStack,
    },
    CHANGE_PASSWORD_SCREEN: {
      screen: ChangePasswordStack,
    },
  },
  {
    contentComponent: SideMenu,
    drawerWidth: globals.screenWidth * 0.77,
    drawerType: 'front',
    overlayColor: colors.black06,
    navigationOptions: {
      gesturesEnabled: false,
    },
    drawerLockMode: 'locked-open',
  }
);

export default createAppContainer(Drawer);
