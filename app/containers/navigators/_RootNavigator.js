/* eslint-disable eqeqeq */
/* eslint-disable import/named */
/* eslint-disable react/no-unused-state */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { View, Linking } from 'react-native';
import { connect } from 'react-redux';
import Loader from '../../components/Loader';
import { getshowLoader } from '../../redux/acrions/showLoader';
import RootMainStackNavigator from './_rootMainNavigator';
import * as globals from '../../utils/globals';

// var PushNotification = require('react-native-push-notification');
const TAG = '==:== RootNavigation:';
let thisInstance;
let _this;

class RootNavigation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: this.props.loader,
    };
  }

  componentDidMount() {
    // Linking.addEventListener('url', this.handleNavigation);
  }

  componentWillUnmount() {
    //Linking.removeEventListener('url', this.handleNavigation);
  }

  // handleNavigation = event => {
  //   if (event.url != undefined) {
  //     const seprateUrlCode = event.url.split('&');
  //     const resetCodeFull = seprateUrlCode[1];
  //     const resetCodeSplit = resetCodeFull.split('=');
  //     const resetCode = resetCodeSplit[1];
  //     globals.RESETPASSWORDCODE = resetCode;
  //     this.props.navigation.navigate('resetPassword');
  //   }
  // };

  // eslint-disable-next-line react/sort-comp
  componentWillReceiveProps(nextProps) {
    const { loader } = nextProps;
    this.setState({ loader });
  }

  render() {
    const { loader } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <RootMainStackNavigator />
        <Loader loading={loader} />
      </View>
    );
  }
}

// ********************** Model mapping method **********************

const mapStateToProps = (state, ownProps) => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch => ({
  getshowLoader: () => dispatch(getshowLoader()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RootNavigation);
