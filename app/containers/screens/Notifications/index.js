import React from 'react';
import { Text, View, Switch } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';
import styles from './style';
import * as globals from '../../../utils/globals';
import globalStyles from '../../../assets/styles/globleStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';


const iPad = DeviceInfo.getModel();
let TAG = "Notifications Screen ::==="
let _this = null;
class Notifications extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: globals.ConnectProbackButton(navigation, 'Notifications'),
            headerStyle: globalStyles.ConnectPropheaderStyle,

        }
    };


    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            switchValueNotificationAll: false,
            switchNotificationMsgs: false,
            switchNotificationEndorsement: false,
            switchNotificationConnection: false
        }
    }

    getValues() {

        AsyncStorage.getItem(globals.CONNECTIONNOTIFICATIONSTATUS).then((value) => {
            if (value != null && value != undefined && value != "") {
                this.setState({ switchNotificationConnection: JSON.parse(value) }, () => {
                    console.log("getValues switchNotificationConnection--", this.state.switchNotificationConnection)
                })
            }
        })
        AsyncStorage.getItem(globals.ENDORSENOTIFICATIONSTATUS).then((value) => {
            if (value != null && value != undefined) {
                let endrosmentVal = JSON.parse(value)
                this.setState({ switchNotificationEndorsement: endrosmentVal }, () => {
                    console.log("getValues switchNotificationEndorsement--", this.state.switchNotificationEndorsement)
                })
            }
        })

        AsyncStorage.getItem(globals.MESSAGESNOTIFICATIONSTATUS).then((value) => {
            if (value != null && value != undefined) {
                this.setState({ switchNotificationMsgs: JSON.parse(value) }, () => {
                    console.log("getValues MESSAGESNOTIFICATIONSTATUS--", this.state.switchNotificationMsgs)
                })
            }
        })
        setTimeout(() => {
            if (this.state.switchNotificationConnection && this.state.switchNotificationEndorsement && this.state.switchNotificationMsgs) {
                this.setState({ switchValueNotificationAll: true })
            } else {
                this.setState({ switchValueNotificationAll: false })
            }
        }, 1000);


    }

    toggleSwitchNotification = (value) => {
        this.setState({ switchValueNotificationAll: !this.state.switchValueNotificationAll }, () => {
            if (this.state.switchValueNotificationAll) {
                this.setState({ switchNotificationMsgs: true, switchNotificationEndorsement: true, switchNotificationConnection: true }, () => {
                    AsyncStorage.setItem(globals.MESSAGESNOTIFICATIONSTATUS, 'true');
                    AsyncStorage.setItem(globals.ENDORSENOTIFICATIONSTATUS, 'true');
                    AsyncStorage.setItem(globals.CONNECTIONNOTIFICATIONSTATUS, 'true');

                })
            } else {
                this.setState({ switchNotificationMsgs: false, switchNotificationEndorsement: false, switchNotificationConnection: false }, () => {
                    AsyncStorage.setItem(globals.MESSAGESNOTIFICATIONSTATUS, 'false');
                    AsyncStorage.setItem(globals.ENDORSENOTIFICATIONSTATUS, 'false');
                    AsyncStorage.setItem(globals.CONNECTIONNOTIFICATIONSTATUS, 'false');
                })
            }
        })
    }

    setAllNotificationSwitchStatus(msgs, notification, connection) {
        if (msgs && notification && connection) {
            this.setState({ switchValueNotificationAll: true })
        } else {
            this.setState({ switchValueNotificationAll: false })

        }
    }

    toggleSwitchMsgs = (value) => {
        this.setState({ switchNotificationMsgs: !this.state.switchNotificationMsgs }, () => {
            AsyncStorage.setItem(globals.MESSAGESNOTIFICATIONSTATUS, this.state.switchNotificationMsgs.toString());
            this.setAllNotificationSwitchStatus(this.state.switchNotificationMsgs, this.state.switchNotificationEndorsement,
                this.state.switchNotificationConnection
            )
        })
    }

    toggleSwitchEndorsement = (value) => {
        this.setState({ switchNotificationEndorsement: !this.state.switchNotificationEndorsement }, () => {
            AsyncStorage.setItem(globals.ENDORSENOTIFICATIONSTATUS, this.state.switchNotificationEndorsement.toString());
            this.setAllNotificationSwitchStatus(this.state.switchNotificationMsgs, this.state.switchNotificationEndorsement,
                this.state.switchNotificationConnection
            )
        })
    }

    toggleSwitchConnection = (value) => {
        this.setState({ switchNotificationConnection: !this.state.switchNotificationConnection }, () => {
            AsyncStorage.setItem(globals.CONNECTIONNOTIFICATIONSTATUS, this.state.switchNotificationConnection.toString());
            this.setAllNotificationSwitchStatus(this.state.switchNotificationMsgs, this.state.switchNotificationEndorsement,
                this.state.switchNotificationConnection
            )
        })
    }

    render() {

        const { switchValueNotificationAll, switchNotificationMsgs, switchNotificationEndorsement, switchNotificationConnection } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                <NavigationEvents
                    onWillFocus={() => this.getValues()}
                />
                <View style={styles.AllTxtView}>
                    <View style={styles.allSwitchView}>
                        <Text style={{
                            marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null,
                            fontSize: globals.font_15, marginRight: 10
                        }}>All</Text>
                        <Switch
                        style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null,}}
                            onValueChange={this.toggleSwitchNotification}
                            value={switchValueNotificationAll}></Switch>
                    </View>

                </View>
                <View style={styles.switchNotiView}>
                    <View style={[styles.mainRenderItemView, { backgroundColor: 'lightgray' }]}>
                        <View style={styles.horizontalItemView}>
                            <View style={styles.middleViewTexts}>
                                <Text style={styles.headingText}>Messages</Text>
                                <Text style={styles.bottomText}>When a registered user sends a message to you</Text>
                            </View>
                            <View style={styles.lastImgViewEnd}>
                                <Switch
                                    onValueChange={this.toggleSwitchMsgs}
                                    value={switchNotificationMsgs}></Switch>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.switchNotiView}>
                    <View style={[styles.mainRenderItemView, { backgroundColor: 'lightgray' }]}>
                        <View style={styles.horizontalItemView}>
                            <View style={styles.middleViewTexts}>
                                <Text style={styles.headingText}>Endorsements</Text>
                                <Text style={styles.bottomText}>When a registered user endorses you</Text>
                            </View>
                            <View style={styles.lastImgViewEnd}>
                                <Switch
                                    onValueChange={this.toggleSwitchEndorsement}
                                    value={switchNotificationEndorsement}></Switch>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.switchNotiView}>
                    <View style={[styles.mainRenderItemView, { backgroundColor: 'lightgray' }]}>
                        <View style={styles.horizontalItemView}>
                            <View style={styles.middleViewTexts}>
                                <Text style={styles.headingText}>Connection Request</Text>
                                <Text style={styles.bottomText}>When a registered user sends a connection request</Text>
                            </View>
                            <View style={styles.lastImgViewEnd}>
                                <Switch
                                    onValueChange={this.toggleSwitchConnection}
                                    value={switchNotificationConnection}></Switch>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Notifications);