import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../utils/globals';

const ITEM_HEIGHT = globals.screenHeight * 0.1136; // 60;
const OPENITEM_HEIGHT = globals.screenHeight * 0.19; // 110;

const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  mainRenderItemView:{
    height: globals.screenHeight * 0.11, borderColor: colors.proUnderline, borderWidth: 1, marginBottom: globals.screenHeight * 0.01, borderRadius: 3,flex:1
  },
  horizontalItemView:{
     flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.015, flex: 1 
  },
  middleViewTexts:{
    justifyContent:'center', marginHorizontal:globals.screenHeight * 0.02,flex:1,
  },
  lastImgViewEnd:{
    justifyContent:'center', alignItems:'flex-end'
  },
  attendesFlatlist:{
     marginTop:globals.screenHeight*0.02,
    paddingHorizontal:globals.screenWidth*0.05
  },
  beforeimgView:{
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946)/2, 
    borderColor:colors.lightGray,
    borderWidth:0.5
  },
  ivItemImageStyle: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946)/2,
    marginTop:-1,
    marginLeft:-1, 
  },
  headingText: {
    fontSize: globals.font_15,
    marginBottom:3,
    color:colors.lightBlack
  },
  bottomText:{
    fontSize: globals.font_13,
    color:colors.lightgray
    
  },
  AllTxtView:{
    alignItems: 'flex-end', marginTop: globals.screenHeight * 0.02
  },
  allSwitchView:{
    flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginRight: 10
  },
  switchNotiView:{
    height: globals.screenHeight * 0.13, marginTop: 10 
  }
});