import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../../utils/globals';
import * as colors from '../../../../../../assets/styles/color';


const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
    mainParentStyle: {
        flex: 1,
      },
      mainRenderItemView:{
        height: globals.screenHeight * 0.13, borderColor: colors.proUnderline, borderWidth: 1, marginBottom: globals.screenHeight * 0.01, borderRadius: 3,flex:1,
        marginHorizontal: globals.screenHeight * 0.02
    },
    loaderbottomview:{
      bottom: (Platform.OS == 'android') ?
                                  globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
    },
      horizontalItemView:{
         flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.017, flex: 1,
         alignItems:'center',
      },
      middleViewTexts:{
        justifyContent:'center', marginHorizontal:globals.screenHeight * 0.02,flex:1,
      },
      lastImgViewEnd:{
        justifyContent:'center', alignItems:'flex-end'
      },
      attendesFlatlist:{
        marginTop:globals.screenHeight*0.05,
        paddingHorizontal:globals.screenWidth*0.05
      },
      beforeimgView:{
        height: globals.screenHeight * 0.0757, //40
        width: globals.screenHeight * 0.0757, //40
        borderRadius: (globals.screenHeight * 0.0946)/2, 
        borderColor:colors.lightGray,
        borderWidth:0.5
      },
      ivItemImageStyle: {
        height: globals.screenHeight * 0.0757, //40
        width: globals.screenHeight * 0.0757, //40
        borderRadius: (globals.screenHeight * 0.0946)/2,
        marginTop:-1,
        marginLeft:-1, 
      },
      attendeeTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12, 
      },
      attende_shareIconStyle: {
        height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.screenHeight*0.05 : globals.screenHeight*0.06 : globals.screenWidth * 0.08,
        width: (iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())  ? (Platform.OS == 'ios') ? globals.screenHeight*0.05  : globals.screenHeight*0.06: globals.screenWidth * 0.08,
      },

      //Search admin modal styles
      modelParentStyle: {
        flex: 1,
      },
      vwHeaderStyle: {
        marginTop: globals.screenHeight * 0.08,
        marginLeft:
          iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
            ? globals.screenWidth * 0.07
            : globals.screenWidth * 0.07,
        alignItems: 'center',
        marginBottom: globals.screenHeight * 0.03,
      },
      tvSearchEventStyle: {
        color: colors.black,
        fontWeight: '500',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
        marginLeft:
          iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
            ? globals.screenWidth * 0.04
            : globals.screenWidth * 0.08,
      },
      vwFlexDirectionRowStyle: {
        flexDirection: 'row',
      },
      toSearchStyle: {
        position: 'absolute',
        bottom: 0,
        height: globals.screenHeight * 0.0662, // 35
        backgroundColor: colors.bgColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: globals.screenWidth,
      },
      tvSearchStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
      },
      textInputViewContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.05,
        borderRadius: 5,
      },
      textInputStyleContainer: {
        flex: 1,
        marginLeft: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        marginVertical: globals.screenHeight*0.03,
      },
    //----------------------

   
});