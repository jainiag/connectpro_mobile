import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { screenHeight, screenWidth } from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  //////Event dashboard common style
  ed_mainContainer: {
    backgroundColor: colors.white,
    flex: 1,
  },
  ed_eventListStyle: {
    flex: 0.2,
    backgroundColor: 'red',
    padding: 10,
  },
  ed_ItemPaddingStyle: {
    marginHorizontal: screenWidth * 0.0312, //10
    marginVertical: screenHeight * 0.0473, //25
  },
  ed_FlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  ed_ItemImageStyle: {
    width: '20%',
  },
  ed_ItemInfoStyle: {
    width: '60%',
  },
  ed_EventNameStyle: {
    fontSize: globals.font_14,
    paddingBottom: 1,
  },
  ed_TimeStyle: {
    fontSize: globals.font_12,
  },
  ed_TypeStyle: {
    fontSize: globals.font_10,
  },
  ed_DateStyle: {
    fontSize: globals.font_14,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    // flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  headerViewStyle: {
    backgroundColor: colors.white,
    height: globals.screenHeight * 0.08,
    width: globals.screenWidth,
    flexDirection: 'row',
    marginTop: 3,
    paddingHorizontal: globals.screenHeight * 0.01,
    paddingVertical: globals.screenHeight * 0.02,
    justifyContent: 'space-between',
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  modalMainView: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.37 : globals.screenHeight * 0.4, justifyContent: 'center', alignItems: 'center', width: globals.screenWidth * 0.9, marginHorizontal: (globals.screenWidth * 0.05), backgroundColor: colors.gray, marginTop: globals.screenHeight * 0.2
  },
  modalInnerMainView: {
    backgroundColor: colors.white, margin: globals.screenWidth * 0.07, width: globals.screenWidth * 0.8, borderRadius: 4, flex: 1,
  },
  modalAlreadyRegisteredView: {
    flexDirection: 'row', padding: globals.screenHeight * 0.01
  },
  modalResendTextView: {
    flex: 1, marginRight: 10
  },
  modalCancelImg: {
    height: globals.screenHeight * 0.03, width: globals.screenHeight * 0.03, tintColor: colors.gray
  },
  modalHorizontalLine: {
    height: 2, backgroundColor: colors.proUnderline, width: '100%', marginTop: (globals.screenHeight * 0.03), marginBottom: (globals.screenHeight * 0.03)
  },
  modalTextInputView: {
    borderColor: colors.gray, borderWidth: 2, marginHorizontal: 10
  },
  modalSubmitView: {
    justifyContent: 'center', backgroundColor: colors.listSelectColor, width: globals.screenWidth * 0.22, alignItems: 'center', height: globals.screenHeight * 0.05, right: 0, position: 'absolute', bottom: (globals.screenHeight * 0.03), marginRight: globals.screenHeight * 0.01, borderRadius: 3
  },
  modalSubmitText: {
    color: colors.white, fontSize: globals.font_13, fontWeight: '600'
  },
  popupinnerViewStyle: {
    flex: 1, marginTop: globals.screenHeight * 0.02,
    marginHorizontal: globals.screenHeight * 0.015,
  },
  popupheaderStyle: {
    fontSize: globals.font_16,
    color: colors.Gray,
    fontWeight: "500"
  },
  popupsubTextStyle: {
    marginTop: globals.screenHeight * 0.01,
    fontSize: globals.font_12,
    color: colors.Gray
  },
  termconditionView: {
    marginVertical: globals.screenHeight * 0.025,
    marginHorizontal: globals.screenWidth * 0.02,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  squareView: {
    // marginTop: globals.screenHeight * 0.0065,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  scceptText: {
    fontSize: globals.font_14,
    color: colors.black
  },
  linkText: {
    fontSize: globals.font_14,
    color: colors.warmBlue,
    textDecorationLine: "underline",
    fontWeight: "500",

  },
  btnViewStyle: {
    marginVertical: globals.screenHeight * 0.01,
    marginHorizontal: globals.screenWidth * 0.02,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  innerbtnViewStyles: {
    alignItems: 'center',
     justifyContent: 'center',
    flexDirection: 'row',
    borderWidth: 0.2,
    borderRadius: 3,
    // marginHorizontal:globals.screenWidth * 0.03
  },
  closeimgStyle: {
    height: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.04,
    tintColor: colors.white,  
    marginLeft:globals.screenWidth * 0.02
  },
  vectorStyle:{
    marginLeft:globals.screenWidth * 0.01,
    alignItems:'center'
  },
  btnTextsStyle: {
    //  paddingLeft:globals.screenWidth * 0.025,
     paddingRight:globals.screenWidth * 0.025,
     paddingVertical: globals.screenWidth * 0.015,
    fontSize: globals.font_16,
    color: colors.white,
    fontWeight: "600",
   textAlign:'center'
  },
  notAvailableDataViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 25,
  },
  tvpaymentTypeStyle: {
    color: colors.redColor,
    fontSize: globals.font_12,
  },
  tvthStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    paddingBottom: 2,
  },
  ed_DateStyle: {
    fontSize: globals.font_14,
  },
  ed_DateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  ed_ThStyle: {
    fontSize: globals.font_10,
  },
  ed_LabelStyle: {
    fontSize: globals.font_14,
  },
  ed_ItemDateStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
  ed_ItemParentPaddingStyle: {
    marginHorizontal: 0,
  },
  ed_dashboardView: {
    height: '80%',
    width: '100%',
  },
  ed_SimpleLineStyle: {
    width: '90%',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 2,
    height: 1,
    backgroundColor: colors.gray,
    opacity: 0.5,
  },
  vsItemParentStyle: {
    flex: 1,
  },
  beforeImgViewStyle: {
    height: screenHeight * 0.0757,//40
    width: screenHeight * 0.0757,//40
    borderRadius: screenHeight * 0.0378,
    borderColor: colors.lightGray,
    borderWidth: 0.3,
  },
  gridViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.10,
    margin: 10,
  },
  imgStyle: {
    tintColor: colors.white,
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.093,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.093,
  },
  gridViewInsideTextItemStyle: {
    marginTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.005,
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 25 : globals.font_14,
    justifyContent: 'center',
    color: colors.white,
    alignItems: 'center',
  },
  unSelectedgridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.17,
    width: screenHeight * 0.17,
    margin: 10,
    backgroundColor: colors.white,
    borderRadius: (screenHeight * 0.17) / 2,
  },
  inactivetext:{
    flex:1,alignItems:'center', justifyContent:'center'
  },
  unSelectedimgStyle: {
    height: 25,
    width: 25,
    tintColor: colors.bgColor,
  },
  unSelectedgridViewInsideTextItemStyle: {
    paddingTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.02,
    fontSize: globals.font_14,
    justifyContent: 'center',
    color: colors.bgColor,
    alignItems: 'center',
  },

  headerParentStyle: {
    flexDirection: 'row',
    height: screenHeight * 0.15,
    alignItems: 'center',
    // backgroundColor: 'blue',
    paddingHorizontal: screenWidth * 0.05,
  },
  ivEventImageStyle: {
    height: screenHeight * 0.0757,//40
    width: screenHeight * 0.0757,//40
    borderRadius: screenHeight * 0.0378,//20
    borderColor: colors.lightGray,
    borderWidth: 0.3,
  },
  eventInfoParentStyle: {
    width: screenWidth * 0.53,
    justifyContent: 'center',
    //  backgroundColor: colors.blue,
    marginLeft: screenWidth * 0.0468, //15
  },
  joinview: {
    width: screenWidth * 0.15,
    justifyContent: 'center',
    backgroundColor: colors.blue,
    // paddingHorizontal:15,
    // paddingVertical:13,
    borderRadius: 2,
    borderWidth: 0.1,
    borderColor: colors.listSelectColor,
    marginLeft: screenWidth * 0.03,
    marginRight: screenWidth * 0.03,
  },
  joinTextStyle: {
    textAlign: 'center',
    color: colors.white,
    // paddingHorizontal:globals.screenWidth * 0.09,
    paddingVertical: globals.screenHeight * 0.01,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_12,
  },
  tvDateTimeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_12,
    marginLeft: globals.screenWidth * 0.01
  },
  tvLabelStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  tvDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
  },
  tvBlueFontStyle: {
    color: colors.blue,
  },
  tvLabeldtStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_14,
  },
  vwDateStyle: {
    flex: 1,
    paddingBottom: globals.screenHeight * 0.012,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  tvEvenTypeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 16 : globals.font_10,
    color: colors.blue,
    marginLeft: globals.screenWidth * 0.01
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fvStyle: {
    marginTop: screenHeight * 0.08,
  },
  bannerView: {
    // marginTop:globals.screenHeight * 0.001,
    height: globals.screenHeight * 0.148,
    width: globals.screenWidth,
    // backgroundColor:"blue"
  },
  adImageStyle: {
    width: '100%',
    height: globals.screenHeight * 0.148,
  },
  waiting_approvalViewContainer: {
     flex:1,
    // height: (Platform.OS == "ios") ? globals.screenHeight * 0.14 : globals.screenHeight * 0.14,
    // width: (Platform.OS == "ios") ? globals.screenWidth * 0.8 : globals.screenWidth * 0.9,
    backgroundColor: colors.gray,
    marginHorizontal:globals.screenWidth * 0.05,
     marginVertical:globals.screenHeight * 0.31,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  waiting_approvalTextStyle: {
    // marginLeft: (Platform.OS == "android") ? globals.screenWidth * 0.04 : 0,
    marginHorizontal:globals.screenWidth * 0.02,
    textAlign:'center',
    fontSize:globals.font_14
  },
  touchableStyle2: {
    marginTop:globals.screenHeight * 0.03,
    paddingHorizontal: 22,
    paddingVertical: 5,
    borderRadius:3,
    backgroundColor: colors.listSelectColor,
  },
  btntextStyless: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
     color:colors.white
  },
});
