/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */
/* eslint-disable no-return-assign */
/* eslint-disable no-useless-constructor */
import React from 'react';
import { Text, View, TouchableOpacity,  TextInput, ScrollView, Alert, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../../../utils/globals';
import globalStyles from '../../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../../assets/styles/color';
import SectionedMultiSelect from '../../../../../../../libs/react-native-sectioned-multi-select';
import MultiSelect from '../../../../../../../libs/react-native-multiple-select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../../../../../redux/acrions/showLoader';
import { API } from '../../../../../../../utils/api';
import Member from '../index';
import loginScreen from '../../../../../AuthenticationScreens/loginScreen';
import KeyboardListener from 'react-native-keyboard-listener';


const TAG = '==:== MemberSearch Screen : ';


class MemberSearch extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Members (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Members ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
        }
    };

    constructor(props) {
        super(props);
        console.log(TAG, props);
        this.state = {
            communityId: this.props.navigation.state.params.communityID,
            memberName: '',
            organizationName: '',
            Communities: '',
            profileCategory: '',
            skills: '',
            loading: false,
            sectionListOpen: false,
            businessCategoryName: '',
            tempBizGroupData: [],
            BizGroups: [],
            categoriesData: [],
            selectedItems: [],
            profileCategory: [],
            profileCategoryData: [],
            skillsAndIneterst: [],
            skillsAndInterestData: [],
            tempGroup: [],
            groupData: [],
            keyboardOpen: false,

        };
    }

    componentDidMount(){
        this._GetMembersIntialData()
    }

    _GetMembersIntialData(){
        if (globals.isInternetConnected === true) {
            this.setState({ loading: true })
            API.getCommunityMembersIntialDataWithId(this.GetCommunityMembersIntialDataResponseData, this.state.communityId, true)
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    GetCommunityMembersIntialDataResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetCommunityMembersIntialDataResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
               const bizGroups = response.Data.BizCategories.BizGroups ;
               const interestSkill = response.Data.SkillsAndInterests ;
               const categoryProfile = response.Data.WhoAmIAll ;
               const group_data = response.Data.GroupsInfo
               this.setState({BizGroups: bizGroups})
               this.setState({skillsAndInterestData: interestSkill})
               this.setState({profileCategoryData: categoryProfile})
                if (group_data == null) {
                    this.setState({ groupData: [] })
                } else {
                    this.setState({ groupData: group_data })
                }
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message);
                })
            }
        },
        error: err => {
            this.setState({ loading: false })
            console.log(
                TAG,
                'GetCommunityMembersIntialDataResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403 || err.StatusCode == 500) {
                this.setState({ loading: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.setState({ loading: false })
        },
    };

      /**
  *  call when _sessionOnPres
  */
 _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
}

    /*
     * method navigate To flatlist screen with paramerters
     */

      navigateToListScreen() {
        const { memberName, organizationName, skillsAndIneterst, profileCategory, tempBizGroupData, tempGroup, } = this.state;
        var profile_Category = profileCategory.toString()
        var skills_And_Ineterst = skillsAndIneterst.toString()
        var tempBiz_GroupData = tempBizGroupData.toString()
        var temp_group = tempGroup.toString()
        Member.searchMember(memberName, organizationName, skills_And_Ineterst, profile_Category, tempBiz_GroupData, temp_group);
        this.props.navigation.goBack();
      }

    onSelectedprofileCategoryChange = profileCategory => {
        this.setState({ profileCategory });
    };

    onSelectedSkillsDataChange = skillsAndIneterst => {
        this.setState({ skillsAndIneterst });
    };

    onSelectedItemsChange = (tempBizGroupData) => {
        this.setState({ tempBizGroupData });
    };

    onSelectedGroupChange = (tempGroup) => {
        this.setState({tempGroup})
    }


    render() {
        const { memberName, organizationName, profileCategoryData, skillsAndInterestData, BizGroups, skillsAndIneterst, profileCategory, tempBizGroupData, groupData, tempGroup } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                <KeyboardListener
                    onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                    onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.svStyle}>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={'Search by Member Name'}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ memberName: text })}
                                returnKeyType="next"
                                blurOnSubmit={false}
                                value={memberName}
                                onSubmitEditing={() => this.memberNameRef.focus()}
                                autoCapitalize="none"
                            />
                        </View>
                        <View style={styles.textInputViewContainer2}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={"Search by Organization Name"}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ organizationName: text })}
                                returnKeyType="done"
                                blurOnSubmit={false}
                                value={organizationName}
                                autoCapitalize="none"
                                ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <MultiSelect
                                hideTags={false}
                                items={profileCategoryData}
                                uniqueKey="Name"
                                ref={(component) => { this.multiSelect = component }}
                                onSelectedItemsChange={this.onSelectedprofileCategoryChange}
                                selectedItems={profileCategory}
                                selectText="Search by Profile Category"
                                searchInputPlaceholderText="Search Profile Category..."
                                onChangeInput={(text) => console.log(text)}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="#CCC"
                                selectedItemTextColor="#CCC"
                                selectedItemIconColor="#CCC"
                                itemTextColor="#000"
                                displayKey="Name"
                                searchInputStyle={{ color: '#CCC' }}
                                submitButtonColor="#CCC"
                                submitButtonText="Submit"
                                hideSubmitButton={true}
                                styleDropdownMenu={styles.multipleSelectDropdownMenu}
                                styleMainWrapper={styles.multipleSelectMainWrapper}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <MultiSelect
                                hideTags={false}
                                items={skillsAndInterestData}
                                uniqueKey="Name"
                                ref={(component) => { this.multiSelect = component }}
                                onSelectedItemsChange={this.onSelectedSkillsDataChange}
                                selectedItems={skillsAndIneterst}
                                selectText="Search by Skills and Interests"
                                searchInputPlaceholderText="Search Skills and Interests..."
                                onChangeInput={(text) => console.log(text)}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="#CCC"
                                selectedItemTextColor="#CCC"
                                selectedItemIconColor="#CCC"
                                itemTextColor="#000"
                                displayKey="Name"
                                searchInputStyle={{ color: colors.black }}
                                submitButtonColor="#CCC"
                                submitButtonText="Submit"
                                styleDropdownMenu={styles.multipleSelectDropdownMenu}
                                styleMainWrapper={styles.multipleSelectMainWrapper}
                                hideSubmitButton={true}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <MultiSelect
                                hideTags={false}
                                items={groupData}
                                uniqueKey="GroupID"
                                ref={(component) => { this.multiSelect = component }}
                                onSelectedItemsChange={this.onSelectedGroupChange}
                                selectedItems={tempGroup}
                                selectText="Search by Groups"
                                searchInputPlaceholderText="Search Group"
                                onChangeInput={(text) => console.log(text)}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="#CCC"
                                selectedItemTextColor="#CCC"
                                selectedItemIconColor="#CCC"
                                itemTextColor="#000"
                                displayKey="GroupName"
                                searchInputStyle={{ color: colors.black }}
                                submitButtonColor="#CCC"
                                submitButtonText="Submit"
                                styleDropdownMenu={styles.multipleSelectDropdownMenu}
                                styleMainWrapper={styles.multipleSelectMainWrapper}
                                hideSubmitButton={true}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <SectionedMultiSelect
                                items={BizGroups}
                                uniqueKey="ID"
                                subKey="BusinessCategories"
                                selectText="Search by Business Categories"
                                showDropDowns={true}
                                readOnlyHeadings={true}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                selectedItems={tempBizGroupData}
                                displayKey="GroupName"
                                subDisplayKey="Name"
                                showCancelButton={false}
                                hideConfirm={false}
                            />
                        </View>
                    </View>
                </ScrollView>
                {(Platform.OS == "android") ?
                    (this.state.keyboardOpen == false) ?
                        <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this.navigateToListScreen()}>
                            <Text style={styles.seachTextStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                        </TouchableOpacity>
                        :
                        null
                    :
                    <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this.navigateToListScreen()}>
                        <Text style={styles.seachTextStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                    </TouchableOpacity>}
            </View>
        );
    }
}


// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(MemberSearch);