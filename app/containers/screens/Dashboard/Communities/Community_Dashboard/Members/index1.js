import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ActivityIndicator, TextInput, Alert, TouchableWithoutFeedback } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from "react-native-modal";
import styles from './style';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import { API } from '../../../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../../../components/NoInternet/index'
import ServerError from '../../../../../../components/ServerError/index'

const iPad = DeviceInfo.getModel();
let TAG = "Members Screen ::==="
let _this = null;

class Members extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        const community_id = (params !== undefined) ? params.communityID : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Members (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Members ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('MemberSearch',{globalEventCount: globalEventCount, communityID: community_id})}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };


    constructor(props) {
        super(props);
        console.log(TAG , props);
        _this = this;
        this.state = {
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isInternetFlag: true,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            totalCommunities: 0,
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            communityID: this.props.navigation.state.params.communityID,
            visibleModal: false,
            receiverID: 0,
            notes: ''
        }
    }

    componentDidMount() {
        this.props.showLoader()
        this.apicallGetMembersList()
    }

    static searchMember( memberName, organizationName, skills_And_Ineterst, profile_Category, tempBiz_GroupData ) {
        _this.setState(
          {
            memberName: memberName,
            businessCategory: tempBiz_GroupData,
            skillsInterest: skills_And_Ineterst,
            whoAmIName: profile_Category,
            companyName: organizationName,
            loading: true,
            responseComes: false
          },
          () => {
            _this.makeListFresh(() => {
                _this.props.showLoader()
              _this.apicallGetMembersList();
            });
          }
        );
      }

      makeListFresh(callback) {
        this.setState(
          {
            pageNumber: 1,
            pageSize: 10,
            totalCommunities: 0,
            membersData: [],
          },
          () => {
            callback();
          }
        );
      }

    /**
     * prepareData when call the API
     */
    prepareData() {
        const {
            pageNumber, pageSize, memberName, companyName, locationSearch, businessCategory, communityName, whoAmIName, skillsInterest, groupname
        } = this.state;

        const data = {};
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        data.MemberNameSearch = memberName
        data.CompanyNameSearch = companyName
        data.LocationSearch = locationSearch
        data.BusinessCategoriesSearch = businessCategory
        data.CommunitiesSearch = this.state.communityID
        data.UserID = globals.userID
        data.WhoAmISearch = whoAmIName
        data.skillsInterestsSearch = skillsInterest
        data.GroupsSearch = groupname
        return data;
    }

    /**
     * APi call for get members list
     */
    apicallGetMembersList() {
        if (globals.isInternetConnected === true) {
            this.setState({ loadingPagination: true, loading: true, isInternetFlag: true })
            API.getMembersList(this.GetCommunityMembersResponseData, this.prepareData(), true)
        } else {
            this.props.hideLoader()
            this.setState({isInternetFlag:false})
            // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
 * Response of MyCommunity listing
 */
    GetCommunityMembersResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetCommunityMembersResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                total = response.Data.TotalRecords;
                _this.props.navigation.setParams({
                    globalEventCount: response.Data.TotalRecords
                });
                if (this.state.pageNumber == 1) {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        membersData: response.Data.MembersList,
                    })
                } else {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        membersData: [...this.state.membersData, ...response.Data.MembersList],
                    })
                }
            } else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetCommunityMembersResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403 ) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
  *  call when _sessionOnPres
  */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes:false },()=>{
            this.props.showLoader();
            this.apicallGetMembersList();
        });
       
    }

    renderFooter = () => {
        if (this.state.loadingPagination && this.state.responseComes) {
            return (
             
                        <ActivityIndicator  style={styles.loaderbottomview} size="large" color={colors.bgColor} />
            )
        } else {
            return (
                <View />
            )
        }
    };

    /**
     * Render FlatList Items
     */
    renderCommunityMembersList(item, index) {
        return (
            <View>
                {
                    this.renderMembersAccordingConditions(item, index)
                }
            </View>
        );
    }

    /**
     * Method of click connect button
     */
    clickConnectBtn() {
        let data = {
            ID: null,
            Note: '',
            ReceiverID: this.state.receiverID,
            SenderID: globals.userID

        }
        if (globals.isInternetConnected === true) {
            this.props.showLoader()
            API.saveMemberConnections(this.saveMemberConnectionsResponseData, data, true)
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    handleConnectModalStatus(visible, id) {
        this.setState({ visibleModal: visible, receiverID: id })
    }

    /**
 * 
 * Upadte TextInput Data
 */
    handleNotes(text) {
        this.setState({
            notes: text,
        });
    }

    /**
    * Render modal of cancel click of waiting for review
    */
    renderConnectModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.visibleModal}
                onRequestClose={() => {
                    this.handleConnectModalStatus(false, this.state.receiverID);
                }}
                onBackdropPress={() => {
                    this.handleConnectModalStatus(false, this.state.receiverID);
                }}
            >
                <View style={[styles.modalMainView]}>
                    <View style={styles.modalInnerMainView}>
                        <Text style={styles.modalUperText}>Your connection request is on it's way. Would you like to add a note to your request.</Text>
                        <View style={styles.underLineView}></View>
                        <Text style={styles.addNoteTxt}>Add Note:</Text>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                multiline={true}
                                maxLength={300}
                                returnKeyType="done"
                                autoCapitalize="none"
                                value={this.state.notes}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.handleNotes(text)}
                                placeholder={'Add Note..'}
                                style={[styles.textInputStyleContainer]}
                            />
                        </View>
                        <View style={styles.modalEndView}>
                            <TouchableOpacity style={styles.closeView} onPress={() => this.handleConnectModalStatus(false, this.state.receiverID)}>
                                <View style={styles.closeView}>
                                    <Text style={styles.closeTxt}>Close</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.sendView} onPress={() => this.clickConnectBtn(false, this.state.receiverID)}>
                                <View style={styles.sendView} >
                                    <Text style={styles.sendTxt}>Send</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>
        )
    }




    /**
     * Response of Accept Reject Invitation 
     */
    saveMemberConnectionsResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveMemberConnectionsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                this.handleConnectModalStatus(false, this.state.receiverID)
                this.clearStates()
                this.props.showLoader()
                this.apicallGetMembersList()
            }
            else {
                Alert.alert(globals.appName, response.Message);
                this.props.hideLoader()
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveMemberConnectionsResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    clearStates(){
        this.setState({
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            totalCommunities: 0,
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            communityID: this.props.navigation.state.params.communityID,
            visibleModal: false,
            receiverID: 0,
            notes: ''
        })
    }

    /**
     * Method of render conditions according views in member lisr
     * @param {*} item 
     * @param {*} index 
     */
    renderMembersAccordingConditions(item, index) {
        const { membersData, loading } = this.state;
        const isMemberName = globals.checkObject(item, 'MemberName');
        const isOrganizationName = globals.checkObject(item, 'OrganizationName');
        const isJobTitle = globals.checkObject(item, 'JobTitle');
        const isEmailAddress = globals.checkObject(item, 'EmailAddress');
        const isPhone = globals.checkObject(item, 'Phone')
        const isMemberProImage = globals.checkImageObject(item, 'ProfilePicturePath');
        if (((item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0) && (item.ConnectionStatus == 0 || item.ConnectionStatus == 3)) ||
            (item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0 && item.ConnectionStatus == 1)) {
            return (
                <TouchableWithoutFeedback style={styles.mainParentStyle}>
                    <View style={{ flex: 1 }}>
                        <View
                            style={[styles.boxViewStyle,
                            { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },]}>
                            <View>
                                <TouchableOpacity>

                                    <View style={styles.userinfoStyle}>
                                        <View style={styles.beforeImgViewStyle}>
                                            <Image
                                                source={{ uri: (isMemberProImage) ? item.ProfilePicturePath :globals.User_img }}
                                                style={styles.imgStyle}

                                            ></Image>
                                        </View>
                                        <View style={styles.textViewStyle}>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle, { fontSize: globals.font_14, marginBottom: 3, fontWeight: '500' }]}>
                                                {(isMemberName) ? item.MemberName : '-'}
                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isOrganizationName) ? item.OrganizationName + "at" + item.JobTitle : '-'}

                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isEmailAddress) ? item.EmailAddress : '-'}

                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isPhone) ? item.Phone : '-'}
                                            </Text>
                                        </View>{
                                            ((item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0) && (item.ConnectionStatus == 0 || item.ConnectionStatus == 3)) ?
                                                <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this.handleConnectModalStatus(true, item.UserID)}>
                                                    <View style={styles.lastImgViewEnd}>
                                                        <View style={styles.connectedBtnView}>
                                                            <Text style={styles.connectTextStyle}>Connect</Text>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                :
                                                (item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0 && item.ConnectionStatus == 1) ?
                                                    <View style={styles.lastImgViewEnd}>
                                                        <View style={[styles.connectedBtnView, { backgroundColor: colors.darkYellow }]}>
                                                            <Text style={styles.connectTextStyle}>Pending</Text>
                                                        </View>
                                                    </View> : null
                                        }

                                    </View>

                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            )
        } else {
            return (
                <TouchableWithoutFeedback style={styles.mainParentStyle}>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity style={{}}>
                            <View style={[styles.userinfoStyleConnected, {}]}>
                                <View style={styles.connectedTwoViews}>
                                    <View style={styles.beforeImgViewStyle}>
                                        <Image
                                            source={{ uri: (isMemberProImage) ? item.ProfilePicturePath : globals.User_img  }}
                                            style={styles.imgStyle}
                                        ></Image>
                                    </View>
                                    <View style={styles.textViewStyle}>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle, { fontSize: globals.font_14, marginBottom: 3, fontWeight: '500' }]}>
                                            {(isMemberName) ? item.MemberName : '-'}
                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isOrganizationName) ? item.OrganizationName + "at" + item.JobTitle : '-'}

                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isEmailAddress) ? item.EmailAddress : '-'}

                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isPhone) ? item.Phone : '-'}
                                        </Text>
                                    </View>
                                </View>
                                {
                                    <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                        <Image source={images.Communities.connectedAttach} style={styles.connectedbtnImgStyle} />
                                    </View>
                                }
                            </View>

                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            )
        }

    }

    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.apicallGetMembersList();
            })
            this.onEndReachedCalledDuringMomentum = true;

        }
    };


    render() {
        const { serverErr, loading, membersData, responseComes, loadingPagination, isInternetFlag } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                {this.renderConnectModal()}
                {
                    (!isInternetFlag) ?
                     <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                    (serverErr === false) ?
                        (
                            (responseComes == false && loadingPagination == true) ?
                                <View></View> :
                                (membersData.length == 0 && responseComes) ?
                                    <View style={globalStyles.nodataStyle}>
                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_MEMBERS_NOT_AVLB}</Text>
                                    </View> :
                                    <View style={styles.mainParentStyle}>
                                        <FlatList
                                            style={{ flex: 1, marginTop: globals.screenHeight * 0.03 }}
                                            showsVerticalScrollIndicator={false}
                                            data={membersData}
                                            renderItem={({ item, index }) => this.renderCommunityMembersList(item, index)}
                                            extraData={this.state}
                                            bounces={false}
                                            keyExtractor={(index, item) => item.toString()}
                                            ListFooterComponent={this.renderFooter}
                                            onEndReached={this.handleLoadMore}
                                            onEndReachedThreshold={0.2}
                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                        />
                                    </View>
                        )
                        :
                        (
                            <ServerError loading={loading} onPress={() => this._tryAgain()} />
                        )
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Members);