/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, KeyboardAvoidingView, Modal, Alert, ScrollView, Image, Linking, ActivityIndicator, FlatList, TextInput, TouchableWithoutFeedback, Keyboard, Platform } from 'react-native';
import styles from './style';
import DocumentPicker from 'react-native-document-picker';
import { API } from '../../../../../../utils/api';
import { connect } from 'react-redux';
import Validation from '../../../../../../utils/validation';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import { bindActionCreators } from 'redux';
import HTML from 'react-native-render-html';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import CustomButton from '../../../../../../components/CustomButton/index';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as globals from '../../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';
import * as colors from '../../../../../../assets/styles/color';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as images from '../../../../../../assets/images/map';
import ImageLoad from 'react-native-image-placeholder';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Nointernet from '../../../../../../components/NoInternet/index'
import RNFS from 'react-native-fs';
import Entypo from 'react-native-vector-icons/Entypo';
import ImagePicker from 'react-native-image-crop-picker';
import { PermissionsAndroid } from 'react-native';
import KeyboardListener from 'react-native-keyboard-listener';

let _this = null;
let commentIndexTrue = '';
let likeIndexTrue = '';
let commentLikeIndex = '';
let likeRefTypeID;
let videosnotallow;
const TAG = '==:== discussionPost : ';
const iPad = DeviceInfo.getModel();

class discussionPost extends Component {

    static navigationOptions = ({ navigation }) => {

        return {
            headerLeft:
                globals.ConnectProbackButton(navigation, 'Discussions'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => { _this.setSearchModalVisible(true); _this.setState({ SearchKeywordDiscussion: '' }) }}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };

    constructor(props) {
        super(props);
        _this = this;
        this.state = {

            selected_category: [],
            isInternetFlag: true,
            email: '',
            drop: false,
            commentDrop: false,
            commentIndexs: '',
            indexs: '',
            communityData: this.props.navigation.state.params.DiccussionCommunityData,
            DiccussionCommunityID: this.props.navigation.state.params.CommunityID,
            loading: false,
            responseComes: false,
            loadingPagination: false,
            serverErr: false,
            PageNumber: 1,
            PageSize: 10,
            DiscussionPostData: [],
            TotalRecords: 0,
            AttachmentList: [],
            modalVisibleImages: false,
            Attachmentimages: '',
            refID: '',
            isLike: false,
            likesCount: '',
            likecommentloader: false,
            totalcommentCount: 0,
            comments: [],
            commentInfo: [],
            commentInfoTopthree: [],
            enableScrollViewScroll: true,
            recentTopCommentID: 0,
            commentPageNumber: 1,
            commentPageSize: 3,
            isFetching: false,
            discussionId: '',
            commentDiscussionID: '',
            isShowCommentModal: false,
            attachmentPopup: false,
            selectedimages: null,
            msgType: '',
            singleFile: '',
            SearchKeywordDiscussion: '',
            isShowSearch: false,
            communityDiscussionId: '',
            isallowkeyboard: true,
            keyboardOpen: false,
            oncrossbottmView: false,
            isOpenFile: false,
            isdownloadLoader: false,
        }
    }

    componentDidMount() {
        if (this.state.communityData == '' || this.state.communityData == undefined || this.state.communityData == null) {
            return null
        }
        else {

            this.setState({
                DiccussionCommunityID:
                    this.props.navigation.state.params.CommunityID
            }, () => {
                this.makeApiCall()
            })
        }
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();

    }

    /**
     * status handle of search modal visibility
     */
    setSearchModalVisible(visible) {
        this.setState({ isShowSearch: visible });
    }

    onCrossApicall() {
        this.props.showLoader()
        this.setState({ SearchKeywordDiscussion: '', responseComes: false, loadingPagination: false, oncrossbottmView: true }, () => {
            this.makeApiCall()
        })
    }

    /**
    * Search modal render function
    */
    renderSearchModel() {
        const { SearchKeywordDiscussion } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setSearchModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <KeyboardListener
                        onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                        onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                    <View style={[styles.vwHeaderStyleSearch, styles.vwFlexDirectionRowStyleSearch]}>
                        <TouchableOpacity onPress={() => { this.onCrossApicall(); this.setSearchModalVisible(false); }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Discussions'}
                        </Text>
                    </View>
                    <View style={styles.mainContainer}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={'Search Discussion..'}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ SearchKeywordDiscussion: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={SearchKeywordDiscussion}
                                autoCapitalize="none"
                            />
                        </View>
                        {(Platform.OS == "android") ?
                            (this.state.keyboardOpen == false) ?
                                <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByDiscussion()}>
                                    <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                                </TouchableOpacity>
                                :
                                null
                            :
                            <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByDiscussion()}>
                                <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                            </TouchableOpacity>}
                    </View>
                </View>
            </Modal>
        );
    }

    /**
    * click on search based on community name
    */
    seacrhByDiscussion() {
        const { SearchKeywordDiscussion } = this.state;
        this.setState({ SearchKeywordDiscussion: SearchKeywordDiscussion }, () => {
            this.setSearchModalVisible(false);
            this.searchDiscussion();
        })
    }

    /**
    * Method of get  communities based on search filter
    */

    searchDiscussion() {
        _this.makeListFresh(() => {
            _this.makeApiCall();
        });
    }

    makeListFresh(callback) {
        this.setState(
            {
                PageNumber: 1,
                PageSize: 10,
                DiscussionPostData: [],
                responseComes: false,

            },
            () => {
                callback();
            }
        );
    }

    async  requestWriteStoragePermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the camera');
                this.setState({ isOpenFile: true }, () => {
                    this.forceUpdate()
                })
            } else {
                this.setState({ isOpenFile: false }, () => {
                    this.forceUpdate()
                })
                console.log('Camera permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    async requestReadStoragePermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('You can use the camera');
                this.setState({ isOpenFile: true }, () => {
                    this.forceUpdate()
                })
            } else {
                this.setState({ isOpenFile: false }, () => {
                    this.forceUpdate()
                })
                console.log('Camera permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    /**
     * API call of DiscussionsCommentList
     */
    makeCommentApiCall() {
        const data = {
            CommunityDiscussionID: this.state.commentDiscussionID,
            PageNumber: this.state.commentPageNumber,
            PageSize: this.state.commentPageSize,
            RecentTopCommentID: this.state.recentTopCommentID,
            UserID: JSON.parse(globals.userID)
        }
        if (globals.isInternetConnected == true) {
            this.setState({ loadingPagination: true })
            API.discussionComments(this.discussionCommentsResponseData, data, true);
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
           * Response callback of discussionCommentsResponseData
           */
    discussionCommentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'discussionCommentsResponseData -> success : ',
                JSON.stringify(response)
            );


            if (response.StatusCode == 200) {
                const recenttotalTopComment = response.Data.CommentInfo
                for (let i = 0; i < recenttotalTopComment.length; i++) {
                    var totalcommentCountList = recenttotalTopComment[i].TotalRecords
                }
                if (this.state.commentPageNumber == 1) {
                    const recentTopComment = response.Data.CommentInfo

                    for (let i = 0; i < recentTopComment.length; i++) {
                        var particularCommentIDList = recentTopComment[i].CommunityDiscussionId
                        var recentTopCommentIDList = recentTopComment[i].RecentTopCommentID
                    }

                    this.setState({
                        responseComes: true, loadingPagination: false, communityDiscussionId: particularCommentIDList,
                        isFetching: false, recentTopCommentID: recentTopCommentIDList, totalcommentCount: totalcommentCountList,
                        commentInfo: response.Data.CommentInfo, userInfo: response.Data.UserInfo, commentInfoTopthree: response.Data.CommentInfo,
                    })
                } else {
                    this.setState({
                        responseComes: true, loadingPagination: false, isFetching: false, totalcommentCount: totalcommentCountList,
                        recentTopCommentID: this.state.recentTopCommentID, communityDiscussionId: particularCommentIDList,
                        commentInfo: [...response.Data.CommentInfo, ...this.state.commentInfo],
                    })
                }


            }
            else {
                Alert.alert(globals.appName, response.Message)
            }
            this.setState({ isFetching: false })

        },
        error: err => {

            this.setState({ responseComes: true, loadingPagination: false, isFetching: false });
            console.log(
                TAG,
                'discussionCommentsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }

        },
        complete: () => {

        },
    };



    /**
    * API call of DiscussionsList
    */
    makeApiCall() {

        {
            (this.state.likecommentloader == true) ?
                this.props.hideLoader() :
                this.props.showLoader()
        }
        this.setState({ loading: true })
        const data = {
            CommunityId: this.state.DiccussionCommunityID,
            SearchKeyword: this.state.SearchKeywordDiscussion,
            RecentTopDiscussionID: this.state.recentTopCommentID,
            PageNumber: this.state.PageNumber,
            PageSize: this.state.PageSize,
            UserId: JSON.parse(globals.userID)
        }
        if (globals.isInternetConnected == true) {
            this.props.showLoader()
            this.setState({ isInternetFlag: true, loadingPagination: true, oncrossbottmView: true, })
            API.discussionsList(this.discussionsListResponseData, data, true);
        } else {
            this.props.hideLoader();
            this.setState({ loading: false, isInternetFlag: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
        * Response callback of discussionsListResponseData
        */
    discussionsListResponseData = {
        success: response => {
            console.log(
                TAG,
                'discussionsListResponseData -> success : ',
                JSON.stringify(response)
            );


            const { DiscussionPostData } = this.state;
            if (response.StatusCode == 200) {
                // total = response.Data.TotalRecords;
                if (this.state.PageNumber == 1) {
                    const TopLike = response.Data

                    for (let i = 0; i < TopLike.length; i++) {
                        var TopLikeIdList = TopLike[i].DiscussionId
                        var TopLikesCount = TopLike[i].LikesCount
                    }

                    this.setState({
                        singleFile: '', selectedimages: null, msgType: '', oncrossbottmView: false,
                        loading: false, responseComes: true, loadingPagination: false, drop: false,
                        DiscussionPostData: response.Data, refID: TopLikeIdList, likesCount: TopLikesCount
                    })



                } else {
                    this.setState({
                        singleFile: '', selectedimages: null, msgType: '', oncrossbottmView: false,
                        loading: false, responseComes: true, loadingPagination: false, refID: TopLikeIdList, drop: false,
                        DiscussionPostData: [...this.state.DiscussionPostData, ...response.Data], likesCount: TopLikesCount
                    })
                }

            }
            else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false })
            }
            this.props.hideLoader()
            this.setState({ loading: false })
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'discussionsListResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                this.props.hideLoader();
                Alert.alert(globals.appName, err.Message, [{ text: 'OK', onPress: this.setState({ msgType: '', selectedimages: null, singleFile: '' }) }])
                this.setState({ msgType: '', selectedimages: null, singleFile: '' })
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /*
    * handleLoadMore for pagination
    */
    handleLoadMore = () => {
        if (!this.onEndReachedPostCalledDuringMomentum) {
            this.setState({
                PageNumber: this.state.PageNumber + 1,
            }, () => {
                this.makeApiCall();
            })
            this.onEndReachedPostCalledDuringMomentum = true;

        }
    };


    /*
        * handleLoadMore for pagination
        */
    handleLoadMoreComments = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                commentPageNumber: this.state.commentPageNumber + 1,
            }, () => {
                this.makeCommentApiCall();
            })
            this.onEndReachedCalledDuringMomentum = true;
        }
    };


    /*
          * renderCommentFooter for pagination
          */
    renderCommentFooter = () => {
        if (this.state.loadingPagination) {
            if (this.state.responseComes) {
                return (

                    <View style={[styles.loaderInner, { marginBottom: (globals.screenHeight * 0.01) }]}>
                        <ActivityIndicator size="large" color={colors.bgColor} />
                    </View>


                )
            }

        } else {
            return (
                <View />
            )
        }
    };

    /*
     * renderFooter for pagination
    */
    renderFooter = () => {
        this.props.hideLoader();
        if (this.state.loadingPagination) {
            if (this.state.responseComes) {
                return (

                    <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />


                )
            }

        } else {
            return (
                <View />
            )
        }
    };


    /*
        * updatePageCount for pagination
        */
    updatePageCount(count, callback) {
        this.setState({ TotalRecords: count }, () => {
            callback();
        });
    }



    /*
    * Try again method
    */
    _tryAgain() {
        this.makeApiCall();
        this.setState({ serverErr: false, loading: false });
    }

    /**
     *  call when _sessionOnPres
     */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }




    /**
      *  method for change CommentView
      */
    changeCommentView = (index, item) => {
        this.setState({ commentDiscussionID: item.DiscussionId }, () => {
            const { DiscussionPostData } = this.state;
            const dataDiscussionPostData = DiscussionPostData;
            for (let i = 0; i < dataDiscussionPostData.length; i++) {
                if (index == i) {
                    commentIndexTrue = index
                    dataDiscussionPostData[index].isSelected = !dataDiscussionPostData[index].isSelected;
                    this.setState({ commentDiscussionID: item.DiscussionId }, () => {
                        this.makeCommentApiCall()
                    })

                } else {
                    dataDiscussionPostData[i].isSelected = false;
                }
            }
            this.setState({ DiscussionPostData: dataDiscussionPostData }, () => { });
        })

    };

    /**
    * API call of commentDeleteApiCall
    */
    commentDeleteApiCall(item, index) {
        deleteCommentIndex = index;
        this.setState({ loading: true })
        const data = {
            CommunityDiscussionId: item.CommunityDiscussionId,
            CommentId: item.CommentId,
            UserID: globals.userID
        }
        if (globals.isInternetConnected == true) {
            this.setState({ isInternetFlag: true })
            API.deleteComments(this.deleteCommentsResponseData, data, true);
        } else {
            this.props.hideLoader();
            this.setState({ loading: false, isInternetFlag: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
       * Response callback of deleteCommentsResponseData
       */
    deleteCommentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'deleteCommentsResponseData -> success : ',
                JSON.stringify(response)
            );
            let array = this.state.DiscussionPostData
            array[commentIndexTrue]["CommentsCount"] = response.Data.CommentsCount;
            this.setState({
                DiscussionPostData: [...array]
            })
            if (response.StatusCode == 200 && response.Result == true) {
                if (this.state.isShowCommentModal == true) {
                    this.setState({
                        commentPageNumber: 1,
                        recentTopCommentID: 0,
                        commentDrop: false,
                        commentInfo: [],
                    }, () => {
                        this.makeCommentApiCall()
                        this.onEndReachedCalledDuringMomentum = false;
                    })
                } else {
                    this.setState({
                        commentPageNumber: 1,
                        recentTopCommentID: 0,
                        commentDrop: false,
                    }, () => {
                        this.makeCommentApiCall()
                        this.onEndReachedCalledDuringMomentum = false;
                    })
                }

            }
            else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false })
            }
            this.props.hideLoader()
            this.setState({ loading: false })
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'deleteCommentsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };



    /**
     *  method for render Comment View
     */
    renderCommentView(item, index) {
        const isCommentLikesCount = globals.checkObject(item, 'LikesCount');
        const isComment = globals.checkObject(item, 'Comment');
        const isFullName = globals.checkObject(item, 'FullName');
        const isCreatedDateStr = globals.checkObject(item, 'CreatedDateStr');
        const isProfilePicturePath = globals.checkImageObject(item, 'ProfilePicturePath');



        return (
            <TouchableWithoutFeedback onPress={() => this._renderCommentOptions2(null, null)}>
                <View style={{ flex: 1 }}>
                    <View style={[styles.itemUpperCommentView]}>
                        <View style={styles.beforeImgView}>
                            <Image
                                source={{ uri: (isProfilePicturePath) ? item.ProfilePicturePath : globals.User_img }}
                                style={styles.profileImageStyle}

                            />
                        </View>
                        <View style={styles.nameView}>
                            <Text numberOfLines={1} style={[styles.firstnamelastname, { width: globals.screenWidth * 0.43 }]}>{(isFullName) ? item.FullName : " "}</Text>
                            <Text numberOfLines={1} style={[styles.innerTexts, { width: globals.screenWidth * 0.46, color: colors.lightBlack }]}>{(isCreatedDateStr) ? "Posted on" + " " + item.CreatedDateStr : "Posted On" + '-'}</Text>
                        </View>
                        <View style={styles.dotView}>
                            {
                                (this.state.commentDrop === false) ?
                                    <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue}
                                        onPress={() => this._renderCommentOptions(item, index)} /> :
                                    (index == this.state.commentIndexs) ?
                                        <View style={styles.commentDelbtns}>
                                            <TouchableOpacity onPress={() => this.commentDeleteApiCall(item, index)}>
                                                <AntDesign style={{
                                                    width: globals.screenWidth * 0.05, height: globals.screenWidth * 0.05,
                                                    alignSelf: 'center', marginLeft:
                                                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 15 : 3, marginTop:
                                                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 9 : 2
                                                }} name='delete' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : 15} color={colors.redColor}
                                                />
                                            </TouchableOpacity>
                                        </View>

                                        :
                                        <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue}
                                            onPress={() => this._renderCommentOptions(item, index)} />
                            }

                        </View>
                    </View>
                    {
                        (isComment) ?

                            <View style={[styles.commentVIews, { paddingBottom: 0 }]}>
                                {/* <HTML uri={item.Comment}
                                            baseFontStyle={styles.htmltagStyle} html={(item.Comment) ? item.Comment : '-'} /> */}
                                <Text style={styles.commentVIewsText}>{(isComment) ? item.Comment : ''}</Text>
                                <View style={[styles.commentnView, { backgroundColor: colors.white }]}>

                                    <TouchableOpacity onPress={() => this.onCommentLikeChecked(item, index)}
                                        style={styles.innerLikeViews}>
                                        <Text style={styles.likeTextstyle}>Like</Text>
                                        {
                                            (item.IsLiked == true) ?
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.filledLike}
                                                    style={styles.likeimgStyle} />
                                                :
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.like}
                                                    style={styles.likeimgStyle} />
                                        }
                                        <Text style={styles.likecountText}>{(isCommentLikesCount) ? item.LikesCount : ''}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>


                            :
                            null
                    }
                </View>
            </TouchableWithoutFeedback>

        )
    }

    /**
         *  method for render comment option for download & delete
         */
    _renderCommentOptions(item, index) {
        this.setState({ commentDrop: true, commentIndexs: index })
    }
    /**
        *  method for render comment option for download & delete
        */
    _renderCommentOptions2(item, index) {
        this.setState({ commentDrop: false, commentIndexs: index })
    }

    /**
         *  method for render option for download & delete
         */
    _renderOptions(item, index) {
        this.setState({ drop: true, indexs: index })
    }
    /**
        *  method for render option for download & delete
        */
    _renderOptions2(item, index) {
        this.setState({ drop: false, indexs: index, commentDrop: false, commentIndexs: index })
    }

    /**
       *  method for clear TextFields
       */
    clearTextFields() {
        this.setState({
            selected_category: [],
            email: '',
            drop: false,
            commentDrop: false,
            commentIndexs: '',
            indexs: '',
            DiccussionCommunityID: this.props.navigation.state.params.DiccussionCommunityData.CommunityInfo.ID,
            loading: false,
            responseComes: false,
            loadingPagination: false,
            serverErr: false,
            PageNumber: 1,
            PageSize: 10,
            DiscussionPostData: [],
            TotalRecords: 0,
            modalVisibleImages: false,
            Attachmentimages: '',
            isLike: false,
            likesCount: '',
            likecommentloader: false,
            comments: '',
            commentInfo: [],
            enableScrollViewScroll: true,
            recentTopCommentID: 0,
            commentPageNumber: 1,
            commentPageSize: 3,
            isFetching: false,
            discussionId: '',
            isShowCommentModal: false,
            SearchKeywordDiscussion: '',
            isShowSearch: false,
            oncrossbottmView: false,
            // isOpenFile: false,
            // isdownloadLoader: false,
        })
    }

    /**
        *  method for delete Data
        */
    deleteAPIcall(item, index) {
        this.props.showLoader();
        this.clearTextFields()
        this.setState({ drop: false })
        this.props.showLoader();
        const data = {
            discussionId: item.DiscussionId,
            UserId: globals.userID,
            AccessToken: JSON.parse(globals.tokenValue),
            IsSuccess: 0
        }
        if (globals.isInternetConnected == true) {
            this.props.showLoader()
            this.setState({ isInternetFlag: true, loadingPagination: true, oncrossbottmView: true, })
            API.deleteDiscussion(this.deleteDiscussionResponseData, data, false);
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
          * Response callback of deleteDiscussionResponseData
          */
    deleteDiscussionResponseData = {
        success: response => {
            console.log(
                TAG,
                'deleteDiscussionResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.props.showLoader();
                this.makeApiCall()
            }
            else {
                this.props.hideLoader()
                Alert.alert(globals.appName, response.Message)
            }

        },
        error: err => {
            this.props.hideLoader();
            console.log(
                TAG,
                'deleteDiscussionResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
    * Method of navigateToWeb
    * 
    */
    navigatePDFToWeb(isAttachmentURL) {
        const URL = isAttachmentURL;
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                console.log("supported" + URL);
                Linking.openURL(URL);
            } else {
                console.log("not supported " + URL);
                Linking.openURL(URL)
            }
        });
    }


    /**
    * Method of navigateToWeb
    * 
    */
    navigateFileUrlToWeb(isAttachmentURL) {
        const URL = isAttachmentURL[1];
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                console.log("supported" + URL);
                Linking.openURL(URL);
            } else {
                console.log("not supported " + URL);
                Linking.openURL(URL)
            }
        });
    }


    /**
          * method for render MimetypePdfView
          */
    renderMimetypePdfView(item, index) {
        isAttachmentURL = globals.checkImageObject(item, 'AttachmentURL');
        return (
            <View style={{
                height: globals.screenHeight * 0.09,
                width: globals.screenWidth * 0.16,
            }}>
                <TouchableOpacity onPress={() => this.navigatePDFToWeb((isAttachmentURL) ? item.AttachmentURL : 'https://www.google.com')}>
                    <Image resizeMode="contain" source={images.Discussion.pdf} style={styles.renderMimetypePdfimgView} />
                </TouchableOpacity>
            </View>
        )
    }

    /**
             * method for render setModalVisible
             */
    setModalVisible(visible) {
        this.setState({ modalVisibleImages: visible })
    }

    /**
     * method for render setCommentModalVisible
    */
    setCommentModalVisible(visible) {
        this.setState({ isShowCommentModal: visible }, () => {
            (visible == false) ?
                this.onEndReachedCalledDuringMomentum = false
                :
                this.setState({ commentInfo: [], commentPageNumber: 1 }, () => {
                    this.makeCommentApiCall()
                })

        })
    }



    /**
    * method for re nder setAttachmentPopupVisible
     */
    setAttachmentPopupVisible(visible) {
        if (visible == false) {
            this.setState({ attachmentPopup: visible, selectedimages: null, singleFile: '', })
        } else {
            this.setState({ attachmentPopup: visible, selectedimages: null, singleFile: '', })
        }

    }


    /**
                * method for _clsoeCommentModal
                */
    _clsoeCommentModal() {
        this.setCommentModalVisible(false)
    }



    /**
            * method for _clsoeModal
            */
    _clsoeModal() {
        this.setModalVisible(false)
    }

    /**
          * method for render MimetypeImageView
          */
    renderMimetypeImageView(item, index) {
        isAttachmentURL = globals.checkImageObject(item, 'AttachmentURL');

        return (
            <View style={{ marginTop: globals.screenWidth * 0.018, marginHorizontal: globals.screenWidth * 0.013, }}>

                <TouchableOpacity
                    onPress={() => {
                        this.setState({ Attachmentimages: item.AttachmentURL }, () => {
                            this.setModalVisible(true)
                        })
                    }}>
                    <ImageLoad
                        resizeMode='contain'
                        style={[styles.imgStyles,]}
                        source={{ uri: (isAttachmentURL) ? item.AttachmentURL : "https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg" }}
                        isShowActivity={false}
                        placeholderSource={"https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg"}
                        placeholderStyle={styles.imgStyles}

                    />
                </TouchableOpacity>

            </View>
        )
    }

    /**
        *  method for image PopView
        */
    imagePopView() {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.state.modalVisibleImages}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}>
                <View style={styles.p_tab_modalMainViewStyle}>
                    <View style={styles.p_tab_modalIconContainer}>
                        <AntDesign
                            name="closecircleo" size={globals.screenHeight * 0.05} color={colors.white} onPress={() => this._clsoeModal()}
                        />
                    </View>
                    <View style={styles.p_tab_modalInnerViewContainer}>
                        <ImageLoad
                            resizeMode='contain'
                            style={styles.p_tab_modalPicStyle}
                            source={{ uri: this.state.Attachmentimages }}
                            isShowActivity={false}
                            placeholderSource={"https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg"}
                            placeholderStyle={styles.p_tab_modalPicStyle}

                        />

                    </View>
                </View>
            </Modal>
        )
    }




    /**
        *  method for LikeApiCall
        */
    LikeApiCall(refId, isLiked, RefTypeID) {
        likeRefTypeID = RefTypeID;
        if (globals.isInternetConnected == true) {
            const data = {
                AccessToken: JSON.parse(globals.tokenValue),
                IsLike: isLiked,
                RefID: refId,
                RefTypeID: RefTypeID,
                UserID: JSON.parse(globals.userID)
            }
            {
                API.saveDiscussionsLikes(this.saveDiscussionsLikesResponseData, data, false);
            }

        } else {

            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
             * Response callback of saveDiscussionsLikesResponseData
             */
    saveDiscussionsLikesResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveDiscussionsLikesResponseData -> success : ',
                JSON.stringify(response)
            );
            if (likeRefTypeID === 2) {
                let array1 = this.state.commentInfo
                array1[commentLikeIndex]["LikesCount"] = response.Data.LikesCount;
                array1[commentLikeIndex]["IsLiked"] = !array1[commentLikeIndex]["IsLiked"]
                this.setState({
                    commentInfo: [...array1]
                })
            } else {
                let array2 = this.state.DiscussionPostData
                array2[likeIndexTrue]["LikesCount"] = response.Data.LikesCount;
                array2[likeIndexTrue]["IsLiked"] = !array2[likeIndexTrue]["IsLiked"]
                this.setState({
                    DiscussionPostData: [...array2]
                })
            }

            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({ likecommentloader: true, PageNumber: 1, }, () => {
                })
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }

        },
        error: err => {
            console.log(
                TAG,
                'saveDiscussionsLikesResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
        },
    };

    /**
    * Method of Checked Like btn
    * 
    */
    onChecked(item, index) {
        const UserStatus = this.props.navigation.state.params.UserStatus;
        if (UserStatus === 1 || UserStatus === 4 || UserStatus === 6 || UserStatus === 2) {
            likeIndexTrue = index;
            this.LikeApiCall(item.DiscussionId, !item.IsLiked, 1)
        } else {
            Alert.alert(globals.appName, "Please join the community to view the community information")
        }
    }

    /**
    * Method of comment's Checked Like btn
    * 
    */
    onCommentLikeChecked(item, index) {
        commentLikeIndex = index;
        this.LikeApiCall(item.CommentId, !item.IsLiked, 2)
    }


    checkcommentwhitespace(comment) {
        let regex = /^[^\s]+(\s+[^\s]+)*$/;
        if (!regex.test(comment)) {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME,
                [{ text: 'OK', onPress: () => this.setState({ comments: '' }) }])
        } else {
            this.props.showLoader()
            const data = {
                AccessToken: JSON.parse(globals.tokenValue),
                Comment: comment,
                CommunityDiscussionId: this.state.commentDiscussionID,
                UserID: JSON.parse(globals.userID)
            }
            if (globals.isInternetConnected == true) {
                API.saveDiscussionsComments(this.saveDiscussionsCommentsResponseData, data, true);
            } else {
                this.props.hideLoader();
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
            }
        }
    }


    /**
      *  API call for SaveComment
      */
    makeSaveCommentApiCall(index, item, comment) {
        if (comment == undefined || comment == '') {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME,
                [{ text: 'OK', onPress: () => this.setState({ comments: '' }) }])
        } else {
            this.checkcommentwhitespace(comment)
        }
    }


    /**
           * Response callback of saveDiscussionsCommentsResponseData
           */
    saveDiscussionsCommentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveDiscussionsCommentsResponseData -> success : ',
                JSON.stringify(response)
            );
            let array = this.state.DiscussionPostData
            array[commentIndexTrue]["CommentsCount"] = response.Data.CommentsCount;
            this.setState({
                DiscussionPostData: [...array]
            })
            if (response.StatusCode == 200 && response.Result == true) {

                this.setState({
                    comments: '',
                    commentPageNumber: 1,
                    commentPageSize: 3,
                    PageNumber: 1,
                }, () => {
                    this.makeCommentApiCall()
                })
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
            this.setState({ isFetching: false });
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false, isFetching: false });
            console.log(
                TAG,
                'saveDiscussionsCommentsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)

        },
        complete: () => {
            this.props.hideLoader();
        },
    };


    downloadimagesondevice(item, index) {

        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        if (this.state.isOpenFile == true) {
            this.setState({ isdownloadLoader: true }, () => {
                this.dwonloadImagesFile(item, index)
            })

        } else {
            // Alert.alert(
            //     globals.appName,
            //     "Allow to access photos, media, and files on your device?",
            //     [{ text: 'OK', onPress: () => Linking.openSettings() },
            //     { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
            // );
        }
    }


    /**
           *  method for dwonloadImagesFile
           */
    dwonloadImagesFile(item, index) {
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        this.setState({ isdownloadLoader: true })
        {
            var AttachmentLists1 = item.AttachmentList;
            for (let m = 0; m < AttachmentLists1.length; m++) {
                var SelectedImg = AttachmentLists1[m].AttachmentURL
                var SelectedImgName = AttachmentLists1[m].FileName
                let imgArray = []
                let ProfileImg = SelectedImg
                let imgNmae = SelectedImgName
                let imgNmaestr = imgNmae.substring(0, imgNmae.length - 4);
                let path = Platform.OS == 'android' ? RNFS.ExternalStorageDirectoryPath : RNFS.DocumentDirectoryPath;
                const absolutePath = `${path}/ConnectProImages`;
                RNFS.exists(absolutePath)
                    .then((result) => {

                        if (result) {
                            RNFS.downloadFile({ fromUrl: ProfileImg, toFile: absolutePath + `/${imgNmaestr}.jpg` }).promise
                                .then((res) => {
                                    this.setState({ isdownloadLoader: false });
                                    {
                                        (m == AttachmentLists1.length - 1) ?
                                            Alert.alert(globals.appName, "Image download successfully.") :
                                            null
                                    }

                                })
                                .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                        } else {
                            RNFS.mkdir(absolutePath)
                                .then((success) => {
                                    RNFS.downloadFile({ fromUrl: ProfileImg, toFile: absolutePath + `/${imgNmaestr}.jpg` }).promise
                                        .then((res) => {
                                            this.setState({ isdownloadLoader: false });
                                            console.log("dwonloadImagesFile resmkdir", res)
                                            Alert.alert(globals.appName, "Image download successfully.")
                                        })
                                        .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                                })
                                .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                        }
                    })
                    .catch((error) => {
                        RNFS.mkdir(absolutePath)
                            .then((success) => {
                                RNFS.downloadFile({ fromUrl: ProfileImg, toFile: absolutePath + `/${imgNmaestr}.jpg` }).promise
                                    .then((res) => {
                                        this.setState({ isdownloadLoader: false });
                                        console.log("dwonloadImagesFile catchDIR", res)
                                    })
                                    .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                            })
                            .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                    });
            }
        }


    }

    downloadondevice(item, index) {
        console.log("this.state.isOpenFile", this.state.isOpenFile);

        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        if (this.state.isOpenFile == true) {
            this.setState({ isdownloadLoader: true }, () => {
                this.dwonloadPDFFile(item, index)
            })
        }
        else {
            // Alert.alert(
            //     globals.appName,
            //     "Allow to access photos, media, and files on your device?",
            //     [{ text: 'OK', onPress: () => Linking.openSettings() },
            //     { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
            // );
        }
    }


    /**
       *  method for dwonloadPDFFile
       */
    dwonloadPDFFile(item, index) {
        this.setState({ isdownloadLoader: true })
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        var AttachmentLists = item.AttachmentList;
        for (let m = 0; m < AttachmentLists.length; m++) {
            var SelectedPDF = AttachmentLists[m].AttachmentURL
            var SelectedFileName = AttachmentLists[m].FileName
        }
        let ProfilePdf = SelectedPDF
        let FileNmaePdf = SelectedFileName
        let Filenamestr = FileNmaePdf.substring(0, FileNmaePdf.length - 4);
        let path = Platform.OS == 'android' ? RNFS.ExternalStorageDirectoryPath : RNFS.DocumentDirectoryPath;
        const absolutePath = `${path}/ConnectPro`;
        console.log("absolutePath ConnectProTestingPDF", absolutePath);

        RNFS.exists(absolutePath)
            .then((result) => {
                if (result) {
                    RNFS.downloadFile({ fromUrl: ProfilePdf, toFile: absolutePath + `/${Filenamestr}.pdf` }).promise
                        .then((res) => {
                            console.log("res", res)
                            this.setState({ isdownloadLoader: false })
                            Alert.alert(globals.appName, "File download successfully.")
                        })
                        .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                } else {
                    RNFS.mkdir(absolutePath)
                        .then((success) => {
                            RNFS.downloadFile({ fromUrl: ProfilePdf, toFile: absolutePath + `/${Filenamestr}.pdf` }).promise
                                .then((res) => {
                                    console.log("resmkdir", res)
                                    this.setState({ isdownloadLoader: false });
                                    Alert.alert(globals.appName, "File download successfully.")
                                })

                                .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                        })
                        .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                }
            })
            .catch((error) => {
                RNFS.mkdir(absolutePath)
                    .then((success) => {
                        RNFS.downloadFile({ fromUrl: ProfilePdf, toFile: absolutePath + `/${Filenamestr}.pdf` }).promise
                            .then((res) => {
                                this.setState({ isdownloadLoader: false });
                                console.log("catchDIR", res)
                            })
                            .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                    })
                    .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
            });
    }



    onChangeAddtext(text, index) {
        let { comments } = this.state;
        comments[index] = text;
        this.setState({
            comments,
        });
    }

    /**
              * method for  parentScroolView can not scroll
              */
    onEnableScroll = (value) => {
        this.setState({
            enableScrollViewScroll: value,
        });
    };

    /**
              * method for convert decode Text into URL
              */
    replaceAll(str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);

    }

    /**
                  *method for convert decode Text into URL
                  */
    escapeRegExp(string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }


    /**
     *  method for render Item View
     */
    renderItemView(item, index) {
        const { commentInfo, totalcommentCount } = this.state;
        const isDescription = globals.checkObject(item, 'Description');
        const isFirstName = globals.checkObject(item, 'FirstName');
        const isLastName = globals.checkObject(item, 'LastName');
        const isLikesCount = globals.checkObject(item, 'LikesCount');
        const isCommentsCount = globals.checkObject(item, 'CommentsCount');
        const isCreatedDateStr = globals.checkObject(item, 'CreatedDateStr');
        const isProfilePicturePath = globals.checkImageObject(item, 'ProfilePicturePath');
        const isTime = (isCreatedDateStr) ? item.CreatedDateStr.substring(11) : '-'
        const Description = (isDescription) ? item.Description : ' ';
        const DescreptedText = decodeURIComponent(Description);
        const url = JSON.stringify(item.Description)
        let FileUrl = url.includes("href")

        if (FileUrl) {
            const finaluRL = DescreptedText.trim()
            var newStr = this.replaceAll(finaluRL, '"', "'")

            const DescreptedUrl = '"' + newStr + '"';
            const finaluRL1 = DescreptedUrl.trim()
            // console.log("finaluRL1-->--------------------" + finaluRL1)

            var result = finaluRL1.match(/href=\'([^']+)\'/);
            // console.log("href-->--------------------" + result)
        }
        const attachment = item.AttachmentList[0];
        const mimetype = JSON.stringify(attachment.MimeType)
        let MimeTypeimage = mimetype.includes("png") || mimetype.includes("jpeg") || mimetype.includes("jpg")
        let MimeTypepdf = mimetype.includes("pdf")
        const topThreeComment = commentInfo.slice(0, 3);


        return (
            <TouchableWithoutFeedback onPress={() => this._renderOptions2(item, index)}>
                <View style={styles.mainItemView}>
                    {this.renderCommentsModel()}
                    <View style={styles.itemUpperView}>
                        <View style={styles.beforeImgView}>
                            <Image
                                source={{ uri: (isProfilePicturePath) ? item.ProfilePicturePath : globals.User_img }}
                                style={styles.profileImageStyle}

                            />
                        </View>
                        <View style={styles.nameView}>
                            <Text numberOfLines={1} style={[styles.firstnamelastname, { width: globals.screenWidth * 0.43 }]}>{(isFirstName) ? (isLastName) ? item.FirstName + " " + item.LastName : '-' : '-'}</Text>
                            <Text numberOfLines={1} style={[styles.innerTexts, { width: globals.screenWidth * 0.46 }]}>{(isCreatedDateStr) ? "Posted on" + " " + item.CreatedDateStr : "Posted On" + '-'}</Text>
                        </View>
                        <View style={styles.dotView}>
                            {(this.state.drop === false) ?
                                (item.UserID == globals.userID) ?
                                    <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue} onPress={() => this._renderOptions(item, index)} />
                                    :
                                    <View />
                                :
                                (index == this.state.indexs) ?

                                    <View style={styles.delndownloadStyle}>
                                        <TouchableOpacity onPress={() => this.deleteAPIcall(item, index)}>
                                            <AntDesign style={{ padding: globals.screenWidth * 0.015 }} name='delete' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : 18} color={colors.redColor}
                                            />
                                        </TouchableOpacity>
                                        {
                                            (MimeTypepdf) ?
                                                <TouchableOpacity onPress={() => (Platform.OS == "ios") ? this.dwonloadPDFFile(item, index) : (this.state.isOpenFile == false) ? this.downloadondevice(item, index) : this.dwonloadPDFFile(item, index)}>
                                                    {
                                                        (this.state.isdownloadLoader == true) ?
                                                            <ActivityIndicator size={"small"} color={colors.warmBlue} />
                                                            :
                                                            <AntDesign style={{ padding: globals.screenWidth * 0.015 }} name='clouddownloado' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : 18} color={colors.warmBlue} />
                                                    }
                                                </TouchableOpacity> :
                                                (MimeTypeimage) ?
                                                    <TouchableOpacity onPress={() => (Platform.OS == "ios") ? this.dwonloadImagesFile(item, index) : (this.state.isOpenFile == false) ? this.downloadimagesondevice(item, index) : this.dwonloadImagesFile(item, index)}>
                                                        {
                                                            (this.state.isdownloadLoader == true) ?
                                                                <ActivityIndicator size={"small"} color={colors.warmBlue} />
                                                                :
                                                                <AntDesign style={{ padding: globals.screenWidth * 0.015 }} name='clouddownloado' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : 20} color={colors.warmBlue} />
                                                        }

                                                    </TouchableOpacity> :
                                                    null
                                        }
                                    </View>

                                    :
                                    (item.UserID == globals.userID) ?
                                        <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue} onPress={() => this._renderOptions(item, index)} />
                                        :
                                        <View />
                            }
                        </View>
                    </View>


                    {
                        (FileUrl == true) ?
                            <View>
                                <View style={styles.mainSecondView}>
                                    <View style={styles.itemLowerView}>
                                        <HTML uri={DescreptedText} onLinkPress={() => this.navigateFileUrlToWeb(result)}
                                            baseFontStyle={styles.htmltagStyle} html={isDescription ? DescreptedText : '-'} />
                                    </View>
                                </View>
                                <View style={styles.commentnView}>


                                    <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                        style={styles.innercommentnview}>
                                        <Text style={styles.likeTextstyle}>Like</Text>

                                        {
                                            (item.IsLiked == true) ?
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.filledLike}
                                                    style={styles.likeimgStyle} />
                                                :
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.like}
                                                    style={styles.likeimgStyle} />
                                        }

                                        <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                        <Text style={styles.likeTextstyle}>Comment</Text>
                                        <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                        <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                    </TouchableOpacity>
                                </View>

                                {
                                    (item.isSelected == true) ?
                                        <View style={styles.commentsView}>
                                            <View style={styles.commentListingView}>
                                                <View style={styles.mainCommentView}>
                                                    <View style={styles.beforeImgView}>
                                                        <Image
                                                            source={{ uri: item.image }}
                                                            style={styles.profileImageStyle}
                                                        />
                                                    </View>
                                                    <View>
                                                        <View style={styles.textInputViewStyle}>
                                                            <TextInput
                                                                placeholder='Add a Comment'
                                                                placeholderTextColor={colors.lightGray}
                                                                style={styles.textInputStyle}
                                                                blurOnSubmit={true}
                                                                value={this.state.comments}
                                                                onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                onFocus={() => { this.setState({ attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                onChangeText={text => {
                                                                    this.setState({
                                                                        comments: text,
                                                                    });
                                                                }}
                                                            />
                                                            <TouchableOpacity style={styles.addCommentTextStyle} onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.comments)}>
                                                                <Icon
                                                                    name='send'
                                                                    size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                    color={colors.warmBlue}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            </TouchableOpacity>
                                                        </View>
                                                    </View>
                                                </View>
                                                {
                                                    (totalcommentCount >= 4) ?
                                                        <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                            <View style={styles.flexDirectionStyle}>
                                                                <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                    name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                />
                                                                <Text style={styles.api_paymentTextStyle}>Load Previous Comments</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                        : null
                                                }


                                                <FlatList
                                                    data={topThreeComment}
                                                    extraData={this.state}
                                                    showsVerticalScrollIndicator={false}
                                                    bounces={false}
                                                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                    renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                    listKey={(x, i) => i.toString()}
                                                />

                                            </View>

                                        </View>
                                        :
                                        null
                                }
                            </View>
                            :
                            (MimeTypepdf == true) ?
                                <View>
                                    <View style={styles.mainSecondView}>
                                        <View style={styles.itemLowerView}>
                                            <HTML baseFontStyle={styles.htmltagStyle} numberOfLines={1} html={DescreptedText} />
                                            <FlatList
                                                style={styles.renderMimetypeImagemainView}
                                                data={item.AttachmentList}
                                                extraData={this.state}
                                                bounces={false}
                                                showsVerticalScrollIndicator={false}
                                                renderItem={({ item, index }) => this.renderMimetypePdfView(item, index)}
                                                listKey={(x, i) => i.toString()} />
                                        </View>
                                    </View>
                                    <View style={styles.commentnView}>
                                        <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                            style={styles.innercommentnview}>
                                            <Text style={styles.likeTextstyle}>Like</Text>
                                            {
                                                (item.IsLiked == true) ?
                                                    <Image resizeMode="contain"
                                                        source={images.Discussion.filledLike}
                                                        style={styles.likeimgStyle} />
                                                    :
                                                    <Image resizeMode="contain"
                                                        source={images.Discussion.like}
                                                        style={styles.likeimgStyle} />
                                            }

                                            <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                            <Text style={styles.likeTextstyle}>Comment</Text>
                                            <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                            <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {
                                        (item.isSelected == true) ?
                                            <View style={styles.commentsView}>
                                                <View style={styles.commentListingView}>
                                                    <View style={styles.mainCommentView}>

                                                        <Image
                                                            source={{ uri: item.image }}
                                                            style={styles.profileImageStyle}
                                                        />
                                                        <View>
                                                            <View style={styles.textInputViewStyle}>
                                                                <TextInput
                                                                    placeholder='Add a Comment'
                                                                    placeholderTextColor={colors.lightGray}
                                                                    style={styles.textInputStyle}
                                                                    blurOnSubmit={true}
                                                                    value={this.state.comments}
                                                                    onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                    onFocus={() => { this.setState({ attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                    onChangeText={text => {
                                                                        this.setState({
                                                                            comments: text,
                                                                        });
                                                                    }}
                                                                />
                                                                <TouchableOpacity style={styles.addCommentTextStyle} onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.comments)}>
                                                                    <Icon
                                                                        name='send'
                                                                        size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                        color={colors.warmBlue}
                                                                        style={{ alignSelf: 'center' }}
                                                                    />
                                                                </TouchableOpacity>
                                                            </View>
                                                        </View>
                                                    </View>
                                                    {
                                                        (totalcommentCount >= 4) ?
                                                            <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                                <View style={styles.flexDirectionStyle}>
                                                                    <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                        name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                    />
                                                                    <Text style={styles.api_paymentTextStyle}>Load Previous Comments</Text>
                                                                </View>
                                                            </TouchableOpacity>
                                                            : null
                                                    }


                                                    <FlatList
                                                        data={topThreeComment}
                                                        extraData={this.state}
                                                        showsVerticalScrollIndicator={false}
                                                        bounces={false}
                                                        onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                        renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                        listKey={(x, i) => i.toString()}
                                                    />

                                                </View>

                                            </View>
                                            :
                                            null
                                    }
                                </View>
                                :
                                (MimeTypeimage == true) ?
                                    <View>
                                        <View style={styles.mainSecondView}>
                                            <View style={styles.itemLowerViewimg}>
                                                <HTML baseFontStyle={[styles.htmltagStyle]} numberOfLines={1} html={DescreptedText} />
                                                {this.imagePopView(item.AttachmentList)}
                                                <FlatList
                                                    style={[styles.renderMimetypeImagemainView]}
                                                    data={item.AttachmentList}
                                                    numColumns={2}
                                                    extraData={this.state}
                                                    bounces={false}
                                                    showsVerticalScrollIndicator={false}
                                                    renderItem={({ item, index }) => this.renderMimetypeImageView(item, index)}
                                                    listKey={(x, i) => i.toString()} />
                                            </View>
                                        </View>
                                        <View style={styles.commentnView}>
                                            <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                                style={styles.innercommentnview}>
                                                <Text style={styles.likeTextstyle}>Like</Text>
                                                {
                                                    (item.IsLiked == true) ?
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.filledLike}
                                                            style={styles.likeimgStyle} />
                                                        :
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.like}
                                                            style={styles.likeimgStyle} />
                                                }
                                                <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                                <Text style={styles.likeTextstyle}>Comment</Text>
                                                <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                                <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                            </TouchableOpacity>
                                        </View>

                                        {
                                            (item.isSelected == true) ?
                                                <View style={styles.commentsView}>
                                                    <View style={styles.commentListingView}>
                                                        <View style={styles.mainCommentView}>

                                                            <Image
                                                                source={{ uri: item.image }}
                                                                style={styles.profileImageStyle}
                                                            />
                                                            <View>
                                                                <View style={styles.textInputViewStyle}>
                                                                    <TextInput
                                                                        placeholder='Add a Comment'
                                                                        placeholderTextColor={colors.lightGray}
                                                                        style={styles.textInputStyle}
                                                                        blurOnSubmit={true}
                                                                        value={this.state.comments}
                                                                        onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                        onFocus={() => { this.setState({ attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                        onChangeText={text => {
                                                                            this.setState({
                                                                                comments: text,
                                                                            });
                                                                        }}
                                                                    />
                                                                    <TouchableOpacity style={styles.addCommentTextStyle} onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.comments)}>
                                                                        <Icon
                                                                            name='send'
                                                                            size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                            color={colors.warmBlue}
                                                                            style={{ alignSelf: 'center' }}
                                                                        />
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </View>
                                                        {
                                                            (totalcommentCount >= 4) ?
                                                                <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                                    <View style={styles.flexDirectionStyle}>
                                                                        <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                            name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                        />
                                                                        <Text style={styles.api_paymentTextStyle}>Load Previous Comments</Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                                : null
                                                        }


                                                        <FlatList
                                                            data={topThreeComment}
                                                            extraData={this.state}
                                                            showsVerticalScrollIndicator={false}
                                                            bounces={false}
                                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                            renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                            listKey={(x, i) => i.toString()}
                                                        />

                                                    </View>

                                                </View>
                                                :
                                                null
                                        }
                                    </View>
                                    :
                                    <View>
                                        <View style={styles.mainSecondView}>
                                            <View style={styles.itemLowerView}>
                                                <HTML baseFontStyle={styles.htmltagStyle} html={isDescription ? DescreptedText : '-'} />
                                            </View>
                                        </View>
                                        <View style={styles.commentnView}>
                                            <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                                style={styles.innercommentnview}>
                                                <Text style={styles.likeTextstyle}>Like</Text>

                                                {
                                                    (item.IsLiked == true) ?
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.filledLike}
                                                            style={styles.likeimgStyle} />
                                                        :
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.like}
                                                            style={styles.likeimgStyle} />
                                                }
                                                <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                                <Text style={styles.likeTextstyle}>Comment</Text>
                                                <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                                <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                            </TouchableOpacity>
                                        </View>

                                        {
                                            (item.isSelected == true) ?
                                                <View style={styles.commentsView}>
                                                    <View style={styles.commentListingView}>
                                                        <View style={styles.mainCommentView}>

                                                            <Image
                                                                source={{ uri: item.image }}
                                                                style={styles.profileImageStyle}
                                                            />
                                                            <View>
                                                                <View style={styles.textInputViewStyle}>
                                                                    <TextInput
                                                                        placeholder='Add a Comment'
                                                                        placeholderTextColor={colors.lightGray}
                                                                        style={styles.textInputStyle}
                                                                        value={this.state.comments}
                                                                        blurOnSubmit={true}
                                                                        onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                        onFocus={() => { this.setState({ attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                        onChangeText={text => {
                                                                            this.setState({
                                                                                comments: text,
                                                                            });
                                                                        }}
                                                                    />
                                                                    <TouchableOpacity style={styles.addCommentTextStyle} onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.comments)}>
                                                                        <Icon
                                                                            name='send'
                                                                            size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                            color={colors.warmBlue}
                                                                            style={{ alignSelf: 'center' }}
                                                                        />
                                                                    </TouchableOpacity>
                                                                </View>
                                                            </View>
                                                        </View>
                                                        {
                                                            (totalcommentCount >= 4) ?
                                                                <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                                    <View style={styles.flexDirectionStyle}>
                                                                        <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                            name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                        />
                                                                        <Text style={styles.api_paymentTextStyle}>Load Previous Comments</Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                                : null
                                                        }


                                                        <FlatList
                                                            data={topThreeComment}
                                                            extraData={this.state}
                                                            showsVerticalScrollIndicator={false}
                                                            bounces={false}
                                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                            renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                            listKey={(x, i) => i.toString()}
                                                        />

                                                    </View>

                                                </View>
                                                :
                                                null
                                        }
                                    </View>

                    }

                </View>
            </TouchableWithoutFeedback>
        );
    }

    /**
           * Render LoadPreviosComment
           */
    LoadPreviosComment() {
        this.onEndReachedCalledDuringMomentum = false
        {
            (this.state.commentInfo == []) ?
                null :
                this.handleLoadMoreComments()
        }

    }

    /**
        * Render Comments modal method
        */
    renderCommentsModel() {
        const { commentInfo, totalcommentCount, loadingPagination } = this.state
        return (

            <View style={{ flex: 1 }}>
                <Modal
                    animationType="none"
                    transparent={false}
                    visible={this.state.isShowCommentModal}
                    onRequestClose={() => {
                        this.setCommentModalVisible(false);
                    }}>
                    <View
                        style={[styles.mainContainer, { marginBottom: globals.screenHeight * 0.04 }]}>
                        <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                            <TouchableOpacity onPress={() => this.setCommentModalVisible(false)}>
                                <Icon name="close" size={globals.screenHeight * 0.04} color="blue" />
                            </TouchableOpacity></View>
                        {
                            ((commentInfo.length === totalcommentCount || totalcommentCount == undefined || loadingPagination)) ?
                                null :
                                <TouchableOpacity onPress={() => this.LoadPreviosComment()}>
                                    <View style={styles.loadCommentmodalView}>
                                        <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                            name='reload1'
                                            size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : 15} color={colors.lightGray}
                                        />
                                        <Text style={[styles.api_paymentCommentTextStyle]}>Load Previous Comments</Text>
                                    </View>
                                </TouchableOpacity>
                        }
                        <ScrollView bounces={false} style={[styles.scrollFlatStyle]} keyboardShouldPersistTaps='always'>
                            <FlatList
                                data={commentInfo}
                                extraData={this.state}
                                scrollEnabled={true}
                                horizontal={false}
                                showsVerticalScrollIndicator={false}
                                bounces={false}
                                ListHeaderComponent={this.renderCommentFooter}
                                onEndReached={this.handleLoadMoreComments}
                                onEndReachedThreshold={0.2}
                                contentContainerStyle={{
                                    flexGrow: 1,
                                }}
                                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                listKey={(x, i) => i.toString()}
                            />
                        </ScrollView>
                    </View>
                </Modal>
            </View>

        );
    }


    pickMultiple() {
        const { selectedimages } = this.state;
        ImagePicker.openPicker({
            multiple: true,
            waitAnimationEnd: false,
            includeExif: true,
            forceJpg: true,
        }).then(images => {
            this.setState({
                selectedimages: images.map(i => {

                    videosnotallow = i.mime.includes("video/mp4") || i.mime.includes("video/mp3") || i.mime.includes("mp4") || i.mime.includes("mp3") || i.mime.includes("video")
                    if (videosnotallow) {
                        this.setAttachmentPopupVisiblefromvideo(false)

                    } else {
                        return { uri: i.path, width: i.width, height: i.height, mime: i.mime, filename: i.filename };
                    }

                })

            });

        }).catch((e) => {
            if (e.message !== "User cancelled image selection") {
                if (Platform.OS === "ios") {
                    Alert.alert(
                        globals.appName,
                        e.message,
                        [{ text: 'OK', onPress: () => Linking.openSettings() },
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
                    );
                } else {
                    console.log(e.message ? "ERROR" + e.message : "ERROR" + e);
                }
            } else {
                console.log(TAG, e.message);
            }
        });
    }


    setAttachmentPopupVisiblefromvideo(visibility) {
        if (visibility == false) {
            Alert.alert(globals.appName, "Only images you can post", [{ text: 'OK', onPress: videosnotallow = '' }])
            this.setState({ attachmentPopup: visible, selectedimages: null, singleFile: '' })
        }

    }

    makePostApiCall() {

        const { selectedimages, singleFile, msgType } = this.state;
        this.props.showLoader()
        var arrMedia = [];
        if (singleFile) {
            arrMedia.push({
                name: 'Files-0',
                fileName: 'file.pdf',
                type: 'application/pdf',
                uri: singleFile.uri,
            })
        } else {
            if (selectedimages !== null && selectedimages !== undefined) {
                selectedimages ? selectedimages.map((i, index) =>
                    arrMedia.push({
                        name: 'Files-' + index,
                        fileName: 'file' + index + '.png',
                        type: 'image/png',
                        uri: i.uri,
                    })

                ) :
                    null
            }
        }
        const PostDetails = {
            HyperlinkURL: '',
            HyperlinkName: '',
            name: '',
            Description: this.state.msgType,
            UserID: JSON.parse(globals.userID),
            PlainTextDesc: '',
            CommunityId: this.state.DiccussionCommunityID
        };
        if (globals.isInternetConnected == true) {
            this.props.showLoader();
            this.setState({ loading: true, isInternetFlag: true, responseComes: false, loadingPagination: true, oncrossbottmView: true })
            API.saveCommunityDiscussion(this.saveCommunityDiscussionResponseData, { 'PostDetails': JSON.stringify(PostDetails) },
                arrMedia, true);
        } else {
            this.props.hideLoader();
            this.setState({ loading: false, isInternetFlag: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
           * render UploadPdfApiCall
           */
    UploadPostApiCall() {
        this.setState({ attachmentPopup: false, SearchKeywordDiscussion: '' })
        Keyboard.dismiss();
        this.props.showLoader();
        const { selectedimages, singleFile, msgType } = this.state;
        if (msgType === '' || msgType == null || msgType == undefined) {
            if (selectedimages == null || selectedimages == undefined || selectedimages == '') {
                if (singleFile == '') {
                    Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME);
                }
                else {
                    this.makePostApiCall()
                }
            }
            else {
                this.makePostApiCall()
            }
        }
        else {
            this.checkisvalidmsgtype(msgType)
        }
    }

    /**
        * check white space in starting only
        */
    checkisvalidmsgtype(msgType) {
        let regex = /^[^\s]+(\s+[^\s]+)*$/;
        if (!regex.test(msgType)) {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME,
                [{ text: 'OK', onPress: () => this.setState({ msgType: '' }) }])
        }
        else if (msgType.includes("http") || msgType.includes("https") || msgType.includes("www")) {
            if (Validation.validateURLWithText(msgType)) {
                this.makePostApiCall()
            }
            else {
                Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_LINK,
                    [{ text: 'OK', onPress: () => this.setState({ msgType: '' }) }])
            }
        }
        else {
            this.makePostApiCall()
        }

    }

    /**
        * Response callback of saveCommunityDiscussionResponseData
        */
    saveCommunityDiscussionResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveCommunityDiscussionResponseData -> success : ',
                JSON.stringify(response)
            );

            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({
                    msgType: '',
                    PageNumber: 1,
                })
                this.props.showLoader()
                this.makeApiCall()
            }
            else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false, msgType: '', selectedimages: null, singleFile: '' })
            }
            this.props.hideLoader();
            this.setState({ loading: false })
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'saveCommunityDiscussionResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                this.props.hideLoader();
                this.setState({ msgType: ' ', selectedimages: null, singleFile: '' })
                Alert.alert(globals.appName, err.Message, [{ text: 'OK', onPress: this.setState({ msgType: ' ', selectedimages: null, singleFile: '' }) }])
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
           * render selectPdfFile
           */
    async selectPdfFile() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log('URI : ' + res.uri);
            console.log('Type : ' + res.type);
            console.log('File Name : ' + res.name);
            console.log('File Size : ' + res.size);
            this.setState({ singleFile: res });
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log(TAG, "CANCEL :", err);
            } else {
                console.log(TAG, "ERROR :", err);
                throw err;
            }
        }
    }


    removeMultipleImgs(indexx) {
        this.state.selectedimages.splice((indexx - 1), 1)
        if (this.state.selectedimages.length !== 0) {
            this.setState({ selectedimages: this.state.selectedimages })
        } else {
            this.setState({ selectedimages: null })
        }
        this.forceUpdate()
    }


    removePdfs() {
        this.setState({ singleFile: '' })
    }





    /**
       * render AttachmentPopup
       */
    AttachmentPopup() {
        const { selectedimages, attachmentPopup, singleFile } = this.state;


        return (
            (attachmentPopup == true) ?
                <View style={[styles.ApmainContainer]}>

                    <AntDesign style={styles.attcrossicon}
                        name="closecircleo" size={globals.screenHeight * 0.03} color={colors.black} onPress={() => this.setAttachmentPopupVisible(false)}
                    />
                    <View style={styles.attContentStyle}>
                        {(this.state.singleFile == '') ?
                            <TouchableOpacity onPress={() => { this.pickMultiple() }}>
                                <Entypo style={styles.crossIconsStyle}
                                    name="images"
                                    size={globals.screenHeight * 0.077}
                                    style={{ marginLeft: (selectedimages) ? globals.screenWidth * 0.25 : null }}
                                    color={colors.warmBlue} />
                            </TouchableOpacity>
                            :
                            <View style={{ flexDirection: 'row', marginHorizontal: globals.screenWidth * 0.015 }}>
                                <Text numberOfLines={1} style={styles.pdftextSTyle}>{singleFile.name}</Text>
                                <Entypo style={styles.crossIconsStyle}
                                    name="circle-with-cross"
                                    size={globals.screenHeight * 0.02}
                                    color={colors.warmBlue} onPress={() => this.removePdfs()} />
                            </View>
                        }
                        {
                            (selectedimages) ? <ScrollView style={styles.scrollViewStyle}>
                                {selectedimages ? selectedimages.map((i, index) =>
                                    <View style={{ flexDirection: 'row', marginRight: globals.screenWidth * 0.04 }}>
                                        {

                                            (selectedimages == []) ?
                                                <TouchableOpacity onPress={() => { this.selectPdfFile() }}>
                                                    <Image resizeMode="contain" source={images.Discussion.pdf} style={styles.attrenderMimetypePdfimgView} />
                                                </TouchableOpacity>
                                                :
                                                <>
                                                    <Text numberOfLines={1} style={styles.SelectedimagesStyle}>
                                                        {

                                                            (Platform.OS === 'android') ?
                                                                "selectedImage" + index + ".png" : i.filename
                                                        }
                                                    </Text>
                                                    {

                                                        <Entypo style={styles.crossIconsStyle}
                                                            name="circle-with-cross" size={globals.screenHeight * 0.02} color={colors.warmBlue} onPress={() => this.removeMultipleImgs(index)} />
                                                    }
                                                </>
                                        }

                                    </View>
                                ) : null}
                            </ScrollView>
                                :
                                <TouchableOpacity onPress={() => { this.selectPdfFile() }}>
                                    <Image resizeMode="contain" source={images.Discussion.pdf} style={[styles.attrenderMimetypePdfimgView, { tintColor: colors.warmBlue }]} />
                                </TouchableOpacity>
                        }

                    </View>

                </View> :
                null

        )
    }



    render() {
        const { oncrossbottmView, isallowkeyboard, serverErr, SearchKeywordDiscussion, responseComes, loadingPagination, DiscussionPostData, loading, isInternetFlag } = this.state;





        return (

            <TouchableWithoutFeedback onPress={() => this._renderOptions2(null, null)}>
                {
                    <View style={styles.mainContainer}>
                        {this.renderSearchModel()}
                        {(Platform.OS == "ios") ? null : this.AttachmentPopup()}
                        {
                            (!isInternetFlag) ?
                                <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                                (serverErr === false) ?
                                    (
                                        (responseComes == false && loadingPagination == true) ?
                                            <View>
                                            </View> :
                                            (DiscussionPostData.length == 0 && responseComes) ?
                                                (DiscussionPostData.length == 0 && SearchKeywordDiscussion !== '') ?
                                                    <View style={globalStyles.nodataStyle}>
                                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DISCUSSION_POST_DATA_NOT_AVLB_SEARCH}</Text>
                                                    </View>
                                                    :
                                                    <View style={globalStyles.nodataStyle}>
                                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DISCUSSION_POST_DATA_NOT_AVLB}</Text>
                                                    </View>
                                                :
                                                <View style={[styles.mainContainer]}>

                                                    {/* <ScrollView scrollEnabled={this.state.enableScrollViewScroll} bounces={false} showsVerticalScrollIndicator={false}> */}
                                                    <FlatList
                                                        data={DiscussionPostData}
                                                        renderItem={({ item, index }) => this.renderItemView(item, index)}
                                                        keyExtractor={(index, item) => item.toString()}
                                                        extraData={this.state}
                                                        bounces={false}
                                                        ListFooterComponent={this.renderFooter}
                                                        onEndReached={this.handleLoadMore}
                                                        onEndReachedThreshold={0.2}
                                                        onMomentumScrollBegin={() => { this.onEndReachedPostCalledDuringMomentum = false; }}
                                                        style={{
                                                            marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?
                                                                globals.screenHeight * 0.012 : 0, marginBottom: (oncrossbottmView) ? globals.screenHeight * 0.11 : globals.screenHeight * 0.01
                                                        }}
                                                    />
                                                    {/* </ScrollView> */}
                                                </View>
                                    )
                                    :

                                    <View style={globalStyles.serverErrViewContainer}>
                                        <Text style={globalStyles.serverTextStyle}>
                                            {globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_PLZ_TRY_AGAIN}
                                        </Text>
                                        <TouchableOpacity style={globalStyles.serverButtonStyle} onPress={() => this._tryAgain()}>
                                            <CustomButton
                                                text={globals.BTNTEXT.LOGINSCREEN.CUSTOM_BTN_TEXT}
                                                backgroundColor={colors.bgColor}
                                                color={colors.white}
                                                loadingStatus={loading}
                                            />
                                        </TouchableOpacity>
                                    </View>


                        }
                        {(isallowkeyboard == true) ?

                            <KeyboardAvoidingView enabled behavior={(Platform.OS == "ios") ? "position" : null} keyboardVerticalOffset={(Platform.OS == "ios") ? globals.screenHeight * 0.09 : 0}>
                                {(Platform.OS == "ios") ? this.AttachmentPopup() : null}

                                <View style={[styles.sendBtnViewMainContainer, {
                                    height: globals.screenHeight * 0.12,
                                    position: (oncrossbottmView) ? 'absolute' : null,
                                    bottom: (oncrossbottmView) ? 0 : null,
                                }]} >

                                    <View style={styles.iconAndInputContainer}>

                                        <View style={[styles.inputViewContainer, { height: (Platform.OS == "android") ? globals.screenHeight * 0.065 : globals.screenHeight * 0.048 }]}>
                                            <TextInput
                                                style={styles.textInputStyleforBotton}
                                                placeholder={'Type Message...'}
                                                multiline={true}
                                                maxLength={1000}
                                                blurOnSubmit={true}
                                                onFocus={() => this.setState({ isallowkeyboard: true })}
                                                autoCapitalize="none"
                                                // numberOfLines = {10}
                                                showsVerticalScrollIndicator={false}
                                                returnKeyType="done"
                                                value={this.state.msgType}
                                                onChangeText={text => {
                                                    this.setState({
                                                        msgType: text,
                                                    });
                                                }} />
                                            <TouchableOpacity style={styles.attchmentView} onPress={() => this.setAttachmentPopupVisible(true)}>
                                                <Image source={images.Discussion.attchment}
                                                    style={styles.attchmentStyle}
                                                    resizeMode="contain" /></TouchableOpacity>
                                        </View>
                                        <View style={styles.sendIconViewContainer}>
                                            {
                                                <TouchableOpacity onPress={() => this.UploadPostApiCall()}>
                                                    <Image
                                                        source={images.Discussion.chatBtn}
                                                        style={styles.sendIconStyle}
                                                        resizeMode="contain"
                                                    />
                                                </TouchableOpacity>

                                            }

                                        </View>
                                    </View>

                                </View>
                            </KeyboardAvoidingView>
                            :
                            null
                        }

                    </View>


                }
            </TouchableWithoutFeedback>

        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(discussionPost);