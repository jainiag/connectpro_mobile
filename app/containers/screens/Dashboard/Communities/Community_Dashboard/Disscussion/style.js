import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../../utils/globals';
import * as colors from '../../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { screenWidth, screenHeight } from '../../../../../../utils/globals';

const iPad = DeviceInfo.getModel();
module.exports = StyleSheet.create({
    mainContainer: {
        flex: 1,
        //marginTop: globals.screenHeight * 0.061,
    },
    flexDirectionStyle: {
        flexDirection: 'row'
    },
    vwHeaderStyle: {
        marginTop: globals.screenHeight * 0.04,
        marginLeft: globals.screenWidth * 0.04,
        alignItems: 'center',
    },
    scrollFlatStyle: {
        flex: 1,
        //backgroundColor:'red',
        paddingTop: 10,
        // paddingBottom:20,
    },
    vwFlexDirectionRowStyle: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    api_buttonViewStyle: {
        flex: 0.1,
        padding: 1,
        marginTop: globals.screenHeight * 0.03,
        borderRadius: 8,
        marginHorizontal: globals.screenWidth * 0.05

    },
    api_paymentTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
        color: colors.lightGray,
        fontWeight: '300',
        marginLeft: globals.screenWidth * 0.015,
        textAlign: 'left',
    },
    api_paymentCommentTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
        color: colors.lightGray,
        fontWeight: '300',
        marginLeft: globals.screenWidth * 0.015,
        textAlign: 'center',
    },
    nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    nodataTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
    },
    mainItemView: {
        borderWidth: 0.7,
        borderColor: colors.proUnderline,
        marginHorizontal: globals.screenHeight * 0.018,
        marginTop: globals.screenHeight * 0.018,
        //  backgroundColor: 'red'
    },
    loaderWrapper: {
        flex: 1,
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 999,
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loaderInner: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    loaderbottomview: {
        bottom: (Platform.OS == 'android') ?
            globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
    },
    itemUpperCommentView: {
        //padding: globals.screenHeight * 0.01,
        flexDirection: 'row',
        // borderWidth: 0.7,
        // borderRadius: 20,
        marginVertical: globals.screenWidth * 0.035,
        marginHorizontal: globals.screenWidth * 0.015,
        // margin: globals.screenWidth * 0.035,
        // borderColor: colors.lightWhitecolor,
        // backgroundColor: colors.lightWhitecolor
    },
    itemUpperView: {
        padding: globals.screenHeight * 0.018,
        flexDirection: 'row',
        borderBottomWidth: 0.7,
        // backgroundColor: 'yellow',
        borderBottomColor: colors.proUnderline,
    },
    mainSecondView: {
        // paddingBottom: globals.screenHeight * 0.018,
        // paddingLeft: globals.screenHeight * 0.018,
        // paddingRight: globals.screenHeight * 0.018,
        paddingTop: globals.screenHeight * 0.012,
        //top:globals.screenHeight * 0.03
    },

    itemLowerViewimg: {
        // backgroundColor: colors.snowWhite,
        paddingVertical: globals.screenHeight * 0.018,
        paddingHorizontal: globals.screenWidth * 0.04
    },
    beforeImgView: {
        height: globals.screenHeight * 0.061,
        width: globals.screenHeight * 0.061,
        borderRadius: globals.screenHeight * 0.061 / 2,
        borderColor: colors.proUnderline,
        borderWidth: 0.5,
    },
    profileImageStyle: {
        height: globals.screenHeight * 0.061,
        width: globals.screenHeight * 0.061,
        borderRadius: globals.screenHeight * 0.061 / 2,
        borderColor: colors.proUnderline,
        borderWidth: 0.5,
    },
    textInputViewStyle: {
        // width:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.75 : globals.screenWidth * 0.65,
        marginLeft: globals.screenWidth * 0.05,
        flexDirection: 'row',
        paddingHorizontal: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.094 : globals.screenWidth * 0.028,
        backgroundColor: colors.discussionCommentColor,
    },
    textInputStyle: {
        paddingVertical: globals.screenWidth * 0.04,
        width: globals.screenWidth * 0.532,
        fontSize: globals.font_15,

    },
    textStyle: {
        color: 'gray',
        fontSize: globals.font_16,
        alignSelf: 'center',
        marginLeft: globals.screenWidth * 0.028,
    },
    mainCommentView: {
        paddingHorizontal: globals.screenHeight * 0.018,
        paddingVertical: globals.screenHeight * 0.018,
        flexDirection: "row"
    },
    commentListView: {
        flex: 1,
        padding: globals.screenWidth * 0.02,
        backgroundColor: colors.snowWhite,
        marginBottom: globals.screenWidth * 0.04,
        marginLeft: globals.screenWidth * 0.22,
        marginRight: globals.screenWidth * 0.05,
    },
    commentmainContainer: {
        flex: 1,
        marginVertical: globals.screenHeight * 0.02
    },
    innerTexts: {
        fontSize: globals.font_12,
        color: colors.lightGray,
    },
    serverErrViewContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    serverTextStyle: {
        textAlign: 'center',
        fontSize: globals.font_18,
        marginBottom: 15,
    },
    serverButtonStyle: {
        marginTop: 15,
    },
    dotView:
    {
        alignItems: 'flex-end', justifyContent: 'center', flex: 1,
        // marginBottom: globals.screenHeight * 0.015
    },
    WhiteView: {
        height: globals.screenHeight * 0.05,
        width: globals.screenWidth * 0.30,
        borderWidth: 0.5,
        // justifyContent:'space-evenly',
        padding: 5,
        borderColor: colors.lightGray,
    },

    touchableStyle: {
        margin: 8,
        paddingHorizontal: 5,
        paddingVertical: 1,
        borderColor: colors.gray,
        borderWidth: 0.5,
        borderRadius: 1.5,

    },
    ep_buttonStyle: {
        justifyContent: 'space-evenly',

    },
    mainbtnStyleview: {
        position: 'absolute',
        marginTop: (globals.iPhoneX) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.037,
        right: globals.screenWidth * 0.02,
    },
    commentDelbtns: {
        width: globals.screenWidth * 0.05,
        height: globals.screenWidth * 0.06,
        right: globals.screenWidth * 0.02,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.proUnderline,
    },
    delndownloadStyle: {
        width: globals.screenWidth * 0.20,
        height: globals.screenWidth * 0.10,
        right: globals.screenWidth * 0.02,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        backgroundColor: colors.proUnderline,
    },
    delStyle: {
        // tintColor:colors.redColor,
        margin: 2,
        // backgroundColor:'yellow',
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.025 : globals.screenHeight * 0.02,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.025 : globals.screenHeight * 0.02,
    },
    firstnamelastname: {
        fontSize: globals.font_14,
        color: colors.lightBlack
    },
    nameView: {
        alignSelf: 'center', paddingLeft: globals.screenWidth * 0.053
    },
    btntextStyless: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
        textAlign: 'center',
        color: colors.warmBlue,
    },
    commentVIews: {
        paddingBottom: globals.screenHeight * 0.012,
        borderBottomWidth: 0.7,
        // backgroundColor: 'yellow',
        borderBottomColor: colors.proUnderline,
    },
    commentVIewsText: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
        marginLeft: globals.screenWidth * 0.04,
        paddingTop: globals.screenWidth * 0.01,
        // paddingBottom:globals.screenWidth * 0.01,
        width: '70%',
    },
    touchableStyle: {
        paddingHorizontal: 22,
        paddingVertical: 5,
        borderColor: colors.gray,
        borderWidth: 1,
    },
    btntextStyless: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
        color: colors.white
    },
    htmltagStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
        paddingBottom: globals.screenWidth * 0.012,

    },
    txtStyles: {
        fontSize: globals.font_14,
        color: colors.lightBlack,
    },
    lineView: {
        height: 0.5, width: globals.screenWidth * 0.066,
    },
    scrollViewStyle: {
        backgroundColor: colors.grayBG,
        // height: globals.screenHeight * 0.15,
        // width:globals.screenWidth
        flex: 1
    },
    nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        // backgroundColor:'red'
    },
    nodataTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
    },

    sendBtnViewMainContainer: {
        backgroundColor: colors.grayBG,
        width: "100%",
        // position: 'absolute',
        // bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    linkInputViewContainers: {
        backgroundColor: colors.grayBG,
        width: "100%",
        height: globals.screenHeight * 0.3,
        position: 'absolute',
        bottom: 0,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    imgStyleViewContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    SelectedimagesStyle: {
        color: colors.warmBlue,
        marginLeft: globals.screenWidth * 0.04,
    },
    linkInputViewStyle1: {
        width: globals.screenWidth * 0.8,
        height: globals.screenHeight * 0.06,
        // backgroundColor: colors.white,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: colors.lightGray,
        justifyContent: 'center',
        marginTop: globals.screenHeight * 0.03
    },
    linkInputViewStyle2: {
        width: globals.screenWidth * 0.8,
        height: globals.screenHeight * 0.06,
        // backgroundColor: colors.white,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: colors.lightGray,
        justifyContent: 'center',
        marginTop: globals.screenHeight * 0.02,
        marginBottom: globals.screenHeight * 0.02,
    },
    linkIconAndSendIconContainer: {
        flexDirection: 'row', alignItems: 'center',
        marginTop: globals.screenHeight * 0.03,
        marginLeft: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.03 : null
    },
    linkIconViewStyle1: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        borderRadius: globals.screenWidth * 0.09,
        marginRight: globals.screenWidth * 0.02,
        marginLeft: globals.screenWidth * 0.04,
    },
    linkIconViewStyle2: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        borderRadius: globals.screenWidth * 0.09,
        marginRight: globals.screenWidth * 0.04,
        marginLeft: globals.screenWidth * 0.02,
    },
    iconAndInputContainer: {
        flexDirection: 'row', alignItems: 'center',
    },
    inputViewContainer: {
        width: globals.screenWidth * 0.7,
        height: globals.screenHeight * 0.05,
        backgroundColor: colors.white,
        flexDirection: 'row',
        borderWidth: 0.5,
        borderRadius: 20,
        borderColor: colors.lightWhite,
        justifyContent: 'center',
    },
    textInputStyleforBotton: {
        // height:globals.screenHeight * 0.02,
        width: globals.screenWidth * 0.6,
        textAlign: 'left',
        // backgroundColor:'red',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.font_11 : globals.font_9,
        marginLeft: globals.screenWidth * 0.03,
        marginRight: globals.screenWidth * 0.01,
        marginVertical: (Platform.OS == "android") ? 0 : globals.screenWidth * 0.014,
        // marginTop: globals.screenHeight * 0.006,
        // marginBottom: globals.screenHeight * 0.006,
    },
    sendIconViewContainer: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        borderRadius: globals.screenWidth * 0.09,
        marginLeft: globals.screenWidth * 0.04,
    },
    loadCommentmodalView: {
        flexDirection: 'row',
        marginLeft: globals.screenWidth * 0.055,
        alignItems: 'center',
        justifyContent: 'center'
    },
    attachIconViewContainer: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        borderRadius: globals.screenWidth * 0.09,
        marginRight: globals.screenWidth * 0.04,
    },
    sendIconStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
        borderRadius: globals.screenWidth * 0.09,
    },
    addFileViewContainer: {
        backgroundColor: colors.grayBG,
        width: "100%",
        // height: globals.screenHeight * 0.11,
        position: 'absolute',
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mainSecondViewforimage: {
        height: globals.screenHeight * 0.15,
        width: globals.screenWidth * 0.45,
        // flex: 1,
        paddingBottom: globals.screenHeight * 0.018,
        paddingLeft: globals.screenHeight * 0.018,
        paddingRight: globals.screenHeight * 0.018,
        paddingTop: globals.screenHeight * 0.038,
    },
    itemLowerView: {
        // backgroundColor: colors.snowWhite,
        // padding: globals.screenHeight * 0.018,
        padding: globals.screenHeight * 0.018,
        marginRight: globals.screenWidth * 0.013
    },
    imgStyles: {
        height: globals.screenHeight * 0.20,
        width: globals.screenWidth * 0.35,
    },
    itemLowerViewforimage: {
        backgroundColor: colors.snowWhite,
        padding: globals.screenHeight * 0.018,
        marginRight: globals.screenWidth * 0.013,
        flexDirection: 'row'
    },
    renderMimetypeImagemainView: {
        flex: 1,
        marginLeft: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : 0,
        marginBottom: globals.screenWidth * 0.015,

        // backgroundColor: colors.snowWhite,

    },
    renderMimetypeTextView: {
        // width: globals.screenWidth * 0.35,
        fontSize: globals.font_14,
        color: colors.lightBlack
    },
    renderMimetypePdfmainView: {
        height: globals.screenHeight * 0.15,
        width: globals.screenWidth * 0.4,
    },
    renderMimetypePdfimgView: {
        height: globals.screenHeight * 0.09,
        width: globals.screenWidth * 0.16,
        tintColor: colors.redColor,
        marginTop: globals.screenWidth * 0.015, marginHorizontal: globals.screenWidth * 0.013,
        // margin: globals.screenHeight * 0.025
    },
    p_tab_modalMainViewStyle: {
        backgroundColor: 'rgba(0,0,0,0.9)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    p_tab_modalIconContainer: {
        alignItems: 'center',
    },
    p_tab_modalInnerViewContainer: {
        height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: globals.screenWidth * 0.9,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.transparent,
        marginTop: globals.screenHeight * 0.03,
    },
    p_tab_modalPicStyle: {
        height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,
        width: globals.screenWidth * 0.9,
        borderRadius: 5,
    },
    commentsView: {
        flex: 1,
        //   backgroundColor:'yellow'
    },
    commentListingView: {
        flex: 1,
        // height: globals.screenHeight * 0.2,
        //  backgroundColor:'red'
    },
    likeimgStyle: {
        marginLeft: globals.screenWidth * 0.015,
        // marginBottom: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.028 : globals.screenWidth * 0.036,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.028 : globals.screenWidth * 0.036,
    },
    commentnView: {
        flexDirection: 'row', backgroundColor: colors.discussionCommentColor,
        flex: 1, alignItems: 'center',
        paddingVertical: globals.screenHeight * 0.013,
        paddingHorizontal: globals.screenWidth * 0.04,
    },
    innerLikeViews: {
        flexDirection: 'row', paddingRight: globals.screenWidth * 0.0533,
        alignItems: 'center', flex: 1, marginRight: globals.screenWidth * 0.65,
    },
    innercommentnview: {
        flexDirection: 'row', paddingRight: globals.screenWidth * 0.0533, alignItems: 'center'
    },
    likeTextstyle: {
        // width:globals.screenWidth * 0.25,
        fontSize: globals.font_14,
        color: colors.listSelectColor
    },
    likecountText: {
        fontSize: globals.font_14,
        color: colors.listSelectColor,
        marginLeft: globals.screenWidth * 0.015,
    },
    commentTextStyles: {
        fontSize: globals.font_14,
        color: colors.darkgrey,
        paddingRight: globals.screenWidth * 0.008
    },
    commentimgStyle: {
        marginLeft: globals.screenWidth * 0.015,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.025 : globals.screenHeight * 0.02,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.025 : globals.screenHeight * 0.02,
    },
    //////Comment Modal /////
    p_tab_CommentmodalMainViewStyle: {
        backgroundColor: 'rgba(0,0,0,0.9)',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    p_tab_CommentmodalIconContainer: {
        // alignItems: 'center',
    },
    p_tab_CommentmodalInnerViewContainer: {
        height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,

        width: globals.screenWidth * 0.9,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.white,
        marginTop: globals.screenHeight * 0.03,
    },
    p_tab_CommentmodalPicStyle: {
        height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,
        width: globals.screenWidth * 0.9,
        borderRadius: 5,
    },
    /////attchmentStyle
    attchmentStyle: {
        alignSelf: "center",
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.03 : globals.screenWidth * 0.04,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.03 : globals.screenWidth * 0.04,
        tintColor: colors.lightGray,
    },
    attchmentView: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    //// ApmainContainer
    ApmainContainer: {
        // flexDirection: 'row',
        height: globals.screenHeight * 0.11,
        marginHorizontal: globals.screenWidth * 0.045,
        width: globals.screenWidth,
        backgroundColor: colors.grayBG,
        borderWidth: 0.5,
        borderColor: colors.proUnderline,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        alignSelf: 'center',
        marginBottom: globals.screenHeight * 0.0945,
        position: 'absolute',
        zIndex: 9,
        bottom: 17,
    },
    attcrossicon: {
        alignSelf: 'flex-end',
        marginTop: -(globals.screenWidth * 0.02),
    },
    crossIconsStyle: {
        marginLeft: globals.screenWidth * 0.01,
        alignSelf: 'center'
    },
    attContentStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    attrenderMimetypePdfimgView: {
        height: globals.screenHeight * 0.08,
        width: globals.screenWidth * 0.12,
        tintColor: colors.redColor,
        alignSelf: 'center'
    },
    //Search discussion modal
    modelParentStyle: {
        flex: 1,
    },
    vwHeaderStyleSearch: {
        marginTop: globals.screenHeight * 0.08,
        marginLeft:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.07
                : globals.screenWidth * 0.07,
        alignItems: 'center',
        marginBottom: globals.screenHeight * 0.03,
    },
    tvSearchEventStyle: {
        color: colors.black,
        fontWeight: '500',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
        marginLeft:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.04
                : globals.screenWidth * 0.08,
    },
    vwFlexDirectionRowStyleSearch: {
        flexDirection: 'row',
    },
    toSearchStyle: {
        position: 'absolute',
        bottom: 0,
        height: globals.screenHeight * 0.0662, // 35
        backgroundColor: colors.bgColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: globals.screenWidth,
    },
    tvSearchStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    },
    textInputViewContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.05,
        borderRadius: 5,
    },
    textInputStyleContainer: {
        flex: 1,
        marginLeft: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        marginVertical: globals.screenHeight * 0.03,
    },
    pdftextSTyle: {
        textAlign: 'center',
        color: colors.warmBlue,
        fontSize: globals.font_15,
    },
    addCommentTextStyle: {
        alignSelf: 'center',
    }
    //----------------------
});
