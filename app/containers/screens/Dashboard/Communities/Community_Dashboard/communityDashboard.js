/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ImageBackground, Alert } from 'react-native';
import styles from './style';
import ImageLoad from 'react-native-image-placeholder';
import * as globals from '../../../../../utils/globals';
import * as images from '../../../../../assets/images/map';
import globalStyles from '../../../../../assets/styles/globleStyles';
import { API } from '../../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import JoinPopUp from '../../../../../components/JoinPopUp/index';
import loginScreen from '../../../../screens/AuthenticationScreens/loginScreen';
import { NavigationEvents } from 'react-navigation';

let _this = null;
let CommunityID;
const TAG = '==:== CommunityDashboard : ';

class CommunityDashboard extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft:
      globals.ConnectProbackButton(navigation, 'Community Dashboard'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });


  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      GridViewItems: [
        {
          key: 'About',
          icon: images.EventDashboard.aboutIcon,
        },
        {
          key: 'Discussions',
          icon: images.Communities.dicussion,
        },
        {
          key: 'Members',
          icon: images.Communities.memberss,
        },
        {
          key: 'Groups',
          icon: images.MemberDashboard.Groups,
        },
        {
          key: 'Events',
          icon: images.Communities.event,
        },
        {
          key: 'Resources',
          icon: images.EventDashboard.resources,
        },
        {
          key: 'Admins',
          icon: images.Communities.admins,
        },
        {
          key: '',
          icon: '',
        },
        {
          key: '',
          icon: '',
        },
      ],
      loading: false,
      dataglobalCommunityList: this.props.navigation.state.params.dataglobalCommunityList,
      communityData: [],
      communityInfo: [],
      userInfoData: [],
      serverErr: false,
      isRefresh: false,
      isCheck: false,
      modalVisibleforJoin: false,
      modalvisibleforTNC: false,
      joinClick: false
    }
  }

  static joinPopUpClose() {
    _this.setState({
      joinClick: false
    })
  }


  componentDidMount() {
    this.makeApiCall()
  }

  /**
   * API call of communityInfo
   */
  makeApiCall() {
    if (this.state.dataglobalCommunityList.activereview && this.state.dataglobalCommunityList.CommunityStatusID == 1) {
      this.props.hideLoader();
    } else if (this.state.dataglobalCommunityList.activereview == false && this.state.dataglobalCommunityList.CommunityStatusID == 2) {
      this.props.hideLoader();
    }
    else {
      this.props.showLoader();
      this.setState({ loading: true })
      const itisFrom = this.props.navigation.state.params.itisFrom
      CommunityID = (itisFrom == "deeplinkCommunity") ? this.props.navigation.state.params.dataglobalCommunityList.CommunityId : this.state.dataglobalCommunityList.CommunityID
      console.log("makeApiCall CommunityID-->",CommunityID)
      // CommunityID = this.state.dataglobalCommunityList.CommunityID;
      if (globals.isInternetConnected == true) {
        this.props.showLoader();
        API.communityInfo(this.communityInfoResponseData, true, CommunityID, globals.userID);
      } else {

        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    }

  }


  /**
          *  method for clearfileds
          */
  clearfileds() {
    this.setState({
      loading: false,
      dataglobalCommunityList: this.props.navigation.state.params.dataglobalCommunityList,
      itisFrom: this.props.navigation.state.params.itisFrom,
      communityData: [],
      communityInfo: [],
      userInfoData: [],
      serverErr: false,
      isRefresh: false,
      isCheck: false,
      modalVisibleforJoin: false,
      modalvisibleforTNC: false,
      joinClick: false
    })
  }

  /**
      * static method for clearStatesJoinPopup for communityDashboard
      */
  static clearStatesJoinPopup() {
    _this.setState({
      joinClick: false
    }, () => {
      _this.clearfileds();
      _this.makeApiCall()
    })

  }


  /**
      * Response callback of communityInfoResponseData
      */
  communityInfoResponseData = {
    success: response => {
      console.log(
        TAG,
        'communityInfoResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        const localData = response.Data
        if (localData == null || localData == '') {
          this.setState({ communityData: [] }, () => {
            setTimeout(() => {
              this.props.hideLoader()
            }, 1000);
          })
        }
        else {
          const communityInfoData = response.Data.CommunityInfo


          const UserInfoData = response.Data.UserInfo
          this.setState({ userInfoData: UserInfoData, communityData: localData, communityInfo: communityInfoData }, () => {
            setTimeout(() => {
              this.props.hideLoader()
            }, 1000);
          })
        }
      }
      else {
        this.setState({ loading: false, serverErr: true }, () => {
          setTimeout(() => {
            this.props.hideLoader()
          }, 1000);
        })
        Alert.alert(globals.appName, response.Message)
      }
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      this.setState({ serverErr: true });
      console.log(
        TAG,
        'communityInfoResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };

  /**
*  call when _sessionOnPres
*/
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  /**
   * Method of set visibility of Modal
   * @param {*} visible 
   */
  setModalVisible(visible) {
    this.setState({ modalVisibleforJoin: visible })
  }

  /**
  * Method of Checked accept btn
  * 
  */
  onChecked() {
    this.setState({ isCheck: true })
  }


  /**
 * method for Select Particular Item
 */
  setSelection(item, index) {
    const { GridViewItems, communityData, communityInfo, dataglobalCommunityList, userInfoData } = this.state;

    const UserStatus = userInfoData.CommunityUserStatus
    const dataGridViewItems = GridViewItems;
    this.setState({ joinClick: false });
    for (let i = 0; i < dataGridViewItems.length; i++) {
      if (index == i && item.key == 'About') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("AboutCommunity", { aboutCommunityData: communityData })
      } else if (index == i && item.key == 'Discussions') {
        dataGridViewItems[index].isSelected = true;
        if (this.state.dataglobalCommunityList.From == 'CommunityDashboard' && communityInfo.CommunityTypeName == 'Private') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        }
        else if (this.state.dataglobalCommunityList.itisFrom == 'Waiting_approval') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        } else {
          this.props.navigation.navigate("discussionPost", { DiccussionCommunityData: communityData, UserStatus: UserStatus, CommunityID: communityInfo.ID })
        }
      } else if (index == i && item.key == 'Members') {
        dataGridViewItems[index].isSelected = true;
        if (this.state.dataglobalCommunityList.From == 'CommunityDashboard' && communityInfo.CommunityTypeName == 'Private') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        }
        else if (this.state.dataglobalCommunityList.itisFrom == 'Waiting_approval') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        } else {
          this.props.navigation.navigate("CommunityMembers", { communityID: CommunityID })
        }
      } else if (index == i && item.key == 'Groups') {
        dataGridViewItems[index].isSelected = true;
        if (this.state.dataglobalCommunityList.From == 'CommunityDashboard' && communityInfo.CommunityTypeName == 'Private') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        }
        else if (this.state.dataglobalCommunityList.itisFrom == 'Waiting_approval') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        } else {
          this.props.navigation.navigate("CommunityGroups", { communityID: CommunityID })
        }
      } else if (index == i && item.key == 'Events') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("Events", { communityID: CommunityID })
      } else if (index == i && item.key == 'Resources') {
        dataGridViewItems[index].isSelected = true;
        if (this.state.dataglobalCommunityList.From == 'CommunityDashboard' && communityInfo.CommunityTypeName == 'Private') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        }
        else if (this.state.dataglobalCommunityList.itisFrom == 'Waiting_approval') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        } else {
          this.props.navigation.navigate("Resources", { communityID: CommunityID })
        }
      }

      else if (index == i && item.key == 'Admins') {
        dataGridViewItems[index].isSelected = true;
        if (this.state.dataglobalCommunityList.From == 'CommunityDashboard' && communityInfo.CommunityTypeName == 'Private') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        }
        else if (this.state.dataglobalCommunityList.itisFrom == 'Waiting_approval') {
          Alert.alert(globals.appName, "Join the community to view the community information")
        } else {
          this.props.navigation.navigate("Admins", { communityID: CommunityID })
        }
      } else {
        dataGridViewItems[i].isSelected = false;
      }
    }
    this.setState({ GridViewItems: dataGridViewItems }, () => {
    });
  }


  /**
  * Render FlatList Items
  */
  _renderItems(item, index) {
    return (
      <View style={styles.vsItemParentStyle}>
        <View style={[styles.gridViewStyle]}>
          <TouchableOpacity
            onPress={() => {
              this.setSelection(item, index);
            }}
          >
            <View style={styles.gridViewBlockStyle}>
              <Image source={item.icon} style={styles.imgStyle} resizeMode="contain" />
              <Text numberOfLines={1} style={styles.gridViewInsideTextItemStyle}>
                {' '}
                {item.key}{' '}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }



  setJoinMethod() {
    this.setState({ joinClick: true })
  }

  render() {
    const { GridViewItems, dataglobalCommunityList, communityData, serverErr, isFromProp, userInfoData, communityInfo, loading } = this.state;
    const isCommunityCategoryName = globals.checkObject(communityInfo, 'CommunityCategoryName');
    const isCommunityTypeName = globals.checkObject(communityInfo, 'CommunityTypeName');
    const isMembersCount = globals.checkObject(communityInfo, 'MembersCount')
    const isImagePath = globals.checkImageObject(communityInfo, 'ImagePath');
    const isLogoPath = globals.checkImageObject(communityInfo, 'LogoPath');
    const isCommunityUserStatus = globals.checkObject(userInfoData, 'CommunityUserStatus');
    const UserStatus = userInfoData.CommunityUserStatus

    return (
      <View style={{ flex: 1 }}>
         <NavigationEvents
          onWillFocus={() => this.makeApiCall()}
        />
        {

          (communityData.length == '' && dataglobalCommunityList.isFromProp == 'Review') ?
            <View style={styles.waiting_approvalViewContainer}>
              <Text style={styles.waiting_approvalTextStyle}>Community is under review by system admin. Please try again later.</Text>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}
                style={[styles.touchableStyle2, { marginLeft: globals.screenWidth * 0.02 }]}
              >
                <View>
                  <Text style={styles.btntextStyless}>BACK</Text>
                </View>
              </TouchableOpacity>
            </View>
            :
            (communityData.length == '' && dataglobalCommunityList.itisFrom == 'Inactive') ?
              // <View style={styles.waiting_approvalViewContainer}>
              <View style={styles.inactivetext}>
                <Text style={styles.waiting_approvalTextStyle}>Sorry! This Community is no longer available.</Text>
              </View>
              // <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}
              //   style={[styles.touchableStyle2, { marginLeft: globals.screenWidth * 0.02 }]}
              // >
              //   <View>
              //     <Text style={styles.btntextStyless}>View communities</Text>
              //   </View>
              // </TouchableOpacity>
              // </View> 
              :
              <View>
                {(this.state.joinClick == true) ?
                  <JoinPopUp
                    Communitystatus={communityInfo.CommunityTypeName}
                    CommunityCategoryName={communityInfo.Name}
                    communityID={communityInfo.ID}
                    TermsAndConditions={communityInfo.TermsAndConditions}
                    isOpen={this.state.joinClick}
                    isFrom={'CommunityDashboard'}
                  /> : null
                }
                <View style={styles.bannerView}>
                  <ImageLoad
                    style={styles.adImageStyle}
                    source={{ uri: (isImagePath) ? communityInfo.ImagePath : globals.Square_img }}
                    isShowActivity={false}
                    placeholderSource={images.EventDashboard.imagePlaceHolder}
                    placeholderStyle={styles.adImageStyle}
                  />
                </View>
                <View style={styles.headerParentStyle}>
                  <View style={styles.beforeImgViewStyle}>
                    <Image style={styles.ivEventImageStyle}
                      source={{ uri: (isLogoPath) ? communityInfo.LogoPath : globals.Event_img }} />
                  </View>
                  <View style={styles.eventInfoParentStyle}>
                    <Text numberOfLines={1} style={styles.tvLabelStyle}>
                      {(isCommunityCategoryName) ? communityInfo.Name : '-'}
                    </Text>
                    <Text style={[styles.tvEvenTypeStyle, { marginLeft: 0 }]}>
                      {(isMembersCount) ? (communityInfo.MembersCount <= '1') ?
                        (communityInfo.MembersCount) + " " + "Member" :
                        (communityInfo.MembersCount + " " + "Members") : ''}
                    </Text>
                    <Text style={[styles.tvDateTimeStyle, { marginLeft: globals.screenWidth * 0.001 }]}>
                      {(isCommunityTypeName) ? communityInfo.CommunityTypeName : '-'}
                    </Text>

                  </View>
                  {
                    (loading || UserStatus === 1 || UserStatus === 4 || UserStatus === 6 || UserStatus === 2) ?
                      null
                      :
                      <View>
                        <TouchableOpacity onPress={() => this.setJoinMethod()}>
                          <View style={styles.joinview}>
                            <Text style={styles.joinTextStyle}>{"Join"}</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                  }
                </View>
                <ImageBackground
                  source={images.InnerBg.InnerBg}
                  style={styles.ed_dashboardView}
                >
                  <FlatList
                    style={styles.fvStyle}
                    data={GridViewItems}
                    renderItem={({ item, index }) => this._renderItems(item, index)}
                    numColumns={3}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                  />
                </ImageBackground>
              </View>
        }

      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommunityDashboard);

