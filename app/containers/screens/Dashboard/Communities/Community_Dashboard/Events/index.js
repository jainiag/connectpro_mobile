import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ActivityIndicator, TextInput, Alert, Modal, TouchableWithoutFeedback, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import styles from './style';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import { API } from '../../../../../../utils/api';
import Icon from 'react-native-vector-icons/EvilIcons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../../../components/NoInternet/index'
import ServerError from '../../../../../../components/ServerError/index'
import KeyboardListener from 'react-native-keyboard-listener';

const iPad = DeviceInfo.getModel();
let TAG = "Events Screen ::==="
let _this = null;
let globalEventId;
class Events extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Events (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Events ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => { _this.setModalVisible(true); _this.setState({ eventName: '' }) }}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };


    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            shareurl: '',
            eventTitle: '',
            eventImg: '',
            isFreeStatus: true,
            allEventDetails: {},
            aboutEventsData: [],
            communityEvents: [],
            loading: false,
            isInternetFlag: true,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            eventName: '',
            countArr: [],
            totalCommunities: 0,
            communityID: this.props.navigation.state.params.communityID,
            keyboardOpen: false,
        }
    }

    componentDidMount() {
        this.props.showLoader()
        this.apicallGetEventsList()
    }

    /**
     * prepareData when call the API
     */
    prepareData() {
        const {
            pageNumber, pageSize, communityID, eventName
        } = this.state;

        const data = {};
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        data.CommunityID = communityID
        data.UserID = globals.userID
        data.EventName = eventName,
            data.StatusID = 0,
            data.TypeID = 0
        return data;
    }

    /**
     * APi call for get members list
     */
    apicallGetEventsList() {
        if (globals.isInternetConnected === true) {
            this.setState({ loadingPagination: true, loading: true, isInternetFlag: true })
            API.getCommunityEvents(this.GetCommunityEventsResponseData, this.prepareData(), true)
        } else {
            this.props.hideLoader();
            this.setState({ isInternetFlag: false })
        }
    }

    /**
 * Response of MyCommunity listing
 */
    GetCommunityEventsResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetCommunityEventsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                total = response.Data.TotalRecords;

                if (this.state.pageNumber == 1) {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        communityEvents: response.Data.EventPocoByCommunity,
                    })
                    for (let i = 0; i < this.state.communityEvents.length; i++) {
                        var totalcount = this.state.communityEvents[i]
                        if (totalcount.EventStatusID == 2) {
                            this.state.countArr.push(totalcount)
                        }
                    }
                    _this.props.navigation.setParams({
                        globalEventCount: this.state.countArr.length
                    });
                    this.setState({communityEvents:this.state.countArr})
                } else {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        communityEvents: [...this.state.communityEvents, ...response.Data.EventPocoByCommunity],
                    })

                }


            } else {
                Alert.alert(globals.appName, response.Message)
            }

            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetCommunityEventsResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
  *  call when _sessionOnPres
  */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.apicallGetEventsList();
        });

    }

    renderFooter = () => {
        if (this.state.loadingPagination && this.state.responseComes) {
            return (
                <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
            )
        } else {
            return (
                <View />
            )
        }
    };


    clearStates() {
        this.setState({

            shareurl: '',
            eventTitle: '',
            eventImg: '',
            isFreeStatus: true,
            allEventDetails: {},
            aboutEventsData: []
        })
    }


    apiGetEventDetails(EventID) {
        globalEventId = EventID;
        this.setState({ loading: true })
        AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
            globals.userID = token;
            if (globals.isInternetConnected === true) {
                API.getEventDetails(this.eventdetailsResponseData, true, EventID, globals.userID);
            } else {
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
            }
        });
    }


    /**
    * Response callback of eventDetails
    */
    eventdetailsResponseData = {
        success: response => {
            console.log(
                TAG,
                'eventDetailsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                const { aboutEventsData } = this.state
                const shareUrl = response.Data.EventDetails.EventURL;
                const eventTitle = response.Data.EventDetails.Title;
                const eventImg = response.Data.EventDetails.EventLogo;
                const isFreeStatus = response.Data.EventDetails.IsFree;
                const eventDetails = response.Data.EventDetails;

                this.setState({ shareurl: shareUrl, eventTitle: eventTitle, eventImg: eventImg, isFreeStatus: isFreeStatus, allEventDetails: eventDetails })
                var aboutevents = [];
                aboutevents = {
                    logo: response.Data.EventDetails.EventLogo,
                    text: response.Data.EventDetails.Description,
                    title: response.Data.EventDetails.Title,
                }
                aboutEventsData.push(aboutevents);
                this.setState({ aboutEventsData: aboutevents, loading: false })


                this.props.navigation.navigate("GLOBAL_EVENT_SCREEN_DASHBOARD", { selectedEventList: this.state.allEventDetails, dataEventsList: this.state.communityEvents, itisFrom: 'community_event' })
                this.clearStates()
            }
            else {
                this.setState({ loading: false })
                Alert.alert(globals.appName, response.Message)
            }

        },
        error: err => {
            console.log(
                TAG,
                'eventDetailsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(
                globals.appName,
                err.Message
            );
        },
        complete: () => {

            this.setState({ loading: false })
        },
    };

    /**
  * method for setSelectionUpcomingEvent Particular Item
  */
    setSelectionUpcomingEvent(item, index) {
        const { communityEvents } = this.state;
        const communityAdminDataList = communityEvents;
        for (let i = 0; i < communityAdminDataList.length; i++) {
            if (index == i) {
                communityAdminDataList[index].isSelected = true;
                this.apiGetEventDetails(item.EventID)
            } else {
                communityAdminDataList[i].isSelected = false;
            }
        }
        this.setState({ communityEvents: communityAdminDataList }, () => { });
    }


    /**
     * Method of render conditions according views in member lisr
     * @param {*} item 
     * @param {*} index 
     */
    renderCommunityEvents(item, index) {
        const { communityEvents, loading } = this.state;
        const isEventName = globals.checkObject(item, 'EventName');
        const isEventImage = globals.checkImageObject(item, 'EventImagePath');
        const isEventPrivate = globals.checkObject(item, 'IsPrivate');
        const isStartMonth = globals.checkObject(item, 'StartDateMonthStr');
        const isStartMonthDate = globals.checkObject(item, 'StartDateDayStr');
        const isEventType = globals.checkObject(item, 'EventType');
        const isEventStatusID = globals.checkObject(item, 'EventStatusID');
        const starttime = moment(item.StartDate).format('LT');
        const endtime = moment(item.EndDate).format('LT');
        const thDate = moment(item.StartDate).format('Do');
        const isPrivate = (isEventPrivate) ? (item.IsPrivate) : null;


        return (
            <>
                {
                    (isEventStatusID) ?
                        (item.EventStatusID == 2) ?

                            <TouchableWithoutFeedback style={styles.mainParentStyle} onPress={() => this.setSelectionUpcomingEvent(item, index)}>
                                <View style={[styles.vwItemParentPaddingStyle, { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white }]}>
                                    <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                        <View>
                                            {(isPrivate) ?
                                                <Image
                                                    source={images.Communities.lock}
                                                    style={styles.lockIconStyle}
                                                />
                                                :
                                                null
                                            }
                                            <View style={[styles.beforeimgView, { marginTop: 4 }]}>
                                                <Image
                                                    source={{ uri: isEventImage ? item.EventImagePath : globals.User_img }}
                                                    style={[styles.ivItemImageStyle]}
                                                />
                                            </View>
                                        </View>
                                        <View style={[styles.middleViewTexts]}>
                                            <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', color: item.isSelected == true ? colors.white : colors.lightBlack }]} numberOfLines={2}>{(isEventName) ? item.EventName : '-'}</Text>
                                            <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.lightBlack, marginTop: 2, }]}>{starttime + " - " + endtime}</Text>
                                            <Text style={[styles.attendeeTextStyle, { marginTop: 2, color: (item.EventType == false) ? colors.freeTextColor : colors.lightOrange }]}>{(isEventType) ? (item.EventType == false) ? 'Free' : 'Paid' : '-'}</Text>
                                        </View>

                                        <View style={[styles.vwItemDateStyle]}>
                                            <Text
                                                style={[
                                                    styles.tvDateStyle,
                                                    { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                                                ]}
                                            >
                                                {(isStartMonth) ? item.StartDateMonthStr : '-'}
                                            </Text>
                                            <View style={styles.vwDateSubViewStyle}>
                                                <Text
                                                    style={[
                                                        styles.tvLabelStyle,
                                                        { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                                                    ]}
                                                >
                                                    {(isStartMonthDate) ? item.StartDateDayStr : ''}
                                                </Text>
                                                <Text
                                                    style={[
                                                        styles.tvThStyle,
                                                        { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                                                    ]}
                                                >
                                                    {thDate.charAt(thDate.length - 2)}
                                                    {thDate.charAt(thDate.length - 1)}
                                                </Text>
                                            </View>
                                        </View>


                                    </View>
                                    <View style={[styles.horizontalSeprator]}></View>
                                </View>
                            </TouchableWithoutFeedback>

                            :
                            null :
                        null
                }

            </>
        )

    }

    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {

            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.apicallGetEventsList();
            })

            this.onEndReachedCalledDuringMomentum = true;

        }
    };


    onCrossApicall() {
        this.props.showLoader()
        this.setState({ eventName: '', responseComes: false, loadingPagination: true }, () => {
            this.apicallGetEventsList()
        })

    }

    /**
     * Search modal render function
     */
    renderSearchModel() {
        const { eventName } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <KeyboardListener
                        onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                        onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                    <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                        <TouchableOpacity onPress={() => { this.onCrossApicall(); this.setModalVisible(false); }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Events'}
                        </Text>
                    </View>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_EVENTS}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ eventName: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={eventName}
                                autoCapitalize="none"
                            />
                        </View>
                        {(Platform.OS == "android") ?
                            (this.state.keyboardOpen == false) ?
                                <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByEventName()}>
                                    <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                                </TouchableOpacity>
                                :
                                null
                            :
                            <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByEventName()}>
                                <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                            </TouchableOpacity>}
                    </View>
                </View>
            </Modal>
        );
    }

    /**
    * click on search based on community name
    */
    seacrhByEventName() {
        const { eventName } = this.state;
        this.setState({ eventName: eventName }, () => {
            this.setModalVisible(false);
            this.searchEvent();
        })
    }

    /**
    * Method of get  communities based on search filter
    */

    searchEvent() {
        _this.makeListFresh(() => {
            _this.props.showLoader();
            _this.apicallGetEventsList();
        });
    }

    makeListFresh(callback) {
        this.setState(
            {
                pageNumber: 1,
                pageSize: 10,
                communityEvents: [],
                responseComes: false
            },
            () => {
                callback();
            }
        );
    }



    /**
     * Method for change modal state
     * @param {*} visible 
     */
    setModalVisible(visible) {
        this.setState({ isShowSearch: visible });
    }



    render() {
        const { serverErr, loading, communityEvents, eventName, responseComes, loadingPagination, isInternetFlag } = this.state;

        return (
            <View style={styles.mainParentStyle}>
                {this.renderSearchModel()}
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (
                                (responseComes == false && loadingPagination == true) ?
                                    <View></View> :
                                    (communityEvents.length == 0 && responseComes) ?
                                        (communityEvents.length == 0 && eventName !== '') ?
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_EVENTS_NOT_AVLB_SEARCH}</Text>
                                            </View> :
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_EVENTS_NOT_AVLB}</Text>
                                            </View> :
                                        <View style={styles.mainParentStyle}>
                                            <FlatList
                                                style={{ flex: 1, }}
                                                showsVerticalScrollIndicator={false}
                                                data={communityEvents}
                                                renderItem={({ item, index }) => this.renderCommunityEvents(item, index)}
                                                extraData={this.state}
                                                bounces={false}
                                                keyExtractor={(index, item) => item.toString()}
                                                ListFooterComponent={this.renderFooter}
                                                onEndReached={this.handleLoadMore}
                                                onEndReachedThreshold={0.2}
                                                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                            />
                                        </View>
                            )
                            :
                            <ServerError loading={loading} onPress={() => this._tryAgain()} />
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Events);