import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../../utils/globals';
import * as colors from '../../../../../../assets/styles/color';


const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  mainRenderItemView:{
    height: globals.screenHeight * 0.11,  marginBottom: globals.screenHeight * 0.01, borderRadius: 3,flex:1
  },
  loaderbottomview: {
    bottom: (Platform.OS == 'android') ?
        globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
},
  horizontalItemView:{
     flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.015, flex: 1 
  },
  middleViewTexts:{
    justifyContent:'center', marginHorizontal:globals.screenHeight * 0.02,flex:5,
  },
  lastImgViewEnd:{
    justifyContent:'center', alignItems:'flex-end',
    marginRight:5
  },
  attendesFlatlist:{
    marginTop:globals.screenHeight*0.05,
    paddingHorizontal:globals.screenWidth*0.05
  },
  beforeimgView:{
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946)/2, 
    borderColor:colors.lightGray,
    borderWidth:0.5,
    marginLeft:5
  },
  ivItemImageStyle: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946)/2,
    marginTop:-1,
    marginLeft:-1, 
  },
  attendeeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12, 
    color:colors.lightBlack,
    
  },
  attende_shareIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.screenHeight*0.03 : globals.screenHeight*0.04 : globals.screenWidth * 0.06,
    width: (iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())  ? (Platform.OS == 'ios') ? globals.screenHeight*0.03  : globals.screenHeight*0.04: globals.screenWidth * 0.06,
  },
  horizontalSeprator:{
    borderColor: colors.proUnderline, borderWidth: 1, marginTop: globals.screenHeight * 0.01,
    marginHorizontal: -(globals.screenWidth * 0.0368)
  },
  vwSimpleLineStyle: {
    width: '90%',
    position: 'absolute',
    bottom: 0,
    height: 1,
    opacity: 0.5,
    marginHorizontal: globals.screenWidth * 0.0468, // 15
  },
  vwFlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  vwItemParentPaddingStyle: {
    marginHorizontal: 0,
    backgroundColor:colors.white
  },
  vwItemPaddingStyle: {
    marginHorizontal:  globals.screenWidth * 0.0368, // 15
    marginVertical:  globals.screenHeight * 0.0284, // 15
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tvThStyle: {
    fontSize: globals.font_10,
  },
  tvLabelStyle: {
    fontSize: globals.font_14,
  },
  vwItemDateStyle: {
    flex: 1,
    justifyContent:'center',
    alignItems: 'center',
    paddingBottom:3
  },
  tvDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },

  //----------modal UI
  modelParentStyle: {
    flex: 1,
  },
  vwHeaderStyle: {
    marginTop: globals.screenHeight * 0.08,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.07
        : globals.screenWidth * 0.07,
    alignItems: 'center',
    marginBottom: globals.screenHeight * 0.03,
  },
  tvSearchEventStyle: {
    color: colors.black,
    fontWeight: '500',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.08,
  },
  vwFlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  toSearchStyle: {
    position: 'absolute',
    bottom: 0,
    height: globals.screenHeight * 0.0662, // 35
    backgroundColor: colors.bgColor,
    alignItems: 'center',
    justifyContent: 'center',
    width: globals.screenWidth,
  },
  tvSearchStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
  },
  textInputViewContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.05,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginVertical: globals.screenHeight*0.03,
  },
  lockIconStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 :15, 
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 :15, 
    position: 'absolute', 
    zIndex: 10, 
    top: 0, 
    right: 5
  }
 
});