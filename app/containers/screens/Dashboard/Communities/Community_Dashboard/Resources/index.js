/* eslint-disable react/no-unused-state */
import React from 'react';
import { Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './style';
import * as colors from '../../../../../../assets/styles/color';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import PhotosTab from './photosTab';
import FilesTab from './filesTab';
import { ScrollableTabView, ScrollableTabBar } from '../../../../../../libs/react-native-scrollable-tabview'

const TAG = '==:== Resources : ';
let _this;


class Resources extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Resources'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    console.log(TAG,'Props ::', props);
    _this = this;

    this.state = {
      loading: true,
      communityID: this.props.navigation.state.params.communityID,

    };
  }

  render() {
    return <ScrollableTabView
    tabBarTextStyle={{ fontSize: globals.font_16 }}
    tabBarInactiveTextColor={colors.black}
    tabBarActiveTextColor={colors.warmBlue}
      style={styles.tabbarStyle}
      initialPage={0}
      renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
    >
      <PhotosTab tabLabel={'Photos'} navigationProps={this.props} communityId={this.state.communityID}/>
      <FilesTab tabLabel={'Files'} navigationProps={this.props} communityId={this.state.communityID}/>
    </ScrollableTabView>
  }
}

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Resources);
