import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, Modal, Alert } from 'react-native';
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { API } from '../../../../../../utils/api';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import Nointernet from '../../../../../../components/NoInternet/index'
import ServerError from '../../../../../../components/ServerError/index'
import ImageLoad from 'react-native-image-placeholder';

const TAG = '==:== PhotosTab : ';

class PhotosTab extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            loading: false,
            isInternetFlag: true,
            serverErr: false,
            responseComes: false,
            modalVisibleInvite: false,
            images: '',
            communityId: this.props.communityId,
            photosData:[],
        }
    }

    componentDidMount() {
        this.makeAPICall()
    }

    /**
     * method for get community photos api
     */
    makeAPICall() {
        if (globals.isInternetConnected === true) {
            this.props.showLoader()
            this.setState({ loading: true,isInternetFlag :true })
            API.getCommunityPhotos(this.GetCommunityPhotosResponseData, this.state.communityId, true)
        } else {
            this.setState({isInternetFlag : false})
        }
    }

    /**
*  call when _sessionOnPres
*/
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    /**
        * Response callback of communityInfoResponseData
        */
    GetCommunityPhotosResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetCommunityPhotosResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({ responseComes: true, loading: false })
                let res = JSON.parse(response.Data);
                 this.setState({photosData : res},()=>{
                    this.props.hideLoader()
                 })
            }
            else {
                this.setState({ loading: false, serverErr: true, responseComes: true })
                Alert.alert(globals.appName, response.Message);
                this.props.hideLoader()
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false, serverErr: true, responseComes: true })
            console.log(
                TAG,
                'GetCommunityPhotosResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            }else{
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };



    /**
     * method for handle status of modal visibility
     * @param {*} visible 
     */
    setModalVisible(visible) {
        this.setState({ modalVisibleInvite: visible })
    }

    _clsoeModal() {
        this.setModalVisible(false)
    }

    /**
     * render modal
     */
    imagePopView() {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.state.modalVisibleInvite}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}>
                <View style={styles.p_tab_modalMainViewStyle}>
                    <View style={styles.p_tab_modalIconContainer}>
                        <AntDesign
                            name="closecircleo" size={globals.screenHeight * 0.05} color={colors.white} onPress={() => this._clsoeModal()}
                        />
                    </View>
                    <View style={styles.p_tab_modalInnerViewContainer}>
                        <Image
                            source={{ uri: this.state.images }}
                            style={styles.p_tab_modalPicStyle}
                        />
                    </View>
                </View>
            </Modal>
        )
    }

    renderPhotos(item, index) {
        const isAttachmentUrl = globals.checkImageObject(item, 'AttachmentURL');

        return (
            <TouchableOpacity style={styles.p_tab_touchableStyle} onPress={() => {
                this.setState({ images: item.AttachmentURL }, () => {
                    this.setModalVisible(true)
                })
            }}>
                <ImageLoad
                    style={styles.p_tab_PicStyle}
                    source={{ uri: (isAttachmentUrl) ? item.AttachmentURL : globals.Square_img }}
                    isShowActivity={false}
                    placeholderSource={{ uri: globals.Square_img }}
                    placeholderStyle={styles.p_tab_PicStyle}
                />
            </TouchableOpacity>
        );
    }


    _tryAgain(){
        this.setState({ serverErr: false, responseComes:false },()=>{
            this.props.showLoader();
            this.makeAPICall();
        });
    }

    render() {
        const { serverErr, photosData,isInternetFlag, loading,responseComes } = this.state;
        return (
            <View style={styles.p_tab_MainContainer}>
                {this.imagePopView(this.state.photosData)}
                {
                    (!isInternetFlag) ?
                    <Nointernet loading={loading} onPress={() => this._tryAgain()} />:
                    (serverErr == false) ?
                        (photosData.length == 0 && responseComes) ?
                            <View style={globalStyles.nodataStyle}>
                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_RESOURCES_NOT_AVLB}</Text>
                            </View> :
                            <FlatList
                                style={[styles.p_tab_flatlistStyle]}
                                numColumns={2}
                                data={this.state.photosData}
                                renderItem={({ item, index }) => this.renderPhotos(item, index)}
                                keyExtractor={(x, i) => i.toString()}
                                extraData={this.state}
                                showsVerticalScrollIndicator={false}
                                bounces={false}
                            /> :
                            <ServerError loading={loading} onPress={() => this._tryAgain()} />
                }

            </View>
        );
    }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PhotosTab);
