import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../../utils/globals';
import * as colors from '../../../../../../assets/styles/color';


const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  bannerImageStyl: {
    height: globals.screenHeight * 0.2,
    width: '100%',
    opacity: 0.8,
    position: 'absolute',
    top: 0,
  },
  shareIconStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05,
    alignSelf: 'flex-end',
    marginRight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.04,
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.02 : globals.screenHeight * 0.017,
  },
  communityNameStyle: {
    textAlign: 'center',
    color: colors.white,
    fontSize: globals.font_15,
    fontWeight: '500',
    marginBottom: globals.screenHeight * 0.018,
    //   marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 5 : 0,
  },
  joinButtonStyle: {
    backgroundColor: colors.listSelectColor,
    height: globals.screenHeight * 0.04,
    width: globals.screenWidth * 0.18,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 3,
    marginBottom: globals.screenHeight * 0.014
  },
  waiting_apprivalBtnStyle: {
    backgroundColor: colors.listSelectColor,
    height: globals.screenHeight * 0.04,
    width: globals.screenWidth * 0.42,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 3,
    marginBottom: globals.screenHeight * 0.014,
    flexDirection: 'row',
  },
  acceptAndRejectBtnStyle: {},
  joinTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 19 : globals.font_11,
    color: colors.white,
    fontWeight: '500'
  },
  memberCountStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_13,
    color: colors.white,
    fontWeight: '500',
    textAlign: 'center'
  },
  inviteFriendButtonStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.warmBlue,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.055 : globals.screenHeight * 0.04,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.2 : globals.screenWidth * 0.3,
    borderRadius: 40,
  },
  inviteFriendTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_12,
    color: colors.white,
  },
  socialIconStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.08,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.08,
  },
  iconsContainerStyle: {
    flexDirection: 'row',
    position: 'absolute',
    top: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.17 : globals.screenHeight * 0.18,
    left: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.1 : globals.screenWidth * 0.08
  },
  eventSpotlightTextStyle: {
    fontSize: globals.font_16,
    color: colors.black,
    marginLeft: globals.screenWidth * 0.08,
  },
  aboutCommunityTitle: {
    fontSize: globals.font_16,
    color: colors.black,
    // marginTop: globals.screenHeight * 0.02,
    marginLeft: globals.screenWidth * 0.08,
  },
  descriptionViewStyle: {
    marginLeft: globals.screenWidth * 0.08,
  },
  renderItemMainViewStyle: {
    marginHorizontal: globals.screenWidth * 0.04,
    marginTop: globals.screenHeight * 0.02,
    marginBottom: globals.screenHeight * 0.02
  },
  image_flatListStyle: {
    height: globals.screenHeight * 0.2,
    width: globals.screenWidth * 0.7,
    borderRadius: 10,
    opacity: 0.8,
  },
  eventNameStyle: {
    textAlign: 'center',
    color: colors.white,
    fontSize: globals.font_14,
    fontWeight: '600',
    position: 'absolute',
    right: 0,
    left: 0,
    top: globals.screenHeight * 0.06,
  },
  dateTextStyle: {
    fontSize: globals.font_13,
    color: colors.white,
    fontWeight: '600',
    textAlign: 'center',
  },
  thDateStyle: {
    fontSize: globals.font_10,
    paddingBottom: 8,
    fontSize: globals.font_13,
    color: colors.white,
    fontWeight: '600',
    textAlign: 'center',
  },
  timeTextStyle: {
    position: 'absolute',
    marginTop: globals.screenHeight * 0.025,
    fontSize: globals.font_13,
    color: colors.white,
    fontWeight: '600',
    textAlign: 'center',
    left: 0,
    right: 0,
    top: globals.screenHeight * 0.1
  },
  textViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'absolute',
    left: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.22 : globals.screenWidth * 0.17,
    top: globals.screenHeight * 0.1
  },
  notDataAvalbleStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notDataAvalbleTextStyle: {
    fontSize: globals.font_14,
    color: colors.black,
    textAlign: 'center',
    marginTop: globals.screenHeight * 0.03
  },
  activityStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  descriptionStyle: {
    fontSize: globals.font_14,
    color: colors.lightBlack,
  },

  // =======: invite Member Modal :=======

  headertextStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_14,
  },
  modalMainView: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor: colors.white,
    marginTop: globals.screenHeight * 0.2,
    borderRadius: 4,
    borderWidth: 4,
    borderColor: colors.gray
  },
  modalInnerMainView: {
    backgroundColor: colors.white,
    margin: globals.screenWidth * 0.03,
    width: globals.screenWidth * 0.85,
    borderRadius: 4,
    flex: 1,
  },
  modalAlreadyRegisteredView: {
    flexDirection: 'row',
    marginHorizontal: globals.screenWidth * 0.04,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.02,
  },
  modalResendTextView: {
    flex: 1,
    marginRight: 10
  },
  modalCancelImg: {
    height: globals.screenHeight * 0.019,
    width: globals.screenHeight * 0.019,
    tintColor: colors.gray
  },
  modalHorizontalLine: {
    height: 2,
    backgroundColor: colors.proUnderline,
    width: '100%',
    marginTop: (globals.screenHeight * 0.025),
    marginBottom: (globals.screenHeight * 0.025)
  },
  modalTextInputView: {
    borderColor: colors.gray,
    marginHorizontal: 10,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
  },
  modalTextInputView2: {
    borderColor: colors.gray,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderRightWidth: 1,
    marginHorizontal: 10,
    height: globals.screenHeight * 0.08,
  },
  modalSubmitView: {
    justifyContent: 'center',
    backgroundColor: colors.listSelectColor,
    width: globals.screenWidth * 0.22,
    alignItems: 'center',
    height: globals.screenHeight * 0.05,
    right: 0, position: 'absolute',
    bottom: (Platform.OS == 'android') ? (globals.screenHeight * 0.01) : (globals.screenHeight * 0.03),
    marginRight: globals.screenHeight * 0.01,
    borderRadius: 3
  },
  modalSubmitText: {
    color: colors.white,
    fontSize: globals.font_13,
    fontWeight: '600'
  },
  mainContainer: {
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.02,
    marginBottom: globals.screenHeight * 0.03,
    flex: 1
  },
  textInputStyle: {
    paddingBottom: globals.screenHeight * 0.015,
    marginTop: globals.screenHeight * 0.015,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    color: colors.black,
    marginLeft: globals.screenWidth * 0.026,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
  },
  noteTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_12,
    color: colors.black,
    marginHorizontal: globals.screenWidth * 0.026,
    marginTop: globals.screenHeight * 0.02
  },
  inviteTextStyle: {
    fontSize: globals.font_16,
  },
  crossIconStyle: {
    alignItems: 'flex-end'
  },
  scrollViewStyle: {
    marginLeft: globals.screenWidth * 0.01,
  },
  waiting_reviewViewStyle: {
    height: globals.screenHeight * 0.2,
    width: globals.screenWidth * 0.8,
    backgroundColor: colors.gray,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: globals.screenHeight * 0.2, 
  },
  community_Under_ReviewTextStyle: {
    fontSize: globals.font_14,
    fontWeight: '500',
  },
  back_BtnStyle: {
    backgroundColor: colors.darkSkyBlue,
    height: globals.screenHeight * 0.04,
    width: globals.screenWidth * 0.26,
    alignItems: 'center',
    justifyContent: 'center'
  },
  acceptAndRejectBtnViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  memberIconViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    alignSelf: 'center',
  },
  memberIconStyle: {
    height: globals.screenWidth * 0.05,
    width: globals.screenWidth * 0.05,
    marginRight: globals.screenWidth * 0.02,
  },
  eventSpotlight_ActivityIndicator: {
    height: globals.screenHeight * 0.2,
    width: globals.screenWidth * 0.7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    alignSelf: 'center'
  },
  waiting_approvalViewContainer: {
    height: (Platform.OS == "ios") ? globals.screenHeight * 0.14 : globals.screenHeight * 0.14,
    width: (Platform.OS == "ios") ? globals.screenWidth * 0.8 : globals.screenWidth * 0.9,
    backgroundColor: colors.gray,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  waiting_approvalIconStyle: {
    height: globals.screenWidth * 0.09,
    width: globals.screenWidth * 0.09,
    marginBottom: globals.screenHeight * 0.01,
    tintColor: colors.redColor
  }
});