import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ActivityIndicator, Modal, TextInput, Alert, Linking, ScrollView, Platform } from 'react-native';
import Share from 'react-native-share';
import styles from './style';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import HTML from 'react-native-render-html';
import { API } from '../../../../../../utils/api';
import moment from 'moment';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ImageLoad from 'react-native-image-placeholder';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import Validation from '../../../../../../utils/validation';
import { TagSelect } from '../../../../../../libs/react-native-tag-select';
import tagStyle from '../../../../Profile/userProfileTabs/style';
import Ionicons from 'react-native-vector-icons/Ionicons';
import JoinPopUp from '../../../../../../components/JoinPopUp/index'

let _this;
const iPad = DeviceInfo.getModel();
let TAG = "AboutCommunity Screen ::==="

class AboutCommunity extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'About Community'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });


    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            CommunityInfoData: this.props.navigation.state.params.aboutCommunityData.CommunityInfo,
            UserInfoData: this.props.navigation.state.params.aboutCommunityData.UserInfo,
            loading: false,
            emailAdd: false,
            spotLightData: [],
            emailID: '',
            emailsData: [],
            fixData: [],
            modalVisibleInvite: false,
            isInvite: false,
            joinClick: false,
        }
    }

    static joinPopUpClose() {
        _this.setState({
            joinClick: false
        })
    }

    componentDidMount() {
        this.setState({ loading: true })
        this.makeApiCall()
        let CommunityID = this.state.CommunityInfoData.ID;
        if (globals.isInternetConnected == true) {
            API.getSpotLightEvents(this.spotLightEventsResponseData, true, CommunityID);
        } else {
            this.setState({ loading: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }



    /**
       * API call of communityInfo
       */
    makeApiCall() {


        let CommunityID = this.state.CommunityInfoData.ID;
        if (globals.isInternetConnected == true) {
            API.communityInfo(this.communityInfoResponseData, true, CommunityID, globals.userID);
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
          *  method for clearfileds
          */
    clearfileds() {
        this.setState({
            CommunityInfoData: this.props.navigation.state.params.aboutCommunityData.CommunityInfo,
            UserInfoData: this.props.navigation.state.params.aboutCommunityData.UserInfo,
            loading: false,
            spotLightData: [],
            emailID: '',
            emailsData: [],
            fixData: [],
            modalVisibleInvite: false,
            isInvite: false,
            joinClick: false,
        })
    }


    /**
      * static method for clearStatesJoinPopup for communityDashboard
      */
    static clearStatesJoinPopup() {
        _this.clearfileds();
        _this.makeApiCall()
    }


    /**
        * Response callback of communityInfoResponseData
        */
    communityInfoResponseData = {
        success: response => {
            console.log(
                TAG,
                'communityInfoResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({
                    CommunityInfoData: response.Data.CommunityInfo,
                    UserInfoData: response.Data.UserInfo
                })
            }
            else {
                this.setState({ loading: false, serverErr: true })
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            this.setState({ serverErr: true });
            console.log(
                TAG,
                'communityInfoResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            Alert.alert(globals.appName, err.Message);
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };




    /**
    * Response callback of spotLightEventsResponseData
    */
    spotLightEventsResponseData = {
        success: response => {
            console.log(
                TAG,
                'spotLightEventsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                const localData = response.Data
                this.setState({ spotLightData: localData })
            }
            else {
                this.setState({ loading: false })
            }
            this.setState({ loading: false })
        },
        error: err => {
            this.setState({ loading: false })
            console.log(
                TAG,
                'spotLightEventsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
            this.setState({ loading: false })
        },
    };


    /**
     * click on invite Member button with api call
     */

    _inviteMember() {
        if (this.state.emailID !== "" || this.state.emailsData.length > 0) {
            if (Validation.isValidEmail(this.state.emailID) || this.state.emailsData.length > 0) {
                if (this.state.emailsData.length > 0 || (this.state.emailID !== "")) {
                    this.setState({ emailAdd: false })
                    this._addEmail();
                    this.setModalVisible(false);
                } else {
                    console.log(TAG, "Invite member api not called ");
                }
                let communityId = this.state.CommunityInfoData.ID;;
                let communityName = this.state.CommunityInfoData.Name;

                const data =
                {
                    "memberEmailIds": 
                        this.state.emailsData
                    ,
                    "communityId": communityId,
                    "communityName": communityName,
                    "userId": globals.userID
                }
                if (this.state.emailsData.length > 0) {
                    this.setState({ isInvite: true })
                    if (globals.isInternetConnected === true) {
                        API.inviteMembers(this.InviteMembersResponseData, data, true)
                        this.clearEmailsData();
                    } else {
                        Alert.alert(globals.appName, "Invalid Email Address");
                    }
                } else {
                    Alert.alert(globals.appName, "Enter at least one Email ID");
                }
            }
        }
    }

    /**
     * clearAll email ID when inviteButton click
     */

    clearEmailsData() {
        this.setState({ emailsData: [], emailID: '', emailAdd: false });
    }

    /**
    * Response callback of SavePortfolio ResponseData
    */
    InviteMembersResponseData = {
        success: response => {
            console.log(
                TAG,
                'InviteMembersResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                Alert.alert(globals.appName, response.Message,
                    [{ text: 'OK', onPress: () => this.setModalVisible(false) }],
                    { cancelable: false });
            } else {
                Alert.alert(
                    globals.appName,
                    response.Message,
                    [{ text: 'OK' }],
                    { cancelable: false }
                );
                this.setState({ isInvite: false })
            }
            this.setState({ isInvite: false })
        },
        error: err => {
            console.log(
                TAG,
                'InviteMembersResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
            this.setState({ isInvite: false });
        },
        complete: () => {
            this.setState({ isInvite: false })
        },
    };

    /**
     * Method of set visibility of Modal
     * @param {*} visible 
     */
    setModalVisible(visible) {
        this.setState({ modalVisibleInvite: visible })
        this.setState({ joinClick: false })
    }

    /*
    * this method for facebook open in default browser
    */
    _opneFacebook = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log(`Don't know how to open URL`);
            }
        });
    };

    /*
    * this method for Linkedin open in default browser
    */
    _opneLinkedin = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log(`Don't know how to open URL`);
            }
        });
    };


    /*
     * this method for Twitter open in default browser
     */
    _opneTwitter = (url) => {
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                console.log(`Don't know how to open URL`);
            }
        });
    };

    _addEmail() {
        const { emailsData, emailID } = this.state;
        let tempArr = emailsData;
        if (Validation.isValidEmail(emailID)) {
            this.setState({ emailAdd: true })
            tempArr.push(emailID);
            this.setState({ emailsData: tempArr, emailID: '' });
            this.textInput.clear();
        } else {
            this.setState({ loading: false });
        }
        this._fixData()
    }

    onSubmitt() {
        this.setState({ emailID: '' })
    }

    _clsoeModal() {
        this.setModalVisible(false)
        this.clearEmailsData();
    }

    _fixData() {
        if (this.state.emailsData.length <= 10) {
            this.setState({ fixData: this.state.emailsData })
        }
    }

    _setJoinMethod() {
        this.setState({ joinClick: true })
    }

    /**
    * onEmailChangeText method for onChangeText
    */
    onEmailChangeText(text) {
        let tempText = text;
        let finalValue;
        if (tempText.charAt(tempText.length - 1) == ',') {
            finalValue = tempText.substr(0, tempText.length - 1);
            if (Validation.isValidEmail(finalValue)) {
                this.setState({ emailID: finalValue }, () => {
                    this._addEmail();
                })
            } else {
                Alert.alert(globals.appName, "Invalid Email Address")
            }
        } else {
            tempText = text
            this.setState({ emailID: tempText })
        }

    }

    /**
    * Method of Render modal of invite member email
    */
    _renderInviteEmailPopup() {
        const { emailID, CommunityInfoData, UserInfoData, emailsData, emailAdd } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisibleInvite}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}>
                <View style={styles.modalMainView}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.modalAlreadyRegisteredView}>
                            <View style={styles.modalResendTextView}>
                                <Text style={styles.inviteTextStyle}>Invite Friends</Text>
                            </View>
                            <TouchableOpacity style={styles.crossIconStyle} onPress={() => this._clsoeModal()}>
                                <Image source={images.drawerMenu.menuClose} style={styles.modalCancelImg} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.modalHorizontalLine}></View>
                        {(emailAdd == true) ?
                            <View style={styles.modalTextInputView2}>
                                <ScrollView bounces={false} style={styles.scrollViewStyle} showsVerticalScrollIndicator={false}>
                                    <TagSelect
                                        isFrom="ProfileCategory"
                                        data={(this.state.emailsData.length <= 10) ? this.state.emailsData : this.state.fixData}
                                        itemStyle={tagStyle.item}
                                        itemLabelStyle={tagStyle.label}
                                        itemStyleSelected={tagStyle.itemSelected}
                                        itemLabelStyleSelected={tagStyle.labelSelected}
                                    />
                                </ScrollView>
                            </View>
                            :
                            null
                        }
                        <View style={[styles.modalTextInputView, { borderTopWidth: (emailAdd == false) ? 1 : null }]}>
                            <TextInput
                                value={`${emailID}`}
                                ref={input => { this.textInput = input }}
                                autoCapitalize="none"
                                blurOnSubmit={(this.state.emailID == '') ? true : false}
                                returnKeyType="done"
                                onChangeText={text => this.onEmailChangeText(text)}
                                placeholderTextColor={colors.gray}
                                placeholder={"Enter Email ID"}
                                style={styles.textInputStyle}
                                onSubmitEditing={() => (this.state.emailID == '') ? this.onSubmitt() : this._addEmail()}
                            />
                        </View>
                        <Text style={styles.noteTextStyle}>{globals.MESSAGE.COMMUNITIES.NOTE_TEXT}</Text>
                        <View style={styles.modalHorizontalLine}></View>
                        {(this.state.isInvite == true) ?
                            <TouchableOpacity style={styles.modalSubmitView}>
                                <ActivityIndicator size='small' color={colors.white} style={styles.activityStyle} />
                            </TouchableOpacity>
                            :
                            <TouchableOpacity style={styles.modalSubmitView} onPress={() => this._inviteMember()}>
                                <Text style={styles.modalSubmitText}>Invite</Text>
                            </TouchableOpacity>
                        }
                    </View>

                </View>
            </Modal>
        )
    }

    renderItemList(item, index) {
        const isImageName = globals.checkObject(item, 'ImageName');
        const isEventName = globals.checkObject(item, 'EventName');
        const isStartDate = globals.checkObject(item, 'EventStartDate');
        const isEndDate = globals.checkObject(item, 'EventEndDate');
        const starttime = (isStartDate) ? moment(item.EventStartDate).format('LT') : '-';
        const endtime = (isEndDate) ? moment(item.EventEndDate).format('LT') : '-';
        const month = (isStartDate) ? moment(item.EventStartDate).format('MMM') : '-';
        const thDate = (isStartDate) ? moment(item.EventStartDate).format('Do') : '-';
        const onlyDt = (isStartDate) ? moment(item.EventStartDate).format('D') : '-';
        const onlyDt2 = (isEndDate) ? moment(item.EventEndDate).format('D') : '-';
        const year = (isStartDate) ? moment(item.EventStartDate).format('YYYY') : '-';
        const day2 = thDate.charAt(thDate.length - 2);
        const day = thDate.charAt(thDate.length - 1);

        return (
            <View style={styles.renderItemMainViewStyle}>
                {/* <Image
                    source={{ uri: (isImageName) ? item.ImageName : 'https://via.placeholder.com/150' }}
                    style={styles.image_flatListStyle}
                /> */}
                <ImageLoad
                    style={styles.image_flatListStyle}
                    source={{ uri: (isImageName) ? item.ImageName : globals.DarkSquare_img }}
                    isShowActivity={false}
                    placeholderSource={{ uri: globals.DarkSquare_img }}
                    placeholderStyle={styles.image_flatListStyle}
                />
                <Text style={styles.eventNameStyle}>{(isEventName) ? item.EventName : '-'}</Text>
                <View style={styles.textViewContainer}>
                    <Text style={styles.dateTextStyle}>{month} </Text>
                    <Text style={styles.dateTextStyle}>{onlyDt}</Text><Text style={styles.thDateStyle}>{day2}{day}</Text>
                    <Text style={styles.dateTextStyle}> - </Text>
                    <Text style={styles.dateTextStyle}>{onlyDt2}</Text><Text style={styles.thDateStyle}>{day2}{day}</Text>
                    <Text style={styles.dateTextStyle}>, {year}</Text>
                </View>
                <Text style={styles.timeTextStyle}>{`${starttime} - ${endtime}`}</Text>
            </View>
        )
    }

    /**
   * Event share method
   */
    _shareCommunity = () => {
        const { CommunityInfoData } = this.state;
        let isCommunityUrl = globals.checkObject(CommunityInfoData, 'CommunityURL');
        let isCommunityName = globals.checkObject(CommunityInfoData, 'Name');
        if (isCommunityUrl) {
            if (CommunityInfoData.CommunityURL != null && CommunityInfoData.CommunityURL != undefined && CommunityInfoData.CommunityURL != "") {
                const shareOptions = {
                    message: "community:" + ((isCommunityName) ? CommunityInfoData.Name : " - ") + "\n" +
                        "Link to open: " + CommunityInfoData.CommunityURL
                };

                Share.open(shareOptions)
                    .catch(err => console.log(err));
            } else {
                Alert.alert(globals.appName, "something went wrong")
            }
        } else {
            Alert.alert(globals.appName, "something went wrong")
        }

    };

    render() {
        const { CommunityInfoData, UserInfoData, loading, spotLightData } = this.state;
        const isImagePath = globals.checkImageObject(CommunityInfoData, 'ImagePath');
        const isMembersCount = globals.checkObject(CommunityInfoData, 'MembersCount');
        const isCommunityName = globals.checkObject(CommunityInfoData, 'Name');
        const isDescription = globals.checkObject(CommunityInfoData, 'Description');
        const isCommunityUserStatus = globals.checkObject(UserInfoData, 'CommunityUserStatus')
        const isCheckCommunityUserRole = globals.checkObject(UserInfoData, 'CommunityUserRoleID')
        const CommunityUserRoleID = (isCheckCommunityUserRole) ? (UserInfoData.CommunityUserRoleID) : '-';
        const CommunityUserStatus = (isCommunityUserStatus) ? (UserInfoData.CommunityUserStatus) : '-';
        const isactive = globals.checkObject(CommunityInfoData, 'IsActive');
        const isActive = (isactive) ? CommunityInfoData.IsActive : null;
        const isCommunityStatusID = globals.checkObject(UserInfoData, 'CommunityStatusID');
        const isCommunityTypeID = globals.checkObject(CommunityInfoData, 'CommunityTypeID');
        const CommunityStatusID = (isCommunityStatusID) ? UserInfoData.CommunityStatusID : '-';
        const inviteCheck = (isActive || CommunityStatusID == 1) && (CommunityUserStatus == 2);
        const isfbUrl = globals.checkObject(CommunityInfoData, "FacebookUrl")
        const istwitterUrl = globals.checkObject(CommunityInfoData, "TwitterUrl")
        const islinkedinUrl = globals.checkObject(CommunityInfoData, "LinkedInUrl")
        const facebookUrl = (isfbUrl) ? CommunityInfoData.FacebookUrl : '-';
        const twitterUrl = (istwitterUrl) ? CommunityInfoData.TwitterUrl : '-';
        const linkedInUrl = (islinkedinUrl) ? CommunityInfoData.LinkedInUrl : '-';
        return (
            <View style={styles.mainParentStyle}>
                {
                    (isActive && CommunityStatusID === 1) ?
                        <View style={styles.waiting_reviewViewStyle}>
                            <Text style={styles.community_Under_ReviewTextStyle}>COMMUNITY IS UNDER REVIEW BY SYSTEM ADMIN. PLEASE TRY AGAIN LATER.</Text>
                            <TouchableOpacity style={styles.back_BtnStyle} onPress={() => this.props.navigation.goBack()}>
                                <Text style={[styles.community_Under_ReviewTextStyle, { color: colors.white }]}>BACK</Text>
                            </TouchableOpacity>
                        </View>

                        :
                        <View style={styles.mainParentStyle} pointerEvents="box-none">

                            {(this.state.joinClick == true) ?
                                <JoinPopUp
                                    Communitystatus={CommunityInfoData.CommunityTypeName}
                                    CommunityCategoryName={CommunityInfoData.CommunityCategoryName}
                                    communityID={CommunityInfoData.ID}
                                    TermsAndConditions={CommunityInfoData.TermsAndConditions}
                                    isOpen={this.state.joinClick}
                                    isFrom={'AboutCommunity'}
                                /> : null
                            }
                            {
                                this._renderInviteEmailPopup()
                            }
                            <View>
                                <ImageLoad
                                    style={styles.bannerImageStyl}
                                    source={{ uri: (isImagePath) ? CommunityInfoData.ImagePath : 'https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg' }}
                                    isShowActivity={false}
                                    placeholderSource={{ uri: 'https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg' }}
                                    placeholderStyle={styles.bannerImageStyl}
                                />
                            </View>
                            <View pointerEvents="box-none">
                                <TouchableOpacity onPress={() => this._shareCommunity()}>
                                    <Image
                                        style={styles.shareIconStyle}
                                        resizeMode="contain"
                                        source={images.About_Community.shareIcon}
                                    />
                                </TouchableOpacity>
                                <View style={{ alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        {(isCommunityTypeID) ?
                                        (CommunityInfoData.CommunityTypeID === 2) ?
                                            <FontAwesome
                                                name="lock"
                                                size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05}
                                                color={colors.white}
                                                style={{ marginRight: globals.screenWidth * 0.02 }}
                                            />
                                            :
                                            null
                                            :
                                            null
                                        }
                                        <Text style={styles.communityNameStyle}>{(isCommunityName) ? CommunityInfoData.Name : '-'}</Text>
                                    </View>
                                </View>
                                {(CommunityUserStatus === 1 || CommunityUserStatus === 4 || CommunityUserStatus === 6 || CommunityUserStatus == 2) ?
                                    (CommunityUserStatus == 1) ?
                                        <View style={styles.waiting_apprivalBtnStyle}>
                                            <Ionicons
                                                name="md-time"
                                                size={globals.screenHeight * 0.02}
                                                color={colors.white}
                                                style={{ marginRight: globals.screenWidth * 0.02 }}
                                            />
                                            <Text style={styles.joinTextStyle}>Waitiing for Approval</Text>
                                        </View>
                                        :
                                        (CommunityUserStatus == 4) ?
                                            <View style={styles.acceptAndRejectBtnViewContainer}>
                                                <View style={[styles.joinButtonStyle, { marginRight: globals.screenWidth * 0.02 }]}>
                                                    <Text style={styles.joinTextStyle}>Accept</Text>
                                                </View>
                                                <View style={styles.joinButtonStyle}>
                                                    <Text style={styles.joinTextStyle}>Reject</Text>
                                                </View>
                                            </View>
                                            :
                                            (CommunityUserStatus == 6) ?
                                                <View style={styles.waiting_apprivalBtnStyle}>
                                                    <MaterialIcons
                                                        name="payment"
                                                        size={globals.screenHeight * 0.02}
                                                        color={colors.white}
                                                        style={{ marginRight: globals.screenWidth * 0.02 }}
                                                    />
                                                    <Text style={styles.joinTextStyle}>Payment Pending</Text>
                                                </View>
                                                :
                                                null
                                    :
                                    <TouchableOpacity style={styles.joinButtonStyle} onPress={() => this._setJoinMethod()}>
                                        <Text style={styles.joinTextStyle}>Join</Text>
                                    </TouchableOpacity>
                                }
                                <View style={styles.memberIconViewStyle}>
                                    <Image
                                        source={images.EventDashboard.attendees}
                                        style={styles.memberIconStyle}
                                        resizeMode="contain"
                                    />
                                     
                                    <Text style={styles.memberCountStyle}>
                                        {(isMembersCount) ? (CommunityInfoData.MembersCount <= '1') ?
                                            (CommunityInfoData.MembersCount) + " " + "Member" :
                                            (CommunityInfoData.MembersCount + " " + "Members") : ''}
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.iconsContainerStyle} pointerEvents="box-none">
                                {(CommunityUserRoleID == 1) && (CommunityStatusID != 1) ?
                                    <TouchableOpacity onPress={() => this.setModalVisible(true)} style={styles.inviteFriendButtonStyle}>
                                        <Text style={styles.inviteFriendTextStyle}>Invite Friends</Text>
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                                {(isfbUrl) ?
                                    <TouchableOpacity onPress={() => this._opneFacebook(facebookUrl)}>
                                        <Image style={[styles.socialIconStyle, {
                                            marginLeft: (CommunityUserRoleID == 1) && (CommunityStatusID != 1) ? (inviteCheck ? (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.35 : globals.screenWidth * 0.25) : globals.screenWidth * 0.55) : globals.screenWidth * 0.55
                                        }]}
                                            source={images.About_Community.innerFbicon}
                                        />
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                                {(istwitterUrl) ?
                                    <TouchableOpacity onPress={() => this._opneTwitter(twitterUrl)}>
                                        <Image style={[styles.socialIconStyle, { marginLeft: globals.screenWidth * 0.04 }]}
                                            source={images.About_Community.innerTwitter}
                                        />
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                                {(islinkedinUrl) ?
                                    <TouchableOpacity onPress={() => this._opneLinkedin(linkedInUrl)}>
                                        <Image style={[styles.socialIconStyle, { marginLeft: globals.screenWidth * 0.04 }]}
                                            source={images.About_Community.innerlinkedIn}
                                        />
                                    </TouchableOpacity>
                                    :
                                    null
                                }
                            </View>
                            <View pointerEvents="box-none">
                                {(loading === true || spotLightData.length > 0 && spotLightData != null) ?
                                    <Text style={[styles.eventSpotlightTextStyle, {
                                        marginTop: (CommunityUserStatus === 1 || CommunityUserStatus === 4 || CommunityUserStatus === 6 || CommunityUserStatus == 2) ? globals.screenHeight * 0.143 : globals.screenHeight * 0.09
                                    }]}>Event Spotlight</Text>
                                    :
                                    null
                                }

                                {(loading === true) ?
                                    <View style={styles.eventSpotlight_ActivityIndicator}>
                                        <ActivityIndicator size='small' style={styles.activityStyle} color={colors.bgColor}/>
                                    </View>
                                    :
                                    (spotLightData.length > 0 && spotLightData != null) ?
                                        <FlatList
                                            data={spotLightData}
                                            showsHorizontalScrollIndicator={false}
                                            renderItem={({ item, index }) => this.renderItemList(item, index)}
                                            keyExtractor={(index, item) => item.toString()}
                                            extraData={this.state}
                                            horizontal
                                        />
                                        :

                                        null
                                }
                            </View>
                            <Text style={[styles.aboutCommunityTitle,
                            { marginTop: (spotLightData.length > 0 && spotLightData != null) ? globals.screenHeight * 0.02 : (CommunityUserStatus === 6 || CommunityUserStatus == 2) ? globals.screenHeight * 0.13 : globals.screenHeight * 0.08, }]}>About Community</Text>
                            <View style={styles.descriptionViewStyle}>
                                <HTML html={(isDescription) ? CommunityInfoData.Description : '-'} baseFontStyle={styles.descriptionStyle} />
                            </View>
                            {(CommunityUserStatus === 6 || CommunityUserStatus == 2) ?
                                null
                                :
                                (CommunityUserStatus === 1 || CommunityUserStatus === 4 || CommunityUserStatus === 6 || CommunityUserStatus == 2) ?
                                    <View style={styles.waiting_approvalViewContainer}>
                                        <Image
                                            source={images.EventDashboard.attendees}
                                            style={styles.waiting_approvalIconStyle}
                                            resizeMode="contain"
                                        />
                                        <Text style={{ marginLeft: (Platform.OS == "android") ? globals.screenWidth * 0.04 : 0 }}>Join the community to view the community information</Text>
                                    </View>
                                    :
                                    null
                            }
                        </View>
                }
            </View>

        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AboutCommunity);