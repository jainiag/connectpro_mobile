/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */
/* eslint-disable no-return-assign */
/* eslint-disable no-useless-constructor */
import React from 'react';
import { Text, View, TouchableOpacity, TextInput, ScrollView, Alert, FlatList } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import Validation from '../../../../../utils/validation';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import { API } from '../../../../../utils/api';
import { Dropdown } from '../../../../../libs/react-native-material-dropdown';
import Icon from 'react-native-vector-icons/AntDesign';
import Communities from '../mycommunityList/index';
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import KeyboardListener from 'react-native-keyboard-listener';

const TAG = '==:== CreateCommunity Screen : ';
let arrCategory;
let arrCategoryName = [];
let uniquenewData;
const iPad = DeviceInfo.getModel();

class CreateCommunity extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: globals.ConnectProbackButton(navigation, 'Create Community'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            isFrom: this.props.navigation.state.params.isFrom,
            Name: '',
            AboutCommunity: '',
            otherPurpose: '',
            category_Name: '',
            categorytype: '',
            profile_Category: [],
            purpose: [],
            data: [{
                value: 'Public',
                ID: '1',
            }, {
                value: 'Private',
                ID: '2',
            },],
            openOther: false,
            isChecked: [],
            selectedLists: [],
            allSelectedList: false,
            isCheck: false,
            communityCategoryId: '',
            communityPurposeTypeId: '',
            communityTypeID: '',
            categorypurposeIDsArray: [],
            logoPath: '',
            keyboardOpen: false,
        };

    }


    componentDidMount() {
        this._CreateCommunityDetails()
    }

    /**
* Method of isIconSelectedOrNot
* 
*/
    isIconSelectedOrNot = (item, index) => {
        let { isChecked, selectedLists } = this.state;
        isChecked[index] = !isChecked[index];
        this.setState({ isChecked: isChecked, });
        if (isChecked[index] == true) {
            selectedLists.push(item)
            {
                (item.Name == 'Other') ?
                    this.setState({ openOther: true })
                    :
                    null
            }
        } else {
            {
                (item.Name == 'Other' && this.state.openOther == true) ?
                    this.setState({ openOther: false, otherPurpose: '' }, () => {
                        selectedLists.pop(item)
                    })
                    :
                    selectedLists.pop(item)
            }
        }
    }

    /**
* Method of Checked accept btn
* 
*/
    onChecked() {
        this.setState({ isCheck: true })
        if (this.state.isCheck == true) {
            this.setState({ isCheck: false, })
        }
        else {
            this.setState({ isCheck: true })
        }
    }

    /**
* API call of _CreateCommunityDetails
* 
*/
    _CreateCommunityDetails() {
        if (globals.isInternetConnected === true) {
            API.createCommunityDetails(this.createCommunityDetailsDataResponseData, this.state.communityId, true)
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
    * callback response of createCommunityDetailsDataResponseData
    *
    */
    createCommunityDetailsDataResponseData = {
        success: response => {
            console.log(
                TAG,
                'createCommunityDetailsDataResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                const responseData = response.Data.Categories;
                const purposeData = response.Data.Purposes
                this.setState({ profile_Category: responseData })
                this.setState({ purpose: purposeData })
            } else {
                Alert.alert(globals.appName, response.Message)
            }
        },
        error: err => {
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            }
            else {
                Alert.alert(globals.appName, err.Message)
            }
            console.log(
                TAG,
                'createCommunityDetailsDataResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
        },
        complete: () => {
        },
    };


    /**
   *  call when _sessionOnPres
   */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    /**
* render flatlist data
* 
*/
    renderItemList(item, index) {
        const isName = globals.checkObject(item, 'Name');
        return (
            <View style={styles.selctedview}>
                <TouchableOpacity style={[styles.selectedtouchbleView, {
                    backgroundColor: (this.state.isChecked[index] == true) || (this.state.allSelectedList == true) ? colors.listSelectColor : colors.white,
                }]}
                    onPress={() => this.isIconSelectedOrNot(item, index)}
                >
                    <View style={styles.beforeTextView}>
                        <Text style={[styles.beforeTextStyle, {
                            color: (this.state.isChecked[index] == true) || (this.state.allSelectedList == true) ? colors.white : colors.black,
                        }]}>
                            {(isName) ? item.Name : '-'}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }



    /**
* OnChange Values of Category Type
* 
*/
    onChangeText2(text, index, data) {
        var ID = data[index].ID;
        return (
            this.setState({ communityTypeID: ID, categorytype: text })
        )
    }

    /**
* OnChange Values of Select Category 
* 
*/
    onChangeText1(text, index, data) {
        var ID = data[index].ID;
        var LogoPath = data[index].LogoPath;
        return (
            this.setState({ communityCategoryId: ID, category_Name: text, logoPath: LogoPath })
        )
    }


    /**
      * prepareData when call the API
      */
    prepareData() {

        var categorypurposeID;
        var obj = {};
        const {
            logoPath, selectedLists,
            communityTypeID,
            isCheck, communityCategoryId,
            Name, AboutCommunity, otherPurpose, categorypurposeIDsArray
        } = this.state;

        var selectedListsIds = selectedLists;
        for (let i = 0; i < selectedListsIds.length; i++) {
            categorypurposeID = selectedListsIds[i].ID
            categorypurposeIDsArray.push(categorypurposeID)
            obj[JSON.parse(selectedListsIds[i].ID)] = true
            var categorypurposeIDs_Array = categorypurposeIDsArray.toString()
        }

        const Data =
        {
            CommunityCategoryId: JSON.stringify(communityCategoryId),
            CommunityPurposeTypeId: categorypurposeIDs_Array,
            CommunityTypeID: communityTypeID,
            Description: AboutCommunity,
            ImagePath: "https://cpcommunityqa.azurewebsites.net/Content/Uploads/Communities/default-community-image.png",
            IsaccesstoGlobalCommunities: isCheck,
            IsActive: true,
            LogoPath: null,
            Name: Name,
            OtherPurpose: otherPurpose,
            Purposes: obj,
            AccessToken: JSON.parse(globals.tokenValue)
        }

        return Data;
    }


    /**
* Method of cancle clear textfileds
* 
*/
    _onCancle() {
        this.setState({
            Name: '',
            AboutCommunity: '',
            category_Name: '',
            categorytype: '',
            isChecked: [],
            otherPurpose: '',
        }, () => {
            (this.state.isFrom === 'GlobalCommunity') ?
                this.props.navigation.navigate("GLOBAL_COMMUNITIES_SCREEN") :
                this.props.navigation.navigate("Communities")

        })
    }

    /**
* Method of create validations
* 
*/
    _onCreate() {
        const { Name, isChecked, openOther, category_Name, categorytype, AboutCommunity, otherPurpose } = this.state;
        if (Validation.textInputCheck(Name)) {
            if (Name.length >= 5 && Name.length <= 1000) {
                if (isChecked.length != '') {
                    if (Validation.textInputCheck(AboutCommunity)) {
                        if (AboutCommunity.length >= 5 && AboutCommunity.length <= 1000) {
                            if (category_Name != '') {
                                if (categorytype != '') {
                                    if (openOther == true)
                                        if (Validation.textInputCheck(otherPurpose)) {
                                            this.props.showLoader()

                                            if (globals.isInternetConnected === true) {
                                                API.saveCommunityAsync(this.saveCommunityAsyncResponseData, this.prepareData(), true, globals.userID)
                                            } else {
                                                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
                                            }
                                        } else {
                                            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_OTHERPRUPOSECATEGORY);
                                        }
                                    else {
                                        this.props.showLoader()
                                        if (globals.isInternetConnected === true) {
                                            API.saveCommunityAsync(this.saveCommunityAsyncResponseData, this.prepareData(), true, globals.userID)
                                        } else {
                                            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
                                        }
                                    }
                                } else {
                                    Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_SELECTTYPE);
                                }
                            } else {
                                Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_SELECTCATEGORY);
                            }
                        } else {
                            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_MAXABOUTCOMMUNITY);
                        }

                    } else {
                        Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_ABOUTCOMMUNITY);
                    }
                } else {
                    Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_PRUPOSECATEGORY);
                }
            } else {
                Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_MAXNAME);
            }
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_COMM_NAME);
        }
    }



    /**
* callback response of saveCommunityAsyncResponseData
* 
*/
    saveCommunityAsyncResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveCommunityAsyncResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.props.hideLoader()
                Alert.alert(
                    globals.appName,
                    response.Message,
                    [{ text: 'OK', onPress: () => this.props.navigation.goBack(null) }],
                    { cancelable: false }
                );

                Communities.updateCommunityData()
            } else {
                Alert.alert(globals.appName, response.Message)
            }
        },
        error: err => {
            this.props.hideLoader()
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            }
            else {
                Alert.alert(globals.appName, err.Message)
            }
            console.log(
                TAG,
                'saveCommunityAsyncResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
        },
        complete: () => {
            this.props.hideLoader()
        },
    };




    render() {
        const { Name, data, isCheck, AboutCommunity, profile_Category, otherPurpose, openOther, purpose } = this.state;
        arrCategory = profile_Category;


        for (i = 0; i < arrCategory.length; i++) {
            var categoryID = arrCategory[i].ID
            var categoryLogopath = arrCategory[i].LogoPath
            var categoryname = arrCategory[i].Name
            let object = {
                "value": categoryname,
                "ID": categoryID,
                "LogoPath": categoryLogopath,
            }
            arrCategoryName.push(object);
            uniquenewData = arrCategoryName.filter((ele, ind) => ind === arrCategoryName.findIndex(elem => elem.ID === ele.ID && elem.id === ele.id))

        }

        return (
            <View style={styles.mainParentStyle}>
                <KeyboardListener
                    onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                    onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.svStyle}>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={'Enter Your Community Name'}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ Name: text })}
                                returnKeyType="next"
                                blurOnSubmit={false}
                                value={Name}
                                onSubmitEditing={() => this.memberNameRef.focus()}
                                autoCapitalize="none"
                            />
                        </View>
                        <View style={styles.purposeViewContainer}>
                            <Text numberOfLines={1} style={[styles.purposeTextStyle, {
                                marginHorizontal: globals.screenWidth * 0.04,
                            }]}>Purpose</Text>
                            <FlatList
                                data={purpose}
                                style={{ marginBottom: globals.screenHeight * 0.013, marginTop: globals.screenHeight * 0.013 }}
                                bounces={false}
                                showsVerticalScrollIndicator={false}
                                renderItem={({ item, index }) => this.renderItemList(item, index)}
                                keyExtractor={(index, item) => item.toString()}
                                extraData={this.state}
                                numColumns={2}
                            />
                        </View>
                        {(openOther == true) ?
                            <View style={styles.textInputViewContainer2}>
                                <TextInput
                                    style={styles.textInputStyleContainer}
                                    placeholder={"Other Purpose"}
                                    placeholderTextColor={colors.lightGray}
                                    onChangeText={text => this.setState({ otherPurpose: text })}
                                    returnKeyType="done"
                                    blurOnSubmit={false}
                                    value={otherPurpose}
                                    autoCapitalize="none"
                                    ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                                />
                            </View>
                            : null
                        }
                        <View style={styles.textInputViewContainer2}>
                            <TextInput
                                multiline={true}
                                maxLength={300}
                                style={styles.textInputStyleContainer}
                                placeholder={"About Community"}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ AboutCommunity: text })}
                                returnKeyType="done"
                                blurOnSubmit={false}
                                value={AboutCommunity}
                                autoCapitalize="none"
                                ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                            />
                        </View>

                        <Dropdown
                            containerStyle={styles.dropdownContainer}
                            fontSize={globals.font_14}
                            itemColor={colors.black}
                            textColor={colors.black}
                            label='Select Category'
                            data={uniquenewData}
                            onChangeText={(text, index, data) => this.onChangeText1(text, index, data)}
                        />

                        <Dropdown
                            containerStyle={styles.dropdownContainer}
                            fontSize={globals.font_14}
                            itemColor={colors.black}
                            textColor={colors.black}
                            label='Select Type'
                            data={data}
                            onChangeText={(text, index, data) => this.onChangeText2(text, index, data)}
                        />

                        <View style={styles.checkBoxViewContainer}>
                            <Text style={styles.accessTextStyle}>Access to External Communities</Text>
                            <TouchableOpacity onPress={() => this.onChecked()} style={styles.squareView}>
                                {(isCheck === true) ? (
                                    <Icon name="check" color={colors.black}
                                        size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 20 : globals.screenWidth * 0.0425} />
                                ) : null}
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                {(Platform.OS == "android") ?
                    (this.state.keyboardOpen == false) ?
                        <View style={styles.createCancelBtnViewContainer} >
                            <TouchableOpacity style={styles.cancelBtnContainer} onPress={() => this._onCancle()}>
                                <Text style={[styles.seachTextStyle, { color: colors.black }]}>{'CANCEL'}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.createBtnContainer} onPress={() => this._onCreate()}>
                                <Text style={[styles.seachTextStyle, { color: colors.white }]}>{'CREATE'}</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                    :
                    <View style={styles.createCancelBtnViewContainer} >
                        <TouchableOpacity style={styles.cancelBtnContainer} onPress={() => this._onCancle()}>
                            <Text style={[styles.seachTextStyle, { color: colors.black }]}>{'CANCEL'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.createBtnContainer} onPress={() => this._onCreate()}>
                            <Text style={[styles.seachTextStyle, { color: colors.white }]}>{'CREATE'}</Text>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        );
    }
}


// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateCommunity);