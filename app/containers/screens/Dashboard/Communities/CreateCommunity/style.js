import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  textInputViewContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.05,
    borderRadius: 5,
    
  },
  textInputViewContainer2: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.025,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginHorizontal: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginVertical: globals.screenHeight *  0.014,
    // marginBottom: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(15),
  },
  
  createBtnContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.073 : globals.screenHeight * 0.06, // change in scrollview style also
    width: '50%',
    backgroundColor: colors.bgColor,
    justifyContent: 'center',
    alignItems: 'center',
    // position: 'absolute',
    // bottom: 0,
  },
  cancelBtnContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.073 : globals.screenHeight * 0.06, // change in scrollview style also
    width: '50%',
    backgroundColor: colors.lightGray,
    justifyContent: 'center',
    alignItems: 'center',
    // position: 'absolute',
    // bottom: 0,
  },
  seachTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    // color: colors.white,
  },
  squareView: {
    // marginTop: globals.screenHeight * 0.0065,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.049,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.049,
    borderColor: colors.lightBlack,
    borderWidth: 1,
    borderRadius:2,
    marginLeft:globals.screenWidth * 0.02
  },
  svStyle: {
    marginBottom: getVerticleBaseValue(57),
  },
  datePickerViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 15 : 20,
  },
  pickerStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.935 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenWidth * 0.84 : globals.screenWidth * 0.87,
  },
  placeHolderTextStyle: {
    color: colors.lightGray,
    fontSize: globals.font_14,
    textAlign: 'left',
    marginRight: iPad.indexOf('iPad') != -1 ? globals.screenWidth * 0.58 : DeviceInfo.isTablet() ? globals.screenWidth * 0.62 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenWidth * 0.41 : globals.screenWidth * 0.4,
  },
  startDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.proUnderline,
    marginTop: 8,
    marginBottom: 8,
    height: iPad.indexOf('iPad') != -1 ? globals.screenHeight * 0.056 : DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : (Platform.OS == 'android') ? globals.screenHeight * 0.065 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenHeight * 0.07 : globals.screenHeight * 0.06,
  },
  endDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.proUnderline,
    marginTop: 8,
    marginBottom: 8,
    marginLeft: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 5 : 10,
    marginRight: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 5 : 20,
  },
  dateIconStyle: {
    position: 'absolute',
    left: 0,
    marginLeft: 0,
  },
  multipleSelectViewStyle: {
    marginTop: globals.screenHeight * 0.025,
  },
  multipleSelectDropdownMenu: {
    alignItems: 'center', 
    alignSelf: 'center',
  },
  multipleSelectMainWrapper: {
    borderWidth: 1, 
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.06 : null,
    borderColor: colors.proUnderline, 
    borderRadius: 5, 
    marginHorizontal: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.026 : (Platform.OS === 'android') ? globals.screenWidth * 0.065 : globals.screenWidth * 0.06,
  },
  purposeViewContainer: {
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    // alignItems: 'center',
    marginTop: globals.screenHeight * 0.025,
    borderRadius: 5,
  },
  purposeTextStyle: {
    color: colors.black, 
    fontWeight: '500', 
    fontSize: globals.font_15, 
    marginTop: globals.screenHeight * 0.01,
    marginLeft: globals.screenWidth * 0.04,
  },
  puroseTagViewContainer: {
    marginTop: globals.screenHeight * 0.012, 
    marginBottom: globals.screenHeight * 0.012 
  },
  tag_ViewContainer: {
    flexDirection: 'row', 
    alignItems: 'center', 
    marginLeft: globals.screenWidth * 0.04,
  },
  commonStyle: {
    marginTop: globals.screenHeight * 0.009
  },
  otherTagStyle: {
    marginLeft: globals.screenWidth * 0.04,
    marginTop: globals.screenHeight * 0.009
  },
  networkingBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.3,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.darkSkyBlue
  },
  marketingBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.3,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
    marginLeft: globals.screenWidth * 0.03
  },
  brandingBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.3,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white
  },
  buildingBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.4,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.darkSkyBlue,
    marginLeft: globals.screenWidth * 0.03
  },
  growBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.38,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white
  },
  investBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.38,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.white,
    marginLeft: globals.screenWidth * 0.03
  },
  otherBtnStyle: {
    height: globals.screenHeight * 0.035,
    width: globals.screenWidth * 0.2,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: colors.white
  },
  createCancelBtnViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
  },
  accessTextStyle: {
    fontSize: globals.font_14
  },
  checkBoxStyle: {
    height: 22, 
    width: 22, 
    borderColor: colors.black, 
    borderWidth: 0.6, 
    borderRadius: 4, 
    marginLeft: globals.screenWidth * 0.05, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  checkBoxViewContainer: {
    flexDirection: 'row', 
    alignItems: 'center', 
    marginLeft: 25, 
    marginTop: globals.screenHeight * 0.015,
  },
  selctedview:
  {
    marginHorizontal: globals.screenWidth * 0.04,
  },
  selectedtouchbleView:{
    flex: 1,
    marginTop: 8,
    borderWidth: 1,
    borderColor: colors.proUnderline, borderRadius: 4
  },
  beforeTextView:{
    margin: 5
  },
  beforeTextStyle:{
    fontSize:globals.font_13
  },
  dropdownContainer:{
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    marginTop: globals.screenHeight * 0.025,
    borderRadius: 5,
  }
});
