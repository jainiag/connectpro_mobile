/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, Alert, TextInput, ActivityIndicator, Modal, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import { NavigationEvents } from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import styles from './style'
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import { connect } from 'react-redux';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import Icon from 'react-native-vector-icons/EvilIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { API } from '../../../../../utils/api';
import { bindActionCreators } from 'redux';
import Swipeable from 'react-native-swipeable';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import Nointernet from '../../../../../components/NoInternet/index'
import ServerError from '../../../../../components/ServerError/index'
import KeyboardListener from 'react-native-keyboard-listener';

const iPad = DeviceInfo.getModel()
let TAG = "My communities :: === "
let _this = null;
let uniquecommunityData;

class Communities extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'My Communities'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
        headerRight: (
            <View style={{ alignItems: 'center' }}>
                <TouchableOpacity
                    onPress={() => { _this.setModalVisible(true); _this.setState({ communityName: '' }) }}
                    style={{
                        marginRight: globals.screenWidth * 0.04,
                        alignItems: 'center',
                        marginTop:
                            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                ? globals.screenHeight * 0.03
                                : null,
                    }}
                >
                    <Image
                        source={images.headerIcon.searchIcon}
                        resizeMode={"contain"}
                        style={{
                            height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                            width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                            tintColor: colors.warmBlue,
                            alignSelf: 'center',
                        }}
                    />
                </TouchableOpacity>
            </View>
        ),
    });


    constructor(props) {
        super(props);
        _this = this
        this.state = {
            communitiesData: [],
            loading: false,
            isInternetFlag: true,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            totalCommunities: 0,
            cancelModalReview: false,
            cancelModalAproval: false,
            leavecommunityModal: false,
            leaveModal: false,
            communityID: 0,
            onSwipOutCancel: false,
            onSwipOutAccept: false,
            searchBtnClick: false,
            keyboardOpen: false,
        }
    }


    /**
  * API Call of get Community List after create community
  */
    static updateCommunityData() {
        _this.apiCallMyCommunities()
    }

    componentDidMount() {
        // var aestTime = new Date().toLocaleString("en-US", { timeZone: "Australia/Brisbane" });
        // aestTime = new Date(aestTime);
        // console.log('AEST time: ' + aestTime.toLocaleString())

        // var asiaTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Shanghai" });
        // asiaTime = new Date(asiaTime);
        // console.log('Asia time: ' + asiaTime.toLocaleString())

        // var usaTime = new Date().toLocaleString("en-US", { timeZone: "America/New_York" });
        // usaTime = new Date(usaTime);
        // console.log('USA time: ' + usaTime.toLocaleString())

        // var indiaTime = new Date().toLocaleString("en-US", { timeZone: "Asia/Kolkata" });
        // indiaTime = new Date(indiaTime);
        // console.log('India time: ' + indiaTime.toLocaleString())
        this.props.showLoader()
        //this.apiCallMyCommunities()
    }

    /**
     * Method for change modal state
     * @param {*} visible 
     */
    setModalVisible(visible) {
        this.setState({ searchBtnClick: true })
        if (visible == false) {
            this.setState({ isShowSearch: visible })
        }
        this.setState({ isShowSearch: visible });
    }

    oncloseModal() {
        this.setModalVisible(false)
        this.props.showLoader()
        this.clearStates();
    }

    /**
     * Search modal render function
     */
    renderSearchModel() {
        const { communityName } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <KeyboardListener
                        onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                        onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                    <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                        <TouchableOpacity onPress={() => { this.oncloseModal() }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Community'}
                        </Text>
                    </View>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_COMMUNITYNAME}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ communityName: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={communityName}
                                autoCapitalize="none"
                            />
                        </View>
                        {(Platform.OS == "android") ?
                            (this.state.keyboardOpen == false) ?
                                <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByCommunityName()}>
                                    <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                                </TouchableOpacity>
                                :
                                null
                            :
                            <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByCommunityName()}>
                                <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </Modal>
        );
    }
    /**
     * prepareData when call the API
     */
    prepareData() {
        const {
            pageNumber,
            pageSize,
            communityName,
        } = this.state;

        const data = {};
        data.UserID = globals.userID
        data.CommunityName = communityName
        data.IsCommunityAdmin = true;
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        data.IsAccessToGlobalCommunities = true;
        data.UserAccessToken = JSON.parse(globals.tokenValue);
        return data;
    }
    /**
     * Api call of my communitylist
     */
    apiCallMyCommunities() {
        if (globals.isInternetConnected === true) {
            this.setState({ loadingPagination: true, loading: true, isInternetFlag: true })
            API.getMyCommunities(this.GetMyCommunityResponseData, this.prepareData(), true)
        } else {
            this.props.hideLoader()
            this.setState({ isInternetFlag: false })
        }
    }

    /**
     * click on search based on community name
     */
    seacrhByCommunityName() {
        const { communityName } = this.state;
        this.setState({ communityName: communityName }, () => {
            this.setModalVisible(false);
            this.searchCommunity();
        })
    }

    /**
     * Method of get  communities based on search filter
     */
    searchCommunity() {
        _this.makeListFresh(() => {
            _this.props.showLoader();
            _this.apiCallMyCommunities();
        });
    }

    makeListFresh(callback) {
        this.setState(
            {
                pageNumber: 1,
                pageSize: 10,
                communitiesData: [],
                responseComes: false
            },
            () => {
                callback();
            }
        );
    }

    /**
  * Response of MyCommunity listing
  */
    GetMyCommunityResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetMyCommunityResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                // this.setState({ responseComes: false, totalCommunities: response.Data.TotalRecords }, () => {
                //      this.updatePageCount(response.Data.TotalRecords, () => {
                //     const { communitiesData } = this.state;
                //     const myCommunities = response.Data.UserCommunities;
                //     console.log(TAG, `new communities : ${myCommunities.length}` + "length --> " + communitiesData.length);
                //     let myCommunitylistData = [];
                //     if (communitiesData.length > 0) {
                //     myCommunitylistData = Object.assign([], communitiesData);
                //     myCommunitylistData = [...communitiesData, ...myCommunities];

                //     }
                //     for (let i = 0; i < myCommunities.length; i++) {
                //         const myCommunity = myCommunities[i];
                //         if (!this.checkUniquiness(myCommunitylistData, 'CommunityID', myCommunity.CommunityID)) {
                //             myCommunitylistData.push(myCommunity);
                //         }
                //     }
                //     this.setState({ communitiesData: myCommunitylistData, responseComes: true }, () => {
                //         console.log("community data ::: ", this.state.communitiesData)
                //     })
                //      });
                // })
                total = response.Data.TotalRecords;
                if (this.state.pageNumber == 1) {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        communitiesData: response.Data.UserCommunities,
                    })
                } else {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        communitiesData: [...this.state.communitiesData, ...response.Data.UserCommunities],
                    })
                }
            } else {
                // if ((this.state.pageNumber == 1) && (response.StatusCode == 401 || response.StatusCode == 403)) {
                //     this.setState({ loading: false, loadingPagination: false })
                //     Alert.alert(
                //         globals.appName,
                //         'Your session is expired, Please login again',
                //         [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                //         { cancelable: false }
                //     );
                // } 
                // else{
                Alert.alert(globals.appName, response.Message)
                // }
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetMyCommunityResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if ((this.state.pageNumber == 1) && (err.StatusCode == 401 || err.StatusCode == 403)) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
*  call when _sessionOnPres
*/
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.apiCallMyCommunities();
            })
            this.onEndReachedCalledDuringMomentum = true;

        }
    };


    updatePageCount(count, callback) {
        this.setState({ totalCommunities: count }, () => {
            callback();
        });
    }

    checkUniquiness(dataToCheck, keyName, valueToSearch) {
        let isExist = false;
        const foundIndex = dataToCheck.findIndex(item => item[keyName] == valueToSearch);
        if (foundIndex == -1) {
            isExist = false;
        } else {
            isExist = true;
        }
        return isExist;
    }


    /**
    * Render FlatList Items
    */
    renderMyCommunities(item, index) {
        return (
            <View>
                {
                    this.renderViewsAccordingConditions(item, index)
                }
            </View>
        );
    }

    /**
     * Handle status for cancel click of waiting for review
     * @param {*} visible 
     */
    handleCancelModalStatusReview(visible, communityId) {
        this.setState({ cancelModalReview: visible, communityID: communityId })
    }

    /**
     * Click on Accept for community request invitation
     * @param {*} communityId 
     */
    AcceptInvitationRequest(communityId) {
        this.setState({ loading: true })
        const data = {
            actionStatusID: 2,
            userids: globals.userID,
            communityID: communityId,
            loggedInUserId: globals.userID,
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.acceptRejectReqInviteMember(this.AcceptRejectInvitationResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    _acceptRequestInvitation(communityId) {
        Alert.alert(
            globals.appName,
            "Are you sure you want to accept invitation this community?",
            [{ text: 'OK', onPress: () => this.AcceptInvitationRequest(communityId) },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') }],
        );
    }

    /**
    * Click on Activate for Inactive community
    * @param {} communityId 
    */
    clickOnActivateInactivateCommunity(communityId) {
        this.setState({ loading: true })
        if (globals.isInternetConnected == true) {
            API.activateCommunity(this.ActivateInactivateCommunityResponseData, communityId, globals.userID, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    _activateInactiveCommunity(communityId) {
        Alert.alert(
            globals.appName,
            "Are you sure you want to activate this community?",
            [{ text: 'OK', onPress: () => this.clickOnActivateInactivateCommunity(communityId) },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') }],
        );
    }

    /**
     * Click on Reject for community invitation
     * @param {} communityId 
     */
    RejectInvitationRequest(communityId) {
        this.setState({ loading: true })
        const data = {
            actionStatusID: 3,
            userids: globals.userID,
            communityID: communityId,
            loggedInUserId: globals.userID,
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.acceptRejectReqInviteMember(this.AcceptRejectInvitationResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    _rejectRequestInvitation(communityId) {
        Alert.alert(
            globals.appName,
            "Are you sure you want to reject invitation this community?",
            [{ text: 'OK', onPress: () => this.RejectInvitationRequest(communityId) },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') }],
        );
    }

    /**
     * Handle status for cancel click of waiting for review
     * @param {*} visible 
     */
    handleLaeaveCommunityModalStatus(visible, communityId) {
        this.setState({ leavecommunityModal: visible, communityID: communityId })
    }

    /**
     * Handle status for cancel click of waiting for Approval
     * @param {*} visible 
     */
    handleCancelModalStatusApproval(visible, communityId) {
        this.setState({ cancelModalAproval: visible, communityID: communityId })
    }

    /**
    * Render modal of cancel click of waiting for review
    */
    renderCancelWaitingReviewModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.cancelModalReview}
                onRequestClose={() => {
                    this.handleCancelModalStatusReview(false, this.state.communityID);
                }}
                onBackdropPress={() => {
                    this.handleCancelModalStatusReview(false, this.state.communityID);
                }}
            >
                <View style={[styles.modalMainView, { height: globals.screenHeight * 0.23 }]}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.popupinnerViewStyle}>
                            <Text style={styles.popupheaderStyle}>{"By cancelling the community review request, the community will not be created. Do you want to be proceed?"}</Text>
                            <View style={styles.btnViewStyle}>
                                <TouchableOpacity onPress={() => this.handleCancelModalStatusReview(false, this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                                            name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                                        <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.ProceedWaitingReviewclick(this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                                            name={"check"} color={colors.white} style={styles.vectorStyle} />
                                        <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    /**
     * Render modal of leave community
     */
    renderLeaveCommunityModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.leavecommunityModal}
                onRequestClose={() => {
                    this.handleLaeaveCommunityModalStatus(false, this.state.communityID);
                }}
                onBackdropPress={() => {
                    this.handleLaeaveCommunityModalStatus(false, this.state.communityID);
                }}
            >
                <View style={styles.modalMainView}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.popupinnerViewStyle}>
                            <Text style={styles.popupheaderStyle}>{"Are you sure you want to leave this community?"}</Text>
                            <View style={styles.btnViewStyle}>
                                <TouchableOpacity onPress={() => this.handleLaeaveCommunityModalStatus(false, this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                                            name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                                        <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.clickProceedLeaveCommunity(this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                                            name={"check"} color={colors.white} style={styles.vectorStyle} />
                                        <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    /**
     * Render modal of cancel click of waiting for approval
     */
    renderCancelWaitingApprovalModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.cancelModalAproval}
                onRequestClose={() => {
                    this.handleCancelModalStatusApproval(false, this.state.communityID);
                }}
                onBackdropPress={() => {
                    this.handleCancelModalStatusApproval(false, this.state.communityID);
                }}
            >
                <View style={styles.modalMainView}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.popupinnerViewStyle}>
                            <Text style={styles.popupheaderStyle}>{"Do you want to cancel the request?"}</Text>
                            <View style={styles.btnViewStyle}>
                                <TouchableOpacity onPress={() => this.handleCancelModalStatusApproval(false, this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                                            name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                                        <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.clickApprovalProceed(this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                                            name={"check"} color={colors.white} style={styles.vectorStyle} />
                                        <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    /**
     * Click of Yes, procced on waiting for approval popup
     */
    clickApprovalProceed() {
        this.handleCancelModalStatusApproval(false, this.state.communityID)
        this.props.showLoader()
        this.setState({ loading: true })
        const data = {
            communityIds: [
                this.state.communityID
            ],
            statusId: 5,
            userId: globals.userID,
            rejectReason: "",
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.saveUserStatusInCommunities(this.saveUserStatusInCommunitiesResponseDataApproval, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    clickProceedLeaveCommunity() {
        this.handleLaeaveCommunityModalStatus(false, this.state.communityID)
        this.props.showLoader()
        this.setState({ loading: true })
        const data = {
            communityIds: [
                this.state.communityID
            ],
            statusId: 5,
            userId: globals.userID,
            rejectReason: "",
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.saveUserStatusInCommunities(this.saveUserStatusInCommunitiesResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
     * Click of Yes, procced on waiting for review popup
     */
    ProceedWaitingReviewclick() {
        this.handleCancelModalStatusReview(false, this.state.communityID)
        this.props.showLoader()
        this.setState({ loading: true })
        const data = {
            communityIds: [
                this.state.communityID
            ],
            statusId: 3,
            userId: globals.userID,
            rejectReason: "",
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.saveCommunityStatus(this.saveCommunityStatusResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
     * Response of saveCommunityStatus
     */
    saveCommunityStatusResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveCommunityStatusResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.clearStates()
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message)
                })

            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveCommunityStatusResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
     * Response of Accept Reject Invitation 
     */
    AcceptRejectInvitationResponseData = {
        success: response => {
            console.log(
                TAG,
                'AcceptRejectInvitationResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                Alert.alert(globals.appName, response.Message);
                this.clearStates()
            }
            else {
                this.setState({ loading: false })
                Alert.alert(globals.appName, response.Message);
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'AcceptRejectInvitationResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
     * Response of Activate community click
     */
    ActivateInactivateCommunityResponseData = {
        success: response => {
            console.log(
                TAG,
                'ActivateInactivateCommunityResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                Alert.alert(globals.appName, response.Message);
                this.clearStates()
            }
            else {
                this.setState({ loading: false })
                Alert.alert(globals.appName, response.Message);
                //  Alert.alert(globals.appName, 'Something went wrong');
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'ActivateInactivateCommunityResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
     * Clear states for waiting for approval cancel click and list refresh
     */
    clearStates() {
        this.setState({
            communitiesData: [],
            loading: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            totalCommunities: 0,
            cancelModalReview: false,
            cancelModalAproval: false,
            leavecommunityModal: false,
            leaveModal: false,
            communityID: 0,
            searchBtnClick: false
        }, () => {
            this.apiCallMyCommunities()
        })
    }

    /**
        * Response callback of saveUserStatusInCommunitiesResponseData
        */
    saveUserStatusInCommunitiesResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveUserStatusInCommunitiesResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                if(response.Message == "" || response.Message == "null" || response.Message == undefined || response.Message == null) {
                    Alert.alert(globals.appName, "You have been removed successfully from this Community.");
                }else{
                    Alert.alert(globals.appName, response.Message);
                }
                this.clearStates()
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message);
                })
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveUserStatusInCommunitiesResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

     /**
        * Response callback of saveUserStatusInCommunitiesResponseData
        */
       saveUserStatusInCommunitiesResponseDataApproval = {
        success: response => {
            console.log(
                TAG,
                'saveUserStatusInCommunitiesResponseDataApproval -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                if(response.Message == "" || response.Message == "null" || response.Message == undefined || response.Message == null) {
                    Alert.alert(globals.appName, "Your request is processed");
                }else{
                    Alert.alert(globals.appName, response.Message);
                }
                this.clearStates()
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message);
                })
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveUserStatusInCommunitiesResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    onOpenSwipeout() {
        this.setState({ onSwipOutAccept: !this.state.onSwipOutAccept, onSwipOutCancel: !this.state.onSwipOutCancel })
    }
    onCloseSwipeout() {
        this.setState({ onSwipOutAccept: !this.state.onSwipOutAccept, onSwipOutCancel: !this.state.onSwipOutCancel })
    }

    renderViewsAccordingConditions(item, index) {
        const isCommunityName = globals.checkObject(item, 'CommunityName');
        const isCommunityType = globals.checkObject(item, 'CommunityTypeID');
        const isCategoryName = globals.checkObject(item, 'CategoryName');
        const isMemberCount = globals.checkObject(item, 'MemberCount');
        const isCommunityLogo = globals.checkImageObject(item, 'LogoWebPath');
        // if(item.CommunityStatusID == 1){
        //    return(
        //        <View></View>
        //    )
        // }
        // else if(item.CommunityUserStatus == 6){
        //     return(
        //         <View></View>
        //     )
        // }
         if ((item.Isactive && item.CommunityStatusID == 1)) {//waiting for review - system admin
            return (
                <Swipeable leftButtons={null} leftContent={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleCancelModalStatusReview(true, item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.cancelApproval}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'CANCEL'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID, isFromProp: "Review", activereview: item.Isactive, CommunityStatusID: item.CommunityStatusID } })}>
                            {/* <TouchableOpacity> */}
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                                item.MemberCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                </View>
                                <View style={styles.lastImgViewEnd}>
                                    <Image
                                        source={images.Communities.waitingReview}
                                        style={styles.attende_shareIconStyle}
                                    />
                                </View>
                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }
                            </View>
                        </TouchableOpacity>
                        <View style={[styles.horizontalSeprator]}></View>
                    </View>
                </Swipeable>

            )
        }
        else if (item.Isactive && item.CommunityUserRoleID == 1 && item.CommunityStatusID != 1) {  //Setting(Admin for this community)
            return (
                <View style={[styles.vwItemParentPaddingStyle]}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID } })}>
                        <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                            <View style={styles.beforeimgView}>
                                <Image
                                    source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                    style={[styles.ivItemImageStyle]}
                                />
                            </View>
                            <View style={styles.middleViewTexts}>
                                <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                    {(isMemberCount) ?
                                        (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                            item.MemberCount + ' Members' : '0 Member'}</Text>
                                <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                            </View>
                            <View style={styles.lastImgViewEnd}>
                                <Image
                                    source={images.Communities.setting}
                                    style={styles.attende_shareIconStyle}
                                />
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.horizontalSeprator]}></View>

                </View>
            )
        }
        else if (item.Isactive == false && item.CommunityUserRoleID == 1) {// Inactive Community (You are the Admin)
            return (
                <Swipeable
                    leftButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 180 : 130}
                    leftButtons={[
                        <TouchableOpacity style={styles.onLeftStyle} onPress={() => { this._activateInactiveCommunity(item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.activeCommunity}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'ACTIVATE'}</Text>
                            </View>
                        </TouchableOpacity>
                    ]} rightButtons={null} rightContent={null}>
                    <View style={[styles.vwItemParentPaddingStyle, { opacity: 0.5 }]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID, itisFrom: "Inactive", activereview: item.Isactive, CommunityStatusID: item.CommunityStatusID } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                {
                                    (this.state.onSwipOutAccept) ? null : <View style={styles.leftGreenLine}></View>
                                }
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                                item.MemberCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                </View>
                                <View style={styles.lastImgViewEnd}>
                                    <Image
                                        source={images.Communities.setting}
                                        style={styles.attende_shareIconStyle}
                                    />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <View style={[styles.horizontalSeprator]}></View>
                    </View>
                </Swipeable>
            )
        }
        else if (item.Isactive == false && item.CommunityUserRoleID != 1) {          // Inactive Community (You are the member)
            return (
                <View style={[styles.vwItemParentPaddingStyle, { opacity: 0.5 }]}>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID, itisFrom: "Inactive", activereview: item.Isactive, CommunityStatusID: item.CommunityStatusID } })}>
                        <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                            <View style={styles.beforeimgView}>
                                <Image
                                    source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                    style={[styles.ivItemImageStyle]}
                                />
                            </View>
                            <View style={styles.middleViewTexts}>
                                <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                    {(isMemberCount) ?
                                        (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                            item.MemberCount + ' Members' : '0 Member'}</Text>
                                <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                            </View>
                            <View style={styles.lastImgViewEnd}>
                                <Text style={[styles.attendeeTextStyle, { color: colors.warmBlue }]}>{'Inactive'}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={[styles.horizontalSeprator]}></View>
                </View>
            )
        }
        else if ((item.Isactive == false || item.CommunityStatusID != 1 || item.CommunityStatusID != 5) && item.CommunityUserStatus == 1) {  //Waiting for approval
            return (
                <Swipeable leftButtons={null} leftContent={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleCancelModalStatusApproval(true, item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.cancelApproval}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'CANCEL'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID, itisFrom:'Waiting_approval' } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                                item.MemberCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                </View>
                                <View style={styles.lastImgViewEnd}>
                                    <Image
                                        source={images.Communities.waitingApproval}
                                        style={styles.attende_shareIconStyle}
                                    />
                                </View>
                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }

                            </View>
                        </TouchableOpacity>
                        <View style={[styles.horizontalSeprator]}></View>
                    </View>
                </Swipeable>
            )
        }
        else if ((item.Isactive == true || item.CommunityStatusID == 1) && item.CommunityUserStatus == 2) {  //Leave  communityl
            return (
                <Swipeable
                    leftContent={null}
                    leftButtons={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleLaeaveCommunityModalStatus(true, item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.LeaveCommunity}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'LEAVE'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                                item.MemberCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                </View>
                                <View style={styles.lastImgViewEnd}>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.warmBlue }]}>{'LEAVE'}</Text>
                                </View>
                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }
                            </View>
                            <View style={[styles.horizontalSeprator]}></View>
                        </TouchableOpacity>
                    </View>
                </Swipeable>
            )
        }
        else if (item.Isactive == true && item.CommunityUserStatus == 4) {    //community admin sent request to join community. Accept reject view
            return (
                <Swipeable
                    leftContent={null}
                    leftButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    leftButtons={[
                        <TouchableOpacity style={styles.onLeftStyle} onPress={() => { this._acceptRequestInvitation(item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.done.donebtn}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'ACCEPT'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}
                    rightContent={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this._rejectRequestInvitation(item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.cancelApproval}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'REJECT'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                {
                                    (this.state.onSwipOutAccept) ? null : <View style={styles.leftGreenLine}></View>
                                }
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.LogoWebPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MemberCount <= 1) ? item.MemberCount + ' Member' :
                                                item.MemberCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                </View>

                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }
                            </View>
                            <View style={[styles.horizontalSeprator]}></View>
                        </TouchableOpacity>
                    </View>
                </Swipeable>
            )
        }
    }

    renderFooter = () => {
        if (this.state.loadingPagination) {
            if (this.state.responseComes) {
                return (
                    <ActivityIndicator
                        style={styles.loaderbottomview} size="large" color={colors.bgColor} />
                )
            }

        } else {
            return (
                <View />
            )
        }
    };

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.apiCallMyCommunities();
        });
    }

    render() {
        const { serverErr, loading, communitiesData, responseComes, loadingPagination, isInternetFlag, searchBtnClick } = this.state;
        uniquecommunityData = this.state.communitiesData.filter((ele, ind) => ind === this.state.communitiesData.findIndex(elem => elem.CommunityID === ele.CommunityID && elem.id === ele.id))

        return (
            <View style={styles.mainParentStyle}>
                <NavigationEvents
                    onWillFocus={() => this.apiCallMyCommunities()}
                    onWillBlur={() => this.clearStates()}
                />
                {this.renderSearchModel()}
                {this.renderLeaveCommunityModal()}
                {this.renderCancelWaitingReviewModal()}
                {this.renderCancelWaitingApprovalModal()}
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (
                                (responseComes == false && loadingPagination == true) ?
                                    <View></View> :
                                    (communitiesData.length == 0 && responseComes) ?
                                        <View style={globalStyles.nodataStyle}>
                                            <Text style={globalStyles.nodataTextStyle}>{(!searchBtnClick) ? globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.MYCOMMUNITIES_DATA_NOT_AVLB : globals.SEARCHNODATAFOUND.MYCOMMUNITY_SEARCH}</Text>
                                        </View> :
                                        <View style={styles.mainParentStyle}>
                                            <FlatList
                                                style={{
                                                    flex: 1,
                                                    marginBottom: globals.screenHeight * 0.06,
                                                    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null,
                                                }}
                                                showsVerticalScrollIndicator={false}
                                                data={uniquecommunityData}
                                                renderItem={({ item, index }) => this.renderMyCommunities(item, index)}
                                                extraData={this.state}
                                                bounces={false}
                                                keyExtractor={(index, item) => item.toString()}
                                                ListFooterComponent={this.renderFooter}
                                                onEndReached={this.handleLoadMore}
                                                onEndReachedThreshold={0.2}
                                                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                            />
                                        </View>
                            )
                            :
                            (
                                <ServerError loading={loading} onPress={() => this._tryAgain()} />
                            )
                }
                <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.props.navigation.navigate('CreateCommunity', { isFrom: "MyCommunity" })}>
                    <Text style={styles.tvSearchStyle}>{'CREATE COMMUNITY'}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Communities);
