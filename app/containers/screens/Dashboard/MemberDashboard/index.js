/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable prettier/prettier */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-console */
/* eslint-disable no-plusplus */
/* eslint-disable eqeqeq */
/* eslint-disable no-empty */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable react/jsx-no-bind */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, SafeAreaView, FlatList, TouchableWithoutFeedback, Image, Linking, TouchableOpacity, Platform } from 'react-native';
import PushNotification from 'react-native-push-notification';
import BackgroundTimer from 'react-native-background-timer';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import { API } from '../../../../utils/api';
import styles from './style';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as images from '../../../../assets/images/map';
import SideMenu from '../../SideMenu/index';

const iPad = DeviceInfo.getModel()
let _this = null;

let TAG = "member dashboard:=="
export default class events extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProDrawerButton(navigation, 'Member Dashboard'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      GridViewItems: [
        {
          key: 'Communities',
          icon: (images.MemberDashboard.Communities),
          count: 0,
        },
        {
          key: 'Groups',
          icon: (images.MemberDashboard.Groups),
          count: 0,
        },
        {
          key: 'Events',
          icon: (images.MemberDashboard.Events),
          count: 0
        },
        {
          key: 'Connections',
          icon: (images.MemberDashboard.Connections),
          count: 0,
        },
        {
          key: 'Endorsements',
          icon: (images.MemberDashboard.Endorsements),
          count: 0,
        },
        {
          key: 'Messages',
          icon: (images.MemberDashboard.Messages),
          count: 0,
        },
        {
          key: 'Matches',
          icon: (images.MemberDashboard.Matches),
          count: 0,
        },
        {
          key: 'Manage Emails',
          icon: (images.MemberDashboard.Manage_Emails),
          count: 0,
        },
      ],
      url: '',
      communitiesCount: 0,
      groupsCount: 0,
      eventsCount: 0,
      connectionsCount: 0,
      endorsementCount: 0,
      messgesCount: 0,
      matchesCount: 0,
      emailsCount: 0,
      dashboardCountData: {}
    };
  }

  componentDidMount() {
    Linking.addEventListener('url', this.handleNavigation);
    Linking.getInitialURL().then((url) => {
      if (url) {
        this.handleNavigation({ url: url })
      }
    })
    API.getMemberDashboardCount(this.GetMemberDashboardCountResponseData, globals.userID, true);
    if (Platform.OS == 'android') {
      PushNotification.configure({
        onNotification: function (notification) {
          _this.manageNav(notification.userInfo.id)

        },
      });
    }
    else {
      PushNotification.configure({
        onNotification: function (notification) {
          _this.manageNav(notification.data.id)

        },
      });
    }

    //this.makeAPICall()
  }

  manageNav(id) {
    if (id == "EndorsementReq") {
      _this.props.navigation.navigate("EndorsementsTab")
    }
    else if (id == "connectionReq") {
      _this.props.navigation.navigate("Connections");
    }
    else if (id == "MessagesReq") {
      _this.props.navigation.navigate("MessagesThread");
    }
    else {

    }
  }

  /**
   * for highlight side menu btn's
   */
  beforeMakeApiCall() {
    SideMenu.onbackPressChange()
    this.makeAPICall()
  }

  makeAPICall() {
    Linking.addEventListener('url', this.handleNavigation);
    Linking.getInitialURL().then((url) => {
      if (url) {
        this.handleNavigation({ url: url })
      }
    })
    
    BackgroundTimer.runBackgroundTimer(() => {
      if (globals.isInternetConnected === true) {
        API.getMemberDashboardCount(this.GetMemberDashboardCountResponseData, globals.userID, true);
      }
    }, 120000)
  }
  
  /**EmailCount_All
   * Response callback of GetSilmilarMatchesInterestResponseData
   */
  GetMemberDashboardCountResponseData = {
    
    success: response => {
      console.log(
        TAG,
        'GetMemberDashboardCountResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
       
        //Logic for get previous count from async and compare it with response

        AsyncStorage.multiGet(['connectionsCount', 'endorsementCount', 'messgesCount']).then((value) => {
          let connectionsCount = JSON.parse(value[0][1]);
          let msgsCount = JSON.parse(value[2][1]);
          let endorsementCount = JSON.parse(value[1][1]);

          let connectNotificationSwitch, endorsementNotificationSwitch, msgsNotificationSwitch;
          AsyncStorage.getItem(globals.ENDORSENOTIFICATIONSTATUS).then((value) => {
            endorsementNotificationSwitch = value;
            if (value != null && value != undefined && value != "") {
              endorsementNotificationSwitch = JSON.parse(value)
              if (endorsementNotificationSwitch) {
                if (endorsementCount != response.Data.EndroseCount) {
                  if (response.Data.EndroseCount != 0) {
                    PushNotification.localNotification({
                      message: 'You have a endorsement request from ' + response.Data.EndroseCount + ' members',
                      userInfo: { id: 'EndorsementReq' },
                      priority: 'high',
                      id: '123',
                    });
                  }
                }
              }
            }
          })
          AsyncStorage.getItem(globals.CONNECTIONNOTIFICATIONSTATUS).then((value) => {
            if (value != null && value != undefined && value != "") {
              connectNotificationSwitch = JSON.parse(value)
              if (connectNotificationSwitch) {
                if (connectionsCount != response.Data.ConnectionsCount) {
                  if (response.Data.ConnectionsCount != 0) {
                    PushNotification.localNotification({
                      message: 'You have a connection request from ' + response.Data.ConnectionsCount + ' members',
                      userInfo: { id: 'connectionReq' },
                      priority: 'high',
                      id: '123',
                    });
                  }
                }
              }
            }
          })
          AsyncStorage.getItem(globals.MESSAGESNOTIFICATIONSTATUS).then((value) => {

            if (value != null && value != undefined && value != "") {
              msgsNotificationSwitch = JSON.parse(value)
              if (msgsNotificationSwitch) {

                if (msgsCount != response.Data.MessagesUnreadCount) {
                  if (response.Data.MessagesUnreadCount != 0) {
                    PushNotification.localNotification({
                      message: 'You have ' + response.Data.MessagesUnreadCount + ' unread messages',
                      userInfo: { id: 'MessagesReq' },
                      priority: 'high',
                      id: '123',
                      // date,
                    });
                  }
                }
              }
            }
          })
        })

        setTimeout(() => {
          this.setState({
            dashboardCountData: response.Data, eventsCount: response.Data.EventsCount,
            groupsCount: response.Data.GroupsCount, communitiesCount: response.Data.CommunitiesCount,
            connectionsCount: response.Data.ConnectionsCount, endorsementCount: response.Data.EndroseCount,
            messgesCount: response.Data.MessagesUnreadCount, emailsCount: response.Data.SentEmailCount, matchesCount: response.Data.SimilarInterestsCount
          }, () => {
  
            let multi_set_pairs = [
              ['connectionsCount', JSON.stringify(this.state.connectionsCount)],
              ['endorsementCount', JSON.stringify(this.state.endorsementCount)],
              ['messgesCount', JSON.stringify(this.state.messgesCount)],
            ];
  
            AsyncStorage.multiSet(multi_set_pairs, (err) => {
            })
  
            // AsyncStorage.setItem(globals.DASHBOARDMODULESCOUNT, dashboardModulesCount);
            this.setState({
              GridViewItems: [
                {
                  key: 'Communities',
                  icon: (images.MemberDashboard.Communities),
                  count: this.state.communitiesCount,
                },
                {
                  key: 'Groups',
                  icon: (images.MemberDashboard.Groups),
                  count: this.state.groupsCount,
                },
                {
                  key: 'Events',
                  icon: (images.MemberDashboard.Events),
                  count: this.state.eventsCount
                },
                {
                  key: 'Connections',
                  icon: (images.MemberDashboard.Connections),
                  count: this.state.connectionsCount,
                },
                {
                  key: 'Endorsements',
                  icon: (images.MemberDashboard.Endorsements),
                  count: this.state.endorsementCount,
                },
                {
                  key: 'Messages',
                  icon: (images.MemberDashboard.Messages),
                  count: this.state.messgesCount,
                },
                {
                  key: 'Matches',
                  icon: (images.MemberDashboard.Matches),
                  count: this.state.matchesCount,
                },
                {
                  key: 'Manage Emails',
                  icon: (images.MemberDashboard.Manage_Emails),
                  count: this.state.emailsCount,
                },
              ]
            })
          })
        }, 1000);
      }
    },
    error: err => {

    },
    complete: () => {

    },
  };



  handleNavigation = (event) => {
    console.log("handleNavigation called")

    AsyncStorage.getItem('@isLogin').then(result => {
      if (result !== null) {
        if (event.url != undefined) {
          console.log("event.url-->",event.url)
          let url = event.url.toString();
          if (url.includes("event/details")) {
            var lastPart = url.split("/").pop();
            console.log("event.url lastPart-->",lastPart)
            setTimeout(() => {
              this.props.navigation.navigate("GLOBAL_EVENT_SCREEN_DASHBOARD", {itisFrom:'deeplink', selectedEventListDeepLink: parseInt(lastPart, 10) });
            }, 1000);
          }
          else if (url.includes("community/info")) {
            var lastPart = url.split("/").pop();
            setTimeout(() => {
              this.props.navigation.navigate("CommunityDashboard", {itisFrom:'deeplinkCommunity', dataglobalCommunityList: { CommunityId: lastPart } });
            }, 1000);
          }
        }
      } else {
        this.props.navigation.navigate('Auth');
      }
    });

  }

  /**
     * method for Select Particular Item
     */
  setSelection(item, index) {
    const { GridViewItems } = this.state;
    const dataCopy = GridViewItems;
    console.log('indexItem : ', `${JSON.stringify(dataCopy)}index${index}`);
    for (let i = 0; i < dataCopy.length; i++) {
      if (index == i && item.key == 'Communities') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("Communities")

      }
      else if (index == i && item.key == 'Groups') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("Groups")
      }
      else if (index == i && item.key == 'Events') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("MYEVENTS_SCREEN")
      }
      else if (index == i && item.key == 'Connections') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("Connections");
      }
      else if (index == i && item.key == 'Endorsements') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("EndorsementsTab")
      }
      else if (index == i && item.key == 'Messages') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("MessagesThread")
        
      }
      else if (index == i && item.key == 'Matches') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("Matches")
      }
      else if (index == i && item.key == 'Manage Emails') {
        dataCopy[index].isSelected = true;
        this.props.navigation.navigate("ManageEmails")
      }
      else {
        dataCopy[i].isSelected = false;
      }
    }
    this.setState({ GridViewItems: dataCopy }, () => {
    });
  }

  /**
     * Render FlatList Items
     */
  _renderItems(item, index) {
    return (
      <View style={styles.gridViewStyle}>
        <TouchableWithoutFeedback onPress={() => { this.setSelection(item, index) }}>
          {item.isSelected == true ? (
            <View style={styles.gridViewBlockStyle}>
              <Image source={item.icon} style={styles.imgStyle} resizeMode={'contain'} />
              <View style={styles.blueCounterView}>
                <Text style={styles.countTxtStyle}>{item.count}</Text>
              </View>
              <Text numberOfLines={1} style={styles.gridViewInsideTextItemStyle}> {item.key} </Text>
            </View>
          ) : (
              <View style={styles.unSelectedgridViewBlockStyle}>
                <Image source={item.icon} style={styles.unSelectedimgStyle} resizeMode={'contain'} />
                <View style={styles.blueCounterView}>
                  <Text style={styles.countTxtStyle}>{item.count}</Text>
                </View>
                <Text numberOfLines={1} style={styles.unSelectedgridViewInsideTextItemStyle}> {item.key} </Text>
              </View>
            )}
        </TouchableWithoutFeedback>
      </View>
    );
  }

  componentWillUnmount() {
     Linking.removeEventListener('url', this.handleNavigation);
  }

  render() {
    const { GridViewItems } = this.state;
    return (
      <SafeAreaView style={styles.safeViewStyle}>
        <View style={styles.mainContainer}>
          <NavigationEvents
            onWillFocus={() => this.beforeMakeApiCall()}
          />
          <FlatList
            style={{ flex: 1, marginTop: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.030 : globals.screenHeight * 0.019 }}
            data={GridViewItems}
            renderItem={({ item, index }) => this._renderItems(item, index)}
            numColumns={2}
            keyExtractor={(x, i) => i.toString()}
            extraData={this.state}
            bounces={false}
            showsVerticalScrollIndicator={false}
          />
        </View>
        {/* <PushController /> */}
      </SafeAreaView>
    );
  }
}
