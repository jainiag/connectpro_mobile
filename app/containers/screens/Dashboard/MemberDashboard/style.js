import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
let {screenHeight,screenWidth} = globals;
module.exports = StyleSheet.create({
  //= ================ Event Screen DashBorad====================//
  safeViewStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  scrollViewStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  mainContainer: {
    flex: 1,
  },
  gridViewStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: screenHeight * 0.015,
  },
  gridViewBlockStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.19,
    width: screenHeight * 0.19,
    backgroundColor: colors.bgColor,
    borderRadius: (screenHeight * 0.19) / 2,
  },
  unSelectedgridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.19,
    width: screenHeight * 0.19,
    backgroundColor: colors.white,
    borderRadius: (screenHeight * 0.19) / 2,
  },
  gridViewInsideTextItemStyle: {
    paddingTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.005,
    fontSize: globals.font_14,
    justifyContent: 'center',
    color: colors.white,
    alignItems: 'center',
  },
  unSelectedgridViewInsideTextItemStyle: {
    marginTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.005,
    fontSize: globals.font_14,
    justifyContent: 'center',
    color: colors.bgColor,
    alignItems: 'center',
  },
  iconStyle: {
    alignSelf: 'center',
  },
  unSelectedimgStyle: {
    height: screenHeight * 0.0473, //25
    width: screenHeight * 0.0473, //25,
    tintColor: colors.bgColor,
  },
  imgStyle: {
    height: screenHeight * 0.0473, //25,
    width: screenHeight * 0.0473, //25,
    tintColor: colors.white,
  },
  blueCounterView: {
    height: (globals.screenHeight * 0.04), 
    width: (globals.screenHeight * 0.04),
    borderRadius:(globals.screenHeight * 0.04 / 2),
    position: 'absolute', 
    //zIndex: 10, 
    top: (globals.screenWidth*0.05),
    right: (globals.screenWidth*0.08),
    backgroundColor:colors.listSelectColor,
    alignItems:'center',
    justifyContent:'center'
  },
  countTxtStyle:{
    color:colors.white,
    fontSize:globals.font_12
  }
});
