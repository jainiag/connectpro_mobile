import React, { Component } from 'react';
import PushNotification from 'react-native-push-notification';

export default class PushController extends Component {
  constructor(props){
    super(props);
    this.state={

    }
  }

  componentDidMount() {
    PushNotification.configure({
      onNotification: function(notification) {
        console.log( 'NOTIFICATION:', notification );
        this.props.navigation.navigate("EndorsementsTab")
      },
    });
  }

  render() {
    return null;
  }
}
