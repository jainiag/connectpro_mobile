import React from 'react';
import { Text, View, Image, FlatList, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style'
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as images from '../../../../assets/images/map';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../components/NoInternet/index'
import ServerError from '../../../../components/ServerError/index'

let TAG = "EndrosementsSent:::==="
const iPad = DeviceInfo.getModel();

class EndrosementsSent extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Endorsements'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });


  constructor(props) {
    super(props);
    this.state = {
      endrosmentSentData: [],
      loading: false,
      isInternetFlag: true,
      serverErr: false,
      responseComes: false,
    }
  }

  componentDidMount() {
    this.props.showLoader()
    this.apicallSentEndrosments()
  }

  /**
  * APi call for get sent endrosment list
  */
  apicallSentEndrosments() {
    if (globals.isInternetConnected === true) {
      this.setState({ loading: true, isInternetFlag: true })
      API.getEndrosments(this.GetEndrsomentsResponseData, "submitted", globals.userID, true)
    } else {
      this.props.hideLoader();
      this.setState({ isInternetFlag: false })
    }
  }

  /**
* Response of sent endrosment list
*/
  GetEndrsomentsResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetEndrsomentsResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.setState({ endrosmentSentData: response.Data, loading: false })

      } else {
        Alert.alert(globals.appName, response.Message)
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true, loading: false });
      console.log(
        TAG,
        'GetEndrsomentsResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.setState({ loading: false })
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
 *  call when _sessionOnPres
 */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  _tryAgain() {
    this.setState({ serverErr: false, responseComes: false }, () => {
      this.props.showLoader();
      this.apicallSentEndrosments();
    });
  }

  /**
   * render endorsement sent list
   * @param {*} item 
   * @param {*} index 
   */
  renderEndrosmentSentList(item, index) {
    const isReceiverName = globals.checkObject(item, 'ReceiverName');
    const isReceiverOrganization = globals.checkObject(item, 'ReceiverOrganization');
    const isReceiverJobTitle = globals.checkObject(item, 'ReceiverJobTitle')
    const isReceiverProfilePic = globals.checkImageObject(item, 'ReceiverProfile');
    const isContent = globals.checkObject(item, 'Content')
    return (
      <View>
        <View style={styles.matches_ChildViewContainer}>
          <View style={styles.userinfoStyle}>
            <View style={[styles.beforeimageview, {marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?globals.screenHeight*0.014 : globals.screenHeight*0.01, marginLeft: (globals.screenWidth * 0.01) }]}>
              <Image
                source={{ uri: (isReceiverProfilePic) ? item.ReceiverProfile : globals.User_img }}
                style={[styles.imgStyle]}
              ></Image>
            </View>
            <View style={styles.textViewStyle}>
              <Text numberOfLines={1} style={styles.usernametextStyle}>
                {(isReceiverName) ? item.ReceiverName : '-'}
              </Text>
              <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                {(isReceiverJobTitle) ? item.ReceiverJobTitle : '-'}
              </Text>
              <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                {(isReceiverOrganization) ? item.ReceiverOrganization : "-"}
              </Text>
            </View>

          </View>
          <View style={styles.grayLineView} />
          <View style={styles.hobbytextStyle}>
            <Text style={styles.hobbyTextstyle}>
              {(isContent) ? item.Content : "-"}
            </Text>
            <View style={styles.lastDateMainView}>
              <Image source={images.Endorsement.clock} style={styles.clockImgStyle} />
              <Text style={styles.createdDateTxt}>{item.CreatedDateStr}</Text>
            </View>
          </View>
        </View>
      </View>
    )
  }


  render() {
    const { serverErr, loading, endrosmentSentData, responseComes, isInternetFlag } = this.state;
    return (
      <View style={styles.mainParentStyle}>
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            (serverErr === false) ?
              (
                (loading == true) ? null :
                  (endrosmentSentData.length == 0) ?
                    <View style={globalStyles.nodataStyle}>
                      <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.ENDORSEMENTSENT_DATA_NOT_AVLB}</Text>
                    </View> :
                    <View style={styles.mainParentStyle}>
                      <View style={styles.backgroundStyle}></View>
                      <View style={styles.matchesMainView}></View>
                      <FlatList
                        style={{ flex: 1, marginTop: globals.screenHeight * 0.03 }}
                        showsVerticalScrollIndicator={false}
                        data={endrosmentSentData}
                        renderItem={({ item, index }) => this.renderEndrosmentSentList(item, index)}
                        extraData={this.state}
                        bounces={false}
                        keyExtractor={(index, item) => item.toString()}
                      />
                    </View>
              )
              :
              <ServerError loading={loading} onPress={() => this._tryAgain()} />
        }
      </View>
    )
  }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EndrosementsSent);

