import React from 'react';
import { Text, View, Image, FlatList, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style'
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as colors from '../../../../assets/styles/color';
import * as images from '../../../../assets/images/map';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import Swipeout from '../../../../libs/react-native-swipeout';
import DeviceInfo from 'react-native-device-info';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../components/NoInternet/index'
import ServerError from '../../../../components/ServerError/index'

let TAG = "EndrosementsReceived:::==="
const iPad = DeviceInfo.getModel();
class EndrosementsReceived extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Endorsements'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });


    constructor(props) {
        super(props);
        this.state = {
            endrosmentSentData: [],
            loading: false,
            isInternetFlag: true,
            serverErr: false,
            responseComes: false,
        }
    }

    componentDidMount() {
        this.props.showLoader()
        this.apicallSentEndrosments()
    }

    /**
    * APi call for get sent endrosment list
    */
    apicallSentEndrosments() {
        if (globals.isInternetConnected === true) {
            this.setState({ loading: true, isInternetFlag: true })
            API.getEndrosments(this.GetEndrsomentsResponseData, "received", globals.userID, true)
        } else {
            this.props.hideLoader();
            this.setState({ isInternetFlag: false })
        }
    }

    /**
  * Response of sent endrosment list
  */
    GetEndrsomentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetEndrsomentsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({ endrosmentSentData: response.Data })

            } else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetEndrsomentsResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
   *  call when _sessionOnPres
   */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.apicallSentEndrosments();
        });
    }

    clearStates() {
        this.setState({
            endrosmentSentData: [],
            loading: false,
            isInternetFlag: true,
            serverErr: false,
            responseComes: false,
        }, () => {
            this.apicallSentEndrosments()
        })
    }

    /**
     * Method of API call of Accept endorsement API
     * @param {*} endorsementId 
     * @param {*} actionType 
     */
    acceptAPICall(endorsementId, actionType) {
        if (globals.isInternetConnected == true) {
            API.acceptRejectEndrosement(this.AcceptRejectEndrosementResponseData, endorsementId, globals.userID, actionType, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
    * Method of API call of Reject endorsement API
    * @param {*} endorsementId 
    * @param {*} actionType 
    */
    rejectAPICall(endorsementId, actionType) {
        if (globals.isInternetConnected == true) {
            API.acceptRejectEndrosement(this.AcceptRejectEndrosementResponseData, endorsementId, globals.userID, actionType, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
     * Response of Accept Reject Invitation 
     */
    AcceptRejectEndrosementResponseData = {
        success: response => {
            console.log(
                TAG,
                'AcceptRejectEndrosementResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.clearStates()
            }
            else {
                this.setState({ loading: false })
                Alert.alert(globals.appName, response.Message);
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'AcceptRejectEndrosementResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
     * Render Endorsements ob based on different status
     * @param {*} item 
     * @param {*} index 
     */
    renderEndrosmentSentList(item, index) {
        const isSenderName = globals.checkObject(item, 'SenderName');
        const isSenderOrganization = globals.checkObject(item, 'SenderOrganization');
        const isSenderJobTitle = globals.checkObject(item, 'SenderJobTitle')
        const isSenderProfilePic = globals.checkImageObject(item, 'SenderProfile');
        const isContent = globals.checkObject(item, 'Content')
        var swipeoutRightBtns = [
            {
                data: item,
                backgroundColor: colors.darkRed,
                component: <View style={styles.imgcontainer}>
                    <Image
                        source={images.colse.closebtn}
                        style={styles.crossimgesStyle}
                        resizeMode="contain" />
                </View>,
                underlayColor: colors.lightBlue,
                onPress: () => this.rejectAPICall(item.ID, "Rejected")
            }
        ]

        var swipeoutLeftBtns = [
            {
                data: item,
                backgroundColor: colors.greenDone,
                component: <View style={styles.imgcontainer}>
                    <Image
                        source={images.done.donebtn}
                        style={styles.imgesStyle}
                        resizeMode="contain" />
                </View>,
                underlayColor: colors.lightBlue,
                onPress: () => this.acceptAPICall(item.ID, "Accepted")

            }
        ]
        if (item.StatusID == 1) {
            return (
                <>
                    <View style={styles.matches_ChildViewContainer}>
                        <Swipeout
                            right={swipeoutRightBtns}
                            left={swipeoutLeftBtns}
                            rowId={item.id}
                            style={styles.swipeOutView}
                        >
                            <View style={[styles.swipeOutView, { width: globals.screenWidth * 0.85 }]}>
                                <View style={styles.leftGreenLine}></View>
                                <View style={{ marginVertical: (globals.screenWidth * 0.025), flexDirection: 'row' }}>
                                    <View style={styles.beforeimageview}>
                                        <Image
                                            source={{ uri: (isSenderProfilePic) ? item.SenderProfile : globals.User_img }}
                                            style={styles.imgStyle}
                                        ></Image>
                                    </View>
                                    <View style={styles.textViewStyleSwipeOut}>
                                        <Text numberOfLines={1} style={styles.usernametextStyle}>
                                            {(isSenderName) ? item.SenderName : '-'}
                                        </Text>
                                        <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                                            {(isSenderJobTitle) ? item.SenderJobTitle : '-'}
                                        </Text>
                                        <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                                            {(isSenderOrganization) ? item.SenderOrganization : "-"}
                                        </Text>
                                    </View>
                                </View>
                                <View style={[styles.rightRedLine, {
                                    alignContent: 'flex-end',
                                    marginLeft:
                                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?
                                            globals.screenWidth * 0.04888 :
                                            2
                                }]}></View>
                            </View>
                        </Swipeout>

                        <View style={styles.grayLineView} />
                        <View style={styles.hobbytextStyle}>
                            <Text style={styles.hobbyTextstyle}>
                                {(isContent) ? item.Content : "-"}
                            </Text>
                            <View style={styles.lastDateMainView}>
                                <Image source={images.Endorsement.clock} style={styles.clockImgStyle} />
                                <Text style={styles.createdDateTxt}>{item.CreatedDateStr}</Text>
                            </View>
                        </View>
                    </View>
                </>
            )
        } else if (item.StatusID == 2) {
            return (
                <>
                    <View style={styles.matches_ChildViewContainer}>
                        <View style={[styles.swipeOutView]}>
                            <View style={{ marginVertical: (globals.screenWidth * 0.025), flexDirection: 'row', flex: 1 }}>
                                <View style={styles.beforeimageview}>
                                    <Image
                                        source={{ uri: (isSenderProfilePic) ? item.SenderProfile : globals.User_img }}
                                        style={styles.imgStyle}
                                    ></Image>
                                </View>
                                <View style={styles.textViewStyle}>
                                    <Text numberOfLines={1} style={styles.usernametextStyle}>
                                        {(isSenderName) ? item.SenderName : '-'}
                                    </Text>
                                    <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                                        {(isSenderJobTitle) ? item.SenderJobTitle : '-'}
                                    </Text>
                                    <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                                        {(isSenderOrganization) ? item.SenderOrganization : "-"}
                                    </Text>
                                </View>
                                <View style={styles.lastStatusMainView}>
                                    <View style={styles.lastStatusView}>
                                        <Text style={styles.statusTextStyle}>{item.StatusText}</Text>
                                    </View>
                                </View>
                            </View>
                        </View>

                        <View style={styles.grayLineView} />
                        <View style={styles.hobbytextStyle}>
                            <Text style={styles.hobbyTextstyle}>
                                {(isContent) ? item.Content : "-"}
                            </Text>
                            <View style={styles.lastDateMainView}>
                                <Image source={images.Endorsement.clock} style={styles.clockImgStyle} />
                                <Text style={styles.createdDateTxt}>{item.CreatedDateStr}</Text>
                            </View>
                        </View>
                    </View>
                </>
            )
        } else {
            return null
        }

    }


    render() {
        const { serverErr, loading, endrosmentSentData, responseComes, isInternetFlag } = this.state;

        return (
            <View style={styles.mainParentStyle}>
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (
                                (loading == true) ? null :
                                    (endrosmentSentData.length == 0) ?
                                        <View style={[globalStyles.nodataStyle]}>
                                            <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.ENDORSEMENTRECEIVED_DATA_NOT_AVLB}</Text>
                                        </View> :
                                        <View style={styles.mainParentStyle}>
                                            <View style={styles.backgroundStyle}></View>
                                            <View style={styles.matchesMainView}></View>
                                            <FlatList
                                                style={{ flex: 1, marginTop: globals.screenHeight * 0.03 }}
                                                showsVerticalScrollIndicator={false}
                                                data={endrosmentSentData}
                                                renderItem={({ item, index }) => this.renderEndrosmentSentList(item, index)}
                                                extraData={this.state}
                                                bounces={false}
                                                keyExtractor={(index, item) => item.toString()}
                                            />
                                        </View>
                            )
                            :
                            <ServerError loading={loading} onPress={() => this._tryAgain()} />
                }
            </View>
        )
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EndrosementsReceived);

