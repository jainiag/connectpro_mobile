/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import globalStyles from '../../../../assets/styles/globleStyles';
import { ScrollableTabView, ScrollableTabBar } from '../../../../libs/react-native-scrollable-tabview'
import EndrosementsSent from './endrosementsSent';
import EndrosementsReceived from './endrosmentReceived';

const iPad = DeviceInfo.getModel();


export default class EndorsementsTab extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'My Endorsements'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });


  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      isSearh: false,
      selectedTab: 1,
      index: 0,
      routes: [
        { key: 'Received', title: 'Received' },
        { key: 'Sent', title: 'Sent' },
      ],
    };
  }


  render() {
    return <ScrollableTabView
      style={{ marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.01 }}
      initialPage={0}
      tabBarTextStyle={{ fontSize: globals.font_16 }}
      tabBarInactiveTextColor={colors.black}
      tabBarActiveTextColor={colors.warmBlue}
      renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
    >
      <EndrosementsReceived tabLabel={'Received'} navigationProps={this.props} />
      <EndrosementsSent tabLabel={'Sent'} navigationProps={this.props} />
    </ScrollableTabView>
  }
}

