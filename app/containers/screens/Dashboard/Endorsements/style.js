import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
    mainParentStyle: {
        flex: 1,
    },
    tvTitleStyle: {
        fontSize: globals.font_20,
        color: 'blue',
    },
    backgroundStyle: {
        width: globals.screenWidth,
        height: globals.screenHeight * 0.20,
        backgroundColor: colors.warmBlue,
        alignItems: 'center'
    },
    headertextStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_16,
        fontWeight: '500',
        paddingVertical: globals.screenHeight * 0.03,
        textAlign: 'center'
    },

    matches_ChildViewContainer: {
        backgroundColor: colors.white,
        overflow:"hidden",
        width: globals.screenWidth * 0.85,
        //  flex: 1,
        // height:globals.screenWidth * 0.85,
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5,
        marginLeft: globals.screenWidth * 0.074,
        borderWidth: 0.7,
        borderColor: colors.gray,
        borderRadius: 5,
        marginBottom: globals.screenHeight * 0.02,
        paddingBottom: globals.screenHeight * 0.02,
    },
    matchesMainView: {
        alignItems: 'center',
        marginTop: globals.screenHeight * (-0.15),
        zIndex: 999,
        // backgroundColor:'red',
        top: globals.screenHeight * 0.08,
    },
    userinfoStyle: {
        flexDirection: 'row',
        //marginHorizontal: globals.screenWidth * 0.03,
        paddingHorizontal: globals.screenWidth * 0.02,
        paddingVertical: globals.screenWidth * 0.025,
    },
    beforeimageview: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        // marginVertical:5,
        borderRadius: (globals.screenWidth * 0.15) / 2,
        borderWidth: 0.1,
        borderColor: colors.lightGray,
        marginLeft: (globals.screenWidth * 0.03)
    },
    imgStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        borderRadius: (globals.screenWidth * 0.15) / 2,
        borderWidth: 0.2,
        borderColor: colors.lightGray,
    },
    nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    nodataTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
    },
    usernametextStyle: {
        // textAlign: 'center',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
    },
    textViewStyle: {
        marginHorizontal: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        flex: 1
    },

    textViewStyleSwipeOut: {
        alignContent: 'center',
        marginLeft: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        width: "70%",
    },

    proffesionTextStyle: {
        color: colors.lightBlack,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
    },
    grayLineView: {
        height: 1,
        alignSelf: 'center',
        width: "89%",
        backgroundColor: colors.proUnderline,
        marginVertical: globals.screenHeight * 0.01
    },
    hobbytextStyle: {
        //marginVertical:globals.screenWidth * 0.02,
        marginHorizontal: globals.screenWidth * 0.055,
        paddingHorizontal: globals.screenWidth * 0.02,
        paddingVertical: globals.screenWidth * 0.025,
        marginBottom: globals.screenWidth * 0.028,
    },
    hobbyTextstyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 21 : globals.font_14,
        color: colors.matteBlack
    },
    sideimgStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.04,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.04,
        position: 'absolute',
        top: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.18 : globals.screenHeight * 0.12,
        left: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? -24 : -16,
    },
    headerStyles: {
        marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.015 : 0
    },
    nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },

    //Recevied enorsenment 

    imgcontainer: {
        alignItems: 'center', justifyContent: 'center', flex: 1,
    },
    imgesStyle: {
        alignSelf: 'center',
        height: globals.screenHeight * 0.03,
        width: globals.screenHeight * 0.03
    },
    swipeOutView: {
        flexDirection: 'row',
        backgroundColor: colors.white,
    },
    leftGreenLine: {
        backgroundColor: colors.greenDone,
        width: globals.screenWidth * 0.015555,
    },
    rightRedLine: {
        backgroundColor: colors.darkRed,
        width: globals.screenWidth * 0.015555,

    },
    crossimgesStyle: {
        alignSelf: 'center',
        height: globals.screenHeight * 0.02,
        width: globals.screenHeight * 0.02
    },
    lastStatusMainView: {
        justifyContent: 'center', alignItems: 'flex-end', marginRight: 10, borderRadius: 3,
    },
    lastStatusView: {
        height: globals.screenHeight * 0.05, width: globals.screenWidth * 0.2, backgroundColor: colors.greenDone, justifyContent: 'center', alignItems: 'center', borderRadius: 3
    },
    statusTextStyle: {
        marginHorizontal: 3, color: colors.white, fontSize: globals.font_13
    },
    lastDateMainView: {
        flex: 1, flexDirection: 'row', marginTop: 10, alignItems: 'center'
    },
    clockImgStyle: {
        height: globals.screenHeight * 0.02, width: globals.screenHeight * 0.02
    },
    createdDateTxt: {
        fontSize: globals.font_12, color: colors.lightBlack, marginLeft: 5
    }
});
