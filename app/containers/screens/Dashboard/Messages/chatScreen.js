/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, KeyboardAvoidingView, Image, FlatList, TouchableWithoutFeedback, TextInput, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import styles from './style'
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as images from '../../../../assets/images/map';
import * as colors from '../../../../assets/styles/color';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import { API } from '../../../../utils/api';
import CustomButton from '../../../../components/CustomButton/index';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import MessageThreads from './messageThreads';

const iPad = DeviceInfo.getModel()
let TAG = "ChatScreen :::==="
let _this;
class ChatScreen extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Chat'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            loading: false,
            messagesList: [],
            serverErr: false,
            responseComes: false,
            memberId: this.props.navigation.state.params.MemberID
        }
    }

    componentDidMount() {
        this.makeApiCall()
    }

    makeApiCall() {
        this.props.showLoader();
        if (globals.isInternetConnected) {
            API.getChatMessages(this.GetChatMessagesResponse, globals.userID, this.state.memberId, true);
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    GetChatMessagesResponse = {
        success: response => {
            console.log(
                TAG,
                'GetChatMessagesResponse -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                this.setState({ messagesList: response.Data.reverse(), responseComes: true }, () => {
                    MessageThreads.makeApiCall()
                    // var result = this.state.messagesList.reverse((unique, o) => {
                    //     if (!unique.some(obj => obj.ID === o.ID)) {
                    //       unique.push(o);
                    //     }
                    //     return unique;
                    //   }, []);
                    //   console.log("msgslist--->", result)
                })
            }
            else {
                Alert.alert(globals.appName, response.Message);
                this.props.hideLoader()
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, responseComes: true });
            this.setState({ loading: false })
            console.log(
                TAG,
                'GetChatMembersResponse -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            }
            else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
 *  call when _sessionOnPres
 */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    /*
  * Try again method
  */
    _tryAgain() {
        this.setState({ loading: true, serverErr: false }, () => {
            this.makeApiCall();
        });
    }

    /**
    * method for setSelectionUpcomingEvent Particular Item
    */
    setSelectionUpcomingEvent(item, index) {
        const { messagesList } = this.state;
        const membersDataList = messagesList;
        for (let i = 0; i < membersDataList.length; i++) {
            if (index == i) {
                membersDataList[index].isSelected = true;
            } else {
                membersDataList[i].isSelected = false;
            }
        }
        this.setState({ messagesList: membersDataList }, () => { });
    }

    messagesChatRender(item, index) {
        const isSenderProfileLogo = globals.checkImageObject(item, 'SenderProfile');
        const isPReceiverProfileLogo = globals.checkImageObject(item, 'ReceiverProfile');
        const isContent = globals.checkObject(item, 'Content');
        const isCreatedDate = globals.checkObject(item, 'CreatedDateStr');

        return (
            <TouchableWithoutFeedback style={styles.mainParentStyle}>
                {
                    (globals.userID == item.SenderID) ?
                        <View style={styles.flatListStyle}>
                            <View style={styles.endFlexView}>
                                <View style={[styles.horizontalItemViewChat]}>
                                    <View style={styles.rightChatContentView}>
                                        <Text style={styles.rightContentTxt}>{(isContent) ? item.Content : ''}</Text>
                                        <Image
                                            source={images.Messages.rightArr}
                                            style={[styles.chatArrStyleRight]}
                                        />
                                    </View>
                                    <TouchableOpacity style={styles.beforeimgView} onPress={() => (item.SenderID == globals.userID) ?
                                        this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" })
                                        :
                                        this.props.navigation.navigate("OtherProfile", { User_ID: item.SenderID, item: item, isfrom: "chatScreen" })}>
                                        <Image
                                            source={{ uri: isSenderProfileLogo ? item.SenderProfile : globals.User_img }}
                                            style={[styles.ivItemImageStyle]}
                                        />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Text style={styles.leftDate}>{(isCreatedDate) ? item.CreatedDateStr : ''}</Text>
                        </View>
                        :
                        <View style={styles.flatListStyle}>
                            <View style={styles.startFlexView}>
                                <View style={[styles.horizontalItemViewChat]}>
                                    <TouchableOpacity style={[styles.beforeimgView, { marginRight: 10 }]} onPress={() => (item.SenderID == globals.userID) ?
                                        this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" })
                                        :
                                        this.props.navigation.navigate("OtherProfile", { User_ID: item.SenderID, item: item, isfrom: "chatScreen" })}>
                                        <Image
                                            source={{ uri: isPReceiverProfileLogo ? item.SenderProfile : globals.User_img }}
                                            style={[styles.ivItemImageStyle]}
                                        />
                                    </TouchableOpacity>
                                    <View style={styles.leftChatContentView}>
                                        <Image
                                            source={images.Messages.leftArr}
                                            style={[styles.chatArrStyle]}
                                        />
                                        <Text style={styles.leftViewContentTxt}>{(isContent) ? item.Content : ''}</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={styles.rightDate}>{(isCreatedDate) ? item.CreatedDateStr : ''}</Text>
                        </View>
                }
            </TouchableWithoutFeedback>
        )
    }

    sendPostMessage() {
        this.setState({ msgType: '' })
        this.checkisvalidmsgtype(this.state.msgType)
    }

    /**
        * check white space in starting only
        */
    checkisvalidmsgtype(msgType) {
        let regex = /^[^\s]+(\s+[^\s]+)*$/;
        if (msgType == '' || msgType == null) {
            Alert.alert(globals.appName, "The message content is required.",
                [{ text: 'OK', onPress: () => this.setState({ msgType: '' }) }])
        }
        else if (!regex.test(msgType)) {
            Alert.alert(globals.appName, "The message content is required.",
                [{ text: 'OK', onPress: () => this.setState({ msgType: '' }) }])
        } else {
            const data = {
                ID: 0,
                SenderID: globals.userID,
                ReceiverID: this.state.memberId,
                Content: this.state.msgType,
                UserAccessToken: JSON.parse(globals.tokenValue)
            };
            API.postChatMessage(this.postMessageResponseData, data, true);
        }
    }

    /**
    * API getchangePass response
    **/
    postMessageResponseData = {
        success: response => {
            if (response.StatusCode == 200) {
                console.log(TAG, 'postMessageResponseData response==', response);
                API.getChatMessages(this.GetChatMessagesResponse, globals.userID, this.state.memberId, true);
            } else {
                Alert.alert(globals.appName, response.Message);
                this.setState({ loading: false });
            }
        },

        error: err => {
            console.log(TAG, `postMessageResponseData responseERROR== ${JSON.stringify(err.message)}`);
            Alert.alert(globals.appName, err.Message);
            this.setState({ loading: false });
        },
        complete: () => { },
    };

    render() {
        const { serverErr, loading, messagesList, responseComes } = this.state;
        return (

            <View style={styles.mainParentStyle}>
                {
                    (serverErr === false) ?
                        (
                            <KeyboardAvoidingView style={{ flex: 1, marginTop: globals.screenHeight * 0.015, backgroundColor: 'white' }} behavior={(Platform.OS == "ios") ? "padding" : ""} keyboardVerticalOffset={globals.screenHeight * 0.08}>
                                <View style={styles.mainParentStyle}>
                                    <FlatList
                                        inverted
                                        style={[styles.flatListStyle, { marginBottom: globals.screenHeight * 0.125 }]}
                                        showsVerticalScrollIndicator={false}
                                        data={messagesList}
                                        renderItem={({ item, index }) => this.messagesChatRender(item, index)}
                                        extraData={this.state}
                                        bounces={false}
                                        keyExtractor={(index, item) => item.toString()}
                                        ref={ref => this.flatList = ref}
                                    />
                                    <View style={[styles.sendBtnViewMainContainer, {
                                        height: globals.screenHeight * 0.12,
                                    }]} >

                                        <View style={styles.iconAndInputContainer}>
                                            <View style={[styles.inputViewContainer,{height:(Platform.OS == "android")? globals.screenHeight * 0.065:globals.screenHeight * 0.048}]}>
                                                <TextInput
                                                    style={styles.textInputStyleforBotton}
                                                    placeholder={'Type Message...'}
                                                    value={this.state.msgType}
                                                    blurOnSubmit={true}
                                                    multiline={true}
                                                    maxLength={1000}
                                                    showsVerticalScrollIndicator={false}
                                                    returnKeyType="done"
                                                    onChangeText={text => {
                                                        this.setState({
                                                            msgType: text,
                                                        });
                                                    }}

                                                    placeholderTextColor={colors.lightGray}
                                                />
                                                {/* <TouchableOpacity style={styles.attchmentView} onPress={() => this.setAttachmentPopupVisible(true)}>
                                                    <Image source={images.Discussion.attchment}
                                                        style={styles.attchmentStyle}
                                                        resizeMode="contain" /></TouchableOpacity> */}
                                            </View>
                                            <View style={styles.sendIconViewContainer}>
                                                {
                                                    <TouchableOpacity onPress={() => this.sendPostMessage()}>
                                                        <Image
                                                            source={images.Discussion.chatBtn}
                                                            style={styles.sendIconStyle}
                                                            resizeMode="contain"
                                                        />
                                                    </TouchableOpacity>
                                                }
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </KeyboardAvoidingView>
                        )
                        :
                        (
                            <View style={styles.serverErrViewContainer}>
                                <Text style={styles.serverTextStyle}>
                                    {globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_PLZ_TRY_AGAIN}
                                </Text>
                                <TouchableOpacity style={styles.serverButtonStyle} onPress={() => this._tryAgain()}>
                                    <CustomButton
                                        text={globals.BTNTEXT.LOGINSCREEN.CUSTOM_BTN_TEXT}
                                        backgroundColor={colors.bgColor}
                                        color={colors.white}
                                        loadingStatus={loading}
                                    />
                                </TouchableOpacity>
                            </View>
                        )
                }

            </View>

        );
    }
}
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChatScreen);
