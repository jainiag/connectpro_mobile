/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, TouchableWithoutFeedback, TextInput } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import styles from './style'
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as images from '../../../../assets/images/map';
import * as colors from '../../../../assets/styles/color';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import { API } from '../../../../utils/api';
import CustomButton from '../../../../components/CustomButton/index';
import loginScreen from '../../AuthenticationScreens/loginScreen';

const iPad = DeviceInfo.getModel()
let TAG = "NewMessageListSearch :::==="

let memberData;
class NewMessageListSearch extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Messages'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            memberList: [],
            serverErr: false,
            responseComes: false,
            searchTxt: '',
        }
    }

    componentDidMount() {
        this.makeApiCall()
    }

    makeApiCall() {
        this.props.showLoader();
        if (globals.isInternetConnected) {
            API.getChatMembers(this.GetChatMembersResponse, true, globals.userID);
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    GetChatMembersResponse = {
        success: response => {
            console.log(
                TAG,
                'GetChatMembersResponse -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                this.setState({ memberList: response.Data, responseComes: true }, () => {
                    memberData = this.state.memberList
                })
            }
            else {
                Alert.alert(globals.appName, response.Message);
                this.props.hideLoader()
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, responseComes: true });
            this.setState({ loading: false })
            console.log(
                TAG,
                'GetChatMembersResponse -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            }
            else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
 *  call when _sessionOnPres
 */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    /*
  * Try again method
  */
    _tryAgain() {
        this.setState({ loading: true, serverErr: false }, () => {
            this.makeApiCall();
        });
    }

    /**
    * method for setSelectionUpcomingEvent Particular Item
    */
    setSelectionUpcomingEvent(item, index) {
        const { memberList } = this.state;
        const membersDataList = memberList;
        for (let i = 0; i < membersDataList.length; i++) {
            if (index == i) {
                membersDataList[index].isSelected = true;
            } else {
                membersDataList[i].isSelected = false;
            }
        }
        this.setState({ memberList: membersDataList }, () => {
            this.props.navigation.navigate('ChatScreen', { MemberID: item.MemberID })
        });
        this.setState({ searchTxt: '' })
        this.makeApiCall();
    }

    memberRenderList(item, index) {
        const isProfileLogo = globals.checkImageObject(item, 'MemberProfile');
        const isMemberName = globals.checkObject(item, 'MemberName');


        return (
            <TouchableWithoutFeedback style={styles.mainParentStyle} onPress={() => this.setSelectionUpcomingEvent(item, index)}>
                <View>
                    <View style={[styles.mainRenderItemView, { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white }]}>
                        <View style={styles.horizontalItemView}>
                            <View style={styles.beforeimgView}>
                                <Image
                                    source={{ uri: isProfileLogo ? item.MemberProfile : globals.User_img }}
                                    style={[styles.ivItemImageStyle]}
                                />
                            </View>
                            <View style={styles.middleViewTexts}>
                                <Text numberOfLines={1} style={[styles.attendeeTextStyle, {
                                    width:
                                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.65 :
                                            globals.screenWidth * 0.50,
                                    fontWeight: (item.IsRead == true) ? '400' : '600', color: item.isSelected == true ? colors.white : colors.black
                                }]}>{(isMemberName) ? item.MemberName : ''}</Text>
                            </View>
                        </View>
                    </View>

                </View>
            </TouchableWithoutFeedback>
        )
    }

    SearchFilterFunction(text) {
        if (text != "") {
            const newData = memberData.filter(function (item) {
                const itemData = item.MemberName ? item.MemberName.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;
            });
            this.setState({
                memberList: newData,
                searchTxt: text,
            });
        } else {
            this.setState({
                memberList: memberData,
                searchTxt: text,
            });
        }

    }

    render() {
        const { searchTxt, serverErr, loading, memberList, responseComes } = this.state;
        return (
            <View style={[styles.mainParentStyle, { paddingTop: globals.screenHeight * 0.02 }]}>
                <View style={styles.findPersonMainHorizontal}>
                    <View style={styles.findPersonView}>
                        <TextInput
                            style={[styles.findPersonTextStyle, {
                                marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null

                            }]}
                            placeholder="Find Person"
                            onChangeText={text => this.SearchFilterFunction(text)}
                            placeholderTextColor={colors.lightgray}
                            value={this.state.searchTxt}
                        />
                    </View>
                    <View style={styles.searchViewRight}>
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null

                            }}
                        />
                    </View>
                </View>
                <View style={styles.grayBorderStyle}></View>
                {
                    (serverErr === false) ?
                        (
                            (memberList.length == 0 && responseComes) ?
                                (memberList.length == 0 && searchTxt !== '') ?
                                    <View style={globalStyles.nodataStyle}>
                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DATA_NOT_AVAILABLE_IN_SEARCH}</Text>
                                    </View> :
                                    <View style={globalStyles.nodataStyle}>
                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.MEMBER_DATA_NOT_AVLB}</Text>
                                    </View> :
                                <View style={styles.mainParentStyle}>
                                    <FlatList
                                        style={[styles.flatListStyle, {}]}
                                        showsVerticalScrollIndicator={false}
                                        data={memberList}
                                        renderItem={({ item, index }) => this.memberRenderList(item, index)}
                                        extraData={this.state}
                                        bounces={false}
                                        keyExtractor={(index, item) => item.toString()}
                                    />
                                </View>
                        )
                        :
                        (
                            <View style={styles.serverErrViewContainer}>
                                <Text style={styles.serverTextStyle}>
                                    {globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_PLZ_TRY_AGAIN}
                                </Text>
                                <TouchableOpacity style={styles.serverButtonStyle} onPress={() => this._tryAgain()}>
                                    <CustomButton
                                        text={globals.BTNTEXT.LOGINSCREEN.CUSTOM_BTN_TEXT}
                                        backgroundColor={colors.bgColor}
                                        color={colors.white}
                                        loadingStatus={loading}
                                    />
                                </TouchableOpacity>
                            </View>
                        )
                }
            </View>
        );
    }
}
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewMessageListSearch);
