import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
const iPad = DeviceInfo.getModel()

module.exports = StyleSheet.create({
  //-------------------------------------------Message threads -------------------------------------------
  mainParentStyle: {
    flex: 1,
    //paddingTop:globals.screenHeight * 0.02,
  },
  tvTitleStyle: {
    fontSize: globals.font_20,
    color: 'blue',
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    marginBottom: 15,
  },
  headerStyles: {
    marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.015 : 0
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  nodataStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  nodataTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
  },
  mainRenderItemView: {
    height: globals.screenHeight * 0.11, borderColor: colors.proUnderline, borderWidth: 1, marginBottom: 7, borderRadius: 3, flex: 1,
    marginHorizontal: globals.screenWidth * 0.05
  },
  horizontalItemView: {
    flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.015, flex: 1,
  },
  middleViewTexts: {
    justifyContent: 'center', marginHorizontal: globals.screenHeight * 0.02, flex: 1,
  },
  lastImgViewEnd: {
    justifyContent: 'flex-end', alignItems: 'flex-end'
  },
  attendesFlatlist: {
    marginTop: globals.screenHeight * 0.05,
    paddingHorizontal: globals.screenWidth * 0.05
  },
  beforeimgView: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946) / 2,
    borderColor: colors.lightGray,
    borderWidth: 0.5
  },
  ivItemImageStyle: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946) / 2,
    marginTop: -1,
    marginLeft: -1,
  },
  attendeeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_13,
    color: colors.black
  },
  attende_shareIconStyle: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.screenHeight * 0.05 : globals.screenHeight * 0.06 : globals.screenWidth * 0.08,
    width: (iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet()) ? (Platform.OS == 'ios') ? globals.screenHeight * 0.05 : globals.screenHeight * 0.06 : globals.screenWidth * 0.08,
  },
  flatListStyle: {
    flex: 1,
    // marginTop: globals.screenHeight * 0.015
  },
  msgDateTxt: {
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 14 : globals.font_9,
  },
  chatActImgStyle: {
    position: 'absolute', height: globals.screenHeight * 0.07, width: globals.screenHeight * 0.07, bottom: globals.screenHeight * 0.035, right: globals.screenWidth * 0.05
  },

  //-------------------------------------new message list with search screen ----------------------------------
  findPersonMainHorizontal: {
    flexDirection: 'row', marginBottom: globals.screenHeight * 0.02, marginHorizontal: globals.screenWidth * 0.05
  },
  findPersonView: {
    justifyContent: 'flex-start', alignItems: 'flex-start', flex: 1
  },
  searchViewRight: {
    justifyContent: 'center', alignItems: 'flex-end', flex: 1
  },
  grayBorderStyle: {
    width: globals.screenWidth, height: 1, bottom: 2, backgroundColor: colors.gray, opacity: 0.5
  },
  findPersonTextStyle: {
    fontSize: globals.font_13,
    color: colors.lightgray
  },
  //chat screen
  sendBtnViewMainContainer: {
    backgroundColor: colors.grayBG,
    width: "100%",
    position: 'absolute',
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconAndInputContainer: {
    flexDirection: 'row', alignItems: 'center', flex: 1
  },
  inputViewContainer: {
    width: globals.screenWidth * 0.7,
    height: globals.screenHeight * 0.05,
    backgroundColor: colors.white,
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 20,
    borderColor: colors.lightWhite,
    justifyContent: 'center',
  },
  horizontalItemViewChat: {
    flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginTop: globals.screenHeight * 0.015,
    marginBottom: globals.screenHeight * 0.007, flex: 1,
  },
  textInputStyleforBotton: {
    // height:globals.screenHeight * 0.02,
    width: globals.screenWidth * 0.6,
    textAlign: 'left',
    // backgroundColor:'red',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.font_12 : globals.font_10,
    marginLeft: globals.screenWidth * 0.03,
    marginRight: globals.screenWidth * 0.01,
    marginVertical: (Platform.OS == "android") ? 0 : globals.screenWidth * 0.014,
    // marginTop: globals.screenHeight * 0.006,
    // marginBottom: globals.screenHeight * 0.006,
  },
  sendIconViewContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
    borderRadius: globals.screenWidth * 0.09,
    marginLeft: globals.screenWidth * 0.04,
  },
  attchmentView: {
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  attchmentStyle: {
    alignSelf: "center",
    height: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.04,
    tintColor: colors.lightGray,
  },
  rightChatContentView: {
    justifyContent: 'center', alignItems: 'flex-end', marginRight: (globals.screenWidth * 0.04), width: globals.screenWidth * 0.70, backgroundColor: colors.lightWhite,
    borderRadius: 5
  },
  leftChatContentView: {
    justifyContent: 'center', backgroundColor: colors.lightWhite, width: globals.screenWidth * 0.70, borderRadius: 5,
    marginLeft: (globals.screenWidth * 0.018)
  },
  leftViewContentTxt: {
    marginLeft: 10,
    fontSize: globals.font_14
  },
  rightContentTxt: {
    marginRight: 10,
    fontSize: globals.font_14
  },
  rightDate: {
    fontSize:((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 14 : globals.font_10,
    alignSelf: 'flex-end',
    marginRight: 10
  },
  sendIconStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.09,
    borderRadius: globals.screenWidth * 0.09,
},
  leftDate: {
    fontSize:((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 14 : globals.font_10,
    alignSelf: 'flex-start',
    marginLeft: 10
  },
  endFlexView: {
    flex: 1, alignSelf: 'flex-end'
  },
  startFlexView: {
    flex: 1, alignSelf: 'flex-start'
  },
  chatArrStyle: {
    height: 20, width: 20, position: 'absolute', tintColor: colors.lightWhite, left: -10
  },
  chatArrStyleRight: {
    height: 20, width: 20, position: 'absolute', tintColor: colors.lightWhite, alignSelf: 'flex-end',
    zIndex: 10,
    right: -10
  }
});
