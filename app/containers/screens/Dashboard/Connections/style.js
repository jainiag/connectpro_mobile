import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
let { screenHeight, screenWidth } = globals;

const iPad = DeviceInfo.getModel();
module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
    marginTop: screenHeight * 0.02,
    //backgroundColor:'red'
  },
  summary_IconsStyle: {
    tintColor: colors.lightGray,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.03 : globals.screenWidth * 0.035,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.03 : globals.screenWidth * 0.035,
  },
  mainParentStyleScroll: {
    flex: 1,
    marginTop: screenHeight * 0.02,
    marginBottom: screenHeight * 0.02,
  },
  vwFlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  vwTabStyle: {
    height: screenHeight * 0.0946, //50
    paddingHorizontal: screenWidth * 0.0266, //10
    // backgroundColor:'red',

  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    marginBottom: 15,
  },
  headerStyles: {
    marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.015 : 0
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  vwCenterStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tvTabTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_20,
    paddingHorizontal: screenWidth * 0.0266, //10
  },
  tvSelectedTabStyle: {
    color: colors.warmBlue,
  },
  toTabStyle: {
    alignItems: 'center',

  },
  tvUnSelectedTabStyle: {
    color: colors.black,
  },
  selectedLineStyle: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth * 0.4562, //50
    height: screenHeight * 0.0088, //5
    borderRadius: screenHeight * 0.0044, //2.5
    backgroundColor: colors.warmBlue,
  },
  vwSimpleLineStyle: {
    width: '90%',
    position: 'absolute',
    bottom: 0,
    height: 1,
    backgroundColor: colors.gray,
    opacity: 1,
    marginHorizontal: screenWidth * 0.0468, //15
  },
  boxViewStyle: {
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    marginBottom: globals.screenHeight * 0.015,
    width: globals.screenWidth * 0.88,
    backgroundColor: 'yellow',
    marginLeft: globals.screenWidth * 0.0595,
    flex: 1,
  },
  flatListStyle: {
    flex: 1,
    // marginTop: globals.screenHeight * 0.01
  },
  userinfoStyle: {
    flexDirection: 'row',

    // backgroundColor: 'red',
    // marginHorizontal: globals.screenWidth * 0.015,
    // paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.025 : globals.screenWidth * 0.035,
  },
  borderColorlightblack: {
    borderColor: colors.lightBlack,
    height: screenWidth * 0.15,
    width: screenWidth * 0.15,
    borderRadius: (screenWidth * 0.15) / 2,
    borderWidth: 0.5,
  },
  borderColorWhite: {
    borderColor: colors.white,
    height: screenWidth * 0.15,
    width: screenWidth * 0.15,
    borderRadius: (screenWidth * 0.15) / 2,
    borderWidth: 0.5,
  },
  beforeImgViewStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 : globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 : globals.screenWidth * 0.15,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (globals.screenWidth * 0.10) / 2 : (globals.screenWidth * 0.15) / 2,
    borderColor: colors.proUnderline,
    borderWidth: 0.5,
    alignSelf: 'center',
    //  marginLeft: globals.screenWidth * 0.015
  },
  imgStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 : globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 : globals.screenWidth * 0.15,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (globals.screenWidth * 0.10) / 2 : (globals.screenWidth * 0.15) / 2,
    borderColor: colors.white,
    borderWidth: 0.5,
    alignSelf: 'center',

  },
  usernametextStyle: {
    // textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  textViewStyle: {
    alignContent: 'center',
    marginLeft: globals.screenWidth * 0.035,
    width: globals.screenWidth * 0.625,
    // backgroundColor: "yellow",
    marginVertical: globals.screenWidth * 0.025,
  },
  jobtitleView: {
    flexDirection: 'row',
    width: '95%',
    //  backgroundColor:'red',
    marginVertical: globals.screenWidth * 0.001
  },
  proffesionTextStyle: {
    // backgroundColor:'red',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
    marginLeft: globals.screenWidth * 0.01,
    // width:'80%',
  },
  swipeItemStyle: {
    backgroundColor: colors.white
  },

  tabViewContainer: {
    width: globals.screenWidth,
  },
  tabbarStyle: {
    backgroundColor: colors.transparent,
    width: globals.screenWidth,
    borderBottomWidth: 1.5,
    borderBottomColor: colors.proUnderline,
    alignSelf: 'flex-end',
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_15,
    elevation: 0,
    shadowOpacity: 0,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 71 : 59,
  },
  tabStyle: {
    backgroundColor: colors.transparent,
    width: globals.screenWidth * 0.49,
    height: globals.screenHeight * 0.059,
    top: 2,
  },
  indicatorStyle: {
    backgroundColor: colors.warmBlue,
    marginLeft: globals.screenWidth * 0.07,
    width: globals.screenWidth * 0.40,
  },
  labelStyle: {
    textAlign: 'center',
    width: globals.screenWidth * 0.39,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_15,
    fontWeight: '600',
    color: colors.black,
  },
  initialLayoutStyle: {
    width: globals.screenWidth - 10,
    flex: 1,
    height: 20,
  },
  container: {
    flex: 1,
  },
  titleimgStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05,
    backgroundColor: colors.warmBlue,
    alignSelf: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },
  counterStyle: {
    textAlign: 'center',
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 15 : globals.font_12,
  },
  flexDirectionRowStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    // backgroundColor:'red'
  },
  pendingRequestView: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    alignContent: "flex-end",
    // backgroundColor: 'pink',
    marginRight: globals.screenWidth * 0.025,
    marginBottom: globals.screenHeight * 0.01
  },
  pendingRequesttext: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 13 : globals.font_10,
    textAlign: 'right',
    alignSelf: 'flex-end',
    // backgroundColor:'red',
    marginRight: globals.screenWidth * 0.025,
    marginBottom: globals.screenHeight * 0.001
  },
  nodataStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  nodataTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
  },
  imgesStyle: {
    alignSelf: 'center',
    height: globals.screenHeight * 0.03,
    width: globals.screenHeight * 0.03
  },
  crossimgesStyle: {
    alignSelf: 'center',
    height: globals.screenHeight * 0.02,
    width: globals.screenHeight * 0.02
  },
  horizontalSeprator: {
    borderColor: colors.proUnderline, borderWidth: 1, marginTop: globals.screenHeight * 0.01,
    marginHorizontal: -(globals.screenWidth * 0.0368)
  },
  imgcontainer: {
    alignItems: 'center', justifyContent: 'center', flex: 1
  },
  rightRedLine: {
    backgroundColor: colors.darkRed,
    width: globals.screenWidth * 0.015555,
    // marginLeft: globals.screenWidth * 0.05,
    //marginRight: -(globals.screenHeight * 0.04),
    marginVertical: -(globals.screenHeight * 0.084)
  },
  leftGreenLine: {
    backgroundColor: colors.greenDone,
    width: globals.screenWidth * 0.015555,
    // marginLeft: globals.screenWidth * 0.4266,
    marginVertical: -(globals.screenHeight * 0.084)
  },

});
