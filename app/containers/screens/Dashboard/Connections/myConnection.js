/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import styles from './style';
import globalStyles from '../../../../assets/styles/globleStyles'
import * as globals from '../../../../utils/globals';
import { API } from '../../../../utils/api';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as images from '../../../../assets/images/map';
import * as colors from '../../../../assets/styles/color';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import ScrollableTabBar from '../../../../libs/react-native-scrollable-tabview/lib/ScrollableTabBar';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import Nointernet from '../../../../components/NoInternet/index'
import ServerError from '../../../../components/ServerError/index'

let _this = null;
const TAG = '==:== MyConnection : ';
class MyConnection extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: false,
      serverErr: false,
      myConnectionData: [],
      newConnectionData: [],
      isInternetFlag: true,
    }
  }

  componentDidMount() {
    this.makeApiCall();
    this.makenewConnectionApicall();
  }

  /**
  *  updatedMyConnectionData
  */
  static updatedMyConnectionData() {
    _this.makeApiCall();
  }

  static updatedNewConnectionData() {
    _this.makenewConnectionApicall()
  }


  /**
   *  call when _sessionOnPres
   */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigationProps.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }


  makenewConnectionApicall() {
    AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
      globals.userID = token;
      if (globals.isInternetConnected === true) {
        API.getNewConnection(this.GetNewConnectionResponseData, true, globals.userID);
      } else {
      }
    });
  }


  /**
  * Response callback of GetNewConnectionResponseData
  */
  GetNewConnectionResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetNewConnectionResponseData -> success : ',
        JSON.stringify(response)
      );

      AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
        globals.userID = token
      });
      if (response.StatusCode == 200) {

        const localData = []
        const responseData = response.Data;
        for (let i = 0; i < responseData.length; i++) {

          localData.push(response.Data[i])
          this.setState({
            newConnectionData: localData
          })
        }
      } else {
        Alert.alert(globals.appName, response.Message);
      }
      ScrollableTabBar.updatedNewCounts(response.Data.length);

    },
    error: err => {
      console.log(
        TAG,
        'GetNewConnectionResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
    },
    complete: () => {
    },
  };


  /**
   * API call
   */
  makeApiCall() {
    this.props.showLoader();

    AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
      globals.userID = token;
      if (globals.isInternetConnected === true) {
        this.setState({ loading: true, isInternetFlag: true })
        API.getMyConnection(this.GetMyConnectionResponseData, true, globals.userID);
      } else {
        this.props.hideLoader();
        this.setState({ isInternetFlag: false })
        // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  /*
  * Try again method
  */
  _tryAgain() {
    // this.props.hideLoader();
    this.setState({ serverErr: false }, () => {
      this.props.showLoader();
      this.makeApiCall();
    });
  }

  /**
    * Response callback of GetMyConnectionResponseData
    */
  GetMyConnectionResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetMyConnectionResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {

        const localData = []
        const responseData = response.Data;
        for (let i = 0; i < responseData.length; i++) {
          localData.push(response.Data[i])
          let uniquemyConnectionData = localData.filter((ele, ind) => ind === localData.findIndex(elem => elem.ID === ele.ID && elem.id === ele.id))
          this.setState({
            myConnectionData: uniquemyConnectionData
          })
        }
      } else {
        Alert.alert(globals.appName, response.Message);
      }
      ScrollableTabBar.updatedMyCounts(this.state.myConnectionData.length);
      this.props.hideLoader()
      this.setState({ loading: false })
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true });
      this.setState({ loading: false })
      console.log(
        TAG,
        'GetMyConnectionResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
     * method for setSelectionMyConnection Particular Item
     */
  setSelectionMyConnection(item, index) {
    const { myConnectionData } = this.state;
    const myConnectionDataList = myConnectionData;
    for (let i = 0; i < myConnectionDataList.length; i++) {
      if (index == i) {
        myConnectionDataList[index].isSelected = true;
        // this.props.navigation.navigate('GLOBAL_EVENT_SCREEN_DASHBOARD', {
        // selectedEventList: item,
        // });
      } else {
        myConnectionDataList[i].isSelected = false;
      }
    }
    this.setState({ myConnectionData: myConnectionDataList }, () => { });
  }

  eventItemUI(item, index) {
    const isSenderProfile = globals.checkImageObject(item, 'SenderProfile');
    const isSenderName = globals.checkObject(item, 'SenderName');
    const isReciverName = globals.checkObject(item, 'ReceiverName');
    const isSenderJobTitle = globals.checkObject(item, 'SenderJobTitle');
    const isReceiverJobTitle = globals.checkObject(item, 'ReceiverJobTitle');
    const isSenderOrganization = globals.checkObject(item, 'SenderOrganization');

    return (
      <View style={{ flex: 1 }}>
        <View
          style={[styles.boxViewStyle,
          { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },]}>
          <View>
            <TouchableOpacity onPress={() => {
              this.setSelectionMyConnection(item, index);
              this.props.navigationProps.navigation.navigate("OtherProfile",
                { User_ID: (globals.userID == item.SenderID) ? item.ReceiverID : item.SenderID, item: item, isfrom: "myConnection" });
            }}>
              <View style={styles.userinfoStyle}>
                <View style={[styles.beforeImgViewStyle, { marginLeft: globals.screenWidth * 0.02 }]}>
                  <Image
                    source={{ uri: (isSenderProfile) ? item.SenderProfile : globals.User_img }}
                    style={styles.imgStyle}

                  ></Image>
                </View>
                <View style={styles.textViewStyle}>
                  <Text numberOfLines={1} style={[styles.usernametextStyle, { color: item.isSelected == true ? colors.white : colors.lightBlack },]}>
                    {(globals.userID == item.SenderID) ?
                      (isReciverName) ? item.ReceiverName : '-'
                      :
                      (isSenderName) ? item.SenderName : '-'}
                  </Text>
                  <View style={styles.jobtitleView}>
                    <Image source={images.MyProfile.bag}
                      resizeMode='contain'
                      style={styles.summary_IconsStyle}
                    />
                    <Text numberOfLines={1} style={[styles.proffesionTextStyle, { color: item.isSelected == true ? colors.white : colors.lightGray },]}>
                      {(globals.userID == item.SenderID) ?
                        (isReceiverJobTitle) ? item.ReceiverJobTitle : '-'
                        :
                        (isSenderJobTitle) ? item.SenderJobTitle : '-'}
                    </Text>
                  </View>
                </View>
              </View>

            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  render() {
    const { myConnectionData, serverErr, loading, isInternetFlag } = this.state;


    return (
      <>
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            (serverErr === false) ?
              (
                (myConnectionData.length == '') ?
                  (loading == true) ?
                    <View style={styles.nodataStyle}></View>
                    :
                    <View style={globalStyles.nodataStyle}>
                      <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.MYCONNECTION_DATA_NOT_AVLB}</Text>
                    </View>
                  :
                  <View style={styles.mainParentStyle}>

                    <FlatList
                      style={styles.flatListStyle}
                      showsVerticalScrollIndicator={false}
                      data={myConnectionData}
                      renderItem={({ item, index }) => this.eventItemUI(item, index)}
                      extraData={this.state}
                      keyExtractor={(index, item) => item.toString()}
                    />
                  </View>
              )

              :
              (
                <ServerError loading={false} onPress={() => this._tryAgain()} />
              )

        }

      </>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyConnection);
