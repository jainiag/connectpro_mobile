/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ScrollView, Alert } from 'react-native';
import styles from './style';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles'
import { API } from '../../../../utils/api';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import * as colors from '../../../../assets/styles/color';
import { bindActionCreators } from 'redux';
import * as images from '../../../../assets/images/map';
import Swipeout from '../../../../libs/react-native-swipeout';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import MyConnection from './myConnection';
import DeviceInfo from 'react-native-device-info';
import ScrollableTabBar from '../../../../libs/react-native-scrollable-tabview/lib/ScrollableTabBar';
import Nointernet from '../../../../components/NoInternet/index'
import ServerError from '../../../../components/ServerError/index'


let _this = null;
const iPad = DeviceInfo.getModel();
const TAG = '==:== NewConnection : ';
class NewConnection extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: false,
      serverErr: false,
      isInternetFlag: true,
      newConnectionData: [],
      onSwipOutCancel: false,
      onSwipOutAccept: false
    }
  }


  componentDidMount() {
    this.makeApiCall();
  }


  /*
  * Try again method
  */
  _tryAgain() {
    this.setState({ serverErr: false }, () => {
      this.props.showLoader();
      this.makeApiCall();
    });
  }

  /**
  *  call when _sessionOnPres
  */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigationProps.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  static refreshNewConnectionList() {
    _this.makeApiCall();
  }

  /**
   * API call for newConnectionRequest
   */
  makeApiCall() {
    this.props.showLoader();
    AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
      globals.userID = token;
      if (globals.isInternetConnected === true) {
        this.setState({ loading: true, isInternetFlag: true })
        API.getNewConnection(this.GetNewConnectionResponseData, true, globals.userID);
      } else {
        this.props.hideLoader();
        this.setState({ isInternetFlag: false })
        // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  /**
   * Response callback of GetNewConnectionResponseData
   */
  GetNewConnectionResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetNewConnectionResponseData -> success : ',
        JSON.stringify(response)
      );

      AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
        globals.userID = token
      });
      if (response.StatusCode == 200) {

        const localData = []
        const responseData = response.Data;
        for (let i = 0; i < responseData.length; i++) {

          localData.push(response.Data[i])
          this.setState({
            newConnectionData: localData
          })
        }
      } else {
        Alert.alert(globals.appName, response.Message);
      }
      ScrollableTabBar.updatedNewCounts(response.Data.length);
      MyConnection.updatedNewConnectionData()
      this.props.hideLoader()
      this.setState({ loading: false })
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true });
      this.setState({ loading: false })
      console.log(
        TAG,
        'GetNewConnectionResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };


  /**
    * method for setSelectionNewConnection Particular Item
    */
  setSelectionNewConnection(item, index) {
    const { newConnectionData } = this.state;
    const newRequestDataList = newConnectionData;
    for (let i = 0; i < newRequestDataList.length; i++) {
      if (index == i) {
        newRequestDataList[index].isSelected = true;
      } else {
        newRequestDataList[i].isSelected = false;
      }
    }
    this.setState({ newConnectionData: newRequestDataList }, () => { });
  }

  /**
   * API call for AcceptorRejectAPICall
   */
  AcceptorRejectAPICall(item, no) {
    this.props.showLoader();
    const data = {
      ID: item.ID,
      SenderID: item.SenderID,
      SenderName: item.SenderName,
      SenderEmail: item.SenderEmail,
      SenderJobTitle: item.SenderJobTitle,
      SenderOrganization: item.SenderOrganization,
      SenderProfile: item.SenderProfile,
      ReceiverID: item.ReceiverID,
      ReceiverName: item.ReceiverName,
      ReceiverEmail: item.ReceiverEmail,
      ReceiverJobTitle: item.ReceiverJobTitle,
      ReceiverOrganization: item.ReceiverOrganization,
      ReceiverProfile: item.ReceiverOrganization,
      CreatedDate: item.CreatedDate,
      ModifiedDate: item.ModifiedDate,
      IsActive: item.IsActive,
      StatusID: no
    };

    if (globals.isInternetConnected === true) {
      if (item.SenderID == globals.userID) {
        API.cancelNewConnectionRequest(this.cancelNewConnectionRequestResponseData, data, false)
      }
      else {
        API.acceptOrRejectConnection(this.AcceptOrRejectConnectionResponseData, data, false)
      }
    } else {
      this.props.hideLoader();
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
     * Response callback of cancelNewConnectionRequestResponseData
     */
  cancelNewConnectionRequestResponseData = {
    success: response => {
      console.log(
        TAG,
        'cancelNewConnectionRequestResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        Alert.alert(globals.appName, "Connection request cancelled.", [{
          text: 'OK', onPress: () => this.setState({
            newConnectionData: []
          }, () => {
            this.makeApiCall()
            MyConnection.updatedNewConnectionData()
          })
        }]);
      }
      else {
        Alert.alert(globals.appName, response.Message);
      }
      this.props.hideLoader();
      this.setState({ loading: false });
    },

    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false });
      console.log(
        TAG,
        'cancelNewConnectionRequestResponseData -> success : ',
        JSON.stringify(err.message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(globals.appName, err.Message);
      }

    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false });
    },
  };


  /**
     * Response callback of AcceptOrRejectConnectionResponseData
     */
  AcceptOrRejectConnectionResponseData = {
    success: response => {
      console.log(
        TAG,
        'AcceptOrRejectConnectionResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({
          newConnectionData: []
        }, () => {
          this.makeApiCall()
          MyConnection.updatedNewConnectionData()
        })

      }
      else {
        Alert.alert(globals.appName, response.Message);
      }
      MyConnection.updatedMyConnectionData()
      MyConnection.updatedNewConnectionData()
      this.setState({ loading: false })
      this.props.hideLoader()
    },

    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false });
      console.log(
        TAG,
        'AcceptOrRejectConnectionResponseData -> success : ',
        JSON.stringify(err.message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(globals.appName, err.Message);
      }

    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false });
    },
  };


  onOpenSwipeout() {
    this.setState({ onSwipOutAccept: !this.state.onSwipOutAccept, onSwipOutCancel: !this.state.onSwipOutCancel })
  }
  onCloseSwipeout() {
    this.setState({ onSwipOutAccept: !this.state.onSwipOutAccept, onSwipOutCancel: !this.state.onSwipOutCancel })
  }

  eventItemUI(item, index) {
    const isSenderID = globals.checkObject(item, 'SenderID');
    const isReceiverProfile = globals.checkImageObject(item, 'ReceiverProfile');
    const isReceiverName = globals.checkObject(item, 'ReceiverName');
    const isSenderProfile = globals.checkImageObject(item, 'SenderProfile');
    const isSenderName = globals.checkObject(item, 'SenderName');
    const isSenderJobTitle = globals.checkObject(item, 'SenderJobTitle');
    const isSenderOrganization = globals.checkObject(item, 'SenderOrganization');
    const isReceiverJobTitle = globals.checkObject(item, 'ReceiverJobTitle');
    const isReceiverOrganization = globals.checkObject(item, 'ReceiverOrganization');
    const { newConnectionData } = this.state;
    var swipeoutRightBtns = [
      {
        data: item,
        backgroundColor: colors.darkRed,
        component: <View style={styles.imgcontainer}>
          <Image
            source={images.colse.closebtn}
            style={styles.crossimgesStyle}
            resizeMode="contain" />
        </View>,
        underlayColor: colors.lightBlue,
        onPress: () => this.AcceptorRejectAPICall(item, (item.SenderID == globals.userID) ? 1 : 3)
      }
    ]

    var swipeoutLeftBtns = [
      {
        data: item,
        backgroundColor: colors.greenDone,
        component: <View style={styles.imgcontainer}>
          <Image
            source={images.done.donebtn}
            style={styles.imgesStyle}
            resizeMode="contain" />
        </View>,
        underlayColor: colors.lightBlue,
        onPress: () => this.AcceptorRejectAPICall(item, 2)

      }
    ]
    return (

      <View>

        <Swipeout
          right={swipeoutRightBtns}
          left={(item.SenderID == globals.userID) ? null : swipeoutLeftBtns}
          rowId={item.id}
          style={[styles.boxViewStyle,
          { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },]}>
          <View>
            <TouchableOpacity
              onPress={() => {
                this.setSelectionNewConnection(item, index);
                this.props.navigationProps.navigation.navigate("OtherProfile", { User_ID: (item.SenderID == globals.userID) ? item.ReceiverID : item.SenderID, item: item, isfrom: "newConnection" })
              }}>

              <View style={[styles.userinfoStyle, {
                paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?
                  globals.screenWidth * 0.025 : (item.SenderID == globals.userID) ? globals.screenWidth * 0.015 : globals.screenWidth * 0.035,
              }]}>
                {
                  (item.SenderID == globals.userID) ?
                    null :
                    (this.state.onSwipOutAccept) ? null : <View style={styles.leftGreenLine}></View>
                }
                <View style={[styles.beforeImgViewStyle,
                { marginLeft: (item.SenderID == globals.userID) ? globals.screenWidth * 0.032 : globals.screenWidth * 0.015 }]}>
                  <Image
                    source=
                    {{ uri: (item.SenderID == globals.userID) ? (isReceiverProfile) ? item.ReceiverProfile : globals.User_img : (isSenderProfile) ? item.SenderProfile : globals.User_img }}
                    style={styles.imgStyle}
                  ></Image>
                </View>
                <View style={styles.textViewStyle}>
                  <Text numberOfLines={1} style={[styles.usernametextStyle, { color: item.isSelected == true ? colors.white : colors.lightBlack },]}>
                    {(item.SenderID == globals.userID) ? (isReceiverName) ? item.ReceiverName : '-' : (isSenderName) ? item.SenderName : '-'}
                  </Text>

                  <View style={styles.jobtitleView}>
                    <Image source={images.MyProfile.bag}
                      resizeMode='contain'
                      style={styles.summary_IconsStyle}
                    />
                    <Text numberOfLines={1} style={[styles.proffesionTextStyle, { color: item.isSelected == true ? colors.white : colors.lightGray },]}>
                      {(item.SenderID == globals.userID) ? (isReceiverJobTitle) ? item.ReceiverJobTitle : '-' : (isSenderJobTitle) ? item.SenderJobTitle : '-'}
                    </Text>
                  </View>
                </View>
                {
                  (this.state.onSwipOutCancel) ? null :
                    <View style={[styles.rightRedLine,
                    {
                      marginLeft: (item.SenderID == globals.userID) ?
                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?
                          globals.screenWidth * 0.075 :
                          globals.screenWidth * 0.02 : globals.screenWidth * 0.0185
                    }]}></View>
                }

              </View>
              {(item.SenderID == globals.userID) ?
                <Text
                  style={[styles.pendingRequesttext, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.PENDING_REQUEST}</Text>
                :
                null
              }
            </TouchableOpacity>
          </View>
        </Swipeout>
      </View>

    )

  }

  render() {
    const { newConnectionData, serverErr, loading, isInternetFlag } = this.state;


    return (
      <>
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            (serverErr === false) ? (
              (newConnectionData.length == '') ?
                (loading == true) ?
                  <View style={styles.nodataStyle}></View>
                  :
                  <View style={globalStyles.nodataStyle}>
                    <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.NEWREQUEST_DATA_NOT_AVLB}</Text>
                  </View>
                :
                <View style={styles.mainParentStyleScroll}>
                  <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
                    <FlatList
                      style={styles.flatListStyle}
                      scrollEnabled={false}
                      showsVerticalScrollIndicator={false}
                      data={newConnectionData}
                      renderItem={({ item, index }) => this.eventItemUI(item, index)}
                      extraData={this.state}
                      keyExtractor={(index, item) => item.toString()}
                    />
                  </ScrollView>
                </View>
            )
              :
              (
                <ServerError loading={false} onPress={() => this._tryAgain()} />
              )

        }
      </>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewConnection);
