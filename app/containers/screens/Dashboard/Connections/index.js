/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import styles from './style'
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import globalStyles from '../../../../assets/styles/globleStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import MyConnection from './myConnection';
import NewConnection from './newConnection';
import { ScrollableTabView, ScrollableTabBar } from '../../../../libs/react-native-scrollable-tabview'

let _this = null;
const TAG = '==:== Connections : ';
class Connections extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        gesturesEnabled: false,
        headerLeft: globals.ConnectProbackButton(navigation, 'Member Connections'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });

    constructor(props) {
        super(props);
        _this = this;

    }
    render() {
        return <ScrollableTabView
            style={styles.headerStyles}
            initialPage={0}
            tabBarTextStyle={{fontSize:globals.font_16}}
            tabBarInactiveTextColor={colors.black} 
            tabBarActiveTextColor={colors.warmBlue}
            renderTabBar={() => <ScrollableTabBar />}
        >
            <MyConnection tabLabel={globals.MESSAGE.CONNECTIONS.MY_CONNECTION} navigationProps={this.props} />
            <NewConnection tabLabel={globals.MESSAGE.CONNECTIONS.NEW_CONNECTION_REQUEST} navigationProps={this.props} />
        </ScrollableTabView>
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Connections);
