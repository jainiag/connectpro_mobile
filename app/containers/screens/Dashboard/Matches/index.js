/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Image, FlatList, TouchableOpacity, ScrollView, Alert } from 'react-native';
import styles from './style'
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as images from '../../../../assets/images/map';
import { API } from '../../../../utils/api';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HTML from 'react-native-render-html';
import CustomButton from '../../../../components/CustomButton';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import Nointernet from '../../../../components/NoInternet/index'
import ServerError from '../../../../components/ServerError/index'

let _this = null;
let matchesData = ''
const TAG = '==:== Matches : ';
class Matches extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: (matchesData == '') ? globals.ConnectProbackButton(navigation, 'My Matches') :
            globals.ConnectProbackButton(navigation, 'My Matches'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            loading: false,
            matchesData: [],
            serverErr: false,
            isInternetFlag: true,
        }
    }

    componentDidMount() {
        this.apiGetSilmilarMatchesInterest()
    }

    /**
 *  call when _sessionOnPres
 */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    /**
 * Api call of apiGetSilmilarMatchesInterest
 */
    apiGetSilmilarMatchesInterest() {
        this.props.showLoader()
        AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
            globals.userID = token;
            if (globals.isInternetConnected === true) {
                this.setState({ loading: true, isInternetFlag: true })
                API.getSilmilarMatchesInterest(this.GetSilmilarMatchesInterestResponseData, true, globals.userID, "totalRecords");
            } else {
                this.props.hideLoader()
                this.setState({ isInternetFlag: false })
            }
        });
    }

    /**
    * Response callback of GetSilmilarMatchesInterestResponseData
    */
    GetSilmilarMatchesInterestResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetSilmilarMatchesInterestResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({ matchesData: response.Data.SimilarInterestUsers })
                console.log("matchesData", this.state.matchesData)
            } else {
                Alert.alert(globals.appName, response.Message);
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false, serverErr: true, })
            console.log(
                TAG,
                'GetSilmilarMatchesInterestResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 403 || err.StatusCode == 401) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message);
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
     *  call when serverError
     */
    _tryAgain() {
        this.setState({ loading: true, serverErr: false }, () => {
            this.apiGetSilmilarMatchesInterest()
        });
    }


    renderItemUI(item, index) {
        const isProffesion = globals.checkObject(item, 'JobTitle');
        const isUserName = globals.checkObject(item, 'UserName')
        const isImage = globals.checkImageObject(item, 'ProfilePicture');
        const isHobby = globals.checkObject(item, 'UserInterests')

        return (
            <View>

                <View style={styles.matches_ChildViewContainer}>
                    {/* <TouchableOpacity>
                        <Image style={styles.sideimgStyle} source={images.EventDashboard.shareBtn} resizeMode={"contain"} />
                    </TouchableOpacity> */}
                    <TouchableOpacity style={styles.userinfoStyle} onPress={() => (item.UserID == globals.userID) ?
                        this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" })
                        :
                        this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "matchesMember" })}>
                        <View style={styles.beforreImgView}>
                            <Image
                                source={{ uri: (isImage) ? item.ProfilePicture : globals.User_img }}
                                style={styles.imgStyle}
                            ></Image>
                        </View>
                        <View style={styles.textViewStyle}>
                            <Text numberOfLines={1} style={styles.usernametextStyle}>
                                {(isUserName) ? item.UserName : '-'}
                            </Text>
                            <Text numberOfLines={1} style={styles.proffesionTextStyle}>
                                {(isProffesion) ? item.JobTitle : "-"}
                            </Text>
                        </View>

                    </TouchableOpacity>
                    <View style={styles.grayLineView} />
                    <View style={styles.hobbytextStyle}>
                        <HTML
                            baseFontStyle={styles.hobbyTextstyle} html={isHobby ? item.UserInterests : '-'} />

                    </View>
                </View>
            </View>
        )
    }


    render() {
        const { matchesData, serverErr, loading, isInternetFlag } = this.state;

        return (
            <>
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?

                            (matchesData.length == '') ?
                                (loading == true) ?
                                    <View style={styles.nodataStyle}></View>
                                    :
                                    <View style={globalStyles.nodataStyle}>
                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.MATCHES_DATA_NOT_AVLB}</Text>
                                    </View>
                                :
                                <View style={styles.mainParentStyle}>

                                    <View style={styles.backgroundStyle}>
                                        <Text style={styles.headertextStyle}>All People With Similar Interests</Text>
                                    </View>
                                    <View style={styles.matchesMainView}></View>

                                    <FlatList
                                        data={matchesData}
                                        showsVerticalScrollIndicator={false}
                                        keyExtractor={(index, item) => item.toString()}
                                        renderItem={({ item, index }) => this.renderItemUI(item, index)}
                                        extraData={this.state} />

                                </View>
                            :
                            (
                                <ServerError loading={loading} onPress={() => this._tryAgain()} />
                            )
                }

            </>
        );
    }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Matches);
