import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';


const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  mainRenderItemView:{
    height: globals.screenHeight * 0.11, borderColor: colors.proUnderline, borderWidth: 1, marginBottom: globals.screenHeight * 0.01, borderRadius: 3,flex:1
  },
  horizontalItemView:{
     flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.015, flex: 1 
  },
  middleViewTexts:{
    justifyContent:'center', marginHorizontal:globals.screenHeight * 0.02,flex:1,
  },
  lastImgViewEnd:{
    justifyContent:'center', alignItems:'flex-end'
  },
  attendesFlatlist:{
     marginTop:globals.screenHeight*0.02,
    paddingHorizontal:globals.screenWidth*0.05
  },
  beforeimgView:{
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946)/2, 
    borderColor:colors.lightGray,
    borderWidth:0.5
  },
  ivItemImageStyle: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946)/2,
    marginTop:-1,
    marginLeft:-1, 
  },
  attendeeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12, 
  },
  attende_shareIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.screenHeight*0.05 : globals.screenHeight*0.06 : globals.screenWidth * 0.08,
    width: (iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())  ? (Platform.OS == 'ios') ? globals.screenHeight*0.05  : globals.screenHeight*0.06: globals.screenWidth * 0.08,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  data_notAvlbViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  data_notAvlb: {
    textAlign: 'center',
    fontSize: globals.font_18,
  },
  regBtnTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_20,
    color: colors.white,
  },
  regBtnStyle: {
    width: '100%',
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 65 : 55,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

