/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */

import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, TouchableWithoutFeedback, Alert } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import { API } from '../../../../../utils/api';
import Nointernet from '../../../../../components/NoInternet/index'
import ServerError from '../../../../../components/ServerError/index'


const TAG = '==:== Attendees : ';
let _this;

const iPad = DeviceInfo.getModel();

class Attendes extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    const attendeeCount = (params !== undefined) ? params.attendeeCount : ''
    return {
      headerLeft: (attendeeCount != undefined) ? globals.ConnectProbackButton(navigation, 'Attendees (' + attendeeCount + ')') : globals.ConnectProbackButton(navigation, 'Attendees ()'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }
  };

  constructor(props) {
    super(props);
    _this = this;
    console.log("props", props);

    this.state = {
      dataEventsList: this.props.navigation.state.params.dataEventsList,
      serverErr: false,
      isInternetFlag: true,
      noAttendeData: false,
      attendesData: [],
      loading: false,
      eventID: this.props.navigation.state.params.eventID,
      allEventDetails: this.props.navigation.state.params.allEventDetails,
      responseComes: false
    };
  }

  componentDidMount() {
    this.apiGetEventAttendes()
  }

  /**
   * Api call of Get Event Attendees
   */
  apiGetEventAttendes() {
    this.props.showLoader()
    this.setState({ loading: true })
    if (globals.isInternetConnected) {
      this.setState({ isInternetFlag: true })
      API.getEventAttendes(this.eventAttendesResponseData, true, this.state.eventID);
    } else {
      this.props.hideLoader()
      this.setState({ loading: false, isInternetFlag: false })
      // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
   * Response callback of eventAttendeses
   */
  eventAttendesResponseData = {
    success: response => {
      console.log(
        TAG,
        'eventAttendesResponseData -> success : ',
        JSON.stringify(response)
      );
      const { attendesData } = this.state;
      if (response.StatusCode == 200) {
        _this.props.navigation.setParams({
          attendeeCount: response.Data.AttendeeInfo.length
        });
        if (response.Data.AttendeeInfo.length == 0) {
          this.setState({ noAttendeData: true, loading: false })
        } else {
          this.setState({ attendesData: response.Data.AttendeeInfo, loading: false }, () => {
          })
          setTimeout(() => {
            this.setState({ responseComes: true })
          }, 2000);
        }
      } else {
        this.setState({ serverErr: true, loading: false })
        Alert.alert(globals.appName, response.Message);
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true, loading: false })
      Alert.alert(globals.appName, err.Message);
      console.log(
        TAG,
        'eventAttendesResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  navigateToOtherProfile(item, index) {
    this.setSelectionUpcomingEvent(item, index);
    (item.UserID == globals.userID) ?
      this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" }) : this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "AttendeeName" });
  }

  /**
   * Render item list event attendee
   * @param {*} item 
   * @param {*} index 
   */
  renderItemList(item, index) {

    const isAttendeeName = globals.checkObject(item, 'AttendeeName');
    const isEventDesignation = globals.checkObject(item, 'Designation');
    const isImage = globals.checkImageObject(item, 'ProfilePicture');
    return (
      <TouchableWithoutFeedback
        style={styles.mainParentStyle}
        onPress={() => {
          (item.IsMember == true) ? this.navigateToOtherProfile(item, index) : null;
        }}>

        <View style={[styles.mainParentStyle]}>
          <View style={[styles.mainRenderItemView, { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white }]}>
            <View style={styles.horizontalItemView}>
              <View style={styles.beforeimgView}>
                <Image
                  source={{ uri: isImage ? item.ProfilePicture : globals.User_img }}
                  style={[styles.ivItemImageStyle]}
                />
              </View>
              <View style={styles.middleViewTexts}>
                <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isAttendeeName) ? item.AttendeeName : ''}</Text>
                <Text style={[styles.attendeeTextStyle, { marginTop: 2, color: item.isSelected == true ? colors.white : colors.black }]}>{(isEventDesignation) ? item.Designation : ''}</Text>
              </View>
              <View style={styles.lastImgViewEnd}>
                {/* <Image
                  source={images.loginAndRegisterScreen.infoIcon}
                  style={styles.attende_shareIconStyle}
                /> */}
              </View>
            </View>

          </View>

        </View>
      </TouchableWithoutFeedback>
    );
  }

  /**
   * method for setSelectionUpcomingEvent Particular Item
   */
  setSelectionUpcomingEvent(item, index) {
    const { attendesData } = this.state;
    const attendesDataList = attendesData;
    for (let i = 0; i < attendesDataList.length; i++) {
      if (index == i) {
        attendesDataList[index].isSelected = true;
      } else {
        attendesDataList[i].isSelected = false;
      }
    }
    this.setState({ attendesData: attendesDataList }, () => { });
  }


  /*
   * Try again method
   */
  _tryAgain() {
    this.setState({ serverErr: false, loading: true });
    this.apiGetEventAttendes()
  }


  render() {
    const { attendesData, serverErr, loading, noAttendeData, isInternetFlag } = this.state;


    return (
      <View style={styles.mainParentStyle}>
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            (serverErr === false) ? (noAttendeData == true) ?
              <View style={[globalStyles.nodataStyle]}>
                <Text style={globalStyles.nodataTextStyle}>
                  {globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.ATTENDEE_NOT_AVAILABLE}
                </Text>
              </View> :

              <FlatList
                style={styles.attendesFlatlist}
                data={attendesData}
                renderItem={({ item, index }) => this.renderItemList(item, index)}
                keyExtractor={(index, item) => item.toString()}
                extraData={this.state}
              /> :
              (
                <ServerError loading={loading} onPress={() => this._tryAgain()} />
              )
        }
        <TouchableOpacity
          style={styles.regBtnStyle} onPress={() => this.props.navigation.navigate('EVENT_REGISTRATION', { dataEventsList: this.state.dataEventsList, eventID: this.state.eventID, allEventDetails: this.state.allEventDetails })}
        >
          <Text style={styles.regBtnTextStyle}>{globals.BTNTEXT.REGISTERSCREEN.REG_BTNTEXT}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Attendes);
