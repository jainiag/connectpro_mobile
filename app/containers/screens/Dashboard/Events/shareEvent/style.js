import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel()
module.exports = StyleSheet.create({
  mainParentStyle: {
     flex: 1,
    // height:globals.screenHeight,
    // width:globals.screenWidth,
    // marginTop: globals.screenHeight * 0.02,
    // backgroundColor: 'red'
  },
  beforeImgViewStyle:{
    height: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.25 : globals.screenWidth * 0.30,
    width: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.25 :   globals.screenWidth * 0.30 ,
    alignSelf: 'center',
    marginTop: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? -72 : -53,
    borderRadius: (globals.screenWidth * 0.30) / 2,
    borderColor: colors.lightBlack,
    borderWidth: 0.2,
  },
  nodataStyle:{
    alignItems:'center',
    justifyContent:'center',
    flex:1
  },
  nodataTextStyle:{
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28: globals.font_14
  },
  whiteViewStyle: {
    height: globals.screenHeight * 0.15,
    width: globals.screenWidth,
    backgroundColor: colors.white,
    // position:'absolute'
  },
  imageBackgroundStyle: {
    height:globals.screenHeight,
    width:globals.screenWidth
  },
  imgView: {
    marginHorizontal: globals.screenWidth * 0.06,
    backgroundColor: colors.white,
    // height: globals.screenHeight * 0.69,
    // alignItems: 'center',
  },
  imgStyle: {
    height: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.25 : globals.screenWidth * 0.30,
    width: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.25 : globals.screenWidth * 0.30,
    alignSelf: 'center',
    // marginTop: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? -72 : -53,
    borderRadius: (globals.screenWidth * 0.30) / 2,
  },
  lineStyle: {
    width: '100%',
    height: 1,
    backgroundColor: colors.lightBorder,
    marginTop: globals.screenHeight * 0.02,//20    
  },
  textView: {
    marginTop: globals.screenHeight * 0.025,
    alignItems: 'center',
    marginHorizontal:globals.screenWidth *0.01,
    justifyContent:'center',
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 24 : globals.font_14
  },
  tvBlueFontStyle: {
    color: colors.listSelectColor
  },
  vwCalenderViewStyle: {
    alignItems: 'center',
  },
  vwSubCalenderStyle: {
    alignItems: 'center',
  },
  ivCalenderImageStyle: {
    height: globals.screenHeight * 0.0468, //30
    width: globals.screenHeight * 0.0468, //50
    marginBottom: globals.screenHeight * 0.0184,//15
  },
  shareStyle: {
    marginTop:globals.screenHeight * 0.035,
    alignItems: 'center',
    // marginBottom: globals.screenHeight * 0.50,
  },
  sharetextStyle:{
    fontSize:(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 24 : globals.font_16,
    color:colors.warmBlue,
    fontWeight:'700'
  },
  shareimgStyle:{
    height: globals.screenHeight * 0.06, //30
    width: globals.screenHeight * 0.06, //50
    marginBottom: globals.screenHeight * 0.0184,
  },
  ivMapImageStyle: {
    height: globals.screenHeight * 0.05, //30
    width: globals.screenHeight * 0.04, //50
    marginBottom: globals.screenHeight * 0.0184,//15
  },
  vwDateBelowCalenderStyle: {
    flexDirection: 'row',
    marginHorizontal:globals.screenWidth * 0.02,
  },
  tvBigDateStyle: {
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 31 : globals.font_19,
  
  },
  tvBigDatelocationStyle:{
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_19,
  },
  tvBigDatestartendStyle:{
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_19,
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    // backgroundColor:'red'
  },
  vwLeftMarginDateStyle: {
    marginLeft: globals.screenWidth * 0.0112,//10
  },
  tvLabelStyle: {
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_16,
    textAlign: 'center'
  },
  tvSubLabelStyle: {
    fontSize: globals.font_12,
  },
  tvThStyle2: {
    fontSize:(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.009,
    // backgroundColor:'red'
  },
  tvBigTimeStyle: {
    marginTop: globals.screenHeight * 0.0094, //5
    color: colors.darkgrey,
    marginBottom: globals.screenHeight * 0.001, //5
  },
  scrollViewStyle:{
    marginTop:(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.030 : globals.screenHeight * 0.019
  }

});
