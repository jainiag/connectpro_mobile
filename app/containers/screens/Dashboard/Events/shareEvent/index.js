/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, ImageBackground, ScrollView } from 'react-native';
import Share from 'react-native-share';
import styles from './style';
import * as images from '../../../../../assets/images/map';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';


let TAG = "==:== Share Event : "
class ShareEvent extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Share Event'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      dataEventsList: this.props.navigation.state.params.dataEventsList,
      loactiondata: this.props.navigation.state.params.loactiondata,
      shareUrl: this.props.navigation.state.params.shareUrl,
      eventDetails: this.props.navigation.state.params.allEventDetails

    }
  }


  /**
   * Event share method
   */
  _shareEvent = () => {
    const { eventDetails } = this.state;
    let isEventName = globals.checkObject(eventDetails, 'Title');

    const shareOptions = {
      message: "Event: " + ((isEventName) ? eventDetails.Title : " - ") + "\n" +
        "Link to open: " + this.state.shareUrl
    };

    Share.open(shareOptions)
      .catch(err => console.log(err));
  };

  render() {

    let { dataEventsList, loactiondata, eventDetails } = this.state;
    const isEventStartDate = globals.checkObject(eventDetails, 'StartDate');
    const isEventEndDate = globals.checkObject(eventDetails, 'EndDateLocal');
    const isEventname = globals.checkObject(eventDetails, 'Title')
    const isImage = globals.checkImageObject(eventDetails, 'EventLogo');
    const isLocation = globals.checkObject(loactiondata, 'Location');
    let starttime = (isEventStartDate) ? moment(eventDetails.StartDate).format('LT') : '-';
    let endtime = (isEventEndDate) ? moment(eventDetails.EndDateLocal).format('LT') : '-';
    let month = (isEventStartDate) ? moment(eventDetails.StartDate).format('MMM') : '-';
    let thDate = (isEventStartDate) ? moment(eventDetails.StartDate).format('Do') : '-';
    let onlyDt = (isEventStartDate) ? moment(eventDetails.StartDate).format('D') : '-';
    let year = (isEventStartDate) ? moment(eventDetails.StartDate).format('YYYY') : '-';
    let fullMonthName = (isEventStartDate) ? moment(eventDetails.StartDate).format('MMMM') : '-';
    return (
      <>
        {
          (loactiondata == '') ?
            <View style={globalStyles.nodataStyle}>
              <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.CANNOT_SHAR_EVENT}</Text>
            </View>
            :
            <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.scrollViewStyle}>
              <View style={styles.mainParentStyle}>

                <View style={styles.whiteViewStyle}>
                </View>
                <ImageBackground
                  source={images.loginAndRegisterScreen.blueBackground}
                  style={styles.imageBackgroundStyle}
                >
                  <View style={{ flex: 1 }}>

                    <View style={styles.imgView}>
                      <View style={styles.beforeImgViewStyle}>
                        <Image
                          source={{ uri: (isImage) ? eventDetails.EventLogo : globals.Event_img }}
                          style={styles.imgStyle}
                        />
                      </View>
                      <View style={styles.textView}>
                        <Text style={styles.tvLabelStyle}>{(isEventname) ? eventDetails.Title : '-'}</Text>
                      </View>
                      <View style={styles.lineStyle} />
                      <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.scrollViewStyle}>
                        <View style={styles.vwCalenderViewStyle}>
                          <View style={[styles.vwSubCalenderStyle, { marginTop: globals.screenHeight * 0.035, }]}>
                            <Image source={images.EventDashboard.calendarBlue} style={styles.ivCalenderImageStyle} resizeMode={"contain"} />
                            <View style={styles.vwDateBelowCalenderStyle}>
                              
                              <View style={[styles.vwDateSubViewStyle, styles.vwLeftMarginDateStyle]}>
                              <Text style={styles.tvBigDateStyle}>{fullMonthName+ " "}</Text>
                                <Text style={[styles.tvLabelStyle, styles.tvBigDateStyle]}>{onlyDt}</Text>
                                <Text style={[styles.tvThStyle2]}>{thDate.charAt(thDate.length - 2)}{thDate.charAt(thDate.length - 1)}</Text>
                                <Text style={[styles.tvLabelStyle, { marginLeft: globals.screenWidth * 0.0212 }]}>{year}</Text>
                              </View>
                            </View>
                            <Text numberOfLines={1}
                              style={[styles.tvBigDateStyle, styles.tvBigTimeStyle,{marginHorizontal:globals.screenWidth * 0.04}]}
                            >{`${eventDetails.StartDate} To ${eventDetails.EndDate}`}</Text>
                           
                          </View>
                        </View>
                        <View style={styles.lineStyle} />
                        <View style={styles.vwCalenderViewStyle}>
                          <View style={[styles.vwSubCalenderStyle, { marginTop: globals.screenHeight * 0.015, }]}>
                            <Image source={images.EventDashboard.locationBlue} style={styles.ivMapImageStyle} resizeMode={"contain"} />
                            <View style={[styles.vwDateBelowCalenderStyle]}>
                              <Text style={[styles.tvBigDatelocationStyle, { textAlign: 'center' }]}>{(isLocation) ? loactiondata.Location : '-'}</Text>
                            </View>
                          </View>
                        </View>
                        <View style={styles.lineStyle} />
                        <View style={styles.shareStyle}>
                          <Text style={styles.sharetextStyle}>SHARE WITH</Text>
                          <TouchableOpacity onPress={() => this._shareEvent()}>
                            <Image source={images.EventDashboard.shareBtn} style={[styles.shareimgStyle, { marginTop: globals.screenHeight * 0.015 }]} resizeMode={"contain"} />
                          </TouchableOpacity>
                        </View>
                      </ScrollView>
                    </View>

                  </View>
                </ImageBackground>

              </View>
            </ScrollView>
        }

      </>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShareEvent);
