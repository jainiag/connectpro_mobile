/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, ImageBackground, Dimensions, YellowBox, Alert, Linking } from 'react-native';
import Share from 'react-native-share';
import styles from './style'
import * as colors from '../../../../../assets/styles/color';
import * as globals from '../../../../../utils/globals';
import * as images from '../../../../../assets/images/map';
import globalStyles from '../../../../../assets/styles/globleStyles'
import MapView, { PROVIDER_GOOGLE, Marker, AnimatedRegion, } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import Geocoder from 'react-native-geocoding';
import Entypo from 'react-native-vector-icons/Entypo';
import MapViewDirections from 'react-native-maps-directions';
import Geolocation from '@react-native-community/geolocation';

let TAG = "==:== ViewMap : "

const screen = globals.WINDOW;

const ASPECT_RATIO = screen.width / screen.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const GOOGLE_MAPS_APIKEY = 'AIzaSyCMMwiglRzF2R-U0yQVbh90yF7qPMlkxKY';

export default class ViewMap extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: globals.ConnectProbackButton(navigation, 'Location'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            loactiondata: this.props.navigation.state.params.loactiondata,
            address: this.props.navigation.state.params.loactiondata.Location,
            shareUrl: this.props.navigation.state.params.shareUrl,
            region: {
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            },
            location: {
                latitude: 37.78825,
                longitude: -122.4324
            },
            showRoute: false,
            CrrntLong: 0,
            CrrntLat: 0,
            data: {},
            locationName: '',
            descriptionLocation: '',
            destinationName: '',
            descriptionDestination: '',
        }
        this.mapView = null;
    }

    UNSAFE_componentWillMount() {
        Geolocation.getCurrentPosition((position) => {
            this.setState({ CrrntLat: position.coords.latitude, CrrntLong: position.coords.longitude });
        }, (error) => {
            if (error.message) {
                if (Platform.OS === 'ios') {
                    Alert.alert('Access Denied',
                        error.message, [{ text: 'Ok', onPress: () => Linking.openSettings() }], { cancelable: true });
                } else {
                    console.log(error.message);
                }
            }
        });
    }

    componentDidMount() {
        console.log("View map :::: ", this.props.navigation.state.params.loactiondata)
        Geocoder.init("AIzaSyCMMwiglRzF2R-U0yQVbh90yF7qPMlkxKY"); // use a valid API key
        Geocoder.from(this.state.address)
            .then(json => {
                var addressComponent = json.results[0].address_components[1];
                var addressDescription = json.results[0].formatted_address;
                this.setState({ destinationName: addressComponent.long_name })
                this.setState({ descriptionDestination: addressDescription })
                var location = json.results[0].geometry.location;
                console.log(TAG, "location : " + JSON.stringify(location));
                let updatedRegion = Object.assign({}, this.state.region);
                updatedRegion.latitude = location.lat;
                updatedRegion.longitude = location.lng;

                var markerLocation = Object.assign({}, this.state.location);
                markerLocation.latitude = location.lat;
                markerLocation.longitude = location.lng;

                this.setState({ region: updatedRegion, location: markerLocation });

            })
            .catch(error => console.warn(error));

        Geolocation.getCurrentPosition(info => {
            const Position = info
            this.setState({ data: Position })
            this.setState({ CrrntLat: this.state.data.coords.latitude, CrrntLong: this.state.data.coords.longitude })
        }, (err) => {
            if (err.message) {
                if (Platform.OS === 'ios') {
                    Alert.alert(globals.appName,
                        err.message, [{ text: 'OK', onPress: () => Linking.openSettings() }, { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },], { cancelable: true });
                } else {
                    console.log(TAG, err.message);
                }
            }
        });

        Geocoder.from({
            lat: this.state.CrrntLat,
            lng: this.state.CrrntLong
        })
            .then(json => {
                var addressComponent = json.results[0].address_components[1];
                var addressDescription = json.results[0].formatted_address;
                this.setState({ locationName: addressComponent.long_name })
                this.setState({ descriptionLocation: addressDescription })
            })
            .catch(error => console.log('Error :: geocoding ::', error));
    }

    /**
    * Event share method
    */
    _shareEvent = () => {
        const shareOptions = {
            message: "Link to open: " + this.state.shareUrl
        };

        Share.open(shareOptions)
            .catch(err => console.log(err));
    };

    _showDirection() {
        this.setState({ showRoute: true })
    }

    render() {
        let { address, region, location, showRoute, descriptionDestination, descriptionLocation, destinationName, locationName } = this.state;
        const origin = { latitude: this.state.CrrntLat, longitude: this.state.CrrntLong };

        const coordinates = [
            {
                latitude: this.state.CrrntLat,
                longitude: this.state.CrrntLong,
            },
            {
                latitude: location.latitude,
                longitude: location.longitude,
            },
        ];


        return (
            <View style={styles.mainParentStyle}>
                <View style={styles.mapStyle}>
                    <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.mapStyle}
                        ref={c => this.mapView = c}
                        showsUserLocation={true}
                        minZoomLevel={0}
                        maxZoomLevel={17}
                        region={region}>
                        <Marker
                            pinColor={'blue'}
                            coordinate={origin}
                            title={locationName}
                            description={descriptionLocation}
                        />
                        <Marker
                            pinColor={'red'}
                            coordinate={location}
                            title={destinationName}
                            description={descriptionDestination}
                        />
                        {/* <MapView.Marker coordinate={location} /> */}
                        {(showRoute === true) ?
                            coordinates.length >= 2 && (
                                <MapViewDirections
                                    origin={origin}
                                    destination={location}
                                    apikey={GOOGLE_MAPS_APIKEY}
                                    strokeWidth={4}
                                    strokeColor="dodgerblue"
                                    onStart={(params) => {
                                        console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                                    }}
                                    onReady={result => {
                                        if (result) {
                                            console.log('Result ::', result);

                                        } else {
                                            Alert.alert(globals.appName, 'Directions Not Available.')
                                        }

                                        this.mapView.fitToCoordinates(coordinates, {
                                            edgePadding: {
                                                right: globals.screenWidth / 20,
                                                bottom: globals.screenHeight / 20,
                                                left: globals.screenWidth / 20,
                                                top: globals.screenHeight / 20,
                                            },
                                        });
                                    }}
                                    onError={errorMessage => {
                                        console.log('GOT AN ERROR', errorMessage);
                                    }}
                                />
                            )
                            :
                            null
                        }
                    </MapView>
                </View>
                <ImageBackground
                    source={images.loginAndRegisterScreen.blueBackground}
                    style={styles.vwShareBelowStyle}
                >
                    <Text style={[styles.tvBigDateStyle, { textAlign: 'center', color: 'white' }]}>{(address == '' || address == undefined) ? '-' : address}</Text>
                </ImageBackground>
                <TouchableOpacity onPress={() => this._showDirection()} style={styles.toFloatingButtonStyle}>
                    <View style={styles.whiteroundView}>
                        <Entypo style={{ alignSelf: 'center' }} name={"direction"} size={globals.screenHeight * 0.05} color={colors.warmBlue} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
