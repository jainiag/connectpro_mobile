import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { screenHeight, screenWidth } from '../../../../../utils/globals';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
    // marginTop: globals.screenHeight * 0.02,
  },
  mapStyle: {
    ...StyleSheet.absoluteFillObject,
    height: screenHeight * 0.6,
    width: '100%',
  },
  vwShareBelowStyle: {
    flex: 1,
    marginTop: screenHeight * 0.6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  toFloatingButtonStyle: {
    position: 'absolute',
    top: screenHeight * 0.54,
    left: screenWidth * 0.44,
    height: screenHeight * 0.1,
    width: screenHeight * 0.1,
  },
  shareImgStyle: {
    height: screenHeight * 0.1,
    width: screenHeight * 0.1,
  },
  tvBigDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_19,
  },
  whiteroundView: {
    marginTop: globals.screenHeight * 0.01,
    alignItems: 'center',
    justifyContent: 'center',
    height: globals.screenHeight * 0.085,
    width: globals.screenHeight * 0.085,
    backgroundColor: colors.white,
    borderRadius: (globals.screenHeight * 0.085 / 2)
  }
});
