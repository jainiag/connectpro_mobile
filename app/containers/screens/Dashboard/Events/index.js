/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Modal, Image, FlatList, ScrollView,  Alert } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import styles from './style';
import { API } from '../../../../utils/api';
import globalStyles from '../../../../assets/styles/globleStyles';
import DateRangePicker from '../../../../components/DatePicker';
import * as colors from '../../../../assets/styles/color';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import * as images from '../../../../assets/images/map';
import { NavigationEvents } from 'react-navigation';
import Nointernet from '../../../../components/NoInternet/index'
import ServerError from '../../../../components/ServerError/index'
import loginScreen from '../../../screens/AuthenticationScreens/loginScreen';

const TAG = '==:== MYEvents : ';
let _this;
const iPad = DeviceInfo.getModel();

class MyEvents extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'My Events'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
    headerRight: (
      <View style={{ alignItems: 'center' }}>
        <TouchableOpacity
          onPress={() => _this.setModalVisible(true)}
          style={{
            marginRight: globals.screenWidth * 0.04,
            alignItems: 'center',
            marginTop:
              iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenHeight * 0.03
                : null,
          }}
        >
          <Image
            resizeMode={"contain"}
            source={images.headerIcon.searchIcon}
            style={{
              height:
                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                  ? globals.screenHeight * 0.03
                  : globals.screenHeight * 0.025,
              width:
                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                  ? globals.screenHeight * 0.03
                  : globals.screenHeight * 0.025,
              tintColor: colors.warmBlue,
              alignSelf: 'center',
            }}
          />
        </TouchableOpacity>
      </View>
    ),
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      startDate: '',
      endDate: '',
      // startDisplayDate: moment()
      //   .format('DD MMM YY')
      //   .toUpperCase(),
      // startDisplayDay: moment()
      //   .format('dddd')
      //   .toUpperCase(),
      // endDisplayDate: moment()
      //   .format('DD MMM YY')
      //   .toUpperCase(),
      // endDisplayDay: moment()
      //   .format('dddd')
      //   .toUpperCase(),
      startDisplayDate: '',
      startDisplayDay: '',
      endDisplayDate: '',
      endDisplayDay: '',
      isShowSearch: false,
      pageNumber: 1,
      pageSize: 10,
      eventName: '',
      eventType: '',
      eventStartDate: '',
      eventEndDate: '',
      imageName: '',
      EventNameSearch: '',
      EventCategorySearch: 0,
      EventLocationSearch: '',
      EventCriteria: 'ALL',
      eventsList: [],
      totalevents: 0,
      serverErr: false,
      isInternetFlag: true,
      FromDateSearch: '',
      ToDateSearch: '',
      isFetchingEvent: false,
      loading: false,
      loadingPaginationUpcoming: false,


    };
  }

  setModalVisible(visible) {
    this.setState({ isShowSearch: visible });
  }

  afterDateSelection(startDate, endDate) {
    

    const stdate = moment(startDate, 'YYYY-MM-DD').format('DD MMM YY');
    const enddate = moment(endDate, 'YYYY-MM-DD').format('DD MMM YY');

    const stDay = moment(startDate, 'YYYY-MM-DD').format('dddd');
    const edDay = moment(endDate, 'YYYY-MM-DD').format('dddd');

    this.setState({
      startDisplayDate: stdate.toUpperCase(),
      endDisplayDate: enddate.toUpperCase(),
      startDisplayDay: stDay.toUpperCase(),
      endDisplayDay: edDay.toUpperCase(),
    });
  }

  onStartDayPress(day) {
    console.log(TAG, `onStartDayPress : ${JSON.stringify(day)}`);
    const stdate = moment(day.dateString, 'YYYY-MM-DD').format('DD MMM YY');
    const stDay = moment(day.dateString, 'YYYY-MM-DD').format('dddd');

    this.setState({
      startDisplayDate: stdate.toUpperCase(),
      endDisplayDate: stdate.toUpperCase(),
      startDisplayDay: stDay.toUpperCase(),
      endDisplayDay: stDay.toUpperCase(),
    });
  
  }

  componentDidMount() {
    this.props.showLoader();
    this.makeApiCall();
  }

  /**
   * prepareData when call the API
   */
  prepareData() {
    const {
      pageNumber,
      pageSize,
      EventNameSearch,
      EventCategorySearch,
      EventLocationSearch,
      EventCriteria,
      FromDateSearch,
      ToDateSearch,
    } = this.state;

    const data = {};
    data.EventCriteria = EventCriteria;
    data.PageNumber = pageNumber;
    data.PageSize = pageSize;
    data.EventNameSearch = EventNameSearch;
    data.EventCategorySearch = EventCategorySearch;
    data.EventLocationSearch = EventLocationSearch;
    data.FromDateSearch = FromDateSearch;
    data.ToDateSearch = ToDateSearch;
    return data;
  }

  /**
   * API call
   */
  makeApiCall() {
    if (globals.isInternetConnected == true) {
      this.setState({ loadingPaginationUpcoming: true, loading: true, isInternetFlag: true })
      AsyncStorage.multiGet([globals.LOGINACCESSTOKEN, globals.LOGINUSERID]).then(token => {
        globals.tokenValue = token[0][1];
        globals.userID = token[1][1];
        API.getMyEvents(this.getMYEventResponseData, this.prepareData(), true);
      });
    } else {
      this.props.hideLoader()
      this.setState({ loading: false, isInternetFlag: false })
      // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  updatePageCount(count, callback) {
    this.setState({ totalevents: count }, () => {
      callback();
    });
  }

  /**
   * method for checkDuplicateValue
   */
  checkDuplicateValue(value, tempArray) {
    let found = false;
    for (var i = 0; i < tempArray.length; i++) {
      if (tempArray[i].title == value) {
        found = true;
        break;
      }
    }
    return found;
  }


  /**
*  call when _sessionOnPres
*/
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  checkUniquiness(dataToCheck, keyName, valueToSearch) {
    let isExist = false;
    const foundIndex = dataToCheck.findIndex(item => item[keyName] == valueToSearch);
    if (foundIndex == -1) {
      isExist = false;
    } else {
      isExist = true;
    }
    return isExist;
  }

  getMYEventResponseData = {
    success: response => {
      console.log(
        TAG,
        'getUpcomingGlobalEventResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        if (this.state.pageNumber == 1) {
          let events = response.Data.EventsList
          let isFound = this.checkDuplicateValue(events.EventID, events);
          if (isFound) {
            this.setState({
              loading: false, isFetchingEvent: true, loadingPagination: false,
              eventsList: response.Data.EventsList,
            })
          }

        }
        else {
          let events1 = [...this.state.eventsList, ...response.Data.EventsList]
          let isFound = this.checkDuplicateValue(events1.EventID, events1);
          if (!isFound) {
            this.setState({
              loading: false, isFetchingEvent: true, loadingPagination: false,
              eventsList: [...this.state.eventsList, ...response.Data.EventsList],
            })
          }
        }

      } else {
        this.setState({ loading: false, loadingPaginationUpcoming: false });
        Alert.alert(globals.appName, response.Message);
      }
      this.setState({ loading: false, isFetchingEvent: true })
    },
    error: err => {
      console.log(
        TAG,
        'getUpcomingGlobalEventResponseData -> success : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
      this.setState({ serverErr: true, loadingPaginationUpcoming: false, isFetchingEvent: true });
      this.setState({ loading: false });

    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  searchEvent(startDisplayDate, endDisplayDate) {
    _this.setState(
      {
        FromDateSearch: startDisplayDate,
        ToDateSearch: endDisplayDate,
      },
      () => {
        _this.makeListFresh(() => {
          _this.props.showLoader();
          _this.makeApiCall();
        });
      }
    );
  }

  makeListFresh(callback) {
    this.setState(
      {
        pageNumber: 1,
        pageSize: 20,
        EventCategorySearch: 0,
        EventCriteria: 'ALL',
        eventsList: [],
        isFetchingEvent: false,
        startDisplayDate: '',
        startDisplayDay: '',
        endDisplayDate: '',
        endDisplayDay: '',
      },
      () => {
        callback();
      }
    );
  }

  seacrhByDate() {
    const { startDisplayDate, endDisplayDate } = this.state;
    this.setModalVisible(false);
    this.searchEvent(startDisplayDate, endDisplayDate);
  }

  clearfileds() {
    this.setState({
      startDate: '',
      endDate: '',
      // startDisplayDate: moment()
      //   .format('DD MMM YY')
      //   .toUpperCase(),
      // startDisplayDay: moment()
      //   .format('dddd')
      //   .toUpperCase(),
      // endDisplayDate: moment()
      //   .format('DD MMM YY')
      //   .toUpperCase(),
      // endDisplayDay: moment()
      //   .format('dddd')
      //   .toUpperCase(),
      startDisplayDate: '',
      startDisplayDay: '',
      endDisplayDate: '',
      endDisplayDay: '',
      isShowSearch: false,
      pageNumber: 1,
      pageSize: 10,
      eventName: '',
      eventType: '',
      eventStartDate: '',
      eventEndDate: '',
      imageName: '',
      EventNameSearch: '',
      EventCategorySearch: 0,
      EventLocationSearch: '',
      EventCriteria: 'ALL',
      eventsList: [],
      totalevents: 0,
      serverErr: false,
      FromDateSearch: '',
      ToDateSearch: '',
      isFetchingEvent: false,
      loading: true,
      loadingPaginationUpcoming: false,
    })
  }

  onCloseModal(){
    this.setModalVisible(false);
    this.clearfileds();
    this.props.showLoader();
    this.makeApiCall()
  }

  renderSearchModel() {
    const { startDisplayDate, endDisplayDate, startDisplayDay, endDisplayDay } = this.state;

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.isShowSearch}
        onRequestClose={() => {
          this.setModalVisible(false);
        }}
      >
        <View style={styles.modelParentStyle}>
          <NavigationEvents
            onWillFocus={() => this.makeApiCall()}
            onWillBlur={() => this.clearfileds()}
          />
          <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
            <TouchableOpacity onPress={() => this.onCloseModal()}>
              <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
            </TouchableOpacity>
            <Text style={styles.tvSearchEventStyle}>
              {globals.MESSAGE.EVENTDASHBOARD.SEARCH_EVENT_TITLE}
            </Text>
          </View>
          <View>
            <Text
              style={[styles.tvSearchEventStyle, { marginBottom: globals.screenHeight * 0.027, fontSize:globals.font_14 }]}
            >
              {globals.MESSAGE.EVENTDASHBOARD.SEARCH_BY_EVENT_DATE}
            </Text>

            <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
              <View style={[styles.vwDateBoxStyle, styles.vwFlexDirectionRowStyle]}>
                <View style={[styles.vwBoxStyle, styles.vwRightMarginStyle]}>
                  <Text style={styles.dateTextStyle}>
                    {globals.MESSAGE.EVENTDASHBOARD.START_DATE}
                  </Text>
                  <View style={[styles.vwBelowLabelMarginStyle, styles.vwFlexDirectionRowStyle]}>
                    <View style={[styles.vwBoxImParentsStyle, styles.vwFlexDirectionRowStyle]}>
                      <Image

                        source={images.calenderImage.calenderIcon}
                        style={styles.ivBoxImageStyle}
                      />
                      <View style={styles.dateViewStyles}>
                        {(startDisplayDay == '') ?
                          <Text style={[styles.vwDateSelectionLableStyle,{marginTop: globals.screenHeight * 0.018, marginLeft: globals.screenWidth * 0.04}]}>{'-'}</Text>
                          :
                          <Text style={styles.vwDateSelectionLableStyle}>
                            {startDisplayDay}
                            {','}
                          </Text>
                        }
                        <Text style={styles.vwDateSelectionLableStyle}>{startDisplayDate}</Text>
                      </View>
                    </View>
                  </View>
                </View>

                <View
                  style={[styles.vwBoxStyle, styles.vwLeftMarginStyle, styles.vwRightMarginStyle]}
                >
                  <Text style={styles.dateTextStyle}>
                    {globals.MESSAGE.EVENTDASHBOARD.END_DATE}
                  </Text>
                  <View style={[styles.vwBelowLabelMarginStyle, styles.vwFlexDirectionRowStyle]}>
                    <View style={[styles.vwBoxImParentsStyle, styles.vwFlexDirectionRowStyle]}>
                      <Image
                        source={images.calenderImage.calenderIcon}
                        style={styles.ivBoxImageStyle}
                      />
                      <View style={styles.dateViewStyles}>
                        {(endDisplayDay == '') ?
                          <Text style={[styles.vwDateSelectionLableStyle,{marginTop: globals.screenHeight * 0.018, marginLeft: globals.screenWidth * 0.04}]}>{'-'}</Text>
                          :
                          <Text style={styles.vwDateSelectionLableStyle}>
                            {endDisplayDay}
                            {','}
                          </Text>
                        }
                        <Text style={styles.vwDateSelectionLableStyle}>{endDisplayDate}</Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>

              <View style={styles.vwLineStyle} />

              <View
                style={{
                  marginHorizontal: globals.screenHeight * 0.0662,
                  alignItems: 'center',
                  backgroundColor: colors.blue,
                }}
              >
                <DateRangePicker
                  initialRange={[startDisplayDate, endDisplayDate]}
                  onSuccess={(s, e) => this.afterDateSelection(s, e)}
                  onStartDayPress={day => this.onStartDayPress(day)}
                  theme={{
                    markColor: colors.white,
                    markTextColor: colors.blue,
                    calendarBackground: colors.darkSkyBlue,
                    monthTextColor: colors.white,
                    textDisabledColor: colors.lightBorder,
                    dayTextColor: colors.white,
                    arrowColor: colors.white,
                  }}
                />
              </View>
            </ScrollView>
          </View>
          <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByDate()}>
            <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }

  /**
   * method for Image Height-Width
   */
  setImageParentParam(event) {
    const { x, y, width, height } = event.nativeEvent.layout;

    let finalValue = height < width ? height : width;
    finalValue -= globals.screenHeight * 0.01;

    this.setState({ imageHeight: finalValue, imageWidth: finalValue, imageRadius: finalValue / 2 });
  }

  /**
   * method for setSelectionUpcomingEvent Particular Item
   */
  setSelectionEventList(item, index) {
    const { eventsList } = this.state;
    const dataEventsList = eventsList;
    for (let i = 0; i < dataEventsList.length; i++) {
      if (index == i) {
        dataEventsList[index].isSelected = true;
        this.props.navigation.navigate('GLOBAL_EVENT_SCREEN_DASHBOARD', {
          selectedEventList: item,
          itisFrom:'myEvent'
        });
      } else {
        dataEventsList[i].isSelected = false;
      }
    }
    this.setState({ eventsList: dataEventsList }, () => { });
  }

  /**
   * Render FlatList Items
   */
  eventItemUI(item, index) {
    const isEventStartDate = globals.checkObject(item, 'EventStartDate');
    const isEventEndDate = globals.checkObject(item, 'EventEndDate');
    const isEventName = globals.checkObject(item, 'EventName');
    const isEventType = globals.checkObject(item, 'EventType');
    const isImage = globals.checkImageObject(item, 'ImageName');
    const isStartDate = globals.checkObject(item, 'StartDate');
    const isEndDate = globals.checkObject(item, 'EndDate');
    const startDate = (isStartDate) ? item.StartDate : '-';
    const endDate = (isEndDate) ?  item.EndDate : '-';
    const starttime = (isEventStartDate) ? moment(item.EventStartDate).format('LT') : '-';
    const endtime = (isEventEndDate) ? moment(item.EventEndDate).format('LT') : '-';
    const month = (isEventStartDate) ? moment(item.EventStartDate).format('MMM') : '-';
    const thDate = (isEventStartDate) ? moment(item.EventStartDate).format('Do') : '-';
    const onlyDt = (isEventStartDate) ? moment(item.EventStartDate).format('D') : '-';
    const { imageHeight, imageWidth, imageRadius } = this.state;

    return (
      <View>
        <View
          style={[
            styles.vwItemParentPaddingStyle,
            { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },
          ]}
        >
          <TouchableOpacity onPress={() => this.setSelectionEventList(item, index)}>
            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
              <View style={styles.beforeImgViewStyle}>
                <Image
                  source={{ uri: isImage ? item.ImageName : globals.Event_img }}
                  style={[styles.ivItemImageStyle]}
                />
              </View>
              <View style={[styles.vwItemInfoStyle]}>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.tvEventNameStyle,
                    { width:"95%" },
                    { color: item.isSelected == true ? colors.white : colors.black },
                  ]}
                >
                  {isEventName ? item.EventName : '-'}
                </Text>
                <Text
                  style={[
                    styles.tvTimeStyle,
                    { width:"95%" },
                    { color: item.isSelected == true ? colors.white : colors.darkgrey },
                  ]}
                >{`${startDate} To ${endDate}`}</Text>
                <Text
                  style={[
                    styles.tvTypeStyle,
                    { flex: 1 },
                    { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                  ]}
                >
                  {isEventType ? item.EventType : '-'}
                  {}
                </Text>
              </View>

              {/* <View style={styles.vwItemDateStyle}>
                <Text
                  style={[
                    styles.tvDateStyle,
                    { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                  ]}
                >
                  {month}
                </Text>
                <View style={styles.vwDateSubViewStyle}>
                  <Text
                    style={[
                      styles.tvLabelStyle,
                      { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                    ]}
                  >
                    {onlyDt}
                  </Text>
                  <Text
                    style={[
                      styles.tvThStyle,
                      { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                    ]}
                  >
                    {thDate.charAt(thDate.length - 2)}
                    {thDate.charAt(thDate.length - 1)}
                  </Text>
                </View>
              </View> */}
            </View>
          </TouchableOpacity>
        </View>
        <View style={[styles.vwSimpleLineStyle, { backgroundColor: colors.gray }]} />
      </View>
    );
  }

  handleLoadMore = () => {
    if (!this.onEndReachedCalledDuringMomentum) {

      this.setState({
        pageNumber: this.state.pageNumber + 1,
      }, () => {
        _this.makeApiCall();
      })
      this.onEndReachedCalledDuringMomentum = true;
    }
  };

  /*
   * Try again method
   */
  _tryAgain() {
    this.setState({ serverErr: false }, () => {
      this.props.showLoader();
      this.makeApiCall();
    });

  }

  renderFooter = () => {
    if (this.state.loadingPagination && this.state.isFetchingEvent) {
      return (
       
            <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
         

      )
    } else {
      return (
        <View />
      )
    }
  };

  render() {
    const { eventsList, serverErr, loading, isInternetFlag } = this.state;


    return (
      <View style={styles.mainParentStyle}>
        {this.renderSearchModel()}
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            serverErr === false ? (
              this.state.isFetchingEvent === false ? null : this.state.isFetchingEvent === true &&
                eventsList.length > 0 ? (
                  <FlatList
                    style={styles.flatlistStyle}
                    data={eventsList}
                    renderItem={({ item, index }) => this.eventItemUI(item, index)}
                    extraData={this.state}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.2}
                    ListFooterComponent={this.renderFooter}
                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                  />
                ) : (
                  
                  <View style={globalStyles.nodataStyle}>
                    <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.EVENT_ERR.MYEVENT_DATA_NOT_AVLB}</Text>
                  </View>
                )
            ) : (
                <ServerError loading={false} onPress={() => this._tryAgain()} />
              )}
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyEvents);
