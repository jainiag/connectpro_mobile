/* eslint-disable class-methods-use-this */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import moment from 'moment';
import styles from './style';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as globals from '../../../../../utils/globals';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import globalStyles from '../../../../../assets/styles/globleStyles';

const TAG = '==:== Agenda : ';
let _this;
class AgendaScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Agenda'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      isFreeStatus: this.props.navigation.state.params.isFreeStatus,
      agendasData: this.props.navigation.state.params.agendasData,
      eventID: this.props.navigation.state.params.eventID,
      allEventDetails: this.props.navigation.state.params.allEventDetails
    };
  }

  /**
  * render FlatLst data
  */
  renderItemList(item, index) {
    const isAgendaStartDateStr = globals.checkObject(item, 'AgendaStartDateStr');
    const isAgendaEndDateStr = globals.checkObject(item, 'AgendaEndDate');
    const isDescription = globals.checkObject(item, 'Description');
    const isVenue = globals.checkObject(item, 'Venue');
    const starttime = (isAgendaStartDateStr) ? moment(item.AgendaStartDateStr).format('LT') : '-';
    const endtime = (isAgendaEndDateStr) ? moment(item.AgendaEndDate).format('LT') : '-';
    const month = (isAgendaStartDateStr) ? moment(item.AgendaStartDateStr).format('MMM') : '-';
    const thDate = (isAgendaStartDateStr) ? moment(item.AgendaStartDateStr).format('Do') : '-';
    const onlyDt = (isAgendaStartDateStr) ? moment(item.AgendaStartDateStr).format('D') : '-';
    const year = (isAgendaStartDateStr) ? moment(item.AgendaStartDateStr).format('YYYY') : '-';

    return (
      <View>
        <View style={styles.mainParentStyle}>
          <View style={styles.dateMonthViewContainer}>
            <Text style={styles.dateMonthTextStyle}>{onlyDt}
            </Text>
            <Text style={styles.tvthStyle}>{thDate.charAt(thDate.length - 2)}{thDate.charAt(thDate.length - 1) + " "}</Text>
            <Text style={styles.dateMonthTextStyle}>{month},{" "}</Text>
            <Text style={styles.dateMonthTextStyle}>{year}</Text>
          </View>
          <View style={styles.timeTextViewContainer}>
            <Text style={styles.startAndEndTimeTextStyle}>{starttime}</Text>
            <Text style={{ alignSelf: 'center' }}> - </Text>
            <Text style={styles.startAndEndTimeTextStyle}>{(isAgendaStartDateStr) ? item.AgendaEndDateStr : '-'}</Text>
          </View>
          <Text style={styles.titleStyle}>{(isVenue) ? "Venue: " + item.Venue : "Venue: " + '-'}</Text>
          <Text style={styles.descriptionStyle}>{(isDescription) ? item.Description == "" ? ' -' : item.Description : '-'}</Text>
          {/* <View style={styles.userNameViewStyle}>
            <Text style={[styles.byTextStyle, { color: colors.textBlackColor }]}>By</Text>
            <Text style={[styles.byTextStyle, { color: colors.listSelectColor }]}>{globals.loginFullName}</Text>

          </View> */}
        </View>
        <View style={styles.dashLineViewStyle} />
      </View>
    );
  }

  render() {
    const { agendasData } = this.state;
    return (
      <>
        {

          (agendasData == '') ?
            <View style={globalStyles.nodataStyle}>
              <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.AGENDA_DATA_NOT_AVLB}</Text>
            </View> :
            <View style={styles.mainHeaderStyle}>

              <FlatList
                style={styles.flatlistStyle}
                data={agendasData}
                renderItem={({ item, index }) => this.renderItemList(item, index)}
                keyExtractor={(index, item) => item.toString()}
                extraData={this.state}
              />
              <TouchableOpacity style={styles.registerBtnStyle} onPress={() => this.props.navigation.navigate('EVENT_REGISTRATION', { dataEventsList: this.state.isFreeStatus, eventID: this.state.eventID, allEventDetails: this.state.allEventDetails })}>
                <Text style={styles.registerTextStyle}>{globals.BTNTEXT.REGISTERSCREEN.REG_BTNTEXT}</Text>
              </TouchableOpacity>
            </View>
        }
      </>

    );
  }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AgendaScreen);
