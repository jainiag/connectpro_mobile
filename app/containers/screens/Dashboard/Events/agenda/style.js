import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';


const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  nodataStyle:{
    alignItems:'center',
    justifyContent:'center',
    flex:1
  },
  nodataTextStyle:{
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28: globals.font_14
  },
  mainHeaderStyle: {
    flex: 1,
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.03
        : globals.screenHeight * 0.019,
  },
  tvTitleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_20,
    color: 'blue',
  },
  dateMonthViewContainer: {
    flexDirection: 'row',
    marginTop: 20,
    marginHorizontal: 15,
  },
  dateMonthTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
    color: colors.warmBlue,
  },
  tvthStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.warmBlue,
    paddingBottom: 2,
  },
  timeTextViewContainer: {
    flexDirection: 'row',
    marginHorizontal: 15,
    marginBottom: 10,
    marginTop: 5,
  },
  startAndEndTimeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    color: colors.textBlackColor,
  },
  titleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    color: colors.darkSkyBlue,
    marginHorizontal: 15,
    marginBottom: 5,
  },
  descriptionStyle: {
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    color: colors.textBlackColor,
    marginHorizontal: 15,
    marginBottom: 10,
  },
  userNameViewStyle: {
    flexDirection: 'row',
    marginHorizontal: 15,
    marginBottom: 10,
  },
  byTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_16,
    fontWeight: '500',
    marginRight: 5,
  },
  userNameTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_16,
    color: colors.darkSkyBlue,
    fontWeight: '500',
  },
  dashLineViewStyle: {
    width: '100%',
    height: 0.5,
    backgroundColor: colors.gray,
    marginTop: 5,
    marginLeft: 15,
  },
  registerBtnStyle: {
    width: '100%',
    height: 55,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  registerTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_20,
    color: colors.white,
  },
  flatlistStyle: {
     marginBottom: globals.screenHeight  * 0.09
   
  },
});
