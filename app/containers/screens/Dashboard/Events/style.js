/* eslint-disable import/no-duplicates */
import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import {
  getHorizontalBaseValue,
  getVerticleBaseValue,
  screenHeight,
  screenWidth,
} from '../../../../utils/globals';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  loaderbottomview: {
    bottom: (Platform.OS == 'android') ?
      globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
  },
  flatlistStyle: {
    flex: 1,
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.025
        : DeviceInfo.getModel() == 'iPhone 5' ||
          DeviceInfo.getModel() == 'iPhone 5s' ||
          DeviceInfo.getModel() == 'iPhone SE' ||
          Platform.OS === 'android'
          ? globals.screenHeight * 0.005
          : globals.screenHeight * 0.018,
  },
  vwFlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  customHeaderStyle: {
    paddingHorizontal: screenWidth * 0.0266, // 10
  },
  tvTabTextStyle: {
    fontSize: globals.font_20,
    paddingHorizontal: screenWidth * 0.0266, // 10
  },
  tvSelectedTabStyle: {
    color: colors.blue,
  },
  tvUnSelectedTabStyle: {
    color: colors.gray,
  },
  vwTabStyle: {
    height: screenHeight * 0.0946, // 50
    paddingHorizontal: screenWidth * 0.0266, // 10
  },
  vwCenterStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedLineStyle: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth * 0.1562, // 50
    height: screenHeight * 0.0088, // 5
    borderRadius: screenHeight * 0.0044, // 2.5
    backgroundColor: colors.blue,
  },
  vwSimpleLineStyle: {
    width: '90%',
    position: 'absolute',
    bottom: 0,
    height: 1,
    opacity: 0.5,
    marginHorizontal: screenWidth * 0.0468, // 15
  },
  beforeImgViewStyle: {
    height: screenHeight * 0.0757, // 40
    width: screenHeight * 0.0757, // 40
    borderRadius: (screenHeight * 0.0948) / 2, // 25,
    marginRight: screenWidth * 0.0375, // 12
    marginLeft: screenWidth * 0.0156,
    borderColor: colors.lightBlack,
    borderWidth: 0.2,
  },
  toTabStyle: {
    alignItems: 'center',
  },
  vwImageViewStyle: {
    backgroundColor: 'yellow',
  },
  vwItemDateStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
  vwItemInfoStyle: {
    width: screenWidth * 0.6,
    justifyContent: 'center',
  },
  vwItemPaddingStyle: {
    marginHorizontal: screenWidth * 0.0468, // 15
    marginVertical: screenHeight * 0.0284, // 15
  },
  ivItemImageStyle: {
    height: screenHeight * 0.0757, // 40
    width: screenHeight * 0.0757, // 40
    borderRadius: (screenHeight * 0.0946) / 2, // 25,
    borderColor: colors.white,
    borderWidth: 0.2,
    // marginRight: screenWidth * 0.0375, // 12
    // marginLeft: screenWidth * 0.0156, // 5
  },
  vwItemParentPaddingStyle: {
    marginHorizontal: 0,
  },
  tvEventNameStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  tvTimeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
  },
  tvTypeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
  },
  tvDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  notAvailableDataViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 25,
  },
  tvpaymentTypeStyle: {
    color: colors.redColor,
    fontSize: globals.font_12,
  },
  tvthStyle: {
    fontSize: globals.font_10,
    color: colors.blue,
    paddingBottom: 2,
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tvThStyle: {
    fontSize: globals.font_10,
  },
  tvLabelStyle: {
    fontSize: globals.font_14,
  },
  modelParentStyle: {
    flex: 1,
  },
  vwHeaderStyle: {
    marginTop: globals.screenHeight * 0.08,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.07
        : globals.screenWidth * 0.07,
    alignItems: 'center',
    marginBottom: globals.screenHeight * 0.03,
  },
  tvSearchEventStyle: {
    color: colors.black,
    fontWeight: '500',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.08,
  },
  vwDateBoxStyle: {
    marginTop: getVerticleBaseValue(20),
    marginHorizontal: getHorizontalBaseValue(30),
  },
  vwBoxStyle: {
    flex: 1,
  },
  vwBelowLabelMarginStyle: {
    marginLeft: globals.screenWidth * 0.01,
    marginVertical: globals.screenWidth * 0.02,
  },
  vwDateSelectionLableStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_13,
    fontWeight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? '500' : '600',
    alignSelf: 'center',
  },
  vwBoxImParentsStyle: {
    flex: 1,
    backgroundColor: colors.darkSkyBlue,
    paddingVertical: globals.screenWidth * 0.025,
    paddingHorizontal: globals.screenWidth * 0.04,
  },
  dateViewStyles: {
    justifyContent: 'center',
  },
  ivBoxImageStyle: {
    height: globals.screenHeight * 0.05,
    width: globals.screenHeight * 0.05,
    marginRight: getHorizontalBaseValue(5),
  },
  vwLeftMarginStyle: {
    marginLeft: getHorizontalBaseValue(8),
  },
  vwRightMarginStyle: {
    marginRight: globals.screenWidth * 0.02,
  },
  dateTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_13,
  },
  vwLineStyle: {
    width: '100%',
    height: 1,
    backgroundColor: colors.lightBorder,
    marginTop: getVerticleBaseValue(5),
    marginBottom: getVerticleBaseValue(20),
  },
  calenderStyle: {
    backgroundColor: colors.blue,
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
  },
  tvThStyle: {
    fontSize: globals.font_10,
    color: colors.blue,
  },
  tvLabelStyle: {
    fontSize: globals.font_14,
    color: colors.blue,
  },
  toSearchStyle: {
    position: 'absolute',
    bottom: 0,
    height: screenHeight * 0.0662, // 35
    backgroundColor: colors.bgColor,
    alignItems: 'center',
    justifyContent: 'center',
    width: screenWidth,
  },
  tvSearchStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
  },
  vwCalenderParentStyle: {
    marginHorizontal: screenHeight * 0.0662,
    alignItems: 'center',
  },
  data_notAvlbViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  data_notAvlb: {
    textAlign: 'center',
    fontSize: globals.font_18,
  },
});
