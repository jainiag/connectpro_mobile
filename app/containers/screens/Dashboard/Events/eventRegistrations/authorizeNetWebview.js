/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, Alert } from 'react-native';
import { WebView } from 'react-native-webview';
import styles from './style';
import RNPaypal from 'react-native-paypal-lib';
import * as images from '../../../../../assets/images/map';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';

class AuthorizeNetWebview extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);


    this.state = {
      payerData:this.props.navigation.state.params.payerData,
      isRadioClick: true,
      paymentId:this.props.navigation.state.params.paymentId,
      totalPrice: this.props.navigation.state.params.ticketInfo,
      ticketInfo: this.props.navigation.state.params.ticketInfo,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      this.setState({ totalPrice: this.state.totalPrice })
    });
  }

  onError(){
    Alert.alert(globals.appName,"Something wrong",[{
        text : "OK", onPress : () =>{
            this.props.navigation.goBack()
        }
    }])
}

  render() {
    const { isRadioClick, totalPrice,paymentId } = this.state;
    // const urlData = {
    //     "PaymentMethodId": 2,
    //     "PaymentId": paymentId,
    //     "CancelUrl": "https://cpcommunityqa.azurewebsites.net/PaymentGateways/CancelTicketPurchaseFromAuthorizeHosted",
    //     "CommunityID": 1082
    //   };

    //   var urlData = ""
    //     urlData += "PaymentMethodId="+2
    //     urlData += "&PaymentId="+paymentId
    //     urlData += "&CancelUrl="+"https://cpcommunityqa.azurewebsites.net/PaymentGateways/CancelTicketPurchaseFromAuthorizeHosted"
    //     urlData += "&CommunityID="+1082

    //     console.log("urlData----> "+urlData)
    var urldata = "PaymentMethodId=2&PaymentId=11965&CancelUrl=https%3A%2F%2Fcpcommunityqa.azurewebsites.net%2FPaymentGateways%2FCancelTicketPurchaseFromAuthorizeHosted&CommunityID=1406"

    return (
      <View style={styles.mp_container}>
            <WebView
                ref={r => this.webview = r}
                style={{ width: globals.screenWidth, height: globals.screenHeight }}
                source={{
                    uri: globals.authorizeNetPaymentUrl,
                    method: 'POST',
                    body: urldata
                }}
                onLoadEnd={() => this.setState({ loading: false })}
                onError={(error) => this.onError()}
            /> 
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthorizeNetWebview);
