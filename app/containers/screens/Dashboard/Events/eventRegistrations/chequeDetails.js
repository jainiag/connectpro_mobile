/* eslint-disable react/sort-comp */
/* eslint-disable no-empty */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, TextInput, Alert, ScrollView } from 'react-native';
import DatePicker from 'react-native-datepicker';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import Validation from '../../../../../utils/validation';

export default class ChequeDetail extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Regitration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      chequeNumber: '',
      chequeDate: '',
      chequeNotes: '',
      redStarForNumber: false,
      redStarForDate: false,
    };
  }

  validateFields() {
    const { chequeNumber, chequeDate, chequeNotes } = this.state;
    if (Validation.textInputCheck(chequeNumber)) {
      this.setState({ redStarForNumber: true });
      if (Validation.isValidChequeNumber(chequeNumber)) {
        if (chequeDate) {
          this.setState({ redStarForDate: true });
          if (Validation.textInputCheck(chequeNotes)) {
            this.props.navigation.navigate('SUCCESS_PAYMENT');
          } else {
            Alert.alert(globals.appName, 'Please enter Cheque Notes...');
          }
        } else {
          Alert.alert(globals.appName, 'Please enter Cheque date');
          this.setState({ redStarForDate: false });
        }
      } else {
        Alert.alert(globals.appName, 'Please enter valid Cheque number');
        this.setState({ redStarForNumber: false });
      }
    } else {
      Alert.alert(globals.appName, 'Please enter Cheque number');
      this.setState({ redStarForNumber: false });
    }
  }

  render() {
    return (
      <View style={styles.cash_container}>
        <View style={styles.cash_makePaymentViewContainer}>
          <Text style={styles.cash_makePaymentTextStyle}>Make Payment</Text>
        </View>
        <View style={styles.Cash_DetailMainViewStyle}>
          <View style={styles.cash_DetailChildViewContainer}>
            <Text style={styles.cash_DetailTextStyle}>Cheque Details</Text>
            <View style={styles.cash_dashLineStyle} />
            <View style={styles.chequeNumberInputViewStyle}>
              <View style={styles.chequeWithRedStarViewStyle}>
                <TextInput
                  value={this.state.chequeNumber}
                  autoCapitalize="none"
                  onFocus={false}
                  returnKeyType="next"
                  keyboardType="numeric"
                  onChangeText={text => {
                    if (text.length > 0) {
                      this.setState({ redStarForNumber: true, chequeNumber: text });
                    } else {
                      this.setState({ redStarForNumber: false, chequeNumber: text });
                    }
                  }}
                  placeholderTextColor={colors.darkgray}
                  placeholder="Cheque Number"
                  style={styles.chequeNumberInputStyle}
                />
                {this.state.redStarForNumber === false ? (
                  <Text style={styles.starTextStyle}>*</Text>
                ) : null}
              </View>
            </View>
            <View style={styles.datePickerMainViewContainer}>
              <View style={styles.datePickerViewContainer}>
                <DatePicker
                  style={styles.datePickerStyle}
                  placeholder="Cheque Date"
                  androidMode="spinner"
                  showIcon={false}
                  date={this.state.chequeDate}
                  mode="date"
                  format="DD MMM YYYY"
                  confirmBtnText={globals.MESSAGE.SEARCH_SCREEN.SEARCH_CONFIRM_DATE}
                  cancelBtnText={globals.MESSAGE.SEARCH_SCREEN.SEARCH_CANCEL_DATE}
                  customStyles={{
                    placeholderText: [styles.placeHolderTextStyle],
                    dateInput: [styles.startDateInputStyle],
                    dateText: [styles.chequeDateTextStyle],
                  }}
                  onDateChange={date => this.setState({ redStarForDate: true, chequeDate: date })}
                />
                {this.state.redStarForDate === false ? (
                  <Text style={styles.starTextStyle}>*</Text>
                ) : null}
                <View>
                  <Image
                    source={images.EventRegistration.calendarGrey}
                    style={styles.calendarIconStyle}
                    resizeMode="contain"
                  />
                </View>
              </View>
            </View>
            <View style={styles.chequeNotesInputViewStyle}>
              <TextInput
                value={this.state.chequeNotes}
                autoCapitalize="none"
                onFocus={false}
                returnKeyType="done"
                onChangeText={text => this.setState({ chequeNotes: text })}
                placeholderTextColor={colors.darkgray}
                placeholder="Enter Cheque Notes"
                style={styles.chequeNotesInputStyle}
              />
            </View>
            <View style={styles.cash_ViewStyle}>
              <Text style={styles.cash_TextStyle}>$160.00</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity style={styles.cash_paymentButtonStyle} onPress={() => this.validateFields()}>
          <Text style={styles.cash_paymentTextStyle}>PAYMENT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
