/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, Alert, FlatList } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import RNPaypal from 'react-native-paypal-lib';
import * as images from '../../../../../assets/images/map';
import { API } from "../../../../../utils/api";
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';

let TAG = "MakePayment Screen ::=="

class makePayment extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);


    this.state = {
      payerData: this.props.navigation.state.params.payerData,
      isRadioClick: true,
      paymentId: this.props.navigation.state.params.paymentId,
      totalPrice: this.props.navigation.state.params.ticketInfo,
      ticketInfo: this.props.navigation.state.params.ticketInfo,
      event_CommunityId: this.props.navigation.state.params.event_CommunityId,
      PaymentMethods: [],
      paymentMethodId: 0,
      paymentIdBookingMethod: 0
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      this.setState({ totalPrice: this.state.totalPrice })
    });
    this.props.hideLoader()
    API.getPaymentMethods(this.getPaymentMethodsResponseData, this.state.event_CommunityId, null, true)
  }

  /**
  * Response callback of Get payment methods
  */
  getPaymentMethodsResponseData = {
    success: response => {
      console.log(
        TAG,
        'getPaymentMethodsResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        console.log('Response :: 200 ::', response);
        this.setState({ PaymentMethods: response.Data.PaymentMethods })
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message);
        this.props.hideLoader()
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      if (err.StatusCode == 403 || err.StatusCode == 401) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
      console.log(
        TAG,
        'getPaymentMethodsResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
*  call when _sessionOnPres
*/
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  /**
       *  method for select particular button
       */
  radioClickHandle() {
    this.setState({ isRadioClick: false });
    // this.props.navigation.navigate("AuthorizeNetWebview", { ticketInfo: this.state.ticketInfo, payerData: globals.payerInfoData, paymentId: this.state.paymentId })

  }

  /**
     *  method for select particular button
     */
  paymentPaypal(paymentMethodId, paymenId) {
    this.setState({ paymentMethodId: paymentMethodId, paymentIdBookingMethod: paymenId }, () => {
      API.getPaymentMethodConfigInfo(this.GetPaymentMethodConfigInfoResponseData, this.state.event_CommunityId, this.state.paymentMethodId, true)
    })
  }




  /**
  * Response callback of Get payment methods
  */
  GetPaymentMethodConfigInfoResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetPaymentMethodConfigInfoResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        console.log('Response :: 200 ::', response);
        RNPaypal.paymentRequest({
          clientId: 'AZQopm6TJLzJ5zQQLI0TjqmE0lqQv0JlhL9tws6uckE8CGJQRigvrNxW_cKp3g9QFSB5EIN7tLueTd6T',
          // clientId: response.Data.APISignature,
          environment: (response.Data.IsSandbox == true) ? RNPaypal.ENVIRONMENT.SANDBOX : RNPaypal.ENVIRONMENT.PRODUCTION,
          // environment: RNPaypal.ENVIRONMENT.SANDBOX,
          intent: RNPaypal.INTENT.SALE,
          price: this.state.totalPrice[0].finalTotalTicketPrice,
          currency: 'USD',
          description: `Total amount`,
          acceptCreditCards: true
        }).then(response => {
          console.log("response ", response);

          Alert.alert("", "Payment success",
            [{
              text: 'OK', onPress: () => this.props.navigation.navigate("SUCCESS_PAYMENT", {
                paymentId: this.state.paymentId, transactionId: response.response.id, transactionDate: response.response.create_time,
                payerData: this.state.payerData, ticketInfo: this.state.ticketInfo, paymentIdBookingMethod: this.state.paymentIdBookingMethod, paymentMethodId: this.state.paymentMethodId
              })
            }])
        }).catch(err => {
          Alert.alert("", "Payment canceled",
            [{ text: 'OK', }])
        })
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message);
        this.props.hideLoader()
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      if (err.StatusCode == 403 || err.StatusCode == 401) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
      console.log(
        TAG,
        'GetPaymentMethodConfigInfoResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  renderBookingMethods(item, index) {
    const { isRadioClick } = this.state;

    return (
      <View>
        {
          (item.FriendlyName.includes("PayPal")) ?
            <TouchableOpacity
              onPress={() => this.paymentPaypal(item.Id, item.PaymentID)}
            >
              <View style={[styles.mp_boxViewStyles, { borderColor: isRadioClick === true ? colors.warmBlue : colors.proUnderline }]}>
                <Image
                  resizeMode="contain"
                  style={styles.mp_logostyle}
                  source={images.Paypal.PaypalLogo} />
              </View>
            </TouchableOpacity>
            :
            (item.FriendlyName.includes("Authorize")) ?
              <TouchableOpacity onPress={() => this.radioClickHandle()}>
                <View style={[styles.mp_boxViewStyles, { borderColor: isRadioClick === false ? colors.warmBlue : colors.proUnderline }]}>
                  <Image
                    resizeMode="contain"
                    style={styles.mp_logostyle}
                    source={images.Authorised.AuthorisedLogo} />
                </View>
              </TouchableOpacity>
              : null
        }
      </View>
    )
  }

  render() {
    const { isRadioClick, totalPrice, PaymentMethods } = this.state;
    return (
      <View style={styles.mp_container}>
        <View style={styles.mp_blueView}>
          <Text style={styles.makePaymentText}>{globals.BTNTEXT.PAYMENTSCREENS.MAKE_PAYMENT}</Text>
        </View>
        <View style={styles.mp_innerContaiuner}>
          <View style={styles.mp_WhiteView}>
            <View style={styles.mp_apiView}>
              <Text style={styles.mp_apiText}>{globals.BTNTEXT.PAYMENTSCREENS.PAYMENT_METHOD}
              </Text>
            </View>
            <View style={styles.mp_buttonContainer}>{
              <View style={{ flex: 1 }}>
                <FlatList
                  data={PaymentMethods}
                  renderItem={({ item, index }) => this.renderBookingMethods(item, index)}
                  keyExtractor={(x, i) => i.toString()}
                  extraData={this.state}
                  showsVerticalScrollIndicator={false}
                />
              </View>
            }
            </View>
          </View>
        </View>
        <View style={styles.mp_bottomViewStyle}>
          <Text style={styles.bookInfo_PricetextStyle}>
            {"$" + this.state.totalPrice[0].finalTotalTicketPrice + ".00"} <Text style={styles.bookInfo_totalPriceTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.TOTAL}</Text>
          </Text>
        </View>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(makePayment);
