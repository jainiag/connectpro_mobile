/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as images from '../../../../../assets/images/map';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import { API } from "../../../../../utils/api";
import loginScreen from '../../../AuthenticationScreens/loginScreen';

var _this = null;
let TAG = "BookingInformation Screen ::=="
const iPad = DeviceInfo.getModel();
class BookingInformation extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      dataEventsList: this.props.navigation.state.params.dataEventsList,
      ticketInfo: this.props.navigation.state.params.ticketInfo,
      isFill: false,
      loading: false,
      event_CommunityId: this.props.navigation.state.params.EventDetails.CommunityId,
      payerInfo: {},
      attednees: [],
      finalTicket: [],
      paymentId: 0

    }
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      this.setState({ ticketInfo: this.state.ticketInfo })
    });
    let finalTicketArr = [];
    var minArr = []
    let array = [];
    let ticketTempArr = this.state.ticketInfo;
    for (let i = 0; i < ticketTempArr.length; i++) {
      const arrayTicket = ticketTempArr[i].arrTicket;
      const obj = { TicketId: ticketTempArr[i].ticketId, Attendees: arrayTicket }
      array.push(obj)
      this.setState({ attednees: array });
    }
    finalTicketArr.push(array)
    this.setState({ finalTicket: finalTicketArr })
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }

  /**
 * this method for when all addpayerinfo filled then navigate
 * payment screen
 */
  clickToContinue() {


    const { dataEventsList } = this.state;
    if (dataEventsList.EventType == "Free" || this.state.ticketInfo[0].finalTotalTicketPrice == 0) {
      if (this.state.ticketInfo[0].finalTotalTicketPrice === 0) {
        this.saveEventAttendeesApiCall();
      }
    } else {

      if (Object.keys(globals.payerInfoData).length === 0 && globals.payerInfoData.constructor === Object) {
        Alert.alert(globals.appName, 'Please add Payer Information');
      } else {
        this.saveEventAttendeesApiCall();
      }
    }

  }

  returnData(payerInfoData) {
    this.setState({
      payerInfo: payerInfoData,
    })
  }

  saveEventAttendeesApiCall() {
    const { payerInfo, finalTicket } = this.state;
    const value = finalTicket[0]
    let actualAmount = this.state.ticketInfo[0].finalTotalTicketPrice;
    let totalAmount = this.state.ticketInfo[0].finalTotalTicketPrice;


    const data = {
      "CommunityId": this.state.event_CommunityId,
      "AccessToken": JSON.parse(globals.tokenValue),
      "TicketAttendees": value,
      "Payer": {
        "UserID": null,
        "EmailID": (actualAmount == 0) ? null : payerInfo.Email,
        "Phone": (actualAmount == 0) ? null : payerInfo.Mobile,
        "FirstName": (actualAmount == 0) ? null : payerInfo.firstname,
        "LastName": (actualAmount == 0) ? null : payerInfo.lastname,
        "Company": (actualAmount == 0) ? null : payerInfo.Profession,
        "Comments": (actualAmount == 0) ? null : payerInfo.notes
      },
      "PaymentInfo": {
        "Currency": "USD",
        "ActualAmount": actualAmount,
        "DiscountAmount": "0",
        "TotalAmount": totalAmount,
        "DiscountCouponID": "null",
        "PaymentPurpose": "null",
        "isFromCheckIn": false
      }
    }

    this.props.showLoader();
    this.setState({ loading: true })
    if (globals.isInternetConnected === true) {
      API.saveEventAttendees(this.saveEventAttendeesResponseData, data, true, globals.userID)

    } else {
      this.setState({ loading: false })
      this.props.hideLoader();
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
 *  call when _sessionOnPres
 */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  saveEventAttendeesResponseData = {
    success: response => {
      console.log(
        TAG,
        'saveEventAttendeesResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ paymentId: response.Data.Id })

        Alert.alert(globals.appName, response.Data.Message,
          [{
            text: 'OK',
            onPress: () => {
              (this.state.ticketInfo[0].finalTotalTicketPrice === 0) ?
                this.props.navigation.navigate("SUCCESS_PAYMENT",
                  {itisFrom:'FreeEvent', ticketInfo: this.state.ticketInfo, payerData: globals.payerInfoData, paymentId: this.state.paymentId })
                :
                this.props.navigation.navigate("makePayment", {itisFrom:'PaidEvent', ticketInfo: this.state.ticketInfo, payerData: globals.payerInfoData, paymentId: this.state.paymentId, event_CommunityId: this.state.event_CommunityId })
            }
          }]);
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message);
        this.props.hideLoader()
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      if (err.StatusCode == 403 || err.StatusCode == 401) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
      console.log(
        TAG,
        'saveEventAttendeesResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };



  /**
   * Render item list booking information list
   * @param {*} item 
   * @param {*} index 
   */
  renderItemBookingInfoList(item, index) {
    return (
      (item.ticketCount > 0) ?
        <View style={styles.bookInfo_goldTicketViewContainer}>
          <View>
            <Text style={styles.bookInfo_goldTicketTextStyle}>{item.ticketName}</Text>
            <Text style={styles.bookInfo_rupeeTextStyle}>${item.ticketPrice}</Text>
          </View>
          <View style={styles.bookInfo_ticketCountViewContainer}>
            <Text style={styles.bookInfo_ticketCountTextStyle}>{item.ticketCount}</Text>
          </View>
        </View> : null
    );
  }


  /**
   * Method of navigate to add payer info screen
   */
  navigateToAddPayerInfoScreen() {
    this.props.navigation.navigate('ADD_PLAYER_INFORMATION', { item: globals.payerInfoData, returnData: this.returnData.bind(this), })
  }


  render() {
    const { ticketInfo, dataEventsList, isFreeStatus } = this.state;
    const payerInfoText = (Object.keys(globals.payerInfoData).length === 0 && globals.payerInfoData.constructor === Object) ? globals.BTNTEXT.PAYMENTSCREENS.ADD_PLAYER_INFO : "Edit Payer Information"



    return (
      <View style={styles.api_container}>

        <View style={styles.api_blueView}>
          <Text style={styles.makePaymentText}>{globals.BTNTEXT.PAYMENTSCREENS.BOOKING_INFO}</Text>
        </View>
        <View style={[styles.Cash_DetailMainViewStyle, {
          top:
            (dataEventsList.EventType == "Free" || this.state.ticketInfo[0].finalTotalTicketPrice == 0) ?
              globals.screenHeight * 0.05 : globals.screenHeight * 0.08
        }]}>

          {
            (dataEventsList.EventType == "Free" || this.state.ticketInfo[0].finalTotalTicketPrice == 0) ?
              null :
              <View style={styles.bookInfo_titleViewContainer}>
                <View style={styles.bookInfo_titleAndAddIconViewContaier}>
                  <Text style={styles.attendees_GoldTicketTextStyle}>{payerInfoText}</Text>
                  <TouchableOpacity onPress={() => this.navigateToAddPayerInfoScreen()}>
                    {
                      (Object.keys(globals.payerInfoData).length === 0 && globals.payerInfoData.constructor === Object) ?

                        <Image
                          source={images.EventRegistration.addIcon}
                          style={styles.bookInfo_AddIconStyle}
                          resizeMode="contain"
                        />
                        : <Image
                          source={images.EventRegistration.editIcon}
                          style={styles.bookInfo_AddIconStyle}
                          resizeMode="contain"
                        />
                    }

                  </TouchableOpacity>
                </View>
              </View>
          }


          <View style={[styles.bookInfo_ticketSummaryViewContainer,
          (ticketInfo.length < 4) ?
            { flex: 1 }
            :
            { height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.62 : globals.screenHeight * 0.57 }
          ]}>
            <Text style={styles.bookInfo_ticketSummaryTextStyle}>Ticket Summary</Text>
            <View style={styles.bookingInfoHorizontalLine} />
            <FlatList
              data={ticketInfo}
              overScrollMode={'always'}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) => this.renderItemBookingInfoList(item, index)}
              keyExtractor={(index, item) => item.toString()}
              extraData={this.state}
              style={{ flex: 1, marginTop: globals.screenHeight * 0.045, marginBottom: globals.screenHeight * 0.03 }}
            />
          </View>
        </View>
        <View style={styles.common_bottomViewStyle}>
          <View style={styles.common_bottomLeftView}>
            <Text style={styles.common_totalPriceStyle}>  {"$" + this.state.ticketInfo[0].finalTotalTicketPrice + ".00"}</Text>
            <Text style={styles.common_totalPriceTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.TOTAL}</Text>
          </View>
          <TouchableOpacity style={styles.common_bottomRightView} onPress={() => this.clickToContinue()}>
            <Text style={styles.common_continueTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.CONTINUE}</Text>
          </TouchableOpacity>

        </View>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingInformation);
