/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as color from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';

const iPad = DeviceInfo.getModel();
class RegistrationEvent extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });


  constructor(props) {
    super(props);
    this.state = {
      countGold: 0,
      countSilver: 0
    }
  }

  incrementGoldCount() {
    this.setState({
      countGold: this.state.countGold + 1
    })
  }

  decrementGoldCount() {
    this.setState({
      countGold: this.state.countGold - 1
    })
  }

  incrementSilverCount() {
    this.setState({
      countSilver: this.state.countSilver + 1
    })
  }

  decrementSilverCount() {
    this.setState({
      countSilver: this.state.countSilver - 1
    })
  }

  render() {
    return (
      <View style={styles.mainParentStyle}>
        <View style={styles.headerViewStyle}>
          <TouchableOpacity style={styles.headerTextViewSTyle}>
            <View>
              <Text  numberOfLines={1} style={styles.headertextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.ALREADY_REGISTRED}</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.headerTextViewSTyle}>
            <View>
              <Text numberOfLines={1} style={styles.headertextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.RESEND}</Text>
            </View>
          </TouchableOpacity>
        </View>

        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View style={styles.mainContainer}>
            <View style={styles.mainViewStyle}>
              <Text style={styles.bookticketTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.BOOK_TICKETS}</Text>
            </View>

            <View>
              <View style={styles.subContainerView}>
                <View style={styles.leftUpperSubView}>
                  <Text style={styles.ticketTypeText}>{globals.BTNTEXT.PAYMENTSCREENS.GOLD_TICKETS}</Text>
                  <Text style={styles.ticketPriceText}>$60.00</Text>
                </View>
                <View style={styles.rightUpperSubView}>
                  <AntDesign name={'up'}
                    size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
                      globals.screenWidth * 0.05 : globals.screenWidth * 0.06} onPress={() => this.incrementGoldCount()} color={color.listSelectColor} />
                  <Text style={styles.counterTextStyle}>{(this.state.countGold < 0) ? '0' : this.state.countGold}</Text>
                  <AntDesign name={'down'} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
                    globals.screenWidth * 0.05 : globals.screenWidth * 0.06} onPress={() => this.decrementGoldCount()} color={color.listSelectColor} />
                </View>
              </View>

              <View style={styles.subContainerView}>
                <View style={styles.leftUpperSubView}>
                  <Text style={styles.ticketTypeText}>{globals.BTNTEXT.PAYMENTSCREENS.SILVER_TICKETS}</Text>
                  <Text style={styles.ticketPriceText}>$60.00</Text>
                </View>
                <View style={styles.rightUpperSubView}>
                  <AntDesign name={'up'} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
                    globals.screenWidth * 0.05 : globals.screenWidth * 0.06} onPress={() => this.incrementSilverCount()} color={color.listSelectColor} />
                  <Text style={styles.counterTextStyle}>{(this.state.countSilver < 0) ? '0' : this.state.countSilver}</Text>
                  <AntDesign name={'down'} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
                    globals.screenWidth * 0.05 : globals.screenWidth * 0.06} onPress={() => this.decrementSilverCount()} color={color.listSelectColor} />
                </View>
              </View>
            </View>

            <View>
              <View style={styles.ticketListView}>
                <Text style={[styles.bottomticketTypeText,]}>{globals.BTNTEXT.PAYMENTSCREENS.STAR}</Text>
                <Text style={[styles.bottomticketPriceText,]}>$60.00</Text>
                <Text style={[styles.bottombookingStatusText,]}>{globals.BTNTEXT.PAYMENTSCREENS.BOOKING_CLOSED}</Text>
              </View>
              <View style={styles.ticketListView}>
                <Text style={[styles.bottomticketTypeText,]}>{globals.BTNTEXT.PAYMENTSCREENS.PLATINUM}</Text>
                <Text style={[styles.bottomticketPriceText,]}>$60.00</Text>
                <Text style={[styles.bottombookingStatusText,]}>{globals.BTNTEXT.PAYMENTSCREENS.SOLD_OUT}</Text>
              </View>
              <View style={styles.ticketListView}>
                <Text style={[styles.bottomticketTypeText,]}>{globals.BTNTEXT.PAYMENTSCREENS.DIMOND}</Text>
                <Text style={[styles.bottomticketPriceText,]}>$60.00</Text>
                <Text style={[styles.bottombookingStatusText,]}>{globals.BTNTEXT.PAYMENTSCREENS.BOOKING_CLOSED}</Text>
              </View>
              <View style={styles.ticketListView}>
                <View style={{ flexDirection: 'row' , marginLeft: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.028 : null}}>
                  <View style={{ padding: 13, backgroundColor: 'red', marginRight: 5 }}></View>
                  <Text style={styles.ticketTypeText}>{globals.BTNTEXT.PAYMENTSCREENS.PREMIUM}</Text>
                </View>
                <Text style={[styles.ticketPriceText,
                { paddingLeft: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.028 : globals.screenWidth * 0.01 }]}>$60.00</Text>
                <Text style={[styles.bookingStatusText, { paddingLeft: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.028 : globals.screenWidth * 0.01 }]}>Sold Out</Text>
              </View>
            </View>

          </View>
        </ScrollView>
        <View style={styles.bottomViewStyle}>
          <View style={styles.bottomLeftView}>
            <Text style={styles.totalPriceStyle}>$160.00</Text>
            <Text style={styles.totalPriceTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.TOTAL}</Text>
          </View>
          <View style={styles.bottomRightView}>
            <TouchableOpacity>
              <Text style={styles.continueTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.CONTINUE}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationEvent);