/* eslint-disable no-useless-constructor /
/ eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, ScrollView, SafeAreaView, FlatList, BackHandler, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import { API } from '../../../../../utils/api';


let TAG = "BookingReceipt Screen:===="
const iPad = DeviceInfo.getModel();

class BookingReceipt extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration', "Receipt"),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });
  constructor(props) {
    super(props);
    this.state = {
      transactionId: this.props.navigation.state.params.transactionId,
      paymentId: this.props.navigation.state.params.paymentId,
      payerInfo: this.props.navigation.state.params.payerInfo,
      BookingResultData: {},
      serverErr: false,
      loading: false,
      transactionDate: this.props.navigation.state.params.transactionDate,
      newTicketSummary: [],
      ticketInfo: this.props.navigation.state.params.ticketInfo,
      paymentIdBookingMethod: this.props.navigation.state.params.paymentIdBookingMethod,
      paymentMethodId: this.props.navigation.state.params.paymentMethodId
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    this.makeAPICall()
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    this.props.navigation.navigate("GLOBAL_EVENT_SCREEN_DASHBOARD")
  }

  makeAPICall() {

    if (globals.isInternetConnected === true) {
      this.props.showLoader();
      if (this.state.transactionId == null || this.state.transactionId == undefined) {
        API.getPaymentSummary(this.paymentSummaryResponse, this.state.paymentId, null);
      }
      else {
        API.getPaymentSummary(this.paymentSummaryResponse, this.state.paymentId, this.state.transactionId);
      }

    } else {
      this.props.hideLoader();
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
Response callback of eventAttendeses
*/
  paymentSummaryResponse = {
    success: response => {
      console.log(
        TAG,
        'paymentSummaryResponse -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ BookingResultData: response.Data }, () => {
          if (response.Data.PaymentSummary.TotalAmount > 0) {
            const data = {
              PaymentMethodId: this.state.paymentMethodId,
              PaymentId: this.state.paymentIdBookingMethod,
              totalAmount: response.Data.PaymentSummary.TotalAmount,
              currency: "USD",
              transationId: this.state.transactionId,
              transactionDate: response.Data.PaymentSummary.TransactionDate,
              paymentStatus: response.Data.PaymentSummary.PaymentStatus,
              jsonString: "String",
              CashNotes: "String",
              ChequeNumber: "String",
              ChequeDate: "2020-02-24T05:48:53.296Z",
              ChequeNotes: "String"
            };
            API.saveEventTicketPaymentInformation(this.saveEventTicketPaymentInformationResponseData, data, false)
          }
        })
      } else {
        this.setState({ serverErr: true, loading: false })
        Alert.alert(globals.appName, response.Message);
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true, loading: false })
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
      console.log(
        TAG,
        'paymentSummaryResponse -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  /**
   * response of save ticket payment information
   */
  saveEventTicketPaymentInformationResponseData = {
    success: response => {
      console.log(
        TAG,
        'saveEventTicketPaymentInformationResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        console.log(TAG, "saveEventTicketPaymentInformationResponseData response success ::")
      } else {
        this.setState({ serverErr: true, loading: false })
        Alert.alert(globals.appName, response.Message);
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true, loading: false })
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
      console.log(
        TAG,
        'saveEventTicketPaymentInformationResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  /**
     *  call when _sessionOnPres
     */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }


  renderTicket(item, index) {
    return (
      <View>
        <Text numberOfLines={1} style={[styles.bookingRecOtherTextsBlackmargin]}>{item.TicketName}</Text>
      </View>
    );
  }
  renderPrice(item, index) {
    return (
      <View>
        <Text style={styles.bookingRecOtherTextsBlackmargin}>{item.TicketPrice}</Text>
      </View>
    );
  }
  renderQuantity(item, index) {
    return (
      <View>
        {(item.ticketCount !== 0) ?
          <Text style={styles.bookingRecOtherTextsBlackmargin}>{item.ticketCount}</Text>
          :
          null
        }
      </View>
    );
  }
  renderAmount(item, index) {
    const countTicket = (item.ticketCount > 0) ? item.ticketPrice * item.ticketCount : item.ticketPrice
    return (
      <View>
        {(item.ticketCount !== 0) ?
          <Text style={styles.bookingRecOtherTextsBlackmargin}>{countTicket}</Text>
          :
          null
        }
      </View>
    );
  }
  render() {
    const { BookingResultData, payerInfo } = this.state;
    let paymentsummary = BookingResultData.PaymentSummary;
    const isEventTitle = globals.checkObject(BookingResultData.PaymentSummary, 'EventTitle');
    const isEventStartDate = globals.checkObject(BookingResultData.PaymentSummary, 'EventStartDateStr');
    const isTisAddress = globals.checkObject(BookingResultData.PaymentSummary, 'Address');
    const isBookingId = globals.checkObject(BookingResultData.PaymentSummary, 'PaymentID')
    const isTransactionDateStr = globals.checkObject(BookingResultData.PaymentSummary, 'TransactionDateStr')
    const isTransactionDate = globals.checkObject(BookingResultData.PaymentSummary, 'TransactionDate')
    const isTotalAmount = globals.checkObject(BookingResultData.PaymentSummary, 'TotalAmount')
    const TotalAmount = (isTotalAmount) ? BookingResultData.PaymentSummary.TotalAmount : '-';
    const bookingId = (isBookingId) ? BookingResultData.PaymentSummary.PaymentID : '-';
    const TransactionDate = (isTransactionDateStr) ? BookingResultData.PaymentSummary.TransactionDateStr : '-'
    const isProfession = globals.checkObject(payerInfo, 'Profession');
    const isemail = globals.checkObject(payerInfo, 'email');
    const ismobNo = globals.checkObject(payerInfo, 'mobNo');
    const isfirstName = globals.checkObject(payerInfo, 'firstName');
    const islastName = globals.checkObject(payerInfo, 'lastName');
    if (this.state.transactionId) {
      const transactionIdinString = JSON.stringify(this.state.transactionId)
      const itistransactionId = transactionIdinString.substring(5);
      var finalTransactionId = itistransactionId.replace(/"/, '');
    }

    if (this.state.transactionDate) {
      // var finalTransactionDate = this.state.transactionDate.replace(/T|Z/gi, ' ')
      var starttime = moment(this.state.transactionDate).format('LT');
      var month = moment(this.state.transactionDate).format('MM');
      var year = moment(this.state.transactionDate).format('YYYY');
      // var thDate = moment(this.state.transactionDate).format('Do');
      var onlyDt = moment(this.state.transactionDate).format('D');
    }





    return (
      <SafeAreaView style={{ flex: 1, }}>
        <View style={styles.mainBookingReceipt}>
          <View style={styles.bookingReceiptTitleView}>
            <Text style={styles.bookingReceiptTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.BOOKING_RECEIPT}</Text>
          </View>
          <View style={styles.mainBookingFormView}>
            <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
              <Text style={styles.bookingRecHi}>Hi <Text style={[styles.bookingRecHi, { color: colors.warmBlue }]}>{globals.loginFullName + ","}</Text></Text>
              <Text style={[styles.bookingRecOtherTextsBlack]}>This is the receipt for a payment of <Text style={[styles.bookingRecOtherTextsBlack, { color: colors.listSelectColor }]}>{"$" + TotalAmount + ".00"}</Text> for {(paymentsummary != null && paymentsummary != undefined) ? (paymentsummary.EventTitle != null && paymentsummary.EventTitle != undefined) ? BookingResultData.PaymentSummary.EventTitle : '' : ''} </Text>
              <View style={styles.grayLineView}></View>
              <View style={styles.horizontalPart}>
                <View style={styles.halfViewBookingTickets}>

                  {
                    (this.state.transactionId) ?
                      <View>
                        <Text style={styles.BookingRecheadingText}>Transaction ID</Text>
                        <Text numberOfLines={2} style={styles.bookingRecOtherTextsBlack}>{finalTransactionId}</Text>
                      </View>
                      :
                      <View>
                        <Text style={styles.BookingRecheadingText}>Booking ID</Text>
                        <Text numberOfLines={1} style={styles.bookingRecOtherTextsBlack}>{bookingId}</Text>
                      </View>


                  }

                </View>

                {
                  (this.state.transactionDate) ?
                    <View style={[styles.halfViewBookingTickets, { alignItems: 'flex-end', flex: 1 }]}>
                      <Text style={styles.BookingRecheadingText}>Transaction Date</Text>
                      <Text style={styles.bookingRecOtherTextsBlack}>{onlyDt + "/" + month + "/" + year + " " + starttime}</Text>
                    </View>
                    :
                    <View style={[styles.halfViewBookingTickets, { alignItems: 'flex-end', flex: 1 }]}>
                      <Text style={styles.BookingRecheadingText}>Booking Date</Text>
                      <Text style={styles.bookingRecOtherTextsBlack}>{TransactionDate}</Text>
                    </View>
                }

              </View>
              <View style={styles.grayLineView}></View>

              {
                (TotalAmount == 0) ?
                  null :
                  <>
                    <Text style={styles.BookingRecheadingText}>Payer</Text>
                    <Text style={[styles.bookingRecOtherTextsBlack]}>
                      {(isfirstName) ? payerInfo.firstName : '-'}{' '}{(islastName) ? payerInfo.lastName : '-'}
                    </Text>
                    <Text style={styles.bookingRecOtherTextsBlack}>{(isProfession) ? payerInfo.Profession : '-'}</Text>
                    <Text style={styles.bookingRecOtherTextsBlack}>{(isemail) ? payerInfo.email : "-"}</Text>
                    <Text style={styles.bookingRecOtherTextsBlack}>{(ismobNo) ? payerInfo.mobNo : "-"}</Text>
                  </>
              }
              {
                (TotalAmount == 0) ?
                  null :
                  <View style={styles.grayLineView}></View>
              }
              <Text style={styles.BookingRecheadingText}>Event</Text>
              <Text style={styles.bookingRecOtherTextsBlack}>{((paymentsummary != null && paymentsummary != undefined)) ? (paymentsummary.EventTitle != null && paymentsummary.EventTitle != undefined) ? BookingResultData.PaymentSummary.EventTitle : '' : ''}</Text>
              <View style={styles.grayLineView}></View>
              <Text style={styles.BookingRecheadingText}>Date & Time</Text>
              <Text style={styles.bookingRecOtherTextsBlack}>{((paymentsummary != null && paymentsummary != undefined)) ? (paymentsummary.EventStartDateStr != null && paymentsummary.EventStartDateStr != undefined) ? BookingResultData.PaymentSummary.EventStartDateStr : '' : ''}</Text>
              <View style={styles.grayLineView}></View>
              <Text style={styles.BookingRecheadingText}>Venue</Text>
              <Text style={styles.bookingRecOtherTextsBlack}>{((paymentsummary != null && paymentsummary != undefined)) ? (paymentsummary.Address != null && paymentsummary.Address != undefined) ? BookingResultData.PaymentSummary.Address : '' : ''}</Text>
              <View style={styles.grayLineView}></View>
              <View style={styles.horizontalPart}>
                <View style={[styles.fourHalfViewBookingTickets, { alignItems: 'center' }]}>
                  <Text style={[styles.bookingRecHi, { color: colors.warmBlue }]}>Ticket</Text>
                  <FlatList
                    data={BookingResultData.TicketsSummary}
                    renderItem={({ item, index }) => this.renderTicket(item, index)}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                    style={{ alignSelf: 'center' }}
                  />

                </View>
                <View style={[styles.fourHalfViewBookingTickets, { alignItems: 'center' }]}>
                  <Text style={[styles.bookingRecHi, { color: colors.warmBlue }]}>Price</Text>
                  <FlatList
                    data={BookingResultData.TicketsSummary}
                    renderItem={({ item, index }) => this.renderPrice(item, index)}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                  />
                </View>
                <View style={[styles.fourHalfViewBookingTickets, { alignItems: 'center' }]}>
                  <Text style={[styles.bookingRecHi, { color: colors.warmBlue, textAlign: 'center' }]}>Quantity</Text>
                  <FlatList
                    data={this.state.ticketInfo}
                    renderItem={({ item, index }) => this.renderQuantity(item, index)}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                  />
                </View>
                <View style={[styles.fourHalfViewBookingTickets, { alignItems: 'center' }]}>
                  <Text style={[styles.bookingRecHi, { color: colors.warmBlue, textAlign: 'right' }]}>Amount</Text>
                  <FlatList
                    data={this.state.ticketInfo}
                    renderItem={({ item, index }) => this.renderAmount(item, index)}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                  />
                </View>
              </View>
              <View style={styles.grayLineView}></View>
              <View style={styles.horizontalPart}>
                <View style={[styles.halfViewBookingTickets, { width: globals.screenWidth / 2.5 }]} />
                <View style={styles.bottomRightViewBookingReceipt}>
                  <Text style={[styles.bookingRecHi, { fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_15, marginTop: 2 }]}>Total Amount : <Text style={{ fontWeight: '600' }}>{"$" + TotalAmount + ".00"}</Text></Text>
                  <Text style={[styles.bookingRecHi, { fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_15, marginTop: (globals.screenHeight * 0.013), marginBottom: (globals.screenHeight * 0.025) }]}>Amount Paid : <Text style={{ fontWeight: '600' }}>{"$" + TotalAmount + ".00"}</Text></Text>
                </View>
              </View>
            </ScrollView>
          </View>
          <View style={styles.bookingBottomView}></View>
        </View>
      </SafeAreaView>
    );
  }
}
// ** Model mapping method **
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BookingReceipt);