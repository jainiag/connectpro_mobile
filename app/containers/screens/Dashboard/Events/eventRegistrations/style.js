/* eslint-disable no-nested-ternary */
import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

console.log('DeviceInfo.getModel() --->', DeviceInfo.getModel());

module.exports = StyleSheet.create({
  // ====== : Cash Details : ======

  cash_container: {
    flex: 1,
  },
  cash_makePaymentViewContainer: {
    width: '100%',
    height: globals.screenHeight * 0.12,
    backgroundColor: colors.warmBlue,
  },
  cash_makePaymentTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.white,
    marginTop: globals.screenHeight * 0.03,
    marginLeft: globals.screenWidth * 0.05,
  },
  Cash_DetailMainViewStyle: {
    alignItems: 'center',
    position: 'absolute',
    top: globals.screenHeight * 0.08,
    // flex: 1,
    // flexGrow: 1,
  },
  cash_DetailChildViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    height: globals.screenHeight * 0.7,
    marginLeft: globals.screenWidth * 0.05,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
  },
  cash_DetailTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.darkSkyBlue,
    fontWeight: Platform.OS == 'android' ? 'bold' : '500',
    marginTop: globals.screenHeight * 0.03,
    marginLeft: globals.screenWidth * 0.05,
  },
  cash_paymentButtonStyle: {
    width: '100%',
    height: globals.screenHeight * 0.06,
    position: 'absolute',
    bottom: 0,
    zIndex: 10,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cash_paymentTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.white,
    textAlign: 'center',
  },
  cash_dashLineStyle: {
    height: 1,
    width: globals.screenWidth * 0.8,
    backgroundColor: colors.proUnderline,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.024,
  },
  cash_textInputViewContainer: {
    width: globals.screenWidth * 0.8,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    height: globals.screenHeight * 0.26,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.034,
    borderRadius: 5,
  },
  cash_textInputStyle: {
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
  },
  cash_ViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.15,
  },
  cash_TextStyle: {
    color: colors.warmBlue,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 35 : globals.font_24,
    textAlign: 'center',
    fontWeight: Platform.OS == 'android' ? 'bold' : '500',
  },

  // ====== : Cheque Details : ======

  chequeNumberInputViewStyle: {
    width: globals.screenWidth * 0.8,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    height: globals.screenHeight * 0.065,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.034,
    borderRadius: 5,
  },
  chequeNumberInputStyle: {
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
  },
  chequeDateInputViewStyle: {
    width: globals.screenWidth * 0.8,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    height: globals.screenHeight * 0.065,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.03,
    borderRadius: 5,
  },
  chequeDateAndIconViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  chequeDateInputStyle: {
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
  },
  calendarIconStyle: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.06,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.06,
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.015
        : DeviceInfo.getModel() == 'iPhone 5' ||
          DeviceInfo.getModel() == 'iPhone 5s' ||
          DeviceInfo.getModel() == 'iPhone SE'
          ? globals.screenHeight * 0.009
          : Platform.OS === 'android'
            ? globals.screenHeight * 0.015
            : globals.screenHeight * 0.017,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.4
        : globals.screenWidth * 0.31,
  },
  chequeNotesInputViewStyle: {
    width: globals.screenWidth * 0.8,
    borderWidth: 1,
    borderColor: colors.proUnderline,
    height: globals.screenHeight * 0.15,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.03,
    borderRadius: 5,
  },
  chequeNotesInputStyle: {
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
  },
  chequeCashViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.1,
  },
  chequeWithRedStarViewStyle: {
    flexDirection: 'row',
    alignItems: Platform.OS == 'android' ? null : 'center',
  },
  starTextStyle: {
    color: colors.redColor,
    fontSize: globals.font_18,
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 0 : globals.screenHeight * 0.006,
  },
  datePickerViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : 20,
  },
  placeHolderTextStyle: {
    // color: colors.black,
    // fontSize: globals.font_14,
    // textAlign: 'left',
    // marginRight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.51 : globals.screenWidth * 0.4,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_16,
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.015
        : globals.screenHeight * 0.008,
    color: Platform.OS == 'android' || DeviceInfo.isTablet() ? colors.lightGray : colors.gray,
  },
  dateIconStyle: {
    position: 'absolute',
    left: 0,
    marginLeft: 0,
  },
  startDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.white,
    marginTop: 8,
    marginBottom: 8,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.069
        : Platform.OS == 'android'
          ? globals.screenHeight * 0.065
          : globals.screenHeight * 0.065,
  },
  chequeDateTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_16,
    fontWeight: '400',
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.015
        : globals.screenHeight * 0.006,
  },
  datePickerStyle: {
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.27
        : globals.screenWidth * 0.35,
  },
  datePickerViewContainer: {
    flexDirection: 'row',
  },
  datePickerMainViewContainer: {
    height: globals.screenHeight * 0.065,
    width: globals.screenWidth * 0.8,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.034,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.proUnderline,
  },

  // ======: SuccessPayment :==========

  successChildViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    height: globals.screenHeight * 0.7,
    marginLeft: globals.screenWidth * 0.05,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  successIconStyle: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.25
        : globals.screenWidth * 0.4,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.25
        : globals.screenWidth * 0.4,
    marginBottom: globals.screenHeight * 0.01,
  },
  successTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 33 : globals.font_22,
    color: colors.warmBlue,
    fontWeight: '500',
    textAlign: 'center',
    marginTop: globals.screenHeight * 0.018,
  },
  bookingTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    color: colors.warmBlue,
    textAlign: 'center',
    marginTop: globals.screenHeight * 0.018,
    opacity: 0.7,
  },

  // ===========: attendees details :=========

  attendees_GoldTicketViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    // height: globals.iPhoneX ? globals.screenHeight * 0.22 : globals.screenHeight * 0.24,
    marginLeft: globals.screenWidth * 0.05,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    flex: 1,
    marginBottom: globals.screenHeight * 0.033,
  },
  attendees_GoldTicketTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.darkSkyBlue,
    fontWeight: Platform.OS == 'android' ? 'bold' : '500',
    marginTop: globals.screenHeight * 0.03,
    marginLeft: globals.screenWidth * 0.05,
  },
  attendees_dashLine: {
    height: 1,
    width: globals.screenWidth * 0.8,
    backgroundColor: colors.proUnderline,
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.018,
    marginBottom: globals.screenHeight * 0.018,
  },
  mainViewContaier: {
    marginTop: globals.screenHeight * 0.01,
  },
  attendees_UserNameAndIconViewContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: globals.screenWidth * 0.05,
    marginBottom: globals.screenHeight * 0.024,
    flex: 1,
  },
  attendees_UserNameAndAttendeesTextStyle: {
    width:globals.screenWidth * 0.7,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_16,
    color: colors.black,
  },
  attendees_IconsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  attendees_editIconStyle: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.053,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.053,
    // marginRight: globals.screenWidth * 0.04,
  },
  attendees_deleteIconStyle: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.043
        : globals.screenWidth * 0.053,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.043
        : globals.screenWidth * 0.053,
  },
  attendees_AttendessAndIconsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: globals.screenWidth * 0.05,
  },
  attendees_addIconStyle: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.05
        : globals.screenWidth * 0.072,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.05
        : globals.screenWidth * 0.072,
    marginLeft: globals.screenWidth * 0.013,
  },
  attendees_SilverTicketViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    // height: globals.iPhoneX ? globals.screenHeight * 0.22 : globals.screenHeight * 0.24,
    marginLeft: globals.screenWidth * 0.05,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    marginTop: globals.screenHeight * 0.03,
    flex: 1,
  },
  attendeeDetail_simpleViewContainer: {
    alignItems: 'center',
    marginTop: globals.screenHeight * (-0.04),
    zIndex: 999,
    top: globals.screenHeight * 0.08,
  },
  attendeeDetail_flatlistStyle: {
    flex: 0.9,
  },

  // ====== : Edit Profile : ======
  ep_container: {
    flex: 1,
    marginTop: globals.screenHeight * 0.018,
  },
  ep_mainView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  ep_spacebetweeenView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
    width: globals.screenWidth,
    marginLeft: globals.screenWidth * 0.04,
  },
  ep_spacebetweenTitleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.darkgrey,
  },
  ep_spacebetweenContentStyle: {
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.0065,
    paddingBottom: globals.screenHeight * 0.01,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
  },
  ep_titleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.darkgrey,
    marginLeft: globals.screenWidth * 0.05,
  },
  ep_contentText: {
    marginTop: globals.screenHeight * 0.0065,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginLeft: globals.screenWidth * 0.05,
  },
  ep_contentText2: {
    marginTop: globals.screenHeight * 0.0065,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginLeft: globals.screenWidth * 0.015,
  },
  roundViewStyle: {
    marginTop: globals.screenHeight * 0.0065,
    height: globals.screenHeight * 0.025,
    width: globals.screenHeight * 0.025,
    borderColor: colors.gray,
    borderWidth: 1,
    marginLeft: globals.screenWidth * 0.05,
    borderRadius: (globals.screenHeight * 0.025) / 2,
  },
  directionView: {
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.001,
  },
  ep_textStyle: {
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.0065,
    marginLeft: globals.screenWidth * 0.015,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 19 : globals.font_14,
  },
  squareView: {
    marginTop: globals.screenHeight * 0.0065,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  ep_buttonStyle: {
    marginTop: globals.screenHeight * 0.015,
    marginHorizontal: globals.screenWidth * 0.25,
    // margin:25,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.03,
  },
  btntextStyless: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    color: colors.white
  },
  bottomStyle: {
    paddingBottom: globals.screenHeight * 0.01,
  },
  buttonsView: {
    marginTop: globals.screenHeight * 0.01,
  },
  touchableStyle: {
    paddingHorizontal: 22,
    paddingVertical: 5,
    backgroundColor: colors.warmBlue,
    // borderWidth: 1,
  },
  touchableStyle2: {
    paddingHorizontal: 22,  
    paddingVertical: 5,
    backgroundColor: colors.gray,
    // borderWidth: 1,
  },
  ep_redStarStyle: {
    fontSize: globals.font_15,
    color: colors.redColor,
  },
  // ====== : register Event : ======
  mainParentStyle: {
    flex: 1,
    marginTop: globals.screenHeight * 0.01,
  },
  headerViewStyle: {
    backgroundColor: colors.white,
    height: globals.screenHeight * 0.10,
    width: globals.screenWidth,
    flexDirection: 'row',
    marginTop: 3,
    paddingHorizontal: globals.screenHeight * 0.01,
    paddingVertical: globals.screenHeight * 0.02,
    justifyContent: 'space-between',
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  headerTextViewSTyle: {
    width: globals.screenWidth * 0.45,
    borderColor: colors.gray,
    borderWidth: 0.5,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
     marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null,
    backgroundColor: colors.warmBlue
  },
  headertextAlreadyRegiStyle: {
    color: colors.warmBlue,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_14,
  },
  headerTextAlreadyRegisterd: {
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.01 : null,
    width: globals.screenWidth * 0.45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headertextStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_14,
  },
  modalMainView: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.37 : globals.screenHeight * 0.4, justifyContent: 'center', alignItems: 'center', width: globals.screenWidth * 0.9, marginHorizontal: (globals.screenWidth * 0.05), backgroundColor: colors.gray, marginTop: globals.screenHeight * 0.2
  },
  modalInnerMainView: {
    backgroundColor: colors.white, margin: globals.screenWidth * 0.07, width: globals.screenWidth * 0.8, borderRadius: 4, flex: 1,
  },
  modalAlreadyRegisteredView: {
    flexDirection: 'row', padding: globals.screenHeight * 0.01
  },
  modalResendTextView: {
    flex: 1, marginRight: 10
  },
  modalCancelImg: {
    height: globals.screenHeight * 0.03, width: globals.screenHeight * 0.03, tintColor: colors.gray
  },
  modalHorizontalLine: {
    height: 2, backgroundColor: colors.proUnderline, width: '100%', marginTop: (globals.screenHeight * 0.03), marginBottom: (globals.screenHeight * 0.03)
  },
  modalTextInputView: {
    borderColor: colors.gray, borderWidth: 2, marginHorizontal: 10
  },
  modalSubmitView: {
    justifyContent: 'center', backgroundColor: colors.listSelectColor, width: globals.screenWidth * 0.22, alignItems: 'center', height: globals.screenHeight * 0.05, right: 0, position: 'absolute', bottom: (globals.screenHeight * 0.03), marginRight: globals.screenHeight * 0.01, borderRadius: 3
  },
  modalSubmitText: {
    color: colors.white, fontSize: globals.font_13, fontWeight: '600'
  },
  mainContainer: {
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.02,
    marginBottom: globals.screenHeight * 0.03,
    flex: 1
  },
  mainViewStyle: {
    marginBottom: globals.screenHeight * 0.01,

  },
  textInputStyle: {
    paddingBottom: globals.screenHeight * 0.015,
    marginTop: globals.screenHeight * 0.09,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_14,
  },
  bookticketTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_18,
    color: colors.darkSkyBlue,
    fontWeight: '500',
  },
  subContainerView: {
    flexDirection: 'row',
  },
  leftUpperSubView: {
    flex: 2,
    padding:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : globals.screenHeight * 0.029,
    marginRight: globals.screenWidth * 0.035,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.darkSkyBlue,
  },
  rightUpperSubView: {
    flex: 1,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.darkSkyBlue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ticketTypeText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    paddingBottom: globals.screenHeight * 0.006,
  },
  bottomticketTypeText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    paddingBottom: globals.screenHeight * 0.006,
    paddingLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.01,
  },
  ticketPriceText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    color: colors.darkgrey,
  },
  bottomticketPriceText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    color: colors.darkgrey,
    paddingLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.01,
  },
  counterTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    fontWeight: '600',
    padding: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 5 : globals.screenHeight * 0.005,
  },
  addIconStyle: {
    fontSize: globals.font_22,
  },
  minusIconStyle: {
    fontSize: globals.font_22,
  },
  ticketListView: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: colors.proUnderline,
    padding: globals.screenHeight * 0.02,
  },
  bookingStatusText: {
    color: colors.redColor,
    paddingTop: globals.screenHeight * 0.01,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
  },
  bottombookingStatusText: {
    color: colors.redColor,
    paddingTop: globals.screenHeight * 0.01,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 17 : globals.font_12,
    paddingLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.01,
  },
  bottomViewStyle: {
    flexDirection: 'row',
    bottom: 0,
    position: 'absolute'
  },
  bottomLeftView: {
    flex: 2.8,
    backgroundColor: colors.proUnderline,
    padding: globals.screenHeight * 0.02,
    flexDirection: 'row',
  },
  bottomRightView: {
    flex: 1.8,
    backgroundColor: colors.warmBlue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  continueTextStyle: {
    color: colors.white,
    fontSize: globals.font_14,
  },
  totalPriceStyle: {
    fontSize: globals.font_17,
    color: colors.warmBlue,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.02
        : globals.screenWidth * 0.01,
  },
  totalPriceTextStyle: {
    fontSize: globals.font_13,
    color: colors.Gray,
    paddingLeft: globals.screenWidth * 0.026,
    paddingTop: globals.screenWidth * 0.009,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },

  // ====== : Add player info : ======
  containers: {
    flex: 1,
  },
  makePaymentViewContainerstyle: {
    width: '100%',
    height: globals.screenHeight * 0.12,
    backgroundColor: colors.warmBlue,
  },
  makePaymentText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_18,
    color: colors.white,
    marginTop: globals.screenHeight * 0.03,
    marginLeft: globals.screenWidth * 0.05,
  },
  CashDetailMainView: {
    alignItems: 'center',
    position: 'absolute',
    top: globals.screenHeight * 0.08,
  },
  cashDetailChildViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    // height: globals.screenHeight * 0.6,
    flex: 1,
    marginLeft: globals.screenWidth * 0.05,
    paddingHorizontal: globals.screenWidth * 0.07,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    paddingBottom: globals.screenWidth * 0.06,
  },
  headerTextView: {
    paddingVertical: globals.screenWidth * 0.04,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
  },
  cashDetailText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 29 : globals.font_16,
    color: colors.darkSkyBlue,
    fontWeight: '500',
  },
  paymentButtonStyle: {
    width: '100%',
    height: globals.screenHeight * 0.06,
    position: 'absolute',
    bottom: 0,
    zIndex: 10,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  paymentTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_18,
    color: colors.white,
    textAlign: 'center',
  },
  attendeeDetailText: {
    paddingTop: globals.screenWidth * 0.04,
    fontSize: globals.font_16,
  },
  textInputViewStyle: {
    borderWidth: 0.5,
    borderColor: colors.darkG,
    marginTop: globals.screenWidth * 0.045,
    flexDirection: 'row',
    paddingHorizontal: globals.screenWidth * 0.065,
  },
  textInputStyle: {
    paddingVertical: globals.screenWidth * 0.01,
    paddingHorizontal: globals.screenWidth * 0.03,
    // paddingLeft: globals.screenWidth * 0.07,
    // paddingRight: globals.screenWidth * 0.03,
    fontSize: globals.font_13,
  },
  textStyle: {
    paddingVertical: globals.screenWidth * 0.04,
    fontSize: globals.font_20,
    // marginRight: globals.screenWidth * 0.2,
    color: colors.redColor,
  },
  addPayerTextStyle: {
    paddingVertical: globals.screenWidth * 0.04,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    // color:
  },
  add_playerAndIconViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  add_editIconStyle: {
    height: globals.screenWidth * 0.053,
    width: globals.screenWidth * 0.053,
  },
  add_not: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingTop: globals.screenHeight * 0.02,
    paddingBottom: globals.screenHeight * 0.02
  },

  // ====== : Attendees Detail : ======
  ad_containers: {
    flex: 1,
    marginTop:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.025
        : DeviceInfo.getModel() == 'iPhone 5' ||
          DeviceInfo.getModel() == 'iPhone 5s' ||
          DeviceInfo.getModel() == 'iPhone SE' ||
          Platform.OS === 'android'
          ? globals.screenHeight * 0.005
          : globals.screenHeight * 0.018,
  },
  ad_makePaymentViewContainerstyle: {
    width: '100%',
    height: globals.screenHeight * 0.12,
    backgroundColor: colors.warmBlue,
  },
  ad_makePaymentText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_18,
    color: colors.white,
    marginTop: globals.screenHeight * 0.03,
    marginLeft: globals.screenWidth * 0.05,
  },
  ad_CashDetailMainView: {
    alignItems: 'center',
    position: 'absolute',
    top: globals.screenHeight * 0.08,
  },
  ad_cashDetailChildViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    // height: globals.screenHeight * 0.6,
    flex: 1,
    marginLeft: globals.screenWidth * 0.05,
    paddingHorizontal: globals.screenWidth * 0.07,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    paddingBottom: globals.screenWidth * 0.06,
  },
  ad_headerTextView: {
    paddingVertical: globals.screenWidth * 0.04,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.gray,
  },
  ad_cashDetailText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 29 : globals.font_16,
    color: colors.darkSkyBlue,
    fontWeight: '500',
  },
  ad_paymentButtonStyle: {
    width: '100%',
    height: globals.screenHeight * 0.06,
    position: 'absolute',
    bottom: 0,
    zIndex: 10,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ad_paymentTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_18,
    color: colors.white,
    textAlign: 'center',
  },
  ad_attendeeDetailText: {
    paddingTop: globals.screenWidth * 0.04,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
  },
  ad_textInputViewStyle: {
    borderWidth: 0.5,
    borderColor: colors.darkG,
    marginTop: globals.screenWidth * 0.045,
    flexDirection: 'row',
    paddingHorizontal: globals.screenWidth * 0.065,
  },
  ad_textInputStyle: {
    // paddingVertical: globals.screenWidth * 0.04,
    // paddingLeft: globals.screenWidth * 0.07,
    // paddingRight: globals.screenWidth * 0.03,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  ad_textStyle: {
    // paddingVertical: globals.screenWidth * 0.04,
    fontSize: globals.font_20,
    // marginTop: globals.screenWidth * 0.022,
    // marginRight: globals.screenWidth * 0.2,
    color: colors.redColor,
  },
  ad_addPayerTextStyle: {
    paddingVertical: globals.screenWidth * 0.04,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    // color:
  },

  // ===========: booking information :===========
  bookInfo_titleViewContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.9,
    height: globals.screenHeight * 0.1,
    marginLeft: globals.screenWidth * 0.05,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
  },
  bookInfo_titleAndAddIconViewContaier: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  bookInfo_AddIconStyle: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.05
        : globals.screenWidth * 0.072,
    width: globals.screenWidth * 0.072,
    marginRight: globals.screenWidth * 0.08,
    marginTop: globals.screenHeight * 0.028,
  },
  bookInfo_ticketSummaryViewContainer: {
    backgroundColor: colors.darkSkyBlue,
    width: globals.screenWidth * 0.9,
    // flex: 1,
    marginLeft: globals.screenWidth * 0.05,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 5,
    marginTop: globals.screenHeight * 0.03,
  },
  bookInfo_ticketSummaryTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.white,
    fontWeight: Platform.OS === 'android' ? 'bold' : '500',
    marginTop: globals.screenHeight * 0.03,
    marginLeft: globals.screenWidth * 0.05,
  },
  bookingInfoHorizontalLine: {
    height: 1, backgroundColor: colors.proUnderline, marginTop: 10, marginHorizontal: globals.screenWidth * 0.05,
  },
  bookInfo_goldTicketViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: globals.screenWidth * 0.05,
    marginBottom: globals.screenHeight * 0.015,
  },
  bookInfo_goldTicketTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_16,
    color: colors.white,
    marginBottom: globals.screenHeight * 0.008,
  },
  bookInfo_rupeeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 26 : globals.font_14,
    color: colors.white,
  },
  bookInfo_ticketCountViewContainer: {
    height: globals.screenHeight * 0.085,
    width: globals.screenWidth * 0.155,
    borderColor: colors.white,
    borderRadius: 5,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bookInfo_ticketCountTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 29 : globals.font_17,
    color: colors.white,
  },
  bookInfo_SilverTicketViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.02,
  },
  bookInfo_totalPriceBtnStyle: {
    width: '100%',
    height: globals.screenHeight * 0.065,
    position: 'absolute',
    bottom: 0,
    zIndex: 10,
    backgroundColor: colors.lightWhite,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bookInfo_ViewText: {
    width: '100%',
    // height: globals.screenHeight * 0.065,
    // marginTop: globals.screenHeight * 0.66,
    // marginBottom: globals.screenHeight * 0.15,
    backgroundColor: colors.lightWhite,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
    zIndex: 10
  },
  bookInfo_PricetextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.warmBlue,
    textAlign: 'center',
  },
  bookInfo_totalPriceTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
    color: colors.brownishGrey,
  },
  // ===========: booking Receipt :===========

  mainBookingReceipt: {
    flex: 1,

    // marginTop: 3,
  },
  bookingReceiptTitleView: {
    width: globals.screenWidth,
    height: globals.screenHeight * 0.08,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
  },
  bookingReceiptTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    color: colors.white,
    marginLeft: globals.screenWidth * 0.05,
  },
  mainBookingFormView: {
    flex: 1,
    // backgroundColor:"red",
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.025,
    paddingBottom: globals.screenHeight * 0.01,
  },
  bookingRecHi: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_15,
  },
  bookingRecOtherTextsBlack: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_13,
    color: colors.lightBlack,
    marginTop: 3
  },
  grayLineView: {
    height: 1,
    backgroundColor: colors.borderColor,
    marginVertical: globals.screenHeight * 0.015
  },
  horizontalPart: {
    flexDirection: 'row',
    flex: 1
  },
  halfViewBookingTickets: {
    width: globals.screenWidth / 2
  },
  fourHalfViewBookingTickets: {
    width: globals.screenWidth / 4,
    flex: 1
  },
  BookingRecheadingText: {
    color: colors.Gray,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_13,
  },
  bookingRecOtherTextsBlackmargin: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_18,
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.010
  },
  bottomRightViewBookingReceipt: {
    flex: 1,
    alignItems: 'flex-end',
  },
  bookingBottomView: {
    flex: 0.02, backgroundColor: colors.warmBlue, bottom: 0,
    alignItems: 'center', justifyContent: 'center'
  },
  ///////// Add NEW PLAYER INfo 

  api_safeareaStyle: {
    flex: 1,

  },
  api_infoTextStyles: {
    fontSize: globals.font_16,
    marginBottom: globals.screenHeight * 0.02,
    marginLeft: globals.screenWidth * 0.08
  },
  api_container: {
    flex: 1
  },
  api_blueView: {
    width: '100%',
    height: globals.screenHeight * 0.12,
    backgroundColor: colors.warmBlue,
  },
  api_innerContaiuner: {
    flex: 1,
    marginTop: - globals.screenHeight * 0.04,
  },
  editable_WhiteView:{
    borderRightColor: colors.darkG,
    borderLeftColor: colors.darkG,
    borderTopColor:colors.darkG,
    borderLeftWidth:0.5,
    borderRightWidth:0.5,
    borderTopWidth:0.5,
    borderTopLeftRadius:5,
    borderTopRightRadius:5,
    flex: 0.95,
    backgroundColor: "red",
    backgroundColor: colors.white, marginHorizontal: globals.screenWidth * 0.04
  },
  api_WhiteView: {
    borderColor: colors.darkG,
    borderWidth: 0.5,
    borderRadius: 5, 
    flex: 0.95,
    backgroundColor: "red",
    backgroundColor: colors.white, marginHorizontal: globals.screenWidth * 0.04
  },
  api_apiView: {
    flexDirection: 'row', paddingBottom: globals.screenHeight * 0.02, borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
    marginVertical: globals.screenHeight * 0.025,
    marginHorizontal: globals.screenWidth * 0.080,
  },
  api_apiText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 29 : globals.font_16,
    color: colors.darkSkyBlue,
    fontWeight: '500',
  },
  api_iconView: {
    flex: 1, alignItems: 'flex-end',
  },
  api_iconimgStyle: {
    height: globals.screenWidth * 0.053,
    width: globals.screenWidth * 0.053,
  },
  api_mainContainerView: {
    flex: 1,
    // backgroundColor:"red"
  },
  api_textfiledView: {
    borderWidth: 0.5, borderColor: colors.gray,
    marginHorizontal: globals.screenWidth * 0.080,
    marginBottom: globals.screenHeight * 0.02,
    paddingHorizontal: globals.screenWidth * 0.04,
    alignItems: 'center', flexDirection: 'row',
    flex: 1

  },
  api_textFiledTextStyle: {
    paddingVertical: globals.screenHeight * 0.02,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
  },
  api_paymentTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_18,
    color: colors.white,
    textAlign: 'center',
  },
  api_buttonViewStyle: {
    flex: 0.1,
    backgroundColor: colors.warmBlue,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  api_textStyle: {
    fontSize: globals.font_20,
    color: colors.redColor,
  },

  ///////// make payment 

  mp_safeareaStyle: {
    flex: 1,

  },
  mp_infoTextStyles: {
    fontSize: globals.font_16,
    marginBottom: globals.screenHeight * 0.02,
    marginLeft: globals.screenWidth * 0.08
  },
  mp_container: {
    flex: 1
  },
  mp_blueView: {
    width: '100%',
    height: globals.screenHeight * 0.12,
    backgroundColor: colors.warmBlue,
  },
  mp_innerContaiuner: {
    flex: 1,
    marginTop: - globals.screenHeight * 0.04,
  },
  mp_WhiteView: {
    borderColor: colors.darkG,
    borderWidth: 0.5,
    borderRadius: 5, flex: 0.7,
    backgroundColor: "red",
    backgroundColor: colors.white, marginHorizontal: globals.screenWidth * 0.04
  },
  mp_apiView: {
    flexDirection: 'row', paddingBottom: globals.screenHeight * 0.02, borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
    marginVertical: globals.screenHeight * 0.025,
    marginHorizontal: globals.screenWidth * 0.080,
  },
  mp_apiText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 29 : globals.font_16,
    color: colors.darkSkyBlue,
    fontWeight: '500',
  },
  mp_buttonViewStyle: {
    flex: 0.1, backgroundColor: colors.warmBlue, bottom: 0,
    alignItems: 'center', justifyContent: 'center'
  },
  mp_textStyle: {
    fontSize: globals.font_20,
    color: colors.redColor,
  },
  mp_buttonContainer: {
    marginHorizontal: globals.screenWidth * 0.08,
    marginVertical: globals.screenHeight * 0.02,
    flex: 1
    // padding:globals.screenWidth * 0.01
  },
  mp_boxViewStyles: {
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1.5,
    borderRadius: globals.screenWidth * 0.015,
    marginBottom: globals.screenHeight * 0.03,
    height: globals.screenHeight * 0.08
  },
  mp_logostyle: {
    height: globals.screenWidth * 0.25,
    width: globals.screenWidth * 0.25
  },
  mp_bottomViewStyle: {
    flex: 0.1, backgroundColor: colors.proUnderline, bottom: 0,
    alignItems: 'center', justifyContent: 'center',
    flexDirection: 'row'
  },
  mp_textStyle: {
    fontSize: globals.font_17,
    color: colors.warmBlue,
  },
  mp_totalPriceTextStyle: {
    fontSize: globals.font_13,
    color: colors.Gray,
    marginLeft: globals.screenWidth * 0.01
  },
  mp_continueButoon: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.warmBlue,
    height: globals.screenHeight * 0.065,
    width: globals.screenWidth,
    //  position: 'absolute',
    top: 8,
    // zIndex:10
  },
  /////////////////// COMMON BUTTON STYLE
  common_bottomViewStyle: {
    flexDirection: 'row',
    bottom: 0,
    position: 'absolute'
  },
  common_bottomLeftView: {
    flex: 2.8,
    backgroundColor: colors.proUnderline,
    padding: globals.screenHeight * 0.02,
    flexDirection: 'row',
  },
  common_bottomRightView: {
    flex: 1.8,
    backgroundColor: colors.warmBlue,
    alignItems: 'center',
    justifyContent: 'center',
  },
  common_continueTextStyle: {
    color: colors.white,
    fontSize: globals.font_14,
  },
  common_totalPriceStyle: {
    fontSize: globals.font_17,
    color: colors.warmBlue,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.02
        : globals.screenWidth * 0.01,
  },
  common_totalPriceTextStyle: {
    fontSize: globals.font_13,
    color: colors.Gray,
    paddingLeft: globals.screenWidth * 0.026,
    paddingTop: globals.screenWidth * 0.009,
  },


});
