/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Modal, Image, TextInput, FlatList, Alert } from 'react-native';
import styles from './style';
import Validation from '../../../../../utils/validation';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import AntDesign from 'react-native-vector-icons/AntDesign';
import * as color from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import { API } from '../../../../../utils/api';
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import moment from 'moment';
import Nointernet from '../../../../../components/NoInternet/index'
import ServerError from '../../../../../components/ServerError/index'

var TAG = "BookTickets ::=="
const iPad = DeviceInfo.getModel();

let ticketInfo = []
let isTcktAvail = false
class RegistrationEvent extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });


  constructor(props) {
    super(props);
    this.state = {
      countGold: 0,
      countSilver: 0,
      dataEventsList: this.props.navigation.state.params.dataEventsList,
      eventID: this.props.navigation.state.params.eventID,
      availableTicketsData: [],
      serverErr: false,
      isInternetFlag: true,
      loading: false,
      modalVisibleResend: false,
      email: '',
      totalPrice: 0,
      ticketInfo: [],
      AllEventDetail: this.props.navigation.state.params.allEventDetails,
    }
  }

  componentDidMount() {
    isTcktAvail=false
    ticketInfo = []
    this.props.showLoader()
    this.makeAPICall()
  }

  /**
     *  call when _sessionOnPres
     */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  makeAPICall() {
    if (globals.isInternetConnected === true) {
      this.setState({ isInternetFlag: true, loading: true })
      API.getAvailableTickets(this.availableTicketsResponse, true, this.state.eventID);
    } else {
      this.props.hideLoader()
      this.setState({ isInternetFlag: false })
      // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }


  /**
   * Response callback of eventAttendeses
   */
  availableTicketsResponse = {
    success: response => {
      console.log(
        TAG,
        'availableTicketsResponse -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ availableTicketsData: response.Data.AvailableTicketInfo }, () => {
          for (let index = 0; index < this.state.availableTicketsData.length; index++) {
            this.state.availableTicketsData[index].ticketCount = 0;
          }
        })
      } else {
        this.setState({ serverErr: true, loading: false })
        Alert.alert(globals.appName, response.Message);
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true, loading: false })
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
      console.log(
        TAG,
        'availableTicketsResponse -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  /**
   * Render item list event attendee
   * @param {*} item 
   * @param {*} index 
   */
  renderItemList(item, index) {
    const { availableTicketsData } = this.state;
    const isFromDate = globals.checkObject(item, 'AvailableFromDateUTC');
    const isUptoDate = globals.checkObject(item, 'AvailableUptoDateUTC');
    const isTicketName = globals.checkObject(item, 'TicketName');
    const isTicketPrice = globals.checkObject(item, 'TicketPrice');
    const isTotalAvailableTickets = globals.checkObject(item, 'TotalAvailableTickets');
    const date = new Date()
    const availableFromdate = (isFromDate) ? new Date(item.AvailableFromDateUTC) : ''
    const availableUptodate = (isUptoDate) ? new Date(item.AvailableUptoDateUTC) : ''
    if (date > availableFromdate && date < availableUptodate && item.TotalAvailableTickets > 0) {
      isTcktAvail = true
    }

    return (
      (date > availableFromdate && date < availableUptodate && item.TotalAvailableTickets > 0) ?
        <View style={styles.mainParentStyle}>
          <View style={styles.subContainerView}>
            <View style={styles.leftUpperSubView}>
              <Text style={styles.ticketTypeText}>{(isTicketName) ? item.TicketName : ''}</Text>
              <Text style={styles.ticketPriceText}>{(isTicketPrice) ? "$" + item.TicketPrice + ".00" : ''}</Text>
            </View>
            <View style={styles.rightUpperSubView}>
              <AntDesign name={'up'}
                size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
                  globals.screenWidth * 0.05 : globals.screenWidth * 0.06} onPress={() => this.incrementTicketCount(index, item)} color={color.listSelectColor} />
              <Text style={styles.counterTextStyle}>{(item.ticketCount < 0) ? '0' : item.ticketCount}</Text>
              <AntDesign name={'down'} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
                globals.screenWidth * 0.05 : globals.screenWidth * 0.06} onPress={() => this.decrementTicketCount(index, item)} color={color.listSelectColor} />
            </View>
          </View>
        </View> :
        <View style={styles.mainParentStyle}>
          <View style={styles.ticketListView}>
            <Text style={[styles.bottomticketTypeText,]}>{(isTicketName) ? item.TicketName : ''}</Text>
            <Text style={[styles.bottomticketPriceText,]}>{(isTicketPrice) ? "$" + item.TicketPrice + ".00" : ''}</Text>
            {
              ((item.TotalAvailableTickets) <= 0) ?
                <Text style={[styles.bottombookingStatusText]}>{"Sold Out"}</Text>
                :
                (date < availableFromdate) ?
                  <Text style={[styles.bottombookingStatusText, { color: color.green }]}>{"Booking starts at " + moment(item.AvailableFromDateUTC).format('MMMM Do YYYY, h:mm:ss a')}</Text>
                  :
                  (availableUptodate < date) ?
                    <Text style={[styles.bottombookingStatusText, { color: color.orange }]}>{"Booking Closed"}</Text>
                    : <Text></Text>
            }
          </View>
        </View>
    );
  }

  /**
   * Method of increment ticket count
   * @param {*} index 
   * @param {*} item 
   */
  incrementTicketCount(index, item) {
    // if (item.TicketType == "Individual") {
    //   if (item.ticketCount == 0) {
    //     item.ticketCount = item.ticketCount + 1;
    //     this.state.availableTicketsData[index].ticketCount = item.ticketCount
    //     this.setState({ availableTicketsData: this.state.availableTicketsData }, () => {
    //       ticketInfo.push({ ticketId: item.TicketId, ticketName: item.TicketName, ticketCount: item.ticketCount, ticketPrice: item.TicketPrice, totalPrice: item.ticketCount * item.TicketPrice })
    //       this.makeDynamicArr(item.ticketCount)
    //     })
    //   } else {
    //     Alert.alert(globals.appName, "This ticket is only for Inidividuals. You can not select more than one ticket");
    //     this.makeDynamicArr(item.ticketCount)
    //   }
    // }
    if (item.TotalAvailableTickets < 10 && item.TotalAvailableTickets > 0) {
      if (item.ticketCount >= item.TotalAvailableTickets) {
        Alert.alert(globals.appName, "Available tickets are " + item.TotalAvailableTickets + " So you can't select more than " + item.TotalAvailableTickets + " tickets");
        this.makeDynamicArr(item.ticketCount)
      } else {
        item.ticketCount = item.ticketCount + 1;
        this.state.availableTicketsData[index].ticketCount = item.ticketCount
        this.setState({ availableTicketsData: this.state.availableTicketsData }, () => {
          ticketInfo.push({ ticketId: item.TicketId, ticketName: item.TicketName, ticketCount: item.ticketCount, ticketPrice: item.TicketPrice, totalPrice: item.ticketCount * item.TicketPrice })
          this.makeDynamicArr(item.ticketCount)
        })
      }
    } else {
      if (item.ticketCount > 9) {
        Alert.alert(globals.appName, "Maximum limit for book ticket is 10. You can't select more than 10 tickets");
        this.makeDynamicArr(item.ticketCount)
      }
      else {
        item.ticketCount = item.ticketCount + 1;
        this.state.availableTicketsData[index].ticketCount = item.ticketCount
        this.setState({ availableTicketsData: this.state.availableTicketsData }, () => {
          ticketInfo.push({ ticketId: item.TicketId, ticketName: item.TicketName, ticketCount: item.ticketCount, ticketPrice: item.TicketPrice, totalPrice: item.ticketCount * item.TicketPrice })
          this.makeDynamicArr(item.ticketCount)
        })
      }
    }
    this.makeDynamicArr(item.ticketCount)
  }

  /**
   * Method of decrement count
   * @param {*} index 
   * @param {*} item 
   */
  decrementTicketCount(index, item) {
    if (item.ticketCount > 0) {
      item.ticketCount = item.ticketCount - 1;
      this.state.availableTicketsData[index].ticketCount = item.ticketCount
      this.setState({ availableTicketsData: this.state.availableTicketsData }, () => {
        ticketInfo.push({ ticketId: item.TicketId, ticketName: item.TicketName, ticketCount: item.ticketCount, ticketPrice: item.TicketPrice, totalPrice: item.ticketCount * item.TicketPrice })
        this.makeDynamicArr(item.ticketCount)
      })
    }
    else {
      this.makeDynamicArr(item.ticketCount)
    }
    this.makeDynamicArr(item.ticketCount)
  }

  /**
   * Method of set visibility of Modal
   * @param {*} visible 
   */
  setModalVisible(visible) {
    if (visible == false) {
      this.setState({ email: '', modalVisibleResend: visible })
    }
    this.setState({ modalVisibleResend: visible })
  }

  /**
   * Method of click submit button in modal
   */
  clickSubmitBtn() {
    const { email } = this.state;
    if (Validation.textInputCheck(email)) {
      if (Validation.isValidEmail(email)) {
        if (globals.isInternetConnected === true) {
          let eventID = this.state.eventID;
          let confirmationMail = this.state.email;
          API.resendEmailConfirmation(this.resendEmailConfirmationResponse, true, eventID, confirmationMail);
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
      }
      else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_VALIDEMAIL);
      }
    }
    else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTEREMAIL);
    }

  }

  clearstateandclosepopup() {
    this.setState({ email: "" }, () => {
      this.setModalVisible(false)
    })
  }

  /**
   * Resend email confirmation response
   */
  resendEmailConfirmationResponse = {
    success: response => {
      console.log(
        TAG,
        'resendEmailConfirmationResponse -> success : ',
        JSON.stringify(response)
      );
      this.setState({ loading: false })
      Alert.alert(globals.appName, response.Message, [{ text: 'OK', onPress: () => this.clearstateandclosepopup() }]);
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      if (err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
      console.log(
        TAG,
        'resendEmailConfirmationResponse -> ERROR : ',
        JSON.stringify(err.message)
      );
    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  /**
   * Method of Render modal of resend email
   */
  renderResendEmailPopup() {
    const { email } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modalVisibleResend}
        onRequestClose={() => {
          this.setModalVisible(false);
        }}
      >
        <View style={styles.modalMainView}>
          <View style={styles.modalInnerMainView}>
            <View style={styles.modalAlreadyRegisteredView}>
              <View style={styles.modalResendTextView}>
                <Text style={{ fontSize: globals.font_16 }}>Re-send Event Registration Email</Text>
              </View>
              <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => this.setModalVisible(false)}>
                <Image source={images.EventDashboard.eventRegistration.resendEventMail.close} style={styles.modalCancelImg} />
              </TouchableOpacity>
            </View>
            <View style={styles.modalHorizontalLine}></View>
            <View style={styles.modalTextInputView}>
              <TextInput
                value={email}
                autoCapitalize="none"
                onFocus={false}
                blurOnSubmit={false}
                returnKeyType="done"
                onChangeText={text => this.setState({ email: text })}
                placeholderTextColor={color.gray}
                placeholder={"Enter your email address with which you"}
                style={styles.textInputStyle} />
            </View>
            <View style={styles.modalHorizontalLine}></View>
            <TouchableOpacity style={styles.modalSubmitView} onPress={() => this.clickSubmitBtn()}>
              <Text style={styles.modalSubmitText}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    )
  }

  /**
   * Method of make dynamic array
   */
  makeDynamicArr(ticketCount) {
    var result = ticketInfo.reverse((unique, o) => {
      if (!unique.some(obj => obj.ticketId === o.ticketId)) {
        unique.push(o);
      }
      return unique;
    }, []);

    var updatedresult = result.reduce((unique, o) => {
      if (!unique.some(obj => obj.ticketId === o.ticketId)) {
        unique.push(o);
      }
      return unique;
    }, []);

    this.setState({ ticketInfo: updatedresult }, () => {
    })
    let sum = 0;
    for (let index = 0; index < updatedresult.length; index++) {
      sum += updatedresult[index].totalPrice;
    }
    this.setState({ totalPrice: sum })

  }

  /**
   * Method of Get unique data
   * @param {*} arr 
   * @param {*} comp 
   */
  getUnique(arr, comp) {
    const unique = arr
      .map(e => e[comp])

      // store the keys of the unique objects
      .map((e, i, final) => final.indexOf(e) === i && i)

      // eliminate the dead keys & store unique objects
      .filter(e => arr[e]).map(e => arr[e]);

    return unique;
  }

  /**
   * Method of click on continue at bottom of screen, Add final ticket price into dynamic ticket info array
   */
  clickContinue() {

    let ticketInfo = this.state.ticketInfo;
    for (let index = 0; index < ticketInfo.length; index++) {
      ticketInfo[index].finalTotalTicketPrice = this.state.totalPrice;

      var arrTicket = []
      for (let arrayIndex = 0; arrayIndex < this.state.ticketInfo[index].ticketCount; arrayIndex++) {
        let object = {
          'userInfoFilled': false
        }
        arrTicket.push(object);
      }
      this.state.ticketInfo[index]['arrTicket'] = arrTicket
    }

    this.setState({ ticketInfo: ticketInfo }, () => {
      let canContinue = true
      let canContinue_2 = true
      for (let index = 0; index < this.state.ticketInfo.length; index++) {
        if (ticketInfo[index].ticketCount == 0) {
          canContinue = false
        } else {
          canContinue_2 = false
        }
      }
      if (((canContinue == false && canContinue_2 == true) || (this.state.ticketInfo.length == 0)) && isTcktAvail == true) {
        Alert.alert(globals.appName, globals.MESSAGE.EVENTREGISTRATIONMODULE.SELECTTICKET);
      }
      else if (isTcktAvail == false || this.state.ticketInfo.length == 0) {
        Alert.alert(globals.appName, "Currently you don't have any ticket for booking this event");
      }
      else {
        var result = ticketInfo.reverse((unique, o) => {
          if (!unique.some(obj => obj.ticketId === o.ticketId)) {
            unique.push(o);
          }
          return unique;
        }, []);

        var updatedresult = result.reduce((unique, o) => {
          if (!unique.some(obj => obj.ticketId === o.ticketId)) {
            unique.push(o);
          }
          return unique;
        }, []);
        this.setState({ ticketInfo: updatedresult }, () => {
        })
        this.props.navigation.navigate("ATTENDEES_DETAILS", { dataEventsList: this.state.dataEventsList, ticketInfo: this.state.ticketInfo, AllEventDetail: this.state.AllEventDetail })
      }
    })
  }

  /*
  * Try again method
  */
  _tryAgain() {
    this.setState({ loading: true, serverErr: false }, () => {
      this.props.showLoader()
      this.makeAPICall();
    });
  }


  render() {
    const { availableTicketsData, totalPrice, serverErr, loading, isInternetFlag } = this.state;
    console.log("ISisTcktAvail---"+isTcktAvail)
    return (
      <View style={[styles.mainParentStyle, { marginTop: 0 }]}>{
        (!isInternetFlag) ?
          <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
          serverErr === false ?
            <View style={[styles.mainParentStyle, { marginTop: 0 }]}>
              {
                // (isTcktAvail == false) ?
                //  null
                //   :
                  <View style={[styles.headerViewStyle]}>{
                    this.renderResendEmailPopup()
                  }
                    <View style={[styles.headerTextAlreadyRegisterd]}>
                      <Text numberOfLines={1} style={styles.headertextAlreadyRegiStyle}>Already Registered?</Text>
                    </View>
                    <TouchableOpacity style={styles.headerTextViewSTyle} onPress={() => this.setModalVisible(true)}>
                      <Text numberOfLines={1} style={styles.headertextStyle}>Resend Confirmation</Text>
                    </TouchableOpacity>
                  </View>
              }
            
              <View style={styles.mainContainer}>
                <View style={styles.mainViewStyle}>
                  <Text style={styles.bookticketTextStyle}>Book Tickets</Text>
                </View>
                <FlatList
                  data={availableTicketsData}
                  showsVerticalScrollIndicator={false}
                  renderItem={({ item, index }) => this.renderItemList(item, index)}
                  keyExtractor={(index, item) => item.toString()}
                  extraData={this.state}
                  style={{ marginBottom: globals.screenHeight * 0.06 }}
                />
              </View>
              <View style={styles.bottomViewStyle}>
                <View style={styles.bottomLeftView}>
                  <Text style={styles.totalPriceStyle}> {"$" + totalPrice + ".00"}</Text>
                  <Text style={styles.totalPriceTextStyle}>(Total Price)</Text>
                </View>
                {
                  // (isTcktAvail == false)  ? null 
                  // :
                    <TouchableOpacity style={styles.bottomRightView} onPress={() => this.clickContinue()}>
                      <Text style={styles.continueTextStyle}>CONTINUE</Text>
                    </TouchableOpacity>
                }


              </View>
            </View> :
            (
              <ServerError loading={loading} onPress={() => this._tryAgain()} />
            )
      }
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationEvent);