/* eslint-disable no-underscore-dangle /
/ eslint-disable no-nested-ternary /
/ eslint-disable consistent-return /
/ eslint-disable no-plusplus /
/ eslint-disable react/destructuring-assignment /
/ eslint-disable react/no-unused-state /
/ eslint-disable class-methods-use-this /
/ eslint-disable react/sort-comp /
/ eslint-disable no-useless-constructor /
/ eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as images from '../../../../../assets/images/map';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import AttendeeDetail2 from './attendeeDetails2';

var TAG = "AttendeesDetails ::=="
class AttendeesDetails extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      dataEventsList:this.props.navigation.state.params.dataEventsList,
      Attendee_Detail: this.props.navigation.state.params.ticketInfo,
      EventDetails: this.props.navigation.state.params.AllEventDetail,
    };
  }

  /**
   * this method for get attendeeDetail from AttendeeDetail2 Screen with updated
   */
  returnData(select, tag, id, ticketObject) {
    let index = this.state.Attendee_Detail.findIndex(obj => obj.ticketId === ticketObject['id']);
    let updatedTicketObject = this.state.Attendee_Detail[index]
    updatedTicketObject['arrTicket'][ticketObject['tappedIndex']] = {
      userInfoFilled: true,
      FirstName: ticketObject['FirstName'],
      LastName: ticketObject['LastName'],
      EmailID: ticketObject['EmailID'],
      Phone: ticketObject['Phone'],
      Company: ticketObject['Company'],
      Id: ticketObject['tappedIndex']
    }


    this.state.Attendee_Detail[index] = updatedTicketObject

    this.setState({
      Attendee_Detail: [...this.state.Attendee_Detail]
    });
  }

  componentDidMount() {
    AsyncStorage.getItem('attendeeData').then(attendenData => {
      console.log(TAG, attendenData);
      
    });
  }

  /**
   * this method for Attendee list in Flatlist
   */
  renderAttendeeName(item, index) {
    const attendee = item.ticketCount;
    const fields = [];
    for (let i = 0; i < attendee; i++) {
      var firstName = (item.arrTicket[i].userInfoFilled == true) ? item.arrTicket[i].FirstName : 'Attendee ' + (i + 1)
      var lastName = (item.arrTicket[i].userInfoFilled == true) ? item.arrTicket[i].LastName : null
      fields.push(
        <View key={i} style={styles.attendees_UserNameAndIconViewContainer}>
          <Text numberOfLines={1} style={styles.attendees_UserNameAndAttendeesTextStyle}>{firstName}{' '}{lastName}</Text>
          {(item.arrTicket[i].userInfoFilled == true) ?
            <View style={styles.attendees_IconsContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ATTENDEES_DETAIL2', {
                    returnData: this.returnData.bind(this),
                    tag: item.ticketName,
                    id: item.ticketId,
                    userData: item.arrTicket[i],
                    tappedIndex: i,
                    addEditStatus: item.arrTicket[i].userInfoFilled,
                  });
                }}
              >
                <Image
                  source={images.EventRegistration.editIcon}
                  style={styles.attendees_editIconStyle}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            :
            <View style={styles.attendees_IconsContainer}>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ATTENDEES_DETAIL2', {
                    returnData: this.returnData.bind(this),
                    tag: item.ticketName,
                    id: item.ticketId,
                    tappedIndex: i
                  }); AttendeeDetail2.makeApiCall()
                }
                }
              >
                <Image
                  source={images.EventRegistration.addIcon}
                  style={styles.attendees_addIconStyle}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
          }
        </View>
      );
    }
    return fields;
  }

  /**
   * this method for Flatlist item 
   */
  _renderItemList(item, index) {
    return (
      <View style={{ flex: 1 }}>
        {(item.ticketCount == 0) ?
          null
          :
          <View style={styles.attendees_GoldTicketViewContainer}>
            <Text style={styles.attendees_GoldTicketTextStyle}>{item.ticketName}</Text>
            <View style={styles.attendees_dashLine} />
            {this.renderAttendeeName(item, index)}
          </View>
        }
      </View>
    );
  }

  /**
   * this method for when all attendeeDetail filled then navigate
   * booking info screen
   */
  clickToContinue() {
    const { Attendee_Detail } = this.state;
    let attendeeDetails = Attendee_Detail
    var isFill = true;
    for (let i = 0; i < attendeeDetails.length; i++) {
      for (let j = 0; j < attendeeDetails[i].arrTicket.length; j++) {
        let infoUserFillStatus = attendeeDetails[i].arrTicket[j].userInfoFilled;
        if (infoUserFillStatus === false) {
          isFill = false
          break
        }
      }
      if (isFill == false) {
        break;
      }
    }
    if (isFill === true) {
      this.props.navigation.navigate("BOOKING_INFORMATION", {dataEventsList:this.state.dataEventsList, ticketInfo: this.state.Attendee_Detail, EventDetails: this.state.EventDetails })
    } else {
      Alert.alert(globals.appName, 'Please add attendee detail');
    }
  }

  render() {
    return (
      <View style={styles.api_container}>
        <View style={styles.api_blueView}>
          <Text style={styles.makePaymentText}>Attendees Details</Text>
        </View>
        <View style={styles.attendeeDetail_simpleViewContainer} />
        <View style={styles.attendeeDetail_flatlistStyle}>
          <FlatList
            bounces={false}
            showsVerticalScrollIndicator={false}
            data={this.state.Attendee_Detail}
            renderItem={({ item, index }) => this._renderItemList(item, index)}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        <TouchableOpacity
          style={styles.api_buttonViewStyle}
          onPress={() => this.clickToContinue()}>
          <Text style={styles.api_paymentTextStyle}>CONTINUE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AttendeesDetails);
