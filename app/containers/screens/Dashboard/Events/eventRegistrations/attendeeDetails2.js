/* eslint-disable react/sort-comp */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-underscore-dangle */
/* eslint-disable camelcase */
/* eslint-disable react/prop-types */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Alert, TextInput } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import Validation from '../../../../../utils/validation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import { API } from '../../../../../utils/api';
import KeyboardListener from 'react-native-keyboard-listener';

var TAG = "AttendeesDetails2 ::=="
let _this;

class AttendeeDetails2 extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      isFreeStatus:this.props.navigation.state.params.isFreeStatus,
      email: '',
      mobile: '',
      firstName: '',
      lastName: '',
      companyName: '',
      redStarForEmail: false,
      redStarForFname: false,
      redStarForLname: false,
      redStarForCname: false,
      Attendee_Data: [],
      keyboardOpen: false
    };
  }

  componentDidMount() {
    /**
     * update state when addEditStatus will true
     */
    if (this.props.navigation.state.params.addEditStatus == true) {
      this.setState({
        redStarForEmail: true,
        redStarForFname: true,
        redStarForLname: true,
        redStarForCname: true,
      })
      this.setState({ email: this.props.navigation.state.params.userData.EmailID })
      this.setState({ mobile: this.props.navigation.state.params.userData.Phone })
      this.setState({ firstName: this.props.navigation.state.params.userData.FirstName })
      this.setState({ lastName: this.props.navigation.state.params.userData.LastName })
      this.setState({ companyName: this.props.navigation.state.params.userData.Company })
    }
  }

  static makeApiCall() {
    AsyncStorage.getItem(globals.LOGINUSEREMAIL).then(token => {
      globals.loginEmail = token;
      if (globals.isInternetConnected === true) {
        API.getUserInfoForAutofill(_this.getUserInfoForAutofillResponseData, globals.loginEmail);
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  getUserInfoForAutofillResponseData = {
    success: response => {
      console.log(
        TAG,
        'getUserInfoForAutofillResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        const responseData = response.Data
        this.setState({ Attendee_Data: responseData })
        this.setState({
          email: this.state.Attendee_Data.EmailID,
          mobile: this.state.Attendee_Data.Phone,
          firstName: this.state.Attendee_Data.FirstName,
          lastName: this.state.Attendee_Data.LastName,
          companyName: this.state.Attendee_Data.Company,

        })
        if (this.state.email !== '') {
          this.setState({ redStarForEmail: true })
        } else {
          this.setState({ redStarForEmail: false })
        } if (this.state.firstName !== '') {
          this.setState({ redStarForFname: true })
        } else {
          this.setState({ redStarForFname: false })
        } if (this.state.lastName !== '') {
          this.setState({ redStarForLname: true })
        } else {
          this.setState({ redStarForLname: false })
        } if (this.state.companyName !== '') {
          this.setState({ redStarForCname: true })
        } else {
          this.setState({ redStarForCname: false })
        }
      } else {
      }
    },
    error: err => {
      console.log(
        TAG,
        'getUserInfoForAutofillResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message);
      }
    },
    complete: () => {
    },
  };

  /**
   * validation method for add attendeeDetails
   */
  checkValidateInput() {
    const { email, mobile, firstName, lastName, companyName } = this.state;
    let phoneno=mobile.replace(/[^+\d]+/g,'')
    if(mobile.slice(0,1)=='+'){
      phoneno = phoneno.substring(3)
   }
    const mobileLength = phoneno.length;
    const tag = this.props.navigation.state.params.tag;
    const id = this.props.navigation.state.params.id;
    const tappedIndex = this.props.navigation.state.params.tappedIndex;

    if (Validation.textInputCheck(email)) {
      this.setState({ redStarForEmail: true });
      if (Validation.isValidEmail(email)) {
        this.setState({ redStarForEmail: true });
        if (Validation.connectProValidMobileNub(mobileLength)) {
          if (Validation.textInputCheck(firstName)) {
            this.setState({ redStarForFname: true });
            if (Validation.allSpecialCharacter(firstName)) {
              this.setState({ redStarForFname: true });
              if (Validation.textInputCheck(lastName)) {
                this.setState({ redStarForLname: true });
                if (Validation.allSpecialCharacter(lastName)) {
                  this.setState({ redStarForLname: true });
                  if (Validation.textInputCheck(companyName)) {
                    this.setState({ redStarForCname: true });

                    AsyncStorage.setItem('attendeeData', JSON.stringify({
                      id: id,
                      type: tag,
                      FirstName: firstName,
                      LastName: lastName,
                      EmailID: email,
                      Phone: mobile,
                      Company: companyName,
                      tappedIndex: tappedIndex
                    }));
                    this.props.navigation.state.params.returnData(true, tag, id, {
                      id: id,
                      type: tag,
                      FirstName: firstName,
                      LastName: lastName,
                      EmailID: email,
                      Phone: mobile,
                      Company: companyName,
                      tappedIndex: tappedIndex
                    });
                    this.props.navigation.goBack();
                  } else {
                    Alert.alert(
                      globals.appName,
                      globals.MESSAGE.ATTENDEE_DETAILS.VALID_COMPANY_NAME
                    );
                    this.setState({ redStarForCname: false });
                  }
                } else {
                  Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.VALID_LAST_NAME);

                }
              } else {
                Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.ENTER_LAST_NAME);
                this.setState({ redStarForLname: false });
              }
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.VALID_FIRST_NAME);

            }
          } else {
            Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.ENTER_FIRST_NAME);
            this.setState({ redStarForFname: false });
          }
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.VALID_MOB_NO);
        }
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.VALID_EMAIL);

      }
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.ATTENDEE_DETAILS.ENTER_EMAIL_ID);
      this.setState({ redStarForEmail: false });
    }
  }

  /**
     * 
     * Upadte TextInput Data
     */
  handleEmailText(text) {
    if (text.length > 0) {
      this.setState({
        email: text,
        redStarForEmail: true,
      });
    } else {
      this.setState({
        email: text,
        redStarForEmail: false,
      });
    }
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handlemobnoText(text) {
    
    this.setState({
      mobile: text,
    });
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handleFirstNameText(text) {
    if (text.length > 0) {
      this.setState({
        firstName: text,
        redStarForFname: true,
      });
    } else {
      this.setState({
        firstName: text,
        redStarForFname: false,
      });
    }
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handleCompanynameText(text) {
    if (text.length > 0) {
      this.setState({
        companyName: text,
        redStarForCname: true,
      });
    } else {
      this.setState({
        companyName: text,
        redStarForCname: false,
      });
    }
  }

  /**
     * 
     * Upadte TextInput Data
     */
  handleLastNameText(text) {
    if (text.length > 0) {
      this.setState({
        lastName: text,
        redStarForLname: true,
      });
    } else {
      this.setState({
        lastName: text,
        redStarForLname: false,
      });
    }
  }


  render() {
    const { firstName, lastName, companyName, mobile, Attendee_Data, email, keyboardOpen } = this.state;
    const first_name = (firstName !== '') ? firstName : '';
    const last_name = (lastName !== '') ? lastName : '';
    const  phone_no = (mobile !== '') ? mobile : '';
    let phoneno=phone_no.replace(/[^+\d]+/g,'')
    if(mobile.slice(0,1)=='+'){
      phoneno = phoneno.substring(3)
   }
    const email_Id = (email !== '') ? email : '';
    const company_Name = (companyName !== '') ? companyName : '';
    const tag = this.props.navigation.state.params.tag;
    const tappedIndex = this.props.navigation.state.params.tappedIndex;
    return (
      <View style={styles.api_container}>
        <KeyboardListener
          onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
          onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
        <View style={styles.api_blueView}>
          <Text style={styles.makePaymentText}>Attendees Details</Text>
        </View>
        <View style={styles.api_innerContaiuner}>
          <View style={[styles.editable_WhiteView, {
            borderBottomLeftRadius: (keyboardOpen) ? 0 : 5, borderBottomRightRadius: (keyboardOpen) ? 0 : 5,
            borderBottomWidth: (keyboardOpen) ? 0 : 0.5, borderBottomColor: (keyboardOpen) ? colors.white : colors.darkG
          }]}>
            <View style={styles.api_apiView}>
              <Text style={styles.api_apiText}>{tag}
              </Text>
            </View>
            <View style={styles.api_mainContainerView}>
              <KeyboardAwareScrollView
                keyboardDismissMode="interactive"
                bounces={false}
                showsVerticalScrollIndicator={false}
                extraHeight={globals.screenHeight * 0.27}
                extraScrollHeight={5}
                keyboardShouldPersistTaps="handled">
                <Text style={styles.api_infoTextStyles}>Attendee {tappedIndex + 1} Details</Text>
                <View style={styles.api_textfiledView}>
                  <TextInput
                    placeholder="Email Address"
                    placeholderTextColor={colors.lightGray}
                    style={styles.addPayerTextStyle}
                    returnKeyType="next"
                    autoCapitalize="none"
                    ref={emailadd => (this.emailadd = emailadd)}
                    onSubmitEditing={() => this.mobno.focus()}
                    value={email_Id}
                    onChangeText={text => this.handleEmailText(text)}
                  />
                  {this.state.redStarForEmail === false ? (
                    <Text style={styles.ad_textStyle}>*</Text>
                  ) : null}
                </View>
                <View style={styles.api_textfiledView}>
                  <TextInput
                    placeholder="Mobile Number"
                    maxLength={10}
                    keyboardType="number-pad"
                    placeholderTextColor={colors.lightGray}
                    style={styles.addPayerTextStyle}
                    value={phoneno}
                    autoCapitalize="none"
                    ref={mobno => (this.mobno = mobno)}
                    onSubmitEditing={() => this.firstnameref.focus()}
                    returnKeyType="done"
                    onChangeText={text => this.handlemobnoText(text)}
                  />
                </View>
                <View style={styles.api_textfiledView}>
                  <TextInput
                    placeholder="First Name"
                    placeholderTextColor={colors.lightGray}
                    style={styles.addPayerTextStyle}
                    value={first_name}
                    returnKeyType="next"
                    autoCapitalize="none"
                    ref={firstnameref => (this.firstnameref = firstnameref)}
                    onSubmitEditing={() => this.lasttnameref.focus()}
                    onChangeText={text => this.handleFirstNameText(text)}
                  />
                  {this.state.redStarForFname === false ? (
                    <Text style={styles.ad_textStyle}>*</Text>
                  ) : null}
                </View>
                <View style={styles.api_textfiledView}>
                  <TextInput
                    placeholder="Last Name"
                    placeholderTextColor={colors.lightGray}
                    style={styles.addPayerTextStyle}
                    value={last_name}
                    returnKeyType="next"
                    autoCapitalize="none"
                    ref={lasttnameref => (this.lasttnameref = lasttnameref)}
                    onSubmitEditing={() => this.companynameref.focus()}
                    onChangeText={text => this.handleLastNameText(text)}
                  />
                  {this.state.redStarForLname === false ? (
                    <Text style={styles.ad_textStyle}>*</Text>
                  ) : null}
                </View>
                <View style={styles.api_textfiledView}>
                  <TextInput
                  maxLength={70}
                    placeholder="Organization Name"
                    placeholderTextColor={colors.lightGray}
                    style={styles.addPayerTextStyle}
                    value={company_Name}
                    returnKeyType="done"
                    autoCapitalize="none"
                    ref={companynameref => (this.companynameref = companynameref)}
                    onChangeText={text => this.handleCompanynameText(text)} />
                  {this.state.redStarForCname === false ? (
                    <Text style={styles.ad_textStyle}>*</Text>
                  ) : null}
                </View>
              </KeyboardAwareScrollView>
            </View>
          </View>
        </View>
        {
          (keyboardOpen) ?
            null :
            <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.checkValidateInput()}>
              <Text style={styles.api_paymentTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.ADD}</Text>
            </TouchableOpacity>
        }
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AttendeeDetails2);