/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, TextInput, Alert, } from 'react-native';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import Validation from '../../../../../utils/validation';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import KeyboardListener from 'react-native-keyboard-listener';


var TAG = "AddPlayerInformation ::=="
let _this;
class AddPlayerInformation extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      email: '',
      mobile: '',
      firstName: '',
      lastName: '',
      Profession: '',
      notes: '',
      redStarForEmail: false,
      redStarForFname: false,
      redStarForLname: false,
      redStarForMobNo: false,
      keyboardOpen: false
    };
  }

  componentDidMount() {
    if (Object.keys(globals.payerInfoData).length === 0 && globals.payerInfoData.constructor === Object) {
    } else {
      let payerInfo = globals.payerInfoData;
      this.setState({
        email: payerInfo.email, mobile: payerInfo.mobNo, firstName: payerInfo.firstName, lastName: payerInfo.lastName, Profession: payerInfo.Profession, notes: payerInfo.notes,
        redStarForEmail: true, redStarForFname: true, redStarForLname: true, redStarForMobNo: true
      })
    }
  }


  /**
    * Validations for textFileds
    */
  checkValidateInput() {
    const { email, mobile, firstName, lastName, Profession, notes } = this.state;

    const mobileLength = mobile.length;

    if (Validation.textInputCheck(email)) {
      this.setState({ redStarForEmail: true });
      if (Validation.isValidEmail(email)) {
        this.setState({ redStarForEmail: true });
            if (Validation.textInputCheck(firstName)) {
              this.setState({ redStarForFname: true });
              if (Validation.allSpecialCharacter(firstName)) {
                this.setState({ redStarForFname: true });
                if (Validation.textInputCheck(lastName)) {
                  this.setState({ redStarForLname: true });
                  if (Validation.allSpecialCharacter(lastName)) {
                    this.setState({ redStarForLname: true });
                    let payerInfoObj = { 'email': email, 'mobNo': mobile, 'firstName': firstName, 'lastName': lastName, 'Profession': Profession, 'notes': notes }
                    globals.payerInfoData = payerInfoObj;

                    this.props.navigation.state.params.returnData({
                      UserID: globals.userID,
                      firstname: firstName,
                      lastname: lastName,
                      Email: email,
                      Mobile: mobile,
                      Profession: Profession,
                      notes: notes
                    });
                    this.props.navigation.goBack();
                  } else {
                    Alert.alert(globals.appName, globals.MESSAGE.ADD_PLAYER_INFO.VALID_LAST_NAME);
                  }
                } else {
                  Alert.alert(globals.appName, globals.MESSAGE.ADD_PLAYER_INFO.ENTER_LAST_NAME);
                  this.setState({ redStarForLname: false });
                }
              } else {
                Alert.alert(globals.appName, globals.MESSAGE.ADD_PLAYER_INFO.VALID_FIRST_NAME);

              }
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.ADD_PLAYER_INFO.ENTER_FIRST_NAME);
              this.setState({ redStarForFname: false });
            }
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.ADD_PLAYER_INFO.VALID_EMAIL);

      }

    } else {
      Alert.alert(globals.appName, globals.MESSAGE.ADD_PLAYER_INFO.ENTER_EMAIL_ID);
      this.setState({ redStarForEmail: false });
    }
  }

  /**
  * 
  * Upadte TextInput Data
  */
  handleEmailText(text) {
    if (text.length > 0) {
      this.setState({
        email: text,
        redStarForEmail: true,
      });
    } else {
      this.setState({
        email: text,
        redStarForEmail: false,
      });
    }
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handlemobnoText(text) {
    if (text.length > 0) {
      this.setState({
        mobile: text,
        redStarForMobNo: true,
      });
    } else {
      this.setState({
        mobile: text,
        redStarForMobNo: false,
      });
    }
  }
  handleFirstNameText(text) {
    if (text.length > 0) {
      this.setState({
        firstName: text,
        redStarForFname: true,
      });
    } else {
      this.setState({
        firstName: text,
        redStarForFname: false,
      });
    }
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handleLastNameText(text) {
    if (text.length > 0) {
      this.setState({
        lastName: text,
        redStarForLname: true,
      });
    } else {
      this.setState({
        lastName: text,
        redStarForLname: false,
      });
    }
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handleProfession(text) {
    this.setState({
      Profession: text,
    });
  }

  /**
   * 
   * Upadte TextInput Data
   */
  handleNotes(text) {
    this.setState({
      notes: text,
    });
  }



  render() {
    const { goBack } = this.props.navigation;
    const {keyboardOpen} = this.state;
    const payerInfoText = (Object.keys(globals.payerInfoData).length === 0 && globals.payerInfoData.constructor === Object) ? globals.BTNTEXT.PAYMENTSCREENS.ADD_PLAYER_INFO : "Edit Payer Information"
    const btnText = (Object.keys(globals.payerInfoData).length === 0 && globals.payerInfoData.constructor === Object) ? globals.BTNTEXT.PAYMENTSCREENS.ADD : "UPDATE"
    return (

      <View style={styles.api_container}>
        <KeyboardListener
          onDidShow={() => { this.setState({ keyboardOpen: true }, () => {console.log('onDidShow')}); }}
          onDidHide={() => { this.setState({ keyboardOpen: false },() => {console.log('onDidHide')}); }} />
        <View style={styles.api_blueView}>
          <Text style={styles.makePaymentText}>{globals.BTNTEXT.PAYMENTSCREENS.BOOKING_INFO}</Text>
        </View>
        <View style={styles.api_innerContaiuner}>
          <View style={[styles.editable_WhiteView,{borderBottomLeftRadius:(keyboardOpen) ? 0 : 5,borderBottomRightRadius:(keyboardOpen) ? 0 : 5,
            borderBottomWidth:(keyboardOpen) ? 0 : 0.5,borderBottomColor:(keyboardOpen) ? colors.white : colors.darkG}]}>
            <View style={styles.api_apiView}>
              <Text style={styles.api_apiText}>{payerInfoText}
              </Text>
              {/* <TouchableOpacity style={styles.api_iconView}>
                <Image
                  source={images.EventRegistration.editIcon}
                  style={styles.api_iconimgStyle}
                />
              </TouchableOpacity> */}
            </View>


            <View style={styles.api_mainContainerView}>

              <KeyboardAwareScrollView bounces={false}
                keyboardDismissMode="interactive"
                extraHeight={globals.screenHeight * 0.27}
                extraScrollHeight={5}
                showsVerticalScrollIndicator={false}
                keyboardShouldPersistTaps="handled"
              >
                <View style={styles.api_textfiledView}>

                  <TextInput
                    placeholderTextColor={colors.lightGray}
                    returnKeyType="next"
                    autoCapitalize="none"
                    blurOnSubmit={true}
                    placeholder={globals.MESSAGE.ADD_PLAYER_INFO.ENTER_EMAIL_ID}
                    value={this.state.email}
                    onSubmitEditing={() => this.mobno.focus()}
                    style={styles.api_textFiledTextStyle}
                    onChangeText={text => this.handleEmailText(text)}
                  />
                  {this.state.redStarForEmail === false ? (
                    <Text style={styles.api_textStyle}>*</Text>
                  ) : null}
                </View>
                <View style={styles.api_textfiledView}>

                  <TextInput
                    keyboardType="number-pad"
                    autoCapitalize="none"
                    maxLength={10}
                    blurOnSubmit={true}
                    returnKeyType="done"
                    placeholderTextColor={colors.lightGray}
                    placeholder={globals.MESSAGE.ADD_PLAYER_INFO.ENTER_MOBNO}
                    value={this.state.mobile}
                    ref={mobno => (this.mobno = mobno)}
                    onSubmitEditing={() => this.firstnameref.focus()}
                    onChangeText={text => this.handlemobnoText(text)}
                    style={styles.api_textFiledTextStyle}
                  />
                </View>
                <View style={styles.api_textfiledView}>

                  <TextInput
                    returnKeyType="next"
                    autoCapitalize="none"
                    placeholderTextColor={colors.lightGray}
                    value={this.state.firstName}
                    blurOnSubmit={true}
                    ref={firstnameref => (this.firstnameref = firstnameref)}
                    onSubmitEditing={() => this.lasttnameref.focus()}
                    onChangeText={text => this.handleFirstNameText(text)}
                    placeholder={globals.MESSAGE.ADD_PLAYER_INFO.ENTER_FIRST_NAME}
                    style={styles.api_textFiledTextStyle}
                  />
                  {this.state.redStarForFname === false ? (
                    <Text style={styles.api_textStyle}>*</Text>
                  ) : null}
                </View>
                <View style={styles.api_textfiledView}>

                  <TextInput
                    placeholderTextColor={colors.lightGray}
                    ref={lasttnameref => (this.lasttnameref = lasttnameref)}
                    onSubmitEditing={() => this.Professionref.focus()}
                    value={this.state.lastName}
                    blurOnSubmit={true}
                    onChangeText={text => this.handleLastNameText(text)}
                    returnKeyType="next"
                    autoCapitalize="none"
                    placeholder={globals.MESSAGE.ADD_PLAYER_INFO.ENTER_LAST_NAME}
                    style={styles.api_textFiledTextStyle}
                  />
                  {this.state.redStarForLname === false ? (
                    <Text style={styles.api_textStyle}>*</Text>
                  ) : null}
                </View>
                <View style={styles.api_textfiledView}>
                  <TextInput
                    returnKeyType="next"
                    autoCapitalize="none"
                    blurOnSubmit={true}
                    value={this.state.Profession}
                    ref={Professionref => (this.Professionref = Professionref)}
                    onSubmitEditing={() => this.Noteref.focus()}
                    placeholderTextColor={colors.lightGray}
                    onChangeText={text => this.handleProfession(text)}
                    placeholder={globals.MESSAGE.ADD_PLAYER_INFO.ORGANIZATION_NAME}
                    style={styles.api_textFiledTextStyle} />
                </View>
                <View style={[styles.api_textfiledView, { marginBottom: globals.screenHeight * 0.04, }]}>
                  <TextInput
                    multiline={true}
                    maxLength={100}
                    returnKeyType="done"
                    blurOnSubmit={true}
                    autoCapitalize="none"
                    value={this.state.notes}
                    ref={Noteref => (this.Noteref = Noteref)}
                    placeholderTextColor={colors.lightGray}
                    onChangeText={text => this.handleNotes(text)}
                    placeholder={globals.MESSAGE.ADD_PLAYER_INFO.ENTER_NOTE}
                    style={[styles.add_not, { paddingHorizontal: globals.screenWidth * 0.01 }]}
                  />
                </View>

              </KeyboardAwareScrollView>
            </View>

          </View>

        </View>

        {
          (keyboardOpen) ?
            null :
            <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.checkValidateInput()}>
              <Text style={styles.api_paymentTextStyle}>{btnText}</Text>
            </TouchableOpacity>
        }

      </View>


    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddPlayerInformation);