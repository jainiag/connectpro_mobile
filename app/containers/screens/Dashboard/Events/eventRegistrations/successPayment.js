/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, TextInput, BackHandler } from 'react-native';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as images from '../../../../../assets/images/map';

let TAG = "SuccessPayment Screen :===="

export default class SuccessPayment extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Registration', "Success"),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      transactionId: this.props.navigation.state.params.transactionId,
      itisFrom: this.props.navigation.state.params.transactionId,
      transactionDate: this.props.navigation.state.params.transactionDate,
      paymentId: this.props.navigation.state.params.paymentId,
      payerInfo: this.props.navigation.state.params.payerData,
      ticketInfo: this.props.navigation.state.params.ticketInfo,
      paymentIdBookingMethod: this.props.navigation.state.params.paymentIdBookingMethod,
      paymentMethodId: this.props.navigation.state.params.paymentMethodId
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    this.props.navigation.navigate("GLOBAL_EVENT_SCREEN_DASHBOARD")
  }
  render() {
    return (
      <View style={styles.cash_container}>
        <View style={styles.cash_makePaymentViewContainer}>
          {
            // (this.state.itisFrom == 'FreeEvent') ?
            <Text style={styles.cash_makePaymentTextStyle}>Make Payment</Text>
            // :
            // < Text style={styles.cash_makePaymentTextStyle}>Booking Confirmation</Text>
          }

        </View>
        <View style={styles.Cash_DetailMainViewStyle}>
          <View style={styles.successChildViewContainer}>
            <Image
              source={images.EventRegistration.successIcon}
              style={styles.successIconStyle}
              resizeMode="contain"
            />
            <Text style={styles.successTextStyle}>Success!</Text>
            <Text style={styles.bookingTextStyle}>Your booking processed successfully</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.cash_paymentButtonStyle} onPress={() => this.props.navigation.navigate("BookingReceipt", {
          transactionId: this.state.transactionId, paymentId: this.state.paymentId, payerInfo: this.state.payerInfo, ticketInfo: this.state.ticketInfo,
          paymentIdBookingMethod: this.state.paymentIdBookingMethod, paymentMethodId: this.state.paymentMethodId, transactionDate: this.state.transactionDate
        })}>
          <Text style={styles.cash_paymentTextStyle}>VIEW BOOKING RECEIPT</Text>
        </TouchableOpacity>
      </View >
    );
  }
}
