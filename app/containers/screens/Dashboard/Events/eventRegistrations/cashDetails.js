/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, TextInput } from 'react-native';
import styles from './style';
import * as colors from '../../../../../assets/styles/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';

class CashDetails extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      CashNotes: '',
    };
  }

  render() {
    return (
      <View style={styles.cash_container}>
        <View style={styles.cash_makePaymentViewContainer}>
          <Text style={styles.cash_makePaymentTextStyle}>Make Payment</Text>
        </View>
        <View style={styles.Cash_DetailMainViewStyle}>
          <View style={styles.cash_DetailChildViewContainer}>
            <Text style={styles.cash_DetailTextStyle}>Cash Details</Text>
            <View style={styles.cash_dashLineStyle} />
            <View style={styles.cash_textInputViewContainer}>
              <TextInput
                value={this.state.CashNotes}
                autoCapitalize="none"
                onFocus={false}
                returnKeyType="done"
                onChangeText={text => this.setState({ CashNotes: text })}
                placeholderTextColor={colors.darkgray}
                placeholder="Enter Cash Notes"
                style={styles.cash_textInputStyle}
              />
            </View>
            <View style={styles.cash_ViewStyle}>
              <Text style={styles.cash_TextStyle}>$160.00</Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={styles.cash_paymentButtonStyle}
          onPress={() => this.props.navigation.navigate('CHEQUE_DETAILS')}
        >
          <Text style={styles.cash_paymentTextStyle}>PAYMENT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CashDetails);
