
import React from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Alert
} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import { API } from '../../../../../utils/api';
import AsyncStorage from '@react-native-community/async-storage';

let _this = null;
let finalID;
const TAG = '==:== EventsDashboard : ';
class eventDashboard extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Event Dashboard'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    console.log("this.props", this.props);
    
    _this = this;
    this.state = {
      GridViewItems: [
        {
          key: 'About Event',
          icon: images.EventDashboard.aboutIcon,
        },
        {
          key: 'Agenda',
          icon: images.EventDashboard.agenda,
        },
        {
          key: 'Speakers',
          icon: images.EventDashboard.speakers,
        },
        {
          key: 'Attendees',
          icon: images.EventDashboard.attendees,
        },
        {
          key: 'Location',
          icon: images.EventDashboard.location,
        },
        {
          key: 'Sponsors',
          icon: images.EventDashboard.sponsors,
        },
        {
          key: 'Resources',
          icon: images.EventDashboard.resources,
        },
        {
          key: 'Registration',
          icon: images.EventDashboard.registration,
        },
        {
          key: 'Share',
          icon: images.EventDashboard.shareBtn,
        },
        // {
        //   key: 'Chat',
        //   icon: images.EventDashboard.chatIcon,
        // },
        // {
        //   key: 'Notification',
        //   icon: images.EventDashboard.notificationWhite,
        // },
        // {
        //   key: 'QR Scanner',
        //   icon: images.EventDashboard.qrScanner,
        // },
      ],
      imageHeight: 0,
      imageWidth: 0,
      imageRadius: 0,
      dataEventsList: this.props.navigation.state.params.selectedEventList,
      selectedEventListDeepLink:this.props.navigation.state.params.selectedEventListDeepLink,
      selectedTab: 1,
      loading: false,
      
      speakersData: [],
      agendasData: [],
      sponsorsData: [],
      documentsData: [],
      albumsData: [],
      videosData: [],
      loactiondata: [],
      aboutEventsData: [],
      shareurl: '',
      eventTitle: '',
      eventImg: '',
      isFreeStatus: true,
      allEventDetails: {}
    };
  }

  componentDidMount() {
    this.apiGetEventDetails()
  }

  /**
   * Api call of Get Event Details
   */
  apiGetEventDetails() {
    const eventID = this.props.navigation.state.params.itisFrom
    const selectedEventListDeepLink = this.props.navigation.state.params.selectedEventListDeepLink
     finalID = (eventID == "myEvent") ? this.state.dataEventsList.EventID : selectedEventListDeepLink
    this.props.showLoader()
    this.setState({ loading: true })
    AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
      globals.userID = token;
      
      if (globals.isInternetConnected === true) {
        API.getEventDetails(this.eventdetailsResponseData, true, finalID, globals.userID)
      } else {
        this.props.hideLoader()
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });

  }

  /**
     * Response callback of eventDetails
     */
  eventdetailsResponseData = {
    success: response => {
      console.log(
        TAG,
        'eventDetailsResponseData -> success : ',
        JSON.stringify(response)
      );

      if (response.StatusCode == 200 && response.Result == true) {        
        let { loactiondata, aboutEventsData, speakersData, agendasData, sponsorsData, documentsData, videosData, albumsData } = this.state;
        const locationevents = response.Data.EventDetails.Location;
        const speakers = response.Data.OtherInformation.Speakers;
        const agenda = response.Data.OtherInformation.Agendas;
        const sponsors = response.Data.OtherInformation.Sponsors;
        const documents = response.Data.EventResources.Resources.Documents;
        const albums = response.Data.EventResources.Resources.Albums;
        const videos = response.Data.EventResources.Resources.Videos;
        const shareUrl = response.Data.EventDetails.EventURL;
        const eventTitle = response.Data.EventDetails.Title;
        const eventImg = response.Data.EventDetails.EventLogo;
        const isFreeStatus = response.Data.EventDetails.IsFree;
        const eventDetails = response.Data.EventDetails;
        this.setState({ shareurl: shareUrl, eventTitle: eventTitle, eventImg: eventImg, isFreeStatus: isFreeStatus, 
          allEventDetails: eventDetails },()=>{
            var aboutevents = [];
            aboutevents = {
              logo: response.Data.EventDetails.EventLogo,
              text: response.Data.EventDetails.Description,
              title: response.Data.EventDetails.Title,
            }
    
            sponsorsData.push(sponsors);
            this.setState({ sponsorsData: sponsors, loading: false })
    
            agendasData.push(agenda);
            this.setState({ agendasData: agenda, loading: false })
    
            speakersData.push(speakers);
            this.setState({ speakersData: speakers, loading: false })
    
            // aboutEventsData.push(aboutevents);
            this.setState({ aboutEventsData: aboutevents, loading: false })
    
            // loactiondata.push(locationevents);
            this.setState({ loactiondata: locationevents, loading: false })
    
            albumsData.push(albums);
            this.setState({ albumsData: albums, loading: false })
    
            documentsData.push(documents);
            this.setState({ documentsData: documents, loading: false })
    
            videosData.push(videos);
            this.setState({ videosData: videos, loading: false })
            setTimeout(() => {
              this.props.hideLoader()
            }, 1000);
          })
     
      }
      else {
        this.setState({ loading: false },()=>{
          this.props.hideLoader()
        })
        Alert.alert(globals.appName, response.Message)

      }
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'eventDetailsResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      Alert.alert(
        globals.appName,
        err.Message
      );
    },
    complete: () => {
     // this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
   * method for Select Particular Item
   */
  setSelection(item, index) {
    const { GridViewItems } = this.state;
    const dataGridViewItems = GridViewItems;
    console.log('DAshboard indexItem : ', `${JSON.stringify(dataGridViewItems)}index${index}`);
    for (let i = 0; i < dataGridViewItems.length; i++) {
      if (index == i && item.key == 'About Event') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("AboutEvent", {isFreeStatus:this.state.dataEventsList, aboutEventsData: this.state.aboutEventsData, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Agenda') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("AgendaScreen", {isFreeStatus:this.state.dataEventsList, agendasData: this.state.agendasData, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Speakers') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("SPEAKER_SCREEN", {isFreeStatus:this.state.dataEventsList, speakersData: this.state.speakersData, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Attendees') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("Attendes", {dataEventsList: this.state.dataEventsList, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Location') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("LOCATION_SCREEN", { dataEventsList: this.state.dataEventsList, loactiondata: this.state.loactiondata, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Sponsors') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("SPONSORS_SCREEN", {isFreeStatus:this.state.dataEventsList, sponsorsData: this.state.sponsorsData, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Resources') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("ResourcesTab", {isFreeStatus:this.state.dataEventsList, albumsData: this.state.albumsData, documentsData: this.state.documentsData, videosData: this.state.videosData, eventID: finalID, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Registration') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate('EVENT_REGISTRATION', { dataEventsList: this.state.dataEventsList,eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Share') {
        dataGridViewItems[index].isSelected = true;
        this.props.navigation.navigate("SHAREEVENT_SCREEN", { dataEventsList: this.state.dataEventsList, loactiondata: this.state.loactiondata, eventID: finalID, shareUrl: this.state.shareurl, allEventDetails: this.state.allEventDetails })
      } else if (index == i && item.key == 'Chat') {
        dataGridViewItems[index].isSelected = true;
      } else if (index == i && item.key == 'Notification') {
        dataGridViewItems[index].isSelected = true;
      } else if (index == i && item.key == 'QR Scanner') {
        dataGridViewItems[index].isSelected = true;
      } else {
        dataGridViewItems[i].isSelected = false;
      }
    }
    this.setState({ GridViewItems: dataGridViewItems }, () => {
      // this.forceUpdate()
    });
  }

  /**
   * Render FlatList Items
   */
  _renderItems(item, index) {
    return (
      <View style={styles.vsItemParentStyle}>
        <View style={styles.gridViewStyle}>
          <TouchableOpacity
            onPress={() => {
              this.setSelection(item, index);
            }}
          >
            <View style={styles.gridViewBlockStyle}>
              <Image source={item.icon} style={styles.imgStyle} resizeMode="contain" />
              <Text numberOfLines={1} style={styles.gridViewInsideTextItemStyle}>
                {' '}
                {item.key}{' '}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    const { GridViewItems, dataEventsList, eventTitle, eventImg, isFreeStatus, allEventDetails } = this.state;
     const isEventStartDate = globals.checkObject(allEventDetails, 'StartDate');
     const isEventEndDat = globals.checkObject(allEventDetails, 'EndDate');
    const isEventEndDate = globals.checkObject(allEventDetails, 'EndDateLocal');
    const isEventname = globals.checkObject(allEventDetails, 'Title')
    const isImage = globals.checkImageObject(allEventDetails, 'EventLogo');
     const starttime = (isEventStartDate) ? moment(allEventDetails.StartDate).format('LT') : '-';
    const endtime = (isEventEndDate) ? moment(allEventDetails.EndDateLocal).format('LT') : '-';
    const month = (isEventStartDate) ? moment(allEventDetails.StartDate).format('MMM') : '-';
    const thDate = (isEventStartDate) ? moment(allEventDetails.StartDate).format('Do') : '';
    const onlyDt = (isEventStartDate) ? moment(allEventDetails.StartDate).format('D') : '';

    return (
      <View>
        <NavigationEvents
          onWillFocus={() => this.apiGetEventDetails()}
        />
        <View>
          <View style={[styles.ed_ItemParentPaddingStyle, { color: colors.white }]}>
            <View style={styles.headerParentStyle}>
              <View style={styles.beforeImgViewStyle}>
                <Image  style={styles.ivEventImageStyle} source={{ uri: (isImage) ? eventImg : globals.Event_img }} />
              </View>
              <View style={styles.eventInfoParentStyle}>
                <Text numberOfLines={1} style={[styles.tvLabelStyle, { width: "85%" }]}>
                  {(isEventname) ? eventTitle : '-'}
                </Text>
                <Text numberOfLines={1}
                  style={[
                    styles.tvTimeStyle,
                    { width: "85%" },
                  ]}
                >{(isEventStartDate && isEventEndDat) ? `${allEventDetails.StartDate} To ${allEventDetails.EndDate}` : '-'}</Text>

                <Text style={styles.tvEvenTypeStyle}>{(isFreeStatus) ? 'Free' : 'Paid'}</Text>
              </View>
              <View style={styles.vwDateStyle}>
                <View style={{ alignItems: 'center' }}>
                  <Text style={[styles.tvLabelStyle, { color: colors.listSelectColor }]}>
                    {month}
                  </Text>
                  <View style={styles.vwDateSubViewStyle}>
                    <Text style={[styles.tvDateStyle, { color: colors.listSelectColor }]}>
                      {onlyDt}
                    </Text>
                    <Text style={[styles.tvthStyle, { color: colors.listSelectColor }]}>
                      {thDate.charAt(thDate.length - 2)}
                      {thDate.charAt(thDate.length - 1)}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.vwSimpleLineStyle} />
        </View>

        <ImageBackground
          source={images.InnerBg.InnerBg}
          style={styles.ed_dashboardView}
        >
          <FlatList
            style={styles.fvStyle}
            data={GridViewItems}
            renderItem={({ item, index }) => this._renderItems(item, index)}
            numColumns={3}
            keyExtractor={(x, i) => i.toString()}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
          />
        </ImageBackground>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(eventDashboard);

