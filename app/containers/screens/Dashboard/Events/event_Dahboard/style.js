import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { screenHeight, screenWidth } from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  ed_mainContainer: {
    backgroundColor: colors.white,
    flex: 1,
  },
  ed_eventListStyle: {
    flex: 0.2,
    backgroundColor: 'red',
    padding: 10,
  },
  ed_ItemPaddingStyle: {
    marginHorizontal: screenWidth * 0.0312, //10
    marginVertical: screenHeight * 0.0473, //25
  },
  ed_FlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  ed_ItemImageStyle: {
    width: '20%',
  },
  ed_ItemInfoStyle: {
    width: '60%',
  },
  ed_EventNameStyle: {
    fontSize: globals.font_14,
    paddingBottom: 1,
  },
  ed_TimeStyle: {
    fontSize: globals.font_12,
  },
  ed_TypeStyle: {
    fontSize: globals.font_10,
  },
  ed_DateStyle: {
    fontSize: globals.font_14,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  notAvailableDataViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 25,
  },
  tvpaymentTypeStyle: {
    color: colors.redColor,
    fontSize: globals.font_12,
  },
  tvthStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    paddingBottom: 2,
  },
  ed_DateStyle: {
    fontSize: globals.font_14,
  },
  ed_DateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  ed_ThStyle: {
    fontSize: globals.font_10,
  },
  ed_LabelStyle: {
    fontSize: globals.font_14,
  },
  ed_ItemDateStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
  ed_ItemParentPaddingStyle: {
    marginHorizontal: 0,
  },
  ed_dashboardView: {
    height: '90%',
    width: '100%',
  },
  ed_SimpleLineStyle: {
    width: '90%',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 2,
    height: 1,
    backgroundColor: colors.gray,
    opacity: 0.5,
  },
  vsItemParentStyle: {
    flex: 1,
  },
  beforeImgViewStyle:{
    height: screenHeight * 0.0757,//40
    width: screenHeight * 0.0757,//40
    borderRadius: screenHeight * 0.0378,
    borderColor: colors.lightBlack,
    borderWidth: 0.2,
  },
  gridViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.10,
    margin: 10,
  },
  imgStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.093,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.093,
  },
  gridViewInsideTextItemStyle: {
    marginTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.005,
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 25 : globals.font_14,
    justifyContent: 'center',
    color: colors.white,
    alignItems: 'center',
  },
  unSelectedgridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.17,
    width: screenHeight * 0.17,
    margin: 10,
    backgroundColor: colors.white,
    borderRadius: (screenHeight * 0.17) / 2,
  },
  unSelectedimgStyle: {
    height: 25,
    width: 25,
    tintColor: colors.bgColor,
  },
  unSelectedgridViewInsideTextItemStyle: {
    paddingTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.02,
    fontSize: globals.font_14,
    justifyContent: 'center',
    color: colors.bgColor,
    alignItems: 'center',
  },

  headerParentStyle: {
    flexDirection: 'row',
    height: screenHeight * 0.18,
    alignItems: 'center',
    paddingHorizontal: screenWidth * 0.05,
  },
  ivEventImageStyle: {
    height: screenHeight * 0.0757,//40
    width: screenHeight * 0.0757,//40
    borderRadius: screenHeight * 0.0378,//20
  },
  eventInfoParentStyle: {
    width: screenWidth * 0.60,
    justifyContent: 'center',
    marginLeft: screenWidth * 0.0468, //15
  },
  tvDateTimeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_12,
  },
  tvLabelStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  tvDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
  },
  tvBlueFontStyle: {
    color: colors.blue,
  },
  tvLabeldtStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_14,
  },
  vwDateStyle: {
    flex: 1,
    paddingBottom: globals.screenHeight * 0.012,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  tvEvenTypeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 16 : globals.font_10,
    color: colors.blue,
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fvStyle: {
    marginTop: screenHeight * 0.08,
  },
});
