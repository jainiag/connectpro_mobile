import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel()
module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
     marginTop: globals.screenHeight * 0.02,
  },
  mainContainer: {
    flex: 1
  },
  registerBtnStyle: {
    width: '100%',
    height: 55,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  registerTextStyle: {
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 25 : globals.font_18,
    color: colors.white,
  },
  titleStyle: {
    // marginTop: globals.screenHeight * 0.02,
    marginLeft: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.045 : globals.screenHeight * 0.03,
    // flex:1
  },
  textStyle: {
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 25 : globals.font_16,
    color: colors.warmBlue
  },
  silverFlatlistStyle: {
    //  backgroundColor:"red",
     marginTop: globals.screenHeight * 0.01,
    marginBottom: globals.screenHeight * 0.03,
    marginHorizontal: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.031 : globals.screenHeight * 0.018,
  },
  directsilverItem:{
    backgroundColor:'red',
    marginHorizontal: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.031 : globals.screenHeight * 0.018,
  },
  flatlistStyle: {
    // marginTop:globals.screenHeight * 0.02,
     marginVertical: globals.screenHeight * 0.02,
    //  backgroundColor:'red',
    marginHorizontal: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.031 : globals.screenHeight * 0.018,
  },
  boxViewStyle: {
    borderWidth: 1,
    borderColor: colors.lightBorder,
    // paddingVertical:(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth* 0.05:globals.screenWidth* 0.07,
    // paddingHorizontal:globals.screenWidth* 0.095,
    margin: globals.screenWidth * 0.017,
    borderRadius: globals.screenWidth * 0.02,
    height: globals.screenWidth * 0.20,
    width: globals.screenWidth * 0.27,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgStyle: {
    height: globals.screenWidth * 0.14,
    width: globals.screenWidth * 0.20,
    alignSelf: 'center',
    // backgroundColor:"red"
  },
  nodataStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  nodataTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
  },

  //section grid
  gridView: {
    marginTop: 20,
    flex: 1,
    marginBottom:globals.screenHeight * 0.07,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: '#fff',
  },
  sectionHeader: {
    flex: 1,
    fontSize: globals.font_17,
    marginLeft:globals.screenWidth * 0.04,
    fontWeight: '500',
    alignItems: 'center',
    // backgroundColor: '#636e72',
    color: colors.warmBlue,
    padding: 5,
  },
  spoonsorsName:{
    flex: 1,
    fontSize: globals.font_12,
    fontWeight:'600',
    marginHorizontal:3,
    marginTop:3,
    alignItems: 'center',
    color: colors.sponsorsTextColor,
  }
});
