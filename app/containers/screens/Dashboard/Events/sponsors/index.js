/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, ScrollView, Image,Linking } from 'react-native';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { SectionGrid } from 'react-native-super-grid';



let TAG = "==:== Sponsors : "
let sectionArr = [];

class Sponsors extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Sponsors'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });


    constructor(props) {
        super(props);
        this.state = {
            isFreeStatus: this.props.navigation.state.params.isFreeStatus,
            sponsorsData: this.props.navigation.state.params.sponsorsData,
            sponsersGoldItem: [],
            sponsersSilverItem: [],
            eventID: this.props.navigation.state.params.eventID,
            allEventDetails: this.props.navigation.state.params.allEventDetails
        };
    }

    componentDidMount() {
        const { sponsersSilverItem, sponsersGoldItem, sponsorsData } = this.state;
        var result = sponsorsData.reduce(function (r, o) {
            var k = o.selectedType;   // unique `loc` key
            if (r[k] || (r[k] = [])) r[k].push({ selectedType: k, ImagePath: o.ImagePath, Name: o.Name, Url: o.Url });
            return r;
        }, {});

       

        let finlObj = result;

        let finalSectionArr = [];
        sectionArr=[];
        for (var key in finlObj) {
            if (finlObj.hasOwnProperty(key)) {
                var value = finlObj[key];
                //do something with value
            }
            sectionArr.push({ title: key, data: value })
            
        }
        finalSectionArr.push(sectionArr)
        


        for (let i = 0; i < sponsorsData.length; i++) {
            if (sponsorsData[i].selectedType == 'Gold') {
                sponsersGoldItem.push(sponsorsData[i])
                this.setState({ sponsersGoldItem: sponsersGoldItem })
            }
            else {
                sponsersSilverItem.push(sponsorsData[i])
                this.setState({ sponsersSilverItem: sponsersSilverItem })
            }
        }
    }

    /**
      * renderGoldItemList Data
      */
    renderGoldItemList(item, index) {
        const isGoldImage = globals.checkImageObject(item.ImagePath, 'ImagePath')
        return (
            <View style={styles.boxViewStyle}>
                <Image
                    resizeMode={"contain"}
                    source={{ uri: (isGoldImage) ? item.ImagePath : globals.Square_img }}
                    style={styles.imgStyle}>
                </Image>
            </View>
        )
    }

    /**
      * renderSilverItemList Data
      */
    renderSilverItemList(item, index) {
        const isSilverimage = globals.checkImageObject(item.ImagePath, 'ImagePath')
        return (
            <View style={styles.boxViewStyle}>
                <Image
                    resizeMode={"contain"}
                    source={{ uri: (isSilverimage) ? item.ImagePath : globals.Square_img }}
                    style={styles.imgStyle}>
                </Image>
            </View>
        )
    }

    render() {
        const { sponsersGoldItem, sponsersSilverItem, sponsorsData } = this.state;
        
        return (
            <>
                {
                    (sponsorsData == '') ?
                        <View style={globalStyles.nodataStyle}>
                            <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.SPOSORS_DATA_NOT_AVLB}</Text>
                        </View>
                        :

                        <View style={styles.mainParentStyle}>
                            <View style={styles.mainContainer}>
                                {/* <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                                    {
                                        (sponsersGoldItem.length > 0) ?
                                            <View style={styles.mainContainer}>
                                                <View style={[styles.titleStyle, { marginTop: globals.screenHeight * 0.02, }]}>
                                                    <Text style={styles.textStyle}>
                                                        {
                                                            (sponsersGoldItem.length > 0) ? "Gold Sponsors" : null
                                                        }
                                                    </Text>
                                                </View>
                                                <View style={styles.flatlistStyle}>
                                                    <FlatList
                                                        numColumns={3}
                                                        data={sponsersGoldItem}
                                                        renderItem={({ item, index }) => this.renderGoldItemList(item, index)}
                                                        keyExtractor={(index, item) => item.toString()}
                                                        extraData={this.state}
                                                    />
                                                </View>
                                            </View> : null
                                    } */}

                                    {/* <View style={styles.mainContainer}>
                                        <View style={styles.titleStyle}>
                                            <Text style={styles.textStyle}>

                                                {
                                                    (sponsersSilverItem.length > 0) ? "Silver Sponsors" : null
                                                }
                                            </Text>
                                        </View>
                                        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                                            <View style={styles.silverFlatlistStyle}>
                                                <FlatList
                                                    numColumns={3}
                                                    data={sponsersSilverItem}
                                                    renderItem={({ item, index }) => this.renderSilverItemList(item, index)}
                                                    keyExtractor={(index, item) => item.toString()}
                                                    extraData={this.state}
                                                />

                                            </View>
                                        </ScrollView>
                                    </View> */}
                                    <ScrollView style={{flex:1, }}>
                                    <SectionGrid
                                        itemDimension={globals.screenWidth*0.25}
                                        // staticDimension={300}
                                        // fixed
                                        // spacing={20}
                                        sections={sectionArr}
                                        style={styles.gridView}
                                        renderItem={({ item, section, index }) => (
                                            <TouchableOpacity onPress={() => Linking.openURL(item.Url)}>
                                                <View style={styles.boxViewStyle}>
                                                    <Image
                                                        resizeMode={"contain"}
                                                        source={{ uri: item.ImagePath }}
                                                        style={styles.imgStyle}>
                                                    </Image>
                                                    <Text numberOfLines={1} style={styles.spoonsorsName}>{item.Name}</Text>
                                                </View>
                                            </TouchableOpacity>
                                          
                                        )}
                                        renderSectionHeader={({ section }) => (
                                            <Text style={styles.sectionHeader}>{(section.title +" "+"Sponsors")}</Text>
                                        )}
                                    />
                                </ScrollView>
                            </View>
                            <TouchableOpacity style={styles.registerBtnStyle} onPress={() => this.props.navigation.navigate('EVENT_REGISTRATION', { dataEventsList: this.state.isFreeStatus, eventID: this.state.eventID, allEventDetails: this.state.allEventDetails })}>
                                <Text style={styles.registerTextStyle}>{globals.BTNTEXT.REGISTERSCREEN.REG_BTNTEXT}</Text>
                            </TouchableOpacity>

                        </View>

                }

            </>
        );
    }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Sponsors);
