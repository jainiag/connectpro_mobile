/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import styles from './style';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as images from '../../../../../assets/images/map';
import globalStyles from '../../../../../assets/styles/globleStyles'
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';

let TAG = "==:== Location : "
const iPad = DeviceInfo.getModel();


class Location extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerLeft: globals.ConnectProbackButton(navigation, 'Location'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            dataEventsList: this.props.navigation.state.params.dataEventsList,
            loactiondata: this.props.navigation.state.params.loactiondata,
            eventID: this.props.navigation.state.params.eventID,
            shareUrl: this.props.navigation.state.params.shareUrl,
            eventDetails: this.props.navigation.state.params.allEventDetails,
            allEventDetails: this.props.navigation.state.params.allEventDetails
        }
    }

    openViewMap() {

        this.props.navigation.navigate("VIEW_MAP", { loactiondata: this.state.loactiondata, shareUrl: this.state.shareUrl });
    }

    render() {
        let { dataEventsList, loactiondata, eventDetails } = this.state;
        const isEventStartDate = globals.checkObject(eventDetails, 'StartDate');
        const isEventEndDate = globals.checkObject(eventDetails, 'EndDateLocal');
        const isEventname = globals.checkObject(eventDetails, 'Title')
        const isImage = globals.checkImageObject(eventDetails, 'EventLogo');
        const isLocation = globals.checkObject(loactiondata, 'Location');
        let starttime = (isEventStartDate) ? moment(eventDetails.StartDate).format('LT') : '-';
        let endtime = (isEventEndDate) ? moment(eventDetails.EndDateLocal).format('LT') : '-';
        let month = (isEventStartDate) ? moment(eventDetails.StartDate).format('MMM') : '-';
        let thDate = (isEventStartDate) ? moment(eventDetails.StartDate).format('Do') : '-';
        let onlyDt = (isEventStartDate) ? moment(eventDetails.StartDate).format('D') : '-';
        let fullMonthName = (isEventStartDate) ? moment(eventDetails.StartDate).format('MMMM') : '-';
        let year = (isEventStartDate) ? moment(eventDetails.StartDate).format('YYYY') : '-';


        return (
            <>
                {
                    (loactiondata == '') ?
                        <View style={globalStyles.nodataStyle}>
                            <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.LOCATION_DATA_NOT_AVLB}</Text>
                        </View>
                        :
                        <View style={styles.mainParentStyle}>

                            <View style={styles.subParentStyle}>
                                <View style={styles.headerParentStyle}>
                                    <View style={styles.beforeImgViewStyle}>
                                        <Image style={styles.ivEventImageStyle} source={{ uri: (isImage) ? eventDetails.EventLogo : globals.Event_img }} />
                                    </View>
                                    <View style={styles.eventInfoParentStyle}>
                                        <Text numberOfLines={1} style={[styles.tvLabelStyle, {
                                            fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_15,
                                        }]}>{(isEventname) ? eventDetails.Title : '-'}</Text>
                                        <Text style={styles.tvDateTimeStyle}>{starttime}{' - '}{endtime}</Text>
                                    </View>
                                    <View style={styles.vwDateStyle}>
                                        <View style={{ alignItems: 'center' }}>
                                            <Text style={[styles.tvLabelStyle, {fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_14, color: colors.listSelectColor }]}>
                                                {month}
                                            </Text>
                                            <View style={styles.vwDateSubViewStyle}>
                                                <Text style={[styles.tvDateStyle, {fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_14, color: colors.listSelectColor }]}>
                                                    {onlyDt}
                                                </Text>
                                                <Text style={[styles.tvthStyle, {paddingBottom:4, fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_12, color: colors.listSelectColor }]}>
                                                    {thDate.charAt(thDate.length - 2)}
                                                    {thDate.charAt(thDate.length - 1)}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.lineStyle} />



                                <View style={styles.vwCalenderViewStyle}>
                                    <View style={styles.vwSubCalenderStyle}>
                                        <Image source={images.EventDashboard.calendarBlue} style={styles.ivCalenderImageStyle} resizeMode={"contain"} />
                                        <View style={styles.vwDateBelowCalenderStyle}>

                                            <View style={[styles.vwDateSubViewStyle, styles.vwLeftMarginDateStyle]}>
                                                <Text style={styles.tvBigDateStyle}>{fullMonthName + " "}</Text>
                                                <Text style={[styles.tvLabelStyle, styles.tvBigDateStyle]}>{onlyDt}</Text>
                                                <Text style={[styles.tvThStyle2]}>{thDate.charAt(thDate.length - 2)}{thDate.charAt(thDate.length - 1)}</Text>
                                                <Text style={[styles.tvLabelStyle, { marginLeft: globals.screenWidth * 0.0212}]}>{year}</Text>
                                            </View>
                                        </View>

                                        <Text style={[styles.tvBigDateStyle, styles.tvBigTimeStyle]}>{'('}{starttime}{' - '}{endtime}{')'}</Text>
                                    </View>
                                </View>

                                <View style={styles.lineStyle} />

                                <View style={styles.vwCalenderViewStyle}>
                                    <View style={styles.vwSubCalenderStyle}>
                                        <Image source={images.EventDashboard.locationBlue} style={styles.ivMapImageStyle} resizeMode={"contain"} />
                                        <View style={[styles.vwDateBelowCalenderStyle]}>
                                            <Text style={[styles.tvBigDateStyle, { textAlign: 'center' }]}>{(isLocation) ? loactiondata.Location : '-'}</Text>
                                        </View>
                                    </View>
                                </View>

                                <View style={styles.lineStyle} />

                                <View style={styles.vwViewMapStyle}>
                                    <TouchableOpacity onPress={() => this.openViewMap()} style={styles.toViewMapStyle}>
                                        <Text style={styles.tvViewMapStyle}>{'VIEW MAP'}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>



                            <TouchableOpacity style={styles.toBottomStyle} onPress={() => this.props.navigation.navigate('EVENT_REGISTRATION', { dataEventsList: this.state.dataEventsList, eventID: this.state.eventID, allEventDetails: this.state.allEventDetails })}>
                                <Text style={styles.tvRegisterStyle}>{'REGISTER'}</Text>
                            </TouchableOpacity>
                        </View>
                }
            </>

        );
    }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Location);
