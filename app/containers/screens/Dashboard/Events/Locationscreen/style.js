import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { screenHeight, screenWidth } from '../../../../../utils/globals';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
    marginTop: globals.screenHeight * 0.02,
  },
  tvthStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    paddingBottom: 2,
  },
  tvDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
  },
  nodataStyle:{
    alignItems:'center',
    justifyContent:'center',
    flex:1
  },
  nodataTextStyle:{
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28: globals.font_14
  },
  beforeImgViewStyle:{
    height: screenHeight * 0.0757, // 40
    width: screenHeight * 0.0757, // 40
    borderRadius: screenHeight * 0.0378, 
    borderColor: colors.lightBlack,
    borderWidth: 0.2,
  },
  subParentStyle: {
    flex: 1,
    paddingHorizontal: screenWidth * 0.0625, // 20
    paddingTop: screenHeight * 0.0378, //20
    marginBottom: screenHeight * 0.0757, //40
  },
  toBottomStyle: {
    position: 'absolute',
    bottom: 0,
    height: screenHeight * 0.0757, // 40
    width: '100%',
    backgroundColor: colors.bgColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tvRegisterStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_18,
    color: colors.white,
  },
  ivEventImageStyle: {
    height: screenHeight * 0.0757, // 40
    width: screenHeight * 0.0757, // 40
    borderRadius: screenHeight * 0.0378, //20
  },
  eventInfoParentStyle: {
    width: '60%',
    justifyContent: 'center',
    marginLeft: screenWidth * 0.0468, // 15
  },
  headerParentStyle: {
    flexDirection: 'row',
  },
  tvLabelStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_16,
  },
  tvSubLabelStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12,
  },
  vwDateStyle: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
    // backgroundColor:'red'
  },
  tvThStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_10,
  },
  lineStyle: {
    width: '100%',
    height: 1,
    backgroundColor: colors.lightBorder,
    marginTop: screenHeight * 0.0378, //20
  },
  tvBlueFontStyle: {
    color: colors.listSelectColor,
  },
  vwCalenderViewStyle: {
    alignItems: 'center',
  },
  vwSubCalenderStyle: {
    marginTop: screenHeight * 0.035,
    alignItems: 'center',
  },
  ivCalenderImageStyle: {
    height: screenHeight * 0.0568, // 30
    width: screenHeight * 0.0568, // 50
    marginBottom: screenHeight * 0.0284, // 15
  },

  ivMapImageStyle: {
    height: screenHeight * 0.05, // 30
    width: screenHeight * 0.04, // 50
    marginBottom: screenHeight * 0.0284, // 15
  },
  vwDateBelowCalenderStyle: {
    flexDirection: 'row',
  },
  tvBigDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 29 : globals.font_19,
  },
  vwLeftMarginDateStyle: {
    marginLeft: screenWidth * 0.0312, // 10
  },
  tvThStyle2: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 24 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.009,
    // backgroundColor:'red'
  },
  tvBigTimeStyle: {
    marginTop: screenHeight * 0.0094, // 5
    color: colors.darkgrey,
    marginBottom: screenHeight * 0.0094, // 5
  },
  toViewMapStyle: {
    paddingHorizontal: screenWidth * 0.0625, // 20
    paddingVertical: screenHeight * 0.0189, // 10
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.listSelectColor,
    width: screenWidth * 0.45, // 160
    height:  iPad.indexOf('iPad') != -1 ? screenHeight * 0.062 : globals.iPhoneX ? screenHeight * 0.06 : (Platform.OS == 'ios') ? screenHeight * 0.072 : screenHeight * 0.06, // 38
    borderRadius: screenWidth * 0.02, //10
  },
  tvViewMapStyle: {
    color: colors.white,
    fontWeight: '600',
    fontSize: DeviceInfo.isTablet() ? 27 : iPad.indexOf('iPad') != -1 ? 25 : globals.font_16,
  },
  tvDateTimeStyle: {
    color: colors.darkgrey,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12,
  },
  vwViewMapStyle: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
