/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */

import React from 'react';
import { Text, View, TouchableOpacity, FlatList,  Linking } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';


const TAG = '==:== Event Resources Videos: ';
let _this;
const iPad = DeviceInfo.getModel();

class Videos extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    const attendeeCount = (params !== undefined) ? params.attendeeCount : ''
    return {
      headerLeft: lobals.ConnectProbackButton(navigation, 'Resources'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }
  };

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      noVideosDataView: false,
      isFreeStatus:this.props.isFreeStatus,
      allEventDetails: this.props.allEventDetails
    };
  }

  componentDidMount() {
    if (this.props.videosData.length == 0) {
      this.setState({ noVideosDataView: true })
    }
  }

  /**
   * Render item list event resource Album
   * @param {*} item 
   * @param {*} index 
   */
  renderItemList(item, index) {
    const isUrl = globals.checkObject(item, 'Url');
    const isTitle = globals.checkObject(item, 'Title');
    const isDescription = globals.checkObject(item, 'Description');
    return (
      
      <View style={[styles.mainParentStyle,{marginTop:globals.screenHeight *0.012}]}>
      <Text onPress={() => Linking.openURL(item.Url)} style={styles.albumtitle}>{(isTitle) ? item.Title : '-'}</Text>
      <Text style={styles.description}>{(isDescription) ? item.Description : '-'}</Text>
      <View style={styles.ed_SimpleLineStyle}></View>
    </View>
    );
  }


  render() {
    const { noVideosDataView } = this.state;
    

    return (
      <View style={styles.mainParentStyle}>
        {
          (noVideosDataView) ?
            <View style={[globalStyles.nodataStyle]}>
              <Text style={globalStyles.nodataTextStyle}>
                {globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.EVENT_RESOURCE_VIDEOS_NOT_AVAILABLE}
              </Text>
            </View> :
            <FlatList
              data={this.props.videosData}
              renderItem={({ item, index }) => this.renderItemList(item, index)}
              keyExtractor={(index, item) => item.toString()}
              extraData={this.state}
            />
        }
        <TouchableOpacity
          style={styles.regBtnStyle} onPress={() => this.props.navigationProps.navigation.navigate('EVENT_REGISTRATION', {dataEventsList:this.state.isFreeStatus, eventID: this.props.eventID, allEventDetails: this.state.allEventDetails })}
        >
          <Text style={styles.regBtnTextStyle}>{globals.BTNTEXT.REGISTERSCREEN.REG_BTNTEXT}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Videos);
