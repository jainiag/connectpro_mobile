import { StyleSheet } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  tabViewContainer: {
    width: globals.screenWidth,
    alignSelf: 'center',
    // marginTop: globals.screenHeight * 0.018,
  },
  albumtitle: {
    color: colors.listSelectColor,
    fontSize: globals.font_14,
    marginTop: globals.screenWidth * 0.03,
    marginBottom:globals.screenWidth * 0.015,
    marginHorizontal: globals.screenWidth * 0.04,
    width: '70%'
  },
  ed_SimpleLineStyle: {
    width: '100%',
    position: 'absolute',
    alignSelf: 'center',
    bottom: 2,
    height: 1,
    backgroundColor: colors.gray,
    opacity: 0.5,
  },
  description: {
    color: colors.lightGray,
    fontSize: globals.font_14,
    // marginTop: globals.screenWidth * 0.01,
    marginBottom:globals.screenWidth * 0.03,
    marginHorizontal: globals.screenWidth * 0.04,
    width: '88%'
  },
  tabbarStyle: {
    width: globals.screenWidth,
    borderBottomWidth: 1,
    borderBottomColor: colors.proUnderline,
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: globals.font_15,
    elevation: 0,
    shadowOpacity: 0,
    height: globals.screenHeight * 0.08,
    backgroundColor: colors.white
  },
  tabStyle: {
    backgroundColor: colors.transparent,
    width: globals.screenWidth * 0.3,
  },
  indicatorStyle: {
    backgroundColor: colors.warmBlue,
    height: 4,
    width: globals.screenWidth * 0.15,
    marginLeft: globals.screenWidth * 0.08,
    borderRadius: 3
  },
  labelStyle: {
    textAlign: 'center',
    width: globals.screenWidth / 3,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_15,
    // letterSpacing: globals.ltr_sp_016,
    color: colors.borderColor,
  },
  initialLayoutStyle: {
    width: globals.screenWidth,
    flex: 1,
    height: 20,
  },
  mainRenderItemView: {
    borderColor: colors.proUnderline, borderWidth: 1, marginVertical: globals.screenHeight * 0.015, borderRadius: 3, flex: 1,
    marginHorizontal: globals.screenWidth * 0.06
  },
  urlTextStyle: {
    margin: globals.screenHeight * 0.015,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  data_notAvlbViewContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  data_notAvlb: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_15,
  },
  horizontalView: {
    flexDirection: 'row'
  },
  pdfImgView: {
    width: globals.screenWidth * 0.15, backgroundColor: colors.warmBlue, borderTopLeftRadius: 4, borderBottomLeftRadius: 4, alignItems: 'center', justifyContent: 'center'
  },
  pdfImgStyle: {
    height: globals.screenHeight * 0.037, width: globals.screenHeight * 0.037
  },
  regBtnTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_20,
    color: colors.white,
  },
  regBtnStyle: {
    width: '100%',
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 65 : 55,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },

});

