/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */

import React from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './style';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import PDFView from 'react-native-view-pdf';

const TAG = '==:== Event Resources PdfView: ';
let _this;
let resources;

class PdfView extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return{
      headerLeft:globals.ConnectProbackButton(navigation, 'Resources'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }
  };

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      screenName:this.props.navigation.state.params.screenName,
      pdfUrl:''
    };
  }

  componentWillMount() {
      resources = {
        url:(this.state.screenName == 'eventResources') ? this.props.navigation.state.params.pdfData.Url : this.props.navigation.state.params.pdfData.AttachmentURL
      }
      this.setState({
        pdfUrl:(this.state.screenName == 'eventResources') ? this.props.navigation.state.params.pdfData.Url : this.props.navigation.state.params.pdfData.AttachmentURL
      })
  }

  render() {
    return (
        <View style={{ flex: 1, backgroundColor: "white" }} >
            {
                (this.state.pdfUrl != null && this.state.pdfUrl != undefined && this.state.pdfUrl != "") ?
                    <PDFView
                        fadeInDuration={250.0}
                        style={{ flex: 1 }}
                        resource={resources['url']}
                        resourceType={'url'}
                        onError={() => console.log('Cannot render PDF')}
                    /> :
                    <View style={[styles.data_notAvlbViewContainer]}>
                        <Text style={styles.data_notAvlb}>
                            {globals.ERROR_MESSAGE.EVENT_ERR.MYEVENT_PDFURL_ERROR}
                        </Text>
                    </View>
            }
        </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PdfView);
