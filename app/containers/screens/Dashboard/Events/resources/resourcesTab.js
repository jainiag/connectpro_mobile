/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */

import React from 'react';
import { Text, View } from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import styles from './style';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import { TabView, TabBar } from 'react-native-tab-view';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import Albums from './albums';
import Documents from './documents';
import Videos from './videos';

const TAG = '==:== Event Resources : ';
let _this;

class ResourcesTab extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    const attendeeCount = (params !== undefined) ? params.attendeeCount : ''
    return{
      headerLeft:globals.ConnectProbackButton(navigation, 'Resources'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }
  };

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      isFreeStatus:this.props.navigation.state.params.isFreeStatus,
      loading: true,
      isSearh: false,
      selectedTab: 1,
      index: 0,
      videosData:this.props.navigation.state.params.videosData,
      documentData:this.props.navigation.state.params.documentsData,
      albumsData:this.props.navigation.state.params.albumsData,
      allEventDetails: this.props.navigation.state.params.allEventDetails,
      routes: [
        { key: 'Albums', title: 'Albums' },
        { key: 'Documents', title: 'Documents' },
        { key: 'Videos', title: 'Videos' },
      ],
      eventID : this.props.navigation.state.params.eventID
    };
  }

  render() {
    return (
      <View style={styles.mainParentStyle}>
         <TabView
          style={[styles.tabViewContainer,{marginTop: 10}]}
          renderTabBar={props => (
            <TabBar
              {...props}
              style={styles.tabbarStyle}
              tabStyle={styles.tabStyle}
              indicatorStyle={styles.indicatorStyle}
              pressColor={colors.white}
              renderPager={this.renderPager}
              renderLabel={({ route }) => (
                <Text
                  style={[
                    styles.labelStyle,
                    {
                      color:
                        this.state.routes[this.state.index].key == route.key
                          ? colors.warmBlue
                          : colors.Gray,
                    },
                  ]}
                >
                  {route.title}
                </Text>
              )}
            />
          )}
          navigationState={this.state}
          renderScene={({ route }) => {
            switch (route.key) {
              case 'Albums':
                return <Albums navigationProps={this.props} isFreeStatus={this.state.isFreeStatus} eventID={this.state.eventID} albumsData={this.state.albumsData} allEventDetails={this.state.allEventDetails}/>;
              case 'Documents':
                return <Documents navigationProps={this.props} isFreeStatus={this.state.isFreeStatus}  eventID={this.state.eventID} documentData={this.state.documentData} allEventDetails={this.state.allEventDetails}/>;
              case 'Videos':
                return <Videos navigationProps={this.props} isFreeStatus={this.state.isFreeStatus}  eventID={this.state.eventID} videosData={this.state.videosData} allEventDetails={this.state.allEventDetails}/>;
              default:
                return null;
            }
          }}
          onIndexChange={index => this.setState({ index })}
          initialLayout={styles.initialLayoutStyle}
        />
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourcesTab);
