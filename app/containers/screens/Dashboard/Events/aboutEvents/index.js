/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import Share from 'react-native-share';
import HTML from 'react-native-render-html';
import ImageLoad from 'react-native-image-placeholder';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import * as images from '../../../../../assets/images/map';
import globalStyles from '../../../../../assets/styles/globleStyles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';

const TAG = '==:== About Events : ';
let _this;
class AboutEvent extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'About Event'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    console.log("Props", this.props);
    
    this.state = {
      isFreeStatus:this.props.navigation.state.params.isFreeStatus,
      aboutEventsData: this.props.navigation.state.params.aboutEventsData,
      eventID : this.props.navigation.state.params.eventID,
      shareUrl:this.props.navigation.state.params.shareUrl,
      allEventDetails: this.props.navigation.state.params.allEventDetails
    }
  }


  componentDidMount() {
    
  }

  /**
   * Event share method
   */
  _shareEvent = () => {
    const { aboutEventsData } = this.state;
    let isEventName = globals.checkObject(aboutEventsData, 'title');

    const shareOptions = {
      message: "Event: " + ((isEventName) ? aboutEventsData.title : " - ") + "\n" +
        "Link to open: " + this.state.shareUrl
    };

    Share.open(shareOptions)
      .catch(err => console.log("share error::",err));
  };

  render() {
    const { aboutEventsData } = this.state;
    const isTitle = globals.checkObject(aboutEventsData, 'title');
    const isText = globals.checkObject(aboutEventsData, 'text');
    const isImage = globals.checkImageObject(aboutEventsData, 'logo');
    return (
      <>
        {
          (aboutEventsData == '') ?
            <View style={globalStyles.nodataStyle}>
              <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.NOTHING_FOR_EVENT}</Text>
            </View>
            :
            <View style={styles.mainParentStyle}>
              <View style={{ flex: 1 }}>
                <ImageLoad
                  style={styles.adImageStyle}
                  source={{ uri: isImage ? aboutEventsData.logo : globals.Event_img  }}
                  isShowActivity={false}
                  // resizeMode="contain"
                  placeholderSource={images.EventDashboard.imagePlaceHolder}
                  placeholderStyle={styles.adImageStyle}
                />
              </View>
              <View style={styles.shareViewStyle}>
                <TouchableOpacity onPress={() => this._shareEvent()}>
                  <Image
                    source={images.EventDashboard.shareBtn}
                    style={styles.shareIconStyle}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>

              <View style={{ flex: 2 }}>
                <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                  <View style={{ marginHorizontal: globals.screenWidth * 0.08 }}>
                    <Text style={styles.login_ConditionTextStyle}>{isTitle ? aboutEventsData.title : '-'}</Text>
                    <HTML html={isText ? aboutEventsData.text : '-'}baseFontStyle={{ fontSize: globals.font_15 }} />
                  </View>
                </ScrollView>
              </View>
              <TouchableOpacity
                style={styles.regBtnStyle} onPress={() => this.props.navigation.navigate('EVENT_REGISTRATION', {dataEventsList:this.state.isFreeStatus, eventID: this.state.eventID, allEventDetails: this.state.allEventDetails })}
              >
                <Text style={styles.regBtnTextStyle}>{globals.BTNTEXT.REGISTERSCREEN.REG_BTNTEXT}</Text>
              </TouchableOpacity>
            </View>
        }
      </>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AboutEvent);
