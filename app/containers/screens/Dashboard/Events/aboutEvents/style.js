/* eslint-disable eqeqeq */
/* eslint-disable no-nested-ternary */
import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
   
  },
  nodataStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  nodataTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
  },
  tvTitleStyle: {
    fontSize: globals.font_20,
    color: 'blue',
  },
  login_ConditionPrivacyTextStyle: {
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'center',
    color: 'white',
    marginBottom: 10,
  },
  login_ConditionTextStyle: {
    color: colors.bgColor,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_15,
    letterSpacing: 0,
  },
  login_byCreatingTextStyle: {
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'center',
    fontSize: globals.font_13,
    color: colors.white,
    marginHorizontal: 32,
    marginBottom: 10,
  },
  mainTextStyle: {
    marginHorizontal: 20,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_16,
    color: colors.textBlackColor,
  },
  regBtnTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
    color: colors.white,
  },
  regBtnStyle: {
    width: '100%',
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 65 : 55,
    backgroundColor: colors.warmBlue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  shareViewStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 60 : 40,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 60 : 40,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 60 : 40 / 2,
    backgroundColor: colors.warmBlue,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 10,
    top: globals.screenHeight * 0.16,
    flex: 1,
  },
  shareIconStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 60 : 35,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 60 : 35,
  },
  adImageStyle: {
    width: globals.screenWidth,
    height: globals.screenHeight * 0.185,
  },
});
