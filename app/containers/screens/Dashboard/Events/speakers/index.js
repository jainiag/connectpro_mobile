/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-destructuring */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable react/no-unused-state */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */

import React from 'react';
import { Text, View, TouchableOpacity,  Image, ScrollView, Linking, Alert } from 'react-native';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import Accordion from '../../../../../libs/react-native-collapsible/Accordion';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as images from '../../../../../assets/images/map';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';


const TAG = '==:== Speakers : ';
let _this;


class Speakers extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    const speakersCount = (params !== undefined) ? params.speakersCount : ''
    return {
      headerLeft: (speakersCount != undefined) ? globals.ConnectProbackButton(navigation, 'Speakers (' + speakersCount + ')') : globals.ConnectProbackButton(navigation, 'Speakers ()'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }
  };


  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      isFreeStatus:this.props.navigation.state.params.isFreeStatus,
      speakersData: this.props.navigation.state.params.speakersData,
      serverErr: false,
      noAttendeData: false,
      loading: false,
      activeSections: [],
      eventID: this.props.navigation.state.params.eventID,
      allEventDetails: this.props.navigation.state.params.allEventDetails

    };
  }

  componentDidMount() {
    const { activeSections, speakersData } = this.state;
    _this.props.navigation.setParams({
      speakersCount: speakersData.length
    });
    for (let index = 0; index < speakersData.length; index++) {
      speakersData[index].key = index;
      speakersData[index].disabled = false;
    }
    for (let i = 0; i < activeSections.length; i++) {
      activeSections[i].key = i;
      activeSections[i].disabled = false;
    }
  }

  headerView = sections => {
    const { activeSections } = this.state;
    return (
      <View style={styles.headerViewContainer}>
        <View>
          {sections.disabled ? (
            <Image
              source={images.EventDashboard.upArrow}
              style={styles.arrowUpAndDownStyle}
              resizeMode="contain"
            />
          ) : (
              <Image
                source={images.EventDashboard.downArrow}
                style={styles.arrowUpAndDownStyle}
                resizeMode="contain"
              />
            )}
        </View>
      </View>
    );
  };

  clickOnProfile(linkedInUrl) {
    if (linkedInUrl != null && linkedInUrl != "" && linkedInUrl != undefined) {
      Linking.canOpenURL(linkedInUrl).then(supported => {
        if (supported) {
          console.log("supported" + linkedInUrl);
          Linking.openURL(linkedInUrl);
        } else {
          console.log("not supported " + linkedInUrl);
          Linking.openURL(linkedInUrl)
        }
      });
    } else {
      Alert.alert(globals.appName, "Please try again later")
    }
  }

  contentView = sections => {   
    const isBio = globals.checkObject(sections, 'Bio');
    const isLinkedInUrl = globals.checkObject(sections, 'LinkedInUrl')


    const { speakersData } = this.state;
    return (
      <View style={styles.contentViewContainer}>
        <Text style={styles.descriptionTextStyle}>{(isBio) ? sections.Bio : '-'}</Text>
        <TouchableOpacity style={styles.ViewProfileBtnContainer} onPress={() => this.clickOnProfile((isLinkedInUrl) ? sections.LinkedInUrl : 'https://www.linkedin.com')}>
          <Text style={styles.viewProfileTextStyle}>VIEW LINKEDIN PROFILE</Text>
        </TouchableOpacity>
      </View>
    );
  };

  _updateSections = activeSections => {
    const { speakersData } = this.state;

    for (let i = 0; i < speakersData.length; i++) {
      speakersData[i].disabled = false;
      for (let index = 0; index < activeSections.length; index++) {
        const activeSectionIndex = activeSections[index];

        speakersData[activeSectionIndex].disabled = true;
      }
    }
    this.setState({ activeSections, speakersData });
  };

  render() {
    const { speakersData, activeSections, loading, noAttendeData } = this.state;
    return (
      <>
        {speakersData == '' ? (
          <View style={globalStyles.nodataStyle}>
            <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.SPEAKERS_DATA_NOT_AVLB}</Text>
          </View>

        ) : (

            <View style={styles.mainContainer}>
              <ScrollView bounces={false} style={styles.scrollViewContainer} showsVerticalScrollIndicator={false}>
                <Accordion
                  sections={speakersData}
                  activeSections={activeSections}
                  // renderSectionTitle={this._renderSectionTitle}
                  renderHeader={this.headerView}
                  renderContent={this.contentView}
                  onChange={this._updateSections}
                  expandMultiple
                />
              </ScrollView>
              <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this.props.navigation.navigate('EVENT_REGISTRATION', {dataEventsList:this.state.isFreeStatus, eventID: this.state.eventID, allEventDetails: this.state.allEventDetails })}>
                <Text style={styles.seachTextStyle}>REGISTER</Text>
              </TouchableOpacity>
            </View>

          )}
      </>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Speakers);
