import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor:colors.white,
    marginTop: globals.screenHeight * 0.03,
  },
  nodataStyle:{
    alignItems:'center',
    justifyContent:'center',
    flex:1
  },
  nodataTextStyle:{
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28: globals.font_14
  },
  scrollViewContainer: {
    flex:1,
    
  },
  headerViewContainer: {
    alignItems: 'flex-end',
    flex:1,
    marginVertical: globals.screenWidth * 0.065,
   paddingHorizontal:globals.screenWidth * 0.045,
    justifyContent:'center',
  //  backgroundColor:'red'
    // paddingTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.03,
    // marginLeft: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.40 : globals.screenWidth * 0.27,
  },
  contentViewContainer: {
    padding: globals.screenWidth * 0.026,
    marginHorizontal: globals.screenWidth * 0.053,
    // backgroundColor:'yellow'
  },
  descriptionTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_13,
    color: colors.lightBlack,
  },
  ViewProfileBtnContainer: {
    backgroundColor: colors.darkSkyBlue,
    marginTop: globals.screenHeight * 0.018,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.06 : globals.screenHeight * 0.06,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.35 : globals.screenWidth * 0.45,
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center'
  },
  viewProfileTextStyle: {
    color: 'white',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
    textAlign: 'center',
    fontWeight: "500"
  },
  searchButtonContainer: {
    flex: 0.1, backgroundColor: colors.warmBlue, bottom: 0,
    alignItems: 'center', justifyContent: 'center'
  },
  seachTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_20,
    color: colors.white,
  },
  arrowUpAndDownStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.045 : globals.screenWidth * 0.05,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.045 : globals.screenWidth * 0.05,
  },
});
