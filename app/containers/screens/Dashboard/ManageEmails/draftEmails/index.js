import React from 'react';
import { Text, View, FlatList, TouchableOpacity, Image, Alert, TextInput } from 'react-native';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { API } from '../../../../../utils/api';
import styles from './style';
import ComposeEmails from '../composeEmails/index';
import ManageEmails from '../index';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import * as images from '../../../../../assets/images/map';
import Nointernet from '../../../../../components/NoInternet/index'
import ServerError from '../../../../../components/ServerError/index'

const TAG = '==:== DraftEmails : ';
let _this;
const iPad = DeviceInfo.getModel();
let draftData;
class DraftEmails extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Draft Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
      });

    constructor(props){
        super(props);
        _this = this;
        this.state = {
            loading: false,
            draftEmail: [],
            searchTxt: '',
            serverErr: false,
            isInternetFlag: true,
            responseComes: false,
        }
    }

   
    static refreshDraftMailList(){
        _this.makeGetDraftmailInformationApiCall();
    }

    static clearSearchText(){
        _this.setState({searchTxt: ''})
    }

/**
* API call of getDraftmailsInformationResponseData
* 
*/
    makeGetDraftmailInformationApiCall(){
        this.props.showLoader()
        this.setState({loading: true, isInternetFlag: true})
        if (globals.isInternetConnected === true) {
            API.getDraftmailsInformation(this.getDraftmailsInformationResponseData, true, globals.userID);
        } else {
            this.props.hideLoader()
            this.setState({loading: false, isInternetFlag: false})
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


/**
* callback response of getDraftmailsInformationResponseData
* 
*/
getDraftmailsInformationResponseData = {
        success: response => {
            console.log(
                TAG,
                'getDraftmailsInformationResponseData-> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                let draft_email = response.Data
                this.setState({draftEmail: draft_email},() => {
                    draftData = this.state.draftEmail
                })
            } else {
                Alert.alert(globals.appName, response.Message)

            }
            this.setState({loading: false})
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader()
            this.setState({ loading: false, serverErr: true })
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK' }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'getDraftmailsInformationResponseData-> ERROR : ',
                JSON.stringify(err.message)
            );
        },
        complete: () => {
            this.props.hideLoader()
            this.setState({ loading: false })
            console.log("getDraftmailsInformationResponseData-> complete");
        },
    };

    goToComposeEmails(batch_ID){
        this.setState({searchTxt: ''})
        ComposeEmails.GetDraftEmailDataToComposeApiCall(batch_ID)
        ComposeEmails.GetdraftattachmentsApiCall(batch_ID)
        ManageEmails.goToPage()
    }

/**
* render method for draftEmail list
* 
*/
    renderDraftEmail(item, index){
        const isEmailSubject = globals.checkObject(item, "EmailSubject");
        const EmailSubject = (isEmailSubject) ? item.EmailSubject : "-";
        const isBatchID = globals.checkObject(item, "BatchID")
        const batch_ID = (isBatchID) ? item.BatchID : "null"
        return(
            <TouchableOpacity onPress={() => this.goToComposeEmails(batch_ID)}>
                <View style={styles.draft_emailSubjectContainerStyle}>
                    <Text style={styles.draft_emailSubjectTextStyle}>{EmailSubject}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    /**
    * SearchFilterFunction for locally search draft emails
    */
    SearchFilterFunction(text) {
        if (text != "") {
            const newData = draftData.filter(function (item) {
                const itemData = item.EmailSubject ? item.EmailSubject.toUpperCase() : ''.toUpperCase();
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1;      
            });
            this.setState({
                draftEmail: newData,
                searchTxt: text,
            });
        }else{
            this.setState({
                draftEmail: draftData,
                searchTxt: text,
            });
        } 
      }

      _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.makeGetDraftmailInformationApiCall();
        });

    }
    
    render() {        
        const { draftEmail, loading, searchTxt, serverErr, isInternetFlag } = this.state;
        return (
            <View style={styles.draft_mainParentStyle}>
                <View style={styles.api_textfiledView}>
                    <TextInput
                        style={styles.addPayerTextStyle}
                        placeholder={"Search here..."}
                        placeholderTextColor={colors.lightGray}
                        onChangeText={text => this.SearchFilterFunction(text)}
                        returnKeyType="done"
                        blurOnSubmit={true}
                        value={searchTxt}
                        autoCapitalize="none"
                    />
                    <Image
                        source={images.headerIcon.searchIcon}
                        resizeMode={"contain"}
                        style={{
                            height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                            width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                            tintColor: colors.warmBlue,
                        }}
                    />
                </View>
                <View style={styles.draft_viewContainerStyle}>
                    {
                        (!isInternetFlag) ?
                            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                            (serverErr === false) ?
                                (loading == true) ?
                                    null
                                    :
                                    (draftEmail.length > 0 && draftEmail !== null) ?
                                        <FlatList
                                            style={styles.draft_flatListStyle}
                                            showsVerticalScrollIndicator={false}
                                            data={draftEmail}
                                            renderItem={({ item, index }) => this.renderDraftEmail(item, index)}
                                            extraData={this.state}
                                            bounces={false}
                                            keyExtractor={(index, item) => item.toString()}
                                        />
                                        :
                                        (draftEmail.length == 0 && searchTxt !== '') ?
                                            <View style={globalStyles.nodataTextStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.MANAGE_EMAILS.DRAFT_EMAIL_NOT_AVLBL}</Text>
                                            </View>
                                            :
                                            <View style={globalStyles.nodataTextStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.MANAGE_EMAILS.DRAFT_EMAIL_NOT_AVLBL_DATA}</Text>
                                            </View>
                                :
                                <ServerError loading={loading} onPress={() => this._tryAgain()} />
                    }
                </View>
            </View>
        );
    }
}


// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DraftEmails);