import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
    draft_mainParentStyle: {
        flex: 1,
    },
    draft_viewContainerStyle: {
        justifyContent: "center", 
        alignItems: "center", 
        flex: 1,
    },
    draft_flatListStyle: {
        flex: 1, 
        marginTop: globals.screenHeight * 0.025,
    },
    draft_emailSubjectContainerStyle: {
        // height: globals.screenHeight * 0.07,
        flex:1,
        width: globals.screenWidth * 0.8,
        borderWidth: 1,
        borderColor: colors.proUnderline,
        justifyContent: 'center',
        marginBottom: 10,
        borderRadius: 4,
    },
    draft_emailSubjectTextStyle: {
        fontSize: globals.font_15,
        color: colors.darkSkyBlue,
        paddingHorizontal:globals.screenWidth * 0.032,
        paddingVertical:globals.screenWidth * 0.03,
    },
    draft_emailNotAvlb: {
        fontSize: globals.font_15,
        color: colors.black
    },
    api_textfiledView: {
        borderWidth: 0.5, borderColor: colors.gray, borderRadius: 5,
        marginHorizontal: globals.screenWidth * 0.066,
        marginTop: globals.screenHeight * 0.018,
        paddingHorizontal: globals.screenWidth * 0.04,
        width: globals.screenWidth * 0.8,
        justifyContent: 'space-between',
        alignSelf: 'center',
        height: globals.screenHeight * 0.055,
        flexDirection: 'row',
        alignItems: 'center',
    },
    addPayerTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
        width: globals.screenWidth * 0.63
    },
});