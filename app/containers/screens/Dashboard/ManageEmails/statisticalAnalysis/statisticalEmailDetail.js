import React from 'react';
import { Text, View, Alert, Image, FlatList } from 'react-native';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import DeviceInfo from 'react-native-device-info';
import moment from 'moment'
import HTML from 'react-native-render-html';
import styles from './style';

const TAG = '==:== Statistical EmailDetail Screen : ';
let _this;
const iPad = DeviceInfo.getModel();

class StatisticalEmailDetails extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Manage Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
      });

      constructor(props) {
        super(props)
        _this=this      
        this.state = {
            emailStatus: this.props.navigation.state.params.title,
            emailData: this.props.navigation.state.params.item,
        };
      };
      
      render(){
          const {  emailData, emailStatus } = this.state;
          const isSubject = globals.checkObject(emailData,"EmailSubject");
          const istoEmail = globals.checkObject(emailData, "Email");
          const isEventTimeStampStr = globals.checkObject(emailData, "EventTimeStampStr");
          const isSentAtStr = globals.checkObject(emailData, "SentAtStr")
          const isEmailBody = globals.checkObject(emailData,"EmailBody");
          const isFromAddress = globals.checkObject(emailData, "FromAddress")
          const EmailSubject = (isSubject) ?  emailData.EmailSubject : '-';
          const EmailBody = (isEmailBody) ?  emailData.EmailBody : '-';
          const EventTimeStampStr = (isEventTimeStampStr) ?  emailData.EventTimeStampStr : (emailStatus == "Emails Sent") ? (isSentAtStr) ? emailData.SentAtStr : '' : '' ;
          const Email = (istoEmail) ? emailData.Email : '-';
          const FromAddress = (isFromAddress) ? emailData.FromAddress : '-';
          let time = (isEventTimeStampStr) ? moment(new Date(EventTimeStampStr)).format(" MM/DD/YYYY HH:mm:ss A") : "-"

          return(
              <View style={styles.mainParentStyle}>
                  <View style={styles.emallist_backgroundStyle}>
                      <Text style={styles.detail_mainTextStyle}>{emailStatus}</Text>
                  </View>
                  <View style={styles.detail_MainViewContainer}></View>
                  <View style={styles.detail_childViewContainer}>
                      <View style={styles.detail_ViewStyle}>
                          <View style={styles.detail_subjectViewContainer}>
                              <Text numberOfLines={1} style={styles.detail_subjectTextstyle}>
                                  {EmailSubject}
                              </Text>
                              <View style={styles.detail_emailViewStyle}>
                                  <Text style={styles.detail_toTextStyle}>To:
                                  <Text numberOfLines={1} style={styles.detail_emailTextStyle}>
                                          {` ${Email}`}
                                      </Text>
                                  </Text>
                              </View>
                              <Text numberOfLines={1} style={styles.detail_timeTextStyle}>
                                  {EventTimeStampStr}
                              </Text>
                              {/* <HTML html={EmailBody} baseFontStyle={styles.detail_toTextStyle} /> */}
                          </View>
                      </View>
                  </View>
              </View> 
          )
      }
}

export default StatisticalEmailDetails;