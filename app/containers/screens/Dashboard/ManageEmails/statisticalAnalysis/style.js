import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
    statics_mainParentStyle: {
        flex: 1,
    },
    loaderbottomview: {
        bottom: (Platform.OS == 'android') ?
          globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
      },
    statics_crossIconStyle: {
        marginTop: (globals.iPhoneX) ? globals.screenHeight * 0.04 : globals.screenHeight * 0.03, 
        marginRight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.027 : globals.screenWidth * 0.06,
    },
    statics_datePickerViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 15 : 20,
    },
    statics_pickerStyle: {
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.935 : globals.screenWidth * 0.87,
    },
    statics_placeHolderTextStyle: {
        color: colors.lightGray,
        fontSize: globals.font_14,
        textAlign: 'left',
        marginRight: iPad.indexOf('iPad') != -1 ? globals.screenWidth * 0.55 : DeviceInfo.isTablet() ? globals.screenWidth * 0.62 : globals.screenWidth * 0.34,
    },
    statics_dateIconStyle: {
        position: 'absolute',
        left: 0,
        marginLeft: 0,
    },
    statics_startDateInputStyle: {
        borderRadius: 5,
        borderColor: colors.proUnderline,
        marginTop: 10,
        marginBottom: 10,
        height: iPad.indexOf('iPad') != -1 ? globals.screenHeight * 0.056 : DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : (Platform.OS == 'android') ? globals.screenHeight * 0.065 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenHeight * 0.07 : globals.screenHeight * 0.06,
    },
    statics_dropdownContainer: {
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: globals.screenWidth * 0.06,
        height: globals.screenHeight * 0.065, 
        paddingBottom: globals.screenHeight * 0.02, 
        justifyContent: 'center',
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
        
    },
    statics_attchIconViewContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.listSelectColor,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.1,
        width:  iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.35 : globals.screenWidth * 0.4,
        borderRadius: 3,
    },
    statics_textStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
    },
    statics_DateBtnStyle: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: globals.screenWidth * 0.06,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.025,
        height: (Platform.OS == "android") ? globals.screenHeight * 0.065 : null,
        borderRadius: 5,
    },
    statics_DateViewStyle: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: 25,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
    },
    statics_dateSelectStyle: {
        flex: 1,
        marginLeft: globals.screenWidth * 0.04,
        color: colors.lightGray,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
        marginTop: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(13),
        marginBottom: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(13),
    },
    statics_childlistViewStyle:{
        borderWidth: 1,
        borderColor: colors.proUnderline,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: globals.screenHeight * 0.45,
        width: globals.screenWidth * 0.85,
        marginTop: globals.screenHeight * 0.05,
        borderRadius: 5,
    },
    statics_flatListStyle: {
        flex: 1, 
        marginTop: globals.screenHeight * 0.015,
    },
    statics_submitStyle: {
        justifyContent: 'center', 
        alignItems: 'center', 
        marginTop: globals.screenHeight * 0.05, 
        alignSelf: 'center',
    },
    statics_emailReportItemViewStyle: {
        borderWidth: 1,
        borderColor: colors.proUnderline,
        justifyContent: 'center',
        alignSelf: 'center',
        height: globals.screenHeight * 0.065,
        width: globals.screenWidth * 0.66,
        borderRadius: 3,
        marginVertical: globals.screenHeight * 0.01,
    },
    statics_titleAndCountStyleViewStyle: {
        flexDirection: "row", 
        justifyContent: "space-between", 
        alignItems: "center",
    },
    statics_titleTextStyle: {
        paddingLeft: 12, 
        fontSize: globals.font_14, 
        // color: colors.black,
    },
    statics_CountViewStyle: {
        // borderWidth: 1,
        // borderBottomWidth,
        borderLeftWidth: 1,
        borderLeftColor: colors.proUnderline,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: globals.screenHeight * 0.065,
        width: globals.screenHeight * 0.065,
        borderRadius: 3,
    },
    statics_countTextStyle: {
        fontSize: globals.font_14, 
        // color: colors.black
    },

    // ============: Email List Screen :==========

    emallist_nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    emallist_nodataTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
    },
    emallist_backgroundStyle: {
        width: globals.screenWidth,
        height: globals.screenHeight * 0.20,
        backgroundColor: colors.warmBlue,
        alignItems: 'center'
    },
    emallist_mainTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_15, 
        color: colors.white, 
        alignSelf: 'flex-start', 
        marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.03, 
        marginLeft: globals.screenWidth * 0.072, 
        marginBottom: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.06 : globals.screenHeight * 0.05,
    },
    emallist_MainViewContainer: {
        alignItems: 'center',
        marginTop: globals.screenHeight * (-0.09),
        zIndex: 999,
        // backgroundColor:'red',
        top: globals.screenHeight * 0.08
    },
    emallist_ChildViewContainer: {
        backgroundColor: colors.white,
        width: globals.screenWidth * 0.85,
        //  flex: 1,
        // height:globals.screenWidth * 0.85,
        marginLeft: globals.screenWidth * 0.074,
        borderWidth: 0.7,
        borderColor: colors.gray,
        borderRadius: 5,
        marginBottom: globals.screenHeight * 0.02,
        paddingBottom: globals.screenHeight * 0.02,
    },
    emallist_userinfoStyle: {
        flexDirection: 'row',
        marginHorizontal: globals.screenWidth * 0.03,
        // backgroundColor: "red",
        paddingHorizontal: globals.screenWidth * 0.02,
        paddingVertical: globals.screenWidth * 0.025,
    },
    emaillist_textViewContainer: {
        alignContent: 'center',
        marginVertical: globals.screenWidth * 0.025,
        width: "100%"
    },
    emaillist_toEmailTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16, color: colors.darkSkyBlue
    },
    emaillist_commanTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14, color: colors.Gray,
    },

    // ======: email detail screen : ======

    detail_MainViewContainer: {
        alignItems: 'center',
        marginTop: globals.screenHeight * (-0.09),
        zIndex: 999,
        // backgroundColor:'red',
        top: globals.screenHeight * 0.08
    },
    detail_childViewContainer: {
        backgroundColor: colors.white,
        width: globals.screenWidth * 0.85,
        //  flex: 1,
        // height:globals.screenWidth * 0.85,
        marginLeft: globals.screenWidth * 0.074,
        borderWidth: 0.7,
        borderColor: colors.gray,
        borderRadius: 5,
        marginBottom: globals.screenHeight * 0.02,
        paddingBottom: globals.screenHeight * 0.02,
    },
    detail_ViewStyle: {
        flexDirection: 'row',
        paddingHorizontal: globals.screenWidth * 0.02,
       paddingVertical: globals.screenWidth * 0.025,
    },
    detail_subjectViewContainer: {
        marginHorizontal: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        flex: 1,
    },
    detail_subjectTextstyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_15,
    },
    detail_emailViewStyle: {
        marginTop: 3, marginBottom: 5
    },
    detail_toTextStyle: {
        color: colors.Gray,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    },
    detail_emailTextStyle: {
        color: colors.darkSkyBlue,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    },
    detail_timeTextStyle: {
        color: colors.Gray,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
        marginBottom: 5
    },
    detail_mainTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_15, 
        color: colors.white, 
        alignSelf: 'flex-start', 
        marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.03, 
        marginLeft: globals.screenWidth * 0.072, 
        marginBottom: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.06 : globals.screenHeight * 0.05,
    },
    detail_OpacityStyle: {
        justifyContent: 'flex-start',
        alignItems: "flex-start",
    },
    detail_attchIconViewContainer: {
        alignItems: 'center',
        justifyContent: 'space-evenly',
        backgroundColor: colors.listSelectColor,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.1,
        width:  iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? "100%" : '100%',
        borderRadius: 3,
        flexDirection: 'row' 
    },
    detail_attchIconStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.screenWidth * 0.05, 
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.screenWidth * 0.05,
    },
    detail_fileNametextStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
        width: globals.screenWidth * 0.6,
    },
    hobbytextStyle: {
        //marginVertical:globals.screenWidth * 0.02,
        marginHorizontal: globals.screenWidth * 0.05,
        paddingHorizontal: globals.screenWidth * 0.01,
        paddingVertical: globals.screenWidth * 0.025,
        marginBottom: globals.screenWidth * 0.028,
    },
});