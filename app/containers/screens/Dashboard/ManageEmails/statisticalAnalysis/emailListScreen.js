import React from 'react';
import { Text, View, Alert, FlatList, TouchableOpacity, ActivityIndicator} from 'react-native';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import DeviceInfo from 'react-native-device-info';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import moment from 'moment'
import { API } from '../../../../../utils/api';
import styles from './style';
import AntDesign from 'react-native-vector-icons/AntDesign'
import Nointernet from '../../../../../components/NoInternet/index'
import ServerError from '../../../../../components/ServerError/index'

const TAG = '==:== EmailListScreen : ';
let _this;
const iPad = DeviceInfo.getModel();
class EmailListScreen extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Manage Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
      });

    constructor(props){
        super(props);
        _this = this;
        this.state = {
            startDate: this.props.navigation.state.params.startDate,
            endDate: this.props.navigation.state.params.endDate,
            emailStatus: this.props.navigation.state.params.item.key,
            emailListData: [],
            pageStart: 0,
            loadingPagination: false,
            serverErr: false,
            isInternetFlag: true,
            responseComes: false,
        }
    }

    componentDidMount(){
        this.props.showLoader();
        this.setState({loading: true, isInternetFlag: true})
        this._getMailEvent();
    }


    /**
    * Api call _getMailEvent on report item click
    *
    */
   _getMailEvent(){
    const {startDate, endDate, emailStatus, pageStart} = this.state;
    const drawValue = (emailStatus == "Emails Sent") ? 2 :
                      (emailStatus == "Emails Delivered") ? 3 :
                      (emailStatus == "Emails Opened") ? 4 : 
                      (emailStatus == "Emails Bounced") ? 5 : null

    const value = (emailStatus == "Emails Sent") ? "processed" :
                  (emailStatus == "Emails Delivered") ? "delivered" :
                  (emailStatus == "Emails Opened") ? "open" : 
                  (emailStatus == "Emails Bounced") ? "bounce" : null

    const data = {
        "StartDate": startDate,
        "EndDate": endDate,
        "UserId": globals.userID,
        "dtParams": {
            "draw": drawValue,
            "columns": [
                { "data": "RowIndex", "name": "", "searchable": false, "orderable": false, "search": { "value": "", "regex": false } },
                { "data": "FromAddress", "name": "", "searchable": true, "orderable": true, "search": { "value": "", "regex": false } },
                { "data": "Email", "name": "", "searchable": true, "orderable": true, "search": { "value": "", "regex": false } },
                { "data": "EmailSubject", "name": "", "searchable": true, "orderable": true, "search": { "value": "", "regex": false } },
                { "data": "SentAt", "name": "", "searchable": true, "orderable": true, "search": { "value": "", "regex": false } },
                { "data": "EventTimeStampStr", "name": "", "searchable": false, "orderable": false, "search": { "value": "", "regex": false } },
                { "data": "IpAddress", "name": "", "searchable": false, "orderable": false, "search": { "value": "", "regex": false } },
                { "data": "UserAgent", "name": "", "searchable": false, "orderable": false, "search": { "value": "", "regex": false } },
                { "data": "Url", "name": "", "searchable": false, "orderable": false, "search": { "value": "", "regex": false } },
                { "data": "Event", "name": "", "searchable": true, "orderable": true, "search": { "value": value, "regex": false } },
                { "data": "CreatedBy", "name": "", "searchable": true, "orderable": true, "search": { "value": "", "regex": false } }
            ],
            "order": [
                { "column": 4, "dir": "desc" }
            ],
            "start": pageStart,
            "length": 10,
            "search": { "value": "", "regex": false }
        }
    }
 if (globals.isInternetConnected === true) {
     this.setState({loadingPagination:true})
     API.getMailEvents(this.getMailEventsResponseData, data, true);
 } else {
     this.props.hideLoader();
     this.setState({loading: false, isInternetFlag: false})
     Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
 }
}

/**
 * callback response of getMailEventsResponseData
 * 
 */
getMailEventsResponseData = {
 success: response => {
     console.log(
         TAG,
         'getMailEventsResponseData-> success : ',
         JSON.stringify(response)
     );
     if (response.StatusCode == 200) {
         console.log("getMailEventsResponseData Data ::", response.Data);
         let list_Data = response.Data.data;
         if (this.state.pageStart == 0) {
            this.setState({
                loading: false, loadingPagination: false,
                emailListData: response.Data.data,
            })
        } else {
            this.setState({
                loading: false, loadingPagination: false,
                emailListData: [...this.state.emailListData, ...response.Data.data],
            })
        }
        //  this.setState({emailListData: list_Data})
     } else {
         Alert.alert(globals.appName, response.Message)
     }
     this.setState({loading: false})
     this.props.hideLoader();
 },
 error: err => {
     this.props.hideLoader();
     this.setState({loading: false, serverErr: true})
     if (err.StatusCode == 401 || err.StatusCode == 403) {
         Alert.alert(
             globals.appName,
             'Your session is expired, Please login again',
             [{ text: 'OK' }],
             { cancelable: false }
         );
     } else {
         Alert.alert(
             globals.appName,
             err.Message
         );
     }
     console.log(
         TAG,
         'getMailEventsResponseData-> ERROR : ',
         JSON.stringify(err.message)
     );
 },
 complete: () => {
     this.setState({loading: false})
     this.props.hideLoader();
     console.log("getMailEventsResponseData-> complete");
 },
};



    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageStart: this.state.pageStart + 10,
            }, () => {
                this._getMailEvent();
            })
            this.onEndReachedCalledDuringMomentum = true;
        }
    };



    renderItemUI(item, index) {
        const isEmail = globals.checkObject(item, "Email")
        const isSubject = globals.checkObject(item, "EmailSubject")
        const isDate = globals.checkObject(item, "SentAtStr")
        const Email = (isEmail) ? item.Email : '';
        const EmailSubject = (isSubject) ? item.EmailSubject : '';
        const date = (isDate) ? item.SentAtStr : '';
        const time = moment(new Date(date)).format("h:mm A")
        
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate("StatisticalEmailDetail",{item: item, title: this.state.emailStatus})}>
                <View style={styles.emallist_ChildViewContainer}>
                    <View style={styles.emallist_userinfoStyle}>
                        <View style={styles.emaillist_textViewContainer}>
                            <Text numberOfLines={1} style={styles.emaillist_toEmailTextStyle}>
                                {Email}
                            </Text>
                            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: globals.screenHeight * 0.009}}>
                                <Text numberOfLines={1} style={[styles.emaillist_commanTextStyle,{width: globals.screenWidth * 0.55}]}>
                                    {`(Subject : ${EmailSubject})`}
                                </Text>
                                <Text numberOfLines={1} style={[styles.emaillist_commanTextStyle,{marginLeft: globals.screenWidth * 0.06}]}>{time}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }


    renderFooter = () => {
        if (this.state.loadingPagination == true) {
            return (
                        <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} /> 
            )

        } else {
            return (
                <View />
            )
        }
    };

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this._getMailEvent();
        });

    }
    
    
    render() {        
        const { emailListData, loading, emailStatus, isInternetFlag, serverErr} = this.state;
        return (
            <View style={styles.mainParentStyle}>
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (loading == true) ? null
                                :
                                (emailListData.length == 0) ?
                                    <View style={globalStyles.nodataStyle}>
                                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.MANAGE_EMAILS.SENT_EMAIL_NOT_AVLBL}</Text>
                                    </View> :
                                    <View style={[styles.mainParentStyle, { paddingBottom: globals.screenHeight * 0.08 }]}>
                                        <View style={styles.emallist_backgroundStyle}>
                                            <Text style={styles.emallist_mainTextStyle}>{emailStatus}</Text>
                                        </View>
                                        <View style={styles.emallist_MainViewContainer}></View>
                                        <FlatList
                                            style={{ marginBottom: globals.screenHeight * 0.15 }}
                                            showsVerticalScrollIndicator={false}
                                            data={emailListData}
                                            renderItem={({ item, index }) => this.renderItemUI(item, index)}
                                            extraData={this.state}
                                            bounces={false}
                                            keyExtractor={(index, item) => item.toString()}
                                            ListFooterComponent={this.renderFooter}
                                            onEndReached={this.handleLoadMore}
                                            onEndReachedThreshold={0.5}
                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                        />
                                    </View>
                            :
                            <ServerError loading={loading} onPress={() => this._tryAgain()} />
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmailListScreen);