import React from 'react';
import { Text, View, FlatList, TouchableOpacity, ActivityIndicator, ScrollView, Alert } from 'react-native';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { API } from '../../../../../utils/api';
import { Dropdown } from '../../../../../libs/react-native-material-dropdown';
import DateRangePicker from '../../../../../components/DatePicker';
import moment from 'moment';
import styles from './style';

const TAG = '==:== StatisticalAnalysis : ';
let _this;
const iPad = DeviceInfo.getModel();

export default class StatisticalAnalysis extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Statistical Analysis'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
      });

    constructor(props){
        super(props);
        _this = this;
        this.state = {
            data: [],
            emailUser: '',
            emailDate: '',
            startDate: '',
            endDate: '',
            // startDisplayDate: moment()
            //     .format('MM/DD/YYYY')
            //     .toUpperCase(),
            // startDisplayDay: moment()
            //     .format('dddd')
            //     .toUpperCase(),
            // endDisplayDate: moment()
            //     .format('MM/DD/YYYY')
            //     .toUpperCase(),
            // endDisplayDay: moment()
            //     .format('dddd')
            //     .toUpperCase(),
            startDisplayDate: '',
            startDisplayDay: '',
            endDisplayDate: '',
            endDisplayDay: '',
            openPicker: false,
            loading: false,
            emailReportData: [],
            reportListShow: false,
            GridViewItems: [
                {
                  key: 'Emails Sent',
                  count: 0,
                },
                {
                  key: 'Emails Delivered',
                  count: 0,
                },
                {
                  key: 'Emails Bounced',
                  count: 0
                },
                {
                  key: 'Emails Opened',
                  count: 0,
                },
                {
                  key: 'Emails Clicked',
                  count: 0,
                },
            ],
        }
    }

    componentDidMount() {
      this.setUserName()
    }

    /**
     * get user name and setState in onChangeText for dropdown
     */
    setUserName(){
        let userName = globals.loginFullName
        let tempArr = [];
        let obj = {
            ID: +1,
            value: userName,
        }
        tempArr.push(obj)
        this.setState({ data: tempArr })
        // this.onChangeText(userName)
    }


    /**
    * Api call GetEmailActivityReport on Submit 
    * 
    */
    _onSubmitGetEmailActivityReport(){
        const {startDisplayDate, endDisplayDate} = this.state;
        this.closeDatePicker();
        if (startDisplayDate !== '' , endDisplayDate !== '') {
            this.setState({ loading: true, reportListShow: true })
            if (globals.isInternetConnected === true) {
                API.getEmailactivityreport(this.getEmailactivityreportResponseData, true, globals.userID, startDisplayDate, endDisplayDate);
            } else {
                this.setState({ loading: false })
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
            }
        } else {
            this.setState({openPicker: true})
            Alert.alert(globals.appName, "Please select schedule date")
        }
    }

    /**
    * callback response of getEmailactivityreportResponseData
    * 
    */
   getEmailactivityreportResponseData = {
    success: response => {
        console.log(
            TAG,
            'getEmailactivityreportResponseData-> success : ',
            JSON.stringify(response)
        );
        if (response.StatusCode == 200) {
            console.log("getEmailactivityreportResponseData Data ::", response.Data[0]);
            let reportData = response.Data;
            this.setState({emailReportData: reportData,})
            this.setState({
                GridViewItems: [
                    {
                      key: 'Emails Sent',
                      count: reportData[0].EmailsSent,
                    },
                    {
                      key: 'Emails Delivered',
                      count: reportData[0].EmailsDelivered,
                    },
                    {
                      key: 'Emails Bounced',
                      count: reportData[0].EmailsBounced
                    },
                    {
                      key: 'Emails Opened',
                      count: reportData[0].EmailsOpened,
                    },
                    {
                      key: 'Emails Clicked',
                      count: reportData[0].EmailsClicked,
                    },
                ],
            })
        } else {
            Alert.alert(globals.appName, response.Message)
        }
        this.setState({loading: false})
    },
    error: err => {
        this.setState({loading: false})
        if (err.StatusCode == 401 || err.StatusCode == 403) {
            Alert.alert(
                globals.appName,
                'Your session is expired, Please login again',
                [{ text: 'OK' }],
                { cancelable: false }
            );
        } else {
            Alert.alert(
                globals.appName,
                err.Message
            );
        }
        console.log(
            TAG,
            'getEmailactivityreportResponseData-> ERROR : ',
            JSON.stringify(err.message)
        );
    },
    complete: () => {
        this.setState({loading: false})
        console.log("getEmailactivityreportResponseData-> complete");
    },
};

     /**
     * method for Select Particular Item
     */
    setSelection(item, index) {
        const { GridViewItems, startDisplayDate, endDisplayDate } = this.state;
        const dataCopy = GridViewItems;

        for (let i = 0; i < dataCopy.length; i++) {
            if (index == i && item.key == 'Emails Sent') {
                dataCopy[index].isSelected = true;
                if (item.count !== 0) {
                    this.props.navigationProps.navigation.navigate("EmailListScreen", { item: item, startDate: startDisplayDate, endDate: endDisplayDate })
                } else {
                    Alert.alert(globals.appName, "No Data found")
                }
            }
            else if (index == i && item.key == 'Emails Delivered') {
                dataCopy[index].isSelected = true;
                if (item.count !== 0) {
                    this.props.navigationProps.navigation.navigate("EmailListScreen", { item: item, startDate: startDisplayDate, endDate: endDisplayDate })
                } else {
                    Alert.alert(globals.appName, "No Data found")
                }
            }
            else if (index == i && item.key == 'Emails Bounced') {
                dataCopy[index].isSelected = true;
                if (item.count !== 0) {
                    this.props.navigationProps.navigation.navigate("EmailListScreen", { item: item, startDate: startDisplayDate, endDate: endDisplayDate })
                } else {
                    Alert.alert(globals.appName, "No Data found")
                }
            }
            else if (index == i && item.key == 'Emails Opened') {
                dataCopy[index].isSelected = true;
                if (item.count !== 0) {
                    this.props.navigationProps.navigation.navigate("EmailListScreen", { item: item, startDate: startDisplayDate, endDate: endDisplayDate })
                } else {
                    Alert.alert(globals.appName, "No Data found")
                }
            }
            else if (index == i && item.key == 'Emails Clicked') {
                dataCopy[index].isSelected = true;
                if (item.count !== 0) {
                    this.props.navigationProps.navigation.navigate("EmailListScreen", { item: item, startDate: startDisplayDate, endDate: endDisplayDate })
                } else {
                    Alert.alert(globals.appName, "No Data found")
                }
            }
            else {
                dataCopy[i].isSelected = false;
            }
        }
        this.setState({ GridViewItems: dataCopy });
    }

    /**
    * dropdown onChangeText method
    */
    onChangeText(text, index, data) {
        return (
            this.setState({ emailUser: text })
        )
    }

    afterDateSelection(startDate, endDate) {
        console.log(TAG, 'afterDateSelection');
        console.log(TAG, `startDate : ${startDate}`);
        console.log(TAG, `endDate : ${endDate}`);
    
        const stdate = moment(startDate, 'YYYY-MM-DD').format('MM/DD/YYYY');
        const enddate = moment(endDate, 'YYYY-MM-DD').format('MM/DD/YYYY');
        this.setState({
            startDisplayDate: moment(startDate)
                .format('MM/DD/YYYY')
                .toUpperCase(),
            startDisplayDay: moment(startDate)
                .format('dddd')
                .toUpperCase(),
            endDisplayDate: moment(endDate)
                .format('MM/DD/YYYY')
                .toUpperCase(),
            endDisplayDay: moment(endDate)
                .format('dddd')
                .toUpperCase(),
        })
    
        this.setState({
          startDisplayDate: stdate,
          endDisplayDate: enddate,
        });
        // if(endDate){
        //     this.closeDatePicker();
        // } else {
        //     console.log(TAG,"endDate :-->",endDate);  
        // }
      }
    
      onStartDayPress(day) {
      
        const stdate = moment(day.dateString, 'YYYY-MM-DD').format('MM/DD/YYYY');
        const stDay = moment(day.dateString, 'YYYY-MM-DD').format('dddd');

        this.setState({
            startDisplayDate: moment(day.dateString)
                .format('MM/DD/YYYY')
                .toUpperCase(),
            startDisplayDay: moment(day.dateString)
                .format('dddd')
                .toUpperCase(),
            endDisplayDate: moment(day.dateString)
                .format('MM/DD/YYYY')
                .toUpperCase(),
            endDisplayDay: moment(day.dateString)
                .format('dddd')
                .toUpperCase(),
        })
    
        this.setState({
          startDisplayDate: stdate.toUpperCase(),
          endDisplayDate: stdate.toUpperCase(),
          startDisplayDay: stDay.toUpperCase(),
          endDisplayDay: stDay.toUpperCase(),
        });
      
      }
    
    closeDatePicker(){
        this.setState({openPicker: false})
    }

    openDatetimePicker() {
        this.setState({ openPicker: true })
    }

    renderItemReportList(item, index){
        const {startDisplayDate, endDisplayDate} = this.state;
        return(
            <View>
                <TouchableOpacity onPress={() => { this.setSelection(item, index) }}>
                    {(item.isSelected == true) ? (
                        <View style={[styles.statics_emailReportItemViewStyle,{backgroundColor: colors.warmBlue,}]}>
                            <View style={styles.statics_titleAndCountStyleViewStyle}>
                                 <Text style={[styles.statics_titleTextStyle,{color: colors.white}]}>{item.key}</Text>
                                <View style={styles.statics_CountViewStyle}>
                                    <Text style={[styles.statics_countTextStyle,{color: colors.white}]}>{item.count}</Text>
                                </View>
                            </View>
                        </View>
                    ) :
                        (<View style={styles.statics_emailReportItemViewStyle}>
                            <View style={styles.statics_titleAndCountStyleViewStyle}>
                                <Text style={styles.statics_titleTextStyle}>{item.key}</Text>
                                <View style={styles.statics_CountViewStyle}>
                                    <Text style={styles.statics_countTextStyle}>{item.count}</Text>
                                </View>
                            </View>
                        </View>)}
                </TouchableOpacity>
            </View>
        )
    }

    
    render() {        
        const {GridViewItems,data, emailDate, emailUser, startDate, endDate, startDisplayDay, endDisplayDay, openPicker, startDisplayDate, endDisplayDate, emailReportData, loading, reportListShow} = this.state;
        const labelUserName = (emailUser == "") ? globals.loginFullName : '' ;

        return (
            <View style={styles.statics_mainParentStyle}>
                <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={{paddingBottom: globals.screenHeight * 0.02}}>
                    <Dropdown
                        containerStyle={styles.statics_dropdownContainer}
                        fontSize={globals.font_14}
                        itemColor={colors.black}
                        textColor={colors.lightGray}
                        label={labelUserName}
                        isfrom={"statisticalAnalysis"}
                        data={data}
                        onChangeText={(text) => this.onChangeText(text)}
                    />
                    <TouchableOpacity style={styles.statics_DateBtnStyle} onPress={() => this.openDatetimePicker()}>
                        <Text style={styles.statics_dateSelectStyle}>
                            {
                                ( startDisplayDate == '',
                                    endDisplayDate == '') ?
                                    "Select the Schedule Date" : startDisplayDate +" "+ "-" + " "+endDisplayDate
                            }
                        </Text></TouchableOpacity>
                    {
                        (openPicker == true) ?
                            <View style={styles.statics_DateViewStyle}>
                                <DateRangePicker
                                    initialRange={[startDisplayDate, endDisplayDate]}
                                    onSuccess={(s, e) => this.afterDateSelection(s, e)}
                                    onStartDayPress={day => this.onStartDayPress(day)}
                                    theme={{
                                        markColor: colors.white,
                                        markTextColor: colors.blue,
                                        calendarBackground: colors.darkSkyBlue,
                                        monthTextColor: colors.white,
                                        textDisabledColor: colors.lightBorder,
                                        dayTextColor: colors.white,
                                        arrowColor: colors.white,
                                    }}
                                />
                            </View> : null
                    }
                    {(openPicker == true) ?
                        <TouchableOpacity
                            onPress={() => this._onSubmitGetEmailActivityReport()}
                            style={styles.statics_submitStyle}>
                            <View style={styles.statics_attchIconViewContainer}>
                                <Text numberOfLines={1} style={styles.statics_textStyle}>{"SUBMIT"}</Text>
                            </View>
                        </TouchableOpacity>
                        :
                        null
                    }
                    {(reportListShow == true) ?
                        (openPicker == false) ?
                            <View style={styles.statics_childlistViewStyle}>
                                <View>
                                    {(loading == true) ?
                                        <View>
                                            <ActivityIndicator size="small" color={colors.bgColor} />
                                        </View>
                                        :
                                        <FlatList
                                            style={styles.statics_flatListStyle}
                                            showsVerticalScrollIndicator={false}
                                            data={GridViewItems}
                                            renderItem={({ item, index }) => this.renderItemReportList(item, index)}
                                            extraData={this.state}
                                            bounces={false}
                                            keyExtractor={(index, item) => item.toString()}
                                        />
                                    }
                                </View>
                            </View>
                            :
                            null
                        :
                        null
                    }
                </ScrollView>
            </View>
        );
    }
}
