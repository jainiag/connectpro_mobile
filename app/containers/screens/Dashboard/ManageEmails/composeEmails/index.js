/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TextInput, TouchableOpacity, Image, FlatList, Modal, ScrollView, Alert, Linking, Platform } from 'react-native';
import styles from './style';
import ReactModal from "react-native-modal";
import { API } from '../../../../../utils/api';
import KeyboardListener from 'react-native-keyboard-listener';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/AntDesign';
import * as images from '../../../../../assets/images/map';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import DatePicker from 'react-native-datepicker';
import DeviceInfo from 'react-native-device-info';
import { Dropdown } from '../../../../../libs/react-native-material-dropdown';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import RNFS from 'react-native-fs';
import { PermissionsAndroid } from 'react-native';
import Validation from '../../../../../utils/validation';
import DocumentPicker from 'react-native-document-picker';
import moment from 'moment';


const TAG = '==:== ComposeEmails : ';
let _this;
let KeyName;
let csvDataformate;
let saveActivecommunityData;
let FinalArrayforcsv = [];
let removeDuplicate = [];
const iPad = DeviceInfo.getModel();
let bulkTypes;
class ComposeEmails extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Compose Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
        headerRight: (
            <View style={{ alignItems: 'center' }}>
                <TouchableOpacity
                    style={{
                        marginRight: globals.screenWidth * 0.04,
                        alignItems: 'center',
                        marginTop:
                            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                ? globals.screenHeight * 0.03
                                : null,
                    }}
                >
                    <Image
                        source={images.headerIcon.searchIcon}
                        resizeMode={"contain"}
                        style={{
                            height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                            width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                            tintColor: colors.warmBlue,
                            alignSelf: 'center',
                        }}
                    />
                </TouchableOpacity>
            </View>
        ),
    });

    constructor(props) {
        super(props);

        _this = this;
        this.state = {
            saveActivecommunityDataarr: [],
            newaddcontachArray: [],
            enableScrollViewScroll: true,
            finalContactData: [],
            opensampleCSV: 'false',
            chooseFromMultipleFiles: [],
            chooseFromFilesName: '',
            chooseFromFiles: '',
            chooseFromFilesType: '',
            csv_chooseFromFilesName: '',
            csv_chooseFromFiles: '',
            csv_chooseFromFilesType: '',
            selectedCSVdata: [],
            selectedEmailView: false,
            addContactData: [],
            toAddress: '',
            redtoAddress: false,
            subject: '',
            redsubject: false,
            composeEmail: '',
            isCheck: false,
            calendarOpen: false,
            visibleModal: false,
            visibleCotactModal: false,
            visibleCSVmodal: false,
            isAddClick: true,
            isChecked: [],
            selectedLists: [],
            allSelectedList: false,
            keyboardOpen: false,
            dropsowndata: [{
                Id: 0,
                value: 'Members',
            }, {
                Id: 1,
                value: 'Communities',
            }, {
                Id: 2,
                value: 'Groups',
            }, {
                Id: 3,
                value: 'Upcoming Events RSVP',
            }, {
                Id: 4,
                value: 'Past Events RSVP',
            }, {
                Id: 5,
                value: 'Canceled Events RSVP',
            }],
            draftEmailData: [],
            batchID: 0,
            dateTime: '',
            currentDateTime: '',
            minDate: moment().format('MM/DD/YYYY hh:mm:ss A'),
            maxDate: moment().add(25, 'year').format('MM/DD/YYYY hh:mm:ss A'),
            isOpenFile: false,
        }
    }


    componentDidMount() {
        var dateTime = moment().utcOffset('+05:30').format('MM/DD/YYYY hh:mm:ss A');
        this.setState({ currentDateTime: dateTime })
        // this.requestReadStoragePermission();
        // this.requestWriteStoragePermission();
    }


    // async   requestReadStoragePermission() {
    //     try {
    //         const granted = await PermissionsAndroid.request(
    //             PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,

    //         );
    //         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //             this.setState({ isOpenFile: true })
    //             console.log('You can use the camera');
    //         } else {
    //             this.setState({ isOpenFile: false })
    //             console.log('Camera permission denied');
    //         }
    //     } catch (err) {
    //         console.warn(err);
    //     }
    // }

    // async  requestWriteStoragePermission() {
    //     try {
    //         const granted = await PermissionsAndroid.request(
    //             PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,

    //         );
    //         if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //             this.setState({ isOpenFile: true })
    //             console.log('You can use the camera');
    //         } else {
    //             this.setState({ isOpenFile: false })
    //             console.log('Camera permission denied');
    //         }
    //     } catch (err) {
    //         console.warn(err);
    //     }
    // }

    /**
 * Method of Checked accept btn
 * 
 */
    onChecked() {
        this.setState({ isCheck: true })
        if (this.state.isCheck == true) {
            this.setState({ isCheck: false })
        }
        else {
            this.setState({ isCheck: true })
        }
    }

    /**
* Method of Checked add contact chechbox
* 
*/
    isIconCheckedOrNot = (item, index) => {
        let { isChecked, newaddcontachArray, allSelectedList } = this.state;
        isChecked[index] = !isChecked[index];

        if (isChecked[index] == false) {
            this.setState({ allSelectedList: false });
        }

        let count = 0;
        isChecked.map(item => {
            if (item == true) {
                count = count + 1;
            }
        })
        if (count === isChecked.length) {
            this.setState({ allSelectedList: true })
        }
        this.setState({ isChecked: isChecked });
        if (isChecked[index] == true) {
            if (item["EmailId"] !== null || item["Email Id"] !== null || item["Email"] !== null) {
                newaddcontachArray.push(item); /// for csv data we added item["EmailId"]
                var filteredArray = newaddcontachArray.filter((item, pos) => {
                    return newaddcontachArray.indexOf(item) == pos;
                });
                this.setState({ newaddcontachArray: filteredArray })
                this.forceUpdate();
            } else {
                newaddcontachArray.push(item);
                var filteredArray = newaddcontachArray.filter((item, pos) => {
                    return newaddcontachArray.indexOf(item) == pos;
                });
                this.setState({ newaddcontachArray: filteredArray })
                this.forceUpdate();
            }
        } else {
            newaddcontachArray.pop(item) /// for csv data we added item["EmailId"]
        }
    }

    /**
    * Method of Checked isHeaderIconCheckedOrNot
    * 
    */
    isHeaderIconCheckedOrNot(addContactData) {
        const { newaddcontachArray, allSelectedList } = this.state;
        let currentStatus = []
        addContactData.map((item, i) => {
            currentStatus[i] = !allSelectedList
        });


        if (addContactData.length == 0 || addContactData == []) {
            null
        } else {

            this.setState({ allSelectedList: true, newaddcontachArray: [] });
            if (allSelectedList == true) {
                this.setState({ allSelectedList: false, isChecked: currentStatus })
            }
            else {
                let finaldatais = newaddcontachArray.concat(addContactData)
                this.setState({ newaddcontachArray: finaldatais, isChecked: currentStatus })
            }
        }
    }

    handleModalStatus(visible) {
        this.setState({ visibleModal: visible })
    }

    openContactsModal(visible) {
        if (visible == false) {
            this.setState({ addContactData: [], KeyName: '', visibleModal: false, visibleCotactModal: visible, allSelectedList: false, isChecked: [] })
        } else {
            this.setState({ visibleModal: false, visibleCotactModal: visible, selectedLists: [], allSelectedList: false, isChecked: [] })
        }

    }

    openCSVModal(visible) {
        if (visible == false) {
            this.setState({
                visibleModal: false, visibleCSVmodal: visible, csv_chooseFromFiles: '',
                opensampleCSV: false,
                csv_chooseFromFilesName: '', csv_chooseFromFilesType: '',
            })
        } else {
            this.setState({ visibleModal: false, visibleCSVmodal: visible })
        }

    }


    /**
       *  method for select particular button
       */
    radioClickHandle2() {
        this.setState({ isAddClick: true });
        this.openContactsModal(true)
    }

    /**
         *  method for select particular button
         */
    radioClickHandle() {
        this.setState({ isAddClick: false });
    }

    /**
 * Method to open calender
 * 
 */
    openCalender() {
        this.setState({
            calendarOpen: true
        })
    }


    /**
    * Api call GetDraftEmailDataToComposeApiCall
    * 
    */
    static GetDraftEmailDataToComposeApiCall(BatchId) {
        _this.props.showLoader()
        let batch_Id = BatchId
        if (globals.isInternetConnected === true) {
            API.GetDraftEmailDataToCompose(_this.getDraftEmailDataToComposeResponseData, true, batch_Id, globals.userID);
        } else {
            _this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
    * callback response of getDraftEmailDataToComposeResponseData
    * 
    */
    getDraftEmailDataToComposeResponseData = {
        success: response => {
            console.log(
                TAG,
                'getDraftmailsInformationResponseData-> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                let draft_emaildata = response.Data
                let emailAddData = draft_emaildata.DraftEmailInfo.ToEmailsList;
                let email_subject = draft_emaildata.DraftEmailInfo.mailQueuePoco.EmailSubject;
                let compose_email = draft_emaildata.DraftEmailInfo.mailQueuePoco.EmailBody;
                let email_date = draft_emaildata.DraftEmailInfo.mailQueuePoco.ScheduledAtStr;
                let BatchId = draft_emaildata.OtherDraftInfo.BatchId;
                const regex = /(<([^>]+)>)/ig;
                const finalEmailBody = compose_email.replace(regex, '');
                this.setState({ draftEmailData: draft_emaildata, finalContactData: emailAddData, subject: email_subject, composeEmail: finalEmailBody, batchID: BatchId, isCheck: true, redsubject: true, dateTime: email_date });
            } else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader()
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK' }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'getDraftEmailDataToComposeResponseData-> ERROR : ',
                JSON.stringify(err.message)
            );
        },
        complete: () => {
            this.props.hideLoader()
            console.log("getDraftEmailDataToComposeResponseData-> complete");
        },
    };


    /**
    * Api call Getdraftattachments for get attachements in draft
    * 
    */
    static GetdraftattachmentsApiCall(BatchId) {
        let batch_Id = BatchId
        if (globals.isInternetConnected === true) {
            API.getDraftattachments(_this.getDraftattachmentsResponseData, true, batch_Id);
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
    * callback response of getDraftattachmentsResponseData
    * 
    */
    getDraftattachmentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'getDraftattachmentsResponseData-> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                console.log("getDraftattachmentsResponseData Data ::", response.Data);
                let attachementData = response.Data
                this.setState({ chooseFromMultipleFiles: attachementData })
            } else {
                Alert.alert(globals.appName, response.Message)
            }
        },
        error: err => {
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK' }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'getDraftattachmentsResponseData-> ERROR : ',
                JSON.stringify(err.message)
            );
        },
        complete: () => {
            console.log("getDraftattachmentsResponseData-> complete");
        },
    };



    renderItemList(item, index) {
        return (
            <View style={styles.flatlistView}>
                <View style={styles.bm_ListmainViewStyle}>
                    <TouchableOpacity
                        onPress={() => this.isIconCheckedOrNot(item, index)}
                        style={[styles.squareView, {
                            borderColor: colors.listSelectColor,
                            backgroundColor: (this.state.isChecked[index] == true) ? colors.listSelectColor : colors.white
                        }]}>
                        {
                            (this.state.isChecked[index] == true) ?
                                <Icon name="check" color={colors.white} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 25 : globals.screenWidth * 0.05} /> :
                                null
                        }
                    </TouchableOpacity>
                </View>
                <View style={styles.bm_ListnameViewStyle}>
                    <View>
                        <Text numberOfLines={1} style={styles.renderNameStyle}>{item.Name}</Text>
                    </View>
                </View>
                <View style={styles.bm_ListmemberViewStyle}>
                    <View>
                        <Text numberOfLines={1} style={styles.renderMenberStyle}>{item.TotalMembers}</Text>
                    </View>
                </View>
            </View>
        )
    }



    getContactsApicall(text, index, data) {
        let CommunityId = "";

        if (text == 'Members') {
            KeyName = "ACTIVE_MEMBERS";
        }
        else if (text == 'Communities') {
            KeyName = "ACTIVE_COMMUNITIES";
        }
        else if (text == 'Groups') {
            KeyName = "ACTIVE_GROUPS";
        }
        else if (text == 'Upcoming Events RSVP') {
            KeyName = "UPCOMING_EVENTS_RSVP";
        }
        else if (text == 'Past Events RSVP') {
            KeyName = "PAST_EVENTS_RSVP";
        }
        else if (text == 'Canceled Events RSVP') {
            KeyName = "CANCELED_EVENTS_RSVP";
        }
        if (globals.isInternetConnected == true) {
            if (KeyName == "ACTIVE_MEMBERS" || KeyName == "ACTIVE_COMMUNITIES") {
                API.getbulkcontacts(this.getbulkcontactsResponseData, true, KeyName, globals.userID);
            } else {
                API.getbulkcontactswithoutCommunity(this.getbulkcontactsResponseData, true, KeyName, globals.userID, CommunityId);
            }
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
     * Response callback of getbulkcontactsResponseData
     */
    getbulkcontactsResponseData = {
        success: response => {
            console.log(
                TAG,
                'getbulkcontactsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                let currentStatus = []
                response.Data.map((item, i) => {
                    currentStatus[i] = false
                });
                this.setState({ addContactData: response.Data, isChecked: currentStatus, allSelectedList: false });
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }
        },
        error: err => {
            console.log(
                TAG,
                'getbulkcontactsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
        },
    };

    /**
     * Method for save community data choose from community SAVE button
     * 
     */
    saveContactsfromcommunity(addContactData) {
        const { newaddcontachArray, finalContactData, saveActivecommunityDataarr } = this.state;
        if (newaddcontachArray.length == '' || newaddcontachArray == [] || newaddcontachArray == undefined || addContactData.length == '') {
            Alert.alert(globals.appName, globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.ADDCONTACT_DATA_NOT_AVLB)
        }
        else {
            if (KeyName == "ACTIVE_COMMUNITIES" || KeyName == "ACTIVE_GROUPS" || KeyName == "UPCOMING_EVENTS_RSVP" || KeyName == "PAST_EVENTS_RSVP" || KeyName == "CANCELED_EVENTS_RSVP") {
                for (let y = 0; y < this.state.newaddcontachArray.length; y++) {
                    saveActivecommunityData = this.state.newaddcontachArray[y].ID
                    saveActivecommunityDataarr.push(saveActivecommunityData)
                }
                this.setState({ saveActivecommunityDataarr: this.state.saveActivecommunityDataarr })
                if (KeyName == "UPCOMING_EVENTS_RSVP" || KeyName == "PAST_EVENTS_RSVP" || KeyName == "CANCELED_EVENTS_RSVP") {
                    API.onlyrspved(this.onlyrspvedResponseData, this.state.saveActivecommunityDataarr, true);
                }
                else if (KeyName == "ACTIVE_GROUPS") {
                    API.getUsersInGroup(this.getUsersInGroupResponseData, this.state.saveActivecommunityDataarr, true);
                } else {
                    API.getUsersInCommunity(this.getUsersInCommunityResponseData, this.state.saveActivecommunityDataarr, true);
                }
            } else {
                let newaddcontactarr = [];
                for (let u = 0; u < newaddcontachArray.length; u++) {
                    let finalobject = {
                        "Type": "members",
                        "Email": newaddcontachArray[u].EmailId,
                        "Firstname": '',
                        "Lastname": '',
                    }
                    newaddcontactarr.push(finalobject)
                }
                let uniquenewData = newaddcontactarr.filter((ele, ind) => ind === newaddcontactarr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let uniquenewfinalContactData = finalContactData.filter((ele, ind) => ind === finalContactData.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let addcontactnewArr = uniquenewfinalContactData.concat(uniquenewData);
                let uniquefinalContactData = addcontactnewArr.filter((ele, ind) => ind === addcontactnewArr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                this.setState({ finalContactData: [...uniquefinalContactData] }, () => {
                    this.closeContactsModal(false)
                    this.forceUpdate()
                })
            }
        }
    }



    /**
        * callback response of getUsersInCommunityResponseData
        * 
        */
    getUsersInCommunityResponseData = {
        success: response => {
            console.log(
                TAG,
                'getUsersInCommunityResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                const { finalContactData } = this.state;
                let newaddcontactarr = [];
                for (let u = 0; u < response.Data.length; u++) {
                    let finalobject = {
                        "Type": "getUsersInCommunity",
                        "Email": response.Data[u].EmailID,
                        "Firstname": response.Data[u].FirstName,
                        "Lastname": response.Data[u].LastName,
                    }
                    newaddcontactarr.push(finalobject)
                }
                let uniquenewData = newaddcontactarr.filter((ele, ind) => ind === newaddcontactarr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let uniquenewfinalContactData = finalContactData.filter((ele, ind) => ind === finalContactData.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let addcontactnewArr = uniquenewfinalContactData.concat(uniquenewData);
                let uniquefinalContactData = addcontactnewArr.filter((ele, ind) => ind === addcontactnewArr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))

                this.setState({ finalContactData: [...uniquefinalContactData] }, () => {
                    this.closeContactsModal(false)
                    this.forceUpdate()
                })
            }
        },
        error: err => {
            console.log(
                TAG,
                'getUsersInCommunityResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
        },
    }


    /**
    * callback response of getUsersInGroupResponseData
    * 
    */
    getUsersInGroupResponseData = {
        success: response => {
            console.log(
                TAG,
                'getUsersInGroupResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                const { finalContactData } = this.state;
                let newaddcontactarr = [];
                for (let u = 0; u < response.Data.length; u++) {
                    let finalobject = {
                        "Type": "getUsersInGroup",
                        "Email": response.Data[u].EmailID,
                        "Firstname": response.Data[u].FirstName,
                        "Lastname": response.Data[u].LastName,
                    }
                    newaddcontactarr.push(finalobject)
                }
                let uniquenewData = newaddcontactarr.filter((ele, ind) => ind === newaddcontactarr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let uniquenewfinalContactData = finalContactData.filter((ele, ind) => ind === finalContactData.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let addcontactnewArr = uniquenewfinalContactData.concat(uniquenewData);
                let uniquefinalContactData = addcontactnewArr.filter((ele, ind) => ind === addcontactnewArr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                this.setState({ finalContactData: [...uniquefinalContactData] }, () => {
                    this.closeContactsModal(false)
                    this.forceUpdate()
                })
            }
        },
        error: err => {
            console.log(
                TAG,
                'getUsersInGroupResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
        },
    }


    /**
        * callback response of onlyrspvedResponseData
        * 
        */
    onlyrspvedResponseData = {
        success: response => {
            console.log(
                TAG,
                'onlyrspvedResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                const { finalContactData } = this.state;
                let newaddcontactarr = [];
                for (let u = 0; u < response.Data.length; u++) {
                    let finalobject = {
                        "Type": "onlyrspved",
                        "Email": response.Data[u].EmailID,
                        "Firstname": response.Data[u].FirstName,
                        "Lastname": response.Data[u].LastName,
                    }
                    newaddcontactarr.push(finalobject)
                }
                let uniquenewData = newaddcontactarr.filter((ele, ind) => ind === newaddcontactarr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let uniquenewfinalContactData = finalContactData.filter((ele, ind) => ind === finalContactData.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let addcontactnewArr = uniquenewfinalContactData.concat(uniquenewData);
                let uniquefinalContactData = addcontactnewArr.filter((ele, ind) => ind === addcontactnewArr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                this.setState({ finalContactData: [...uniquefinalContactData] }, () => {
                    this.closeContactsModal(false)
                    this.forceUpdate()
                })
            }
        },
        error: err => {
            console.log(
                TAG,
                'onlyrspvedResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
        },
    }

    closeContactsModal() {
        this.setState({ finalContactData: [] })
        this.openContactsModal(false)
    }


    /**
 * Method to open Contact Modal
 * 
 */
    renderContactsModal() {

        const { allSelectedList, addContactData, dropsowndata } = this.state;
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.state.visibleCotactModal}
                onRequestClose={() => {
                    this.openContactsModal(false);
                }}
                onBackdropPress={() => {
                    this.openContactsModal(false);
                }}
            >
                <View style={styles.Endrs_MainViewStyle}>
                    <View style={styles.Endrs_CotactsChildViewContainer}>
                        <View style={[styles.Endrs_ContactIconViewStyle]}>
                            <Text style={styles.addContactTextstyle}>{"Add Contacts"}</Text>
                            <View style={styles.crossIcon}>
                                <Icon name="closecircleo" size={globals.screenHeight * 0.03} color={colors.warmBlue} onPress={() => this.openContactsModal(false)} />
                            </View>
                        </View>
                        <View style={styles.contactByStyle}>
                            <Text style={styles.contactByStyleText}>{"Get Contacts By:"}</Text>
                        </View>
                        <View style={styles.dropdownViewContainer}>
                            <Dropdown
                                containerStyle={styles.dropdownStyleContainer}
                                fontSize={globals.font_14}
                                itemColor={colors.black}
                                textColor={colors.black}
                                label='Get Contacts'
                                data={dropsowndata}
                                onChangeText={(text, index, data) => this.getContactsApicall(text, index, data)}
                            />
                        </View>
                        <View style={styles.bm_ViewStyle}>
                            <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                                <View style={styles.headerViewStyle}>
                                    <View style={[styles.bm_mainViewStyle, { backgroundColor: colors.listSelectColor }]}>
                                        <TouchableOpacity
                                            onPress={() => this.isHeaderIconCheckedOrNot(addContactData)}
                                            style={[styles.squareView, {
                                                borderColor: colors.white,
                                                backgroundColor: (allSelectedList == true) ? colors.white : colors.white
                                            }]}>
                                            {
                                                (allSelectedList == true) ?
                                                    <Icon name="check" color={colors.warmBlue} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 25 : globals.screenWidth * 0.05} /> :
                                                    null
                                            }
                                        </TouchableOpacity>
                                    </View>
                                    <View style={[styles.bm_nameViewStyle, { backgroundColor: colors.listSelectColor }]}>
                                        <Text numberOfLines={1} style={styles.headerNameTextStyle}>Name</Text>
                                    </View>
                                    <View style={[styles.bm_memberViewStyle, { backgroundColor: colors.listSelectColor }]}>
                                        {
                                            (KeyName == "ACTIVE_GROUPS") ?
                                                <Text numberOfLines={1} style={styles.headerMenberTextStyle}>Members</Text> :
                                                <Text numberOfLines={1} style={styles.headerMenberTextStyle}>Member</Text>
                                        }

                                    </View>
                                </View>
                                <View style={styles.flatlistborderradius}>
                                    {
                                        (addContactData.length == 0 || addContactData == []) ?
                                            <View style={globalStyles.nodataStyle}>
                                                {/* <Text style={globalStyles.nodataTextStyle}>{"There is no data Please try again"}</Text> */}
                                            </View>
                                            :
                                            <FlatList
                                                style={{ flex: 1 }}
                                                data={addContactData}
                                                renderItem={({ item, index }) => this.renderItemList(item, index)}
                                                keyExtractor={(index, item) => item.toString()}
                                                extraData={this.state}
                                            />
                                    }

                                </View>
                            </ScrollView>



                        </View>
                        <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.saveContactsfromcommunity(addContactData)}>
                            <Text style={styles.tvSearchStyle}>{'SAVE'}</Text>
                        </TouchableOpacity>
                    </View>

                </View>

            </Modal>
        )
    }

    /**
    * Method to open CSVModal
    * 
    */
    async chooseCSVFile() {
         FinalArrayforcsv = [];
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log('URI : ' + res.uri);
            console.log('Type : ' + res.type);
            console.log('File Name : ' + res.name);
            console.log('File Size : ' + res.size);
            this.setState({ csv_chooseFromFiles: res.uri, csv_chooseFromFilesType: res.type, csv_chooseFromFilesName: res.name })
            this.convertTocsv(res.uri);
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log(TAG, "CANCEL :", err);
            } else {
                console.log(TAG, "ERROR :", err);
                throw err;
            }
        }
    }

    /**
* Method to convertTocsv
* 
*/
    async convertTocsv(path) {
        const file = RNFS.readFile(path);
        csvDataformate = await file;
        var DataArray = [];
        //  console.log("csvDataformate after read file ::", csvDataformate);
        csvDataformate = csvDataformate.replace(/ /g, '');
        // console.log("csvDataformate after remove sapce",csvDataformate);
        
        let replaceCSV = '';

        if (csvDataformate.includes('\r') || csvDataformate.includes('\r\n') || csvDataformate.includes('\n\r') ||  csvDataformate.includes('\n')) {
            replaceCSV = csvDataformate.replace(/\r\n|\n\r|\n\n|\n|\r/gi, '\n');
        }

        DataArray = replaceCSV.split("\n");
        DataArray.splice(0, 1);
        // console.log("DataArray ::", DataArray);
        // console.log("replaceCSV ::", replaceCSV);
        

        for (var i = 0; i < DataArray.length; i++) {
            var array = DataArray[i].split(",");
            if (array[0].length > 0) {
                let finalobject = {
                    "Type": "fromcsv",
                    "Email": array[0],
                    "Firstname": '',
                    "Lastname": '',
                }
                FinalArrayforcsv[i] = finalobject;
            }
        }
        // console.log("FinalArrayforcsv ::", FinalArrayforcsv);
        
        removeDuplicate = FinalArrayforcsv.filter((ele, ind) => ind === FinalArrayforcsv.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
    }



    /**
     * Method to download Samplecsv File
     * 
     */
    downloadSamplecsvFile() {
        this.setState({ csv_chooseFromFilesType: '', csv_chooseFromFilesName: '', csv_chooseFromFiles: '', FinalArrayforcsv: [] })

        let Filenamestr = "SampleInput";
        let Profileurl = "https://cpcommunityqa.azurewebsites.net/Content/CSVBulkInput/SampleInput.csv";
        let path = Platform.OS == 'android' ? RNFS.ExternalStorageDirectoryPath : RNFS.DocumentDirectoryPath;
        const absolutePath = `${path}/ConnectProSampleInputFile/`;

        RNFS.exists(absolutePath)
            .then((result) => {
                if (result) {
                    RNFS.downloadFile({ fromUrl: Profileurl, toFile: absolutePath + `/${Filenamestr}.csv` }).promise
                        .then((res) => {
                            console.log("res", res)
                            Alert.alert(globals.appName, "File download successfully.")
                        })
                        .catch((err) => { console.log("err", err) });
                } else {
                    RNFS.mkdir(absolutePath)
                        .then((success) => {
                            RNFS.downloadFile({ fromUrl: Profileurl, toFile: absolutePath + `/${Filenamestr}.csv` }).promise
                                .then((res) => {
                                    Alert.alert(globals.appName, "File download successfully.")
                                })
                                .catch((err) => { console.log("errDownload", err) })
                        })
                        .catch((err) => { console.log("err_makedir", err) })
                }
            })
            .catch((error) => {
                RNFS.mkdir(absolutePath)
                    .then((success) => {
                        RNFS.downloadFile({ fromUrl: Profileurl, toFile: absolutePath + `/${Filenamestr}.csv` }).promise
                            .then((res) => {
                                console.log("catchDIR", res)
                            })
                            .catch((err) => { console.log("errerr", err) });
                    })
                    .catch((err) => { console.log("errerrEND", err) })
            });
    }

    /**
 * Method to saveCSVFile
 * 
 */
    saveCSVFile() {
        const { csv_chooseFromFiles } = this.state;
        let isitCSV = csv_chooseFromFiles.includes("csv")
        if (isitCSV) {
            let csvData = csvDataformate.includes("EmailId") || csvDataformate.includes("Email Id") || csvDataformate.includes("Email")

            if (csvData) {
                let uniquenewfinalContactDatafromcsv = this.state.finalContactData.filter((ele, ind) => ind === this.state.finalContactData.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let removeduplicateFromFinalData = removeDuplicate.filter((ele, ind) => ind === removeDuplicate.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
                let afterRemoveaddcontactnewArr = removeduplicateFromFinalData.concat(uniquenewfinalContactDatafromcsv);
                let afterRemoveuniquefinalContactData = afterRemoveaddcontactnewArr.filter((ele, ind) => ind === afterRemoveaddcontactnewArr.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))

                this.setState({ finalContactData: [...afterRemoveuniquefinalContactData], csv_chooseFromFiles: '', csv_chooseFromFilesType: '', csv_chooseFromFilesName: '' }, () => {
                    this.openCSVModal(false)
                })

            }
            else {
                Alert.alert(globals.appName, globals.MESSAGE.MANAGE_EMAILS.EMAILDID_COLUMN_NEEDED);
                this.setState({ opensampleCSV: true, csv_chooseFromFilesName: '' })
            }
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.MANAGE_EMAILS.ONLY_CSV_FILE);
            this.setState({ opensampleCSV: true, csv_chooseFromFilesName: '' })
        }

    }

    /**
    * Method to cancleCSVFile
    * 
    */
    cancleCSVFile() {
        this.setState({ csv_chooseFromFiles: '', csv_chooseFromFilesType: '', csv_chooseFromFilesName: '' }, () => {
            this.openCSVModal(false)
        })
    }

    /**
    * Method to open CSVModal
    * 
    */
    renderCSVModal() {
        const { isAddClick, csv_chooseFromFilesName, opensampleCSV } = this.state;
        return (
            <ReactModal
                animationType="none"
                transparent={true}
                onBackdropPress={() => this.setState({ visibleCSVmodal: false })}
                visible={this.state.visibleCSVmodal}
                onRequestClose={() => {
                    this.openCSVModal(false);
                }}
            >
                <View style={styles.Endrs_MainViewStyle}>

                    <View style={styles.Endrs_CSVChildViewContainer}>
                        <View style={[styles.Endrs_ContactIconViewStyle]}>
                            <Text style={styles.addContactTextstyle}>{"Import From CSV"}</Text>
                            <View style={styles.crossIcon}>
                                <Icon name="closecircleo" size={globals.screenHeight * 0.03} color={colors.warmBlue} onPress={() => this.openCSVModal(false)} />
                            </View>
                        </View>
                        <View style={[styles.mp_buttonContainer, { marginHorizontal: 0 }]}>
                            <View style={[styles.textInputViewContainer2, { marginHorizontal: globals.screenWidth * 0.05 }]}>
                                <TouchableOpacity style={styles.choosebtnStyle} onPress={() => { this.chooseCSVFile() }}>
                                    <Text style={styles.chooseFilebtn}>{"Choose File"}</Text>
                                </TouchableOpacity>
                                <Text numberOfLines={1} style={[styles.chooseFilebtn, { width: globals.screenWidth * 0.35, marginHorizontal: globals.screenWidth * 0.015 }]}>
                                    {(csv_chooseFromFilesName) ? csv_chooseFromFilesName :
                                        "No File Chosen"}</Text>
                            </View>
                            {/* {
                                (opensampleCSV == true) ?
                                    <View style={{
                                        marginVertical: globals.screenHeight * 0.01,
                                        marginHorizontal: globals.screenWidth * 0.01, alignItems: 'flex-end', justifyContent: 'flex-end'
                                    }}>
                                        <TouchableOpacity onPress={() => this.downloadSamplecsvFile()}
                                            style={[styles.choosebtnStyle, { width: globals.screenWidth * 0.4, backgroundColor: colors.listSelectColor }]}>
                                            <Text style={[styles.chooseFilebtn, { color: colors.white }]}>{"Sample Input File"}</Text>
                                        </TouchableOpacity>
                                    </View> :
                                    null
                            } */}
                        </View>
                        <View style={styles.createCancelBtnViewContainer} >
                            <TouchableOpacity style={styles.cancelBtnContainer} onPress={() => { this.cancleCSVFile() }}>
                                <Text style={[styles.seachTextStyle, { color: colors.black }]}>{'CANCEL'}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.createBtnContainer} onPress={() => { this.saveCSVFile() }}>
                                <Text style={[styles.seachTextStyle, { color: colors.white }]}>{'SAVE'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </View>
            </ReactModal>
        )
    }


    closeContactsModal() {
        this.setState({ newaddcontachArray: [] })
        this.openContactsModal(false)
    }


    /**
    * Method to open Modal
    * 
    */
    renderContentModal() {
        const { isAddClick } = this.state;
        return (
            <ReactModal
                animationType="none"
                transparent={true}
                onBackdropPress={() => this.setState({ visibleModal: false })}
                visible={this.state.visibleModal}
                onRequestClose={() => {
                    this.handleModalStatus(false);
                }}
            >
                <View style={styles.Endrs_MainViewStyle}>

                    <View style={styles.Endrs_ChildViewContainer}>
                        <View style={styles.Endrs_ViewStyle}>
                            <View style={styles.Endrs_IconViewStyle}>
                                <Icon name="closecircleo" size={globals.screenHeight * 0.03} color={colors.warmBlue} onPress={() => this.closeContactsModal()} />
                            </View>
                            <View style={styles.mp_buttonContainer}>
                                <TouchableOpacity onPress={() => this.openContactsModal(true)}>
                                    <View style={[styles.mp_boxViewStyles, { backgroundColor: isAddClick === true ? colors.listSelectColor : colors.white }]}>
                                        <Text style={[styles.innerTextStyle, { color: isAddClick === true ? colors.white : colors.black }]}>{"Add Contacts"}</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.openCSVModal(true)}>
                                    <View style={[styles.mp_boxViewStyles, { backgroundColor: isAddClick === false ? colors.listSelectColor : colors.white }]}>
                                        <Text style={[styles.innerTextStyle, { color: isAddClick === false ? colors.white : colors.black }]}>{"Import From CSV"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>

                    </View>
                </View>
            </ReactModal>
        )
    }

    /**
    * Method to chooseFiles
    * 
    */
    async chooseFiles() {

        const { chooseFromMultipleFiles } = this.state;
        try {
            if (chooseFromMultipleFiles.length > 3) {
                this.setState({ chooseFromMultipleFiles: [] })
            }
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.allFiles],
            });
            for (const res of results) {
                console.log("Selected file -",
                    res.uri,
                    res.type, // mime type
                    res.name,
                    res.size
                );
                let resourenm = res.name;
                let slectedFiledata = resourenm.includes("jpeg") || resourenm.includes("jpg") || resourenm.includes("png") || resourenm.includes("pdf") ||
                    resourenm.includes("gif") || resourenm.includes(".doc") || resourenm.includes("docx") || resourenm.includes("bmp") || resourenm.includes("xlsx") ||
                    resourenm.includes(".txt") || resourenm.includes("docx") || resourenm.includes(".xls")
                if (slectedFiledata) {
                    if (res.size > 5000000) {
                        Alert.alert(globals.appName, "File Size Must Be 5 MB")
                    } else {
                        this.state.chooseFromMultipleFiles.push(res)
                        this.setState({ chooseFromFiles: res.uri, chooseFromFilesType: res.type, chooseFromFilesName: res.name, chooseFromMultipleFiles: this.state.chooseFromMultipleFiles })
                    }
                } else {
                    Alert.alert(globals.appName, "file not supported")
                }
            }

        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }

    }

    /**
               * Method to makeSavEmailApiCall
               * 
               */
    makeSavEmailApiCall(BulkType) {
        bulkTypes = BulkType;
        const { subject, finalContactData, chooseFromMultipleFiles, currentDateTime, dateTime } = this.state;
        const dateFormate = moment(new Date(dateTime)).format('MM/DD/YYYY hh:mm:ss A')
        const finalDate = (dateTime !== "") ? dateFormate : currentDateTime;
        let finalToListData = [];
        var arrMedia = [];

        for (let r = 0; r < finalContactData.length; r++) {

            if (finalContactData[r].Type) {
                var FirstName = finalContactData[r].Firstname;
                var LastName = finalContactData[r].Lastname;
                var emailAdd = finalContactData[r].Email;
                finalToListData.push({
                    "display": FirstName + LastName + "(" + emailAdd + ")",
                    "email": emailAdd
                })
            } else {
                finalToListData.push({
                    "display": "(" + finalContactData[r] + ")",
                    "email": finalContactData[r]
                })
            }
        }

        if (globals.isInternetConnected == true) {
            for (b = 0; b < chooseFromMultipleFiles.length; b++) {
                if (chooseFromMultipleFiles == '') {
                    null
                } else {
                    arrMedia.push({
                        name: chooseFromMultipleFiles[b].name,
                        fileName: chooseFromMultipleFiles[b].name,
                        type: chooseFromMultipleFiles[b].type,
                        uri: chooseFromMultipleFiles[b].uri,
                    })
                }
            }
            const data = {
                ToList: finalToListData,
                Subject: subject,
                Message: this.state.composeEmail,
                ScheduledDate: finalDate,
                BulkType: BulkType,
                BatchId: this.state.batchID,
                UserID: JSON.parse(globals.userID)
            }
            this.props.showLoader()
            API.savebulkmailrequest(this.savebulkmailrequestResponseData, { 'model': JSON.stringify(data) }, arrMedia, true);
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }




    /**
           * Method to cancleBulkMail & navigate to back screen
           * 
           */
    async cancleBulkMail() {
        this.setState({
            chooseFromMultipleFiles: [], saveActivecommunityDataarr: [], newaddcontactarr: [], tempArr: [], addcontactnewArr: [],
            selectedLists: [], isCheck: false, subject: '', composeEmail: '', finalarrofadd: '', toAddress: '', composeEmail: '', chooseFromFiles: '',
            chooseFromFilesName: '', chooseFromFilesType: '', selectedEmailView: false, finalContactData: [],
            redtoAddress: false, redsubject: false, dateTime: '', newaddcontachArray: [], batchID: 0
        }, () => {
            // this.props.navigationProps.navigation.goBack(null)
        })
    }




    /**
           * Method to send mail to server
           * 
           */
    sendBulkMail() {
        const { subject, finalContactData, toAddress } = this.state;
        if (toAddress !== "" || finalContactData.length > 0) {
            if (Validation.isValidEmail(toAddress) || finalContactData.length > 0) {
                if (finalContactData.length > 0 || (toAddress !== "")) {
                    this.addEmailinToAdd();
                } else {
                    console.log(TAG, "saveBulkMail api not called ");
                }
                if (Validation.textInputCheck(subject)) {
                    this.makeSavEmailApiCall("send")
                } else {
                    Alert.alert(globals.appName, globals.MESSAGE.MANAGE_EMAILS.SUBJECT_NEEDED);
                }
            }
            else {
                Alert.alert(globals.appName, "Invalid Email Address");
            }
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.MANAGE_EMAILS.TOADDRESS_NEEDED);
        }
    }


    /**
        * Method to save mail in darft
        * 
        */
    saveBulkMail() {
        const { subject, finalContactData, toAddress } = this.state;
        if (toAddress !== "" || finalContactData.length > 0) {
            if (Validation.isValidEmail(toAddress) || finalContactData.length > 0) {
                if (finalContactData.length > 0 || (toAddress !== "")) {
                    this.addEmailinToAdd();
                } else {
                    console.log(TAG, "saveBulkMail api not called ");
                }
                if (Validation.textInputCheck(subject)) {
                    this.makeSavEmailApiCall("save")
                } else {
                    Alert.alert(globals.appName, globals.MESSAGE.MANAGE_EMAILS.SUBJECT_NEEDED);
                }
            }
            else {
                Alert.alert(globals.appName, "Invalid Email Address");
            }
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.MANAGE_EMAILS.TOADDRESS_NEEDED);
        }
    }

    /**
    * Response callback of savebulkmailrequestResponseData
    */
    savebulkmailrequestResponseData = {
        success: response => {
            console.log(
                TAG,
                'savebulkmailrequestResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                console.log("response.Data", response.Data);

                this.setState({
                    saveActivecommunityDataarr: [], newaddcontactarr: [], tempArr: [],
                    finalContactData: [], chooseFromMultipleFiles: [], isCheck: false, composeEmail: '', subject: '', toAddress: '', composeEmail: '', chooseFromFiles: '',
                    chooseFromFilesName: '', chooseFromFilesType: '', selectedEmailView: false, redsubject: false, redtoAddress: false, dateTime: ''
                })
                if (response.StatusCode == 200 && response.Data == 'success!') {
                    if (bulkTypes == 'send') {
                        Alert.alert(globals.appName, "Bulk Mail sent.")
                    } else {
                        Alert.alert(globals.appName, "Bulk Mail saved.")
                    }

                }

                this.props.hideLoader()
            }
            else {
                Alert.alert(globals.appName, response.Data)
                this.props.hideLoader()
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            console.log(
                TAG,
                'savebulkmailrequestResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
            this.props.hideLoader();
        },
    };


    /**
         * 
         *  method for remove particular  email address (from Flatlist)
         */
    removeEmailforaddcontact(item, index) {
        const { finalContactData } = this.state;
        if (index > -1) {
            finalContactData.splice(index, 1);
        }
        this.setState({ finalContactData: finalContactData, redtoAddress: false, })
    }


    /**
         * 
         *  method for display email address (Flatlist)
         */
    renderAddContactsList(item, index) {

        // const isEmailName = globals.checkObject(item, 'Name');
        // const isEmailID = globals.checkObject(item, 'EmailId');
        return (
            <View style={[styles.textInputViewContainer2, { borderRadius: 9, marginHorizontal: 3, marginTop: 3, backgroundColor: colors.proUnderline }]}>
                {
                    (item.Type) ?
                        <Text
                            style={[styles.textInputStyleContainer,
                            { marginVertical: globals.screenWidth * 0.015, marginHorizontal: globals.screenWidth * 0.01 }]}>
                            {item.Firstname + " " + item.Lastname + "(" + item.Email + ")"}</Text> :

                        <Text style={[styles.textInputStyleContainer,
                        { marginVertical: globals.screenWidth * 0.015, marginHorizontal: globals.screenWidth * 0.01 }]}>
                            {item}</Text>
                }
                <Entypo
                    name="circle-with-cross"
                    size={globals.screenHeight * 0.02}
                    color={colors.listSelectColor} onPress={() => this.removeEmailforaddcontact(item, index)} />
            </View>
        )
    }





    /**
     * 
     *  method for add manually email address
     */
    addEmailinToAdd() {
        const { toAddress, finalContactData } = this.state;
        let temdatarr = [];
        if (Validation.isValidEmail(toAddress)) {
            temdatarr.push(toAddress);

            for (let u = 0; u < temdatarr.length; u++) {
                let finalobject = {
                    "Type": "manuallyEmail",
                    "Email": temdatarr[u],
                    "Firstname": '',
                    "Lastname": '',
                }
                finalContactData.push(finalobject)
            }
            let resultArray = finalContactData.filter((ele, ind) => ind === finalContactData.findIndex(elem => elem.Email === ele.Email && elem.id === ele.id))
            this.setState({ finalContactData: [...resultArray], toAddress: '' });


            this.memberNameRef.clear();
        } else {
            console.log(TAG, "Invalid Email Address");
        }
    }

    /**
    * 
    * onSubmitt method for clear state of address
    */
    onSubmitt() {
        this.setState({ toAddress: '', })
    }

    /**
    * onEmailChangeText method for onChangeText
    */
    onEmailChangeText(text) {
        let tempText = text;
        let finalValue;
        if (tempText.length > 0) {
            this.setState({ redtoAddress: true })
            if (tempText.charAt(tempText.length - 1) == ',') {
                finalValue = tempText.substr(0, tempText.length - 1);
                if (Validation.isValidEmail(finalValue)) {
                    this.setState({ toAddress: finalValue }, () => {
                        this.addEmailinToAdd();
                    })
                } else {
                    Alert.alert(globals.appName, "Invalid Email Address")
                }
            } else {
                tempText = text
                this.setState({ toAddress: tempText })
            }
        } else {
            this.setState({ redtoAddress: false, toAddress: "" })
        }

    }

    removemultipleFiles(item, index) {
        const { chooseFromMultipleFiles } = this.state;
        if (index > -1) {
            chooseFromMultipleFiles.splice(index, 1);
        }
        this.setState({ chooseFromMultipleFiles: chooseFromMultipleFiles })
    }


    /**
    * 
    * rendermultipleFilesView for display file name 
    */
    rendermultipleFilesView(item, index) {
        const FileName = (item.name) ? item.name : item.FileName

        return (
            <View style={[styles.textInputViewContainer2, { borderRadius: 9, marginHorizontal: 3, marginTop: 3, backgroundColor: colors.proUnderline }]}>
                {

                    <Text style={[styles.textInputStyleContainer,
                    { marginVertical: globals.screenWidth * 0.015, marginHorizontal: globals.screenWidth * 0.01 }]}>
                        {FileName}</Text>
                }

                <Entypo
                    name="circle-with-cross"
                    size={globals.screenHeight * 0.02}
                    color={colors.listSelectColor} onPress={() => this.removemultipleFiles(item, index)} />
            </View>
        )
    }



    /**
    * 
    * Upadte TextInput Data
    */
    handleEmailText(text) {
        if (text.length > 0) {
            this.setState({
                toAddress: text,
                redtoAddress: true,
            });
        } else {
            this.setState({
                toAddress: text,
                redtoAddress: false,
            });
        }
    }

    /**
    * 
    * Upadte TextInput subject Data
    */
    handleSubjectText(text) {
        if (text.length > 0) {
            this.setState({
                subject: text,
                redsubject: true,
            });
        } else {
            this.setState({
                subject: text,
                redsubject: false,
            });
        }
    }

    _openDocument() {
        // this.requestReadStoragePermission();
        // this.requestWriteStoragePermission();
        // if (this.state.isOpenFile == true) {
        this.chooseFiles();
        // } else {
        //     Alert.alert(
        //         globals.appName,
        //         "Allow to access photos, media, and files on your device?",
        //         [{ text: 'OK', onPress: () => Linking.openSettings() },
        //         { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
        //     );
        // }
    }


    render() {
        const { keyboardOpen, redsubject, redtoAddress, chooseFromMultipleFiles, finalContactData, toAddress, subject, composeEmail, isCheck, dateTime, minDate, maxDate } = this.state;


        return (
            <View style={styles.mainParentStyle} onStartShouldSetResponderCapture={() => {
                this.setState({ enableScrollViewScroll: true });
            }}>

                <KeyboardListener
                    onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                    onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                {this.renderContentModal()}
                {this.renderContactsModal()}
                {this.renderCSVModal()}
                <ScrollView scrollEnabled={this.state.enableScrollViewScroll} bounces={false} showsVerticalScrollIndicator={false} style={{ marginBottom: globals.screenHeight * 0.08 }}>
                    {

                        (finalContactData.length !== 0) ?
                            <View style={[styles.textinputselectedlist]} onStartShouldSetResponderCapture={() => {
                                this.setState({ enableScrollViewScroll: false });
                            }}>
                                <FlatList
                                    style={styles.flatlistwrapstyle}
                                    data={finalContactData}
                                    bounces={false}
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    showsVerticalScrollIndicator={false}
                                    renderItem={({ item, index }) => this.renderAddContactsList(item, index)}
                                    keyExtractor={(index, item) => item.toString()}
                                    extraData={this.state}
                                />
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <TextInput
                                        blurOnSubmit={(this.state.toAddress == '') ? true : false}
                                        style={[styles.textInputStyleContainer]}
                                        placeholder={"To Address"}
                                        placeholderTextColor={colors.lightGray}
                                        onChangeText={text => this.onEmailChangeText(text)}
                                        returnKeyType="done"
                                        value={toAddress}
                                        autoCapitalize="none"
                                        onSubmitEditing={() => (this.state.toAddress == '') ? this.onSubmitt() : this.addEmailinToAdd()}
                                        ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                                    />
                                    <TouchableOpacity onPress={() => this.handleModalStatus(true)}>
                                        <Image resizeMode="contain" source={images.EventRegistration.addIcon} style={styles.addContacts}></Image>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            :
                            <View style={[styles.api_textfiledView]}>
                                <TextInput
                                    blurOnSubmit={(this.state.toAddress == '') ? true : false}
                                    style={[styles.addPayerTextStyle]}
                                    placeholder={"To Address"}
                                    placeholderTextColor={colors.lightGray}
                                    onChangeText={text => this.onEmailChangeText(text)}
                                    returnKeyType="done"
                                    value={toAddress}
                                    autoCapitalize="none"
                                    onSubmitEditing={() => (this.state.toAddress == '') ? this.onSubmitt() : this.addEmailinToAdd()}
                                    ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                                />
                                {redtoAddress === false ? (
                                    <Text style={styles.api_textStyle}>*</Text>
                                ) : null}
                                <View style={styles.addcontactView}>
                                    <TouchableOpacity onPress={() => this.handleModalStatus(true)}>
                                        <Image resizeMode="contain" source={images.EventRegistration.addIcon} style={styles.addContacts}></Image>
                                    </TouchableOpacity>
                                </View>
                            </View>
                    }



                    <View style={styles.api_textfiledView}>
                        <TextInput
                            style={styles.addPayerTextStyle}
                            placeholder={"Subject"}
                            placeholderTextColor={colors.lightGray}
                            onChangeText={text => this.handleSubjectText(text)}
                            returnKeyType="next"
                            blurOnSubmit={true}
                            value={subject}
                            autoCapitalize="none"
                            ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                        />
                        {redsubject === false ? (
                            <Text style={styles.api_textStyle}>*</Text>
                        ) : null}
                    </View>
                    <View style={styles.textInputViewContainer2}>
                        <TextInput
                            multiline={true}
                            maxLength={300}
                            style={styles.textInputStyleContainer2}
                            placeholder={"Compose email"}
                            placeholderTextColor={colors.lightGray}
                            onChangeText={text => this.setState({ composeEmail: text })}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            value={composeEmail}
                            autoCapitalize="none"
                            ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                        />
                    </View>
                    <View style={styles.textInputViewContainer2}>
                        <TouchableOpacity style={styles.choosebtnStyle} onPress={() => (Platform.OS == "ios") ? this.chooseFiles() : (this.state.isOpenFile == false) ? this._openDocument() : this.chooseFiles()}>
                            <Text style={styles.chooseFilebtn}>{"Choose Files"}</Text>
                        </TouchableOpacity>
                        <Text numberOfLines={1} style={[styles.chooseFilebtn, { width: globals.screenWidth * 0.40, marginLeft: globals.screenWidth * 0.015 }]}>
                            {(chooseFromMultipleFiles.length !== 0) ? chooseFromMultipleFiles.length + " " + "Files" : "No File Chosen"}</Text></View>
                    {
                        (chooseFromMultipleFiles.length !== 0) ?
                            (chooseFromMultipleFiles.length > 3) ?
                                <View style={styles.textInputViewwithoutborder}>
                                    <Text numberOfLines={1} style={[styles.textoffilelimitation, { width: globals.screenWidth * 0.70 }]}>
                                        {"Only 3 files can be uploaded"}</Text></View>
                                :
                                <View style={styles.textInputViewContainer2}>
                                    <FlatList
                                        style={styles.flatlistwrapstyle}
                                        data={chooseFromMultipleFiles}
                                        bounces={false}
                                        horizontal={true}
                                        showsHorizontalScrollIndicator={false}
                                        showsVerticalScrollIndicator={false}
                                        renderItem={({ item, index }) => this.rendermultipleFilesView(item, index)}
                                        keyExtractor={(index, item) => item.toString()}
                                        extraData={this.state} />
                                </View>
                            : null
                    }
                    <View style={[styles.textInputViewwithoutborder, { marginTop: globals.screenHeight * 0.020, }]}>
                        <Text style={[styles.infoText, { textAlign: 'left' }]}>{"File Size limit: 5mb"}</Text>
                        <Text style={[styles.infoText, {
                            marginLeft: (Platform.OS == 'android') ? globals.screenWidth * 0.24 :
                                (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenWidth * 0.52 :
                                    globals.screenWidth * 0.20, textAlign: 'right'
                        }]}>{"Files Attachment limit: 3"}</Text>
                    </View>
                    <View style={[styles.textInputViewwithoutborder, { marginTop: globals.screenHeight * 0.001, }]}>
                        <Text style={styles.infoText}>{"File Types Allowed: "}</Text>
                        <Text style={[styles.infoText]}>{"jpeg,jpg,png,pdf,gif,doc,bmp,xls,xlsx,"}</Text>
                    </View>
                    <View style={styles.attachmentText}>
                        <Text style={[styles.infoText]}>{"txt,docx"}</Text>
                    </View>
                    <View style={[styles.textInputViewwithoutborder, { marginTop: globals.screenHeight * 0.025 }]}>
                        <TouchableOpacity onPress={() => this.onChecked()} style={[styles.squareView, {
                            backgroundColor: (isCheck === true) ? colors.warmBlue : colors.white
                        }]}>
                            {(isCheck === true) ? (
                                <Icon name="check" color={colors.white} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 25 : globals.screenWidth * 0.05} />
                            ) : null}
                        </TouchableOpacity>
                        <Text style={styles.scheduleText}>{"Do you want to schedule"}</Text>
                    </View>
                    {
                        (isCheck === true) ?
                            <DatePicker
                                style={styles.pickerStyle}
                                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_BY_EMAIL_DATE}
                                androidMode="spinner"
                                maxDate={maxDate}
                                minDate={minDate}
                                showIcon={true}
                                date={dateTime}
                                mode="datetime"
                                minuteInterval={10}
                                format="MM/DD/YYYY hh:mm:ss A"
                                confirmBtnText={globals.MESSAGE.SEARCH_SCREEN.SEARCH_CONFIRM_DATE}
                                cancelBtnText={globals.MESSAGE.SEARCH_SCREEN.SEARCH_CANCEL_DATE}
                                customStyles={{
                                    placeholderText: [styles.placeHolderTextStyle],
                                    dateIcon: [styles.dateIconStyle],
                                    dateInput: [styles.startDateInputStyle],
                                }}
                                onDateChange={date => this.setState({ dateTime: date })}
                            />
                            :
                            null
                    }

                </ScrollView>
                {
                    (keyboardOpen) ?
                        null :
                        <View style={styles.bottomViewStyle}>
                            <TouchableOpacity style={styles.bottomLeftView} onPress={() => this.cancleBulkMail()}>
                                <Text style={styles.cancleTextStyle}>CANCEL</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.bottomCenterView} onPress={() => this.saveBulkMail()}>
                                <Text style={styles.continueTextStyle}>SAVE</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.bottomRightView} onPress={() => this.sendBulkMail()}>
                                <Text style={styles.continueTextStyle}>SEND</Text>
                            </TouchableOpacity>

                        </View>
                }


            </View>
        );
    }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ComposeEmails);