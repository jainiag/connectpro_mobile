import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
    ////TextFieldViewStyle
    mainParentStyle: {
        flex: 1,
    },
    textInputViewContainer2: {


        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: 25,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
    },
    textinputselectedlist: {
        // backgroundColor:'red',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        // height: globals.screenHeight * 0.20,
        width: globals.screenWidth - 50,
        marginHorizontal: 25, borderWidth: 1,
        borderColor: colors.proUnderline,
        borderRadius: 5,
        marginTop: globals.screenHeight * 0.025,
    },
    flatlistwrapstyle: {
        marginHorizontal: globals.screenWidth * 0.01,
        marginVertical: 3
    },
    textInputStyleselectedList: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: 25,
        alignItems: 'center',
        height: globals.screenHeight * 0.15,
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
    },
    textInputViewContainer3: {
        flexDirection: 'row',
        //  marginHorizontal: 2,
        width: globals.screenWidth - 50,
        //  backgroundColor:'red', 
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.015,
    },
    crossIconsStyle: {
        marginRight: globals.screenWidth * 0.01,
        alignSelf: 'center'
    },
    api_textfiledView: {
        marginTop: globals.screenHeight * 0.025,
        borderWidth: 0.5, borderColor: colors.gray, borderRadius: 5,
        marginHorizontal: 25,
        // marginBottom: globals.screenHeight * 0.02,
        paddingHorizontal: globals.screenWidth * 0.04,
        alignItems: 'center', flexDirection: 'row',
        flex: 1

    },
    addPayerTextStyle: {
        paddingVertical: globals.screenWidth * 0.04,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
        // color:
    },
    textInputStyleContaineraddemails: {
        //  flex: 1,
        height: globals.screenHeight * 0.04,
        marginHorizontal: globals.screenWidth * 0.015,
        marginVertical: globals.screenHeight * 0.04,
    },
    textInputStyleContainer: {
        flex: 1,
        marginHorizontal: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        marginVertical: globals.screenHeight * 0.014,
    },
    scrollViewStyle: {
        marginLeft: globals.screenWidth * 0.01,
    },
    modalTextInputView2: {
        borderWidth: 1,
        borderRadius: 5,
        borderColor: colors.proUnderline,
        marginHorizontal: globals.screenWidth * 0.04,
        marginTop: globals.screenHeight * 0.015,
        // backgroundColor:'red',
        height: globals.screenHeight * 0.05,
    },
    textInputStyleContainer2: {
        height: globals.screenHeight * 0.12,
        marginHorizontal: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        marginVertical: globals.screenHeight * 0.014,
    },
    choosebtnStyle: {
        margin: globals.screenWidth * 0.015,
        backgroundColor: colors.lightBorder,
        width: globals.screenWidth * 0.3,
        alignItems: 'center', justifyContent: 'center',
        height: globals.screenWidth * 0.08,
        marginHorizontal: globals.screenWidth * 0.04,
        marginVertical: globals.screenHeight * 0.014,
    },
    chooseFilebtn: {
        fontSize: globals.font_14,
        color: colors.lightGray,
        alignSelf: 'center',
        textAlign: 'center',
    },
    textoffilelimitation: {
        color: colors.redColor,
        fontSize: globals.font_14
    },
    api_textStyle: {
        fontSize: globals.font_20,
        color: colors.redColor,
    },
    textInputViewwithoutborder: {
        flexDirection: 'row',
        marginHorizontal: 25,
        alignItems: 'center',
        // backgroundColor:'red'
    },
    infoText: {
        color: colors.grayShadow,
        fontSize: globals.font_12,
    },
    attachmentText: {
        marginHorizontal: 25,
        paddingBottom: globals.screenHeight * 0.02,
        borderBottomColor: colors.lightShadeGray,
        borderBottomWidth: 0.25,
    },
    squareView: {
        height:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.035
                : globals.screenWidth * 0.055,
        width:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.035
                : globals.screenWidth * 0.055,
        borderColor: colors.gray,
        borderWidth: 1,
        borderRadius: 4,
    },
    scheduleText: {
        marginLeft: globals.screenWidth * 0.03,
        color: colors.black,
        fontSize: globals.font_14,
        textAlign: 'center',
        alignSelf: 'center'
    },
    addcontactView:{
        flex: 1,  justifyContent: 'center', alignItems: 'flex-end'
    },
    addContacts: {
        alignSelf: 'center',
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.035 :globals.screenWidth * 0.045,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ?globals.screenWidth * 0.035 : globals.screenWidth * 0.045,
        tintColor: colors.listSelectColor,
        marginHorizontal: globals.screenWidth * 0.02
    },
    /////// bottom btn
    bottomViewStyle: {
        flexDirection: 'row',
        bottom: 0,
        position: 'absolute'
    },
    bottomLeftView: {
        flex: 1.5,
        backgroundColor: colors.proUnderline,
        alignItems: 'center',
        padding: globals.screenHeight * 0.02,
        justifyContent: 'center',
    },
    bottomRightView: {
        flex: 2.3,
        backgroundColor: colors.warmBlue,
        alignItems: 'center',
        justifyContent: 'center',
    },
    bottomCenterView: {
        flex: 1.9,
        backgroundColor: colors.listSelectColor,
        alignItems: 'center',
        justifyContent: 'center',
    },
    continueTextStyle: {
        color: colors.white,
        fontSize: globals.font_14,
    },
    cancleTextStyle: {
        fontSize: globals.font_13,
        color: colors.black,
    },
    /////Picker
    pickerStyle: {
        marginHorizontal: 25,
        marginTop:globals.screenHeight * 0.025,
        paddingVertical:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ?globals.screenHeight * 0.01 : 0,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ?globals.screenWidth * 0.92 :globals.screenWidth * 0.88
    },
    placeHolderTextStyle: {
        color: colors.lightGray,
        fontSize: globals.font_14,
        textAlign: 'center',
        marginLeft: globals.screenWidth * 0.05
    },
    dateIconStyle: {
        position: 'absolute',
        left: 0,
        marginLeft: 0,
    },
    startDateInputStyle: {
        borderRadius: 5,
        borderColor: colors.proUnderline,
        marginTop: 8,
        marginBottom: 8,
    },
    //////// open add contach modal 
    Endrs_MainViewStyle: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: -globals.screenWidth * 0.26,
        marginVertical: -globals.screenHeight * 0.12,
    },
    Endrs_CotactsChildViewContainer: {
        height: globals.screenHeight * 0.85,
        width: globals.screenWidth * 0.93,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.white,
        borderRadius: 3,
        borderWidth: 1,
        borderColor: colors.gray,
    },
    Endrs_ChildViewContainer: {
        height: globals.screenHeight * 0.30,
        justifyContent: 'center', alignItems: 'center',
        alignSelf: 'center',
        width: globals.screenWidth * 0.9,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.white,
        borderRadius: 3,
        borderWidth: 1,
        borderColor: colors.gray,
    },
    Endrs_CSVChildViewContainer: {
        height: globals.screenHeight * 0.40,
        width: globals.screenWidth * 0.9,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.white,
        borderRadius: 3,
        borderWidth: 1,
        borderColor: colors.gray,
    },
    Endrs_ViewStyle: {
        backgroundColor: colors.white,
        flex: 1,
        width: '85%',
        margin: 10,
    },
    Endrs_ContactIconViewStyle: {
        flexDirection: 'row',
        // marginHorizontal: globals.screenWidth * 0.05,
        marginVertical: globals.screenWidth * 0.04,
        paddingBottom: globals.screenWidth * 0.04,
        borderBottomColor: colors.proUnderline,
        borderBottomWidth: 0.5,
        // width:globals.screenWidth * 0.95
    },
    crossIcon: {
        flex: 1,
        marginHorizontal: globals.screenWidth * 0.05,
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    Endrs_IconViewStyle: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginBottom: 10,
    },
    Endrs_TextStyle: {
        color: colors.warmBlue,
        fontSize: globals.font_16,
    },
    Endrs_InputViewStyle: {
        flexDirection: 'row',
        height: globals.screenHeight * 0.25,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.01,
    },
    Endrs_InputStyle: {
        flex: 1,
        marginLeft: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        marginBottom: 5,
        height: globals.screenHeight * 0.18
    },
    Endrs_BtnViewStyle: {
        justifyContent: 'flex-end',
        marginTop: 10,
        flexDirection: 'row',
        width: "100%",
    },
    Endrs_OpacityStyle: {
        height: globals.screenHeight * 0.05,
        width: '100%',
        backgroundColor: colors.listSelectColor,
        marginLeft: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    Endrs_sendTextStyle: {
        fontSize: globals.font_14,
        color: colors.white,
    },
    ////////// modal inner btn style

    mp_buttonContainer: {
        marginHorizontal: globals.screenWidth * 0.08,
        marginVertical: globals.screenHeight * 0.02,
        flex: 1
        // padding:globals.screenWidth * 0.01
    },
    mp_boxViewStyles: {
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 1.5,
        borderColor: colors.proUnderline,
        borderRadius: globals.screenWidth * 0.015,
        marginBottom: globals.screenHeight * 0.03,
        height: globals.screenHeight * 0.055
    },
    innerTextStyle: {
        fontSize: globals.font_14,
    },
    mp_bottomViewStyle: {
        flex: 0.1, backgroundColor: colors.proUnderline, bottom: 0,
        alignItems: 'center', justifyContent: 'center',
        flexDirection: 'row'
    },
    //////// add contact modal style
    mainParentStylemodal: {
        flex: 1,
        marginTop: globals.screenHeight * 0.02,
        marginBottom: globals.screenHeight * 0.02,
    },
    bm_ListmainViewStyle: {
        width: globals.screenWidth * 0.157,
        // paddingVertical: 15,
        paddingTop: 15,
        paddingHorizontal: 20,
        alignItems: 'center',
    },
    bm_ViewStyle: {
        flex: 1,
        margin: globals.screenWidth * 0.05,

    },
    bm_ListnameViewStyle: {
        borderLeftWidth: 1,
        borderLeftColor: colors.grayBG,
        width: globals.screenWidth * 0.45,
        paddingVertical: 15,
        // paddingTop:15,
        paddingHorizontal: 15,
    },
    bm_ListmemberViewStyle: {
        borderLeftWidth: 1,
        borderLeftColor: colors.grayBG,
        width: globals.screenWidth * 0.21,
        paddingVertical: 15,
        alignItems: 'center',
        // paddingTop:15,
        paddingHorizontal: 20,
    },
    bm_mainViewStyle: {
        width: globals.screenWidth * 0.157,
        paddingTop: globals.screenWidth * 0.04,
        // padding: globals.screenWidth * 0.04,
        alignItems: "center",
        borderTopLeftRadius: 8,
    },
    bm_nameViewStyle: {
        borderLeftWidth: 1.5,
        borderLeftColor: colors.white,
        width: globals.screenWidth * 0.45,
        padding: globals.screenWidth * 0.04,
    },
    bm_memberViewStyle: {
        borderLeftWidth: 1.5,
        borderLeftColor: colors.white,
        width: globals.screenWidth * 0.22,
        justifyContent:'center',
        paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?26 : 15,
        borderTopRightRadius: 8,
    },
    touchableStyle: {
        alignItems: "center",
        justifyContent: 'center'
    },
    textStyle: {
        alignSelf: 'center',
        fontSize: globals.font_10
    },
    boxStyle: {
        // padding: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 20 : 10,
        borderWidth: 1,
        alignSelf: 'center',
        borderRadius: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 12 : 6,
    },
    headerViewStyle: {
        flexDirection: 'row',
        marginTop: globals.screenHeight * 0.02,
    },
    headerNameTextStyle: {
        fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_16,
        color: colors.white
    },
    headerMenberTextStyle: {
        fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_16,
        color: colors.white,
        alignSelf: 'center'
    },
    renderNameStyle: {
        fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 22 : globals.font_14,
        color: colors.lightGray
    },
    renderMenberStyle: {
        fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 22 : globals.font_14,
        color: colors.lightGray,
        alignSelf: 'center'
    },
    flatlistView: {
        flexDirection: 'row',
        // backgroundColor:'red',
        flex: 1,
        borderTopWidth: 0.5,
        borderTopColor: colors.gray
    },
    flatlistborderradius: {
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        borderColor: colors.grayBG,
        borderWidth: 1,
        marginBottom: globals.screenHeight * 0.055,
    },
    addContactTextstyle: {
        fontSize: globals.font_16,
        color: colors.warmBlue,
        marginHorizontal: globals.screenWidth * 0.05,
    },
    contactByStyle: {
        marginVertical: globals.screenHeight * 0.01,
        marginHorizontal: globals.screenWidth * 0.05,
    },
    contactByStyleText: {
        fontSize: globals.font_16,
        color: colors.lightgray,
    },
    dropdownViewContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        marginHorizontal: globals.screenWidth * 0.05,
        borderColor: colors.proUnderline,
        // alignItems: 'center',
        justifyContent:'center',
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
    },
    dropdownStyleContainer: {
        flex: 1,
        marginRight: globals.screenWidth * 0.05,
    },
    //////// bottom save btn style 
    toSearchStyle: {
        position: 'absolute',
        bottom: 0,
        height: globals.screenHeight * 0.0662, // 35
        backgroundColor: colors.bgColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
    },
    tvSearchStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    },
    ////// 2 -bootm btn view style 
    createCancelBtnViewContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
    },
    cancelBtnContainer: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.073 : globals.screenHeight * 0.06, // change in scrollview style also
        width: '50%',
        backgroundColor: colors.proUnderline,
        justifyContent: 'center',
        alignItems: 'center',
    },
    seachTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    },
    createBtnContainer: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.073 : globals.screenHeight * 0.06, // change in scrollview style also
        width: '50%',
        backgroundColor: colors.listSelectColor,
        justifyContent: 'center',
        alignItems: 'center',
    },
    /////   tagSelect library 

    item: {
        backgroundColor: 'lightgrey',
    },
    label: {
        // color: '#333',
        fontSize: globals.font_13
    },
    itemSelected: {
        // backgroundColor: '#333',
    },
    labelSelected: {
        // color:colors.white,
    },

});