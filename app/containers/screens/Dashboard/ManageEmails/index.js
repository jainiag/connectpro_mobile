/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import styles from './style'
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as images from '../../../../assets/images/map';
import * as colors from '../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { ScrollableTabView, ScrollableTabBar } from '../../../../libs/react-native-scrollable-tabview'
import ComposeEmails from './composeEmails/index';
import SentEmails from './sentEmails/index';
import DraftEmails from './draftEmails/index';
import StatisticalAnalysis from './statisticalAnalysis/index'

const TAG = '==:== ManageEmails : ';
let _this;
const iPad = DeviceInfo.getModel();
export default class ManageEmails extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const isSearch = (params) ? params.isSearch : '';
        return {
        headerLeft: globals.ConnectProbackButton(navigation, 'Manage Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
        headerRight: (
                (isSearch == false) ?
                    null
                    :
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => (isSearch == true) ? SentEmails.openSearchModal() : null}
                            style={{
                                marginRight: globals.screenWidth * 0.04,
                                alignItems: 'center',
                                marginTop:
                                    iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                        ? globals.screenHeight * 0.03
                                        : null,
                            }}
                        >
                            <Image
                                source={images.headerIcon.searchIcon}
                                resizeMode={"contain"}
                                style={{
                                    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                    tintColor: colors.warmBlue,
                                    alignSelf: 'center',
                                }}
                            />
                        </TouchableOpacity>
                    </View>
            ),
       }
      };

    constructor(props){
        super(props);
        _this = this;
        this.state = {
            initialTab: 0,
        }
    }

    componentDidMount(){
        this.props.navigation.setParams({ isSearch: false });
    }

    static goToPage(pageId){
        _this.tabView.goToPage(pageId)
    }

    refreshDraftEmailsList(){
        DraftEmails.clearSearchText();
        DraftEmails.refreshDraftMailList();
        this.props.navigation.setParams({ isSearch: false })
    }

    refreshSentEmailsList(){
        SentEmails.refreshSentEmailList();
        this.props.navigation.setParams({ isSearch: true })
    }

    
    render() {      
        const {initialTab} = this.state;  

        return <ScrollableTabView
            onChangeTab={(tab) => 
                (tab.i == 1) ? this.refreshSentEmailsList()
                : 
                (tab.i == 2) ? this.refreshDraftEmailsList() : this.props.navigation.setParams({ isSearch: false })}
            style={{ marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.01 }}
            initialPage={initialTab}
            tabBarTextStyle={{ fontSize: globals.font_16 }}
            tabBarInactiveTextColor={colors.lightGray}
            tabBarActiveTextColor={colors.warmBlue}
            ref={(tabView) => { this.tabView = tabView }}
            renderTabBar={() => <ScrollableTabBar isFrom={'Profile'}/>}
        >
            <ComposeEmails tabLabel={'Compose Emails'} navigationProps={this.props} />
            <SentEmails tabLabel={'Sent Emails'} navigationProps={this.props} isfrom={"sentEmails"}/>
            <DraftEmails tabLabel={'Draft Emails'} navigationProps={this.props} />
            <StatisticalAnalysis tabLabel={'Statistical Analysis' + "  "} navigationProps={this.props} />
        </ScrollableTabView>
    }
}
