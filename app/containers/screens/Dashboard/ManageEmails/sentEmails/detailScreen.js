import React from 'react';
import { Text, View,  Alert, Image, FlatList, ScrollView} from 'react-native';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as images from '../../../../../assets/images/map';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment'
import HTML from 'react-native-render-html';
import { API } from '../../../../../utils/api';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import styles from './style';

const TAG = '==:== EmailDetail Screen : ';
let _this;


class EmailDetails extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Manage Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
      });

      constructor(props) {
        super(props)
        _this=this      
        this.state = {
            emailData: this.props.navigation.state.params.item,
            loading: false,
            mailID: this.props.navigation.state.params.item.ID,
            AttachementData: [],
            Mitem_1: [],
        };
      };

/**
* API call of getAttachmentEmailApiCall
* 
*/
      componentDidMount(){
        this.props.showLoader();
        this.setState({loading: true})
        if (globals.isInternetConnected === true) {
            API.GetAttachmentPoco(this.GetAttachmentPocoResponseData, true, this.state.mailID);
        } else {
            this.props.hideLoader()
            this.setState({loading: false})
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
      }

      /**
* callback response of GetAttachmentPocoResponseData
* 
*/
GetAttachmentPocoResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetAttachmentPocoResponseData-> success : ',
                response
            );
            const item_1 = response.m_Item1;
            this.setState({AttachementData: response, Mitem_1: item_1})
            this.setState({loading: false})
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader()
            this.setState({loading: false})
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK'}],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'GetAttachmentPocoResponseData-> ERROR : ',
                JSON.stringify(err.message)
            );
        },
        complete: () => {
            this.props.hideLoader()
            this.setState({loading: false})
            console.log("GetAttachmentPocoResponseData-> complete");
        },
    };


    renderItemList(item, index){
        const isFileName = globals.checkObject(item, "FileName");
        const isMimeType = globals.checkObject(item, "MimeType");
        const isFileSize = globals.checkObject(item, "FileSize");
        const isFileContent = globals.checkObject(item, "FileContent")
        const FileName = (isFileName) ? item.FileName : '-';
        return(
            <View>
                <View style={[styles.detail_OpacityStyle,{marginBottom: globals.screenHeight * 0.018}]}>
                    <View style={styles.detail_OpacityStyle}>
                        <View style={styles.detail_attchIconViewContainer}>
                            <Image
                            source={images.ManageEmails.Attch_white} style={styles.detail_attchIconStyle} resizeMode={"contain"}/>
                            <Text numberOfLines={1} style={styles.detail_fileNametextStyle}>{FileName}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

      
      render(){
          const {  AttachementData, Mitem_1 } = this.state;
          const isSubject = globals.checkObject(AttachementData.m_Item2,"EmailSubject");
          const istoEmail = globals.checkObject(AttachementData.m_Item2, "ToEmail");
          const isScheduledAtStr = globals.checkObject(AttachementData.m_Item2, "ScheduledAtStr");
          const isEmailBody = globals.checkObject(AttachementData.m_Item2,"EmailBody");
          const isFromAddress = globals.checkObject(AttachementData.m_Item2, "FromAddress")
          const EmailSubject = (isSubject) ?  AttachementData.m_Item2.EmailSubject : '-';
          const EmailBody = (isEmailBody) ?  AttachementData.m_Item2.EmailBody : '-';
          const ScheduledAtStr = (isScheduledAtStr) ?  AttachementData.m_Item2.ScheduledAtStr : '-';
          const ToEmail = (istoEmail) ? AttachementData.m_Item2.ToEmail : '-';
          const FromAddress = (isFromAddress) ? AttachementData.m_Item2.FromAddress : '-';
          let time =(isScheduledAtStr) ? moment(new Date(ScheduledAtStr)).format(" MM/DD/YYYY HH:mm:ss A") : "-";

          return(
              <View style={styles.mainParentStyle}>
                  <View style={[styles.backgroundStyle]}>
                      <Text style={styles.detail_mainTextStyle}>Sent Emails</Text>
                  </View>
                  <View style={styles.detail_MainViewContainer}></View>
                  <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                      <View style={styles.detail_childViewContainer}>
                          <View style={styles.detail_ViewStyle}>
                              <View style={styles.detail_subjectViewContainer}>
                                  <Text numberOfLines={1} style={styles.detail_subjectTextstyle}>
                                      {EmailSubject}
                                  </Text>
                                  <View style={styles.detail_emailViewStyle}>
                                      <Text style={styles.detail_toTextStyle}>To:
                                  <Text numberOfLines={1} style={styles.detail_emailTextStyle}>
                                              {` ${ToEmail}`}
                                          </Text>
                                      </Text>
                                  </View>
                                  <Text numberOfLines={1} style={styles.detail_timeTextStyle}>
                                      {time}
                                  </Text>
                                  {/* <Text numberOfLines={1} style={{
                                  color: colors.Gray,
                                  fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
                              }}>
                                  {EmailBody}
                              </Text> */}
                                  <HTML html={EmailBody} baseFontStyle={styles.detail_toTextStyle} />
                                  {(Mitem_1.length > 0 && Mitem_1 !== null) ?
                                      <FlatList
                                          showsVerticalScrollIndicator={false}
                                          data={Mitem_1}
                                          renderItem={({ item, index }) => this.renderItemList(item, index)}
                                          extraData={this.state}
                                          bounces={false}
                                          keyExtractor={(index, item) => item.toString()}
                                      />
                                      :
                                      null
                                  }
                              </View>
                          </View>
                      </View>
                  </ScrollView>
              </View> 
          )
      }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EmailDetails);