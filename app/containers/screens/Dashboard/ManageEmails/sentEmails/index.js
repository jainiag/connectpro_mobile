import React from 'react';
import { Text, View, TextInput, TouchableOpacity, Alert, Modal, FlatList,  ActivityIndicator, Platform} from 'react-native';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import moment from 'moment'
import { API } from '../../../../../utils/api';
import { Dropdown } from '../../../../../libs/react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';
import styles from './style';
import AntDesign from 'react-native-vector-icons/AntDesign'
import KeyboardListener from 'react-native-keyboard-listener';
import Nointernet from '../../../../../components/NoInternet/index'
import ServerError from '../../../../../components/ServerError/index'

const TAG = '==:== SentEmails : ';
let _this;
const iPad = DeviceInfo.getModel();
class SentEmails extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Sent Emails'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
      });

    constructor(props){
        super(props);
        _this = this;
        this.state = {
            emailSubject: '',
            toEmail: '',
            scheduledAt: '',
            drawValue: 1,
            data: [
                {
                    ID: 1,
                    value: "Queue",
                },
                {
                    ID: 2,
                    value: "Sent",
                },
                {
                    ID: 3,
                    value: "Reject",
                },
                {
                    ID: 4,
                    value: "Error",
                },
            ],
            isShowSearch: false,
            emailListData: [],
            loading: false,
            emailStatus: '',
            pageStart: 0,
            loadingPagination: false,
            keyboardOpen: false,
            serverErr: false,
            isInternetFlag: true,
            responseComes: false,
        }
    }

    componentDidMount(){
        this.props.showLoader()
        this.setState({loading: true, loadingPagination: true})
        this.GetMailinfoApiCall();
    }

    static refreshSentEmailList(){
        _this.props.showLoader()
        _this.setState({loading: true, loadingPagination: true})
        _this.GetMailinfoApiCall();
    }

/**
* API call of getMailSentInformation
* 
*/
    GetMailinfoApiCall(){
        const { emailSubject, toEmail, scheduledAt, drawValue, emailStatus, pageStart } = this.state;
        const dateFormate = moment(new Date(scheduledAt)).format('MM/DD/YYYY')
        if (emailSubject !== "") {
            this.setState({ drawValue: 2 })
        } else if (toEmail !== "") {
            this.setState({ drawValue: 3 })
        } else if (scheduledAt !== "") {
            this.setState({ drawValue: 7 })
        } else if (emailStatus !== "") {
            this.setState({ drawValue: 8 })
        } else {
            this.setState({ drawValue: 1 })
        }
        let data = {
            "Draw": drawValue,
            "Columns": [
              {
                "data": "RowIndex", "name": "", "searchable": false, "orderable": false, "search": {"value": "", "regex": false}
              },
              {
                "data": "EmailSubject", "name": "", "searchable": true, "orderable": true, "search": {"value": emailSubject, "regex": false}
              },
              {
                "data": "ToEmail", "name": "", "searchable": true, "orderable": true, "search": {"value": toEmail, "regex": false}
              },
              {
                "data": "ScheduledAt", "name": "", "searchable": true, "orderable": true, "search": {"value": dateFormate, "regex": false}
              },
              {
                "data": "SentStatus", "name": "", "searchable": true, "orderable": true, "search": {"value": emailStatus, "regex": false}
              },
              {
                "data": "ID", "name": "", "searchable": false, "orderable": false, "search": {"value": "", "regex": false}
              }
            ],
            "Order": [
              {
                "Column": 3,
                "Dir": "desc"
              }
            ],
            "Start": pageStart,
            "Length": 10,
            "Search": {
              "Value": "string",
              "Regex": false
            }
          }
        if (globals.isInternetConnected === true) {
            this.setState({loadingPagination: true,  isInternetFlag: true })
            API.GetMailSentInformation(this.getMailSentInformationResponse, data, globals.userID, true);
        } else {
            this.setState({loading: false, isInternetFlag: false })
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


/**
* callback response of getMailSentInformationResponse
* 
*/
    getMailSentInformationResponse = {
        success: response => {
            console.log(
                TAG,
                'getMailSentInformationResponse-> success : ',
                response
            );
            if (response.StatusCode == 200) {
                const {emailListData, pageStart} = this.state;
               
                const listData = response.Data.data;
                // this.setState({
                //         loading: false, loadingPagination: false,
                //         emailListData: listData,
                //     })
                if (pageStart == 0) {
                    this.setState({
                        loading: false, loadingPagination: false,
                        emailListData: response.Data.data,
                    })
                } else {
                    this.setState({
                        loading: false, loadingPagination: false,
                        emailListData: [...emailListData, ...response.Data.data],
                    })
                }
                this.setState({emailSubject: '',toEmail: '',scheduledAt: '',emailStatus: ''})
            } else {
                Alert.alert(globals.appName, response.Message)

            }
            this.setState({loading: false})
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader()
            this.setState({loading: false, serverErr: true,  })
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK'}],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'getMailSentInformationResponse-> ERROR : ',
                JSON.stringify(err.message)
            );
        },
        complete: () => {
            this.props.hideLoader()
            this.setState({loading: false})
            console.log("getMailSentInformationResponse-> complete");
        },
    };

    handleLoadMore()  {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageStart: this.state.pageStart + 10,
            }, () => {
                this.GetMailinfoApiCall();
            })
            this.onEndReachedCalledDuringMomentum = true;
        }
    };

    /**
     * dropdown onChangeText method
     */
    onChangeText(text, index, data) {
        return (
            this.setState({ emailStatus: text })
        )
    }

    clearTextFields(){
        this.setState({emailSubject: "", toEmail: "", scheduledAt: "", drawValue: 2, emailStatus: ""})
    }

/**
* API call of triggerMailsinQueueStatus
* 
*/
    triggerQueueApicall(){
        this.setModalVisible(false)
        this.props.showLoader();
        this.setState({loading: true})
        if (globals.isInternetConnected === true) {
            API.TriggerMailsinQueueStatus(this.triggerMailsinQueueStatusResponse, true);
        } else {
            this.props.hideLoader();
            this.setState({loading: false})
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

/**
* callback response of triggerMailsinQueueStatusResponse
* 
*/
    triggerMailsinQueueStatusResponse = {
        success: response => {
            console.log(
                TAG,
                'triggerMailsinQueueStatusResponse-> success : ',
                response
            );
            if (response.StatusCode == 200) {
                this._resetSentMails()
                Alert.alert(globals.appName, `Total Jobs in Queue : ${response.Data}`)
            } else {
                Alert.alert(globals.appName, response.Message)

            }
            this.props.hideLoader();
            this.setState({loading: false})
        },
        error: err => {
            this.setState({loading: false})
            this.props.hideLoader();
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK'}],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'triggerMailsinQueueStatusResponse-> ERROR : ',
                JSON.stringify(err.message)
            );
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({loading: false})
            console.log("triggerMailsinQueueStatusResponse-> complete");
        },
    };

    /**
     * Method for change modal state
     * @param {*} visible 
     */
    setModalVisible(visible) {
        this.setState({ isShowSearch: visible });
    }

    static openSearchModal(){
        _this.setState({ isShowSearch: true });
    }

    closeModal(){
        this.setState({
            emailSubject: '',
            toEmail: '',
            scheduledAt: '',
            emailStatus: ''
        })
        this.setModalVisible(false)
    }

    _searchApicall(){
        const { emailStatus, emailSubject, toEmail, scheduledAt } = this.state;
        const dateFormate = moment(new Date(scheduledAt)).format('DD/MM/YYYY')
        const status = emailStatus.charAt(0)

        this.setState({
            emailStatus: status,
            emailSubject: emailSubject,
            toEmail: toEmail,
            scheduledAt: scheduledAt,
            pageStart: 0,
        },() => {
            this.setModalVisible(false)  
            this.searchEmail();  
        })
    }

    /**
     * Method of get  communities based on search filter
     */
    searchEmail() {
        _this.makeListFresh(() => {
            _this.props.showLoader()
            _this.setState({loading: true})
            _this.GetMailinfoApiCall();
        });
    }

    makeListFresh(callback) {
        this.setState(
            {
                emailListData: [],
                loading: false,
            },
            () => {
                callback();
            }
        );
    }

    _resetSentMails(){
        this.clearTextFields();
        let data = {
            "Draw": 1,
            "Columns": [
              {
                "data": "RowIndex", "name": "", "searchable": false, "orderable": false, "search": {"value": "", "regex": false}
              },
              {
                "data": "EmailSubject", "name": "", "searchable": true, "orderable": true, "search": {"value": "", "regex": false}
              },
              {
                "data": "ToEmail", "name": "", "searchable": true, "orderable": true, "search": {"value": "", "regex": false}
              },
              {
                "data": "ScheduledAt", "name": "", "searchable": true, "orderable": true, "search": {"value": "", "regex": false}
              },
              {
                "data": "SentStatus", "name": "", "searchable": true, "orderable": true, "search": {"value": "", "regex": false}
              },
              {
                "data": "ID", "name": "", "searchable": false, "orderable": false, "search": {"value": "", "regex": false}
              }
            ],
            "Order": [
              {
                "Column": 3,
                "Dir": "desc"
              }
            ],
            "Start": this.state.pageStart,
            "Length": 10,
            "Search": {
              "Value": "string",
              "Regex": false
            }
          }
        this.setModalVisible(false)
        this.props.showLoader()
        if (globals.isInternetConnected === true) {
            this.setState({loading: true, loadingPagination: true})
            API.GetMailSentInformation(this.getMailSentInformationResponse, data, globals.userID, true);
        } else {
            this.setState({loading: false})
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.GetMailinfoApiCall();
        });

    }


    renderSearchModel() {
        const { emailSubject, scheduledAt, emailListData, toEmail, emailStatus, data } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={styles.mainParentStyle}>
                    <KeyboardListener
                        onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                        onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                    <View style={styles.cross_iconStyle}>
                        <AntDesign
                            name="closecircleo"
                            size={(globals.iPhoneX) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.035}
                            color={colors.warmBlue}
                            onPress={() => this.closeModal()}
                            style={{ alignSelf: 'flex-end' }}
                        />
                    </View>
                    <View style={styles.textInputViewContainer2}>
                        <TextInput
                            maxLength={300}
                            style={[styles.textInputStyleContainer,{marginVertical: (Platform.OS == 'ios') ?globals.screenHeight * 0.014 : null}]}
                            placeholder={"Search by Email Subject"}
                            placeholderTextColor={colors.lightGray}
                            onChangeText={text => this.setState({ emailSubject: text })}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            value={emailSubject}
                            autoCapitalize="none"
                            ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                        />
                    </View>
                    <View style={styles.textInputViewContainer2}>
                        <TextInput
                            maxLength={300}
                            style={[styles.textInputStyleContainer,{marginVertical: (Platform.OS == 'ios') ?globals.screenHeight * 0.014 : null}]}
                            placeholder={"Search by To Email"}
                            placeholderTextColor={colors.lightGray}
                            onChangeText={text => this.setState({ toEmail: text })}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            value={toEmail}
                            autoCapitalize="none"
                            ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                        />
                    </View>
                    <View style={[styles.datePickerViewStyle, { marginBottom: ( iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 15 : null }]}>
                        <DatePicker
                            style={styles.pickerStyle}
                            placeholder={"Search by Scheduled At"}
                            androidMode="spinner"
                            showIcon={false}
                            date={scheduledAt}
                            mode="date"
                            format="MM/DD/YYYY"
                            confirmBtnText={globals.MESSAGE.SEARCH_SCREEN.SEARCH_CONFIRM_DATE}
                            cancelBtnText={globals.MESSAGE.SEARCH_SCREEN.SEARCH_CANCEL_DATE}
                            customStyles={{
                                placeholderText: [styles.placeHolderTextStyle],
                                dateIcon: [styles.dateIconStyle],
                                dateInput: [styles.startDateInputStyle],
                            }}
                            onDateChange={date => this.setState({ scheduledAt: date })}
                        />
                    </View>
                    <Dropdown
                        containerStyle={[styles.dropdownContainer, (emailStatus == "") ? { height: globals.screenHeight * 0.065, paddingBottom: globals.screenHeight * 0.02, justifyContent: 'center', } : null]}
                        fontSize={globals.font_14}
                        itemColor={colors.black}
                        textColor={colors.black}
                        label='--Select Emails Status--'
                        data={data}
                        onChangeText={(text, index, data) => this.onChangeText(text, index, data)}
                    />
                    <View style={styles.buttons_viewContainer}>
                        <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this.triggerQueueApicall()}>
                            <View style={styles.lastImgViewEnd}>
                                <View style={styles.connectedBtnView}>
                                    <Text style={styles.connectTextStyle}>Trigger Queued Emails(s)</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this._resetSentMails()}>
                            <View style={styles.lastImgViewEnd}>
                                <View style={styles.connectedBtnView2}>
                                    <Text style={styles.connectTextStyle}>Reset</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {(Platform.OS == "android") ?
                        (this.state.keyboardOpen == false) ?
                            <TouchableOpacity style={styles.toSearchStyle} onPress={() => this._searchApicall()}>
                                <Text style={styles.tvSearchStyle}>{'SEARCH'}</Text>
                            </TouchableOpacity>
                            :
                            null
                        :
                        <TouchableOpacity style={styles.toSearchStyle} onPress={() => this._searchApicall()}>
                            <Text style={styles.tvSearchStyle}>{'SEARCH'}</Text>
                        </TouchableOpacity>
                    }
                </View>
            </Modal>
        );
    }

    renderItemUI(item, index) {
        const istoEmail = globals.checkObject(item, 'ToEmail');
        const isScheduledAtStr = globals.checkObject(item, 'ScheduledAtStr')
        const isEmailSubject = globals.checkImageObject(item, 'EmailSubject');
        const isEmailBody = globals.checkObject(item, 'EmailBody')
        const isSentStatus = globals.checkObject(item, 'SentStatus')
        const SentStatus = (isSentStatus) ? item.SentStatus : '-';
        const statusSent = (item.SentStatus == "Q") ? "Queue" : (item.SentStatus == "S") ? "Sent" : (item.SentStatus == "R") ? "Reject" : (item.SentStatus == "E") ? "Error" : '-'
        const EmailSubject = (isEmailSubject) ? item.EmailSubject : '-';
        const EmailBody = (isEmailBody) ? item.EmailBody : '-';
        const ScheduledAtStr = (isScheduledAtStr) ? item.ScheduledAtStr : '-';
        let time = moment(new Date(ScheduledAtStr)).format("h:mm A")

        return (
            <TouchableOpacity onPress={() => this.props.navigationProps.navigation.navigate("EmailDetails",{item: item})}>
                <View style={styles.matches_ChildViewContainer}>
                    <View style={styles.userinfoStyle}>
                        <View style={styles.list_textViewContainer}>
                            <Text numberOfLines={1} style={styles.list_toEmailTextStyle}>
                                {(istoEmail) ? item.ToEmail : '-'}
                            </Text>
                            <Text numberOfLines={1} style={styles.list_commanTextStyle}>
                                {`(Subject : ${item.EmailSubject})`}
                            </Text>
                            <View style={styles.list_viewContainer}>
                                <Text numberOfLines={1} style={styles.list_commanTextStyle}>{EmailBody}</Text>
                                <Text numberOfLines={1} style={styles.list_commanTextStyle}>{time}</Text>
                            </View>
                            <Text numberOfLines={1} style={styles.list_commanTextStyle}>
                                {`(Status : ${statusSent})`}
                            </Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    renderFooter = () => {
        if (this.state.loadingPagination == true) {
            return (
             
                        <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
                    
            )
        } else {
            return (
                <View />
            )
        }
    };
    
    render() {        
        const { emailListData, loading, serverErr, isInternetFlag} = this.state;
        return (
            <View style={styles.mainParentStyle}>
                {this.renderSearchModel()}
                {(!isInternetFlag) ?
                    <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                    (serverErr === false) ?
                        (loading == true) ? 
                        <View style={globalStyles.nodataStyle}></View>
                            :
                            (emailListData.length == 0 || emailListData == null) ?
                                <View style={globalStyles.nodataStyle}>
                                    <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.MANAGE_EMAILS.SENT_EMAIL_NOT_AVLBL}</Text>
                                </View> :
                                <View style={styles.mainParentStyle}>
                                    <View style={styles.backgroundSentViewStyle}></View>
                                    <View style={styles.matchesMainView}></View>
                                    <FlatList
                                        style={{ flex: 1, marginTop: globals.screenHeight * 0.03 }}
                                        showsVerticalScrollIndicator={false}
                                        data={emailListData}
                                        renderItem={({ item, index }) => this.renderItemUI(item, index)}
                                        extraData={this.state}
                                        bounces={false}
                                        keyExtractor={(index, item) => item.toString()}
                                        ListFooterComponent={this.renderFooter}
                                        onEndReached={() => this.handleLoadMore()}
                                        onEndReachedThreshold={0.5}
                                        onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                    />
                                </View>
                        :
                        <ServerError loading={loading} onPress={() => this._tryAgain()} />
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SentEmails);