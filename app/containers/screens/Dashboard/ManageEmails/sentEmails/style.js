import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
    mainParentStyle: {
        flex: 1,
    },
    loaderbottomview: {
        bottom: (Platform.OS == 'android') ?
          globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
      },
    textInputViewContainer2: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: 25,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
    },
    textInputStyleContainer: {
        flex: 1,
        marginHorizontal: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        // marginVertical: globals.screenHeight * 0.014,
    },
    dropdownContainer: {
        borderWidth: 1,
        borderColor: colors.proUnderline,
        marginHorizontal: 25,
        // height: 50,
        marginTop: globals.screenHeight * 0.025,
        borderRadius: 5,
        // justifyContent: 'center',
        // paddingBottom: 15,
    },
    toSearchStyle: {
        position: 'absolute',
        bottom: 0,
        height: globals.screenHeight * 0.0662,
        backgroundColor: colors.bgColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: globals.screenWidth,
    },
    tvSearchStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    },
    lastImgViewEnd: {
        justifyContent: 'center', alignItems: 'flex-end',
    },
    connectedBtnView: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.listSelectColor,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.1,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.34 : globals.screenWidth * 0.45,
        borderRadius: 5,
        marginHorizontal: 25,
    },
    connectedBtnView2: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.listSelectColor,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.1,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.15 : globals.screenWidth * 0.2,
        borderRadius: 5,
        marginHorizontal: 25,
    },
    connectTextStyle: {
        color: colors.white,
        fontSize: globals.font_13
    },
    datePickerViewStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 15 : 20,
    },
    pickerStyle: {
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.935 : globals.screenWidth * 0.87,
    },
    placeHolderTextStyle: {
        color: colors.lightGray,
        fontSize: globals.font_14,
        textAlign: 'left',
        marginRight: iPad.indexOf('iPad') != -1 ? globals.screenWidth * 0.55 : DeviceInfo.isTablet() ? globals.screenWidth * 0.62 : globals.screenWidth * 0.34,
    },
    startDateInputStyle: {
        borderRadius: 5,
        borderColor: colors.proUnderline,
        marginTop: 10,
        marginBottom: 10,
        height: iPad.indexOf('iPad') != -1 ? globals.screenHeight * 0.056 : DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : (Platform.OS == 'android') ? globals.screenHeight * 0.065 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenHeight * 0.07 : globals.screenHeight * 0.06,
    },
    dateIconStyle: {
        position: 'absolute',
        left: 0,
        marginLeft: 0,
    },
    tvTitleStyle: {
        fontSize: globals.font_20,
        color: 'blue',
    },
    backgroundStyle: {
        width: globals.screenWidth,
        height: globals.screenHeight * 0.20,
        backgroundColor: colors.warmBlue,
        alignItems: 'center'
    },
    backgroundSentViewStyle: {
        width: globals.screenWidth,
        height: globals.screenHeight * 0.17,
        backgroundColor: colors.warmBlue,
        alignItems: 'center'
    },
    headertextStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_16,
        fontWeight: '500',
        paddingVertical: globals.screenHeight * 0.03,
        textAlign: 'center'
    },

    // userinfoStyle: {
    //     flexDirection: 'row',
    //     //marginHorizontal: globals.screenWidth * 0.03,
    //     backgroundColor: colors.white,
    //     paddingHorizontal: globals.screenWidth * 0.02,
    //     paddingVertical: globals.screenWidth * 0.025,
    // },
    beforeimageview: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        marginVertical: 5,
        borderRadius: (globals.screenWidth * 0.15) / 2,
        borderWidth: 0.2,
        borderColor: colors.lightGray,
        marginLeft: (globals.screenWidth * 0.03)
    },
    textViewStyle: {
        marginHorizontal: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        flex: 1
    },

    textViewStyleSwipeOut: {
        alignContent: 'center',
        marginLeft: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        width: "70%",
    },

    proffesionTextStyle: {
        color: colors.lightBlack,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
    },
    grayLineView: {
        height: 1,
        alignSelf: 'center',
        width: "89%",
        backgroundColor: colors.proUnderline,
        marginVertical: globals.screenHeight * 0.01
    },
    hobbytextStyle: {
        //marginVertical:globals.screenWidth * 0.02,
        marginHorizontal: globals.screenWidth * 0.055,
        paddingHorizontal: globals.screenWidth * 0.02,
        paddingVertical: globals.screenWidth * 0.025,
        marginBottom: globals.screenWidth * 0.028,
    },
    hobbyTextstyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 21 : globals.font_14,
        color: colors.matteBlack
    },
    sideimgStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.04,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.04,
        position: 'absolute',
        top: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.18 : globals.screenHeight * 0.12,
        left: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? -24 : -16,
    },
    headerStyles: {
        marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.015 : 0
    },

    imgcontainer: {
        alignItems: 'center', justifyContent: 'center', flex: 1
    },
    imgesStyle: {
        alignSelf: 'center',
        height: globals.screenHeight * 0.03,
        width: globals.screenHeight * 0.03
    },
    swipeOutView: {
        flexDirection: 'row',
        backgroundColor: colors.white,
    },
    leftGreenLine: {
        backgroundColor: colors.greenDone,
        width: globals.screenWidth * 0.015555,
    },
    rightRedLine: {
        backgroundColor: colors.darkRed,
        width: globals.screenWidth * 0.015555,

    },
    crossimgesStyle: {
        alignSelf: 'center',
        height: globals.screenHeight * 0.02,
        width: globals.screenHeight * 0.02
    },
    lastStatusMainView: {
        justifyContent: 'center', alignItems: 'flex-end', marginRight: 10, borderRadius: 3,
    },
    lastStatusView: {
        height: globals.screenHeight * 0.05, width: globals.screenWidth * 0.2, backgroundColor: colors.greenDone, justifyContent: 'center', alignItems: 'center', borderRadius: 3
    },
    statusTextStyle: {
        marginHorizontal: 3, color: colors.white, fontSize: globals.font_13
    },
    lastDateMainView: {
        flex: 1, flexDirection: 'row', marginTop: 10, alignItems: 'center'
    },
    clockImgStyle: {
        height: globals.screenHeight * 0.02, width: globals.screenHeight * 0.02
    },
    createdDateTxt: {
        fontSize: globals.font_12, color: colors.lightBlack, marginLeft: 5
    },
    matches_ChildViewContainer: {
        backgroundColor: colors.white,
        width: globals.screenWidth * 0.85,
        //  flex: 1,
        // height:globals.screenWidth * 0.85,
        marginLeft: globals.screenWidth * 0.074,
        borderWidth: 0.7,
        borderColor: colors.gray,
        borderRadius: 5,
        marginBottom: globals.screenHeight * 0.02,
        paddingBottom: globals.screenHeight * 0.02,
    },
    matchesMainView: {
        alignItems: 'center',
        marginTop: globals.screenHeight * (-0.12),
        zIndex: 999,
        // backgroundColor:'red',
        top: globals.screenHeight * 0.08,

    },

    userinfoStyle: {
        flexDirection: 'row',
        marginHorizontal: globals.screenWidth * 0.03,
        // backgroundColor: "red",
        paddingHorizontal: globals.screenWidth * 0.02,
        paddingVertical: globals.screenWidth * 0.025,
    },
    beforeimageview: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        borderRadius: (globals.screenWidth * 0.15) / 2,
        borderWidth: 0.2,
        borderColor: colors.lightGray
    },
    imgStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
        borderRadius: (globals.screenWidth * 0.15) / 2,
        borderWidth: 0.2,
        borderColor: colors.lightGray
    },
    nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },

    nodataTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
    },
    usernametextStyle: {
        //textAlign: 'center',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
    },
    textViewStyle: {
        alignContent: 'center',
        marginLeft: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        width: "70%"
    },
    serverErrViewContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    serverTextStyle: {
        textAlign: 'center',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
        marginBottom: 15,
    },
    serverButtonStyle: {
        marginTop: 15,
    },
    cross_iconStyle: {
        marginTop: (globals.iPhoneX) ? globals.screenHeight * 0.04 : globals.screenHeight * 0.03, 
        marginRight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.027 : globals.screenWidth * 0.06,
    },
    buttons_viewContainer: {
        flexDirection: 'row', 
        alignItems: 'center', 
        marginTop: globals.screenHeight * 0.025, 
        justifyContent: 'space-between',
    },
    list_textViewContainer: {
        alignContent: 'center',
        marginVertical: globals.screenWidth * 0.025,
        width: "100%"
    },
    list_toEmailTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16, color: colors.darkSkyBlue
    },
    list_commanTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14, color: colors.Gray,
    },
    list_viewContainer: {
        flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'
    },
    loadingIndicatorViewStyle: {
        justifyContent: 'center', 
        alignItems: 'center', 
        flex: 1
    },
    loadingIndicatorStyle: {
        justifyContent: 'space-between', 
        alignItems: 'center'
    },

    // ======: email detail screen : ======

    detail_MainViewContainer: {
        alignItems: 'center',
        marginTop: globals.screenHeight * (-0.09),
        zIndex: 999,
        // backgroundColor:'red',
        top: globals.screenHeight * 0.08
    },
    detail_childViewContainer: {
        backgroundColor: colors.white,
        width: globals.screenWidth * 0.85,
        //  flex: 1,
        // height:globals.screenWidth * 0.85,
        marginLeft: globals.screenWidth * 0.074,
        borderWidth: 0.7,
        borderColor: colors.gray,
        borderRadius: 5,
        marginBottom: globals.screenHeight * 0.02,
        paddingBottom: globals.screenHeight * 0.02,
    },
    detail_ViewStyle: {
        flexDirection: 'row',
        paddingHorizontal: globals.screenWidth * 0.02,
       paddingVertical: globals.screenWidth * 0.025,
    },
    detail_subjectViewContainer: {
        marginHorizontal: globals.screenWidth * 0.035,
        marginVertical: globals.screenWidth * 0.025,
        flex: 1,
    },
    detail_subjectTextstyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_15,
    },
    detail_emailViewStyle: {
        marginTop: 3, marginBottom: 5
    },
    detail_toTextStyle: {
        color: colors.Gray,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    },
    detail_emailTextStyle: {
        color: colors.darkSkyBlue,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    },
    detail_timeTextStyle: {
        color: colors.Gray,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
        marginBottom: 5
    },
    detail_mainTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_15, 
        color: colors.white, 
        alignSelf: 'flex-start', 
        marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : globals.screenHeight * 0.03, 
        marginLeft: globals.screenWidth * 0.072, 
        marginBottom: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.06 : globals.screenHeight * 0.05,
    },
    detail_OpacityStyle: {
        justifyContent: 'flex-start',
        alignItems: "flex-start",
    },
    detail_attchIconViewContainer: {
        alignItems: 'center',
        justifyContent: 'space-evenly',
        backgroundColor: colors.listSelectColor,
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.07 : globals.screenWidth * 0.1,
        width:  iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? "100%" : '100%',
        borderRadius: 3,
        flexDirection: 'row' 
    },
    detail_attchIconStyle: {
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.screenWidth * 0.05, 
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.screenWidth * 0.05,
    },
    detail_fileNametextStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
        width: globals.screenWidth * 0.6,
    },
    renderFooterViewStyle: {
        flex: 1,
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 999,
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    renderFooter_childViewStyle: {
        marginBottom: (globals.screenHeight * 0.05), 
        alignItems: "center", 
        justifyContent: 'center',
    },
});

