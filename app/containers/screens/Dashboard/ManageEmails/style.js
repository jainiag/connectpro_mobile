import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';


module.exports = StyleSheet.create({
    mainParentStyle:{
        flex: 1,
        marginTop:globals.screenHeight * 0.02,
    },  
    tvTitleStyle:{
        fontSize: globals.font_20,
        color:'blue',        
    }
});
