import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ActivityIndicator, TextInput, Alert, Modal, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import { NavigationEvents } from 'react-navigation';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import { API } from '../../../../../utils/api';
import Icon from 'react-native-vector-icons/EvilIcons';
import { connect } from 'react-redux';
import Swipeable from 'react-native-swipeable';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import Entypo from 'react-native-vector-icons/Entypo';
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../../components/NoInternet'
import ServerError from '../../../../../components/ServerError/index'
import KeyboardListener from 'react-native-keyboard-listener';

const iPad = DeviceInfo.getModel();
let TAG = "Groups Listing Screen ::==="
let _this = null;
class Groups extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      headerLeft:
        globals.ConnectProbackButton(navigation, 'My Groups'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
      headerRight: (
        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity
            onPress={() => { _this.setModalVisible(true); _this.setState({ groupName: '' }) }}
            style={{
              marginRight: globals.screenWidth * 0.04,
              alignItems: 'center',
              marginTop:
                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                  ? globals.screenHeight * 0.03
                  : null,
            }}
          >
            <Image
              source={images.headerIcon.searchIcon}
              resizeMode={"contain"}
              style={{
                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                tintColor: colors.warmBlue,
                alignSelf: 'center',
              }}
            />
          </TouchableOpacity>
        </View>
      ),
    }
  };


  constructor(props) {
    super(props);
    _this = this;
    console.log(TAG, props);
    this.state = {
      groupsData: [],
      loading: false,
      isInternetFlag: true,
      loadingPagination: false,
      serverErr: false,
      isShowSearch: false,
      responseComes: false,
      pageNumber: 1,
      pageSize: 10,
      groupName: '',
      totalGroups: 0,
      groupID: '',
      action: '',
      onSwipOutCancel: false,
      leavecommunityModal: false,
      joincommunityModal: false,
      cancelModalAproval: false,
      onSwipOutAccept: false,
      keyboardOpen: false,
    }
  }



  static UpdatedData() {
    _this && _this.apicallGetGroupsList();
  }



  componentDidMount() {
    this.props.showLoader()
    this.apicallGetGroupsList()
  }

  /**
   * prepareData when call the API
   */
  prepareData() {
    const {
      pageNumber, pageSize, groupName
    } = this.state;
    const data = {};
    data.UserID = globals.userID
    data.GroupName = groupName
    data.PageNumber = pageNumber;
    data.PageSize = pageSize;
    return data;
  }

  /**
   * APi call for Get GroupsList
   */
  apicallGetGroupsList() {
    if (globals.isInternetConnected === true) {
      this.setState({ loadingPagination: true, loading: true, isInternetFlag: true, })
      API.getMyGroupData(this.allGroupsDataResponseData, this.prepareData(), true)
    } else {
      this.props.hideLoader();
      this.setState({ isInternetFlag: false })
    }
  }

  /**
* Response of allGroupsDataResponseData
*/
  allGroupsDataResponseData = {
    success: response => {
      console.log(
        TAG,
        'allGroupsDataResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        total = response.Data.TotalRecords;
        if (this.state.pageNumber == 1) {
          this.setState({
            loading: false, responseComes: true, loadingPagination: false,
            groupsData: response.Data.UserGroups,
          })
        } else {
          this.setState({
            loading: false, responseComes: true, loadingPagination: false,
            groupsData: [...this.state.groupsData, ...response.Data.UserGroups],
          })
        }
      } else {
        Alert.alert(globals.appName, response.Message)
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ serverErr: true, loading: false });
      console.log(
        TAG,
        'allGroupsDataResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403 || err.StatusCode == 500) {
        this.setState({ loading: false, loadingPagination: false })
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
*  call when _sessionOnPres
*/
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  _tryAgain() {
    this.setState({ serverErr: false, responseComes: false }, () => {
      this.props.showLoader();
      this.apicallGetGroupsList();
    });

  }

  renderFooter = () => {
    if (this.state.loadingPagination && this.state.responseComes) {
      return (
        
            <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
         
      )
    } else {
      return (
        <View />
      )
    }
  };


  clearStates() {
    this.setState({
      groupsData: [],
      loading: false,
      loadingPagination: false,
      serverErr: false,
      isShowSearch: false,
      responseComes: false,
      pageNumber: 1,
      leavecommunityModal: false,
      cancelModalAproval: false,
      pageSize: 10,
      groupName: '',
      totalGroups: 0,
      groupID: '',
      action: '',
      onSwipOutCancel: false,
      onSwipOutAccept: false,
    }, () => {
      this.apicallGetGroupsList()
    })
  }




  /**
   * Method of render conditions according views in member lisr
   * @param {*} item 
   * @param {*} index 
   */
  renderGroupList(item, index) {
    const isGroupName = globals.checkObject(item, 'GroupName');
    const isCommunityLogoPath = globals.checkImageObject(item, 'CommunityLogoPath');
    const isMembersCount = globals.checkObject(item, 'MemberCount');
    const isGroupTypeID = globals.checkObject(item, 'GroupTypeID');
    const isCommunityName = globals.checkObject(item, 'CommunityName')
    // if ((item.Isactive == true) && (item.GroupUserStatus == 0 || item.GroupUserStatus == 3 || item.GroupUserStatus == 5)) {  //Join  communityl
    //   return (
    //     <Swipeable
    //                 leftButtonWidth={85}
    //                 leftButtons={[
    //                     <TouchableOpacity style={styles.onLeftStyle} onPress={() => { this.handleJoinCommunityModalStatus(true, item.CommunityID, item.GroupID, "join") }}>
    //                         <View style={styles.vwFlexDirectionRowStyle}>
    //                             <Image
    //                                 source={images.Communities.joinPlus}
    //                                 style={styles.crossimgesStyle}
    //                                 resizeMode="contain" />
    //                             <Text style={styles.renderswipebtnText}>{'Join'}</Text>
    //                         </View>
    //                     </TouchableOpacity>
    //                 ]} rightButtons={null} rightContent={null}>
    //         <View style={[styles.vwItemParentPaddingStyle, { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white }]}>
    //           <TouchableOpacity  onPress={()=>this.props.navigation.navigate('GroupDashboard',{groupId:item.GroupID,isFromProp:'', communityID:0})}>
    //             <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
    //               {
    //                 (this.state.onSwipOutAccept) ? null : <View style={styles.leftGreenLine}></View>
    //               }


    //               <View style={styles.beforeimgView}>
    //                 {(isGroupTypeID) ?
    //                   (item.GroupTypeID == 2) ?
    //                     <Image
    //                       source={images.Communities.lock}
    //                       style={styles.lockIconStyle}
    //                     />
    //                     : null
    //                   :
    //                   null
    //                 }
    //                 <Image

    //                   source={{ uri: isCommunityLogoPath ? item.CommunityLogoPath : globals.User_img }}
    //                   style={[styles.ivItemImageStyle]}
    //                 />
    //               </View>
    //               <View style={styles.middleViewTexts}>
    //                 <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isGroupName) ? item.GroupName : ''}</Text>
    //                 <View style={styles.memberView}>
    //                   <Image resizeMode="contain" source={images.Communities.member}
    //                     style={[styles.memberimgStyle, { tintColor: item.isSelected == true ? colors.white : colors.lightGray }]} />
    //                   <Text style={[styles.membetText, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{(isMembersCount) ? item.MemberCount : ''}</Text>
    //                 </View>
    //               </View>
    //             </View>

    //           </TouchableOpacity>
    //           <View style={[styles.horizontalSeprator]}></View>
    //         </View>
    //       </Swipeable>

    //   )
    // }
    if (item.Isactive == true && (item.GroupUserStatus == 2 || item.GroupUserStatus == 6)) {  //Leave  communityl
      return (
        <Swipeable
          leftContent={null}
          leftButtons={null}
          rightButtonWidth={100}
          rightButtons={[
            <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleLaeaveCommunityModalStatus(true, item.CommunityID, item.GroupID, "leave") }}>
              <View style={styles.vwFlexDirectionRowStyle}>
                <Image
                  source={images.Communities.LeaveCommunity}
                  style={styles.crossimgesStyle}
                  resizeMode="contain" />
                <Text style={styles.renderswipebtnText}>{'LEAVE'}</Text>
              </View>
            </TouchableOpacity>

          ]}>
          <View style={[styles.vwItemParentPaddingStyle]}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GroupDashboard', { groupId: item.GroupID, isFromProp: 'LEAVE', communityID: 0 })}>
              <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>


                <View style={styles.beforeimgView}>
                  {(isGroupTypeID) ?
                    (item.GroupTypeID == 2) ?
                      <Image
                        source={images.Communities.lock}
                        style={styles.lockIconStyle}
                      />
                      : null
                    :
                    null
                  }
                  <Image

                    source={{ uri: isCommunityLogoPath ? item.CommunityLogoPath : globals.User_img }}
                    style={[styles.ivItemImageStyle]}
                  />
                </View>
                <View style={styles.middleViewTexts}>
                  <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isGroupName) ? item.GroupName : ''}</Text>
                  <View style={styles.memberView}>
                    <Image resizeMode="contain" source={images.Communities.member}
                      style={[styles.memberimgStyle, { tintColor: item.isSelected == true ? colors.white : colors.lightGray }]} />
                    <Text style={[styles.membetText, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{(isMembersCount) ? item.MemberCount : ''}</Text>
                  </View>
                  <Text style={[styles.membetText, { marginLeft: 0, color: item.isSelected == true ? colors.white : colors.lightGray }]}>Community: {(isCommunityName) ? item.CommunityName : '-'}</Text>
                </View>
                {
                  (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                }
              </View>
              <View style={[styles.horizontalSeprator]}></View>
            </TouchableOpacity>
          </View>
        </Swipeable>
      )
    }
    else if (item.Isactive == true && item.GroupUserStatus == 1) {  //Waiting for the approval
      return (
        <Swipeable leftButtons={null} leftContent={null}
          rightButtonWidth={100}
          rightButtons={[
            <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleCancelModalStatusApproval(true, item.CommunityID, item.GroupID, "leave") }}>
              <View style={styles.vwFlexDirectionRowStyle}>
                <Image
                  source={images.Communities.joinMinus}
                  style={styles.crossimgesStyle}
                  resizeMode="contain" />
                <Text style={styles.renderswipebtnText}>{'CANCEL'}</Text>
              </View>
            </TouchableOpacity>

          ]}>
          <View style={[styles.vwItemParentPaddingStyle]}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GroupDashboard', { groupId: item.GroupID, isFromProp: 'WFA', communityID: 0 })}>
              <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                <View style={styles.beforeimgView}>
                  {(isGroupTypeID) ?
                    (item.GroupTypeID == 2) ?
                      <Image
                        source={images.Communities.lock}
                        style={styles.lockIconStyle}
                      />
                      : null
                    :
                    null
                  }
                  <Image
                    source={{ uri: isCommunityLogoPath ? item.CommunityLogoPath : globals.User_img }}
                    style={[styles.ivItemImageStyle]}
                  />
                </View>
                <View style={styles.middleViewTexts}>
                  <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isGroupName) ? item.GroupName : ''}</Text>
                  <View style={styles.memberView}>
                    <Image resizeMode="contain" source={images.Communities.member}
                      style={[styles.memberimgStyle, { tintColor: item.isSelected == true ? colors.white : colors.lightGray }]} />
                    <Text style={[styles.membetText, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{(isMembersCount) ? item.MemberCount : ''}</Text>
                  </View>
                  <Text style={[styles.membetText, { marginLeft: 0, color: item.isSelected == true ? colors.white : colors.lightGray }]}>Community: {(isCommunityName) ? item.CommunityName : '-'}</Text>
                </View>

                <View style={styles.lastImgViewEnd}>
                  <Image
                    source={images.Communities.waitingApproval}
                    style={styles.attende_shareIconStyle}
                  />
                </View>

                {
                  (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                }
              </View>
              <View style={[styles.horizontalSeprator]}></View>
            </TouchableOpacity>
          </View>
        </Swipeable>
      )
    }
    else if ( item.GroupUserStatus == 3) {  //Rejected Request
      return (
        <View>

          <View style={[styles.vwItemParentPaddingStyle,{ opacity: 0.5 }]}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GroupDashboard', { groupId: item.GroupID, isFromProp: 'Inactive', communityID: 0 })}>
              <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                <View style={styles.beforeimgView}>
                  {(isGroupTypeID) ?
                    (item.GroupTypeID == 2) ?
                      <Image
                        source={images.Communities.lock}
                        style={styles.lockIconStyle}
                      />
                      : null
                    :
                    null
                  }
                  <Image
                    source={{ uri: isCommunityLogoPath ? item.CommunityLogoPath : globals.User_img }}
                    style={[styles.ivItemImageStyle]}
                  />
                </View>
                <View style={styles.middleViewTexts}>
                  <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isGroupName) ? item.GroupName : ''}</Text>
                  <View style={styles.memberView}>
                    <Image resizeMode="contain" source={images.Communities.member}
                      style={[styles.memberimgStyle, { tintColor: item.isSelected == true ? colors.white : colors.lightGray }]} />
                    <Text style={[styles.membetText, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{(isMembersCount) ? item.MemberCount : ''}</Text>
                  </View>
                  <Text style={[styles.membetText, { marginLeft: 0, color: item.isSelected == true ? colors.white : colors.lightGray }]}>Community: {(isCommunityName) ? item.CommunityName : '-'}</Text>

                </View>
                <View style={styles.lastImgViewEnd}>
                  <View style={[styles.selectedtouchbleView,]}>
                    <Text style={styles.beforeTextStyle}>Rejected</Text>
                  </View>
                </View>
              </View>
              <View style={[styles.horizontalSeprator]}></View>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
    else if (item.GroupUserRoleID == 1 && item.Isactive == true) {  //Group Admin(Setting Icon)
      return (
        <View>

          <View style={[styles.vwItemParentPaddingStyle]}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GroupDashboard', { groupId: item.GroupID, isFromProp: 'GroupAdmin', communityID: 0 })}>
              <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                <View style={styles.beforeimgView}>
                  {(isGroupTypeID) ?
                    (item.GroupTypeID == 2) ?
                      <Image
                        source={images.Communities.lock}
                        style={styles.lockIconStyle}
                      />
                      : null
                    :
                    null
                  }
                  <Image

                    source={{ uri: isCommunityLogoPath ? item.CommunityLogoPath : globals.User_img }}
                    style={[styles.ivItemImageStyle]}
                  />
                </View>
                <View style={styles.middleViewTexts}>
                  <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isGroupName) ? item.GroupName : ''}</Text>
                  <View style={styles.memberView}>
                    <Image resizeMode="contain" source={images.Communities.member}
                      style={[styles.memberimgStyle, { tintColor: item.isSelected == true ? colors.white : colors.lightGray }]} />
                    <Text style={[styles.membetText, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{(isMembersCount) ? item.MemberCount : ''}</Text>
                  </View>
                  <Text style={[styles.membetText, { marginLeft: 0, color: item.isSelected == true ? colors.white : colors.lightGray }]}>Community: {(isCommunityName) ? item.CommunityName : '-'}</Text>
                </View>
                <View style={styles.lastImgViewEnd}>
                  <Image
                    source={images.Communities.globaletting}
                    style={styles.attende_shareIconStyle}
                  />
                </View>
              </View>
              <View style={[styles.horizontalSeprator]}></View>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
    else if (item.Isactive == false) {  // Inactive Group
      return (
        <View>
          <View style={[styles.vwItemParentPaddingStyle, { opacity: 0.5 }]}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('GroupDashboard', { groupId: item.GroupID, isFromProp: 'Inactive', communityID: 0 })}>
              <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                <View style={styles.beforeimgView}>
                  {(isGroupTypeID) ?
                    (item.GroupTypeID == 2) ?
                      <Image
                        source={images.Communities.lock}
                        style={styles.lockIconStyle}
                      />
                      : null
                    :
                    null
                  }
                  <Image
                    source={{ uri: isCommunityLogoPath ? item.CommunityLogoPath : globals.User_img }}
                    style={[styles.ivItemImageStyle]}
                  />
                </View>
                <View style={styles.middleViewTexts}>
                  <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isGroupName) ? item.GroupName : ''}</Text>
                  <View style={styles.memberView}>
                    <Image resizeMode="contain" source={images.Communities.member}
                      style={[styles.memberimgStyle, { tintColor: item.isSelected == true ? colors.white : colors.lightGray }]} />
                    <Text style={[styles.membetText, { color: item.isSelected == true ? colors.white : colors.lightGray }]}>{(isMembersCount) ? item.MemberCount : ''}</Text>
                  </View>
                  <Text style={[styles.membetText, { marginLeft: 0, color: item.isSelected == true ? colors.white : colors.lightGray }]}>Community: {(isCommunityName) ? item.CommunityName : '-'}</Text>
                </View>
                <View style={styles.lastImgViewEnd}>
                  <TouchableOpacity style={[styles.selectedtouchbleViewforinactive,]}>
                    <Text style={[styles.beforeTextStyleforinactive, {
                      textDecorationLine: "underline"
                    }]}>Inactive</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.horizontalSeprator]}></View>
            </TouchableOpacity>
          </View>
        </View>
      )
    }
  }

  handleLoadMore = () => {
    if (!this.onEndReachedCalledDuringMomentum) {
      this.setState({
        pageNumber: this.state.pageNumber + 1,
      }, () => {
        _this.apicallGetGroupsList();
      })
      this.onEndReachedCalledDuringMomentum = true;

    }
  };

  onCrossApicall() {
    this.props.showLoader()
    this.setState({ groupName: '', responseComes: false, loadingPagination: true }, () => {
      this.apicallGetGroupsList()
    })
  }

  /**
   * Search modal render function
   */
  renderSearchModel() {
    const { groupName } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.isShowSearch}
        onRequestClose={() => {
          this.setModalVisible(false);
        }}
      >
        <View style={styles.modelParentStyle}>
          <KeyboardListener
            onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
            onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
          <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
            <TouchableOpacity onPress={() => { this.onCrossApicall(); this.setModalVisible(false); }}>
              <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
            </TouchableOpacity>
            <Text style={styles.tvSearchEventStyle}>
              {'Search Group'}
            </Text>
          </View>
          <View style={styles.mainParentStyle}>
            <View style={styles.textInputViewContainer}>
              <TextInput
                style={styles.textInputStyleContainer}
                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_GROUPS}
                placeholderTextColor={colors.lightGray}
                onChangeText={text => this.setState({ groupName: text })}
                returnKeyType="done"
                blurOnSubmit={true}
                value={groupName}
                autoCapitalize="none"
              />
            </View>
            {(Platform.OS == "android") ?
              (this.state.keyboardOpen == false) ?
                <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByGroupName()}>
                  <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                </TouchableOpacity>
                :
                null
              :
              <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByGroupName()}>
                <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
              </TouchableOpacity>
            }
          </View>
        </View>
      </Modal>
    );
  }

  /**
 * click on search based on Group name
 */
  seacrhByGroupName() {
    const { groupName } = this.state;
    this.setState({ groupName: groupName }, () => {
      this.setModalVisible(false);
      this.searchGroup();
    })
  }

  /**
  * Method of get  communities based on search filter
  */

  searchGroup() {
    _this.makeListFresh(() => {
      _this.props.showLoader()
      _this.apicallGetGroupsList();
    });
  }

  makeListFresh(callback) {
    this.setState(
      {
        pageNumber: 1,
        pageSize: 10,
        groupsData: [],
        responseComes: false
      },
      () => {
        callback();
      }
    );
  }



  /**
   * Method for change modal state
   * @param {*} visible 
   */
  setModalVisible(visible) {
    this.setState({ isShowSearch: visible });
  }

  /**
    * Handle status for cancel click of leave
    * @param {*} visible 
    */
  handleLaeaveCommunityModalStatus(visible, communityId, GroupID, Action) {
    this.setState({ leavecommunityModal: visible, communityID: communityId, groupID: GroupID, action: Action })
  }


  /**
      * Handle status for cancel click of join
      * @param {*} visible 
      */
  handleJoinCommunityModalStatus(visible, communityId, GroupID, Action) {
    this.setState({ joincommunityModal: visible, communityID: communityId, groupID: GroupID, action: Action })
  }


  /**
      * Handle status for cancel click of waiting for Approval
      * @param {*} visible 
      */
  handleCancelModalStatusApproval(visible, communityId, GroupID, Action) {
    this.setState({ cancelModalAproval: visible, communityID: communityId, groupID: GroupID, action: Action })
  }



  /**
  * method for Join the community
  */
  clickProceedJoinCommunity() {
    this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action)
    this.props.showLoader()
    this.setState({ loading: true })
    if (globals.isInternetConnected == true) {
      API.joinOrLeaveGroup(this.joinOrLeaveGroupResponseData, true, this.state.groupID, this.state.action, globals.userID);
    } else {
      this.props.hideLoader()
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }



  /**
          * method for Cancle the community
          */
  clickProceedCancleCommunity() {
    this.handleCancelModalStatusApproval(false, this.state.communityID, this.state.groupID, this.state.action)
    this.props.showLoader()
    this.setState({ loading: true })
    if (globals.isInternetConnected == true) {
      API.joinOrLeaveGroup(this.joinOrLeaveGroupResponseData, true, this.state.groupID, this.state.action, globals.userID);
    } else {
      this.props.hideLoader()
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
  * method for Leave the community
  */
  clickProceedLeaveCommunity() {
    this.handleLaeaveCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action)
    this.props.showLoader()
    this.setState({ loading: true })
    if (globals.isInternetConnected == true) {
      API.joinOrLeaveGroup(this.joinOrLeaveGroupResponseData, true, this.state.groupID, this.state.action, globals.userID);
    } else {
      this.props.hideLoader()
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
  * Response callback of joinOrLeaveGroupResponseData
  */
  joinOrLeaveGroupResponseData = {
    success: response => {
      console.log(
        TAG,
        'joinOrLeaveGroupResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.props.showLoader()
        this.clearStates()
        this.apicallGetGroupsList()
      }
      else {
        this.setState({ loading: false }, () => {
          Alert.alert(globals.appName, response.Message);
        })
      }
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'joinOrLeaveGroupResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.setState({ loading: false, loadingPagination: false })
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };



  /**
    * Render modal of leave community
    */
  renderLeaveCommunityModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.leavecommunityModal}
        onRequestClose={() => {
          this.handleLaeaveCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
        onBackdropPress={() => {
          this.handleLaeaveCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
      >
        <View style={styles.modalMainView}>
          <View style={styles.modalInnerMainView}>
            <View style={styles.popupinnerViewStyle}>
              <Text style={styles.popupheaderStyle}>{"Are you sure you want to leave this group?"}</Text>
              <View style={styles.btnViewStyle}>
                <TouchableOpacity onPress={() => this.handleLaeaveCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action)}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                      name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                    <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.clickProceedLeaveCommunity()}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                      name={"check"} color={colors.white} style={styles.vectorStyle} />
                    <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }


  /**
   * Render modal of leave community
   */
  renderJoinCommunityModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.joincommunityModal}
        onRequestClose={() => {
          this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
        onBackdropPress={() => {
          this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
      >
        <View style={styles.modalMainView}>
          <View style={styles.modalInnerMainView}>
            <View style={styles.popupinnerViewStyle}>
              <Text style={styles.popupheaderStyle}>{"Are you sure you want to join this group?"}</Text>
              <View style={styles.btnViewStyle}>
                <TouchableOpacity onPress={() => this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action)}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                      name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                    <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.clickProceedJoinCommunity()}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                      name={"check"} color={colors.white} style={styles.vectorStyle} />
                    <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  /**
       * Render modal of cancel click of waiting for approval
       */
  renderCancelWaitingApprovalModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.cancelModalAproval}
        onRequestClose={() => {
          this.handleCancelModalStatusApproval(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
        onBackdropPress={() => {
          this.handleCancelModalStatusApproval(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
      >
        <View style={styles.modalMainView}>
          <View style={styles.modalInnerMainView}>
            <View style={styles.popupinnerViewStyle}>
              <Text style={styles.popupheaderStyle}>{"Do you want to cancel the request?"}</Text>
              <View style={styles.btnViewStyle}>
                <TouchableOpacity onPress={() => this.handleCancelModalStatusApproval(false, this.state.communityID, this.state.groupID, this.state.action)}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                      name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                    <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.clickProceedCancleCommunity()} >
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                      name={"check"} color={colors.white} style={styles.vectorStyle} />
                    <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }




  render() {
    const { serverErr, loading, groupName, groupsData, responseComes, loadingPagination, isInternetFlag } = this.state;
    

    return (
      <View style={styles.mainParentStyle}>
        <NavigationEvents
          // onWillFocus={() => this.apicallGetGroupsList()}
          onWillBlur={() => this.clearStates()}
        />
        {this.renderSearchModel()}
        {this.renderJoinCommunityModal()}
        {this.renderLeaveCommunityModal()}
        {this.renderCancelWaitingApprovalModal()}
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            (serverErr === false) ?
              (
                (responseComes == false && loadingPagination == true) ?
                  <View></View> :
                  (groupsData.length == 0 && responseComes) ?
                    (groupsData.length == 0 && groupName !== '') ?
                      <View style={globalStyles.nodataStyle}>
                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_GROUP_NOT_AVLB_SEARCH}</Text>
                      </View> :
                      <View style={globalStyles.nodataStyle}>
                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_GROUP_NOT_AVLB_SEARCH}</Text>
                      </View> :
                    <View style={styles.mainParentStyle}>
                      <FlatList
                        style={{ flex: 1 }}
                        showsVerticalScrollIndicator={false}
                        data={groupsData}
                        renderItem={({ item, index }) => this.renderGroupList(item, index)}
                        extraData={this.state}
                        bounces={false}
                        keyExtractor={(index, item) => item.toString()}
                        ListFooterComponent={this.renderFooter}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.2}
                        onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                      />
                    </View>
              )
              :
              <ServerError loading={loading} onPress={() => this._tryAgain()} />
        }
      </View>
    );
  }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Groups);