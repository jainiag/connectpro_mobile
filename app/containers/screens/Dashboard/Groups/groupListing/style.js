import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
    mainParentStyle: {
        flex: 1,
    },
    tvTitleStyle: {
        fontSize: globals.font_20,
        color: 'blue',
    },
    loaderbottomview: {
        bottom: (Platform.OS == 'android') ?
          globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
      },
    mainRenderItemView: {
        height: globals.screenHeight * 0.13, borderColor: colors.proUnderline, marginBottom: globals.screenHeight * 0.01, flex: 1,
        // marginHorizontal: globals.screenHeight * 0.02
    },
    horizontalItemView: {
        flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.017, flex: 1,
        alignItems: 'center',
    },
    middleViewTexts: {
        justifyContent: 'center', marginHorizontal: globals.screenHeight * 0.02, flex: 1,
    },
    lastImgViewEnd: {
        justifyContent: 'center', alignItems: 'flex-end',
    },
    attendesFlatlist: {
        marginTop: globals.screenHeight * 0.05,
        paddingHorizontal: globals.screenWidth * 0.05
    },
    beforeimgView: {
        height: globals.screenHeight * 0.0757, //40
        width: globals.screenHeight * 0.0757, //40
        borderRadius: (globals.screenHeight * 0.0946) / 2,
        borderColor: colors.lightGray,
        borderWidth: 0.3,
        marginLeft: 6
    },
    selectedtouchbleView: {

        height: globals.screenHeight * 0.04,
        width:globals.screenWidth*0.2,
        marginTop: 8,
        borderWidth: 1,
        backgroundColor: colors.orange,
        borderColor: colors.proUnderline, borderRadius: 4,
        alignItems:'center',
        justifyContent:'center'
    },
    selectedtouchbleViewforinactive: {

        marginTop: 8,

    },
    selctedview:
    {
        marginHorizontal: globals.screenWidth * 0.04,
    },
    beforeTextView: {
        margin: 5,
        backgroundColor: colors.darkYellow
    },
    beforeTextStyle: {
        fontSize: globals.font_13,
        textAlign: 'center',
        color: colors.white
    },
    beforeTextStyleforinactive: {
        fontSize: globals.font_13,
        textAlign: 'center',
        padding: 8,
        color: colors.lightGray
    },
    ivItemImageStyle: {
        height: globals.screenHeight * 0.0757, //40
        width: globals.screenHeight * 0.0757, //40
        borderRadius: (globals.screenHeight * 0.0946) / 2,
        marginTop: -1,
        marginLeft: -1,
        borderColor: colors.lightGray,
        borderWidth: 0.3,
    },
    attendeeTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    },
    attende_shareIconStyle: {
        height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.screenHeight * 0.05 : globals.screenHeight * 0.06 : globals.screenWidth * 0.08,
        width: (iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet()) ? (Platform.OS == 'ios') ? globals.screenHeight * 0.05 : globals.screenHeight * 0.06 : globals.screenWidth * 0.08,
    },
    memberView: {
        flexDirection: 'row',marginVertical:1
    },
    memberimgStyle: {
        height: globals.screenWidth * 0.04,
        width: globals.screenWidth * 0.04,

    },
    lockIconStyle: {
        height: 15,
        width: 15,
        position: 'absolute',
        zIndex: 10,
        top: -4,
        right: 5
    },
    lastImgViewEnd: {
        justifyContent: 'center',
        alignItems:'flex-end',
        marginRight: globals.screenWidth * 0.0001,
        
    },
    attende_shareIconStyle: {
        height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.screenHeight * 0.03 : globals.screenHeight * 0.04 : globals.screenWidth * 0.06,
        width: (iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet()) ? (Platform.OS == 'ios') ? globals.screenHeight * 0.03 : globals.screenHeight * 0.04 : globals.screenWidth * 0.06,
    },
    membetText: {
        fontSize: globals.font_12,
        marginLeft: globals.screenWidth * 0.02
    },
    horizontalSeprator: {
        borderColor: colors.proUnderline, borderWidth: 1,
        // marginTop: globals.screenHeight * 0.01,
        marginHorizontal: -(globals.screenWidth * 0.0368)
    },
    //Search admin modal styles
    modelParentStyle: {
        flex: 1,
    },
    vwHeaderStyle: {
        marginTop: globals.screenHeight * 0.08,
        marginLeft:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.07
                : globals.screenWidth * 0.07,
        alignItems: 'center',
        marginBottom: globals.screenHeight * 0.03,
    },
    tvSearchEventStyle: {
        color: colors.black,
        fontWeight: '500',
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
        marginLeft:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.04
                : globals.screenWidth * 0.08,
    },
    vwFlexDirectionRowStyle: {
        flexDirection: 'row',
        flex:1
    },
    toSearchStyle: {
        position: 'absolute',
        bottom: 0,
        height: globals.screenHeight * 0.0662, // 35
        backgroundColor: colors.bgColor,
        alignItems: 'center',
        justifyContent: 'center',
        width: globals.screenWidth,
    },
    tvSearchStyle: {
        color: colors.white,
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
    },
    textInputViewContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.proUnderline,
        alignItems: 'center',
        marginTop: globals.screenHeight * 0.05,
        borderRadius: 5,
    },
    textInputStyleContainer: {
        flex: 1,
        marginLeft: globals.screenWidth * 0.04,
        color: colors.black,
        fontSize: globals.font_14,
        marginVertical: globals.screenHeight * 0.03,
    },
    mainParentbtnStyle: {
        flex: 1,
        marginTop: globals.screenHeight * 0.01,
    },
    vwFlexDirectionRowStyle: {
        flexDirection: 'row',
    },
    vwItemParentPaddingStyle: {
        marginHorizontal: 0,
        color:colors.white,
        flex:1
        // backgroundColor:colors.white
    },
    //swipeout design styles:

    crossimgesStyle: {
        alignSelf: 'center',
        height: globals.screenHeight * 0.02,
        width: globals.screenHeight * 0.02
    },
    imgesStyle: {
        alignSelf: 'center',
        height: globals.screenHeight * 0.03,
        width: globals.screenHeight * 0.03
    },
    imgcontainer: {
        alignItems: 'center', justifyContent: 'center', flex: 1
    },
    //cancel click for waiting for review and approval

    modalMainView: {
        height: globals.screenHeight * 0.18,
        justifyContent: 'center', alignItems: 'center',
        alignSelf: 'center',
        width: globals.screenWidth * 0.9,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.gray,
        borderRadius: 6,
        borderWidth: 1,
        borderColor: colors.gray,
        marginTop: globals.screenHeight * 0.2
    },
    modalInnerMainView: {
        backgroundColor: colors.gray,
        //  margin: globals.screenWidth * 0.035,
        width: globals.screenWidth * 0.8,
        // borderRadius: 4,
        flex: 1,
    },
    modalSecondView: {
        height: (globals.iPhoneX) ? globals.screenHeight * 0.37 : globals.screenHeight * 0.4,
        justifyContent: 'center',
        alignItems: 'center',
        width: globals.screenWidth * 0.9,
        marginHorizontal: (globals.screenWidth * 0.05),
        backgroundColor: colors.gray,
        marginTop: globals.screenHeight * 0.6
    },
    modalAlreadyRegisteredView: {
        flexDirection: 'row', padding: globals.screenHeight * 0.01
    },
    modalResendTextView: {
        flex: 1, marginRight: 10
    },
    modalCancelImg: {
        height: globals.screenHeight * 0.03, width: globals.screenHeight * 0.03, tintColor: colors.gray
    },
    modalHorizontalLine: {
        height: 2, backgroundColor: colors.proUnderline, width: '100%', marginTop: (globals.screenHeight * 0.03), marginBottom: (globals.screenHeight * 0.03)
    },
    modalTextInputView: {
        borderColor: colors.gray, borderWidth: 2, marginHorizontal: 10
    },
    modalSubmitView: {
        justifyContent: 'center', backgroundColor: colors.listSelectColor, width: globals.screenWidth * 0.22, alignItems: 'center', height: globals.screenHeight * 0.05, right: 0, position: 'absolute', bottom: (globals.screenHeight * 0.03), marginRight: globals.screenHeight * 0.01, borderRadius: 3
    },
    modalSubmitText: {
        color: colors.white, fontSize: globals.font_13, fontWeight: '600'
    },
    popupinnerViewStyle: {
        flex: 1, marginTop: globals.screenHeight * 0.02,
        marginHorizontal: globals.screenHeight * 0.015,
    },
    popupheaderStyle: {
        fontSize: globals.font_16,
        color: colors.Gray,
        fontWeight: "500"
    },
    popupsubTextStyle: {
        marginTop: globals.screenHeight * 0.01,
        fontSize: globals.font_12,
        color: colors.Gray
    },
    termconditionView: {
        marginVertical: globals.screenHeight * 0.025,
        marginHorizontal: globals.screenWidth * 0.02,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center'
    },
    squareView: {
        // marginTop: globals.screenHeight * 0.0065,
        height:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.028
                : globals.screenWidth * 0.045,
        width:
            iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                ? globals.screenWidth * 0.028
                : globals.screenWidth * 0.045,
        borderColor: colors.gray,
        borderWidth: 1,
    },
    scceptText: {
        fontSize: globals.font_14,
        color: colors.black
    },
    linkText: {
        fontSize: globals.font_14,
        color: colors.warmBlue,
        textDecorationLine: "underline",
        fontWeight: "500",

    },
    btnViewStyle: {
        marginVertical: globals.screenHeight * 0.01,
        marginHorizontal: globals.screenWidth * 0.02,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    innerbtnViewStyles: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderWidth: 0.2,
        borderRadius: 3,
        // marginHorizontal:globals.screenWidth * 0.03
    },
    closeimgStyle: {
        height: globals.screenWidth * 0.04,
        width: globals.screenWidth * 0.04,
        tintColor: colors.white,
        marginLeft: globals.screenWidth * 0.02
    },
    vectorStyle: {
        marginLeft: globals.screenWidth * 0.01,
        alignItems: 'center'
    },
    btnTextsStyle: {
        //  paddingLeft:globals.screenWidth * 0.025,
        paddingRight: globals.screenWidth * 0.025,
        paddingVertical: globals.screenWidth * 0.015,
        fontSize: globals.font_14,
        color: colors.white,
        fontWeight: "600",
        textAlign: 'center'
    },
    nodataStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    },
    nodataTextStyle: {
        fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : globals.font_14
    },
    loaderWrapper: {
        flex: 1,
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex: 999,
        backgroundColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loaderInner: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    vwFlexDirectionRowStyle: {
        flexDirection: 'row',
    },
    vwItemParentPaddingStyle: {
        marginHorizontal: 0,
        backgroundColor: colors.white
    },
    vwItemPaddingStyle: {
        marginHorizontal: globals.screenWidth * 0.0368, // 15
        marginVertical: globals.screenHeight * 0.0284, // 15
    },
    rightRedLine: {
        backgroundColor: colors.darkRed, width: 7,
        marginRight: -(globals.screenWidth * 0.0368),
        marginVertical: -(globals.screenHeight * 0.028),
    },
    leftGreenLine: {
        backgroundColor: colors.greenDone, width: 7,
        marginLeft: -(globals.screenWidth * 0.0368),
        marginVertical: -(globals.screenHeight * 0.028),
    },
    renderswipebtnText: {
        marginLeft: 3, color: colors.white,
        fontSize: globals.font_14
    },
    onLeftStyle: {
        flex: 1,
        backgroundColor: colors.greenDone,
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingRight: 20,
    },
    onRightStyle: {
        flex: 1,
        backgroundColor: colors.darkRed,
        justifyContent: 'center',
        // alignItems: 'flex-end',
        paddingLeft: 20
    },
});
