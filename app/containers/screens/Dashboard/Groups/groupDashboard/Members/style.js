import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../../utils/globals';
import * as colors from '../../../../../../assets/styles/color';


const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  loaderbottomview: {
    bottom: (Platform.OS == 'android') ?
      globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
  },
  userinfoStyle: {
    flexDirection: 'row',
    marginHorizontal: globals.screenWidth * 0.04,
    paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.025 : globals.screenWidth * 0.035,
    alignItems:'flex-start',borderWidth: 0.7,
    borderColor: colors.gray,
    alignItems:'center',
    borderRadius: 5,marginBottom: globals.screenHeight * 0.015,
  },
  userinfoStyleConnected: {
    flex:1,
    flexDirection: 'row',
    marginHorizontal: globals.screenWidth * 0.04,
    //paddingHorizontal: globals.screenWidth * 0.02,
   // paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.025 : globals.screenWidth * 0.035,
    borderWidth: 0.7,
    borderColor: colors.gray,
    //justifyContent:'center',
    alignItems:'flex-start',
    borderRadius: 5,marginBottom: globals.screenHeight * 0.015,
  },
  connectedTwoViews:{
    flex:1,
     flexDirection: 'row',
    paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.025 : globals.screenWidth * 0.035,
    alignItems:'center', 
    justifyContent:'center',
  },
  connectedbtnImgStyle:{
    height:globals.screenHeight*0.12,
    width:globals.screenHeight*0.12
  },
//   boxViewStyle: {
//     borderWidth: 0.7,
//     borderColor: colors.gray,
//     borderRadius: 5,
//     marginBottom: globals.screenHeight * 0.015,
//     width: globals.screenWidth * 0.90,
//     marginLeft: globals.screenWidth * 0.0595,
//     flex:1,
//   },
  beforeImgViewStyle:{
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 :globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 :globals.screenWidth * 0.15,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (globals.screenWidth * 0.10)/2 :(globals.screenWidth * 0.15) / 2,
    borderColor: colors.proUnderline,
    borderWidth: 0.5,
  },
  imgStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 :globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 :globals.screenWidth * 0.15,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (globals.screenWidth * 0.10)/2 :(globals.screenWidth * 0.15) / 2,
    borderColor: colors.white,
    borderWidth:0.5,
  },
  textViewStyle: {
    // alignContent: 'center',
    marginHorizontal: globals.screenWidth * 0.035,
    marginVertical: globals.screenWidth * 0.025,
    flex:1,
    justifyContent:'center'
  },
  usernametextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_12,
    color:colors.lightBlack,
},
  proffesionTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
    marginRight:5,flex:1,marginTop:2,color:colors.lightBlack,

  },
  lastImgViewEnd:{
    justifyContent:'center', alignItems:'flex-end',
  },
  connectedBtnView:{
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:colors.listSelectColor,
      height:globals.screenWidth*0.09,
      width:globals.screenWidth*0.22,
      borderRadius:5
  },
  connectTextStyle:{
      color:colors.white,
      fontSize:globals.font_13
  },

  //connect modal styles
  modalMainView:{
    height: globals.screenHeight * 0.44,
    justifyContent: 'center', alignItems: 'center',
    alignSelf:'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor:colors.proUnderline,
     borderRadius: 6,
     borderWidth:1,
     borderColor:colors.gray,
    marginTop: globals.screenHeight * 0.2
  },
  modalInnerMainView: {
    backgroundColor: colors.proUnderline,
    flex: 1,
    margin: 10,
  },
  add_not: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    marginTop:7,
    marginBottom : 5
  },
  underLineView:{
    height:1, backgroundColor:colors.gray, marginVertical:10, marginHorizontal:-10
  },
  addNoteTxt:{
    marginTop: 7, marginBottom:5, fontWeight:'600'
  },
  modalUperText:{
    fontSize:globals.font_13,
    fontWeight:'500',
  },
  textInputViewContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.gray,
    // marginHorizontal: 10,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.01,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginTop: 5,
    marginBottom: 5,
    height:globals.screenHeight * 0.18
  },
  modalEndView:{justifyContent:'flex-end', height:(globals.screenHeight*0.06), marginTop:10, flexDirection:'row'},
  closeView:{
    height: globals.screenHeight * 0.045, width: globals.screenWidth * 0.2, 
     borderWidth:1,  borderColor:colors.gray,
    borderRadius: 3,  alignItems: 'center', justifyContent: 'center'
  },
  sendView: {
    height: globals.screenHeight * 0.045, width: globals.screenWidth * 0.2, 
    backgroundColor: colors.listSelectColor,
    borderRadius: 3, marginLeft: 5, alignItems: 'center', justifyContent: 'center'
  },
  sendTxt: {
    fontSize: globals.font_13,
    color: colors.white
  },
  closeTxt: {
    fontSize: globals.font_13,
    color: colors.black
  },
   //Search admin modal styles
   modelParentStyle: {
    flex: 1,
  },
  vwHeaderStyle: {
    marginTop: globals.screenHeight * 0.08,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.07
        : globals.screenWidth * 0.07,
    alignItems: 'center',
    marginBottom: globals.screenHeight * 0.03,
  },
  tvSearchEventStyle: {
    color: colors.black,
    fontWeight: '500',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_18,
    marginLeft:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.04
        : globals.screenWidth * 0.08,
  },
  vwFlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  toSearchStyle: {
    position: 'absolute',
    bottom: 0,
    height: globals.screenHeight * 0.0662, // 35
    backgroundColor: colors.bgColor,
    alignItems: 'center',
    justifyContent: 'center',
    width: globals.screenWidth,
  },
  tvSearchStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_16,
  },
  textInputViewContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.05,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginVertical: globals.screenHeight*0.03,
  },
//----------------------

 
});