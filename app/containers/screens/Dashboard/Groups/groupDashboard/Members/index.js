import React from 'react';
import { Text, View, Modal, TouchableOpacity, Image, FlatList, ActivityIndicator, TextInput, Alert, TouchableWithoutFeedback } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import { API } from '../../../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/EvilIcons';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../../../components/NoInternet/index'
import ServerError from '../../../../../../components/ServerError/index'

const iPad = DeviceInfo.getModel();
let TAG = "Members Screen ::==="
let _this = null;
class GroupMembers extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Members (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Members ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => { _this.setModalVisible(true); _this.setState({ memberName: '' }) }}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };


    constructor(props) {
        super(props);
        _this = this;
        console.log(TAG, props);
        this.state = {
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isInternetFlag: true,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            groupId: this.props.navigation.state.params.groupId,
            visibleModal: false,
            receiverID: 0,
            notes: ''
        }
    }

    componentDidMount() {
        this.props.showLoader()
        this.apicallGetMembersList()
    }


    /**
 * click on search based on community name
 */
    seacrhByMembernName() {
        const { memberName } = this.state;
        this.setState({ memberName: memberName }, () => {
            this.setModalVisible(false);
            this.searchMember();
        })
    }

    /**
    * Method of get  communities based on search filter
    */

    searchMember() {
        _this.makeListFresh(() => {
            _this.props.showLoader();
            _this.apicallGetMembersList();
        });
    }

    clearStateValue() {
        this.setState({
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            visibleModal: false,
            receiverID: 0,
            notes: ''
        }, () => {
            this.props.showLoader()
            this.apicallGetMembersList()
        })
    }

    onCloseModal() {
        this.setModalVisible(false);
        this.clearStateValue()
    }

    /**
  * Search modal render function
  */
    renderSearchModel() {
        const { memberName } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                        <TouchableOpacity onPress={() => { this.onCloseModal() }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Members'}
                        </Text>
                    </View>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_GROUPS_MEMBERS}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ memberName: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={memberName}
                                autoCapitalize="none"
                            />
                        </View>
                        <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByMembernName()}>
                            <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    /**
    * Method for change modal state
    * @param {*} visible 
    */
    setModalVisible(visible) {

        this.setState({ isShowSearch: visible });
    }

    makeListFresh(callback) {
        this.setState(
            {
                pageNumber: 1,
                pageSize: 10,
                membersData: [],
                responseComes: false
            },
            () => {
                callback();
            }
        );
    }

    /**
     * prepareData when call the API
     */
    prepareData() {
        const {
            pageNumber, pageSize, groupId, memberName
        } = this.state;

        const data = {};
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        data.UserID = globals.userID
        data.GroupID = groupId;
        data.Name = memberName,
            data.Type = 'Members'
        return data;
    }

    /**
     * APi call for get members list
     */
    apicallGetMembersList() {
        if (globals.isInternetConnected === true) {
            this.setState({ loadingPagination: true, loading: true, isInternetFlag: true })
            API.getGroupAdmins(this.GetCommunityMembersResponseData, this.prepareData(), true)
        } else {
            this.props.hideLoader()
            this.setState({ isInternetFlag: false })
            // Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    static refreshGroupMember() {
        _this.makeListFresh(() => {
            _this.apicallGetMembersList();
        });
    }

    /**
 * Response of MyCommunity listing
 */
    GetCommunityMembersResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetGroupMembersResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                total = response.Data.TotalRecords;
                _this.props.navigation.setParams({
                    globalEventCount: response.Data.TotalRecords
                });
                if (this.state.pageNumber == 1) {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        membersData: response.Data.GroupResultUserPoco,
                    })
                } else {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        membersData: [...this.state.membersData, ...response.Data.GroupResultUserPoco],
                    })
                }
            } else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetCommunityMembersResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
  *  call when _sessionOnPres
  */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.apicallGetMembersList();
        });

    }

    renderFooter = () => {
        if (this.state.loadingPagination && this.state.responseComes) {
            return (

                <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />

            )
        } else {
            return (
                <View />
            )
        }
    };

    /**
     * Render FlatList Items
     */
    renderCommunityMembersList(item, index) {
        return (
            <View>
                {
                    this.renderMembersAccordingConditions(item, index)
                }
            </View>
        );
    }

    /**
     * Method of click connect button
     */
    clickConnectBtn() {
        let data = {
            ID: null,
            Note: '',
            ReceiverID: this.state.receiverID,
            SenderID: globals.userID

        }
        if (globals.isInternetConnected === true) {
            this.props.showLoader()
            API.saveMemberConnections(this.saveMemberConnectionsResponseData, data, true)
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    handleConnectModalStatus(visible, id) {
        this.setState({ visibleModal: visible, receiverID: id })
    }

    /**
 * 
 * Upadte TextInput Data
 */
    handleNotes(text) {
        this.setState({
            notes: text,
        });
    }

    /**
    * Render modal of cancel click of waiting for review
    */
    renderConnectModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.visibleModal}
                onRequestClose={() => {
                    this.handleConnectModalStatus(false, this.state.receiverID);
                }}
                onBackdropPress={() => {
                    this.handleConnectModalStatus(false, this.state.receiverID);
                }}
            >
                <View style={[styles.modalMainView]}>
                    <View style={styles.modalInnerMainView}>
                        <Text style={styles.modalUperText}>Your connection request is on it's way. Would you like to add a note to your request.</Text>
                        <View style={styles.underLineView}></View>
                        <Text style={styles.addNoteTxt}>Add Note:</Text>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                multiline={true}
                                maxLength={100}
                                returnKeyType="done"
                                autoCapitalize="none"
                                value={this.state.notes}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.handleNotes(text)}
                                placeholder={'Add Note..'}
                                style={[styles.textInputStyleContainer]}
                            />
                        </View>
                        <View style={styles.modalEndView}>
                            <TouchableOpacity style={styles.closeView} onPress={() => this.handleConnectModalStatus(false, this.state.receiverID)}>
                                <View style={styles.closeView}>
                                    <Text style={styles.closeTxt}>Close</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.sendView} onPress={() => this.clickConnectBtn(false, this.state.receiverID)}>
                                <View style={styles.sendView} >
                                    <Text style={styles.sendTxt}>Send</Text>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </View>
            </Modal>
        )
    }




    /**
     * Response of Accept Reject Invitation 
     */
    saveMemberConnectionsResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveMemberConnectionsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                this.handleConnectModalStatus(false, this.state.receiverID)
                this.clearStates()
                this.props.showLoader()
                this.apicallGetMembersList()
            }
            else {
                Alert.alert(globals.appName, response.Message);
                this.props.hideLoader()
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveMemberConnectionsResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    clearStates() {
        this.setState({
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            visibleModal: false,
            receiverID: 0,
            notes: ''
        })
    }

    /**
     * Method of render conditions according views in member lisr
     * @param {*} item 
     * @param {*} index 
     */
    renderMembersAccordingConditions(item, index) {
        const { membersData, loading } = this.state;
        const isMemberName = globals.checkObject(item, 'FullName');
        const isOrganizationName = globals.checkObject(item, 'OrganizationName');
        const isJobTitle = globals.checkObject(item, 'JobTitle');
        const isEmailAddress = globals.checkObject(item, 'EmailId');
        const isPhone = globals.checkObject(item, 'PhoneNO')
        const isMemberProImage = globals.checkImageObject(item, 'ProfilePicturePath');
        if (((item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0) && (item.ConnectionStatus == 0 || item.ConnectionStatus == 3)) ||
            (item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0 && item.ConnectionStatus == 1)) {
            return (
                <TouchableWithoutFeedback style={styles.mainParentStyle}>
                    <View style={{ flex: 1 }}>
                        <View
                            style={[styles.boxViewStyle,
                            { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },]}>
                            <View>
                                <TouchableOpacity onPress={() => (item.UserID == globals.userID) ?
                                    this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" }) : this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "groupMember" })}>

                                    <View style={styles.userinfoStyle}>
                                        <View style={styles.beforeImgViewStyle}>
                                            <Image
                                                source={{ uri: (isMemberProImage) ? item.ProfilePicturePath : globals.User_img }}
                                                style={styles.imgStyle}

                                            ></Image>
                                        </View>
                                        <View style={styles.textViewStyle}>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle, { fontSize: globals.font_14, marginBottom: 3, fontWeight: '500' }]}>
                                                {(isMemberName) ? item.FullName : '-'}
                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isJobTitle) ?
                                                    (isOrganizationName) ? item.JobTitle + " at " + item.OrganizationName : '' :
                                                    (isOrganizationName) ? item.JobTitle + item.OrganizationName : ''
                                                }

                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isEmailAddress) ? item.EmailId : '-'}

                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isPhone) ? item.PhoneNO : '-'}
                                            </Text>
                                        </View>{
                                            ((item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0) && (item.ConnectionStatus == 0 || item.ConnectionStatus == 3)) ?
                                                <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this.handleConnectModalStatus(true, item.UserID)}>
                                                    <View style={styles.lastImgViewEnd}>
                                                        <View style={styles.connectedBtnView}>
                                                            <Text style={styles.connectTextStyle}>Connect</Text>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                :
                                                (item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0 && item.ConnectionStatus == 1) ?
                                                    <View style={styles.lastImgViewEnd}>
                                                        <View style={[styles.connectedBtnView, { backgroundColor: colors.darkYellow }]}>
                                                            <Text style={styles.connectTextStyle}>Pending</Text>
                                                        </View>
                                                    </View> : null
                                        }

                                    </View>

                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            )
        } else {
            return (
                <TouchableWithoutFeedback style={styles.mainParentStyle}>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity style={{}} onPress={() => (item.UserID == globals.userID) ?
                            this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" }) : this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "groupMember" })}>
                            <View style={[styles.userinfoStyleConnected, {}]}>
                                <View style={styles.connectedTwoViews}>
                                    <View style={styles.beforeImgViewStyle}>
                                        <Image
                                            source={{ uri: (isMemberProImage) ? item.ProfilePicturePath : globals.User_img }}
                                            style={styles.imgStyle}
                                        ></Image>
                                    </View>
                                    <View style={styles.textViewStyle}>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle, { fontSize: globals.font_14, marginBottom: 3, fontWeight: '500' }]}>
                                            {(isMemberName) ? item.FullName : '-'}
                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isJobTitle) ?
                                                (isOrganizationName) ? item.JobTitle + " at " + item.OrganizationName : '' :
                                                (isOrganizationName) ? item.JobTitle + item.OrganizationName : ''
                                            }

                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isEmailAddress) ? item.EmailId : '-'}

                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isPhone) ? item.PhoneNO : '-'}
                                        </Text>
                                    </View>
                                </View>
                                {
                                    (globals.userID != item.UserID) ?
                                        <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Image source={images.Communities.connectedAttach} style={styles.connectedbtnImgStyle} />
                                        </View> : null
                                }
                            </View>

                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            )
        }

    }

    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.apicallGetMembersList();
            })
            this.onEndReachedCalledDuringMomentum = true;

        }
    };


    render() {
        const { serverErr, loading, memberName, membersData, responseComes, loadingPagination, isInternetFlag } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                {this.renderSearchModel()}
                {this.renderConnectModal()}
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (
                                (responseComes == false && loadingPagination == true) ?
                                    <View></View> :
                                    (membersData.length == 0 && responseComes) ?
                                        (membersData.length == 0 && memberName !== '') ?
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_MEMBERS_NOT_AVLB_SEARCH}</Text>
                                            </View> :
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_MEMBERS_NOT_AVLB}</Text>
                                            </View> :
                                        <View style={styles.mainParentStyle}>
                                            <FlatList
                                                style={{ flex: 1, marginTop: globals.screenHeight * 0.03 }}
                                                showsVerticalScrollIndicator={false}
                                                data={membersData}
                                                renderItem={({ item, index }) => this.renderCommunityMembersList(item, index)}
                                                extraData={this.state}
                                                bounces={false}
                                                keyExtractor={(index, item) => item.toString()}
                                                ListFooterComponent={this.renderFooter}
                                                onEndReached={this.handleLoadMore}
                                                onEndReachedThreshold={0.2}
                                                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                            />
                                        </View>
                            )
                            :
                            (
                                <ServerError loading={loading} onPress={() => this._tryAgain()} />
                            )
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GroupMembers);