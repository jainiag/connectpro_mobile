import React from 'react';
import { Text, View, KeyboardAvoidingView, TouchableOpacity, Modal, Alert, ScrollView, Image, Linking, ActivityIndicator, FlatList, TextInput, TouchableWithoutFeedback, Keyboard } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import Validation from '../../../../../../utils/validation';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import DocumentPicker from 'react-native-document-picker';
import CustomButton from '../../../../../../components/CustomButton/index';
import ImagePicker from 'react-native-image-crop-picker';
import RNFS from 'react-native-fs';
import HTML from 'react-native-render-html';
import { API } from '../../../../../../utils/api';
import Entypo from 'react-native-vector-icons/Entypo';
import ImageLoad from 'react-native-image-placeholder';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import { PermissionsAndroid } from 'react-native';
import Nointernet from '../../../../../../components/NoInternet/index'

const iPad = DeviceInfo.getModel();
let TAG = "GroupDiscussion Screen ::==="
let _this = null;
let gd_commentIndexTrue = '';
let gd_likeIndexTrue = '';
let gd_commentLikeIndex = '';
let gd_likeRefTypeID;
let videosnotallow;
class groupDiscussion extends React.Component {
    static navigationOptions = ({ navigation }) => {

        return {
            headerLeft:
                globals.ConnectProbackButton(navigation, 'Discussions'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => { _this.setSearchModalVisible(true); _this.setState({ gd_SearchKeywordDiscussion: '' }) }}

                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };


    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            gd_selected_category: [],
            gd_isInternetFlag: true,
            gd_email: '',
            gd_drop: false,
            gd_commentDrop: false,
            gd_commentIndexs: '',
            gd_indexs: '',
            gd_DiccussionCommunityID: this.props.navigation.state.params.groupId,
            gd_loading: false,
            gd_responseComes: false,
            gd_loadingPagination: false,
            gd_serverErr: false,
            gd_PageNumber: 1,
            gd_PageSize: 10,
            gd_DiscussionPostData: [],
            gd_TotalRecords: 0,
            gd_AttachmentList: [],
            gd_modalVisibleImages: false,
            gd_Attachmentimages: '',
            gd_refID: '',
            gd_isLike: false,
            gd_likesCount: '',
            gd_likecommentloader: false,
            gd_totalcommentCount: 0,
            gd_comments: [],
            gd_commentInfo: [],
            gd_commentInfoTopthree: [],
            gd_enableScrollViewScroll: true,
            gd_recentTopCommentID: 0,
            gd_commentPageNumber: 1,
            gd_commentPageSize: 3,
            gd_isFetching: false,
            gd_discussionId: '',
            gd_commentDiscussionID: '',
            gd_isShowCommentModal: false,
            gd_attachmentPopup: false,
            gd_selectedimages: null,
            gd_msgType: '',
            gd_singleFile: '',
            gd_SearchKeywordDiscussion: '',
            gd_isShowSearch: false,
            gd_communityDiscussionId: '',
            isallowkeyboard: true,
            oncrossbottmView: false,
            isOpenFile: false,
            isdownloadLoader: false,
        }
    }

    componentDidMount() {
        if (this.state.gd_DiccussionCommunityID == '' || this.state.gd_DiccussionCommunityID == undefined || this.state.gd_DiccussionCommunityID == null) {
            return null
        }
        else {
            this.setState({ gd_DiccussionCommunityID: this.props.navigation.state.params.groupId }, () => {
                this.makeApiCall()
            })
        }
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
    }

    async   requestReadStoragePermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,

            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.setState({ isOpenFile: true }, () => {
                    this.forceUpdate()
                })
                console.log('You can use the camera');
            } else {
                this.setState({ isOpenFile: false }, () => {
                    this.forceUpdate()
                })
                console.log('Camera permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    async  requestWriteStoragePermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,

            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.setState({ isOpenFile: true }, () => {
                    this.forceUpdate()
                })
                console.log('You can use the camera');
            } else {
                this.setState({ isOpenFile: false }, () => {
                    this.forceUpdate()
                })
                console.log('Camera permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }

    /*
  * handleLoadMore for pagination
  */
    handleLoadMore = () => {
        if (!this.onEndReachedPostCalledDuringMomentum) {
            this.setState({
                gd_PageNumber: this.state.gd_PageNumber + 1,
            }, () => {
                this.makeApiCall();
            })
            this.onEndReachedPostCalledDuringMomentum = true;
        }
    };


    /*
       * handleLoadMore for pagination
       */
    handleLoadMoreComments = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                gd_commentPageNumber: this.state.gd_commentPageNumber + 1,
            }, () => {
                this.makeCommentApiCall();
            })
            this.onEndReachedCalledDuringMomentum = true;
        }
    };


    /*
          * renderCommentFooter for pagination
          */
    renderCommentFooter = () => {
        if (this.state.gd_loadingPagination) {
            if (this.state.gd_responseComes) {
                return (
                    <View style={[styles.loaderInner, { marginBottom: (globals.screenHeight * 0.01) }]}>
                        <ActivityIndicator size="large" color={colors.bgColor} />
                    </View>
                )
            }

        } else {
            return (
                <View />
            )
        }
    };

    /*
     * renderFooter for pagination
    */
    renderFooter = () => {
        this.props.hideLoader();
        if (this.state.gd_loadingPagination) {
            if (this.state.gd_responseComes) {
                return (

                    <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
                )
            }

        } else {
            return (
                <View />
            )
        }
    };


    /*
        * updatePageCount for pagination
        */
    updatePageCount(count, callback) {
        this.setState({ gd_TotalRecords: count }, () => {
            callback();
        });
    }


    /*
        * Try again method
        */
    _tryAgain() {
        this.makeApiCall();
        this.setState({ gd_serverErr: false, gd_loading: false });
    }

    /**
     *  call when _sessionOnPres
     */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    /**
        * API call of DiscussionsList
        */
    makeApiCall() {

        {
            (this.state.gd_likecommentloader == true) ?
                this.props.hideLoader() :
                this.props.showLoader()
        }
        this.setState({ gd_loading: true })
        const data = {
            GroupId: this.state.gd_DiccussionCommunityID,
            PageNumber: this.state.gd_PageNumber,
            PageSize: this.state.gd_PageSize,
            RecentTopDiscussionID: this.state.gd_recentTopCommentID,
            SearchKeyword: this.state.gd_SearchKeywordDiscussion,
            UserId: JSON.parse(globals.userID),
        }
        if (globals.isInternetConnected == true) {
            this.props.showLoader()
            this.setState({ gd_isInternetFlag: true, gd_loadingPagination: true, oncrossbottmView: true, })
            API.getGroupDiscussion(this.getGroupDiscussionResponseData, data, true);
        } else {
            this.props.hideLoader();
            this.setState({ gd_loading: false, gd_isInternetFlag: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
        * Response callback of getGroupDiscussionResponseData
        */
    getGroupDiscussionResponseData = {
        success: response => {
            console.log(
                TAG,
                'getGroupDiscussionResponseData -> success : ',
                JSON.stringify(response)
            );

            if (response.StatusCode == 200) {
                if (this.state.gd_PageNumber == 1) {
                    const TopLike = response.Data

                    for (let i = 0; i < TopLike.length; i++) {
                        var TopLikeIdList = TopLike[i].DiscussionId
                        var TopLikesCount = TopLike[i].LikesCount
                    }

                    this.setState({
                        gd_singleFile: '', gd_selectedimages: null, gd_msgType: '', oncrossbottmView: false,
                        gd_loading: false, gd_responseComes: true, gd_loadingPagination: false, gd_drop: false,
                        gd_DiscussionPostData: response.Data, gd_refID: TopLikeIdList, gd_likesCount: TopLikesCount
                    })



                } else {
                    this.setState({
                        gd_singleFile: '', gd_selectedimages: null, gd_msgType: '', oncrossbottmView: false,
                        gd_loading: false, gd_responseComes: true, gd_loadingPagination: false, gd_refID: TopLikeIdList, gd_drop: false,
                        gd_DiscussionPostData: [...this.state.gd_DiscussionPostData, ...response.Data], gd_likesCount: TopLikesCount
                    })
                }

            }
            else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ gd_loading: false })
            }
            this.props.hideLoader()
            this.setState({ gd_loading: false })
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ gd_serverErr: true, gd_loading: false });
            console.log(
                TAG,
                'getGroupDiscussionResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ gd_loading: false, gd_loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                this.props.hideLoader();
                this.setState({ gd_msgType: '', gd_selectedimages: null, gd_singleFile: '' })
                Alert.alert(globals.appName, err.Message, [{ text: 'OK', onPress: this.setState({ gd_msgType: '', gd_selectedimages: null, gd_singleFile: '' }) }])
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ gd_loading: false })
        },
    };


    /**
            * method for render setModalVisible
            */
    setModalVisible(visible) {
        this.setState({ gd_modalVisibleImages: visible })
    }

    /**
         *  method for render comment option for download & delete
         */
    _renderCommentOptions(item, index) {
        this.setState({ gd_commentDrop: true, gd_commentIndexs: index })
    }
    /**
        *  method for render comment option for download & delete
        */
    _renderCommentOptions2(item, index) {
        // console.log("_renderCommentOptions2Pressed");
        this.setState({ gd_commentDrop: false, gd_commentIndexs: index })
    }

    /**
         *  method for render option for download & delete
         */
    _renderOptions(item, index) {
        this.setState({ gd_drop: true, gd_indexs: index })
    }
    /**
        *  method for render option for download & delete
        */
    _renderOptions2(item, index) {
        this.setState({ gd_drop: false, gd_indexs: index, gd_commentDrop: false, gd_commentIndexs: index })
    }

    /**
                * method for _clsoeModal
                */
    _clsoeModal() {
        this.setModalVisible(false)
    }

    /**
   * method for re nder setAttachmentPopupVisible
    */
    setAttachmentPopupVisible(visible) {
        if (visible == false) {
            this.setState({ gd_attachmentPopup: visible, gd_selectedimages: null, gd_singleFile: '', })
        }
        else {
            this.setState({ gd_attachmentPopup: visible, gd_selectedimages: null, gd_singleFile: '', })
        }

    }

    /**
     * status handle of search modal visibility
     */
    setSearchModalVisible(visible) {
        this.setState({ gd_isShowSearch: visible });
    }

    /**
   * method for render setCommentModalVisible
  */
    setCommentModalVisible(visible) {
        this.setState({ gd_isShowCommentModal: visible }, () => {
            (visible == false) ?
                this.onEndReachedCalledDuringMomentum = false
                :
                this.setState({ gd_commentInfo: [], gd_commentPageNumber: 1 }, () => {
                    this.makeCommentApiCall()
                })

        })
    }

    /**
* Method of navigateToWeb
* 
*/
    navigatePDFToWeb(isAttachmentURL) {
        const URL = isAttachmentURL;
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                console.log("supported" + URL);
                Linking.openURL(URL);
            } else {
                console.log("not supported " + URL);
                Linking.openURL(URL)
            }
        });
    }

    /**
    * method for render MimetypePdfView
    */
    renderMimetypePdfView(item, index) {
        isAttachmentURL = globals.checkImageObject(item, 'AttachmentURL');
        return (
            <View style={{
                height: globals.screenHeight * 0.09,
                width: globals.screenWidth * 0.16,
            }}>
                <TouchableOpacity onPress={() => this.navigatePDFToWeb((isAttachmentURL) ? item.AttachmentURL : 'https://www.google.com')}>
                    <Image resizeMode="contain" source={images.Discussion.pdf} style={styles.renderMimetypePdfimgView} />
                </TouchableOpacity>

            </View>
        )
    }

    downloadondevice(item, index) {
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        if (this.state.isOpenFile == true) {
            this.setState({ isdownloadLoader: true }, () => {
                this.dwonloadPDFFile(item, index)
            })
        } else {
            // Alert.alert(
            //     globals.appName,
            //     "Allow to access photos, media, and files on your device?",
            //     [{ text: 'OK', onPress: () => Linking.openSettings() },
            //     { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
            // );
        }
    }


    /**
     *  method for dwonloadPDFFile
     */
    dwonloadPDFFile(item, index) {
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        this.setState({ isdownloadLoader: true })
        var AttachmentLists = item.AttachmentList;
        for (let m = 0; m < AttachmentLists.length; m++) {
            var SelectedPDF = AttachmentLists[m].AttachmentURL
            var SelectedFileName = AttachmentLists[m].FileName
        }
        let ProfilePdf = SelectedPDF
        let FileNmaePdf = SelectedFileName
        let Filenamestr = FileNmaePdf.substring(0, FileNmaePdf.length - 4);
        let path = Platform.OS == 'android' ? RNFS.ExternalStorageDirectoryPath : RNFS.DocumentDirectoryPath;
        const absolutePath = `${path}/ConnectPro`;
        console.log("absolutePath ::", absolutePath);


        RNFS.exists(absolutePath)
            .then((result) => {
                console.log("result", result);
                if (result) {
                    RNFS.downloadFile({ fromUrl: ProfilePdf, toFile: absolutePath + `/${Filenamestr}.pdf` }).promise
                        .then((res) => {
                            console.log("res", res)
                            this.setState({ isdownloadLoader: false });
                            Alert.alert(globals.appName, "File download successfully.")
                        })
                        .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                } else {
                    RNFS.mkdir(absolutePath)
                        .then((success) => {
                            RNFS.downloadFile({ fromUrl: ProfilePdf, toFile: absolutePath + `/${Filenamestr}.pdf` }).promise
                                .then((res) => {
                                    console.log("resmkdir", res)
                                    this.setState({ isdownloadLoader: false });
                                    Alert.alert(globals.appName, "File download successfully.")
                                })
                                .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                        })
                        .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                }
            })
            .catch((error) => {
                console.log("errorerrorerror", error)
                RNFS.mkdir(absolutePath)
                    .then((success) => {
                        RNFS.downloadFile({ fromUrl: ProfilePdf, toFile: absolutePath + `/${Filenamestr}.pdf` }).promise
                            .then((res) => {
                                console.log("catchDIR", res)
                                this.setState({ isdownloadLoader: false });
                            })
                            .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                    })
                    .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
            });
    }


    downloadimagesondevice(item, index) {
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        if (this.state.isOpenFile == true) {
            this.setState({ isdownloadLoader: true }, () => {
                this.dwonloadImagesFile(item, index)
            })
        } else {
            // Alert.alert(
            //     globals.appName,
            //     "Allow to access photos, media, and files on your device?",
            //     [{ text: 'OK', onPress: () => Linking.openSettings() },
            //     { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
            // );
        }
    }

    /**
          *  method for dwonloadImagesFile
          */
    dwonloadImagesFile(item, index) {
        this.requestReadStoragePermission();
        this.requestWriteStoragePermission();
        this.setState({ isdownloadLoader: true })
        var AttachmentLists1 = item.AttachmentList;
        for (let m = 0; m < AttachmentLists1.length; m++) {
            var SelectedImg = AttachmentLists1[m].AttachmentURL
            var SelectedImgName = AttachmentLists1[m].FileName
            let ProfileImg = SelectedImg
            let imgNmae = SelectedImgName
            let imgNmaestr = imgNmae.substring(0, imgNmae.length - 4);
            let path = Platform.OS == 'android' ? RNFS.ExternalStorageDirectoryPath : RNFS.DocumentDirectoryPath;
            const absolutePath = `${path}/ConnectProImages`;

            RNFS.exists(absolutePath)
                .then((result) => {
                    if (result) {
                        RNFS.downloadFile({ fromUrl: ProfileImg, toFile: absolutePath + `/${imgNmaestr}.jpg` }).promise
                            .then((res) => {
                                this.setState({ isdownloadLoader: false });
                                {
                                    (m == AttachmentLists1.length - 1) ?
                                        Alert.alert(globals.appName, "Image download successfully.") :
                                        null
                                }

                            })
                            .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                    } else {
                        RNFS.mkdir(absolutePath)
                            .then((success) => {
                                RNFS.downloadFile({ fromUrl: ProfileImg, toFile: absolutePath + `/${imgNmaestr}.jpg` }).promise
                                    .then((res) => {
                                        console.log("resmkdir", res)
                                        this.setState({ isdownloadLoader: false });
                                        Alert.alert(globals.appName, "Image download successfully.")
                                    })
                                    .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                            })
                            .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                    }
                })
                .catch((error) => {
                    RNFS.mkdir(absolutePath)
                        .then((success) => {
                            RNFS.downloadFile({ fromUrl: ProfileImg, toFile: absolutePath + `/${imgNmaestr}.jpg` }).promise
                                .then((res) => {
                                    console.log("catchDIR", res)
                                    this.setState({ isdownloadLoader: false });
                                })
                                .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") });
                        })
                        .catch((err) => { this.setState({ isdownloadLoader: false }); Alert.alert("Something went wrong. Please check your storage permission.") })
                });
        }


    }

    /**
           *  method for image PopView
           */
    imagePopView() {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.state.gd_modalVisibleImages}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}>
                <View style={styles.p_tab_modalMainViewStyle}>
                    <View style={styles.p_tab_modalIconContainer}>
                        <AntDesign
                            name="closecircleo" size={globals.screenHeight * 0.05} color={colors.white} onPress={() => this._clsoeModal()}
                        />
                    </View>
                    <View style={styles.p_tab_modalInnerViewContainer}>
                        <ImageLoad
                            resizeMode='contain'
                            style={styles.p_tab_modalPicStyle}
                            source={{ uri: this.state.gd_Attachmentimages }}
                            isShowActivity={false}
                            placeholderSource={"https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg"}
                            placeholderStyle={styles.p_tab_modalPicStyle}

                        />

                    </View>
                </View>
            </Modal>
        )
    }


    /**
         * method for render MimetypeImageView
         */
    renderMimetypeImageView(item, index) {
        isAttachmentURL = globals.checkImageObject(item, 'AttachmentURL');
        return (
            <View style={{ margin: globals.screenWidth * 0.015 }}>


                <TouchableOpacity
                    onPress={() => {
                        this.setState({ gd_Attachmentimages: item.AttachmentURL }, () => {
                            this.setModalVisible(true)
                        })
                    }}>
                    <ImageLoad
                        resizeMode='contain'
                        style={[styles.imgStyles,]}
                        source={{ uri: (isAttachmentURL) ? item.AttachmentURL : "https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg" }}
                        isShowActivity={false}
                        placeholderSource={"https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg"}
                        placeholderStyle={styles.imgStyles}

                    />
                </TouchableOpacity>

            </View>
        )
    }

    /**
           *  method for clear TextFields
           */
    clearTextFields() {
        this.setState({
            gd_selected_category: [],
            gd_email: '',
            gd_drop: false,
            gd_commentDrop: false,
            gd_commentIndexs: '',
            gd_indexs: '',
            gd_DiccussionCommunityID: this.props.navigation.state.params.groupId,
            gd_loading: false,
            gd_responseComes: false,
            gd_loadingPagination: false,
            gd_serverErr: false,
            gd_PageNumber: 1,
            gd_PageSize: 10,
            gd_DiscussionPostData: [],
            gd_TotalRecords: 0,
            gd_modalVisibleImages: false,
            gd_Attachmentimages: '',
            gd_isLike: false,
            gd_likesCount: '',
            gd_likecommentloader: false,
            gd_comments: '',
            gd_commentInfo: [],
            gd_enableScrollViewScroll: true,
            gd_recentTopCommentID: 0,
            gd_commentPageNumber: 1,
            gd_commentPageSize: 3,
            gd_isFetching: false,
            gd_discussionId: '',
            gd_isShowCommentModal: false,
            gd_SearchKeywordDiscussion: '',
            gd_isShowSearch: false,
            oncrossbottmView: false,
            // isOpenFile: false,
            //  isdownloadLoader: false 
        })
    }


    /**
       *  method for delete Data
       */
    deleteAPIcall(item, index) {
        this.clearTextFields()
        this.setState({ gd_drop: false, gd_loading: true })
        this.props.showLoader();
        if (globals.isInternetConnected == true) {
            this.props.showLoader()
            this.setState({ gd_isInternetFlag: true, gd_loadingPagination: true, oncrossbottmView: true, })
            API.deleteGroupDiscussion(this.deleteDiscussionResponseData, item.DiscussionId, true);
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
          * Response callback of deleteDiscussionResponseData
          */
    deleteDiscussionResponseData = {
        success: response => {
            console.log(
                TAG,
                'deleteDiscussionResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                this.props.showLoader();
                this.makeApiCall()
            }
            else {
                this.props.hideLoader()
                Alert.alert(globals.appName, response.Message)
            }
        },
        error: err => {
            this.props.hideLoader();
            console.log(
                TAG,
                'deleteDiscussionResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
         *  method for change CommentView
         */
    changeCommentView = (index, item) => {
        this.setState({ gd_commentDiscussionID: item.DiscussionId }, () => {
            const { gd_DiscussionPostData } = this.state;
            const dataDiscussionPostData = gd_DiscussionPostData;
            for (let i = 0; i < dataDiscussionPostData.length; i++) {
                if (index == i) {
                    gd_commentIndexTrue = index
                    dataDiscussionPostData[index].isSelected = !dataDiscussionPostData[index].isSelected;
                    this.setState({ gd_commentDiscussionID: item.DiscussionId }, () => {
                        this.makeCommentApiCall()
                    })

                } else {
                    dataDiscussionPostData[i].isSelected = false;
                }
            }
            this.setState({ DiscussionPostData: dataDiscussionPostData }, () => { });
        })

    };


    /**
     * API call of DiscussionsCommentList
     */
    makeCommentApiCall() {
        const data = {
            GroupDiscussionID: this.state.gd_commentDiscussionID,
            PageNumber: this.state.gd_commentPageNumber,
            PageSize: this.state.gd_commentPageSize,
            RecentTopCommentID: this.state.gd_recentTopCommentID,
            UserID: JSON.parse(globals.userID)
        }
        if (globals.isInternetConnected == true) {
            this.setState({ gd_loadingPagination: true })
            API.getGroupDiscussionComment(this.getGroupDiscussionCommentResponseData, data, true);
        } else {
            this.props.hideLoader();
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
           * Response callback of discussionCommentsResponseData
           */
    getGroupDiscussionCommentResponseData = {
        success: response => {
            console.log(
                TAG,
                'getGroupDiscussionCommentResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                const recenttotalTopComment = response.Data.CommentInfo
                for (let i = 0; i < recenttotalTopComment.length; i++) {
                    var totalcommentCountList = recenttotalTopComment[i].TotalRecords
                }
                if (this.state.gd_commentPageNumber == 1) {
                    const recentTopComment = response.Data.CommentInfo

                    for (let i = 0; i < recentTopComment.length; i++) {
                        var particularCommentIDList = recentTopComment[i].GroupDiscussionId
                        var recentTopCommentIDList = recentTopComment[i].RecentTopCommentID
                    }

                    this.setState({
                        gd_responseComes: true, gd_loadingPagination: false, gd_communityDiscussionId: particularCommentIDList,
                        gd_isFetching: false, gd_recentTopCommentID: recentTopCommentIDList, gd_totalcommentCount: totalcommentCountList,
                        gd_commentInfo: response.Data.CommentInfo, gd_userInfo: response.Data.UserInfo, gd_commentInfoTopthree: response.Data.CommentInfo,
                    })
                } else {
                    this.setState({
                        gd_responseComes: true, gd_loadingPagination: false, gd_isFetching: false, gd_totalcommentCount: totalcommentCountList,
                        gd_recentTopCommentID: this.state.recentTopCommentID, gd_communityDiscussionId: particularCommentIDList,
                        gd_commentInfo: [...response.Data.CommentInfo, ...this.state.gd_commentInfo],
                    })
                }
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }
            this.setState({ gd_isFetching: false })
        },
        error: err => {
            this.setState({ gd_responseComes: true, gd_loadingPagination: false, gd_isFetching: false });
            console.log(
                TAG,
                'getGroupDiscussionCommentResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }

        },
        complete: () => {
        },
    };

    /**
           * Render LoadPreviosComment
           */
    LoadPreviosComment() {
        this.onEndReachedCalledDuringMomentum = false
        {
            (this.state.gd_commentInfo == []) ?
                null :
                this.handleLoadMoreComments()
        }

    }




    /**
       * Method of Checked Like btn
       * 
       */
    onChecked(item, index) {
        gd_likeIndexTrue = index;
        this.LikeApiCall(item.DiscussionId, !item.IsLiked, 1)
    }


    /**
   * Method of comment's Checked Like btn
   * 
   */
    onCommentLikeChecked(item, index) {
        gd_commentLikeIndex = index;
        this.LikeApiCall(item.CommentId, !item.IsLiked, 2)
    }


    /**
            *  method for LikeApiCall
            */
    LikeApiCall(refId, isLiked, RefTypeID) {
        gd_likeRefTypeID = RefTypeID;
        if (globals.isInternetConnected == true) {
            const data = {
                IsLike: isLiked,
                RefID: refId,
                RefTypeID: RefTypeID,
                UserID: JSON.parse(globals.userID)
            }
            {
                API.saveDiscussionsLike(this.saveDiscussionsLikesResponseData, data, false);
            }

        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
             * Response callback of saveDiscussionsLikesResponseData
             */
    saveDiscussionsLikesResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveDiscussionsLikesResponseData -> success : ',
                JSON.stringify(response)
            );
            if (gd_likeRefTypeID === 2) {
                let array1 = this.state.gd_commentInfo
                array1[gd_commentLikeIndex]["LikesCount"] = response.Data.LikesCount;
                array1[gd_commentLikeIndex]["IsLiked"] = !array1[gd_commentLikeIndex]["IsLiked"]
                this.setState({
                    gd_commentInfo: [...array1]
                })
            } else {
                let array2 = this.state.gd_DiscussionPostData
                array2[gd_likeIndexTrue]["LikesCount"] = response.Data.LikesCount;
                array2[gd_likeIndexTrue]["IsLiked"] = !array2[gd_likeIndexTrue]["IsLiked"]
                this.setState({
                    gd_DiscussionPostData: [...array2]
                })
            }
            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({ gd_likecommentloader: true, gd_PageNumber: 1, }, () => {
                })
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }

        },
        error: err => {
            console.log(
                TAG,
                'saveDiscussionsLikesResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
        },
    };



    /**
    * API call of commentDeleteApiCall
    */
    commentDeleteApiCall(item, index) {

        this.setState({ loading: true })
        const data = {
            GroupDiscussionId: item.GroupDiscussionId,
            CommentId: item.CommentId,
        }
        if (globals.isInternetConnected == true) {
            this.setState({ isInternetFlag: true })
            API.deletDiscussionsComment(this.deleteCommentsResponseData, data, true);
        } else {
            this.props.hideLoader();
            this.setState({ loading: false, isInternetFlag: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
       * Response callback of deleteCommentsResponseData
       */
    deleteCommentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'deleteCommentsResponseData -> success : ',
                JSON.stringify(response)
            );
            let array = this.state.gd_DiscussionPostData
            array[gd_commentIndexTrue]["CommentsCount"] = response.Data.CommentsCount;
            this.setState({
                gd_DiscussionPostData: [...array]
            })
            if (response.StatusCode == 200 && response.Result == true) {
                if (this.state.gd_isShowCommentModal == true) {
                    this.setState({
                        gd_commentPageNumber: 1,
                        gd_recentTopCommentID: 0,
                        gd_commentDrop: false,
                        gd_commentInfo: [],
                    }, () => {
                        this.makeCommentApiCall()
                        this.onEndReachedCalledDuringMomentum = false;
                    })
                } else {
                    this.setState({
                        gd_commentPageNumber: 1,
                        gd_recentTopCommentID: 0,
                        gd_commentDrop: false
                    }, () => {
                        this.makeCommentApiCall()
                        this.onEndReachedCalledDuringMomentum = false;
                    })
                }


            }
            else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ gd_loading: false })
            }
            this.props.hideLoader()
            this.setState({ gd_loading: false })
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ gd_serverErr: true, gd_loading: false });
            console.log(
                TAG,
                'deleteCommentsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ gd_loading: false })
        },
    };


    checkcommentwhitespace(comment) {
        console.log("comment", comment);
        let regex = /^(?!\s)[a-zA-Z0-9_\s-]*$/;
        if (!regex.test(comment)) {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME,
                [{ text: 'OK', onPress: () => this.setState({ gd_comments: '' }) }])
        } else {
            this.props.showLoader()
            const data = {
                Comment: comment,
                GroupDiscussionId: this.state.gd_commentDiscussionID,
                UserID: JSON.parse(globals.userID)
            }
            if (globals.isInternetConnected == true) {
                API.saveGroupDiscussionComment(this.saveDiscussionsCommentsResponseData, data, true);
            } else {
                this.props.hideLoader();
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
            }
        }
    }

    /**
         *  API call for SaveComment
         */
    makeSaveCommentApiCall(index, item, comment) {
        if (comment == undefined || comment == '') {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_COMMENTNAME);
        } else {
            this.checkcommentwhitespace(comment)
        }
    }


    /**
           * Response callback of saveDiscussionsCommentsResponseData
           */
    saveDiscussionsCommentsResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveDiscussionsCommentsResponseData -> success : ',
                JSON.stringify(response)
            );
            let array = this.state.gd_DiscussionPostData
            array[gd_commentIndexTrue]["CommentsCount"] = response.Data.CommentsCount;
            this.setState({
                gd_DiscussionPostData: [...array]
            })

            if (response.StatusCode == 200 && response.Result == true) {

                this.setState({
                    gd_comments: '',
                    gd_commentPageNumber: 1,
                    gd_commentPageSize: 3,
                    gd_PageNumber: 1,
                }, () => {
                    this.makeCommentApiCall()
                })
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
            this.setState({ gd_isFetching: false });
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ gd_serverErr: true, gd_loading: false, gd_isFetching: false });
            console.log(
                TAG,
                'saveDiscussionsCommentsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            Alert.alert(globals.appName, err.Message)

        },
        complete: () => {
            this.props.hideLoader();
        },
    };



    /**
     *  method for render Comment View
     */
    renderCommentView(item, index) {
        const isCommentLikesCount = globals.checkObject(item, 'LikesCount');
        const isComment = globals.checkObject(item, 'Comment');
        const isFullName = globals.checkObject(item, 'FullName');
        const isCreatedDateStr = globals.checkObject(item, 'CreatedDateStr');
        const isProfilePicturePath = globals.checkImageObject(item, 'ProfilePicturePath');

        return (
            <TouchableWithoutFeedback onPress={() => this._renderCommentOptions2(null, null)}>
                <View style={{ flex: 1 }}>
                    <View style={[styles.itemUpperCommentView]}>
                        <View style={styles.beforeImgView}>
                            <Image
                                source={{ uri: (isProfilePicturePath) ? item.ProfilePicturePath : globals.User_img }}
                                style={styles.profileImageStyle}

                            />
                        </View>
                        <View style={styles.nameView}>
                            <Text numberOfLines={1} style={[styles.firstnamelastname, { width: globals.screenWidth * 0.43 }]}>{(isFullName) ? item.FullName : " "}</Text>
                            <Text numberOfLines={1} style={[styles.innerTexts, { width: globals.screenWidth * 0.46, color: colors.lightBlack }]}>{(isCreatedDateStr) ? "Posted on" + " " + item.CreatedDateStr : "Posted On" + '-'}</Text>
                        </View>
                        <View style={styles.dotView}>
                            {
                                (this.state.gd_commentDrop === false) ?
                                    <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue}
                                        onPress={() => this._renderCommentOptions(item, index)} /> :
                                    (index == this.state.gd_commentIndexs) ?
                                        <View style={styles.commentDelbtns}>
                                            <TouchableOpacity onPress={() => this.commentDeleteApiCall(item, index)}>
                                                <AntDesign style={{
                                                    width: globals.screenWidth * 0.05, height: globals.screenWidth * 0.05,
                                                    marginLeft:
                                                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 15 : 3, marginTop:
                                                        iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 9 : 2
                                                }} name='delete' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : 15} color={colors.redColor}
                                                />
                                            </TouchableOpacity>
                                        </View>

                                        :
                                        <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue}
                                            onPress={() => this._renderCommentOptions(item, index)} />
                            }

                        </View>
                    </View>
                    {
                        (isComment) ?

                            <View style={[styles.commentVIews, { paddingBottom: 0 }]}>
                                {/* <HTML uri={item.Comment}
                                            baseFontStyle={styles.htmltagStyle} html={(item.Comment) ? item.Comment : '-'} /> */}
                                <Text style={styles.commentVIewsText}>{(isComment) ? item.Comment : ''}</Text>
                                <View style={[styles.commentnView, { backgroundColor: colors.white }]}>

                                    <TouchableOpacity onPress={() => this.onCommentLikeChecked(item, index)}
                                        style={styles.innerLikeViews}>
                                        <Text style={styles.likeTextstyle}>Like</Text>
                                        {
                                            (item.IsLiked == true) ?
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.filledLike}
                                                    style={styles.likeimgStyle} />
                                                :
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.like}
                                                    style={styles.likeimgStyle} />
                                        }
                                        <Text style={styles.likecountText}>{(isCommentLikesCount) ? item.LikesCount : ''}</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>


                            :
                            null
                    }
                </View>
            </TouchableWithoutFeedback>
        )
    }


    /**
        * Render Comments modal method
        */
    renderCommentsModel() {
        const { gd_commentInfo, gd_totalcommentCount, gd_loadingPagination } = this.state
        if (!this.state.gd_isShowCommentModal)
            return null

        return (

            <View style={{ flex: 1 }}>
                <Modal
                    animationType="none"
                    transparent={false}
                    visible={this.state.gd_isShowCommentModal}
                    onRequestClose={() => {
                        this.setCommentModalVisible(false);
                    }}
                >


                    <View
                        style={[styles.mainContainer, { marginBottom: globals.screenHeight * 0.04 }]}
                    >

                        <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                            <TouchableOpacity onPress={() => this.setCommentModalVisible(false)}>
                                <Icon name="close" size={globals.screenHeight * 0.04} color="blue" />
                            </TouchableOpacity></View>
                        {

                            (gd_commentInfo.length === gd_totalcommentCount || gd_totalcommentCount == undefined || gd_loadingPagination) ?
                                null :
                                <TouchableOpacity onPress={() => this.LoadPreviosComment()}>
                                    <View style={styles.loadCommentmodalView}>
                                        <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                            name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                        />
                                        <Text style={[styles.api_paymentCommentTextStyle]}>Load Provious Comments</Text>
                                    </View>
                                </TouchableOpacity>

                        }


                        <ScrollView bounces={false} style={[styles.scrollFlatStyle]} keyboardShouldPersistTaps='always'>

                            <FlatList
                                data={gd_commentInfo}
                                extraData={this.state}
                                scrollEnabled={true}
                                horizontal={false}
                                showsVerticalScrollIndicator={false}
                                bounces={false}
                                ListHeaderComponent={this.renderCommentFooter}
                                onEndReached={this.handleLoadMoreComments}
                                onEndReachedThreshold={0.2}
                                contentContainerStyle={{
                                    flexGrow: 1,
                                }}
                                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                listKey={(x, i) => i.toString()}
                            />



                        </ScrollView>
                    </View>


                </Modal>
            </View>

        );
    }


    /**
              * method for convert decode Text into URL
              */
    replaceAll(str, term, replacement) {
        return str.replace(new RegExp(this.escapeRegExp(term), 'g'), replacement);

    }

    /**
                  *method for convert decode Text into URL
                  */
    escapeRegExp(string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
    }



    /**
    * Method of navigateToWeb
    * 
    */
    navigateFileUrlToWeb(isAttachmentURL) {
        const URL = isAttachmentURL[1];
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                console.log("supported" + URL);
                Linking.openURL(URL);
            } else {
                console.log("not supported " + URL);
                Linking.openURL(URL)
            }
        });
    }

    /**
    *  method for render Item View
    */
    renderItemView(item, index) {
        const { gd_commentInfo, gd_totalcommentCount } = this.state;

        const isDescription = globals.checkObject(item, 'Description');
        const isFirstName = globals.checkObject(item, 'FirstName');
        const isLastName = globals.checkObject(item, 'LastName');
        const isLikesCount = globals.checkObject(item, 'LikesCount');
        const isCommentsCount = globals.checkObject(item, 'CommentsCount');
        const isCreatedDateStr = globals.checkObject(item, 'CreatedDateStr');
        const isProfilePicturePath = globals.checkImageObject(item, 'ProfilePicturePath');
        const isTime = (isCreatedDateStr) ? item.CreatedDateStr.substring(11) : '-'
        const Description = (isDescription) ? item.Description : '-';
        const DescreptedText = decodeURIComponent(Description);
        const url = JSON.stringify(item.Description)
        let FileUrl = url.includes("href")


        if (FileUrl) {
            const finaluRL = DescreptedText.trim()
            var newStr = this.replaceAll(finaluRL, '"', "'")

            const DescreptedUrl = '"' + newStr + '"';
            const finaluRL1 = DescreptedUrl.trim()
            // console.log("finaluRL1-->--------------------" + finaluRL1)

            var result = finaluRL1.match(/href=\'([^']+)\'/);
            // console.log("href-->--------------------" + result)
        }

        const attachment = item.AttachmentList[0];
        const mimetype = JSON.stringify(attachment.MimeType)
        let MimeTypeimage = mimetype.includes("png") || mimetype.includes("jpeg") || mimetype.includes("jpg")
        let MimeTypepdf = mimetype.includes("pdf")
        const topThreeComment = gd_commentInfo.slice(0, 3);


        return (
            <TouchableWithoutFeedback onPress={() => this._renderOptions2(item, index)}>
                <View style={styles.mainItemView}>
                    {this.renderCommentsModel()}
                    <View style={styles.itemUpperView}>
                        <View style={styles.beforeImgView}>
                            <Image
                                source={{ uri: (isProfilePicturePath) ? item.ProfilePicturePath : globals.User_img }}
                                style={styles.profileImageStyle}

                            />
                        </View>
                        <View style={styles.nameView}>
                            <Text numberOfLines={1} style={[styles.firstnamelastname, { width: globals.screenWidth * 0.43 }]}>{(isFirstName) ? (isLastName) ? item.FirstName + " " + item.LastName : '-' : '-'}</Text>
                            <Text numberOfLines={1} style={[styles.innerTexts, { width: globals.screenWidth * 0.46 }]}>{(isCreatedDateStr) ? "Posted on" + " " + item.CreatedDateStr : "Posted On" + '-'}</Text>
                        </View>
                        <View style={styles.dotView}>
                            {(this.state.gd_drop === false) ?
                                (item.UserID == globals.userID) ?
                                    <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue} onPress={() => this._renderOptions(item, index)} />
                                    :
                                    <View />
                                :
                                (index == this.state.gd_indexs) ?

                                    <View style={styles.delndownloadStyle}>
                                        <TouchableOpacity onPress={() => this.deleteAPIcall(item, index)}>
                                            <AntDesign style={{ padding: globals.screenWidth * 0.015 }} name='delete' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : 18} color={colors.redColor}
                                            />
                                        </TouchableOpacity>
                                        {
                                            (MimeTypepdf) ?
                                                <TouchableOpacity onPress={() => (Platform.OS == "ios") ? this.dwonloadPDFFile(item, index) : (this.state.isOpenFile == false) ? this.downloadondevice(item, index) : this.dwonloadPDFFile(item, index)}>
                                                    {
                                                        (this.state.isdownloadLoader == true) ?
                                                            <ActivityIndicator size={"small"} color={colors.warmBlue} />
                                                            :
                                                            <AntDesign style={{ padding: globals.screenWidth * 0.015 }} name='clouddownloado' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : 18} color={colors.warmBlue}
                                                            />
                                                    }

                                                </TouchableOpacity> :
                                                (MimeTypeimage) ?
                                                    <TouchableOpacity onPress={() => (Platform.OS == "ios") ? this.dwonloadImagesFile(item, index) : (this.state.isOpenFile == false) ? this.downloadimagesondevice(item, index) : this.dwonloadImagesFile(item, index)}>
                                                        {
                                                            (this.state.isdownloadLoader == true) ?
                                                                <ActivityIndicator size={"small"} color={colors.warmBlue} />
                                                                :
                                                                <AntDesign style={{ padding: globals.screenWidth * 0.015 }} name='clouddownloado' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 28 : 20} color={colors.warmBlue}
                                                                />
                                                        }

                                                    </TouchableOpacity> :
                                                    null
                                        }
                                    </View>

                                    :
                                    (item.UserID == globals.userID) ?
                                        <Icon name='dots-vertical' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 24} color={colors.warmBlue} onPress={() => this._renderOptions(item, index)} />
                                        :
                                        <View />
                            }
                        </View>
                    </View>


                    {
                        (FileUrl == true) ?
                            <View>
                                <View style={styles.mainSecondView}>
                                    <View style={styles.itemLowerView}>
                                        <HTML uri={DescreptedText} onLinkPress={() => this.navigateFileUrlToWeb(result)}
                                            baseFontStyle={styles.htmltagStyle} html={isDescription ? DescreptedText : '-'} />
                                    </View>
                                </View>
                                <View style={styles.commentnView}>

                                    <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                        style={styles.innercommentnview}>
                                        <Text style={styles.likeTextstyle}>Like</Text>

                                        {
                                            (item.IsLiked == true) ?
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.filledLike}
                                                    style={styles.likeimgStyle} />
                                                :
                                                <Image resizeMode="contain"
                                                    source={images.Discussion.like}
                                                    style={styles.likeimgStyle} />
                                        }

                                        <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                        <Text style={styles.likeTextstyle}>Comment</Text>
                                        <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                        <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                    </TouchableOpacity>
                                </View>

                                {
                                    (item.isSelected == true) ?
                                        <View style={styles.commentsView}>
                                            <View style={styles.commentListingView}>
                                                <View style={styles.mainCommentView}>
                                                    <View style={styles.beforeImgView}>
                                                        <Image
                                                            source={{ uri: item.image }}
                                                            style={styles.profileImageStyle}
                                                        />
                                                    </View>
                                                    <View>
                                                        <View style={styles.textInputViewStyle}>
                                                            <TextInput
                                                                placeholder='Add a Comment'
                                                                blurOnSubmit={true}
                                                                placeholderTextColor={colors.lightGray}
                                                                style={styles.textInputStyle}
                                                                onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                onFocus={() => { this.setState({ gd_attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                value={this.state.gd_comments}
                                                                onChangeText={text => {
                                                                    this.setState({
                                                                        gd_comments: text,
                                                                    });
                                                                }}
                                                            />
                                                            <Icon
                                                                name='send'
                                                                size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                color={colors.warmBlue}
                                                                onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.gd_comments)}
                                                                style={{ alignSelf: 'center' }}
                                                            />
                                                        </View>
                                                    </View>
                                                </View>
                                                {
                                                    (gd_totalcommentCount >= 4) ?
                                                        <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                            <View style={styles.flexDirectionStyle}>
                                                                <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                    name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                />
                                                                <Text style={styles.api_paymentTextStyle}>Load Provious Comments</Text>
                                                            </View>
                                                        </TouchableOpacity>
                                                        : null
                                                }


                                                <FlatList
                                                    data={topThreeComment}
                                                    extraData={this.state}
                                                    showsVerticalScrollIndicator={false}
                                                    bounces={false}
                                                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                    renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                    listKey={(x, i) => i.toString()}
                                                />

                                            </View>

                                        </View>
                                        :
                                        null
                                }
                            </View>
                            :
                            (MimeTypepdf == true) ?
                                <View>
                                    <View style={styles.mainSecondView}>
                                        <View style={styles.itemLowerView}>
                                            <HTML baseFontStyle={[styles.htmltagStyle]} numberOfLines={1} html={DescreptedText} />
                                            <FlatList
                                                style={styles.renderMimetypeImagemainView}
                                                data={item.AttachmentList}
                                                extraData={this.state}
                                                bounces={false}
                                                showsVerticalScrollIndicator={false}
                                                renderItem={({ item, index }) => this.renderMimetypePdfView(item, index)}
                                                listKey={(x, i) => i.toString()} />
                                        </View>
                                    </View>
                                    <View style={styles.commentnView}>
                                        <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                            style={styles.innercommentnview}>
                                            <Text style={styles.likeTextstyle}>Like</Text>
                                            {
                                                (item.IsLiked == true) ?
                                                    <Image resizeMode="contain"
                                                        source={images.Discussion.filledLike}
                                                        style={styles.likeimgStyle} />
                                                    :
                                                    <Image resizeMode="contain"
                                                        source={images.Discussion.like}
                                                        style={styles.likeimgStyle} />
                                            }

                                            <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                            <Text style={styles.likeTextstyle}>Comment</Text>
                                            <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                            <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {
                                        (item.isSelected == true) ?
                                            <View style={styles.commentsView}>
                                                <View style={styles.commentListingView}>
                                                    <View style={styles.mainCommentView}>

                                                        <Image
                                                            source={{ uri: item.image }}
                                                            style={styles.profileImageStyle}
                                                        />
                                                        <View>
                                                            <View style={styles.textInputViewStyle}>
                                                                <TextInput
                                                                    placeholder='Add a Comment'
                                                                    blurOnSubmit={true}
                                                                    placeholderTextColor={colors.lightGray}
                                                                    style={styles.textInputStyle}
                                                                    value={this.state.gd_comments}
                                                                    onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                    onFocus={() => { this.setState({ gd_attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                    onChangeText={text => {
                                                                        this.setState({
                                                                            gd_comments: text,
                                                                        });
                                                                    }}
                                                                />
                                                                <Icon
                                                                    name='send'
                                                                    size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                    color={colors.warmBlue}
                                                                    onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.gd_comments)}
                                                                    style={{ alignSelf: 'center' }}
                                                                />
                                                            </View>
                                                        </View>
                                                    </View>
                                                    {
                                                        (gd_totalcommentCount >= 4) ?
                                                            <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                                <View style={styles.flexDirectionStyle}>
                                                                    <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                        name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                    />
                                                                    <Text style={styles.api_paymentTextStyle}>Load Provious Comments</Text>
                                                                </View>
                                                            </TouchableOpacity>
                                                            : null
                                                    }


                                                    <FlatList
                                                        data={topThreeComment}
                                                        extraData={this.state}
                                                        showsVerticalScrollIndicator={false}
                                                        bounces={false}
                                                        onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                        renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                        listKey={(x, i) => i.toString()}
                                                    />

                                                </View>

                                            </View>
                                            :
                                            null
                                    }
                                </View>
                                :
                                (MimeTypeimage == true) ?
                                    <View>
                                        <View style={styles.mainSecondView}>
                                            <View style={styles.itemLowerViewimg}>
                                                <HTML baseFontStyle={[styles.htmltagStyle]} numberOfLines={1} html={DescreptedText} />
                                                {this.imagePopView(item.AttachmentList)}
                                                <FlatList
                                                    style={styles.renderMimetypeImagemainView}
                                                    data={item.AttachmentList}
                                                    numColumns={2}
                                                    extraData={this.state}
                                                    bounces={false}
                                                    showsVerticalScrollIndicator={false}
                                                    renderItem={({ item, index }) => this.renderMimetypeImageView(item, index)}
                                                    listKey={(x, i) => i.toString()} />
                                            </View>
                                        </View>
                                        <View style={styles.commentnView}>
                                            <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                                style={styles.innercommentnview}>
                                                <Text style={styles.likeTextstyle}>Like</Text>
                                                {
                                                    (item.IsLiked == true) ?
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.filledLike}
                                                            style={styles.likeimgStyle} />
                                                        :
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.like}
                                                            style={styles.likeimgStyle} />
                                                }
                                                <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                                <Text style={styles.likeTextstyle}>Comment</Text>
                                                <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                                <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                            </TouchableOpacity>
                                        </View>

                                        {
                                            (item.isSelected == true) ?
                                                <View style={styles.commentsView}>
                                                    <View style={styles.commentListingView}>
                                                        <View style={styles.mainCommentView}>

                                                            <Image
                                                                source={{ uri: item.image }}
                                                                style={styles.profileImageStyle}
                                                            />
                                                            <View>
                                                                <View style={styles.textInputViewStyle}>
                                                                    <TextInput
                                                                        placeholder='Add a Comment'
                                                                        placeholderTextColor={colors.lightGray}
                                                                        style={styles.textInputStyle}
                                                                        blurOnSubmit={true}
                                                                        value={this.state.gd_comments}
                                                                        onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                        onFocus={() => { this.setState({ gd_attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                        onChangeText={text => {
                                                                            this.setState({
                                                                                gd_comments: text,
                                                                            });
                                                                        }}
                                                                    />
                                                                    <Icon
                                                                        name='send'
                                                                        size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                        color={colors.warmBlue}
                                                                        onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.gd_comments)}
                                                                        style={{ alignSelf: 'center' }}
                                                                    />
                                                                </View>
                                                            </View>
                                                        </View>
                                                        {
                                                            (gd_totalcommentCount >= 4) ?
                                                                <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                                    <View style={styles.flexDirectionStyle}>
                                                                        <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                            name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                        />
                                                                        <Text style={styles.api_paymentTextStyle}>Load Provious Comments</Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                                : null
                                                        }


                                                        <FlatList
                                                            data={topThreeComment}
                                                            extraData={this.state}
                                                            showsVerticalScrollIndicator={false}
                                                            bounces={false}
                                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                            renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                            listKey={(x, i) => i.toString()}
                                                        />

                                                    </View>

                                                </View>
                                                :
                                                null
                                        }
                                    </View>
                                    :
                                    <View>
                                        <View style={styles.mainSecondView}>
                                            <View style={styles.itemLowerView}>
                                                <HTML baseFontStyle={styles.htmltagStyle} html={isDescription ? DescreptedText : '-'} />
                                            </View>
                                        </View>
                                        <View style={styles.commentnView}>
                                            <TouchableOpacity onPress={() => this.onChecked(item, index)}
                                                style={styles.innercommentnview}>
                                                <Text style={styles.likeTextstyle}>Like</Text>

                                                {
                                                    (item.IsLiked == true) ?
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.filledLike}
                                                            style={styles.likeimgStyle} />
                                                        :
                                                        <Image resizeMode="contain"
                                                            source={images.Discussion.like}
                                                            style={styles.likeimgStyle} />
                                                }
                                                <Text style={styles.likecountText}>{(isLikesCount) ? item.LikesCount : ''}</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={styles.innercommentnview} onPress={() => this.changeCommentView(index.toString(), item)}>
                                                <Text style={styles.likeTextstyle}>Comment</Text>
                                                <Image resizeMode="contain" source={images.Discussion.comment} style={styles.commentimgStyle} />
                                                <Text style={styles.likecountText}>{(isCommentsCount) ? item.CommentsCount : ''}</Text>
                                            </TouchableOpacity>
                                        </View>

                                        {
                                            (item.isSelected == true) ?
                                                <View style={styles.commentsView}>
                                                    <View style={styles.commentListingView}>
                                                        <View style={styles.mainCommentView}>

                                                            <Image
                                                                source={{ uri: item.image }}
                                                                style={styles.profileImageStyle}
                                                            />
                                                            <View>
                                                                <View style={styles.textInputViewStyle}>
                                                                    <TextInput
                                                                        placeholder='Add a Comment'
                                                                        placeholderTextColor={colors.lightGray}
                                                                        style={styles.textInputStyle}
                                                                        blurOnSubmit={true}
                                                                        value={this.state.gd_comments}
                                                                        onBlur={() => { this.setState({ isallowkeyboard: true }); this.forceUpdate() }}
                                                                        onFocus={() => { this.setState({ gd_attachmentPopup: false, isallowkeyboard: false }); this.forceUpdate() }}
                                                                        onChangeText={text => {
                                                                            this.setState({
                                                                                gd_comments: text,
                                                                            });
                                                                        }}
                                                                    />
                                                                    <Icon
                                                                        name='send'
                                                                        size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : 25}
                                                                        color={colors.warmBlue}
                                                                        onPress={() => this.makeSaveCommentApiCall(index.toString(), item, this.state.gd_comments)}
                                                                        style={{ alignSelf: 'center' }}
                                                                    />
                                                                </View>
                                                            </View>
                                                        </View>
                                                        {
                                                            (gd_totalcommentCount >= 4) ?
                                                                <TouchableOpacity style={styles.api_buttonViewStyle} onPress={() => this.setCommentModalVisible(true)}>
                                                                    <View style={styles.flexDirectionStyle}>
                                                                        <AntDesign style={{ marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.001 : null }}
                                                                            name='reload1' size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : 15} color={colors.lightGray}
                                                                        />
                                                                        <Text style={styles.api_paymentTextStyle}>Load Provious Comments</Text>
                                                                    </View>
                                                                </TouchableOpacity>
                                                                : null
                                                        }


                                                        <FlatList
                                                            data={topThreeComment}
                                                            extraData={this.state}
                                                            showsVerticalScrollIndicator={false}
                                                            bounces={false}
                                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                            renderItem={({ item, index }) => this.renderCommentView(item, index)}
                                                            listKey={(x, i) => i.toString()}
                                                        />

                                                    </View>

                                                </View>
                                                :
                                                null
                                        }
                                    </View>

                    }

                </View>
            </TouchableWithoutFeedback>
        );
    }


    onCrossApicall() {
        this.props.showLoader()
        this.setState({ gd_SearchKeywordDiscussion: '', gd_responseComes: false, gd_loadingPagination: false, oncrossbottmView: true }, () => {
            this.makeApiCall()
        })
    }

    /**
   * Search modal render function
   */
    renderSearchModel() {
        const { gd_SearchKeywordDiscussion } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.gd_isShowSearch}
                onRequestClose={() => {
                    this.setSearchModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <View style={[styles.vwHeaderStyleSearch, styles.vwFlexDirectionRowStyleSearch]}>
                        <TouchableOpacity onPress={() => { this.onCrossApicall(); this.setSearchModalVisible(false); }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Discussions'}
                        </Text>
                    </View>
                    <View style={styles.mainContainer}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={'Search Discussion..'}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ gd_SearchKeywordDiscussion: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={gd_SearchKeywordDiscussion}
                                autoCapitalize="none"
                            />
                        </View>
                        <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByDiscussion()}>
                            <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    /**
        * click on search based on community name
        */
    seacrhByDiscussion() {
        const { gd_SearchKeywordDiscussion } = this.state;
        this.setState({ gd_SearchKeywordDiscussion: gd_SearchKeywordDiscussion }, () => {
            this.setSearchModalVisible(false);
            this.searchDiscussion();
        })
    }

    makeListFresh(callback) {
        this.setState(
            {
                gd_PageNumber: 1,
                gd_PageSize: 10,
                gd_DiscussionPostData: [],
                gd_responseComes: false
            },
            () => {
                callback();
            }
        );
    }

    /**
* Method of get  communities based on search filter
*/

    searchDiscussion() {
        _this.makeListFresh(() => {
            _this.makeApiCall();
        });
    }

    pickMultiple() {
        const { gd_selectedimages } = this.state;
        ImagePicker.openPicker({
            multiple: true,
            waitAnimationEnd: false,
            includeExif: true,
            forceJpg: true,
        }).then(images => {
            this.setState({
                gd_selectedimages: images.map(i => {
                    console.log('received image', i);
                    videosnotallow = i.mime.includes("video/mp4") || i.mime.includes("video/mp3") || i.mime.includes("mp4") || i.mime.includes("mp3") || i.mime.includes("video")
                    if (videosnotallow) {
                        this.setAttachmentPopupVisiblefromvideo(false)
                        console.log("not allow");
                    } else {
                        return { uri: i.path, width: i.width, height: i.height, mime: i.mime, filename: i.filename };
                    }

                })

            });

        }).catch((e) => {
            if (e.message !== "User cancelled image selection") {
                if (Platform.OS === "ios") {
                    Alert.alert(
                        globals.appName,
                        e.message,
                        [{ text: 'OK', onPress: () => Linking.openSettings() },
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],
                    );
                } else {
                    console.log(e.message ? "ERROR" + e.message : "ERROR" + e);
                }
            } else {
                console.log(TAG, e.message);
            }
        });
    }

    removePdfs() {
        this.setState({ gd_singleFile: '' })
    }


    setAttachmentPopupVisiblefromvideo(visibility) {
        if (visibility == false) {
            Alert.alert(globals.appName, "Only images you can post", [{ text: 'OK', onPress: videosnotallow = '' }])
            this.setState({ gd_attachmentPopup: visible, gd_selectedimages: null, gd_singleFile: '' })
        }

    }

    /**
          * render selectPdfFile
          */
    async selectPdfFile() {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
            });
            console.log('res : ' + JSON.stringify(res));
            console.log('URI : ' + res.uri);
            console.log('Type : ' + res.type);
            console.log('File Name : ' + res.name);
            console.log('File Size : ' + res.size);
            this.setState({ gd_singleFile: res });
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                console.log(TAG, "CANCEL :", err);
            } else {
                console.log(TAG, "ERROR :", err);
                throw err;
            }
        }
    }



    removeMultipleImgs(indexx) {
        this.state.gd_selectedimages.splice((indexx - 1), 1)
        this.setState({ gd_selectedimages: this.state.gd_selectedimages })
        this.forceUpdate()
    }


    /**
       * render AttachmentPopup
       */
    AttachmentPopup() {
        const { gd_selectedimages, gd_attachmentPopup, gd_singleFile } = this.state;

        return (
            (gd_attachmentPopup == true) ?
                <View style={styles.ApmainContainer}>

                    <AntDesign style={styles.attcrossicon}
                        name="closecircleo" size={globals.screenHeight * 0.03} color={colors.black} onPress={() => this.setAttachmentPopupVisible(false)}
                    />
                    <View style={styles.attContentStyle}>
                        {(this.state.gd_singleFile == '') ?
                            <TouchableOpacity onPress={() => this.pickMultiple()}>
                                <Entypo tyle={styles.crossIconsStyle}
                                    name="images"
                                    size={globals.screenHeight * 0.077}
                                    style={{ marginLeft: (gd_selectedimages) ? globals.screenWidth * 0.25 : null }}
                                    color={colors.warmBlue} />
                            </TouchableOpacity>
                            :
                            <View style={{ flexDirection: 'row', marginHorizontal: globals.screenWidth * 0.015 }}>
                                <Text numberOfLines={1} style={styles.pdftextSTyle}>{gd_singleFile.name}</Text>
                                <Entypo style={styles.crossIconsStyle}
                                    name="circle-with-cross"
                                    size={globals.screenHeight * 0.02}
                                    color={colors.warmBlue} onPress={() => this.removePdfs()} />
                            </View>
                        }
                        {
                            (gd_selectedimages) ? <ScrollView style={styles.scrollViewStyle}>
                                {gd_selectedimages ? gd_selectedimages.map((i, index) =>
                                    <View style={{ flexDirection: 'row', marginRight: globals.screenWidth * 0.04 }}>
                                        {

                                            (gd_selectedimages == []) ?
                                                <TouchableOpacity onPress={() => { this.selectPdfFile() }}>
                                                    <Image resizeMode="contain" source={images.Discussion.pdf} style={styles.attrenderMimetypePdfimgView} />
                                                </TouchableOpacity>
                                                :
                                                <>
                                                    <Text numberOfLines={1} style={styles.SelectedimagesStyle}>
                                                        {

                                                            (Platform.OS === 'android') ?
                                                                "selectedImage" + index + ".png" : i.filename
                                                        }
                                                    </Text>
                                                    {

                                                        <Entypo style={styles.crossIconsStyle}
                                                            name="circle-with-cross" size={globals.screenHeight * 0.02} color={colors.warmBlue} onPress={() => this.removeMultipleImgs(index)} />

                                                    }
                                                </>
                                        }

                                    </View>
                                ) : null}
                            </ScrollView>
                                :
                                <TouchableOpacity onPress={() => { this.selectPdfFile() }}>
                                    <Image resizeMode="contain" source={images.Discussion.pdf} style={[styles.attrenderMimetypePdfimgView, { tintColor: colors.warmBlue }]} />
                                </TouchableOpacity>
                        }

                    </View>

                </View>
                :
                null
        )
    }

    makePostApiCall() {
        const { gd_selectedimages, gd_singleFile, gd_msgType } = this.state;
        this.props.showLoader()
        var arrMedia = [];

        if (gd_singleFile) {
            arrMedia.push({
                name: 'Files-0',
                fileName: 'file.pdf',
                type: 'application/pdf',
                uri: gd_singleFile.uri,
            })
        } else {
            if (gd_selectedimages !== null && gd_selectedimages !== undefined) {
                gd_selectedimages ? gd_selectedimages.map((i, index) =>
                    arrMedia.push({
                        name: 'Files-' + index,
                        fileName: 'file' + index + '.png',
                        type: 'image/png',
                        uri: i.uri,
                    })


                ) : null
            }
        }



        const PostDetails = {
            HyperlinkURL: '',
            HyperlinkName: '',
            name: '',
            Description: this.state.gd_msgType,
            UserID: JSON.parse(globals.userID),
            PlainTextDesc: this.state.gd_msgType,
            GroupId: this.state.gd_DiccussionCommunityID
        };



        if (globals.isInternetConnected == true) {
            this.props.showLoader()
            this.setState({ gd_isInternetFlag: true, gd_responseComes: false, gd_loadingPagination: true, oncrossbottmView: true, })
            API.savegroupdiscussions(this.saveCommunityDiscussionResponseData, { 'PostDetails': JSON.stringify(PostDetails) },
                arrMedia, true);

        } else {
            this.props.hideLoader();
            this.setState({ gd_loading: false, gd_isInternetFlag: false })
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
          * render UploadPdfApiCall
          */
    UploadPostApiCall() {
        this.setState({ gd_attachmentPopup: false, gd_SearchKeywordDiscussion: '' })
        Keyboard.dismiss();
        const { gd_selectedimages, gd_singleFile, gd_msgType } = this.state;
        if (gd_msgType == '' || gd_msgType == null || gd_msgType == undefined) {
            if (gd_selectedimages == null || gd_selectedimages == undefined || gd_selectedimages == '') {
                if (gd_singleFile == '') {
                    Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME);
                }
                else {
                    this.makePostApiCall()
                }
            }
            else {
                this.makePostApiCall()
            }
        }
        else {
            this.checkisvalidmsgtype(gd_msgType)
        }
    }


    /**
        * check white space in starting only
        */
    checkisvalidmsgtype(gd_msgType) {
        let regex = /^[^\s]+(\s+[^\s]+)*$/;
        if (!regex.test(gd_msgType)) {
            Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_NAME,
                [{ text: 'OK', onPress: () => this.setState({ gd_msgType: '' }) }])
        }
        else if (gd_msgType.includes("http") || gd_msgType.includes("https") || gd_msgType.includes("www")) {
            if (Validation.validateURLWithText(gd_msgType)) {
                this.makePostApiCall()
            }
            else {
                Alert.alert(globals.appName, globals.MESSAGE.COMMUNITIES.CREATE_DISSCOMM_LINK,
                    [{ text: 'OK', onPress: () => this.setState({ gd_msgType: '' }) }])
            }
        }
        else {
            this.makePostApiCall()
        }
    }

    /**
        * Response callback of saveCommunityDiscussionResponseData
        */
    saveCommunityDiscussionResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveCommunityDiscussionResponseData -> success : ',
                JSON.stringify(response)
            );

            if (response.StatusCode == 200 && response.Result == true) {
                this.setState({
                    gd_msgType: '',
                    gd_PageNumber: 1,
                })
                this.makeApiCall()
            }
            else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ gd_loading: false, gd_msgType: '', gd_selectedimages: null, gd_singleFile: '', gd_attachmentPopup: false })
            }
            this.props.hideLoader();
            this.setState({ gd_loading: false })
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ gd_serverErr: true, gd_loading: false });
            console.log(
                TAG,
                'saveCommunityDiscussionResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ gd_loading: false, gd_loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                this.props.hideLoader();
                this.setState({ gd_msgType: ' ', gd_selectedimages: null, gd_singleFile: '' })
                Alert.alert(globals.appName, err.Message, [{ text: 'OK', onPress: this.setState({ gd_msgType: ' ', gd_selectedimages: null, gd_singleFile: '' }) }])
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ gd_loading: false })
        },
    };



    render() {
        const { oncrossbottmView, isallowkeyboard, gd_serverErr, gd_responseComes, gd_SearchKeywordDiscussion, gd_DiccussionCommunityID, gd_loadingPagination, gd_DiscussionPostData, gd_loading, gd_isInternetFlag } = this.state;

        return (
            <TouchableWithoutFeedback onPress={() => this._renderOptions2(null, null)}>

                <View style={styles.mainContainer}>
                    {this.renderSearchModel()}
                    {(Platform.OS == "ios") ? null : this.AttachmentPopup()}
                    {

                        (!gd_isInternetFlag) ?
                            <Nointernet loading={gd_loading} onPress={() => this._tryAgain()} /> :
                            (gd_serverErr === false) ?
                                (
                                    (gd_responseComes == false && gd_loadingPagination == true) ?
                                        <View></View> :
                                        ((gd_DiscussionPostData.length == 0 || gd_DiscussionPostData == []) && gd_responseComes) ?
                                            ((gd_DiscussionPostData.length == 0 || gd_DiscussionPostData == []) && gd_SearchKeywordDiscussion !== '') ?
                                                <View style={globalStyles.nodataStyle}>
                                                    <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DISCUSSION_POST_DATA_NOT_AVLB_SEARCH}</Text>
                                                </View>
                                                :
                                                <View style={globalStyles.nodataStyle}>
                                                    <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DISCUSSION_GROUP_POST_DATA_NOT_AVLB}</Text>
                                                </View>
                                            :
                                            <View style={styles.mainContainer}>

                                                {/* <ScrollView scrollEnabled={this.state.gd_enableScrollViewScroll} bounces={false} showsVerticalScrollIndicator={false}> */}
                                                <FlatList
                                                    data={gd_DiscussionPostData}
                                                    renderItem={({ item, index }) => this.renderItemView(item, index)}
                                                    keyExtractor={(index, item) => item.toString()}
                                                    extraData={this.state}
                                                    bounces={false}
                                                    ListFooterComponent={this.renderFooter}
                                                    onEndReached={this.handleLoadMore}
                                                    onEndReachedThreshold={0.2}
                                                    onMomentumScrollBegin={() => { this.onEndReachedPostCalledDuringMomentum = false; }}
                                                    style={{
                                                        marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?
                                                            globals.screenHeight * 0.012 : 0, marginBottom: (oncrossbottmView) ? globals.screenHeight * 0.11 : globals.screenHeight * 0.01
                                                    }}
                                                />
                                                {/* </ScrollView> */}
                                            </View>
                                )
                                :

                                <View style={globalStyles.serverErrViewContainer}>
                                    <Text style={globalStyles.serverTextStyle}>
                                        {globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_PLZ_TRY_AGAIN}
                                    </Text>
                                    <TouchableOpacity style={globalStyles.serverButtonStyle} onPress={() => this._tryAgain()}>
                                        <CustomButton
                                            text={globals.BTNTEXT.LOGINSCREEN.CUSTOM_BTN_TEXT}
                                            backgroundColor={colors.bgColor}
                                            color={colors.white}
                                            loadingStatus={gd_loading}
                                        />
                                    </TouchableOpacity>
                                </View>


                    }

                    {(isallowkeyboard == true) ?
                        <KeyboardAvoidingView enabled behavior={(Platform.OS == "ios") ? "position" : null} keyboardVerticalOffset={(Platform.OS == "ios") ? globals.screenHeight * 0.09 : 0}>
                            {(Platform.OS == "ios") ? this.AttachmentPopup() : null}
                            <View style={[styles.sendBtnViewMainContainer, {
                                height: globals.screenHeight * 0.12,
                                position: (oncrossbottmView) ? 'absolute' : null,
                                bottom: (oncrossbottmView) ? 0 : null,
                            }]} >

                                <View style={styles.iconAndInputContainer}>

                                    <View style={[styles.inputViewContainer, { height: (Platform.OS == "android") ? globals.screenHeight * 0.065 : globals.screenHeight * 0.048 }]}>
                                        <TextInput
                                            style={styles.textInputStyleforBotton}
                                            placeholder={'Type Message...'}
                                            blurOnSubmit={true}
                                            multiline={true}
                                            maxLength={1000}
                                            // numberOfLines = {10}
                                            returnKeyType="done"
                                            showsHorizontalScrollIndicator={false}
                                            showsVerticalScrollIndicator={false}
                                            onFocus={() => this.setState({ isallowkeyboard: true })}
                                            value={this.state.gd_msgType}
                                            onChangeText={text => {
                                                this.setState({
                                                    gd_msgType: text,
                                                });
                                            }}
                                            autoFocus={false}
                                            autoCapitalize="none"
                                            ref={input => {
                                                textInput = input;
                                            }}
                                            placeholderTextColor={colors.lightGray}
                                        />
                                        <TouchableOpacity style={styles.attchmentView} onPress={() => this.setAttachmentPopupVisible(true)}>
                                            <Image source={images.Discussion.attchment}
                                                style={styles.attchmentStyle}
                                                resizeMode="contain" /></TouchableOpacity>
                                    </View>
                                    <View style={styles.sendIconViewContainer}>
                                        {
                                            <TouchableOpacity onPress={() => this.UploadPostApiCall()}>
                                                <Image
                                                    source={images.Discussion.chatBtn}
                                                    style={styles.sendIconStyle}
                                                    resizeMode="contain"
                                                />
                                            </TouchableOpacity>

                                        }

                                    </View>
                                </View>
                            </View>
                        </KeyboardAvoidingView>
                        :
                        null
                    }

                </View>

            </TouchableWithoutFeedback>

        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(groupDiscussion);