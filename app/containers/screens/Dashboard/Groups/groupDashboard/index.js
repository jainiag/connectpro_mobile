import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ImageBackground, Alert, Modal } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../utils/globals';
import globalStyles from '../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../assets/styles/color';
import * as images from '../../../../../assets/images/map';
import { API } from '../../../../../utils/api';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImageLoad from 'react-native-image-placeholder';
import { showLoader, hideLoader } from '../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Groups from '../groupListing'
import loginScreen from '../../../AuthenticationScreens/loginScreen';
import CommunityGroups from '../../../Dashboard/Communities/Community_Dashboard/Groups/index';

const iPad = DeviceInfo.getModel();
let TAG = "GroupDashboard Screen ::==="
let _this = null;
class GroupDashboard extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      headerLeft:
        globals.ConnectProbackButton(navigation, 'Group Dashboard'),
      headerStyle: globalStyles.ConnectPropheaderStyle,

    }
  };


  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      groupsDetailData: {},
      communityData: [],
      joincommunityModal: false,
      leavecommunityModal: false,
      isFromProp: this.props.navigation.state.params.isFromProp,
      groupId: this.props.navigation.state.params.groupId,
      communityId: this.props.navigation.state.params.communityID,
      joinClick: false,
      groupsModel: [],
      groupUser: [],
      loading:false,
      GridViewItems: [
        {
          key: 'Discussions',
          icon: images.Communities.dicussion,
        },
        {
          key: 'Members',
          icon: images.Communities.memberss,
        },
        {
          key: 'Resources',
          icon: images.EventDashboard.resources,
        },
        {
          key: 'Admins',
          icon: images.Communities.admins,
        },
      ],
    }
  }




  componentDidMount() {
    const {groupUser,isFromProp} = this.state;
    if(isFromProp == "Inactive" && (groupUser.GroupStatus == false || groupUser.GroupStatus == undefined)){
    }else{
      this.makeAPICall()
    }
  }

  /**
* API call of groupDashboardInfo
*/
  makeAPICall() {
    this.props.showLoader()
    this.setState({ loading: true })
    if (globals.isInternetConnected == true) {
      API.groupDashboardInfo(this.groupInfoResponseData, true, this.state.communityId, this.state.groupId, globals.userID);
    } else {
      this.props.hideLoader();
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
     * Response callback of groupInfoResponseData
     */
  groupInfoResponseData = {
    success: response => {
      console.log(
        TAG,
        'groupInfoResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.setState({ groupsDetailData: response.Data, groupsModel: response.Data.GroupsModel, groupUser: response.Data.groupUser, communityData: response.Data.communityData },()=>{
          setTimeout(() => {
            this.props.hideLoader()
          }, 1000);
        })

      }
      else {
        this.setState({ loading: false },()=>{
          setTimeout(() => {
            this.props.hideLoader()
          }, 1000);
        })
        Alert.alert(globals.appName, response.Message)
        this.props.hideLoader()
      }
      
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'groupInfoResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };

  /**
     *  call when _sessionOnPres
     */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }
  

  /**
  * clearStates
  */
  clearStates() {
    this.setState({
      groupsDetailData: {},
      groupsModel: [],
      groupUser: [],
      action: '',
      joinClick: false,
      leavecommunityModal: false,
      joincommunityModal: false
    })
  }




  /**
         * Response callback of joinOrLeaveGroupResponseData
         */
  joinOrLeaveGroupResponseData = {
    success: response => {
      console.log(
        TAG,
        'joinOrLeaveGroupResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.clearStates()
        this.makeAPICall()
        Groups.UpdatedData()
         CommunityGroups.UpdatedData()
      }
      else {
        Alert.alert(globals.appName, response.Message);
      }
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'joinOrLeaveGroupResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      Alert.alert(globals.appName, err.Message)
    },
    complete: () => {
    },
  };








  /**
* method for Select Particular Item
*/
  setSelection(item, index) {
    const { GridViewItems, communityData, groupUser } = this.state;
    if (groupUser.GroupUserStatus == 0 || groupUser.GroupUserStatus == 3 || groupUser.GroupUserStatus == 5) {
      Alert.alert(globals.appName, "Join the group to view the Group Information");
    } else {
      const dataGridViewItems = GridViewItems;
      this.setState({ joinClick: false });
      console.log('indexItem : ', `${JSON.stringify(dataGridViewItems)}index${index}`);
      for (let i = 0; i < dataGridViewItems.length; i++) {
        if (index == i && item.key == 'Discussions') {
          dataGridViewItems[index].isSelected = true;
          this.props.navigation.navigate('groupDiscussion', { groupId: this.state.groupId })
        } else if (index == i && item.key == 'Members') {
          dataGridViewItems[index].isSelected = true;
          this.props.navigation.navigate('GroupMembers', { groupId: this.state.groupId })
        } else if (index == i && item.key == 'Resources') {
          dataGridViewItems[index].isSelected = true;
          this.props.navigation.navigate('ResourcesGroups', { groupId: this.state.groupId })
        } else if (index == i && item.key == 'Admins') {
          dataGridViewItems[index].isSelected = true;
          this.props.navigation.navigate('GroupAdmins', { groupId: this.state.groupId })
        } else {
          dataGridViewItems[i].isSelected = false;
        }
      }
      this.setState({ GridViewItems: dataGridViewItems }, () => {
      });
    }
  }


  /**
 * method for Leave the community
 */
  clickProceedLeaveCommunity() {
    this.handleLaeaveCommunityModalStatus(false, this.state.communityId, this.state.groupId, this.state.action)
    if (globals.isInternetConnected == true) {
      API.joinOrLeaveGroup(this.joinOrLeaveGroupResponseData, true, this.state.groupId, this.state.action, globals.userID);
    }
    else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }


  /**
     * Handle status for cancel click of join
     * @param {*} visible 
     */
  handleJoinCommunityModalStatus(visible, communityId, GroupID, Action) {
    CommunityGroups.refreshList();
    this.setState({ joincommunityModal: visible, communityId: communityId, groupID: GroupID, action: Action })
  }

  /**
   * Handle status for cancel click of leave
   * @param {*} visible 
   */
  handleLaeaveCommunityModalStatus(visible, communityId, GroupID, Action) {
    this.setState({ leavecommunityModal: visible, communityId: communityId, groupID: GroupID, action: Action })
  }

  /**
      * Render modal of leave community
      */
  renderLeaveCommunityModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.leavecommunityModal}
        onRequestClose={() => {
          this.handleLaeaveCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
        onBackdropPress={() => {
          this.handleLaeaveCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
      >
        <View style={styles.modalMainView}>
          <View style={styles.modalInnerMainView}>
            <View style={styles.popupinnerViewStyle}>
              <Text style={styles.popupheaderStyle}>{"Are you sure you want to leave this group?"}</Text>
              <View style={styles.btnViewStyle}>
                <TouchableOpacity onPress={() => this.handleLaeaveCommunityModalStatus(false, this.state.communityId, this.state.groupId, this.state.action)}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                      name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                    <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.clickProceedLeaveCommunity()}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                      name={"check"} color={colors.white} style={styles.vectorStyle} />
                    <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }

  /**
            * method for Join the community
            */
  clickProceedJoinCommunity() {
    this.handleJoinCommunityModalStatus(false, this.state.communityId, this.state.groupId, this.state.action)
    if (globals.isInternetConnected == true) {
      API.joinOrLeaveGroup(this.joinOrLeaveGroupResponseData, true, this.state.groupId, this.state.action, globals.userID);
    }
    else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
     * Render modal of leave community
     */
  renderJoinCommunityModal() {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.joincommunityModal}
        onRequestClose={() => {
          this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
        onBackdropPress={() => {
          this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action);
        }}
      >
        <View style={styles.modalMainView}>
          <View style={styles.modalInnerMainView}>
            <View style={styles.popupinnerViewStyle}>
              <Text style={styles.popupheaderStyle}>{"Are you sure you want to join this group?"}</Text>
              <View style={styles.btnViewStyle}>
                <TouchableOpacity onPress={() => this.handleJoinCommunityModalStatus(false, this.state.communityID, this.state.groupID, this.state.action)}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                      name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                    <Text style={[styles.btnTextsStyle]}>{"No"}
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.clickProceedJoinCommunity()}>
                  <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                    <Entypo
                      size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                      name={"check"} color={colors.white} style={styles.vectorStyle} />
                    <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    )
  }


  /**
  * Render FlatList Items
  */
  _renderItems(item, index) {
    return (
      <View style={styles.vsItemParentStyle}>
        <View style={[styles.gridViewStyle]}>
          <TouchableOpacity
            onPress={() => {
              this.setSelection(item, index);
            }}
          >
            <View style={styles.gridViewBlockStyle}>
              <Image source={item.icon} style={styles.imgStyle} resizeMode="contain" />
              <Text numberOfLines={1} style={styles.gridViewInsideTextItemStyle}>
                {' '}
                {item.key}{' '}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  render() {
    const { groupsDetailData, groupsModel, GridViewItems, groupUser, isFromProp, communityData } = this.state;
    const isImagePath = globals.checkImageObject(groupsModel, 'ImagePath');
    const isName = globals.checkObject(groupsModel, 'Name');
    const isMembersCount = globals.checkObject(groupsModel, 'MembersCount');
    const isGroupTypeID = globals.checkObject(groupsModel, 'GroupTypeID');


    return (

      <View style={styles.mainParentStyle}>
        {

          (isFromProp == "Inactive" && (groupUser.GroupStatus == false || groupUser.GroupStatus == undefined)) ?
            <View style={styles.waiting_approvalViewContainerfornolonger}>
              <Text style={styles.sorryText}>Sorry!</Text>
              <Text style={styles.nolongerText}>This Group Is No Longer Available</Text>
              <TouchableOpacity onPress={() => this.props.navigation.goBack(null)}
                style={[styles.touchableStyle2, { marginLeft: globals.screenWidth * 0.02 }]}
              >
                <View>
                  <Text style={styles.btntextStyless}>My Groups</Text>
                </View>
              </TouchableOpacity>
            </View>
            :
          <View style={styles.mainParentStyle}>
            {this.renderJoinCommunityModal()}
            {this.renderLeaveCommunityModal()}
            <View style={styles.bannerView}>
              <ImageLoad
                style={styles.adImageStyle}
                source={{ uri: (isImagePath) ? groupsModel.ImagePath : globals.DarkSquare_img }}
                isShowActivity={false}
                placeholderSource={{ uri: globals.DarkSquare_img }}
                placeholderStyle={styles.adImageStyle}
              />
              <View style={styles.lockviewStyle}>
                {
                  (isGroupTypeID) ?
                    (groupsModel.GroupTypeID == 1) ?
                      <View></View> :
                      <FontAwesome
                        name="lock"
                        size={iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05}
                        color={colors.white}
                        style={{ marginRight: globals.screenWidth * 0.02 }}
                      />
                    :
                    null
                }

                <Text style={styles.eventNameStyle}>{(isName) ? groupsModel.Name : ''}</Text>
              </View>
              {
                (groupUser.GroupUserStatus == 0 || groupUser.GroupUserStatus == 3 || groupUser.GroupUserStatus == 5) ?
                  <TouchableOpacity style={styles.joinButtonStyle} onPress={() => this.handleJoinCommunityModalStatus(true, this.state.communityId, groupsModel.GroupID, "join")}>
                    <Text style={styles.joinTextStyle}>Join</Text>
                  </TouchableOpacity>
                  :
                  (groupUser.GroupUserStatus == 2 || groupUser.GroupUserStatus == 6) ?
                    <TouchableOpacity style={styles.joinButtonStyle} onPress={() => this.handleLaeaveCommunityModalStatus(true, this.state.communityId, groupsModel.GroupID, "leave")}>
                      <Text style={styles.joinTextStyle}>Leave</Text>
                    </TouchableOpacity> :
                    (isGroupTypeID) ?
                      (groupUser.GroupUserStatus == 1) ?
                        <View style={styles.waiting_apprivalBtnStyle}>
                          <Ionicons
                            name="md-time"
                            size={globals.screenHeight * 0.02}
                            color={colors.white}
                            style={{ marginRight: globals.screenWidth * 0.02 }}
                          />
                          <Text style={styles.joinTextStyle}>Waitiing for Approval</Text>
                        </View> :
                        null :
                      null
              }
              {
                // (isMembersCount) ?
                //   <View style={styles.eventmembercountStyle}>
                //     <Image resizeMode="contain" source={images.Communities.member}
                //       style={[styles.memberimgStyle, { tintColor: colors.white }]} />
                //     <Text style={[styles.membetText, { color: colors.white }]}>{(isMembersCount) ? groupsModel.MembersCount : ''}</Text>
                //   </View>
                //   :
                //   null
              }
            </View>
            {
              (groupUser.GroupUserStatus == 1) ?
                <View style={styles.waiting_approvalViewContainer}>
                  <Image
                    source={images.EventDashboard.attendees}
                    style={styles.waiting_approvalIconStyle}
                    resizeMode="contain"
                  />
                  <Text style={styles.waiting_approvalTextStyle}>Join the group to view the Group Information</Text>
                </View> :
                <ImageBackground
                  source={images.Group.GroupBG}
                  style={styles.ed_dashboardView}
                >
                  <FlatList
                    style={styles.fvStyle}
                    data={GridViewItems}
                    renderItem={({ item, index }) => this._renderItems(item, index)}
                    numColumns={2}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                  />
                </ImageBackground>
            }


          </View>
        }
      </View>


    );
  }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GroupDashboard);