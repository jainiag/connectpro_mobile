import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../../utils/globals';
import * as colors from '../../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import { screenHeight, screenWidth } from '../../../../../utils/globals';

const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  bannerView: {
    // marginTop:globals.screenHeight * 0.001,
    height: globals.screenHeight * 0.25,
    width: globals.screenWidth,
    // backgroundColor:"blue"
  },
  lockviewStyle: {
    flexDirection: 'row',
    position: 'absolute',
    right: 0,
    left: 0,
    alignItems: 'center', justifyContent: 'center',
    top: globals.screenHeight * 0.06,
  },
  adImageStyle: {
    height: globals.screenHeight * 0.25,
    width: '100%',
    // opacity: 0.8,
    position: 'absolute',
    top: 0,
  },
  ed_dashboardView: {
    height: '89%',
    width: '100%',
  },
  eventNameStyle: {
    textAlign: 'center',
    color: colors.white,
    fontSize: globals.font_16,
    fontWeight: '600',
    // position: 'absolute',
    // right: 0,
    // left: 0,
    // top: globals.screenHeight * 0.06,
  },
  eventmembercountStyle: {
    textAlign: 'center',
    alignItems: 'center', justifyContent: 'center',
    color: colors.white,

    position: 'absolute',
    right: 0,
    left: 0,
    flexDirection: 'row',
    top: globals.screenHeight * 0.15,
  },
  memberView: {
    flexDirection: 'row',
    marginTop: globals.screenHeight * 0.15,
  },
  memberimgStyle: {

    alignSelf: 'center',
    height: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.04,

  },
  membetText: {
    fontSize: globals.font_12,
    fontWeight: '900',
    marginLeft: globals.screenWidth * 0.02
  },
  joinButtonStyle: {
    backgroundColor: colors.listSelectColor,
    height: globals.screenHeight * 0.04,
    width: globals.screenWidth * 0.18,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 3,
    marginTop: globals.screenHeight * 0.1,
    // marginBottom: globals.screenHeight * 0.014
  },
  waiting_apprivalBtnStyle: {
    backgroundColor: colors.listSelectColor,
    height: globals.screenHeight * 0.04,
    width: globals.screenWidth * 0.42,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 3,
    marginTop: globals.screenHeight * 0.1,
    // marginBottom: globals.screenHeight * 0.014,
    flexDirection: 'row',
  },
  acceptAndRejectBtnStyle: {},
  joinTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 19 : globals.font_11,
    color: colors.white,
    fontWeight: '500'
  },
  memberCountStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_13,
    color: colors.white,
    fontWeight: '500',
    textAlign: 'center'
  },
  ed_EventNameStyle: {
    fontSize: globals.font_14,
    paddingBottom: 1,
  },
  ed_TimeStyle: {
    fontSize: globals.font_12,
  },
  ed_TypeStyle: {
    fontSize: globals.font_10,
  },
  ed_DateStyle: {
    fontSize: globals.font_14,
  },

  fvStyle: {
    marginTop: globals.screenHeight * 0.08,
    // backgroundColor:'red'
  },
  vsItemParentStyle: {
    flex: 1,
  },
  beforeImgViewStyle: {
    height: screenHeight * 0.0757,//40
    width: screenHeight * 0.0757,//40
    borderRadius: screenHeight * 0.0378,
    borderColor: colors.lightGray,
    borderWidth: 0.3,
  },
  gridViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  gridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.10,
    margin: 10,
  },
  imgStyle: {
    tintColor: colors.white,
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.093,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.093,
  },
  gridViewInsideTextItemStyle: {
    marginTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.005,
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 25 : globals.font_14,
    justifyContent: 'center',
    color: colors.white,
    alignItems: 'center',
  },
  unSelectedgridViewBlockStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: screenHeight * 0.17,
    width: screenHeight * 0.17,
    margin: 10,
    backgroundColor: colors.white,
    borderRadius: (screenHeight * 0.17) / 2,
  },
  unSelectedimgStyle: {
    height: 25,
    width: 25,
    tintColor: colors.bgColor,
  },
  unSelectedgridViewInsideTextItemStyle: {
    paddingTop: screenHeight * 0.01,
    paddingHorizontal: screenHeight * 0.02,
    fontSize: globals.font_14,
    justifyContent: 'center',
    color: colors.bgColor,
    alignItems: 'center',
  },

  headerParentStyle: {
    flexDirection: 'row',
    height: screenHeight * 0.15,
    alignItems: 'center',
    // backgroundColor: 'blue',
    paddingHorizontal: screenWidth * 0.05,
  },
  ivEventImageStyle: {
    height: screenHeight * 0.0757,//40
    width: screenHeight * 0.0757,//40
    borderRadius: screenHeight * 0.0378,//20
    borderColor: colors.lightGray,
    borderWidth: 0.3,
  },
  eventInfoParentStyle: {
    width: screenWidth * 0.53,
    justifyContent: 'center',
    //  backgroundColor: colors.blue,
    marginLeft: screenWidth * 0.0468, //15
  },
  waiting_approvalViewContainerfornolonger:{
    height: (Platform.OS == "ios") ? globals.screenHeight * 0.20 : globals.screenHeight * 0.5,
    width: (Platform.OS == "ios") ? globals.screenWidth * 0.8 : globals.screenWidth * 0.9,
    backgroundColor: colors.gray,
    marginTop: globals.screenWidth * 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  waiting_approvalViewContainer: {
    // flex: 1,
    height: (Platform.OS == "ios") ? globals.screenHeight * 0.14 : globals.screenHeight * 0.2,
    width: (Platform.OS == "ios") ? globals.screenWidth * 0.8 : globals.screenWidth * 0.9,
    backgroundColor: colors.gray,
    marginTop: globals.screenWidth * 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  waiting_approvalIconStyle: {
    height: globals.screenWidth * 0.09,
    width: globals.screenWidth * 0.09,
    marginBottom: globals.screenHeight * 0.01,
    tintColor: colors.redColor
  },
  waiting_approvalTextStyle: {
    // marginLeft: (Platform.OS == "android") ? globals.screenWidth * 0.04 : 0,
    marginHorizontal: globals.screenWidth * 0.02,
    textAlign: 'center',
    fontSize: globals.font_14
  },
  nolongerText:{
    // :globals.screenHeight * 0.02,
    marginHorizontal: globals.screenWidth * 0.02,
    textAlign: 'center',
    fontSize: globals.font_14
  },
  sorryText: {
    marginVertical:globals.screenHeight * 0.02,
    marginHorizontal: globals.screenWidth * 0.03,
    textAlign: 'center',
    color: colors.darkRed,
    fontSize: globals.font_14
  },
  touchableStyle2: {
    marginVertical: globals.screenHeight * 0.03,
    paddingHorizontal: 22,
    paddingVertical: 5,
    borderRadius: 3,
    backgroundColor: colors.listSelectColor,
  },
  btntextStyless: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    color: colors.white
  },
  modalMainView: {
    height: globals.screenHeight * 0.18,
    justifyContent: 'center', alignItems: 'center',
    alignSelf: 'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor: colors.gray,
    borderRadius: 6,
    borderWidth: 1,
    borderColor: colors.gray,
    marginTop: globals.screenHeight * 0.2
},
modalInnerMainView: {
    backgroundColor: colors.gray,
    //  margin: globals.screenWidth * 0.035,
    width: globals.screenWidth * 0.8,
    // borderRadius: 4,
    flex: 1,
},
modalSecondView: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.37 : globals.screenHeight * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor: colors.gray,
    marginTop: globals.screenHeight * 0.6
},
modalAlreadyRegisteredView: {
    flexDirection: 'row', padding: globals.screenHeight * 0.01
},
modalResendTextView: {
    flex: 1, marginRight: 10
},
modalCancelImg: {
    height: globals.screenHeight * 0.03, width: globals.screenHeight * 0.03, tintColor: colors.gray
},
modalHorizontalLine: {
    height: 2, backgroundColor: colors.proUnderline, width: '100%', marginTop: (globals.screenHeight * 0.03), marginBottom: (globals.screenHeight * 0.03)
},
modalTextInputView: {
    borderColor: colors.gray, borderWidth: 2, marginHorizontal: 10
},
modalSubmitView: {
    justifyContent: 'center', backgroundColor: colors.listSelectColor, width: globals.screenWidth * 0.22, alignItems: 'center', height: globals.screenHeight * 0.05, right: 0, position: 'absolute', bottom: (globals.screenHeight * 0.03), marginRight: globals.screenHeight * 0.01, borderRadius: 3
},
modalSubmitText: {
    color: colors.white, fontSize: globals.font_13, fontWeight: '600'
},
popupinnerViewStyle: {
    flex: 1, marginTop: globals.screenHeight * 0.02,
    marginHorizontal: globals.screenHeight * 0.015,
},
popupheaderStyle: {
    fontSize: globals.font_16,
    color: colors.Gray,
    fontWeight: "500"
},
popupsubTextStyle: {
    marginTop: globals.screenHeight * 0.01,
    fontSize: globals.font_12,
    color: colors.Gray
},
termconditionView: {
    marginVertical: globals.screenHeight * 0.025,
    marginHorizontal: globals.screenWidth * 0.02,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
},
btnViewStyle: {
  marginVertical: globals.screenHeight * 0.01,
  marginHorizontal: globals.screenWidth * 0.02,
  flexDirection: 'row',
  justifyContent: 'space-evenly',
  alignItems: 'center'
},
innerbtnViewStyles: {
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'row',
  borderWidth: 0.2,
  borderRadius: 3,
  // marginHorizontal:globals.screenWidth * 0.03
},
closeimgStyle: {
  height: globals.screenWidth * 0.04,
  width: globals.screenWidth * 0.04,
  tintColor: colors.white,
  marginLeft: globals.screenWidth * 0.02
},
btnTextsStyle: {
  //  paddingLeft:globals.screenWidth * 0.025,
  paddingRight: globals.screenWidth * 0.025,
  paddingVertical: globals.screenWidth * 0.015,
  fontSize: globals.font_14,
  color: colors.white,
  fontWeight: "600",
  textAlign: 'center'
},
});
