import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ActivityIndicator, TextInput, Alert, Modal, TouchableWithoutFeedback } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import { API } from '../../../../../../utils/api';
import Icon from 'react-native-vector-icons/EvilIcons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import Nointernet from '../../../../../../components/NoInternet/index'
import ServerError from '../../../../../../components/ServerError/index'

const iPad = DeviceInfo.getModel();
let TAG = "Admins Screen ::==="
let _this = null;
class Admins extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Admins (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Admins ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => { _this.setModalVisible(true); _this.setState({ adminName: '' }) }}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };


    constructor(props) {
        super(props);
        _this = this;
        console.log(TAG, props);
        this.state = {
            communityAdminData: [],
            loading: false,
            isInternetFlag: true,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            adminName: '',
            totalCommunities: 0,
            groupId: this.props.navigation.state.params.groupId,
        }
    }

    componentDidMount() {
        this.props.showLoader()
        this.apicallGetAdminList()
    }

    /**
     * prepareData when call the API
     */
    prepareData() {
        const {
            pageNumber, pageSize, groupId, adminName
        } = this.state;

        const data = {};
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        data.UserID = globals.userID
        data.GroupID = groupId;
        data.Name = adminName,
            data.Type = 'Admins'
        return data;
    }

    /**
     * APi call for get members list
     */
    apicallGetAdminList() {
        if (globals.isInternetConnected === true) {
            this.setState({ loadingPagination: true, loading: true, isInternetFlag: true })
            API.getGroupAdmins(this.GetCommunityAdminResponseData, this.prepareData(), true)
        } else {
            this.props.hideLoader();
            this.setState({ isInternetFlag: false })
        }
    }

    static refreshAdminList() {
        _this.makeListFresh(() => {
            _this.apicallGetAdminList();
        });
    }

    /**
 * Response of MyCommunity listing
 */
    GetCommunityAdminResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetCommunityAdminResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                total = response.Data.TotalRecords;
                _this.props.navigation.setParams({
                    globalEventCount: response.Data.TotalRecords
                });
                if (this.state.pageNumber == 1) {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        communityAdminData: response.Data.GroupResultUserPoco,
                    })
                } else {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        communityAdminData: [...this.state.communityAdminData, ...response.Data.GroupResultUserPoco],
                    })
                }
            } else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetCommunityAdminResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403 || err.StatusCode == 500) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
  *  call when _sessionOnPres
  */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.apicallGetAdminList();
        });
    }

    renderFooter = () => {
        if (this.state.loadingPagination && this.state.responseComes) {
            return (
               
                        <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
                
            )
        } else {
            return (
                <View />
            )
        }
    };


    clearStates() {
        this.setState({
            communityAdminData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            adminName: '',
            totalCommunities: 0,
            communityID: this.props.navigation.state.params.communityID,
        })
    }

    /**
  * method for setSelectionUpcomingEvent Particular Item
  */
    setSelectionUpcomingEvent(item, index) {
        const { communityAdminData } = this.state;
        const communityAdminDataList = communityAdminData;
        for (let i = 0; i < communityAdminDataList.length; i++) {
            if (index == i) {
                communityAdminDataList[index].isSelected = true;
            } else {
                communityAdminDataList[i].isSelected = false;
            }
        }
        this.setState({ communityAdminData: communityAdminDataList }, () => { });
    }


    /**
     * Method of render conditions according views in member lisr
     * @param {*} item 
     * @param {*} index 
     */
    renderAdminList(item, index) {
        const { communityAdminData, loading } = this.state;
        const isAdminName = globals.checkObject(item, 'FullName');
        const isAdminProImage = globals.checkImageObject(item, 'ProfilePicturePath');
        return (
            <TouchableWithoutFeedback style={styles.mainParentStyle} onPress={() => this.setSelectionUpcomingEvent(item, index)}>
                <TouchableOpacity style={[styles.mainParentStyle]} onPress={() => (item.UserID == globals.userID) ?
                                    this.props.navigation.navigate("MY_PROFILEDEMO",{isfrom: "global"}) : this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "groupAdmin" })}>
                    <View style={[styles.mainRenderItemView, { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white }]}>
                        <View style={styles.horizontalItemView}>
                            <View style={styles.beforeimgView}>
                                <Image
                                    source={{ uri: isAdminProImage ? item.ProfilePicturePath : globals.User_img }}
                                    style={[styles.ivItemImageStyle]}
                                />
                            </View>
                            <View style={styles.middleViewTexts}>
                                <Text style={[styles.attendeeTextStyle, { color: item.isSelected == true ? colors.white : colors.black }]}>{(isAdminName) ? item.FullName : ''}</Text>
                            </View>
                        </View>

                    </View>

                </TouchableOpacity>
            </TouchableWithoutFeedback>
        )

    }

    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.apicallGetAdminList();
            })
            this.onEndReachedCalledDuringMomentum = true;

        }
    };


    onCrossApicall() {
        this.props.showLoader()
        this.setState({ adminName: '', responseComes: false, loadingPagination: true }, () => {
          this.apicallGetAdminList()
        })
      }

    /**
     * Search modal render function
     */
    renderSearchModel() {
        const { adminName } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                        <TouchableOpacity onPress={() => {this.onCrossApicall(); this.setModalVisible(false);  }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Admins'}
                        </Text>
                    </View>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_ADMIN}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ adminName: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={adminName}
                                autoCapitalize="none"
                            />
                        </View>
                        <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByAdminName()}>
                            <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Modal>
        );
    }

    /**
   * click on search based on community name
   */
    seacrhByAdminName() {
        const { adminName } = this.state;
        this.setState({ adminName: adminName }, () => {
            this.setModalVisible(false);
            this.searchAdmin();
        })
    }

    /**
    * Method of get  communities based on search filter
    */

    searchAdmin() {
        _this.makeListFresh(() => {
            _this.props.showLoader();
            _this.apicallGetAdminList();
        });
    }

    makeListFresh(callback) {
        this.setState(
            {
                pageNumber: 1,
                pageSize: 10,
                communityAdminData: [],
                responseComes: false
            },
            () => {
                callback();
            }
        );
    }



    /**
     * Method for change modal state
     * @param {*} visible 
     */
    setModalVisible(visible) {

        this.setState({ isShowSearch: visible });
    }



    render() {
        const { serverErr, loading, communityAdminData, adminName, responseComes, loadingPagination, isInternetFlag } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                {this.renderSearchModel()}
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (
                                (responseComes == false && loadingPagination == true) ?
                                    <View></View> :
                                    (communityAdminData.length == 0 && responseComes) ?
                                        (communityAdminData.length == 0 && adminName !== '') ?
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_ADMIN_NOT_AVLB_SEARCH}</Text>
                                            </View> :
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_ADMIN_NOT_AVLB}</Text>
                                            </View> :
                                        <View style={styles.mainParentStyle}>
                                            <FlatList
                                                style={{ flex: 1, marginTop: globals.screenHeight * 0.03 }}
                                                showsVerticalScrollIndicator={false}
                                                data={communityAdminData}
                                                renderItem={({ item, index }) => this.renderAdminList(item, index)}
                                                extraData={this.state}
                                                bounces={false}
                                                keyExtractor={(index, item) => item.toString()}
                                                ListFooterComponent={this.renderFooter}
                                                onEndReached={this.handleLoadMore}
                                                onEndReachedThreshold={0.2}
                                                onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                            />
                                        </View>
                            )
                            :
                            <ServerError loading={loading} onPress={() => this._tryAgain()} />
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Admins);