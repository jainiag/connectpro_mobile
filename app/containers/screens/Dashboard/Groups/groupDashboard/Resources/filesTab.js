import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, Modal, Alert } from 'react-native';
import styles from './style';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import CustomButton from '../../../../../../components/CustomButton/index';
import loginScreen from '../../../../AuthenticationScreens/loginScreen';
import * as globals from '../../../../../../utils/globals';
import * as images from '../../../../../../assets/images/map';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import DeviceInfo from 'react-native-device-info';
import * as colors from '../../../../../../assets/styles/color';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { API } from '../../../../../../utils/api';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import Nointernet from '../../../../../../components/NoInternet/index'
import ServerError from '../../../../../../components/ServerError/index'

const TAG = '==:== FilesTab : ';
const iPad = DeviceInfo.getModel();

class FilesTab extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: false,
      isInternetFlag: true,
      serverErr: false,
      responseComes: false,
      groupId: this.props.groupId,
      filesData: []

    }
  }

  componentDidMount() {
    this.makeAPICall()
  }

  /**
     * method for get community photos api
     */
  makeAPICall() {
    if (globals.isInternetConnected === true) {
      this.props.showLoader()
      this.setState({ loading: true, isInternetFlag: true })
      API.getGroupFiles(this.GetCommunityFilesResponseData, this.state.groupId, true)
    } else {
      this.setState({ isInternetConnected: false })
    }
  }

  /**
      * Response callback of communityInfoResponseData
      */
  GetCommunityFilesResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetCommunityFilesResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.setState({ responseComes: true, loading: false })
        let res = response.Data;
        this.setState({filesData : res},()=>{
          this.props.hideLoader()
        })
      }
      else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false, serverErr: true, responseComes: true })
      }
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false, serverErr: true, responseComes: true })
      console.log(
        TAG,
        'GetCommunityFilesResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
      Alert.alert(globals.appName, err.Message);
      if (err.StatusCode == 401) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  _tryAgain() {
    this.setState({ serverErr: false, responseComes: false }, () => {
      this.makeAPICall();
    });
  }

  /**
*  call when _sessionOnPres
*/
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }

  /**
   * Render item list event resource documents
   * @param {*} item 
   * @param {*} index 
   */
  renderFiles(item, index) {
    console.log("renderFiles ::",item)
    const isuploadedDoc = globals.checkObject(item, 'FileName');
    // const isUrl = globals.checkObject(item, 'Url');

    return (
      <TouchableOpacity style={[styles.mainParentStyle]} onPress={() => this.props.navigationProps.navigation.navigate('PdfView', { pdfData: item, screenName:'communityResources' })}>
        <View style={styles.mainRenderItemView}>
          <View style={styles.horizontalView}>
            <View style={styles.pdfImgView}>
              <Image source={images.EventDashboard.whitepdf} style={styles.pdfImgStyle} />
            </View>
            <View style={styles.pdfName}>
              <Text style={styles.urlTextStyle}>{(isuploadedDoc) ? item.FileName : ''}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
     
    );
  }



  render() {
    const { serverErr, isInternetFlag, loading, filesData, responseComes } = this.state;
    return (
      <View style={{flex:1}}>
        {
          (!isInternetFlag) ?
            <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
            (serverErr == false) ?
              (filesData.length == 0 && responseComes) ?
                <View style={globalStyles.nodataStyle}>
                  <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_RESOURCES_NOT_AVLB}</Text>
                </View> :
                  <FlatList
                    style={[styles.p_tab_flatlistStyle,{marginTop: globals.screenHeight * 0.020}]}
                    data={this.state.filesData}
                    renderItem={({ item, index }) => this.renderFiles(item, index)}
                    keyExtractor={(x, i) => i.toString()}
                    extraData={this.state}
                    showsVerticalScrollIndicator={false}
                    bounces={false}
                  />
               :
              <ServerError loading={loading} onPress={() => this._tryAgain()} />
        }
      </View>
    );
  }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FilesTab);
