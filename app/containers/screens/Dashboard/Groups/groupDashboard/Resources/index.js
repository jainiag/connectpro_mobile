/* eslint-disable react/no-unused-state */
import React from 'react';
import { Text, View, Image, FlatList, TouchableOpacity } from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import * as globals from '../../../../../../utils/globals';
import globalStyles from '../../../../../../assets/styles/globleStyles';
import { showLoader, hideLoader } from '../../../../../../redux/acrions/showLoader';
import CustomButton from '../../../../../../components/CustomButton/index';
import * as colors from '../../../../../../assets/styles/color';
import * as images from '../../../../../../assets/images/map';
import PhotosTab from './photosTab';
import FilesTab from './filesTab';
import { ScrollableTabView, ScrollableTabBar } from '../../../../../../libs/react-native-scrollable-tabview'

const TAG = '==:== ResourcesGroups : ';
let _this;
const iPad = DeviceInfo.getModel();

class ResourcesGroups extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Resources'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    console.log(TAG,'Props ::', props);
    _this = this;

    this.state = {
      loading: true,
      groupId: this.props.navigation.state.params.groupId,

    };
  }

  render() {
    return <ScrollableTabView
    tabBarTextStyle={{ fontSize: globals.font_16 }}
    tabBarInactiveTextColor={colors.black}
    tabBarActiveTextColor={colors.warmBlue}
      style={styles.tabbarStyle}
      initialPage={0}
      renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
    >
      <PhotosTab tabLabel={'Photos'} navigationProps={this.props} groupId={this.state.groupId}/>
      <FilesTab tabLabel={'Files'} navigationProps={this.props} groupId={this.state.groupId}/>
    </ScrollableTabView>
  }
}

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResourcesGroups);
