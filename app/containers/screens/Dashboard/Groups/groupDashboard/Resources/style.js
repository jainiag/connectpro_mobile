import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../../../utils/globals';
import * as colors from '../../../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

console.log('DeviceInfo.getModel() --->', DeviceInfo.getModel());

module.exports = StyleSheet.create({

  tabbarStyle: {
    marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.01
  },  

  // ====== : Photos Tab Styles : ======

  p_tab_MainContainer: {
    flex: 1,
    alignItems: 'center',
  },
  p_tab_flatlistStyle: {
    marginTop: globals.screenHeight * 0.025, 
    marginBottom: globals.screenHeight * 0.015,
    flex:1,
   
  },
  p_tab_touchableStyle: {
    margin: 2,
  },
  p_tab_PicStyle: {
    height: globals.screenHeight * 0.27,
    width: globals.screenWidth * 0.45,
    borderRadius: 4,
  },
  p_tab_modalMainViewStyle: {
    backgroundColor: 'rgba(0,0,0,0.8)', 
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  p_tab_modalIconContainer: {
      alignItems: 'center',
  },
  p_tab_modalInnerViewContainer: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor: colors.transparent,
    marginTop: globals.screenHeight * 0.03,
  },
  p_tab_modalPicStyle: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.45 : globals.screenHeight * 0.5,
    width: globals.screenWidth * 0.9,
    borderRadius: 5,
  },

  //files tab
  mainParentStyle: {
    flex: 1,
  },
  mainRenderItemView:{
    borderColor: colors.proUnderline, borderWidth: 1, marginVertical: globals.screenHeight * 0.015, borderRadius: 3,flex:1,
    marginHorizontal:globals.screenWidth * 0.06,height:globals.screenHeight*0.075
  },
  horizontalView:{
    flexDirection:'row', 
  },
  pdfImgView:{
    width:globals.screenWidth*0.15,height:globals.screenHeight*0.075, backgroundColor:colors.warmBlue, borderTopLeftRadius:4, borderBottomLeftRadius:4, alignItems:'center', justifyContent:'center'
  },
  pdfName:{
    flex:1, justifyContent:'center',
  },
  pdfImgStyle:{
    height:globals.screenHeight*0.037, width:globals.screenHeight*0.037
  },
  urlTextStyle:{
    margin: globals.screenHeight*0.015,
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
 },
})