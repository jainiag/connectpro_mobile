/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, TextInput, Modal, FlatList, Alert, ActivityIndicator, Platform } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import * as globals from '../../../utils/globals';
import Nointernet from '../../../components/NoInternet/index';
import ServerError from '../../../components/ServerError/index';
import globalStyles from '../../../assets/styles/globleStyles';
import { API } from '../../../utils/api';
import loginScreen from '../AuthenticationScreens/loginScreen';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/EvilIcons';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import JoinPopUp from '../../../components/JoinPopUp/index';
import Swipeable from 'react-native-swipeable';
import * as images from '../../../assets/images/map';
import * as colors from '../../../assets/styles/color';
import KeyboardListener from 'react-native-keyboard-listener';

let _this;
const iPad = DeviceInfo.getModel()

let TAG = "GlobalCommunities ===="
class GlobalCommunities extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalCommunityCount = (params !== undefined) ? params.globalCommunityCount : ''

        return {
            headerLeft: (globalCommunityCount != undefined) ? globals.ConnectProbackButton(navigation, 'Global Communities (' + globalCommunityCount + ')') : globals.ConnectProbackButton(navigation, 'Global Communities ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => { _this.setModalVisible(true); _this.setState({ communityName: '' }) }}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }

    };

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            isShowSearch: false,
            isInternetFlag: true,
            globalCommunityList: [],
            serverErr: false,
            loading: false,
            loadingPagination: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            communityID: 0,
            totalCommunities: 0,
            cancelModalReview: false,
            cancelModalAproval: false,
            leavecommunityModal: false,
            leaveModal: false,
            onSwipOutCancel: false,
            onSwipOutAccept: false,
            joinClick: false,
            joinCommunityID: 0,
            joinCommunityName: '',
            joinTermsAndConditions: '',
            communitystatus: '',
            keyboardOpen: false,
        }
    }


    /**
* API Call of get Community List after create community
*/
    static updateCommunityData() {
        _this.makeAPICall()
    }

    componentDidMount() {
        this.makeAPICall()
    }

    /**
  * API Call of get Community List
  */
    makeAPICall() {
        this.props.showLoader()
        this.getCommunityList()
    }

    /**
   * prepareData when call the API
   */
    prepareData() {
        const {
            pageNumber,
            pageSize,
            communityName,
            communityID
        } = this.state;

        const data = {};
        data.CategoryID = communityID;
        data.CommunityName = communityName
        data.UserID = globals.userID
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        return data;
    }

    /**
     * manage state of visibility of search modal
     */
    setModalVisible(visible) {
        this.setState({ isShowSearch: visible });
    }


    onCrossApicall() {
        this.props.showLoader()
        this.setState({ communityName: '', responseComes: false, loadingPagination: true }, () => {
            this.makeAPICall()
        })
    }

    /**
     * Render search modal method
     */
    renderSearchModel() {
        const { communityName } = this.state;
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isShowSearch}
                onRequestClose={() => {
                    this.setModalVisible(false);
                }}
            >
                <View style={styles.modelParentStyle}>
                    <KeyboardListener
                        onDidShow={() => { this.setState({ keyboardOpen: true }) }}
                        onDidHide={() => { this.setState({ keyboardOpen: false }) }} />
                    <View style={[styles.vwHeaderStyle, styles.vwFlexDirectionRowStyle]}>
                        <TouchableOpacity onPress={() => { this.onCrossApicall(); this.setModalVisible(false); }}>
                            <Icon name="close" size={globals.screenHeight * 0.06} color="blue" />
                        </TouchableOpacity>
                        <Text style={styles.tvSearchEventStyle}>
                            {'Search Community'}
                        </Text>
                    </View>
                    <View style={styles.mainParentStyle}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_COMMUNITYNAME}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ communityName: text })}
                                returnKeyType="done"
                                blurOnSubmit={true}
                                value={communityName}
                                autoCapitalize="none"
                            />
                        </View>
                        {(Platform.OS == "android") ?
                            (this.state.keyboardOpen == false) ?
                                <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByCommunityName()}>
                                    <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                                </TouchableOpacity>
                                :
                                null
                            :
                            <TouchableOpacity style={styles.toSearchStyle} onPress={() => this.seacrhByCommunityName()}>
                                <Text style={styles.tvSearchStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </Modal>
        );
    }


    /**
    * Method for search community by name
    */
    seacrhByCommunityName() {
        const { communityName } = this.state;
        this.setState({ communityName: communityName }, () => {
            this.setModalVisible(false);
            this.searchCommunity();
        })
    }


    /**
    * Method for search community 
    */
    searchCommunity() {
        _this.makeListFresh(() => {
            _this.makeAPICall();
        });
    }

    /**
    * Method for Refresh state's
    */
    makeListFresh(callback) {
        this.setState(
            {
                pageNumber: 1,
                pageSize: 10,
                communityID: 0,
                globalCommunityList: [],
                responseComes: false
            },
            () => {
                callback();
            }
        );
    }

    /**
     * Method of call getGlobalCommunities
     */
    getCommunityList() {
        if (globals.isInternetConnected == true) {
            this.setState({ loadingPagination: true, isInternetFlag: true })
            API.getGlobalCommunities(this.GetGlobalCommunityResponse, this.prepareData(), true);
        } else {
            this.props.hideLoader()
            this.setState({ isInternetFlag: false })
        }
    }

    /**
    * Response of get global communities api
    */
    GetGlobalCommunityResponse = {
        success: response => {
            console.log(
                TAG,
                'GetGlobalCommunityResponse -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                _this.props.navigation.setParams({
                    globalCommunityCount: response.Data.TotalRecords
                });
                if (this.state.pageNumber == 1) {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        globalCommunityList: response.Data.CommunityInfoPoco,
                    })
                } else {
                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        globalCommunityList: [...this.state.globalCommunityList, ...response.Data.CommunityInfoPoco],
                    })
                }
            }
            else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, responseComes: true, loadingPagination: false, });
            this.setState({ loading: false })
            console.log(
                TAG,
                'GetGlobalCommunityResponse -> ERROR : ',
                JSON.stringify(err.Message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    }

    /**
   * Mwthod called when noInternet
   */
    _tryAgain() {
        this.clearfileds();
        this.makeAPICall();
    }


    /**
     * Method for Clear state's
     */
    clearStates() {
        this.clearfileds();
        this.makeAPICall()
    }

    /**
    * Static Method for Clear state's JoinPopup
    */
    static clearStatesJoinPopup() {
        _this.clearfileds();
        _this.makeAPICall()
    }

    static joinPopUpClose() {
        _this.setState({
            joinClick: false
        })
    }

    /**
 *  call when _sessionOnPres
 */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }


    /**
*  Method for pagination to load More data
*/
    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.getCommunityList();
            })
            this.onEndReachedCalledDuringMomentum = true;
        }
    };


    /**
         * method for Select Particular Item
         */
    setSelection_community(item, index) {
        const { globalCommunityList } = this.state;
        const dataglobalCommunityList = globalCommunityList;
        for (let i = 0; i < dataglobalCommunityList.length; i++) {
            if (index == i) {
                dataglobalCommunityList[index].isSelected = true;
                this.props.navigation.navigate('CommunityDashboard', {
                    dataglobalCommunityList: item
                });
            } else {
                dataglobalCommunityList[i].isSelected = false;
            }
        }
        this.setState({ globalCommunityList: dataglobalCommunityList }, () => {
            // this.forceUpdate()
        });
    }

    /**
   * Render FlatList Items
   */
    renderGlobalCommunities(item, index) {
        return (
            <View>
                {
                    this.renderViewsAccordingConditions(item, index)
                }
            </View>
        );
    }

    /**
  * Method for JoinCommunity
  */
    clickJoinCommunity(id, communityname, TermsAndConditions, communityType) {
        if (communityType == 1) {
            var communityTypes = 'public'
        } else {
            var communityTypes = 'Private'
        }
        this.setState({ communitystatus: communityTypes, joinCommunityID: id, joinCommunityName: communityname, joinTermsAndConditions: TermsAndConditions, joinClick: true })
    }


    /**
     * Render communities according status handling
     * @param {*} item 
     * @param {*} index 
     */
    renderViewsAccordingConditions(item, index) {
       
        const isCommunityName = globals.checkObject(item, 'CommunityName');
        const isCommunityType = globals.checkObject(item, 'CommunityTypeID');
        const isCategoryName = globals.checkObject(item, 'CategoryName');
        const isMemberCount = globals.checkObject(item, 'MembersCount');
        const isCommunityLogo = globals.checkImageObject(item, 'CommunityLogoPath');

        if (item.CommunityUserStatus == 0) {  // Join community
            return (

                <Swipeable
                    leftButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 95 : 85}
                    leftButtons={[
                        <TouchableOpacity style={styles.onLeftStyle} onPress={() => { this.clickJoinCommunity(item.CommunityID, item.CommunityName, item.TermsAndConditions, item.CommunityTypeID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.done.donebtn}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'Join'}</Text>
                            </View>
                        </TouchableOpacity>
                    ]} rightButtons={null} rightContent={null}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        {/* <TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID, From: "CommunityDashboard" } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                {
                                    (this.state.onSwipOutAccept) ? null : <View style={styles.leftGreenLine}></View>
                                }
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.CommunityLogoPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MembersCount <= 1) ? item.MembersCount + ' Member' :
                                                item.MembersCount + ' Members' : '0 Member'}
                                    </Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCategoryName) ? "Category: " + item.CategoryName : "Category: " + '-'}</Text>
                                </View>
                                {/* <View style={styles.lastImgViewEnd}>
                                        <Text style={[styles.attendeeTextStyle, { color: colors.warmBlue }]}>{'INACTIVE'}</Text>
                                    </View> */}
                            </View>
                        </TouchableOpacity>
                        <View style={[styles.horizontalSeprator]}></View>
                    </View>
                </Swipeable>
            )
        }
        if (item.CommunityUserStatus == 1) {  //Waiting for approval
            return (
                <Swipeable leftButtons={null} leftContent={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleCancelModalStatusApproval(true, item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.cancelApproval}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'CANCEL'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID, From: "CommunityDashboard" } })}>
                            {/* <TouchableOpacity> */}
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.CommunityLogoPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MembersCount <= 1) ? item.MembersCount + ' Member' :
                                                item.MembersCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCategoryName) ? "Category: " + item.CategoryName : "Category: " + '-'}</Text>
                                </View>
                                <View style={styles.lastImgViewEnd}>
                                    <Image
                                        source={images.Communities.waitingApproval}
                                        style={styles.attende_shareIconStyle}
                                    />
                                </View>
                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }

                            </View>
                        </TouchableOpacity>
                        <View style={[styles.horizontalSeprator]}></View>
                    </View>
                </Swipeable>
            )
        }
        if (item.CommunityUserStatus == 2) {  //Leave  communityl
            return (
                <Swipeable
                    leftContent={null}
                    leftButtons={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this.handleLaeaveCommunityModalStatus(true, item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.Communities.LeaveCommunity}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'LEAVE'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        {/* <TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.CommunityLogoPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MembersCount <= 1) ? item.MembersCount + ' Member' :
                                                item.MembersCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCategoryName) ? "Category: " + item.CategoryName : "Category: " + '-'}</Text>
                                </View>
                                <View style={styles.lastImgViewEnd}>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.warmBlue }]}>{'LEAVE'}</Text>
                                </View>
                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }
                            </View>
                            <View style={[styles.horizontalSeprator]}></View>
                        </TouchableOpacity>
                    </View>
                </Swipeable>
            )
        }
        else if (item.CommunityUserStatus == 4) {    //community admin sent request to join community. Accept reject view
            return (
                <Swipeable
                    leftContent={null}
                    leftButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    leftButtons={[
                        <TouchableOpacity style={styles.onLeftStyle} onPress={() => { this._acceptRequestInvitation(item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.done.donebtn}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'ACCEPT'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}
                    rightContent={null}
                    rightButtonWidth={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 150 : 100}
                    rightButtons={[
                        <TouchableOpacity style={styles.onRightStyle} onPress={() => { this._rejectRequestInvitation(item.CommunityID) }}>
                            <View style={styles.vwFlexDirectionRowStyle}>
                                <Image
                                    source={images.colse.closebtn}
                                    style={styles.crossimgesStyle}
                                    resizeMode="contain" />
                                <Text style={styles.renderswipebtnText}>{'REJECT'}</Text>
                            </View>
                        </TouchableOpacity>

                    ]}>
                    <View style={[styles.vwItemParentPaddingStyle]}>
                        {/* <TouchableOpacity> */}
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('CommunityDashboard', { dataglobalCommunityList: { CommunityID: item.CommunityID } })}>
                            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
                                {
                                    (this.state.onSwipOutAccept) ? null : <View style={styles.leftGreenLine}></View>
                                }
                                <View style={styles.beforeimgView}>
                                    <Image
                                        source={{ uri: isCommunityLogo ? item.CommunityLogoPath : globals.Event_img }}
                                        style={[styles.ivItemImageStyle]}
                                    />
                                </View>
                                <View style={styles.middleViewTexts}>
                                    <Text style={[styles.attendeeTextStyle, { fontSize: globals.font_14, fontWeight: '500', }]} numberOfLines={2}>{(isCommunityName) ? item.CommunityName : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { color: colors.blue, marginTop: 2, }]}>
                                        {(isMemberCount) ?
                                            (item.MembersCount <= 1) ? item.MembersCount + ' Member' :
                                                item.MembersCount + ' Members' : '0 Member'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCommunityType) ? (item.CommunityTypeID == 1) ? 'public' : 'Private' : '-'}</Text>
                                    <Text style={[styles.attendeeTextStyle, { marginTop: 2, }]}>{(isCategoryName) ? "Category: " + item.CategoryName : "Category: " + '-'}</Text>
                                </View>

                                {
                                    (this.state.onSwipOutCancel) ? null : <View style={styles.rightRedLine}></View>
                                }
                            </View>
                            <View style={[styles.horizontalSeprator]}></View>
                        </TouchableOpacity>
                    </View>
                </Swipeable>
            )
        }
    }

    /**
   * Render modal of cancel click of waiting for review
   */
    renderCancelWaitingReviewModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.cancelModalReview}
                onRequestClose={() => {
                    this.handleCancelModalStatusReview(false, this.state.communityID);
                }}
                onBackdropPress={() => {
                    this.handleCancelModalStatusReview(false, this.state.communityID);
                }}
            >
                <View style={[styles.modalMainView, { height: globals.screenHeight * 0.23 }]}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.popupinnerViewStyle}>
                            <Text style={styles.popupheaderStyle}>{"By cancelling the community review request, the community will not be created. Do you want to be proceed?"}</Text>
                            <View style={styles.btnViewStyle}>
                                <TouchableOpacity onPress={() => this.handleCancelModalStatusReview(false, this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                                            name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                                        <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.ProceedWaitingReviewclick(this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                                            name={"check"} color={colors.white} style={styles.vectorStyle} />
                                        <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    /**
     * Render modal of leave community
     */
    renderLeaveCommunityModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.leavecommunityModal}
                onRequestClose={() => {
                    this.handleLaeaveCommunityModalStatus(false, this.state.communityID);
                }}
                onBackdropPress={() => {
                    this.handleLaeaveCommunityModalStatus(false, this.state.communityID);
                }}
            >
                <View style={styles.modalMainView}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.popupinnerViewStyle}>
                            <Text style={styles.popupheaderStyle}>{"Are you sure you want to leave this community?"}</Text>
                            <View style={styles.btnViewStyle}>
                                <TouchableOpacity onPress={() => this.handleLaeaveCommunityModalStatus(false, this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                                            name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                                        <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.clickProceedLeaveCommunity(this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                                            name={"check"} color={colors.white} style={styles.vectorStyle} />
                                        <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    /**
     * Render modal of cancel click of waiting for approval
     */
    renderCancelWaitingApprovalModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.cancelModalAproval}
                onRequestClose={() => {
                    this.handleCancelModalStatusApproval(false, this.state.communityID);
                }}
                onBackdropPress={() => {
                    this.handleCancelModalStatusApproval(false, this.state.communityID);
                }}
            >
                <View style={styles.modalMainView}>
                    <View style={styles.modalInnerMainView}>
                        <View style={styles.popupinnerViewStyle}>
                            <Text style={styles.popupheaderStyle}>{"Do you want to cancel the request?"}</Text>
                            <View style={styles.btnViewStyle}>
                                <TouchableOpacity onPress={() => this.handleCancelModalStatusApproval(false, this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                                            name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                                        <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.clickApprovalProceed(this.state.communityID)}>
                                    <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.green, borderColor: colors.green }]}>
                                        <Entypo
                                            size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                                            name={"check"} color={colors.white} style={styles.vectorStyle} />
                                        <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    /**
     * Handle status for cancel click of waiting for review
     * @param {*} visible 
     */
    handleLaeaveCommunityModalStatus(visible, communityId) {
        this.setState({ leavecommunityModal: visible, communityID: communityId })
    }

    /**
     * Handle status for cancel click of waiting for review
     * @param {*} visible 
     */
    handleCancelModalStatusReview(visible, communityId) {
        this.setState({ cancelModalReview: visible, communityID: communityId })
    }

    /**
     * Handle status for cancel click of waiting for Approval
     * @param {*} visible 
     */
    handleCancelModalStatusApproval(visible, communityId) {
        this.setState({ cancelModalAproval: visible, communityID: communityId })
    }

    /**
   * Click on Activate for Inactive community
   * @param {} communityId 
   */
    clickOnActivateInactivateCommunity(communityId) {
        this.setState({ loading: true })
        if (globals.isInternetConnected == true) {
            API.activateCommunity(this.ActivateInactivateCommunityResponseData, communityId, globals.userID, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
       * Click of Yes, procced on waiting for approval popup
       */
    clickApprovalProceed() {
        this.handleCancelModalStatusApproval(false, this.state.communityID)
        this.props.showLoader()
        this.setState({ loading: true })
        const data = {
            communityIds: [
                this.state.communityID
            ],
            statusId: 5,
            userId: globals.userID,
            rejectReason: "",
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.saveUserStatusInCommunities(this.saveUserStatusInCommunitiesResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
        * Response callback of saveUserStatusInCommunitiesResponseData
        */
    saveUserStatusInCommunitiesResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveUserStatusInCommunitiesResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.props.showLoader()
                this.clearStates()
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message);
                })
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveUserStatusInCommunitiesResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
        * method for onOpenSwipeout OPen
        */
    onOpenSwipeout() {
        this.setState({ onSwipOutAccept: !this.state.onSwipOutAccept, onSwipOutCancel: !this.state.onSwipOutCancel })
    }

    /**
        * method for onOpenSwipeout Close
        */
    onCloseSwipeout() {
        this.setState({ onSwipOutAccept: !this.state.onSwipOutAccept, onSwipOutCancel: !this.state.onSwipOutCancel })
    }

    /**
       * method for Leave the community
       */
    clickProceedLeaveCommunity() {
        this.handleLaeaveCommunityModalStatus(false, this.state.communityID)
        this.props.showLoader()
        this.setState({ loading: true })
        const data = {
            communityIds: [
                this.state.communityID
            ],
            statusId: 5,
            userId: globals.userID,
            rejectReason: "",
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.saveUserStatusInCommunities(this.saveUserStatusInCommunitiesResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
     * Click of Yes, procced on waiting for review popup
     */
    ProceedWaitingReviewclick() {
        this.handleCancelModalStatusReview(false, this.state.communityID)
        this.props.showLoader()
        this.setState({ loading: true })
        const data = {
            communityIds: [
                this.state.communityID
            ],
            statusId: 3,
            userId: globals.userID,
            rejectReason: "",
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.saveCommunityStatus(this.saveCommunityStatusResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }


    /**
        * Click on Accept for community request invitation
        * @param {*} communityId 
        */
    AcceptInvitationRequest(communityId) {
        this.setState({ loading: true })
        const data = {
            actionStatusID: 2,
            userids: globals.userID,
            communityID: communityId,
            loggedInUserId: globals.userID,
            AccessToken: JSON.parse(globals.tokenValue)
        }
        if (globals.isInternetConnected == true) {
            API.acceptRejectReqInviteMember(this.AcceptRejectInvitationResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    _acceptRequestInvitation(communityId){
        Alert.alert(
            globals.appName,
            "Are you sure you want to accept invitation this community?",
            [{ text: 'OK' , onPress: () => this.AcceptInvitationRequest(communityId)},
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') }],
          );
    }

    /**
     * Click on Reject for community invitation
     * @param {} communityId 
     */
    RejectInvitationRequest(communityId) {
        this.setState({ loading: true })
        const data = {
            actionStatusID: 3,
            userids: globals.userID,
            communityID: communityId,
            loggedInUserId: globals.userID,
            AccessToken: globals.tokenValue
        }
        if (globals.isInternetConnected == true) {
            API.acceptRejectReqInviteMember(this.AcceptRejectInvitationResponseData, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    _rejectRequestInvitation(communityId){
        Alert.alert(
            globals.appName,
            "Are you sure you want to reject invitation this community?",
            [{ text: 'OK' , onPress: () => this.RejectInvitationRequest(communityId)},
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed')}],
          );
    }

    /**
     * Response of saveCommunityStatus
     */
    saveCommunityStatusResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveCommunityStatusResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.clearStates()
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message)
                })

            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveCommunityStatusResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
     * Response of Accept Reject Invitation 
     */
    AcceptRejectInvitationResponseData = {
        success: response => {
            console.log(
                TAG,
                'AcceptRejectInvitationResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.clearStates()
            }
            else {
                this.setState({ loading: false })
                Alert.alert(globals.appName, response.Message);
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'AcceptRejectInvitationResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };


    /**
     * Response of Activate community click
     */
    ActivateInactivateCommunityResponseData = {
        success: response => {
            console.log(
                TAG,
                'ActivateInactivateCommunityResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                this.clearStates()
            }
            else {
                Alert.alert(globals.appName, response.Message);
            }
        },
        error: err => {
            this.props.hideLoader();
            console.log(
                TAG,
                'ActivateInactivateCommunityResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
       * Flatlist Methof for render footer
       */
    renderFooter = () => {
        if (this.state.loadingPagination && this.state.responseComes) {
            return (
               
                        <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
                  
            )
        }
        else {
            return (
                <View />
            )
        }
    };

    /**
     * clear all states
     */
    clearfileds() {
        this.setState({
            isShowSearch: false,
            globalCommunityList: [],
            serverErr: false,
            loading: false,
            loadingPagination: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            communityID: 0,
            totalCommunities: 0,
            cancelModalReview: false,
            cancelModalAproval: false,
            leavecommunityModal: false,
            leaveModal: false,
            onSwipOutCancel: false,
            onSwipOutAccept: false,
            joinClick: false,
            joinCommunityID: 0,
            joinCommunityName: '',
            communitystatus: ''
        })
    }

    render() {
        const { serverErr, loading, joinTermsAndConditions, communitystatus, communityName, globalCommunityList, responseComes, joinCommunityID, joinCommunityName, isInternetFlag } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                <NavigationEvents
                    onWillFocus={() => this.makeAPICall()}
                    onWillBlur={() => this.clearfileds()}
                />
                {this.renderSearchModel()}
                {this.renderLeaveCommunityModal()}
                {this.renderCancelWaitingReviewModal()}
                {this.renderCancelWaitingApprovalModal()}
                {
                    (this.state.joinClick == true) ?
                        <JoinPopUp
                            Communitystatus={communitystatus}
                            CommunityCategoryName={joinCommunityName}
                            communityID={joinCommunityID}
                            TermsAndConditions={joinTermsAndConditions}
                            isOpen={this.state.joinClick}
                            isFrom={'globalCommunityScreen'}
                        /> : null
                }

                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :

                        (serverErr === false) ?
                            (
                                (globalCommunityList.length == 0 && responseComes) ?
                                    (globalCommunityList.length == 0 && communityName !== '') ?
                                        <View style={globalStyles.nodataStyle}>
                                            <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DATA_NOT_AVAILABLE_IN_SEARCH}</Text>
                                        </View> :
                                        <View style={globalStyles.nodataStyle}>
                                            <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.GLOBALCOMMUNITIES_DATA_NOT_AVLB}</Text>
                                        </View> :
                                    <View style={styles.mainParentStyle}>
                                        <FlatList
                                            style={{ flex: 1, marginBottom: globals.screenHeight * 0.07, marginTop: globals.screenHeight * 0.01 }}
                                            showsVerticalScrollIndicator={false}
                                            data={globalCommunityList}
                                            renderItem={({ item, index }) => this.renderGlobalCommunities(item, index)}
                                            extraData={this.state}
                                            bounces={false}
                                            keyExtractor={(index, item) => item.toString()}
                                            ListFooterComponent={this.renderFooter}
                                            onEndReached={this.handleLoadMore}
                                            onEndReachedThreshold={0.2}
                                            onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                        />
                                    </View>
                            )
                            :
                            (
                                <ServerError loading={loading} onPress={() => this._tryAgain()} />
                            )
                }
                <TouchableOpacity style={styles.toSearchStyle}
                    onPress={() => this.props.navigation.navigate("CreateCommunity", { isFrom: "GlobalCommunity" })}>
                    <Text style={styles.tvSearchStyle}>{'CREATE COMMUNITY'}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};
const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GlobalCommunities);
