import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  textInputViewContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.05,
    borderRadius: 5,
  },
  textInputViewContainer2: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.025,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.lightGray,
    fontSize: globals.font_14,
    marginTop: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(13),
    marginBottom: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(13),
  },
  searchButtonContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.073 : globals.screenHeight * 0.06, // change in scrollview style also
    width: '100%',
    backgroundColor: colors.bgColor,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  seachTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_20,
    color: colors.white,
  },
  svStyle: {
    marginBottom: getVerticleBaseValue(57),
  },
  datePickerViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 15 : 20,
  },
  pickerStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.935 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenWidth * 0.84 : globals.screenWidth * 0.87,
  },
  placeHolderTextStyle: {
    color: colors.lightGray,
    fontSize: globals.font_14,
    textAlign: 'left',
    marginRight: iPad.indexOf('iPad') != -1 ? globals.screenWidth * 0.58 : DeviceInfo.isTablet() ? globals.screenWidth * 0.62 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenWidth * 0.41 : globals.screenWidth * 0.4,
  },
  startDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.proUnderline,
    marginTop: 8,
    marginBottom: 8,
    height: iPad.indexOf('iPad') != -1 ? globals.screenHeight * 0.056 : DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : (Platform.OS == 'android') ? globals.screenHeight * 0.065 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenHeight * 0.07 : globals.screenHeight * 0.06,
  },
  endDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.proUnderline,
    marginTop: 8,
    marginBottom: 8,
    marginLeft: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 5 : 10,
    marginRight: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 5 : 20,
  },
  dateIconStyle: {
    position: 'absolute',
    left: 0,
    marginLeft: 0,
  },
});
