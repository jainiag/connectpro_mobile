/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */
/* eslint-disable no-return-assign */
/* eslint-disable no-useless-constructor */
import React from 'react';
import { Text, View, TouchableOpacity, TextInput, ScrollView, Alert, Platform } from 'react-native';
import DatePicker from 'react-native-datepicker';
import DeviceInfo from 'react-native-device-info';
import DateRangePicker from '../../../../components/DatePicker';
import styles from './style';
import moment from 'moment';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as colors from '../../../../assets/styles/color';
import GlobalEvent from '../globalEvents/index';
import Validation from '../../../../utils/validation';
import KeyboardListener from 'react-native-keyboard-listener';


const TAG = '==:== GlobalEventsSearchScreen : ';
const iPad = DeviceInfo.getModel();
export default class GlobalEventSearchScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    const globalEventCount = (params !== undefined) ? params.count : ''
    return {
    headerLeft: (globalEventCount != undefined) ?
    globals.ConnectProbackButton(navigation, 'Global Events (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Global Events ()'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  };
};

  constructor(props) {
    super(props);
    this.state = {
      eventName: '',
      locationName: '',
      startDate: '',
      endDate: '',
      startDisplayDate: "",
      startDisplayDay: "",
      endDisplayDate: "",
      endDisplayDay: "",
      minDate: moment().format('MM/DD/YYYY'),
      maxDate: moment().add(25, 'year').format('MM/DD/YYYY'),
      openPicker: false,
      keyboardOpen: false,
    };
  }

  componentDidMount(){
    let today = new Date();
    let date = parseInt(today.getMonth() + 1) + "/" + parseInt(today.getDate() + 1) + "/" + today.getFullYear();
    const tab = this.props.navigation.state.params.selectedTab;
   if(tab !== 1){
     this.setState({
      minDate: moment().subtract(25, 'year').format('MM/DD/YYYY'),
      maxDate: moment().format('MM/DD/YYYY'),
     })
   } else {
     this.setState({
      minDate: date,
      maxDate: moment().add(25, 'year').format('MM/DD/YYYY'),
     })
   }
  }


  /*
   * method navigate To flatlist screen with paramerters
   */
  navigateToListScreen() {
    const { eventName, locationName, startDisplayDate, endDisplayDate } = this.state;
    GlobalEvent.searchEvent(eventName, locationName, startDisplayDate, endDisplayDate);
    this.clearFields()
    this.props.navigation.goBack();
  }

  clearFields() {
    this.setState({
      eventName: '',
      locationName: '',
      startDate: '',
      endDate: '',
      startDisplayDate: moment()
        .format('MM/DD/YYYY')
        .toUpperCase(),
      startDisplayDay: moment()
        .format('dddd')
        .toUpperCase(),
      endDisplayDate: moment()
        .format('MM/DD/YYYY')
        .toUpperCase(),
      endDisplayDay: moment()
        .format('dddd')
        .toUpperCase(),
      openPicker: false
    })
  }

  /**
   * Validation for Global search Event
   */
  _validation() {
    this.navigateToListScreen();
  }

  afterDateSelection(startDate, endDate) {
    const stdate = moment(startDate, 'YYYY-MM-DD').format('MM/DD/YYYY');
    const enddate = moment(endDate, 'YYYY-MM-DD').format('MM/DD/YYYY');

    const stDay = moment(startDate, 'YYYY-MM-DD').format('dddd');
    const edDay = moment(endDate, 'YYYY-MM-DD').format('dddd');

    this.setState({
      startDisplayDate: stdate.toUpperCase(),
      endDisplayDate: enddate.toUpperCase(),
      startDisplayDay: stDay.toUpperCase(),
      endDisplayDay: edDay.toUpperCase(),
    });
  }

  onStartDayPress(day) {
    const stdate = moment(day.dateString, 'YYYY-MM-DD').format('MM/DD/YYYY');
    const stDay = moment(day.dateString, 'YYYY-MM-DD').format('dddd');

    this.setState({
      startDisplayDate: stdate.toUpperCase(),
      endDisplayDate: stdate.toUpperCase(),
      startDisplayDay: stDay.toUpperCase(),
      endDisplayDay: stDay.toUpperCase(),
    });
  }

  openDatetimePicker() {
    this.setState({
      startDisplayDate: moment()
      .format('MM/DD/YYYY')
      .toUpperCase(),
    startDisplayDay: moment()
      .format('dddd')
      .toUpperCase(),
    endDisplayDate: moment()
      .format('MM/DD/YYYY')
      .toUpperCase(),
    endDisplayDay: moment()
      .format('dddd')
      .toUpperCase(),
    })
    this.setState({ openPicker: true })
  }

  render() {
    const { eventName, locationName, openPicker, startDisplayDate, endDisplayDate } = this.state;
    return (
      <View style={styles.mainParentStyle}>
        <KeyboardListener
          onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
          onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
        <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.svStyle}>
          <View style={styles.mainParentStyle}>
            <View style={styles.textInputViewContainer}>
              <TextInput
                style={styles.textInputStyleContainer}
                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_BY_EVENT_NAME}
                placeholderTextColor={colors.lightGray}
                onChangeText={text => this.setState({ openPicker: false, eventName: text })}
                returnKeyType="next"
                blurOnSubmit={false}
                value={eventName}
                onSubmitEditing={() => this.eventNameRef.focus()}
                autoCapitalize="none"
              />
            </View>
            <View style={styles.textInputViewContainer2}>
              <TextInput
                style={styles.textInputStyleContainer}
                placeholder={globals.MESSAGE.SEARCH_SCREEN.SEARCH_BY_LOCATION}
                placeholderTextColor={colors.lightGray}
                onChangeText={text => this.setState({ openPicker: false, locationName: text })}
                returnKeyType="done"
                blurOnSubmit={false}
                value={locationName}
                autoCapitalize="none"
                ref={eventNameRef => (this.eventNameRef = eventNameRef)}
              />
            </View>
            <TouchableOpacity style={[styles.textInputViewContainer2,{height: globals.screenHeight * 0.065}]} onPress={() => this.openDatetimePicker()}>
              <Text style={styles.textInputStyleContainer}>
                {
                  (startDisplayDate == "",
                    endDisplayDate == "") ?
                    globals.MESSAGE.SEARCH_SCREEN.SEARCH_BY_EVENT_DATE :
                    startDisplayDate + "-" + endDisplayDate
                }
              </Text></TouchableOpacity>
            {
              (openPicker == true) ?
                <View style={styles.textInputViewContainer2}>
                  <DateRangePicker
                    initialRange={[startDisplayDate, endDisplayDate]}
                    onSuccess={(s, e) => this.afterDateSelection(s, e)}
                    onStartDayPress={day => this.onStartDayPress(day)}
                    minDate={this.state.minDate}
                    maxDate={this.state.maxDate}
                    theme={{
                      markColor: colors.white,
                      markTextColor: colors.blue,
                      calendarBackground: colors.darkSkyBlue,
                      monthTextColor: colors.white,
                      textDisabledColor: colors.lightBorder,
                      dayTextColor: colors.white,
                      arrowColor: colors.white,
                    }}
                  />
                </View> : null
            }
          </View>
        </ScrollView>
        {(Platform.OS == "android") ?
          (this.state.keyboardOpen == false) ?
            <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this._validation()}>
              <Text style={styles.seachTextStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
            </TouchableOpacity>
            :
            null
          :
          <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this._validation()}>
            <Text style={styles.seachTextStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
          </TouchableOpacity>
        }
      </View>
    );
  }
}
