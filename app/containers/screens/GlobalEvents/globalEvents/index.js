/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable no-underscore-dangle */
/* eslint-disable import/no-duplicates */
/* eslint-disable no-plusplus */
/* eslint-disable eqeqeq */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/sort-comp */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Image, FlatList, Platform, TouchableOpacity, Alert, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import { API } from '../../../../utils/api';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import CustomButton from '../../../../components/CustomButton/index';
import * as colors from '../../../../assets/styles/color';
import * as images from '../../../../assets/images/map';
import { NavigationEvents, StackActions, NavigationActions } from 'react-navigation';

const TAG = '==:== GlobalEvents : ';
let _this;
let iPad = DeviceInfo.getModel();

class GlobalEvents extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
    return {
      headerLeft: (globalEventCount != undefined) ?
        globals.ConnectProbackButton(navigation, 'Global Events (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Global Events ()', 'globalevents'),
      headerStyle: globalStyles.ConnectPropheaderStyle,
      headerRight: (
        <View style={{ alignItems: 'center' }}>
          <TouchableOpacity
            onPress={() => _this.navigateToSearch(navigation)}
            style={{
              marginRight: globals.screenWidth * 0.04,
              alignItems: 'center',
              marginTop:
                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                  ? globals.screenHeight * 0.03
                  : null,
            }}
          >
            <Image
              source={images.headerIcon.searchIcon}
              resizeMode={"contain"}
              style={{
                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                tintColor: colors.warmBlue,
                alignSelf: 'center',
              }}
            />
          </TouchableOpacity>
        </View>
      ),
    }
  };

  constructor(props) {
    super(props);
    _this = this;

    this.state = {
      imageHeight: 0,
      imageWidth: 0,
      imageRadius: 0,
      selectedTab: 1,
      pageNumberUpcomingEvent: 1,
      pageNumberPastEvent: 1,
      pageSize: 10,
      totalUpcomingEvents: 0,
      totalPastEvents: 0,
      upcomingEventsList: [],
      pastEventsList: [],
      EventNameSearch: '',
      EventLocationSearch: '',
      FromDateSearch: '',
      ToDateSearch: '',
      serverErr: false,
      loading: true,
      loadingPaginationUpcoming: false,
      loadingPaginationPast: false,
      isSearh: false,
      allEventList: [],
      responseComes: false
    };
  }


  static updateselecedindex() {
    _this && _this.setState({ selectedTab: 1 })
  }

  navigateToSearch(navigation) {
    const count = _this.props.navigation.state.params.globalEventCount
    _this.setState({
      isSearh: true
    }, () => {
      navigation.navigate('GLOBAL_EVENT_SEARCH_SCREEN', { count: count, selectedTab: this.state.selectedTab })
    })
  }
  componentDidMount() {
    // this.makeApiCall();
  }

  /*
   * static method for seacrhEvents
   */
  static searchEvent(eventName, locationName, startDate, endDate) {
    _this.setState(
      {
        EventNameSearch: eventName,
        EventLocationSearch: locationName,
        FromDateSearch: startDate,
        ToDateSearch: endDate,
        isSearh: false,
      },
      () => {
        _this.makeListFresh(() => {
          // _this.makeApiCall();
        });
      }
    );
  }

  /**
   * states reset method
   */
  makeListFresh(callback) {
    this.setState(
      {
        pageNumberUpcomingEvent: 1,
        pageNumberPastEvent: 1,
        pageSize: 10,
        totalUpcomingEvents: 0,
        totalPastEvents: 0,
        upcomingEventsList: [],
        pastEventsList: [],

      },
      () => {
        callback();
      }
    );
  }

  /**
   * manage API based on selected Tab
   */
  onChangeTab(index) {
    this.props.showLoader();
    this.setState({
      pageNumberUpcomingEvent: 1,
      pageNumberPastEvent: 1,
      pageSize: 10,
      totalUpcomingEvents: 0,
      totalPastEvents: 0,
      upcomingEventsList: [],
      pastEventsList: [],
      selectedTab: index, loading: true,
      responseComes: false
    }, () => {

      // d0 something
      this.makeApiCall();
    });

  }

  /**
  * handleLoadMore method for pagination
  */
  handleLoadMore() {
    const {
      pageNumberUpcomingEvent,
      pageNumberPastEvent,
      selectedTab,
    } = this.state;


    if (selectedTab == 1) {
      if (!this.onEndReachedCalledDuringMomentum) {
        this.setState({ pageNumberUpcomingEvent: pageNumberUpcomingEvent + 1 }, () => {
          _this.makeApiCall();
        });
        this.onEndReachedCalledDuringMomentum = true;
      }
    } else {
      if (!this.onEndReachedCalledDuringMomentum) {
        this.setState({ pageNumberPastEvent: pageNumberPastEvent + 1 }, () => {
          _this.makeApiCall();
        });
        this.onEndReachedCalledDuringMomentum = true;
      }
    }

  }



  /**
   * prepareData when call the API
   */
  prepareData() {
    const {
      selectedTab,
      pageNumberUpcomingEvent,
      pageNumberPastEvent,
      pageSize,
      EventNameSearch,
      EventLocationSearch,
      FromDateSearch,
      ToDateSearch,
    } = this.state;

    const data = {};
    data.EventCriteria = selectedTab == 1 ? 'UPCOMING' : 'PAST';
    data.PageNumber = selectedTab == 1 ? pageNumberUpcomingEvent : pageNumberPastEvent;
    data.PageSize = pageSize;
    data.EventNameSearch = EventNameSearch;
    data.EventLocationSearch = EventLocationSearch;
    data.FromDateSearch = FromDateSearch;
    data.ToDateSearch = ToDateSearch;
    return data;
  }

  navigateToScreen = route => {
    const { navigation } = this.props;
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    navigation.dispatch(navigateAction);
  };

  /**
   * API call
   */
  makeApiCall() {
    this.navigateToScreen(globals.GLOBAL_EVENT_SCREEN)
    if (globals.isInternetConnected === true) {

      this.setState({ loadingPaginationPast: true, loadingPaginationUpcoming: true, loading: true })
      // this.props.showLoader();
      API.getGlobalUpcomingEvents(this.getUpcomingGlobalEventResponseData, this.prepareData(), false);
    }
    else {
      this.props.hideLoader()
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  componentDidMount() {
    this.props.showLoader();
    this.makeApiCall();
  }


  /**
    * updatePageCount method for pagination
    */
  updatePageCount(count, callback) {
    const { selectedTab } = this.state;
    if (selectedTab == 1) {
      this.setState({ totalUpcomingEvents: count }, () => {
        callback();
      });
    } else {
      this.setState({ totalPastEvents: count }, () => {
        callback();
      });
    }
  }

  /**
   * checkUniquiness method for pagination to check unique Item
   */
  checkUniquiness(dataToCheck, keyName, valueToSearch) {
    let isExist = false;
    const foundIndex = dataToCheck.findIndex(item => item[keyName] == valueToSearch);
    if (foundIndex == -1) {
      isExist = false;
    } else {
      isExist = true;
    }
    return isExist;
  }

  /**
   * API response for getUpcomingGlobalEventResponseData
   */
  getUpcomingGlobalEventResponseData = {
    success: response => {
      console.log(
        TAG,
        'getUpcomingGlobalEventResponseData -> success : ',
        JSON.stringify(response)
      );

      if (response.StatusCode == 200) {
        _this.props.navigation.setParams({
          globalEventCount: response.Data.TotalRecords
        });
        this.setState({ loading: false })
        const { selectedTab, upcomingEventsList, pastEventsList } = this.state;

        const events = response.Data.EventsList;

        if (selectedTab == 1) {
          if (this.state.pageNumberUpcomingEvent == 1) {
            let events = response.Data.EventsList
            let isFound = this.checkDuplicateValue(events.EventID, events);
            if (isFound) {
              this.setState({ upcomingEventsList: response.Data.EventsList, loadingPaginationUpcoming: false, loading: false, responseComes: true })
            } else {
              this.setState({ loadingPaginationUpcoming: false, loading: false, responseComes: true })
            }
          } else {
            let events1 = [...this.state.upcomingEventsList, ...response.Data.EventsList]
            let isFound = this.checkDuplicateValue(events.EventID, events1);
            if (isFound) {
              this.setState({ upcomingEventsList: [...this.state.upcomingEventsList, ...response.Data.EventsList], loadingPaginationUpcoming: false, loading: false, responseComes: true });
            }
            else {
              this.setState({ loadingPaginationUpcoming: false, loading: false, responseComes: true });
            }
          }
        } else {
          if (this.state.pageNumberPastEvent == 1) {
            this.setState({ pastEventsList: response.Data.EventsList, loadingPaginationPast: false, loading: false, responseComes: true });
          } else {
            this.setState({ pastEventsList: [...this.state.pastEventsList, ...response.Data.EventsList], loadingPaginationPast: false, loading: false, responseComes: true });
          }
        }
        this.props.hideLoader();
      } else {
        this.setState({ loading: false, responseComes: true });
        Alert.alert(globals.appName, response.Message)
      }
    },

    error: err => {
      console.log(
        TAG,
        'getUpcomingGlobalEventResponseData -> Error : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(globals.appName, err.Message)
      }
      this.setState({ serverErr: true, loadingPaginationPast: false, loadingPaginationUpcoming: false });
      this.setState({ loading: false, responseComes: true });
      this.props.hideLoader();
    },
    complete: () => {
      this.props.hideLoader();
    },
  };

  /**
   * method for checkDuplicateValue
   */
  checkDuplicateValue(value, tempArray) {
    let found = false;
    for (var i = 0; i < tempArray.length; i++) {
      if (tempArray[i].title == value) {
        found = true;
        break;
      }
    }
    return found;
  }


  /*
   * Try again method when noInternet
   */
  _tryAgain() {
    this.setState({ loading: true, serverErr: false }, () => {
      this.makeApiCall();
    });
  }

  /**
   * method for setSelectionUpcomingEvent Particular Item
   */
  setSelectionUpcomingEvent(item, index) {
    const { upcomingEventsList } = this.state;
    const dataUpcomingEventsList = upcomingEventsList;
    for (let i = 0; i < dataUpcomingEventsList.length; i++) {
      if (index == i) {
        dataUpcomingEventsList[index].isSelected = true;
        this.props.navigation.navigate('GLOBAL_EVENT_SCREEN_DASHBOARD', {
          selectedEventList: item,
          itisFrom: 'myEvent'
        });
      } else {
        dataUpcomingEventsList[i].isSelected = false;
      }
    }
    this.setState({ upcomingEventsList: dataUpcomingEventsList }, () => { });
  }

  /**
   * method for setSelectionPastEventsList Particular Item
   */
  setSelectionPastEventsList(item, index) {
    const { pastEventsList } = this.state;
    const datapastEventsList = pastEventsList;
    for (let i = 0; i < datapastEventsList.length; i++) {
      if (index == i) {
        datapastEventsList[index].isSelected = true;
        this.props.navigation.navigate('GLOBAL_EVENT_SCREEN_DASHBOARD', {
          selectedEventList: item,
          itisFrom: 'myEvent'
        });
      } else {
        datapastEventsList[i].isSelected = false;
      }
    }
    this.setState({ pastEventsList: datapastEventsList }, () => { });
  }

  /**
   * render Flatlist Data
   */
  eventItemUI(item, index) {
    const isEventName = globals.checkObject(item, 'EventName');
    const isEventType = globals.checkObject(item, 'EventType');
    const isImage = globals.checkImageObject(item, 'ImageName');
    const isStartDate = globals.checkObject(item, 'StartDate');
    const isEndDate = globals.checkObject(item, 'EndDate');
    const startDate = (isStartDate) ? item.StartDate : '-';
    const endDate = (isEndDate) ? item.EndDate : '-';
    const isEventStartDate = globals.checkObject(item, 'EventStartDate');
    const isEventEndDate = globals.checkObject(item, 'EventEndDate');
    const starttime = (isEventStartDate) ? moment(item.EventStartDate).format('LT') : '-';
    const endtime = (isEventEndDate) ? moment(item.EventEndDate).format('LT') : '-';
    const month = (isEventStartDate) ? moment(item.EventStartDate).format('MMM') : '-';
    const thDate = (isEventStartDate) ? moment(item.EventStartDate).format('Do') : '-';
    const onlyDt = (isEventStartDate) ? moment(item.EventStartDate).format('D') : '-';
    const { selectedTab } = this.state;

    return (
      <View>
        <View
          style={[
            styles.vwItemParentPaddingStyle,
            { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },
          ]}
        >
          <TouchableOpacity
            onPress={() =>
              selectedTab == 1
                ? this.setSelectionUpcomingEvent(item, index)
                : this.setSelectionPastEventsList(item, index)
            }
          >
            <View style={[styles.vwItemPaddingStyle, styles.vwFlexDirectionRowStyle]}>
              <View style={styles.beforeImgViewStyle}>
                <Image
                  source={{ uri: isImage ? item.ImageName : globals.Event_img }}
                  style={[styles.ivItemImageStyle]}
                />
              </View>
              <View style={[styles.vwItemInfoStyle]}>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.tvEventNameStyle,
                    { width: "85%" },
                    { color: item.isSelected == true ? colors.white : colors.black },
                  ]}
                >
                  {isEventName ? item.EventName : '-'}
                </Text>
                <Text
                  numberOfLines={1}
                  style={[
                    styles.tvTimeStyle,
                    { width: "85%" },
                    { color: item.isSelected == true ? colors.white : colors.darkgrey },
                  ]}
                >{`${startDate} To ${endDate}`}</Text>
                <Text
                  style={[
                    styles.tvTypeStyle,
                    { flex: 1 },
                    { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                  ]}
                >
                  {isEventType ? item.EventType : '-'}

                </Text>
              </View>

              {/* <View style={styles.vwItemDateStyle}>
                <Text
                  style={[
                    styles.tvDateStyle,
                    { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                  ]}
                >
                  {month}
                </Text>
                <View style={styles.vwDateSubViewStyle}>
                  <Text
                    style={[
                      styles.tvLabelStyle,
                      { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                    ]}
                  >
                    {onlyDt}
                  </Text>
                  <Text
                    style={[
                      styles.tvThStyle,
                      { color: item.isSelected == true ? colors.white : colors.listSelectColor },
                    ]}
                  >
                    {thDate.charAt(thDate.length - 2)}
                    {thDate.charAt(thDate.length - 1)}
                  </Text>
                </View>
              </View> */}
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.vwSimpleLineStyle} />
      </View>
    );
  }

  /**
   * clear fileds of events
   */
  clearfileds() {
    this.setState({
      imageHeight: 0,
      imageWidth: 0,
      imageRadius: 0,
      // selectedTab: 1,
      pageNumberUpcomingEvent: 1,
      pageNumberPastEvent: 1,
      pageSize: 10,
      totalUpcomingEvents: 0,
      totalPastEvents: 0,
      upcomingEventsList: [],
      pastEventsList: [],
      EventNameSearch: '',
      EventLocationSearch: '',
      FromDateSearch: '',
      ToDateSearch: '',
      serverErr: false,
      loading: true,
      responseComes: false,
      allEventList: [],
    })
  }

  /**
   * Flatlist method for render footer
   */
  renderFooter = () => {
    if (this.state.selectedTab == 1) {
      if (this.state.loadingPaginationUpcoming && this.state.responseComes) {
        return (
          <ActivityIndicator style={styles.loaderbottomstyle} size="large" color={colors.bgColor} />
        )
      } else {
        return (
          <View />
        )
      }
    } else {
      if (this.state.loadingPaginationPast && this.state.responseComes) {
        return (
          <ActivityIndicator style={styles.loaderbottomstyle} size="large" color={colors.bgColor} />
        )
      } else {
        return (
          <View />
        )
      }
    }
  };

  render() {
    const { isSearh, selectedTab, upcomingEventsList, pastEventsList, serverErr, loading, responseComes, loadingPaginationUpcoming, loadingPaginationPast } = this.state;



    return (
      <View style={styles.mainParentStyle}>
        <NavigationEvents
          onWillFocus={() => { this.props.showLoader(); this.makeApiCall() }}
          onWillBlur={() => this.clearfileds()}
        />
        {serverErr === false ? (
          <View style={styles.mainParentStyle}>
            <View>
              <View style={[styles.vwFlexDirectionRowStyle]}>
                <TouchableOpacity onPress={() => this.onChangeTab(1)} style={styles.toTabStyle}>
                  <View style={[styles.vwTabStyle, styles.vwCenterStyle]}>
                    <Text
                      style={[
                        styles.tvTabTextStyle,
                        selectedTab == 1 ? styles.tvSelectedTabStyle : styles.tvUnSelectedTabStyle,
                      ]}
                    >
                      {globals.MESSAGE.GLOBAL_EVENT.GLOBAL_EVENT_UPCOMING}
                    </Text>
                  </View>
                  {selectedTab == 1 ? <View style={styles.selectedLineStyle} /> : null}
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.onChangeTab(2)} style={styles.toTabStyle}>
                  <View style={[styles.vwTabStyle, styles.vwCenterStyle]}>
                    <Text
                      style={[
                        styles.tvTabTextStyle,
                        selectedTab == 2 ? styles.tvSelectedTabStyle : styles.tvUnSelectedTabStyle,
                      ]}
                    >
                      {globals.MESSAGE.GLOBAL_EVENT.GLOBAL_EVENT_PAST}
                    </Text>
                  </View>
                  {selectedTab == 2 ? <View style={styles.selectedLineStyle} /> : null}
                </TouchableOpacity>
              </View>
              <View style={styles.vwSimpleLineStyle} />
            </View>
            {
              (selectedTab == 1)
                ?
                (responseComes == false && loadingPaginationUpcoming == true) ?
                  <View></View> :
                  (upcomingEventsList.length == 0 && responseComes) ?
                    (upcomingEventsList.length == 0 && isSearh !== '') ?
                      <View style={globalStyles.nodataStyle}>
                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DATA_NOT_AVAILABLE_IN_SEARCH}</Text>
                      </View>
                      :
                      <View style={globalStyles.nodataStyle}>
                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.UPCOMING_DATA_NOT_AVLB}</Text>
                      </View> :
                    <FlatList
                      style={{ flex: 1 }}
                      data={selectedTab == 1 ? upcomingEventsList : pastEventsList}
                      renderItem={({ item, index }) => this.eventItemUI(item, index)}
                      extraData={this.state}
                      keyExtractor={(index, item) => item.toString()}
                      ListFooterComponent={this.renderFooter}
                      onEndReached={() => this.handleLoadMore()}
                      onEndReachedThreshold={0.2}
                      onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                    />

                :
                (responseComes == false && loadingPaginationPast == true) ?
                  <View></View> :
                  (pastEventsList.length == 0 && responseComes) ?
                    (pastEventsList.length == 0 && isSearh !== '') ?
                      <View style={globalStyles.nodataStyle}>
                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DATA_NOT_AVAILABLE_IN_SEARCH}</Text>
                      </View> :
                      <View style={globalStyles.nodataStyle}>
                        <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.PAST_DATA_NOT_AVLB}</Text>
                      </View> :
                    <FlatList
                      style={{ flex: 1 }}
                      data={selectedTab == 1 ? upcomingEventsList : pastEventsList}
                      renderItem={({ item, index }) => this.eventItemUI(item, index)}
                      keyExtractor={(index, item) => item.toString()}
                      extraData={this.state}
                      ListFooterComponent={this.renderFooter}
                      onEndReached={() => this.handleLoadMore()}
                      onEndReachedThreshold={0.2}
                      onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                    />

            }
          </View>
        ) : (
            <View style={styles.serverErrViewContainer}>
              <Text style={styles.serverTextStyle}>
                {globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_PLZ_TRY_AGAIN}
              </Text>
              <TouchableOpacity style={styles.serverButtonStyle} onPress={() => this._tryAgain()}>
                <CustomButton
                  text={globals.BTNTEXT.LOGINSCREEN.CUSTOM_BTN_TEXT}
                  backgroundColor={colors.bgColor}
                  color={colors.white}
                  loadingStatus={loading}
                />
              </TouchableOpacity>
            </View>
          )}
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GlobalEvents);
