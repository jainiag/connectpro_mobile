/* eslint-disable import/no-duplicates */
import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
let { screenHeight,screenWidth} = globals;
const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  loaderWrapper: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 999,
    backgroundColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
},
loaderInner: {
  alignItems: 'center',
  justifyContent: 'center',
},
loaderbottomstyle: {
  bottom: (Platform.OS == 'android') ?
              globals.screenHeight * 0.025 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
},
  vwFlexDirectionRowStyle: {
    flexDirection: 'row',
  },
  customHeaderStyle: {
    paddingHorizontal: screenWidth * 0.0266, //10
  },
  tvTabTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 :globals.font_20,
    paddingHorizontal: screenWidth * 0.0266, //10
  },
  tvSelectedTabStyle: {
    color: colors.warmBlue,
  },
  tvUnSelectedTabStyle: {
    color: colors.gray,
  },
  vwTabStyle: {
    height: screenHeight * 0.0946, //50
    paddingHorizontal: screenWidth * 0.0266, //10
  },
  vwCenterStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedLineStyle: {
    position: 'absolute',
    bottom: 0,
    width: screenWidth * 0.1562, //50
    height: screenHeight * 0.0088, //5
    borderRadius: screenHeight * 0.0044, //2.5
    backgroundColor: colors.warmBlue,
  },
  vwSimpleLineStyle: {
    width: '90%',
    position: 'absolute',
    bottom: 0,
    height: 1,
    backgroundColor: colors.gray,
    opacity: 0.5,
    marginHorizontal: screenWidth * 0.0468, //15
  },
  toTabStyle: {
    alignItems: 'center',
  },
  vwImageViewStyle: {
    backgroundColor: 'yellow',
  },
  vwItemDateStyle: {
    flex: 1,
    alignItems: 'flex-end',
  },
  vwItemInfoStyle: {
    width: screenWidth * 0.75,
    justifyContent: 'center',
  },
  vwItemPaddingStyle: {
    marginHorizontal: screenWidth * 0.0468, //15
    marginVertical: screenHeight * 0.0284, //15
  },
  ivItemImageStyle: {
    height: screenHeight * 0.0757, // 40
    width: screenHeight * 0.0757, // 40
    borderRadius: (screenHeight * 0.0946) / 2, // 25,
    borderColor:colors.white,
    borderWidth: 0.1,
  },
  vwItemParentPaddingStyle: {
    marginHorizontal: 0,
  },
  tvEventNameStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  tvTimeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
  },
  tvTypeStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
  },
  tvDateStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  beforeImgViewStyle:{
    height: screenHeight * 0.0757, // 40
    width: screenHeight * 0.0757, // 40
    borderRadius: (screenHeight * 0.0948) / 2, // 25,
    marginRight: screenWidth * 0.0375, // 12
    marginLeft: screenWidth * 0.0156,
    borderColor: colors.lightBlack,
    borderWidth: 0.2,
  },
  notAvailableDataViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginHorizontal: 25,
  },
  tvpaymentTypeStyle: {
    color: colors.redColor,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_12,
  },
  tvthStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_10,
    color: colors.blue,
    paddingBottom: 2,
  },
  vwDateSubViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  tvThStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
  },
  tvLabelStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_14,
  },
});
