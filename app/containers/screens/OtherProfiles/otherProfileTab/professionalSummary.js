/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Image, ScrollView } from 'react-native';
import * as images from '../../../../assets/images/map';
import styles from './style';
import * as globals from '../../../../utils/globals';
import AsyncStorage from '@react-native-community/async-storage';
import { NavigationEvents, StackActions, NavigationActions } from 'react-navigation';


var _this = null;
let MemberProfile = [];
let MyProfile = [];
let email;
let organizationName;
let ProfileAllData = [];
let TAG = "ProfessionalSummary Tab ::===="

export default class ProfessionalSummary extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        AsyncStorage.getItem(globals.ORGANIZATIONID).then(org_Id => {
            globals.organizationId = org_Id;
        })
    }

    static getMemberProfileData(memberProfile, myProfileData, EmailID, OrganizationName, profileAllData) {
        MemberProfile = memberProfile;
        MyProfile = myProfileData;
        email = EmailID;
        organizationName = OrganizationName;
        ProfileAllData = profileAllData;
    }

    clearfileds() {
        MemberProfile = [];
        MyProfile = [];
    }

    render() {
        const isBiography = globals.checkObject(MemberProfile, 'Biography')
        const isExtension = globals.checkObject(MemberProfile, 'Extension')
        const isPhone = globals.checkObject(MemberProfile, 'Phone')
        const isJobTitle = globals.checkObject(MemberProfile, 'JobTitle')
        const isEmailId = globals.checkObject(MyProfile, 'EmailID')
        const isOrganizationName = globals.checkObject(MyProfile, 'OrganizationName')
        const finalPhoneNumber = (isPhone) ? (MemberProfile.Phone == '') ? MemberProfile.Phone : '+' + MemberProfile.Phone : '-';
        const isPrivateCheck = globals.checkObject(MemberProfile, 'IsPrivate')
        const isEmailPrivateCheck = globals.checkObject(MemberProfile, 'IsEmailPrivate')
        const isPhonePrivateCheck = globals.checkObject(MemberProfile, 'IsPhonePrivate')
        const isPrivate = (isPrivateCheck) ? MemberProfile.IsPrivate : '-';
        const isEmailPrivate = (isEmailPrivateCheck) ? MemberProfile.IsEmailPrivate : '-';
        const isPhonePrivate = (isPhonePrivateCheck) ? MemberProfile.IsPhonePrivate : '-';
        const atNumber = (isPhone) ? MemberProfile.Phone : '-';
        const finalPhone = (atNumber.charAt(0) == "+") ? atNumber : (isPhone) ? "+" + atNumber : "-"

        return (
            <View style={styles.summary_MainContainer}>
                <NavigationEvents
                    onWillBlur={() => this.clearfileds()}
                />
                <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                    {(isPrivate == true) ?
                        <View style={styles.summary_ViewContainer}>
                            <Image source={images.MyProfile.bag}
                                resizeMode='contain'
                                style={styles.summary_IconsStyle}
                            />
                            <Text style={[styles.summary_TextStyles, { width: globals.screenWidth * 0.80 }]}>{(isJobTitle) ? MemberProfile.JobTitle : '-'}</Text>
                        </View>
                        :
                        <View>
                            <View style={styles.summary_ViewContainer}>
                                <Image source={images.MyProfile.bag}
                                    resizeMode='contain'
                                    style={styles.summary_IconsStyle}
                                />
                                <Text style={[styles.summary_TextStyles, { width: globals.screenWidth * 0.80 }]}>{(isJobTitle) ? MemberProfile.JobTitle : '-'}</Text>
                            </View>
                            <View style={styles.summary_ViewContainer}>
                                <Image source={images.MyProfile.buildings}
                                    resizeMode='contain'
                                    style={styles.summary_IconsStyle}
                                />
                                <Text style={[styles.summary_TextStyles, { width: globals.screenWidth * 0.80 }]}>{(isOrganizationName) ? MyProfile.OrganizationName : '-'}</Text>
                            </View>
                        </View>
                    }
                    {(isPrivate == true) ? null :
                        (isEmailPrivate == true && isPhonePrivate == true) ?
                            null
                            :
                            <View>
                                {(isEmailPrivate == true) ?
                                    null
                                    :
                                    <View style={styles.summary_ViewContainer}>
                                        <Image source={images.MyProfile.emailBlack}
                                            resizeMode='contain'
                                            style={styles.summary_IconsStyle}
                                        />
                                        <Text style={[styles.summary_TextStyles, { width: globals.screenWidth * 0.80 }]}>{(isEmailId) ? MyProfile.EmailID : '-'}</Text>
                                    </View>}
                                {(isPhonePrivate == true) ?
                                    null
                                    :
                                    <View style={styles.summary_ViewContainer}>
                                        <Image source={images.MyProfile.phone}
                                            resizeMode='contain'
                                            style={styles.summary_IconsStyle}
                                        />
                                        <View style={styles.summary_TextViewContainer}>
                                            <Text style={styles.summary_TextStyles}>{finalPhone}</Text>
                                            <Text style={styles.summary_TextStyles}>Ext: {(isExtension) ? MemberProfile.Extension : '-'}</Text>
                                        </View>
                                    </View>
                                }
                            </View>
                    }
                    <Text style={[styles.summary_TextStyles, {
                        marginLeft: globals.screenWidth * 0.012,
                        marginHorizontal: globals.screenWidth * 0.03, paddingBottom: globals.screenHeight * 0.02,
                    }]}>{(isBiography) ? MemberProfile.Biography : '-'}</Text>
                </ScrollView>
            </View>
        );
    }
}
