/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Alert, ActivityIndicator, TouchableOpacity, TextInput, KeyboardAvoidingView, Platform, Keyboard } from 'react-native';
import styles from './style';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import AsyncStorage from '@react-native-community/async-storage';
import { API } from '../../../../utils/api';
import Profile from '../profile'
import Modal from "react-native-modal";
import GlobalMember from '../../GlobalMember/index';
import GroupMember from '../../Dashboard/Groups/groupDashboard/Members/index';
import GroupAdmin from '../../Dashboard/Groups/groupDashboard/Admins/index';
import MyConnection from '../../Dashboard/Connections/myConnection';
import NewConnection from '../../Dashboard/Connections/newConnection';
import CommunityMember from '../../Dashboard/Communities/Community_Dashboard/Members/index';
import CommunityAdmin from '../../Dashboard/Communities/Community_Dashboard/Admins/index';
import SimilarInterert from '../../Profile/userProfileTabs/similarInterests';

let TAG = "ConnectionTab:==="
class ConnectionTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      matchesData: [],
      serverErr: false,
      ConnectionStatus: this.props.ConnectionStatus,
      receiver_Id: this.props.User_ID,
      Id: this.props.memberProfile.ID,
      notes: '',
      visibleModal: false,
      isCancel: false,
      isConnected: this.props.isConnected,
      isFrom: this.props.isFrom,
      sendStatus: this.props.sendStatus
    }
    console.log("this.props.ConnectionStatus-->",this.props.ConnectionStatus)
  }

  CancelConnectionRequest(){
    this.setState({ loading: true })
    const data = {
        "ID": this.state.Id,
        "SenderID": globals.userID,
        "ReceiverID": this.state.receiver_Id,
    }
    if (globals.isInternetConnected === true) {
        API.CancelNewconnectionRequest(this.cancelNewconnectionRequestResponse, data, true);
    } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }
  

  cancelNewconnectionRequestResponse = {
    success: response => {
        console.log(
            TAG,
            'cancelNewconnectionRequestResponse -> success : ',
            JSON.stringify(response)
        );
        if (response.StatusCode == 200) {
          if (this.state.isFrom == "globalMember") {
            GlobalMember.refreshList();
            Profile.refreshMyProfile();
          } else if (this.state.isFrom == "groupMember") {
            GroupMember.refreshGroupMember();
            Profile.refreshMyProfile();
          } else if(this.state.isFrom == "groupAdmin"){
            GroupAdmin.refreshAdminList();
            Profile.refreshMyProfile();
          } else if (this.state.isFrom == "newConnection"){
            NewConnection.refreshNewConnectionList();
            Profile.refreshMyProfile();
          } else if (this.state.isFrom == "myConnection"){
            MyConnection.updatedMyConnectionData();
            Profile.refreshMyProfile();
          } else if (this.state.isFrom == "communityMember"){
            CommunityMember.refreshCommunityMember();
            Profile.refreshMyProfile();
          } else if(this.state.isFrom == "communityAdmin"){
            CommunityAdmin.refreshCommunityAdmin();
            Profile.refreshMyProfile();
          } else if(this.state.isFrom == "SimilarInterests"){
            SimilarInterert.refreshMatchesInterestList();
            Profile.refreshMyProfile();
          } else {
            console.log(TAG,"isFrom ::", this.state.isFrom);
          }
        } else {
            this.setState({ loading: false, isCancel: false })
        }
        this.setState({ loading: false, isCancel: true })
        Alert.alert(globals.appName, response.Message, [{ text: 'OK', onPress: () =>  this.setState({isCancel: true, ConnectionStatus: 0, sendStatus: 0 ,isConnected: false })}]);
    },
    error: err => {
        this.setState({ loading: false , isCancel: false})
        if (err.StatusCode == 401 || err.StatusCode == 403) {
            Alert.alert(
                globals.appName,
                'Your session is expired, Please login again',
                [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                { cancelable: false }
            );
        } else {
            Alert.alert(
                globals.appName,
                err.Message
            );
        }
        console.log(
            TAG,
            'cancelNewconnectionRequestResponse-> ERROR : ',
            JSON.stringify(err.message)
        );

        this.setState({ loading: false });
    },
    complete: () => {
        this.setState({ loading: false, isCancel: true })
    },
};

_sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigationProps.navigation.navigate('LoginScreen');
}


saveConnection(){
  this.setState({ loading: true })
  let data = {
    ID: null ,//this.state.Id,
    Note: this.state.notes,
    ReceiverID: this.state.receiver_Id,
    SenderID: globals.userID
  }
  Keyboard.dismiss();
  this.handleConnectModalStatus(false, this.state.receiver_Id);
  if (globals.isInternetConnected === true) {
    API.saveMemberConnections(this.saveMemberConnectionsResponseData, data, true)
  } else {
    this.setState({ loading: false })
    Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
  }
}

saveMemberConnectionsResponseData = {
  success: response => {
      console.log(
          TAG,
          'saveMemberConnectionsResponseData -> success : ',
          JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ ConnectionStatus: 1, isConnected: true, sendStatus: 0, loading: false })
        Alert.alert(globals.appName, response.Message, [{ text: 'OK' , onPress: () => this.handleConnectModalStatus(false, this.state.receiver_Id)}]);
        if (this.state.isFrom == "globalMember") {
          GlobalMember.refreshList();
          Profile.refreshMyProfile();
        } else if (this.state.isFrom == "groupMember") {
          GroupMember.refreshGroupMember();
          Profile.refreshMyProfile();
        } else if(this.state.isFrom == "groupAdmin"){
          GroupAdmin.refreshAdminList();
          Profile.refreshMyProfile();
        } else if (this.state.isFrom == "newConnection"){
          NewConnection.refreshNewConnectionList();
          Profile.refreshMyProfile();
        } else if (this.state.isFrom == "myConnection"){
          MyConnection.updatedMyConnectionData();
          Profile.refreshMyProfile();
        } else if (this.state.isFrom == "communityMember"){
          CommunityMember.refreshCommunityMember();
          Profile.refreshMyProfile();
        } else if(this.state.isFrom == "communityAdmin"){
          CommunityAdmin.refreshCommunityAdmin();
          Profile.refreshMyProfile();
        } else {
          console.log(TAG,"isFrom ::", this.state.isFrom);
        }
      }
      else {
          this.setState({ loading: false, isCancel: true })
      }
      this.setState({ loading: false, isCancel: false })
  },
  error: err => {
    this.setState({ loading: false, isCancel: true })
      console.log(
          TAG,
          'saveMemberConnectionsResponseData -> ERROR : ',
          JSON.stringify(err.message)
      );

      if (err.StatusCode == 401 || err.StatusCode == 403) {
          Alert.alert(
              globals.appName,
              'Your session is expired, Please login again',
              [{ text: 'OK', onPress: () => this._sessionOnPres() }],
              { cancelable: false }
          );
      } else {
          Alert.alert(globals.appName, err.Message)
      }

  },
  complete: () => {
    this.setState({ loading: false, isCancel: false })
  },
};

handleConnectModalStatus(visible, id) {
  if(visible== false){
    this.setState({ visibleModal: visible, receiver_Id: id, notes:'' })
    Keyboard.dismiss();
  }else{
    this.setState({ visibleModal: visible, receiver_Id: id })
    Keyboard.dismiss();
  }
 
}

handleNotes(text) {
  this.setState({
      notes: text,
  });
}

renderConnectModal() {
  return (
      <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.visibleModal}
          onRequestClose={() => {
              this.handleConnectModalStatus(false, this.state.receiver_Id);
          }}
          onBackdropPress={() => {
              this.handleConnectModalStatus(false, this.state.receiver_Id);
          }}
      >
        <KeyboardAvoidingView behavior={(Platform.OS == "ios") ? "padding" : "height" } keyboardVerticalOffset={(Platform.OS == "ios") ? 80 : null}>
          <View style={[styles.modalMainView]}>
              <View style={styles.modalInnerMainView}>
                  <Text style={styles.modalUperText2}>Your connection request is on it's way. Would you like to add a note to your request.</Text>
                  <View style={styles.underLineView}></View>
                  <Text style={styles.addNoteTxt2}>Add Note:</Text>
                  <View style={styles.textInputViewContainer}>
                      <TextInput
                          multiline={true}
                          maxLength={100}
                          returnKeyType="done"
                          autoCapitalize="none"
                          value={this.state.notes}
                          placeholderTextColor={colors.lightGray}
                          onChangeText={text => this.handleNotes(text)}
                          placeholder={'Add Note..'}
                          style={[styles.textInputStyleContainer]}
                      />
                  </View>
                  <View style={styles.modalEndView}>
                      <TouchableOpacity style={styles.closeView} onPress={() => {this.handleConnectModalStatus(false, this.state.receiver_Id); this.setState({notes: ''})}}>
                          <View style={styles.closeView}>
                              <Text style={styles.closeTxt}>Close</Text>
                          </View>
                      </TouchableOpacity>
                      <TouchableOpacity style={styles.sendView} onPress={() => this.saveConnection(false, this.state.receiver_Id)}>
                          <View style={styles.sendView} >
                              <Text style={styles.sendTxt}>Send</Text>
                          </View>
                      </TouchableOpacity>

                  </View>
              </View>
          </View>
          </KeyboardAvoidingView>
      </Modal>
  )
}


  render() {
    const statusConnection = this.props.navigationProps.navigationProps.navigation.state.params.item.ConnectionStatus;
    console.log("statusConnection--->",statusConnection)
    console.log("statusConnection props--->",this.state.ConnectionStatus)

    return (
      <View style={styles.connectionTabContainerStyle}>
        {this.renderConnectModal()}
        {(this.state.loading == true) ? 
        <ActivityIndicator size="small" style={styles.actvity_indicatorStyle} color={colors.bgColor}/> 
        : 
        (this.state.ConnectionStatus == 2 || this.state.sendStatus == 2) ?
          <Text style={styles.connection_TextStyle}>Already Connected</Text>
          :
          (this.state.ConnectionStatus == 1 || this.state.sendStatus == 1) ?
            <View>
              <Text style={styles.connection_TextStyle}>Request Pending</Text>
              <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this.CancelConnectionRequest()}>
                <View style={styles.lastImgViewEnd}>
                  <View style={[styles.connectedBtnView,{width: globals.screenWidth * 0.3, marginTop: 8}]}>
                    <Text style={styles.connectTextStyle}>Cancel Request</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            :
            <View style={{alignItems:'center',justifyContent:'center'}}>
              <Text style={styles.connection_TextStyle}>Send Connection Request</Text>
              <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this.handleConnectModalStatus(true, this.state.receiver_Id)}>
                <View style={styles.lastImgViewEnd}>
                  <View style={[styles.connectedBtnView,{ marginTop: 8}]}>
                    <Text style={styles.connectTextStyle}>Connect</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
        }
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectionTab);
