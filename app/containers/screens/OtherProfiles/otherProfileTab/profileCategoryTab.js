/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Alert, ScrollView, ActivityIndicator } from 'react-native';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import styles from './style';
import * as globals from '../../../../utils/globals';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import AsyncStorage from '@react-native-community/async-storage';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import * as colors from '../../../../assets/styles/color';

let TAG = "MyProfile ProfileCategories Tab:: =="
let _this ;

class ProfileCategoryTab extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      profileCategoryData: [],
      profile_categoryData: [],
      loading: false,
      serverErr: false,
      User_id: this.props.User_ID,
      isPrivate: this.props.memberProfile.IsPrivate,
    };
  }

  componentDidMount() {
    ProfileCategoryTab._profileCategoryApiCall()
  }

  static _profileCategoryApiCall(){
    if (globals.isInternetConnected === true) {
      _this.setState({ loading: true }, () => {
        API.getProfileCategories(_this.GetProfileCategoriesResponseData, true, _this.state.User_id)
      })
    } else {
      _this.setState({ loading: false })
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
    * Response callback of GetPortfolio ResponseData
    */
  GetProfileCategoriesResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetProfileCategoriesResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        console.log('response :: Data ::', response.Data);
        if (response.Data == null) {
          let profile_CategoryData = response.Data
          this.setState({ profile_categoryData: profile_CategoryData })
        } else {
          let profileCategoryAllData = response.Data.WhoAmIAll
          this.setState({ profileCategoryData: profileCategoryAllData })
        }
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message)
      }
      
      this.setState({ loading: false })
    },
    error: err => {
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.setState({ loading: false })
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(globals.appName, err.Message)
      }
      console.log(
        TAG,
        'GetProfileCategoriesResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ loading: false });
      this.setState({ serverErr: true })
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };

 /**
  * When session is expired then this method called
  */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigationProps.navigationProps.navigation.navigate('LoginScreen');
  }

  render() {
    const { profileCategoryData, profile_categoryData } = this.state;
    const statusConnection = this.props.navigationProps.navigationProps.navigation.state.params.item.ConnectionStatus;
    return (
      <View style={styles.container2}>
        {(this.state.isPrivate == true && (statusConnection == 2 || statusConnection == undefined)) ?
          (this.state.loading === true) ?
            <View style={styles.activity_IndicatorViewStyle}>
              <ActivityIndicator
                style={styles.actvity_indicatorStyle}
                size="small"
                color={colors.bgColor}
              />
            </View>
            :
            <ScrollView bounces={false}>
              {
                (this.state.serverErr == true) ?
                  <View style={styles.notAvialableViewStyle}>
                    <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_SMTHING_WENT_WRNG}</Text>
                  </View>
                  :
                  (profileCategoryData.length > 0 && profileCategoryData != null) ?
                    <View style={styles.pro_tagViewContainer}>
                      <TagSelect
                        isFrom="ProfileCategoryTab"
                        data={profileCategoryData}
                        itemStyle={styles.item}
                        itemLabelStyle={styles.label}
                        itemStyleSelected={styles.itemSelected}
                        itemLabelStyleSelected={styles.labelSelected}
                      />
                    </View>
                    :
                    (profile_categoryData != null) ?
                      <View style={styles.notAvialableViewStyle}>
                        <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.PORFILE_CATEGORY_NOT_AVLBL}</Text>
                      </View>
                      :
                      <View style={styles.notAvialableViewStyle}>
                        <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.PORFILE_CATEGORY_NOT_AVLBL}</Text>
                      </View>
              }
            </ScrollView>
          :
          (this.state.isPrivate == true && statusConnection !== 2 && statusConnection !== undefined) ?
            <View style={styles.connectionTabContainerStyle}>
              <Text style={styles.notAvialableTextStyle}>This Account is Private</Text>
            </View>
            :

            (this.state.loading === true) ?
              <View style={styles.activity_IndicatorViewStyle}>
                <ActivityIndicator
                  style={styles.actvity_indicatorStyle}
                  size="small"
                  color={colors.bgColor}
                />
              </View>
              :
              <ScrollView bounces={false}>
                {
                  (this.state.serverErr == true) ?
                    <View style={styles.notAvialableViewStyle}>
                      <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_SMTHING_WENT_WRNG}</Text>
                    </View>
                    :
                    (profileCategoryData.length > 0 && profileCategoryData != null) ?
                      <View style={styles.pro_tagViewContainer}>
                        <TagSelect
                          isFrom="ProfileCategoryTab"
                          data={profileCategoryData}
                          itemStyle={styles.item}
                          itemLabelStyle={styles.label}
                          itemStyleSelected={styles.itemSelected}
                          itemLabelStyleSelected={styles.labelSelected}
                        />
                      </View>
                      :
                      (profile_categoryData != null) ?
                        <View style={styles.notAvialableViewStyle}>
                          <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.PORFILE_CATEGORY_NOT_AVLBL}</Text>
                        </View>
                        :
                        <View style={styles.notAvialableViewStyle}>
                          <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.PORFILE_CATEGORY_NOT_AVLBL}</Text>
                        </View>
                }
              </ScrollView>
        }
      </View>
    );
  }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileCategoryTab);
