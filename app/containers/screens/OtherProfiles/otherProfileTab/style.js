import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';
import * as colors from '../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.045 : (globals.iPhoneX) ? globals.screenHeight * 0.018 : (Platform.OS == 'android') ? globals.screenHeight * 0.005 : globals.screenHeight * 0.03,
  },
  container2: {
    flex: 1,
    backgroundColor:  colors.white,
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.028 : (globals.iPhoneX) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.022,
  },
  buttonContainer: {
    padding: globals.screenHeight * 0.018,
  },
  buttonInner: {
    marginBottom: globals.screenHeight * 0.018,
  },
  labelText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_15,
    fontWeight: '500',
    marginBottom: globals.screenHeight * 0.018,
  },
  item: {
    backgroundColor: 'lightgrey',
  },
  label: {
    // color: '#333',
    fontSize: globals.font_13
  },
  itemSelected: {
    // backgroundColor: '#333',
  },
  labelSelected: {
    // color:colors.white,
  },
  activity_IndicatorViewStyle: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  actvity_indicatorStyle: {
    justifyContent: 'center', 
    alignItems: 'center'
  },

  // ========: profileSummary Style :=======
  summary_MainContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    marginTop: globals.screenHeight * 0.018,
  },
  summary_ViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: globals.screenHeight * 0.013,
  },
  summary_TextStyles: {
    marginLeft: globals.screenWidth * 0.03,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
  },
  iconViewStyle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.025,
    marginRight: globals.screenWidth * 0.03,
  },
  summary_IconsStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.06,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.06,
  },
  IconsStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08, 
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
  },
  summary_TextViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  summary_EditButtonStyle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.025,
    marginRight: globals.screenWidth * 0.03,
  },
  // ======: Other profileCategory tab :=====
  pro_tagViewContainer: {
    marginLeft: globals.screenWidth * 0.04,
  },
  pro_imageStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
  },
  notAvialableTextStyle: {
    fontSize: globals.font_14,
    textAlign: 'center'
  },
  notAvialableViewStyle: {
    justifyContent: 'center', 
    alignItems: 'center', 
    marginTop: globals.screenHeight * 0.185,
  },
  // ======: edit profileCategory :=====
  scrollViewContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
  },
  mainInputViewContaier: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  inputViewContainer: {
    width: '100%',
    borderWidth: 0.5,
    borderColor: colors.proUnderline,
  },
  textInputContainer: {
    paddingBottom: globals.screenHeight * 0.015,
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_13,
  },
  buttonViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: globals.iPhoneX ? 0 : 15,
  },
  saveButtonContariner: {
    backgroundColor: colors.warmBlue,
    height: globals.screenHeight * 0.036,
    width: globals.screenWidth * 0.19,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: globals.screenWidth * 0.018,
  },
  saveAndCancelTextStyle: {
    color: colors.white,
    fontSize: globals.font_16,
  },
  cancelButtonContainer: {
    backgroundColor: colors.gray,
    height: globals.screenHeight * 0.036,
    width: globals.screenWidth * 0.19,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: globals.screenWidth * 0.018,
  },
  //----------------------------: Endorsement Tab :----------------------------------------
  mainParentStyle: {
    flex: 1,
  },
  mainRenderItemView: {
    height: globals.screenHeight * 0.11, borderColor: colors.proUnderline, borderWidth: 1, marginBottom: globals.screenHeight * 0.01, borderRadius: 3, flex: 1,
    marginHorizontal: 10
  },
  mainViewSimilarInterst: {
    flex: 1, marginTop: globals.screenHeight * 0.018
  },
  horizontalItemView: {
    flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.015, flex: 1
  },
  middleViewTexts: {
    justifyContent: 'center', marginHorizontal: globals.screenHeight * 0.02, flex: 1,
  },
  lastImgViewEnd: {
    justifyContent: 'center', alignItems: 'flex-end'
  },
  attendesFlatlist: {
    marginTop: globals.screenHeight * 0.05,
    paddingHorizontal: globals.screenWidth * 0.05
  },
  beforeimgView: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946) / 2,
    borderColor: colors.lightGray,
    borderWidth: 0.5
  },
  ivItemImageStyle: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946) / 2,
    marginTop: -1,
    marginLeft: -1,
  },
  attendeeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12,
  },
  attende_shareIconStyle: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 30 : globals.screenHeight * 0.03,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 40 : globals.screenWidth * 0.07
  },
  list_mainContainer: {
    backgroundColor: colors.white,
    width: globals.screenWidth * 0.85,
    marginLeft: globals.screenWidth * 0.074,
    borderWidth: 0.7,
    borderColor: colors.gray,
    borderRadius: 4,
    marginBottom: globals.screenHeight * 0.02,
    paddingBottom: globals.screenHeight * 0.02,
  },
  list_childContainer: {
    flexDirection: 'row',
    paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: globals.screenWidth * 0.025,
  },
  list_imageViewContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
    marginVertical: 5,
    borderRadius: (globals.screenWidth * 0.15) / 2,
    borderWidth: 0.2,
    borderColor: colors.lightGray,
    marginLeft: (globals.screenWidth * 0.03),
    // marginLeft: (globals.screenWidth * 0.01),
  },
  list_imageStyleContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.11 : globals.screenWidth * 0.15,
    borderRadius: (globals.screenWidth * 0.15) / 2,
    borderWidth: 0.2,
    borderColor: colors.lightGray,
  },
  list_DetailViewContainer: {
    marginHorizontal: globals.screenWidth * 0.035,
    marginVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.01 : globals.screenWidth * 0.025,
    flex: 1
  },
  list_ReceiveTextstyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_14,
  },
  list_jobTitlestyle: {
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
  },
  list_ContentStyle: {
    marginHorizontal: globals.screenWidth * 0.055,
    paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: globals.screenWidth * 0.025,
    marginBottom: globals.screenWidth * 0.028,
  },
  list_ContentTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 21 : globals.font_14,
    color: colors.matteBlack
  },
  list_clockIconViewStyle: {
    flex:1, flexDirection:'row', marginTop:10, alignItems:'center'
  },
  list_clockIconStyle: {
    height:globals.screenHeight*0.02, width:globals.screenHeight*0.02
  },
  list_timeTextStyle: {
    fontSize:globals.font_12, color:colors.lightBlack, marginLeft:5
  },
  Endrs_MainViewStyle: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)', 
    alignItems: 'center', 
    justifyContent: 'center', 
    marginHorizontal: -globals.screenWidth * 0.26, 
    marginVertical: -globals.screenHeight * 0.12,
  },
  Endrs_ChildViewContainer: {
    height: globals.screenHeight * 0.44,
    justifyContent: 'center', alignItems: 'center',
    alignSelf: 'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor: colors.white,
    borderRadius: 3,
    borderWidth: 1,
    borderColor: colors.gray,
  },
  Endrs_ViewStyle: {
    backgroundColor: colors.white,
    flex: 1,
    width: '85%',
    margin: 10,
  },
  Endrs_IconViewStyle: {
    flexDirection: 'row', 
    alignItems: 'center', 
    justifyContent: 'space-between', 
    marginBottom: 10,
  },
  Endrs_TextStyle: {
    color: colors.warmBlue,
    fontSize: globals.font_16,
  },
  Endrs_InputViewStyle: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.gray,
    height: globals.screenHeight * 0.25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.01,
    borderRadius: 2,
  },
  Endrs_InputStyle: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginBottom: 5,
    height: globals.screenHeight * 0.18
  },
  Endrs_BtnViewStyle: {
    justifyContent:'flex-end', 
    marginTop:10, 
    flexDirection: 'row', 
    width: "100%",
  },
  Endrs_OpacityStyle: {
    height: globals.screenHeight * 0.05,
    width: '100%',
    backgroundColor: colors.listSelectColor,
    marginLeft: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Endrs_sendTextStyle: {
    fontSize: globals.font_14,
    color: colors.white,
  },
  Endrs_AccPrivate:{
    fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 28 : globals.font_16,
    color: colors.black
  },

  // ========:  Portfolio Tab :=========
  mainViewContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.01,
  },

  PO_TAB_MainContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.018,
  },
  PO_TAB_AddButtonContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.002,
    marginRight: globals.screenWidth * 0.03,
  },
  PO_TAB_addIconStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.08,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.08,
  },
  PO_TAB_flatListMainView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: globals.screenHeight * 0.01,
  },
  PO_TAB_titleViewContainer: {
    marginLeft: globals.screenHeight * 0.018,
  },
  PO_TAB_titleTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
    marginBottom: globals.screenHeight * 0.01,
  },
  PO_TAB_linkTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
    color: colors.darkSkyBlue,
  },
  PO_TAB_EditDeleteBtnViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  PO_TAB_editIconStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    marginRight: globals.screenHeight * 0.01
  },
  PO_TAB_deleteIconStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    marginRight: globals.screenHeight * 0.04,
  },
  PO_TAB_underLineStyle: {
    height: 1,
    width: '100%',
    backgroundColor: colors.gray,
  },
  //:==================================Skills & Interests---------------------------------
  horizontalView:{
    flexDirection: 'row', marginBottom: globals.screenHeight * 0.015, marginHorizontal: globals.screenWidth * 0.04
  },
  findPersonView:{
    justifyContent: 'center', alignItems: 'flex-start', flex:1 
  },
  searchViewRight:{
    justifyContent: 'flex-end', alignItems: 'flex-end',flex:1
  },
  findPersonTextStyle:{
      fontSize:globals.font_14,
      color:colors.lightgray,
      fontWeight:'600'
  },
  grayBorderStyle:{
    width: globals.screenWidth, height: 1, bottom: 2, backgroundColor: colors.gray, opacity: 0.5,
    marginVertical:5, marginTop:10
  },
  connectionTabContainerStyle: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
  },
// ======: Modal Styles :=====

modalMainView:{
    height: globals.screenHeight * 0.43,
    justifyContent: 'center', alignItems: 'center',
    alignSelf:'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor:colors.proUnderline,
    borderRadius: 6,
    borderWidth:1,
    borderColor:colors.gray,
    marginTop: globals.screenHeight * 0.2
  },
  modalInnerMainView: {
    backgroundColor: colors.proUnderline,
    flex: 1,
    width: '85%',
    margin: 10,
  },
  add_not: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    marginTop:7,
    marginBottom : 5
  },
  underLineView:{
    height:1, backgroundColor:colors.gray, marginVertical:10, marginHorizontal:-10
  },
  addNoteTxt:{
    marginTop: 7, marginBottom:5, fontWeight:'500'
  },
  modalUperText:{
    fontSize:globals.font_16,
    fontWeight:'600',
  },
  addNoteTxt2:{
    marginTop: 7, marginBottom:5, fontWeight:'600'
  },
  modalUperText2:{
    fontSize:globals.font_13,
    fontWeight:'500',
  },
  textInputViewContainer: {
    flexDirection: 'row',
    marginVertical:globals.screenHeight * 0.01,
    borderWidth: 1,
    borderColor: colors.gray,
    height: globals.screenHeight * 0.17,
    // marginHorizontal: 10,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.01,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    // backgroundColor:'red',
    marginVertical:globals.screenHeight * 0.02,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginTop: 5,
    marginBottom: 5,
    height:globals.screenHeight * 0.15
  },
  modalEndView:{justifyContent:'flex-end', height:(globals.screenHeight*0.06), marginTop:10, flexDirection:'row',},
  closeView:{
    height: globals.screenHeight * 0.045, width: globals.screenWidth * 0.2, 
     borderWidth:1,  borderColor:colors.gray,
    borderRadius: 3,  alignItems: 'center', justifyContent: 'center'
  },
  sendView: {
    height: globals.screenHeight * 0.045, width: globals.screenWidth * 0.2, 
    backgroundColor: colors.listSelectColor,
    borderRadius: 3, marginLeft: 5, alignItems: 'center', justifyContent: 'center'
  },
  sendTxt: {
    fontSize: globals.font_13,
    color: colors.white
  },
  closeTxt: {
    fontSize: globals.font_13,
    color: colors.black
  },
  lastImgViewEnd:{
    justifyContent:'center', alignItems:'center',
  },
  connectedBtnView:{
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:colors.listSelectColor,
      height:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?globals.screenWidth*0.07:globals.screenWidth*0.09,
      width:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?globals.screenWidth*0.20:globals.screenWidth*0.22,
      borderRadius:5
  },
  connectTextStyle:{
      color:colors.white,
      fontSize:globals.font_13
  },
  lastImgViewEnd2:{
    justifyContent:'center', alignItems:'flex-end',
  },
  connection_TextStyle:{
    fontSize: globals.font_14, 
    color: colors.black,
    textAlign:'center'
  }
});


