/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Image, FlatList, TouchableOpacity, Alert, ActivityIndicator, TextInput } from 'react-native';
import styles from './style';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import * as images from '../../../../assets/images/map';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import AsyncStorage from '@react-native-community/async-storage';
import { API } from '../../../../utils/api';
import Modal from "react-native-modal";
import AntDesign from 'react-native-vector-icons/AntDesign'
import DeviceInfo from 'react-native-device-info';
import Validation from '../../../../utils/validation';
import globalStyles from '../../../../assets/styles/globleStyles';

const iPad = DeviceInfo.getModel();
let _this = null;

let TAG = "EndorsementTab:==="
class EndorsementTab extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            loading: false,
            matchesData: [],
            serverErr: false,
            visibleModal: false,
            content: '',
            receiver_Id: this.props.User_ID,
            Id: this.props.memberProfile.ID,
            isPrivate: this.props.memberProfile.IsPrivate,
            EndorsementsData: [],
            allData: [],
        }
    }

    componentDidMount() {
        const statusConnection = this.props.navigationProps.navigationProps.navigation.state.params.item.ConnectionStatus;
        if (this.state.isPrivate == true && statusConnection !== 2 && statusConnection !== undefined) {
            console.log(TAG,"This Account is Private"); 
        } else {
            if (globals.isInternetConnected === true) {
                this.setState({ loading: true })
                API.getProfileData(this.GetProfileDataResponseData, true, this.state.receiver_Id);
            } else {
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
            }
        }
    }

    GetProfileDataResponseData = {
        success: response => {
            console.log(
                TAG,
                'GetProfileDataResponseData-> success : ',
                response
            );
            if (response.StatusCode == 200) {
                if(response.Data !== null){
                let endorsements_data = response.Data.Endorsements;
                let all_data = response.Data;
                this.setState({ EndorsementsData: endorsements_data, allData: all_data })
                } else {
                    this.setState({EndorsementsData: []})
                }
            } else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false })
            }
            this.setState({ loading: false })
        },
        error: err => {
            this.setState({ loading: false })
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'GetProfileDataResponseData-> ERROR : ',
                JSON.stringify(err.message)
            );

            this.setState({ loading: false });
        },
        complete: () => {
            this.setState({ loading: false })
        },
    };

    _contentValidation(){
        const {content} = this.state;
        let regex = /^[^\s]+(\s+[^\s]+)*$/;
        if (Validation.textInputCheck(content) && content !== "") {
            if (regex.test(content)) {
                this.makeApiCall();
            } else {
                Alert.alert(globals.appName, "Content is Required");
                this.setState({ content: "",});
            }
        }
        else {
            Alert.alert(globals.appName, "Content is Required");
            this.setState({ content: ""});
          }
    }


    makeApiCall() {
        this.setState({ loading: true })
        const data = {
            "ID": this.state.Id,
            "SenderID": globals.userID,
            "ReceiverID": this.state.receiver_Id,
            "Content": this.state.content
        }
        this.handleModalStatus(false)
        if (globals.isInternetConnected === true) {
            API.SendUserEndorsements(this.sendUserEndorsementsResponse, data, true);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    sendUserEndorsementsResponse = {
        success: response => {
            console.log(
                TAG,
                'sendUserEndorsementsResponse -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                Alert.alert(globals.appName, response.Message);

            } else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false })
            }
            this.setState({ loading: false })
        },
        error: err => {
            this.setState({ loading: false })
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'sendUserEndorsementsResponse-> ERROR : ',
                JSON.stringify(err.message)
            );

            this.setState({ loading: false });
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
            this.setState({ content: '' })
        },
    };

    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        loginScreen.clearTextFields();
        this.props.navigationProps.navigation.navigate('LoginScreen');
    }

    handleModalStatus(visible) {
        this.setState({ visibleModal: visible })
    }

    handleNotes(text) {
        this.setState({
            content: text,
        });
    }

    closeRenderContentModal(){
        this.setState({ content: "" })
        this.handleModalStatus(false)
    }
    renderContentModal() {
        return (
            <Modal
                animationType="none"
                transparent={true}
                visible={this.state.visibleModal}
                onRequestClose={() => {
                    this.handleModalStatus(false);
                }}
                onBackdropPress={() => {
                    this.handleModalStatus(false);
                }}
            >
                <View style={styles.Endrs_MainViewStyle}>
                    <View style={styles.Endrs_ChildViewContainer}>
                        <View style={styles.Endrs_ViewStyle}>
                            <View style={styles.Endrs_IconViewStyle}>
                                <Text style={styles.Endrs_TextStyle}>Endorsements</Text>
                                <AntDesign name="closecircleo" size={globals.screenHeight * 0.03} color={colors.warmBlue} onPress={() => this.closeRenderContentModal()} />
                            </View>
                            <View style={styles.Endrs_InputViewStyle}>
                                <TextInput
                                    multiline={true}
                                    maxLength={100}
                                    returnKeyType="done"
                                    autoCapitalize="none"
                                    value={this.state.content}
                                    placeholderTextColor={colors.lightGray}
                                    onChangeText={text => this.handleNotes(text)}
                                    placeholder={'Add Content'}
                                    style={styles.Endrs_InputStyle}
                                />
                            </View>
                        </View>
                        <View style={styles.Endrs_BtnViewStyle}>
                            <TouchableOpacity style={styles.Endrs_OpacityStyle} onPress={() => this._contentValidation()}>
                                <Text style={styles.Endrs_sendTextStyle}>SEND</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }


    renderEndrosmentList(item, index) {
        const isSenderName = globals.checkObject(item, 'SenderName');
        const isSenderOrganization = globals.checkObject(item, 'SenderOrganization');
        const isSenderJobTitle = globals.checkObject(item, 'SenderJobTitle')
        const isSenderProfile = globals.checkImageObject(item, 'SenderProfile');
        const isContent = globals.checkObject(item, 'Content')
        return (
            <View>
                <View style={styles.list_mainContainer}>
                    <View style={styles.list_childContainer}>
                        <View style={styles.list_imageViewContainer}>
                            <Image
                                source={{ uri: (isSenderProfile) ? item.SenderProfile : globals.User_img }}
                                style={styles.list_imageStyleContainer}
                            />
                        </View>
                        <View style={styles.list_DetailViewContainer}>
                            <Text numberOfLines={1} style={styles.list_ReceiveTextstyle}>
                                {(isSenderName) ? item.SenderName : '-'}
                            </Text>
                            <Text numberOfLines={1} style={styles.list_jobTitlestyle}>
                                {(isSenderJobTitle) ? item.SenderJobTitle : '-'}
                            </Text>
                            <Text numberOfLines={1} style={styles.list_jobTitlestyle}>
                                {(isSenderOrganization) ? item.SenderOrganization : "-"}
                            </Text>
                        </View>

                    </View>
                    <View style={styles.list_ContentStyle}>
                        <Text style={styles.list_ContentTextStyle}>
                            {(isContent) ? item.Content : "-"}
                        </Text>
                        <View style={styles.list_clockIconViewStyle}>
                            <Image source={images.Endorsement.clock} style={styles.list_clockIconStyle} />
                            <Text style={styles.list_timeTextStyle}>{item.CreatedDateStr}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }

    render() {
        const { EndorsementsData, loading, isPrivate } = this.state;
        const statusConnection = this.props.navigationProps.navigationProps.navigation.state.params.item.ConnectionStatus;
        return (
            <View style={styles.mainViewSimilarInterst}>

                {this.renderContentModal()}
                {(isPrivate == true && (statusConnection == 2 || statusConnection == undefined)) ?
                    (loading == true) ?
                        <View style={[globalStyles.nodataStyle, { backgroundColor: colors.white }]}>
                            <ActivityIndicator size="small" style={globalStyles.nodataStyle} color={colors.bgColor} />
                        </View>
                        :
                        (EndorsementsData.length > 0 && EndorsementsData !== null) ?
                            <FlatList
                                style={{ flex: 1 }}
                                showsVerticalScrollIndicator={false}
                                data={EndorsementsData}
                                renderItem={({ item, index }) => this.renderEndrosmentList(item, index)}
                                extraData={this.state}
                                bounces={false}
                                keyExtractor={(index, item) => item.toString()}
                            />
                            :
                            <View style={globalStyles.nodataStyle}>
                                <Text style={globalStyles.nodataTextStyle}>There are no endorsements to display</Text>
                            </View>
                    :
                    (isPrivate == true && statusConnection !== 2 && statusConnection !== undefined) ?
                        <View style={styles.connectionTabContainerStyle}>
                            <Text style={styles.notAvialableTextStyle}>This Account is Private</Text>
                        </View>
                        :
                        (loading == true) ?
                            <View style={[globalStyles.nodataStyle, { backgroundColor: colors.white }]}>
                                <ActivityIndicator size="small" style={globalStyles.nodataStyle} color={colors.bgColor} />
                            </View>
                            :
                            (EndorsementsData.length > 0 && EndorsementsData !== null) ?
                                <FlatList
                                    style={{ flex: 1 }}
                                    showsVerticalScrollIndicator={false}
                                    data={EndorsementsData}
                                    renderItem={({ item, index }) => this.renderEndrosmentList(item, index)}
                                    extraData={this.state}
                                    bounces={false}
                                    keyExtractor={(index, item) => item.toString()}
                                />
                                :
                                <View style={globalStyles.nodataStyle}>
                                    <Text style={globalStyles.nodataTextStyle}>There are no endorsements to display</Text>
                                </View>
                }
                {
                    (isPrivate == true && statusConnection !== 2 && statusConnection !== undefined) ?
                        null :
                        <TouchableOpacity style={styles.iconViewStyle} onPress={() => this.handleModalStatus(true)}>
                            <Image source={images.EventRegistration.addIcon}
                                resizeMode='contain'
                                style={styles.IconsStyle}
                            />
                        </TouchableOpacity>
                }

            </View>
        );
    }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EndorsementTab);
