/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import { Text, View, ScrollView } from 'react-native';
import styles from './style';
import globalStyles from '../../../../assets/styles/globleStyles';
import { connect } from 'react-redux';
import * as globals from '../../../../utils/globals';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';


class BusinessCategoryTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      businessCategoryData: this.props.businessDescriptionDataList,
      renderData: this.props.businessDescriptionDataList.BusinessCategories,
      categoryArrar: [],
    }
  }

  componentDidMount(){
    let arrTemp = [];
    let mainData = this.state.renderData
    for (let i = 0; i < mainData.length; i++) {
      var CategoryName = mainData[i].CategoryName
         let obj = {
        Name: CategoryName
      };
      arrTemp.push(obj);
      this.setState({categoryArrar: arrTemp})
    }

  }

  render() {
    const { categoryArrar } = this.state;
    return (
      <View style={styles.container}>
        {
          (categoryArrar != null && categoryArrar.length > 0) ?
            <>
              <ScrollView bounces={false}>
                <View style={styles.mainView}>
                  <TagSelect
                    isFrom="Business_Categories"
                    data={categoryArrar}
                    itemStyle={styles.item}
                    itemLabelStyle={styles.label}
                    itemStyleSelected={styles.itemSelected}
                    itemLabelStyleSelected={styles.labelSelected}
                  />
                </View>
              </ScrollView>
            </>
            :
            <View style={globalStyles.nodataStyle}>
              <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.BUSINESS_CATEGORY}</Text>
            </View>
        }

      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessCategoryTab);