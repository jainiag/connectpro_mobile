import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

var iPad = DeviceInfo.getModel()

module.exports = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
  },
  container: {
    flex: 1,
  },
  settingStyle:{
    height: globals.screenHeight * 0.035,
    width: globals.screenHeight * 0.035,
    borderRadius:(globals.screenHeight * 0.035/2)
  },
  profileView: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 10,
    marginLeft: 10,
    alignItems: 'center',
  },
  userInfoView: {
    justifyContent: 'center',
    marginLeft: 10,
  },
  drawerSaperator: {
    width: '100%',
    height: 1,
    backgroundColor: colors.gray,
  },
  drawerImageView: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  drawerImageLogoView: {
    width: 30,
    height: 30,
  },
  profilePicImg: {
    width: globals.screenWidth * 0.16,
    height: globals.screenWidth * 0.16,
    borderRadius: (globals.screenWidth * 0.16) / 2,
  },
  userNameText: {
    fontSize: globals.font_15,
    marginBottom: 3,
  },
  contactNumText: {
    opacity: Platform.OS === 'android' ? 0.7 : 0.7,
  },
  sliderMenuUpperSafeareaView: {
    flex: 0,
  },
  headerContainer: {
    backgroundColor: colors.HeaderColor,
    alignItems: 'center',
    justifyContent: 'center',
    height: globals.screenHeight * 0.08,
    marginBottom: 30,
  },
  headerTextStyle: {
    fontWeight: '500',
    fontSize: globals.font_20,
    color: colors.white,
  },
  navItemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    marginVertical: 5,
    height: globals.screenHeight * 0.05,
    alignItems: 'center',
  },
  navItemContainStyle: {
    fontSize: globals.font_15,
    flex: 1,
    marginLeft: 20,
  },
  iconContainer: {
    alignItems: 'flex-end',
  },
  lineStyle: {
    borderBottomWidth: 1,
    borderColor: colors.separate,
  },
  footerSupportView: {
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: colors.gray,
    flexDirection: 'row',
    bottom: 0,
    position: 'absolute',
  },
  iconCallEmail: {
    flex: 1,
    width: 50,
    height: 50,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerCallEmailIcon: {
    width: 50,
    height: 50,
  },
  supportText: {
    marginTop: 8,
    fontSize: globals.font_11,
    color: colors.purple,
  },
  supportBoxView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: colors.gray,
    padding: 10,
  },
  headerStyle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.black,
    elevation: 0,
    shadowColor: 'transparent',
    textAlign: 'center',
  },
  headerGroupStyle: {
    flex: 2,
    textAlign: 'center',
    // fontFamily: globals.SFProTextRegular,
    fontSize: globals.font_17,
    color: colors.darkgrey,
  },
  menuTextStyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 27 : globals.font_20,
    color: colors.black,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 15,
    marginBottom: (DeviceInfo.getModel() === ' iPhone 6s Plus') ? 10 : 0,
    fontWeight: '500',
    marginTop: ((iPad.indexOf('iPad')!=-1)) ? 4 : 0,
  },
  profileViewContainer: {
    marginTop: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.07 : globals.screenWidth * 0.03,
    marginBottom: globals.screenHeight * 0.015,
    alignItems: 'center',
  },
  imgAndIconViewContainer: {
    // flexDirection: 'row',
  },
  before_imageView:{
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140 : globals.screenWidth * 0.26,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140 : globals.screenWidth * 0.26,
    borderRadius: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140/2 : (globals.screenWidth * 0.26) / 2,
    borderWidth: 0.5,
    borderColor: colors.proUnderline,
  },
  profile_ImageStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140 : globals.screenWidth * 0.26,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140 : globals.screenWidth * 0.26,
    borderRadius: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140/2 : (globals.screenWidth * 0.26) / 2,
    borderWidth: 0.5,
    borderColor: colors.proUnderline
  },
  settingOpacityContainer: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 40 : globals.screenWidth * 0.085,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 40 : globals.screenWidth * 0.085,
    borderRadius: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 40 : (globals.screenWidth * 0.085) / 2,
    backgroundColor: colors.darkSkyBlue,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    marginLeft: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? Platform.OS === 'ios' ? globals.screenWidth * 0.11 : globals.screenWidth * 0.088 : globals.screenWidth * 0.17,
    bottom: 0,
  },
  settingIconStyle: {
    alignSelf: 'center',
  },
  userNameAndEmailTextViewContainer: {
    flexDirection: 'column',
    marginTop: 10,
    // justifyContent: 'center',
    alignItems: 'center'
  },
  userNameTextStyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 27 : globals.font_20,
    color: colors.black,
    marginBottom: 5,
    width: globals.screenWidth * 0.5,
    textAlign: 'center',
  },
  userEmailTextStyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 24 : globals.font_15,
    color: colors.lightgray,
    textAlign: 'center',
  },
  IconsViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  playIconViewStyle: {
    backgroundColor: colors.darkSkyBlue,
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    width:((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: globals.screenWidth * 0.026,
    borderRadius: 3,
  },
  iconsStyles: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.066,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.066,
  },
  linkedinAndTwitterIconViewStyle: {
    backgroundColor: colors.darkSkyBlue,
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    width:((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 3,
  },
  fbIconStyle: {
    backgroundColor: colors.darkSkyBlue,
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    width:((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: globals.screenWidth * 0.026,
    borderRadius: 3,
  },
  googlePlusIconsStyle: {
    backgroundColor: colors.darkSkyBlue,
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    width:((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.05 : globals.screenWidth * 0.072,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: globals.screenWidth * 0.026,
    borderRadius: 3,
  },
  applyBtnViewStyle: {
    height: globals.screenHeight * 0.04,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.36 : globals.screenWidth * 0.47,
    alignItems: 'center',
    backgroundColor: colors.darkSkyBlue,
    justifyContent: 'center',
    borderRadius: 3,
    marginBottom: globals.screenHeight * 0.024,
  },
  applyTextStyle: {
    color: colors.white,
    fontSize: globals.font_14,
  },
  tabViewContainer: {
    width: globals.screenWidth,
    alignSelf: 'center',
    // marginTop: globals.screenHeight * 0.018,
  },
  tabbarStyle: {
    backgroundColor: colors.transparent,
    width: globals.screenWidth * 0.957,
    borderBottomWidth: 1.5,
    borderBottomColor: colors.proUnderline,
    alignSelf: 'flex-end',
    alignContent: 'center',
    justifyContent: 'center',
    fontSize: globals.font_15,
    elevation: 0,
    shadowOpacity: 0,
    height: 46,
  },
  tabStyle: {
    backgroundColor: colors.transparent,
    width: globals.screenWidth * 0.3,
    height: globals.screenHeight * 0.054,
    top: 2,
  },
  indicatorStyle: {
    backgroundColor: colors.warmBlue,
    height: 2,
  },
  labelStyle: {
    textAlign: 'center',
    width: globals.screenWidth / 3,
    fontSize: globals.font_15,
    letterSpacing: globals.ltr_sp_016,
    fontWeight: '600',
    color: colors.black,
  },
  initialLayoutStyle: {
    width: globals.screenWidth - 10,
    flex: 1,
    height: 20,
  },
});
