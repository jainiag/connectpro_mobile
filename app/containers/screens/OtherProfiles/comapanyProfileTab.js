/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, Linking, Alert } from 'react-native';
import styles from './style';
import * as globals from '../../../utils/globals';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeviceInfo from 'react-native-device-info';
import ImageLoad from 'react-native-image-placeholder';
import * as colors from '../../../assets/styles/color';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import * as images from '../../../assets/images/map';
import { bindActionCreators } from 'redux';
import { API } from '../../../utils/api';
import AsyncStorage from '@react-native-community/async-storage';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import BusinessCategoryTab from './companyProfileTab/businessCategoryTab';
import BusinessDescriptionTab from './companyProfileTab/businessDescriptionTab';
import RNFS from 'react-native-fs';
import loginScreen from '../AuthenticationScreens/loginScreen';
import { ScrollableTabView, ScrollableTabBar } from '../../../libs/react-native-scrollable-tabview/lib'


let _this = null;
let apiImage;
const TAG = '==:== CompanyProfileTab : ';
const iPad = DeviceInfo.getModel();
class CompanyProfileTab extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: false,
      getCompanyProfile: [],
      businessCategoryData: [],
      sourceImage: apiImage,
      isLoading: false,
      isRequestFailed: false,
      connectionStatus: this.props.navigationProps.navigation.state.params.item.ConnectionStatus,
      ItemData: this.props.navigationProps.navigation.state.params.item,
      user_ID: this.props.User_ID,
    }
  }

  static _refreshCompanyData() {
   _this.GetCompanyProfile()
  }

  componentDidMount() {
    this.GetCompanyProfile()
  }

  /**
   * Api call of get Company Profile
   */
  GetCompanyProfile() {
    this.props.showLoader()
    this.setState({ loading: true })
    AsyncStorage.getItem(globals.OTHER_ORG_ID).then(token => {
      if (token !== "null") {
        globals.other_org_id = token;
      } else {
        globals.other_org_id = 0;
      }
      if (globals.isInternetConnected === true) {
        API.getCompanyProfile(this.getEventDetailsResponseData, true, globals.other_org_id, this.state.user_ID);
      } else {
        this.props.hideLoader()
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    })
  }

  /**
     * Response callback of getEventDetails
     */
  getEventDetailsResponseData = {
    success: response => {
      console.log(
        TAG,
        'getEventDetailsResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        const responseData = response.Data.CompanyDetails;
        const businessCategoryresponseData = response.Data.CompanyDetails.BusinessCategories;
        this.setState({ getCompanyProfile: responseData, businessCategoryData: businessCategoryresponseData })
        BusinessDescriptionTab.setDescriptionData(this.state.getCompanyProfile);
      }
      else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false })
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'getEventDetailsResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.props.hideLoader();
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          globals.appName,
          err.Message
        );
        this.setState({isRequestFailed: true})
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };


  /**
     * Method called when session expire
     */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigationProps.navigation.navigate('LoginScreen');
  }

  /*
  * this method for Linkedin open in default browser
  */
  _opneLinkedin = (LinkedinURl) => {
    let URL = LinkedinURl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        return Linking.openURL(URL);
      } else {
        return Linking.openURL(URL)
      }
    });
  };


  /*
   * this method for Twitter open in default browser
   */
  _opneTwitter = (TwitterURl) => {
    let URL = TwitterURl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        Linking.openURL(URL);
      } else {
        Linking.openURL(URL)
      }
    });
  };



  /*
  * this method for _openFacebook open in default browser
  */
  _openFacebook = (FacebookURl) => {
    let URL = FacebookURl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        Linking.openURL(URL);
      } else {
        Linking.openURL(URL)
      }
    });
  };


  render() {
    const { getCompanyProfile, sourceImage, isLoading } = this.state;
    const isCompanyLogoPath = globals.checkImageObject(getCompanyProfile, 'CompanyLogoPath');
    const isName = globals.checkObject(getCompanyProfile, 'Name');
    const isEmail = globals.checkObject(getCompanyProfile, 'Email');
    const isLinkedInUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'LinkedInUrl')
    const isFacebookUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'FacebookUrl')
    const isTwitterUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'TwitterUrl')
    const LinkedinURl = (isLinkedInUrl) ? getCompanyProfile.SocialLinksobj.LinkedInUrl : '';
    const FacebookURl = (isFacebookUrl) ? getCompanyProfile.SocialLinksobj.FacebookUrl : '';
    const TwitterURl = (isTwitterUrl) ? getCompanyProfile.SocialLinksobj.TwitterUrl : '';


    apiImage = getCompanyProfile.CompanyLogoPath;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.profileViewContainer}>
          <View style={styles.imgAndIconViewContainer}>
            <View>
              <ImageLoad
                source={{ uri: (isCompanyLogoPath) ? getCompanyProfile.CompanyLogoPath : globals.User_img }}
                style={styles.profile_ImageStyle}
                placeholderSource={images.MyProfile.placeHolder}
                placeholderStyle={styles.profile_ImageStyle}
                isShowActivity={true}
                borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 140/2 : (globals.screenWidth * 0.26) / 2}
              />
            </View>           
          </View>
          <View style={styles.userNameAndEmailTextViewContainer}>
            <Text style={styles.userNameTextStyle} numberOfLines={1}>
              {(isName) ? getCompanyProfile.Name : '-'}
            </Text>
            <Text style={styles.userEmailTextStyle}>{(isEmail) ? getCompanyProfile.Email : '-'}</Text>
          </View>
        </View>
        <View style={styles.IconsViewStyle}>
          {(LinkedinURl == "") ? null :
            <View style={styles.linkedinAndTwitterIconViewStyle}>
              <TouchableOpacity onPress={() => this._opneLinkedin(LinkedinURl)}>
                <Image
                  source={images.MyProfile.linkedIn}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(FacebookURl == "") ? null :
            <View style={styles.fbIconStyle}>
              <TouchableOpacity onPress={() => this._openFacebook(FacebookURl)}>
                <Image
                  source={images.MyProfile.fbBlue}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(TwitterURl == "") ? null :
            <View style={styles.linkedinAndTwitterIconViewStyle}>
              <TouchableOpacity onPress={() => this._opneTwitter(TwitterURl)}>
                <Image
                  source={images.MyProfile.twitter}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
        </View>
        <ScrollableTabView
          initialPage={0}
          tabBarTextStyle={{fontSize:globals.font_16}}
          tabBarInactiveTextColor={colors.black} 
          tabBarActiveTextColor={colors.warmBlue}
          renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
        >
          <BusinessDescriptionTab tabLabel={'Business Description'} navigationProps={this.props} isRequest={this.state.isRequestFailed} Org_ID={globals.organizationId}/>
          <BusinessCategoryTab tabLabel={'Business Categories'} navigationProps={this.props} businessDescriptionDataList={this.state.getCompanyProfile} />
        </ScrollableTabView>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyProfileTab);
