/* eslint-disable react/no-unused-state */
import React from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as colors from '../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../utils/globals';
import globalStyles from '../../../assets/styles/globleStyles';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import Profile from './profile';
import CompanyProfileTab from './comapanyProfileTab';
import { ScrollableTabView, ScrollableTabBar } from '../../../libs/react-native-scrollable-tabview'

const TAG = '==:== OtherProfile : ';
let _this;
const iPad = DeviceInfo.getModel();

class OtherProfile extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    console.log("params :: ", params);

    const MemberName =
      (params.isfrom == "AttendeeName") ? params.item.AttendeeName :
        (params.item.MemberName) ? params.item.MemberName :
          (params.isfrom == "myConnection") ?
            (params.item.SenderID == globals.userID) ?
              params.item.ReceiverName :
              params.item.SenderName :
            (params.item.UserName) ? params.item.UserName :
              (params.isfrom == "newConnection") ?
                (params.item.SenderID == globals.userID) ? params.item.ReceiverName : params.item.SenderName : (params.item.UserName) ? params.item.UserName : (params.isfrom == "chatScreen") ? params.item.SenderName : params.item.FullName;
    return {
      headerLeft: globals.ConnectProbackButton(navigation, MemberName + " Profile"),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }
  };

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: true,
      isSearh: false,
      User_ID: this.props.navigation.state.params.User_ID,
      selectedTab: 1,
      index: 0,
      routes: [
        { key: 'MyProfile', title: 'My Profile' },
        { key: 'CompanyProfile', title: 'Company Profile' },
      ],
    };
  }

  render() {
    return <ScrollableTabView
      style={{ marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.01 }}
      initialPage={0}
      tabBarTextStyle={{ fontSize: globals.font_16 }}
      tabBarInactiveTextColor={colors.black}
      tabBarActiveTextColor={colors.warmBlue}
      tabsContainerStyle={{ width: 40 }}
      // tabBarUnderlineStyle={{width:'10%'}}
      renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
    >
      <Profile tabLabel={'Profile'} navigationProps={this.props} User_ID={this.state.User_ID} />
      <CompanyProfileTab tabLabel={'Organization Details'} navigationProps={this.props} User_ID={this.state.User_ID} />
    </ScrollableTabView>
  }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OtherProfile);
