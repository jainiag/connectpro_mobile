/* eslint-disable no-duplicate-case */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, Alert, Linking } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import * as globals from '../../../utils/globals';
import * as images from '../../../assets/images/map';
import EndorsementTab from './otherProfileTab/endorsementTab';
import ProfileCategoryTab from './otherProfileTab/profileCategoryTab';
import ProfessionalSummary from './otherProfileTab/professionalSummary';
import SkillsAndInterests from './otherProfileTab/skilsAndInterestsTab';
import ConnectionsTab from './otherProfileTab/connectionsTab';
import * as colors from '../../../assets/styles/color';
import PortfolioTab from './otherProfileTab/portfolioTab';
import { ScrollableTabView, ScrollableTabBar } from '../../../libs/react-native-scrollable-tabview'
import { API } from '../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ImageLoad from 'react-native-image-placeholder';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import AsyncStorage from '@react-native-community/async-storage';
import loginScreen from '../AuthenticationScreens/loginScreen';
import { NavigationEvents, StackActions, NavigationActions } from 'react-navigation';


const iPad = DeviceInfo.getModel();
let TAG = 'Profile Screen:==='
let apiImage;

let _this = null;
class Profile extends React.Component {
    constructor(props) {
        super(props);
        _this = this;
        const Conct_StatusID = this.props.navigationProps.navigation.state.params.item.ConnectionStatus
        const Status_ID = this.props.navigationProps.navigation.state.params.item.StatusID
        const isConnected = this.props.navigationProps.navigation.state.params.item.IsConnected
        console.log("Profile isConnected-->", isConnected)
        console.log("Profile Status_ID->", Status_ID)
        console.log("Profile Conct_StatusID-->", Conct_StatusID)

        const isConnectionStatus = (isConnected == true) ? 2 : 0;
        this.state = {
            myProfileData: [],
            memberProfile: [],
            loading: false,
            sourceImage: apiImage,
            profileAllData: [],
            isLoading: false,
            user_id: this.props.User_ID,
            connectionStatus: (Conct_StatusID) ? Conct_StatusID : (Status_ID) ? Status_ID : isConnectionStatus,
            sendStatus: Status_ID,
            ItemData: this.props.navigationProps.navigation.state.params.item,
            Org_ID: null,
            isFrom: this.props.navigationProps.navigation.state.params.isfrom,
        }



    }


    /**
     * static method for refresh My Profile
     */
    static refreshMyProfile() {
        _this.GetMyProfile()
    }

    componentDidMount() {
        this.GetMyProfile()
    }


    /**
   * Method called when session expire
   */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        loginScreen.clearTextFields();
        this.props.navigationProps.navigation.navigate('LoginScreen');
    }

    /**
     * Api call of get my Profile
     */
    GetMyProfile() {
        this.props.showLoader()
        this.setState({ loading: true })
        if (globals.isInternetConnected === true) {
            API.getOtherProfileDetails(this.getOtherProfileDetailsResponse, true, this.state.user_id);
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
     * Response callback of getMyProfile
     */
    getOtherProfileDetailsResponse = {
        success: response => {
            console.log(
                TAG,
                'getOtherProfileDetailsResponse-> success : ',
                response
            );
            if (response.StatusCode == 200) {
                AsyncStorage.setItem(globals.OTHER_ORG_ID, JSON.stringify(response.Data.MemberInfo.OrganizationID));
                const responseData = response.Data.MemberInfo;
                const responseMemberProfile = response.Data.MemberProfile
                this.setState({ myProfileData: responseData })
                this.setState({ memberProfile: responseMemberProfile })
                this.setState({ profileAllData: response.Data, Org_ID: response.Data.MemberInfo.OrganizationID })
                ProfessionalSummary.getMemberProfileData(
                    this.state.memberProfile,
                    this.state.myProfileData,
                    this.state.myProfileData.EmailID,
                    this.state.myProfileData.OrganizationName,
                    this.state.profileAllData,
                );
            } else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false })
            }
            this.props.hideLoader()
        },
        error: err => {
            this.setState({ loading: false })
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.props.hideLoader();
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(
                    globals.appName,
                    err.Message
                );
            }
            console.log(
                TAG,
                'getOtherProfileDetailsResponse-> ERROR : ',
                JSON.stringify(err.message)
            );

            this.setState({ loading: false });
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
   * Method for open Facebook Url
   */
    _opneFacebook = (FacebookUrl) => {
        let URL = FacebookUrl
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                return Linking.openURL(URL);
            } else {
                return Linking.openURL(URL)
            }
        });
    };

    /*
    * this method for Linkedin open in default browser
    */
    _opneLinkedin = (LinkedInUrl) => {
        let URL = LinkedInUrl
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                return Linking.openURL(URL);
            } else {
                return Linking.openURL(URL)
            }
        });
    };


    /*
     * this method for Twitter open in default browser
     */
    _opneTwitter = (TwitterUrl) => {
        let URL = TwitterUrl
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                return Linking.openURL(URL);
            } else {
                return Linking.openURL(URL)
            }
        });
    };

    /*
     * this method for Twitter open in default browser
     */
    _opneYoutube = (YouTubeUrl) => {
        let URL = YouTubeUrl
        Linking.canOpenURL(URL).then(supported => {
            if (supported) {
                return Linking.openURL(URL);
            } else {
                return Linking.openURL(URL)
            }
        });
    };


    clearfileds() {
        // this.setState({
        //     myProfileData: [],
        //     memberProfile: [],
        //     profileAllData: [],
        // })
    }

    render() {
        const { myProfileData, sourceImage, memberProfile, isLoading } = this.state;
        const isProfileImage = globals.checkImageObject(memberProfile, 'ProfilePicturePath');
        const isFullName = globals.checkObject(myProfileData, 'FullName');
        const isEmailId = globals.checkObject(myProfileData, 'EmailID');
        const EmailID = (isEmailId) ? myProfileData.EmailID : '-';
        const isyouTubeUrl = globals.checkObject(memberProfile, 'YouTubeUrl')
        const islinkedInUrl = globals.checkObject(memberProfile, 'LinkedInUrl')
        const isfacebookUrl = globals.checkObject(memberProfile, 'FacebookUrl')
        const istwitterUrl = globals.checkObject(memberProfile, 'TwitterUrl')
        const YouTubeUrl = (isyouTubeUrl) ? memberProfile.YouTubeUrl : '';
        const LinkedInUrl = (islinkedInUrl) ? memberProfile.LinkedInUrl : '';
        const FacebookUrl = (isfacebookUrl) ? memberProfile.FacebookUrl : '';
        const TwitterUrl = (istwitterUrl) ? memberProfile.TwitterUrl : '';
        apiImage = memberProfile.ProfilePicturePath;
        AsyncStorage.getItem(globals.MYPROFILEPICTURE).then(token => {
            globals.my_profile_picture = token;
        });
        const isPrivateCheck = globals.checkObject(memberProfile, 'IsPrivate')
        const isEmailPrivateCheck = globals.checkObject(memberProfile, 'IsEmailPrivate')
        const isPhonePrivateCheck = globals.checkObject(memberProfile, 'IsPhonePrivate')
        const isPrivate = (isPrivateCheck) ? memberProfile.IsPrivate : '-';
        const isEmailPrivate = (isEmailPrivateCheck) ? memberProfile.IsEmailPrivate : '-';
        const isPhonePrivate = (isPhonePrivateCheck) ? memberProfile.IsPhonePrivate : '-';
        const isConnected = this.props.navigationProps.navigation.state.params.item.IsConnected




        return (
            <View style={styles.mainContainer}>
                <NavigationEvents
                    onWillBlur={() => this.clearfileds()}
                />
                {(isPrivate == true) ?
                    <View style={styles.profileViewContainer}>
                        <View style={styles.imgAndIconViewContainer} >
                            <View>
                                <ImageLoad
                                    source={{ uri: (isProfileImage) ? memberProfile.ProfilePicturePath : globals.User_img }}
                                    style={styles.profile_ImageStyle}
                                    placeholderSource={images.MyProfile.placeHolder}
                                    placeholderStyle={styles.profile_ImageStyle}
                                    isShowActivity={true}
                                    borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 140 / 2 : (globals.screenWidth * 0.26) / 2}
                                />
                            </View>
                        </View>
                        <View style={styles.userNameAndEmailTextViewContainer}>
                            <Text style={styles.userNameTextStyle} numberOfLines={1}>
                                {(isFullName) ? myProfileData.FullName : '-'}
                            </Text>
                        </View>
                    </View>
                    :
                    <View style={styles.profileViewContainer}>
                        <View style={styles.imgAndIconViewContainer} >
                            <View>
                                <ImageLoad
                                    source={{ uri: (isProfileImage) ? memberProfile.ProfilePicturePath : globals.User_img }}
                                    style={styles.profile_ImageStyle}
                                    placeholderSource={images.MyProfile.placeHolder}
                                    placeholderStyle={styles.profile_ImageStyle}
                                    isShowActivity={true}
                                    borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 140 / 2 : (globals.screenWidth * 0.26) / 2}
                                />
                            </View>
                        </View>
                        <View style={[styles.userNameAndEmailTextViewContainer, { marginBottom: 8 }]}>
                            <Text style={styles.userNameTextStyle} numberOfLines={1}>
                                {(isFullName) ? myProfileData.FullName : '-'}
                            </Text>
                            {(isPrivate == false) ?
                                (isEmailPrivate == false) ?
                                    <Text style={styles.userEmailTextStyle}>{EmailID}</Text>
                                    :
                                    null
                                :
                                null
                            }
                        </View>

                        <View style={styles.IconsViewStyle}>
                            {(YouTubeUrl == "") ? null
                                :
                                <View style={styles.playIconViewStyle}>
                                    <TouchableOpacity onPress={() => this._opneYoutube(YouTubeUrl)}>
                                        <Image
                                            source={images.MyProfile.play}
                                            resizeMode="contain"
                                            style={styles.iconsStyles}
                                        />
                                    </TouchableOpacity>
                                </View>
                            }
                            {(LinkedInUrl == "") ? null :
                                <View style={styles.linkedinAndTwitterIconViewStyle}>
                                    <TouchableOpacity onPress={() => this._opneLinkedin(LinkedInUrl)}>
                                        <Image
                                            source={images.MyProfile.linkedIn}
                                            resizeMode="contain"
                                            style={styles.iconsStyles}
                                        />
                                    </TouchableOpacity>
                                </View>
                            }
                            {(FacebookUrl == "") ? null
                                :
                                <View style={styles.fbIconStyle}>
                                    <TouchableOpacity onPress={() => this._opneFacebook(FacebookUrl)}>
                                        <Image
                                            source={images.MyProfile.fbBlue}
                                            resizeMode="contain"
                                            style={styles.iconsStyles}
                                        />
                                    </TouchableOpacity>
                                </View>
                            }
                            {(TwitterUrl == "") ? null
                                :
                                <View style={styles.linkedinAndTwitterIconViewStyle}>
                                    <TouchableOpacity onPress={() => this._opneTwitter(TwitterUrl)}>
                                        <Image
                                            source={images.MyProfile.twitter}
                                            resizeMode="contain"
                                            style={styles.iconsStyles}
                                        />
                                    </TouchableOpacity>
                                </View>
                            }
                        </View>
                    </View>}


                <ScrollableTabView
                    initialPage={0}
                    tabBarTextStyle={{ fontSize: globals.font_16 }}
                    tabBarInactiveTextColor={colors.black}
                    tabBarActiveTextColor={colors.warmBlue}
                    renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
                >
                    <ProfessionalSummary tabLabel={'Professional Summary'} navigationProps={this.props} />
                    <PortfolioTab tabLabel={'Portfolio'} navigationProps={this.props} User_ID={this.state.user_id} memberProfile={memberProfile} />
                    <EndorsementTab tabLabel={'Endorsement'} navigationProps={this.props} User_ID={this.state.user_id} memberProfile={memberProfile} />
                    <ProfileCategoryTab tabLabel={'Profile Category'} navigationProps={this.props} User_ID={this.state.user_id} memberProfile={memberProfile} />
                    <SkillsAndInterests tabLabel={'Skills & Interests'} navigationProps={this.props} User_ID={this.state.user_id} memberProfile={memberProfile} />
                    <ConnectionsTab tabLabel={'Connections' + ' '} navigationProps={this.props} User_ID={this.state.user_id} memberProfile={memberProfile} item={this.state.ItemData} ConnectionStatus={this.state.connectionStatus} memberProfile={memberProfile} isConnected={isConnected} isFrom={this.state.isFrom} sendStatus={this.state.sendStatus} />
                </ScrollableTabView>
            </View>
        );
    }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Profile);