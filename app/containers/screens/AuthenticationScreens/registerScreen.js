/* eslint-disable func-names */
/* eslint-disable no-return-assign */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-console */
/* eslint-disable no-shadow */
/* eslint-disable eqeqeq */
/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  SafeAreaView,
  ScrollView,
  Alert,
  ImageBackground,
  Linking,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import styles from './style';
import Validation from '../../../utils/validation';
import CustomButton from '../../../components/CustomButton/index';
import { API } from '../../../utils/api';
import * as images from '../../../assets/images/map';
import CustomHeader from '../../../components/CustomHeader/index';
import LoginScreen from './loginScreen';
import WebViewComponent from '../../../components/WebView/index';
import { NavigationActions, StackActions } from 'react-navigation';

const TAG = 'RegisterScreen ::=';
let _this;

class RegisterScreen extends Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      userFirstName: '',
      userLastName: '',
      userEmail: '',
      userPassword: '',
      loading: false,
      isterms: false,
      isPrivacy: false,
      isFblogin: true,
      userName: '',
      userFbid: '',
    };
  }

  /**
   * method for validation user registration
   */
  userRegisterValidation() {
    const { userFirstName, userLastName, userEmail, userPassword, userFbid, userName, isFblogin } = this.state;

    if (Validation.textInputCheck(userFirstName)) {
      if (Validation.textInputCheck(userLastName)) {
        if (Validation.textInputCheck(userEmail)) {
          if (Validation.isValidEmail(userEmail)) {
            if (Validation.textInputCheck(userPassword) || (isFblogin === false)) {
              if (Validation.passwordLength(userPassword, 7) || (isFblogin === false)) {
                if (Validation.isValidPassword(userPassword) || (isFblogin === false)) {
                  if (isFblogin === true) {
                    this.setState({ loading: true });
                    const data = {
                      PhoneNumber: '',
                      FirstName: userFirstName,
                      LastName: userLastName,
                      Email: userEmail,
                      Password: userPassword,
                    };
                    if (globals.isInternetConnected === true) {
                      API.userRegister(this.userRegisterResponseData, data, true);
                    } else {
                      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
                      this.setState({ loading: false });
                    }
                  } else {
                    this.setState({ loading: true });
                    const data = {
                      "email": userEmail,
                      "id": userFbid,
                      "first_name": userFirstName,
                      "last_name": userLastName,
                      "name": userName
                    };
                    if (globals.isInternetConnected === true) {
                      API.externalLoginCallbackAPI(this.externalLoginCallbackapiResponse, data, true);
                    } else {
                      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
                      this.setState({ loading: false });
                    }
                  }
                } else {
                  Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_PASSVALID);
                  this.setState({ loading: false });
                }
              } else {
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_NEED_7_CHARACTER);
              }
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_PASSWORD);
            }
          } else {
            Alert.alert(globals.appName, globals.MESSAGE.REGISTERSTACK.REG_EMAILVALID);
            this.setState({ loading: false });
          }
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTEREMAIL);
        }
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.REGISTERSTACK.REG_LASTNAME);
      }
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.REGISTERSTACK.REG_FIRSTNAME);
    }
  }

  /**
   * storing data into Async
   * @function storeUserInfo
   * @param {*} response data to store
   */
  storeUserInfo(response) {
    globals.USERID = response.Id;
    globals.USERFIRSTNAME = response.Data.FirstName;
    globals.USERLASTNAME = response.Data.LastName;
    globals.USEREMAIL = response.Data.Email;
    globals.USERPASS = response.Data.Password;
  }

  /**
   * API userRegister response
   */
  userRegisterResponseData = {
    success: response => {
      console.log(TAG, 'RegisterAPI response==', response);
      if (response.StatusCode == 200) {
        if (response.Result === true) {
          AsyncStorage.setItem(globals.LOGINRESPONSEKEY, JSON.stringify(response.Data));
          this.storeUserInfo(response);
          Alert.alert(globals.appName, response.Message, [
            { text: 'OK', onPress: () => this.props.navigation.navigate('LoginScreen') },
          ]);
          LoginScreen.clearTextFields();
        } else {
          Alert.alert(globals.appName, response.Message, [{ text: 'OK' }]);
        }
        this.setState({ loading: false });
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => {},
  };

   /**
   * API externalLoginCallbackAPI response
   */

  externalLoginCallbackapiResponse = {
    success: response => {
      console.log(TAG, 'externalLoginCallbackapiResponse', response);
      if (response.StatusCode == 200) {
          if (response.Result === true || this.state.isFblogin === false) {
              AsyncStorage.setItem('@isLogin', 'true');
              AsyncStorage.setItem(globals.LOGINUSERID, JSON.stringify(response.Data.UserID));
              AsyncStorage.setItem(globals.LOGINACCESSTOKEN, JSON.stringify(response.Data.AccessToken));
  
  
              AsyncStorage.multiGet([globals.LOGINACCESSTOKEN, globals.LOGINUSERID]).then(token => {
                globals.tokenValue = token[0][1];
                globals.userID = token[1][1];
                let proData = {};
                API.getMyProfileDetails(this.getMyProfileResponseData, proData, true);
              });
  
              Keyboard.dismiss();

            // this.navigateToScreen('DrawerNavigator');
          } else {
            console.log(globals.appName, response.Message);
          }
          this.setState({ loading: false });
       
      
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => { },
  };


  /**
   * API getMyProfileResponseData 
   */
  getMyProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'getMyProfileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        AsyncStorage.setItem(globals.LOGINUSERNAME, response.Data.MemberInfo.FullName);
        AsyncStorage.setItem(globals.LOGINUSEREMAIL, response.Data.MemberInfo.EmailID);
        AsyncStorage.setItem(globals.LOGINUSERPROFILEIMAGE, response.Data.MemberProfile.ProfilePicturePath);
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
      this.navigateToScreen('DrawerNavigator');
    },
    error: err => {
      this.navigateToScreen('DrawerNavigator');
      console.log(
        TAG,
        'getMyProfileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      // Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => {
    },
  };

  navigateToScreen = routeName => {
    const { navigation } = this.props;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName })],
    });

    navigation.dispatch(resetAction);
  };

  /**
   * fo login with facebook
   * @function performFBLogin
   */
  performFBLogin() {
    console.log(TAG, 'performFBLogin');
    LoginManager.logOut();
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function(result) {
        console.log(TAG, `facebook login result : ${JSON.stringify(result)}`);
        if (result.isCancelled) {
          console.log('Login cancelled');
          _this.setState({isFblogin: result.isCancelled})
          AsyncStorage.setItem(globals.FB_LOGINKEY, JSON.stringify(result.isCancelled));
        } else {
          console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
          _this.setState({isFblogin: result.isCancelled})
          AsyncStorage.setItem(globals.FB_LOGINKEY, JSON.stringify(result.isCancelled));

          AccessToken.getCurrentAccessToken()
            .then(user => {
              console.log(TAG, `user info : ${JSON.stringify(user)}`);
              return user;
            })
            .then(user => {
              const responseInfoCallback = (error, result) => {
                if (error) {
                  console.log(error);
                  alert(`Error fetching data: ${error.toString()}`);
                } else {
                  console.log(TAG, `user result : ${JSON.stringify(result)}`);
                  if (result.hasOwnProperty('email')) {
                    _this.setState({ userEmail: result.email });
                  } else {
                    console.log(TAG, 'email not exist in info');
                  }

                  if (result.hasOwnProperty('first_name')) {
                    _this.setState({ userFirstName: result.first_name });
                  } else {
                    console.log(TAG, 'first_name not exist in info');
                  }

                  if (result.hasOwnProperty('last_name')) {
                    _this.setState({ userLastName: result.last_name });
                  } else {
                    console.log(TAG, 'last_name not exist in info');
                  }

                  if (result.hasOwnProperty('name')) {
                    _this.setState({ userName: result.name });
                  } else {
                    console.log(TAG, 'name not exist in info');
                  }

                  if (result.hasOwnProperty('id')) {
                    _this.setState({ userFbid: result.id });
                  } else {
                    console.log(TAG, 'id not exist in info');
                  }
                }
              };

              const infoRequest = new GraphRequest(
                '/me',
                {
                  accessToken: user.accessToken,
                  parameters: {
                    fields: {
                      string: 'email,name,first_name,last_name',
                    },
                  },
                },
                responseInfoCallback
              );

              // Start the graph request.
              new GraphRequestManager().addRequest(infoRequest).start();
            });
        }
      },
      function(error) {
        console.log(`Login fail with error: ${error}`);
      }
    );
  }

  /**
   * this method for term&condition open in default browser
   */
  handleClick = () => {
    Linking.canOpenURL(globals.termsCondUrl).then(supported => {
      if (supported) {
        Linking.openURL(globals.termsCondUrl);
      } else {
        console.log(`Don't know how to open URI:`);
      }
    });
  };

  /**
   * this method for term&privacyPolicy open in default browser
   */
  handleClick2 = () => {
    Linking.canOpenURL(globals.privacyUrl).then(supported => {
      if (supported) {
        Linking.openURL(globals.privacyUrl);
      } else {
        console.log(`Don't know how to open URI:`);
      }
    });
  };

   /**
   * this method for closeWebView
   */
  static closeWebView() {
    if (_this.state.isterms === true) {
      _this.setState({ isterms: false })
    } else {
      _this.setState({ isPrivacy: false })
    }
  }

  render() {
    const { loading, userEmail, userFirstName, userLastName, isterms, isPrivacy, isFblogin } = this.state;
    return (
      <View style={{flex: 1}}>
      {(isterms === true || isPrivacy === true) ?
          <WebViewComponent
            url={(isterms === true) ? globals.termsCondUrl : (isPrivacy === true) ? globals.privacyUrl : null}
            isOpenRegister={(isterms === true) ? isterms : (isPrivacy === true) ? isPrivacy : null}
            isterms={isterms}
            isPrivacy={isPrivacy}
          />
        :
      <ImageBackground
        source={images.loginAndRegisterScreen.blueBackground}
        style={styles.imageBackgroundStyle}
      >
        <CustomHeader onPress={() => this.props.navigation.goBack()} />
        <ScrollView
          bounces={false}
          style={styles.login_ScrollViewStyle}
          showsVerticalScrollIndicator={false}
        >
          <SafeAreaView style={styles.login_safeAreaStyle}>
            <Text style={styles.reg_createTextStyle}>
              {globals.BTNTEXT.REGISTERSCREEN.REG_CREATE_ACC}
            </Text>
            <View style={styles.regtr_emailUserNameTextViewStyle}>
              <TextInput
                style={styles.reg_textInputStyle}
                placeholder={globals.PLACEHOLDERTXT.REGISTER.FIRST_NAME}
                placeholderTextColor={colors.white}
                onChangeText={text => this.setState({ userFirstName: text })}
                returnKeyType="next"
                value={userFirstName}
                onSubmitEditing={() => this.userlastNameRef.focus()}
                autoCapitalize="none"
              />
            </View>
            <View style={styles.regtr_emailUserNameTextViewStyle}>
              <TextInput
                style={styles.reg_textInputStyle}
                placeholder={globals.PLACEHOLDERTXT.REGISTER.LAST_NAME}
                placeholderTextColor={colors.white}
                onChangeText={text => this.setState({ userLastName: text })}
                returnKeyType="next"
                value={userLastName}
                ref={userlastNameRef => (this.userlastNameRef = userlastNameRef)}
                onSubmitEditing={() => this.userEmailRef.focus()}
                autoCapitalize="none"
              />
            </View>
            <View style={styles.regtr_emailUserNameTextViewStyle}>
              <TextInput
                style={styles.reg_textInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.EMAIL_ID}
                placeholderTextColor={colors.white}
                onChangeText={text => this.setState({ userEmail: text })}
                returnKeyType="next"
                value={userEmail}
                ref={userEmailRef => (this.userEmailRef = userEmailRef)}
                onSubmitEditing={() => this.passwordRef.focus()}
                autoCapitalize="none"
              />
            </View>
                {(isFblogin === true) ?
                  <View style={styles.regtr_emailUserNameTextViewStyle}>
                    <TextInput
                      style={styles.reg_textInputStyle}
                      placeholder={globals.PLACEHOLDERTXT.LOGIN.PASSWORD}
                      placeholderTextColor={colors.white}
                      onChangeText={text => this.setState({ userPassword: text })}
                      returnKeyType="done"
                      secureTextEntry
                      ref={passwordRef => (this.passwordRef = passwordRef)}
                      autoCapitalize="none"
                    />
                  </View>
                  :
                  null
                }
            <TouchableOpacity
              style={styles.touchableStyle}
              onPress={() => this.userRegisterValidation()}
            >
              <CustomButton
                text={globals.PLACEHOLDERTXT.REGISTER.SIGN_UP}
                backgroundColor={colors.white}
                color={colors.warmBlue}
                loadingStatus={loading}
              />
            </TouchableOpacity>
            <Text style={styles.login_orTextstyle}>{globals.BTNTEXT.LOGINSCREEN.LOGIN_OR}</Text>
            <TouchableOpacity onPress={() => this.performFBLogin()}>
              <Image
                source={images.loginAndRegisterScreen.fbIcon}
                style={styles.login_fbIconStyle}
              />
            </TouchableOpacity>
            <Text style={styles.login_byCreatingTextStyle}>
              {globals.BTNTEXT.LOGINSCREEN.LOGIN_BY_CREATING_TEXT}
              <TouchableWithoutFeedback onPress={() => this.setState({isterms: true})}>
                <Text style={styles.login_ConditionTextStyle}>
                  {' '}
                  {globals.BTNTEXT.LOGINSCREEN.LOGIN_CONDITIONS}
                </Text>
              </TouchableWithoutFeedback>
              <Text style={styles.login_ConditionTextStyle}>
                {' '}
                {globals.BTNTEXT.LOGINSCREEN.LOGIN_SLASHLINE}
              </Text>
              <TouchableWithoutFeedback onPress={() => this.setState({isPrivacy: true})}>
                <Text style={styles.login_ConditionTextStyle}>
                  {' '}
                  {globals.BTNTEXT.LOGINSCREEN.LOGIN_PRIVACY}
                </Text>
              </TouchableWithoutFeedback>
            </Text>
          </SafeAreaView>
        </ScrollView>
      </ImageBackground>
      }
      </View>
    );
  }
}

export default RegisterScreen;
