/* eslint-disable eqeqeq */
/* eslint-disable import/named */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
/* eslint-disable react/sort-comp */
/* eslint-disable no-console */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint-disable no-lone-blocks */
/* eslint-disable class-methods-use-this */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable prettier/prettier */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, SafeAreaView, TextInput, TouchableOpacity, Alert, Keyboard, ScrollView, ImageBackground } from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { getshowLoader } from "../../../redux/acrions/showLoader";
import styles from './style';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import CustomButton from '../../../components/CustomButton';
import Validation from '../../../utils/validation';
import { API } from '../../../utils/api';
import * as images from '../../../assets/images/map';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();
const TAG = "ForgotPasswordScreen ::="
class forgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      loading: false,
    };
  }

  /**
   * method for valid email
   */
  ValidationForEmail() {
    const { email } = this.state;
    if (Validation.textInputCheck(email)) {
      if (Validation.isValidEmail(email)) {
        const data = {
          Email: email,
        };
        if (globals.isInternetConnected === true) {
          API.forgotPasscode(this.getforgotpassResponseData, data, false);
          this.setState({ loading: true });
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
          this.setState({ loading: false });
        }
      }
      else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_VALIDEMAIL)
        this.setState({ loading: false });
      }
    }
    else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_REGEMAIL)
      this.setState({ loading: false });
    }
  }

  /**
   * API getforgotpass response
   */
  getforgotpassResponseData = {
    success: response => {
      if (response.StatusCode == 200) {
        console.log(TAG, 'ForgotPasswordAPI response==', response);
        if (response.Result === true) {
          Keyboard.dismiss();
          Alert.alert(globals.appName, response.Message, [
            { text: 'OK' },
          ]),
            this.setState({email:'', loading: false });
        }
        else {
          Alert.alert(globals.appName, response.Message, [
            { text: 'OK' },
          ]),
            this.setState({email:'', loading: false });
        }
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      console.log(TAG, `ForgotPasswordAPI responseERROR== ${JSON.stringify(err.Message)}`);
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false ,email:''});
    },
    complete: () => { },
  };

  render() {
    const { email, loading } = this.state;

    return (
      <ImageBackground
        source={images.loginAndRegisterScreen.blueBackground}
        style={styles.imageBackgroundStyle}
      >
        <SafeAreaView style={styles.safeViewStyle}>
          <ScrollView bounces={false} style={styles.scrollViewStyle}>
            <View style={styles.container}>
              <Text style={styles.forgotpassStyle}>{globals.MESSAGE.FORGOTSCREEN.FRGT_TITLE}</Text>
              <Text style={[styles.forgotpassTextStyle, { marginTop: globals.screenHeight * 0.03 }]}>
                {globals.MESSAGE.FORGOTSCREEN.FRGT_REG_EMAIL_ADD}
              </Text>
              <Text style={styles.forgotpassTextStyle}>{globals.MESSAGE.FORGOTSCREEN.FRGT_RECEIVE_PASSWORD_RESET_LINK}</Text>
              <View style={styles.innerViewStyle}>
                <TextInput
                  value={email}
                  autoCapitalize="none"
                  onFocus={false}
                  blurOnSubmit={false}
                  returnKeyType="done"
                  onChangeText={text => this.setState({ email: text })}
                  placeholderTextColor={colors.white}
                  placeholder={globals.PLACEHOLDERTXT.LOGIN.ENTER_EMAIL}
                  style={styles.textInputStyle} />
              </View>
              <TouchableOpacity style={styles.touchableStyle} onPress={() => this.ValidationForEmail()} >
                <CustomButton
                  text={globals.BTNTEXT.LOGINSCREEN.FORGOTPASSLOGINBTNTEXT}
                  color={colors.bgColor}
                  backgroundColor={colors.white}
                  loadingStatus={loading} />
              </TouchableOpacity>
              <View style={styles.loginTextViewStyle}>
                <Text style={styles.loginTextStyle}>{globals.MESSAGE.FORGOTSCREEN.FRGT_DO_YOU_HAVE_ACC}</Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("LoginScreen")}>
                  <Text style={[styles.loginTextStyle, { marginLeft: 6, color: colors.darkSkyBlue, fontWeight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 'bold' : '600' }]}>Log In</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch => (bindActionCreators({
  getshowLoader,
}, dispatch));

export default connect(mapStateToProps, mapDispatchToProps)(forgotPassword);

