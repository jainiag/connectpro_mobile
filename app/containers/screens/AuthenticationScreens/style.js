import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  // ============: global :========
  imageBackgroundStyle: {
    width: '100%',
    height: '100%',
  },
  scrollViewStyle: {
    flex: 1,
  },

  // ===========: forget Password :===========

  safeViewStyle: {
    flex: 1,
  },
  container: {
    marginTop: globals.screenHeight * 0.19,
    width: globals.screenWidth,
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgotpassStyle: {
    alignSelf: 'center',
    fontSize:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 36 : globals.font_22,
    color: colors.white,
  },
  forgotpassTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_14,
    color: colors.white,
  },
  innerViewStyle: {
    width: '85%',
  },
  textInputStyle: {
    paddingBottom: globals.screenHeight * 0.015,
    marginTop: globals.screenHeight * 0.09,
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_14,
  },
  touchableStyle: {
    marginTop: globals.screenHeight * 0.09,
  },
  buttonViewStyle: {
    borderWidth: 1,
    borderColor: colors.white,
    backgroundColor: colors.white,
    borderRadius: globals.screenWidth * 0.06,
    paddingVertical: globals.screenWidth * 0.019,
    paddingHorizontal: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.85,
    height: globals.screenHeight * 0.0685,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnTextStyle: {
    alignSelf: 'center',
    fontSize: globals.font_16,
    color: colors.bgColor,
    fontWeight: 'bold',
  },
  loginTextViewStyle: {
    marginTop: globals.screenHeight * 0.09,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  loginTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 33 : globals.font_16,
    color: colors.white,
  },

  //= ==================: login screen :=============

  login_ScrollViewStyle: {
    flex: 1,
  },
  login_safeAreaStyle: {
    flex: 1,
    alignItems: 'center',
    marginBottom: 20,
  },
  login_logoViewContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  login_logoIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.35 : globals.screenWidth * 0.5,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.35 : globals.screenWidth * 0.5,
  },
  login_emailUserNameTextViewStyle: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    marginHorizontal: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 40 : 30,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.05,
  },
  login_userEmailTextInputStyle: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.white,
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS === 'ios') ? 22 : 25 : globals.font_14,
    marginTop: globals.screenHeight * 0.03,
    paddingBottom: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 15 : 9,
  },
  login_passwordTextViewStyle: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    alignItems: 'center',
    marginHorizontal: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 40 : 30,
    marginTop: 20,
  },
  login_questionMarkStyle: {
    marginRight: globals.screenWidth * 0.02,
    backgroundColor: colors.questionIconBgColor,
    borderRadius: (globals.screenHeight * 0.03) / 2,
    marginTop: Platform.OS === 'android' ? 20 : 10,
  },
  login_touchAbleOpacityContainer: {
    borderWidth: 1,
    borderColor: colors.white,
    borderRadius: globals.screenWidth * 0.06,
    marginTop: globals.screenHeight * 0.09,
    backgroundColor: colors.white,
  },
  login_touchableViewStyle: {
    borderWidth: 1,
    borderColor: colors.white,
    borderRadius: globals.screenWidth * 0.06,
    paddingVertical: globals.screenWidth * 0.019,
    paddingHorizontal: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.85,
    height: globals.screenHeight * 0.0685,
    alignItems: 'center',
    justifyContent: 'center',
  },
  login_textLoginStyle: {
    textAlign: 'center',
    fontSize: globals.font_16,
    color: colors.bgColor,
  },
  login_orTextstyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS === 'ios') ? 23 : 27 : globals.font_14,
    color: colors.white,
    marginTop: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 25 : 20,
  },
  login_fbIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.09 : globals.screenWidth * 0.12,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.09 : globals.screenWidth * 0.12,
    marginTop: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS === 'android') ? 30 : 25 : 20,
    marginBottom:((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS === 'android') ? 30 : 25 : 20,
  },
  login_withSignUpTextViewContainer: {
    flexDirection: 'row',
    marginBottom: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 30 : 20,
  },
  login_dontTextStyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 27 : 30 : globals.font_14,
    color: colors.white,
  },
  login_SignUpTextStyle: {
    color: colors.darkSkyBlue,
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 27 : 30 : globals.font_14,
    fontWeight: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 'bold' : (Platform.OS == 'android') ? 'bold' : '600',
  },
  login_termsPolicyTextViewContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
  },
  login_byCreatingTextStyle: {
    textAlign: 'center',
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios')  ? 24 : 27 : globals.font_13,
    color: colors.white,
    marginHorizontal: 32,
    marginBottom: 10,
    marginTop: 10,
  },
  login_ConditionPrivacyTextStyle: {
    letterSpacing: 0,
    textAlign: 'center',
    color: 'white',
    marginBottom: 10,
  },
  login_ConditionTextStyle: {
    color: colors.darkSkyBlue,
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios')  ? 24 : 27 : globals.font_13,
    textAlign: 'center',
    marginBottom: 10,
    marginTop: 10,
    fontWeight:  ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? '600' : '500',
  },
  login_PrivacyTextStyle: {
    color: colors.darkSkyBlue,
    fontSize: globals.font_13,
    letterSpacing: 0,
    lineHeight: 18,
    textAlign: 'center',
  },
  login_AlertViewContainer: {
    height: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenHeight * 0.17 : (DeviceInfo.isTablet()) ? globals.screenHeight * 0.14 : (globals.iPhoneX) ? globals.screenHeight * 0.15 : globals.screenHeight * 0.165,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.8 : globals.screenWidth * 0.85,
    backgroundColor: colors.white,
    alignItems: 'center',
    marginTop: 20,
  },
  login_alertIconViewContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  login_alertIconViewStyle: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  login_alertCloseIconStyle: {
    marginLeft: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.4 :  globals.iPhoneX ? globals.screenWidth * 0.26 : globals.screenWidth * 0.24,
  },
  login_emailVerfiedTextStyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 25 : globals.font_16,
    color: colors.black,
    textAlign: 'center',
    marginTop: 10,
  },
  login_clickHereTextStyle: {
    fontSize: globals.font_14,
    color: colors.darkSkyBlue,
  },
  login_clickVerifyTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  login_IconStyle: {
    marginTop: Platform.OS === 'android' ? 18 : 8,
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.getHorizontalBaseValue(37) : globals.screenWidth * 0.06,
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.getHorizontalBaseValue(37) : globals.screenHeight * 0.03,
  },
  login_infoIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? globals.getHorizontalBaseValue(45) : globals.getHorizontalBaseValue(50) : globals.screenWidth * 0.08,
    width: (iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())  ? (Platform.OS == 'ios') ? globals.getHorizontalBaseValue(45) : globals.getHorizontalBaseValue(50): globals.screenWidth * 0.08,
    marginTop: Platform.OS === 'android' ? 20 : 8,
  },

  login_forgoticonStyle: {
    height: globals.screenWidth * 0.08,
    width: globals.screenWidth * 0.08,
    marginTop: Platform.OS === 'android' ? 20 : 8,
  },
  login_alertIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenHeight * 0.04 : globals.screenHeight * 0.04, 
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenHeight * 0.04 : globals.screenHeight * 0.04,
    marginTop: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 20 : 17, 
  },
  login_AlertCloseBtnStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.045 : globals.screenWidth * 0.064, 
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.045 : globals.screenWidth * 0.064,
    marginRight: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 15 : 10, 
    marginTop: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 15 : 10, 
  },

  // ==============: register style :=============

  reg_textInputStyle: {
    flex: 1,
    color: colors.white,
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS === 'ios') ? 22 : 25 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.015,
    marginTop:
      Platform.OS === 'android' ? globals.screenHeight * 0.04 : globals.screenHeight * 0.05,
  },
  regtr_emailUserNameTextViewStyle: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    marginHorizontal: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 40 : 30,
    alignItems: 'center',
  },
  reg_createTextStyle: {
    fontSize: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS === 'ios') ? 30 : 37 : globals.font_20, 
    color: colors.white, 
    marginTop: 25, 
    marginBottom: 15,
  },

  // =================== Reset Password ====================//

  resetPass_changePasssafeViewStyle: {
    flex: 1,
    backgroundColor: colors.white,
  },
  resetPass_mainContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.085,
  },
  resetPass_subContainerStyle: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    alignItems: 'center',
    marginHorizontal: globals.screenWidth * 0.07,
    marginTop: globals.screenHeight * 0.07,
    paddingBottom:
      Platform.OS === 'android' ? globals.screenHeight * 0.001 : globals.screenHeight * 0.015,
  },
  resetPass_currentPassTextInputStyle: {
    width: '85%',
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_16,
  },
  resetPass_touchableViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: globals.screenHeight * 0.09,
  },

  resetPass_lockIconStyle: {
    left: 0,
    position: 'relative',
  },
  backgroundStyle: {
    height: '100%',
    width: '100%',
  },
  resetPass_titleStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 34 : globals.font_24,
  },
  resetPAss_titleView: {
    marginTop: globals.screenHeight * 0.05,
    alignItems: 'center',
    justifyContent: 'center',
  },
  resetpass_backViewstyle: {
    marginTop: globals.screenHeight * 0.09,
    alignItems: 'center',
    justifyContent: 'center',
  },
  resetpass_backIconStyles: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.05
        : globals.screenHeight * 0.04,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.05
        : globals.screenHeight * 0.04,
  },
});
