/* eslint-disable class-methods-use-this */
/* eslint-disable no-sequences */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-alert */
/* eslint-disable no-shadow */
/* eslint-disable func-names */
/* eslint-disable no-console */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable no-return-assign */
/* eslint-disable react/no-unused-state */
/* eslint-disable eqeqeq */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable react/sort-comp */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  SafeAreaView,
  ScrollView,
  Alert,
  Keyboard,
  ImageBackground,
  TouchableWithoutFeedback,
  Linking,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from 'react-native-fbsdk';
import { NavigationActions, StackActions } from 'react-navigation';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import styles from './style';
import Validation from '../../../utils/validation';
import CustomButton from '../../../components/CustomButton/index';
import { API } from '../../../utils/api';
import * as images from '../../../assets/images/map';
import WebViewComponent from '../../../components/WebView/index';

const TAG = '==:== LoginScreen :';
let _this;

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      userEmail: '',
      userPassword: '',
      verification: false,
      loading: false,
      loginResult: true,
      isterms: false,
      isPrivacy: false,
      isFblogin: true,
      userName: '',
      userFirstName: '',
      userLastName: '',
      userFbId: '',
    };
  }

  /**
   * Validation method for user email and password
   */
  validateFields() {
    console.log("validateFields method")
    const { userEmail, userPassword, userFbId, userFirstName, userLastName, userName, isFblogin } = this.state;
    if (Validation.textInputCheck(userEmail)) {
      console.log("Validation.textInputCheck")
      if (Validation.isValidEmail(userEmail)) {
        console.log("Validation.isValidEmail")
        if (Validation.textInputCheck(userPassword) || (isFblogin === false)) {
          console.log("Validation.isFblogin",isFblogin)

          if (isFblogin === true) {
            const data = {
              Email: userEmail,
              Password: userPassword,
              RememberMe: true,
              Message: "string",
              ReturnUrl: "string",
              PasswordChangeRequired: false,
              DeviceToken: "string",
              CommunityLogo: "string"
            };
            if (globals.isInternetConnected === true) {
              this.setState({ loading: true });
              API.loginPasscode(this.userLoginResponseData, data, true);
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
              this.setState({ loading: false });
            }
          } else {
            const data = {
              "email": userEmail,
              "id": userFbId,
              "first_name": userFirstName,
              "last_name": userLastName,
              "name": userName
            };
            if (globals.isInternetConnected === true) {
              this.setState({ loading: true });
              console.log("In isfblogin false",isFblogin)
              console.log("data - " ,data)
              API.externalLoginCallbackAPI(this.externalLoginCallbackapiResponse, data, true);
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
              this.setState({ loading: false });
            }
          }
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_PASSWORD);
        }
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_VALIDEMAIL);
        this.setState({ loading: false });
      }
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTEREMAIL);
    }
  }

  navigateToScreen = routeName => {
    const { navigation } = this.props;
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName })],
    });

    navigation.dispatch(resetAction);
  };

  /**
   * API userLogin response
   */
  userLoginResponseData = {
    success: response => {
      console.log(TAG, 'LoginAPI response==', response);
      if (response.StatusCode == 200) {

        this.setState({ loginResult: response.Data.Result });
        if (response.Data.Result === true) {
          globals.PasswordChangeRequired = response.Data.PasswordChangeRequired;
          if (response.Data.PasswordChangeRequired == true) {
            AsyncStorage.setItem('@isLogin', 'true');
            AsyncStorage.setItem(globals.LOGINUSERID, JSON.stringify(response.Data.UserId));
            AsyncStorage.setItem(globals.LOGINACCESSTOKEN, JSON.stringify(response.Data.AccessToken));


            AsyncStorage.multiGet([globals.LOGINACCESSTOKEN, globals.LOGINUSERID]).then(token => {
              globals.tokenValue = token[0][1];
              globals.userID = token[1][1];

              API.getMyProfileDetails(this.getMyProfileResponseData, true);
            });
            Alert.alert(
              globals.appName,
              response.Data.Message,
              [{ text: 'OK', onPress: () => this.props.navigation.navigate('CHANGE_PASSWORD_SCREEN') }],
              { cancelable: false }
            );
          } else {
            AsyncStorage.setItem('@isLogin', 'true');
            AsyncStorage.setItem(globals.LOGINUSERID, JSON.stringify(response.Data.UserId));
            AsyncStorage.setItem(globals.LOGINACCESSTOKEN, JSON.stringify(response.Data.AccessToken));


            AsyncStorage.multiGet([globals.LOGINACCESSTOKEN, globals.LOGINUSERID]).then(token => {
              globals.tokenValue = token[0][1];
              globals.userID = token[1][1];
              API.getMyProfileDetails(this.getMyProfileResponseData, true);
            });

            Keyboard.dismiss();
          }
        } else {
          // API.getMyProfileDetails(this.getMyProfileResponseData, true);
          console.log(globals.appName, response.Message);
        }
        this.setState({ loading: false });
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => { },
  };


  /**
   * API externalLoginCallbackAPI response
   */
  externalLoginCallbackapiResponse = {
    success: response => {
      console.log(TAG, 'externalLoginCallbackapiResponse', response);
      if (response.StatusCode == 200) {
        this.setState({ loginResult: response.Result });
        const ID = (response.Data == null) ? 0 : response.Data.UserID
        const Token = (response.Data == null) ? 0 : response.Data.AccessToken
        if (response.Data !== null) {
          AsyncStorage.setItem('@isLogin', 'true');
          AsyncStorage.setItem(globals.LOGINUSERID, JSON.stringify(ID));
          AsyncStorage.setItem(globals.LOGINACCESSTOKEN, JSON.stringify(Token));

          AsyncStorage.multiGet([globals.LOGINACCESSTOKEN, globals.LOGINUSERID]).then(token => {
            globals.tokenValue = token[0][1];
            globals.userID = token[1][1];
            API.getMyProfileDetails(this.getMyProfileResponseData, true);
          });
          Keyboard.dismiss();

          // this.navigateToScreen('DrawerNavigator');
        } else {
          Alert.alert(globals.appName, "Please check your facebook email id");
        }
        this.setState({ loading: false });

      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => { },
  };

  /**
  * API getMyProfileResponseData 
  */
  getMyProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'getMyProfileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        AsyncStorage.setItem(globals.LOGINUSERNAME, response.Data.MemberInfo.FullName);
        AsyncStorage.setItem(globals.LOGINUSEREMAIL, response.Data.MemberInfo.EmailID);
        AsyncStorage.setItem(globals.LOGINUSERPROFILEIMAGE, response.Data.MemberProfile.ProfilePicturePath);
        AsyncStorage.multiGet([globals.LOGINUSERNAME, globals.LOGINUSEREMAIL, globals.LOGINUSERPROFILEIMAGE, globals.FB_LOGINKEY]).then(profileInfo => {
          this.setState({
            userEmail: profileInfo[1][1],
            userName: profileInfo[0][1],
            isFblogin: JSON.parse(profileInfo[3][1]),
          }, () => {
            globals.loginFullName = profileInfo[0][1];
            globals.loginEmail = profileInfo[1][1];
            globals.loginProfileImg = profileInfo[2][1];
            globals.fbLoginKey = JSON.parse(profileInfo[3][1])
          });
        });
        this.navigateToScreen('DrawerNavigator');

      } else {
        this.setState({ loading: false });
      }
    },
    error: err => {
      console.log(
        TAG,
        'getMyProfileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ loading: false });
    },
    complete: () => {
    },
  };


  /**
   * fo login with facebook
   * @function performFBLogin
   */
  performFBLogin() {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logOut();
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function (result) {
        console.log(TAG, `facebook login result : ${JSON.stringify(result)}`);
        if (result.isCancelled) {
          console.log("result.cancelable ::", JSON.stringify(result.isCancelled))
          _this.setState({ isFblogin: result.isCancelled })
          AsyncStorage.setItem(globals.FB_LOGINKEY, JSON.stringify(result.isCancelled));
        } else {
          console.log(`Login success with permissions: ${result.grantedPermissions.toString()}`);
          _this.setState({ isFblogin: result.isCancelled })
          console.log("result.cancelable ::", JSON.stringify(result.isCancelled))
          AsyncStorage.setItem(globals.FB_LOGINKEY, JSON.stringify(result.isCancelled));
          AccessToken.getCurrentAccessToken()
            .then(user => {
              console.log(TAG, `user info : ${JSON.stringify(user)}`);
              return user;
            })
            .then(user => {
              const responseInfoCallback = (error, result) => {
                if (error) {
                  console.log(error);
                  alert(`Error fetching data: ${error.toString()}`);
                } else {
                  console.log(TAG, `user result : ${JSON.stringify(result)}`);
                  if (result.hasOwnProperty('email')) {
                    _this.setState({ userEmail: result.email });
                  } else {
                    console.log(TAG, 'email not exist in info');
                  }

                  if (result.hasOwnProperty('first_name')) {
                    _this.setState({ userFirstName: result.first_name });
                  } else {
                    console.log(TAG, 'first_name not exist in info');
                  }

                  if (result.hasOwnProperty('last_name')) {
                    _this.setState({ userLastName: result.last_name });
                  } else {
                    console.log(TAG, 'last_name not exist in info');
                  }

                  if (result.hasOwnProperty('name')) {
                    _this.setState({ userName: result.name });
                  } else {
                    console.log(TAG, 'name not exist in info');
                  }

                  if (result.hasOwnProperty('id')) {
                    _this.setState({ userFbId: result.id });
                  } else {
                    console.log(TAG, 'id not exist in info');
                  }
                }
              };

              const infoRequest = new GraphRequest(
                '/me',
                {
                  accessToken: user.accessToken,
                  parameters: {
                    fields: {
                      string: 'email,name,first_name,last_name',
                    },
                  },
                },
                responseInfoCallback
              );

              // Start the graph request.
              new GraphRequestManager().addRequest(infoRequest).start();
            });
        }
      },
      function (error) {
        console.log(`Login fail with error: ${error}`);
      }
    );
  }

  /**
   * textInputs clear when user logout
   */
  static clearTextFields() {
    try {
      _this.setState({ userEmail: '', userPassword: '' });
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * this method for term&condition open in default browser
   */
  handleClick = () => {
    Linking.canOpenURL(globals.termsCondUrl).then(supported => {
      if (supported) {
        Linking.openURL(globals.termsCondUrl);
      } else {
        console.log(`Don't know how to open URI:`);
      }
    });
  };

  /**
   * this method for privacyPolicy open in default browser
   */
  handleClick2 = () => {
    Linking.canOpenURL(globals.privacyUrl).then(supported => {
      if (supported) {
        Linking.openURL(globals.privacyUrl);
      } else {
        console.log(`Don't know how to open URI:`);
      }
    });
  };

  // renderWebView(){
  //   console.log(this.state.isOpenWeb, TAG);
  //   return(
  //     <View style={{flex: 1}}>
  //       {(this.state.isOpenWeb === true) ?
  //         <WebViewComponent url={globals.termsCondUrl} />
  //         :
  //         null
  //       }
  //     </View>
  //   )
  // }


  /**
   * this method for closeWebView
   */
  static closeWebView() {
    if (_this.state.isterms === true) {
      _this.setState({ isterms: false })
    } else {
      _this.setState({ isPrivacy: false })
    }
  }

  pressClickHere(){
    API.resendClickHereReq(this.resendEmailResponseData, this.state.userEmail, true);
  }

  /**
   * API resendEmailResponseData response
   */
  resendEmailResponseData = {
    success: response => {
      console.log(TAG, 'resendEmailResponseData response==', response);
      if (response.StatusCode == 200) {

        if (response.Result === true) {
          Alert.alert(globals.appName, response.Message, [
            { text: 'OK', onPress: () => this.setState({loginResult : true}) }
          ]);
          // this.navigateToScreen('DrawerNavigator');
        } else {
          Alert.alert(globals.appName, response.Message);
          console.log(globals.appName, response.Message);
        }
        this.setState({ loading: false });


      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => { },
  };

  render() {
    const { loading, userEmail, userPassword, isterms, isPrivacy, isFblogin } = this.state;
    return (
      <View style={{ flex: 1 }}>
        {(isterms === true || isPrivacy === true) ?
          <WebViewComponent
            url={(isterms === true) ? globals.termsCondUrl : (isPrivacy === true) ? globals.privacyUrl : null}
            isOpenLogin={(isterms === true) ? isterms : (isPrivacy === true) ? isPrivacy : null}
            isterms={isterms}
            isPrivacy={isPrivacy}
          />
          :
          <ImageBackground
            source={images.loginAndRegisterScreen.blueBackground}
            style={styles.imageBackgroundStyle}
          >
            <ScrollView
              bounces={false}
              style={styles.login_ScrollViewStyle}
              showsVerticalScrollIndicator={false}
            >
              <SafeAreaView style={styles.login_safeAreaStyle}>
                {(this.state.loginResult === true) || (isFblogin === false) ? (
                  <View style={styles.login_logoViewContainer}>
                    <Image
                      source={images.loginAndRegisterScreen.logo}
                      style={styles.login_logoIconStyle}
                    />
                  </View>
                ) : (
                    <View style={styles.login_AlertViewContainer}>
                      <View style={styles.login_alertIconViewContainer}>
                        <View style={styles.login_alertIconViewStyle}>
                          <Image
                            source={images.loginAndRegisterScreen.alertIcon}
                            style={styles.login_alertIconStyle}
                            resizeMode="contain"
                          />
                        </View>
                        <TouchableOpacity onPress={() => this.setState({ loginResult: true })}>
                          <Image
                            source={images.loginAndRegisterScreen.closeBtn}
                            style={styles.login_AlertCloseBtnStyle}
                            resizeMode="contain"
                          />
                        </TouchableOpacity>
                      </View>
                      <Text style={styles.login_emailVerfiedTextStyle}>
                        {globals.MESSAGE.LOGINSTACK.EMAIL_VERIFY}
                    </Text>
                      {/* <Text style={styles.login_emailVerfiedTextStyle}>  {globals.MESSAGE.LOGINSTACK.EMAIL_VERIFY}
                        <TouchableWithoutFeedback onPress={()=>this.pressClickHere()}>
                          <Text style={[styles.login_emailVerfiedTextStyle, { color: colors.listSelectColor, textDecorationLine: 'underline' }]}> Click here </Text>
                        </TouchableWithoutFeedback>
                        <Text style={styles.login_emailVerfiedTextStyle}>to verify</Text>
                      </Text> */}

                    </View>
                  )}
                <View style={styles.login_emailUserNameTextViewStyle}>
                  <Image
                    source={images.loginAndRegisterScreen.userIcon}
                    style={styles.login_IconStyle}
                  />
                  <TextInput
                    style={styles.login_userEmailTextInputStyle}
                    placeholder={globals.PLACEHOLDERTXT.LOGIN.EMAIL_ID}
                    placeholderTextColor={colors.white}
                    onChangeText={text => this.setState({ userEmail: text })}
                    returnKeyType="next"
                    keyboardType={"email-address"}
                    value={userEmail}
                    onSubmitEditing={() => this.passwordRef.focus()}
                    autoCapitalize="none"
                  />
                </View>
                {(isFblogin === true) ?
                  <View style={styles.login_passwordTextViewStyle}>
                    <Image
                      source={images.loginAndRegisterScreen.password}
                      style={styles.login_IconStyle}
                    />
                    <TextInput
                      style={styles.login_userEmailTextInputStyle}
                      placeholder={globals.PLACEHOLDERTXT.LOGIN.PASSWORD}
                      placeholderTextColor={colors.white}
                      secureTextEntry
                      value={userPassword}
                      onChangeText={text => this.setState({ userPassword: text })}
                      autoCapitalize="none"
                      ref={passwordRef => (this.passwordRef = passwordRef)}
                      returnKeyType="done"
                    />
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('forgotPassword')}>
                      <Image
                        source={images.loginAndRegisterScreen.infoIcon}
                        style={styles.login_infoIconStyle}
                      />
                    </TouchableOpacity>
                  </View>
                  :
                  null
                }
                <TouchableOpacity style={styles.touchableStyle} onPress={() => this.validateFields()}>
                  <CustomButton
                    text={globals.PLACEHOLDERTXT.LOGIN.LOGIN}
                    backgroundColor={colors.white}
                    color={colors.warmBlue}
                    loadingStatus={loading}
                  />
                </TouchableOpacity>
                <Text style={styles.login_orTextstyle}>{globals.BTNTEXT.LOGINSCREEN.LOGIN_OR}</Text>
                <TouchableOpacity onPress={() => this.performFBLogin()}>
                  <Image
                    source={images.loginAndRegisterScreen.fbIcon}
                    style={styles.login_fbIconStyle}
                  />
                </TouchableOpacity>

                <View style={styles.login_withSignUpTextViewContainer}>
                  <Text style={styles.login_dontTextStyle}>
                    {globals.BTNTEXT.LOGINSCREEN.LOGIN_DONT_HAVE_ACC}{' '}
                  </Text>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('RegisterScreen')}>
                    <Text style={styles.login_SignUpTextStyle}>
                      {globals.BTNTEXT.LOGINSCREEN.LOGIN_SIGN_UP}
                    </Text>
                  </TouchableOpacity>
                </View>
                <Text style={styles.login_byCreatingTextStyle}>
                  {globals.BTNTEXT.LOGINSCREEN.LOGIN_BY_CREATING_TEXT}
                  <TouchableWithoutFeedback onPress={() => this.setState({ isterms: true })}>
                    <Text style={styles.login_ConditionTextStyle}>
                      {' '}
                      {globals.BTNTEXT.LOGINSCREEN.LOGIN_CONDITIONS}
                    </Text>
                  </TouchableWithoutFeedback>
                  <Text style={styles.login_ConditionTextStyle}>
                    {' '}
                    {globals.BTNTEXT.LOGINSCREEN.LOGIN_SLASHLINE}
                  </Text>
                  <TouchableWithoutFeedback onPress={() => this.setState({ isPrivacy: true })}>
                    <Text style={styles.login_ConditionTextStyle}>
                      {' '}
                      {globals.BTNTEXT.LOGINSCREEN.LOGIN_PRIVACY}
                    </Text>
                  </TouchableWithoutFeedback>
                </Text>
              </SafeAreaView>
            </ScrollView>
          </ImageBackground>
        }
      </View>
    );
  }
}

export default LoginScreen;
