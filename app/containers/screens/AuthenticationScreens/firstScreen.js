/* eslint-disable no-shadow */
/* eslint-disable no-useless-constructor */
/* eslint-disable no-underscore-dangle */
/* eslint-disable no-empty */
/* eslint-disable prefer-destructuring */
/* eslint-disable no-console */
/* eslint-disable react/sort-comp */
/* eslint-disable eqeqeq */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import { View, Alert } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import AsyncStorage from '@react-native-community/async-storage';
import * as globals from '../../../utils/globals';
import { API } from '../../../utils/api';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginScreen from './loginScreen';


const TAG = '==:== First Screen';

class FirstScreen extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { navigation } = this.props;
    SplashScreen.hide();
    AsyncStorage.multiGet([globals.LOGINACCESSTOKEN, globals.LOGINUSERID]).then(token => {
      globals.tokenValue = token[0][1];
      globals.userID = token[1][1];
      const data = {
        pageNumber: 0,
        pageSize: 0,
        EventNameSearch: '',
        EventCategorySearch: 0,
        EventLocationSearch: '',
        EventCriteria: '',
        FromDateSearch: '',
        ToDateSearch: '',
      };
      AsyncStorage.getItem(globals.LOGINACCESSTOKEN).then(token => {
        globals.tokenValue = token;
        if (globals.tokenValue != null) {
          this.props.showLoader();
          let proData = {};
          API.getMyProfileDetails(this.getMyProfileResponseData, proData, true);
          API.getMyEvents(this.getMYEventResponseData, data, true);
        } else {
          AsyncStorage.getItem('@isLogin').then(result => {
            if (result !== null) {
              this.props.navigation.navigate('App');
            } else {
              this.props.navigation.navigate('Auth');
            }
          });
        }
      });
    });

    // });
    // Linking.addEventListener('url', this.handleNavigation);
  }



  /**
   * Method called when session expire
   */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('Auth');
    loginScreen.clearTextFields();
  }


  /**
   * callback function of getMyProfileDetails
   */
  getMyProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'getMyProfileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        AsyncStorage.setItem(globals.LOGINUSERNAME, response.Data.MemberInfo.FullName);
        AsyncStorage.setItem(globals.LOGINUSEREMAIL, response.Data.MemberInfo.EmailID);
        AsyncStorage.setItem(globals.LOGINUSERPROFILEIMAGE, response.Data.MemberProfile.ProfilePicturePath);
      } else {
        this.setState({ loading: false });
        Alert.alert(globals.appName, response.Message);
      }
      this.props.hideLoader()
    },
    error: err => {
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.props.hideLoader();
      } 
      
      console.log(
        TAG,
        'getMyProfileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ loading: false });
    },
    complete: () => {
      this.props.hideLoader();
    },
  };


  /**
   * callback function of getMYEventResponseData
   */
  getMYEventResponseData = {
    success: response => {
      this.props.hideLoader();
      console.log(TAG, 'getMyEventData -> success : ', JSON.stringify(response));
      AsyncStorage.getItem('@isLogin').then(result => {
        console.log(TAG, `result : ${JSON.stringify(result)}`);
        if (result !== null) {
          this.props.navigation.navigate('App');
        } else {
          this.props.navigation.navigate('Auth');
        }
      });
    },
    error: err => {
      this.props.hideLoader();
      console.log(TAG, 'getMyEventDataErr -> success : ', JSON.stringify(err.Message));
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        AsyncStorage.getItem('@isLogin').then(result => {
          console.log(TAG, `result : ${JSON.stringify(result)}`);
          if (result !== null) {
            this.props.navigation.navigate('App');
          } else {
            this.props.navigation.navigate('Auth');
          }
        });
      }
    },
    complete: () => { },
  };

  // handleNavigation = event => {
  //   if (event.url != undefined) {
  //     const seprateUrlCode = event.url.split('&');
  //     const resetCodeFull = seprateUrlCode[1];
  //     const resetCodeSplit = resetCodeFull.split('=');
  //     const resetCode = resetCodeSplit[1];
  //     globals.RESETPASSWORDCODE = resetCode;
  //     this.props.navigation.navigate('resetPassword');
  //   }
  // };

  render() {
    return <View />;
  }
}

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FirstScreen);
