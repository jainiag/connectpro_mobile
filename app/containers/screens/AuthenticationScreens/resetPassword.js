/* eslint-disable no-console */
/* eslint-disable no-return-assign */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable eqeqeq */
/* eslint-disable no-alert */
/* eslint-disable no-undef */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import {
  View,
  ImageBackground,
  TextInput,
  Alert,
  Text,
  ScrollView,
  TouchableOpacity,
  Keyboard,
  Image,
} from 'react-native';
import styles from './style';
import * as colors from '../../../assets/styles/color';
import * as globals from '../../../utils/globals';
import CustomButton from '../../../components/CustomButton';
import Validation from '../../../utils/validation';
import { API } from '../../../utils/api';
import * as images from '../../../assets/images/map';

const TAG = 'ResetPasswordScreen ::=';
let _this;
export default class resetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      loading: false,
    };
  }

  componentDidMount() {}

  /**
   * method for valid passwords
   */
  passwordValidation() {
    const { email, password, confirmPassword } = this.state;
    if (Validation.textInputCheck(email)) {
      if (Validation.isValidEmail(email)) {
        if (Validation.textInputCheck(password)) {
          if (Validation.passwordLength(password, 7)) {
            if (Validation.isValidPassword(password)) {
              if (Validation.textInputCheck(confirmPassword)) {
                if (Validation.isValidPassword(password)) {
                  if (Validation.passwordLength(confirmPassword, 7)) {
                    if (Validation.isValidPassword(confirmPassword)) {
                      if (password === confirmPassword) {
                        const data = {
                          Email: email,
                          Password: password,
                          ConfirmPassword: confirmPassword,
                          Code: globals.RESETPASSWORDCODE,
                        };
                        if (globals.isInternetConnected === true) {
                          API.resetPasscode(this.getresetPassResponseData, data, true);
                          this.setState({ loading: true });
                        } else {
                          Alert.alert(
                            globals.appName,
                            globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET
                          );
                          this.setState({ loading: false });
                        }
                      } else {
                        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_RESETSAMEPASS);
                        this.setState({ loading: false });
                      }
                    } else {
                      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_PASSCONFIRM);
                      this.setState({ loading: false });
                    }
                  } else {
                    Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_NEED_7_CHARACTER);
                  }
                } else {
                  Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_PASSVALID);
                  this.setState({ loading: false });
                }
              } else {
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTERCONFIRMPASS);
                this.setState({ loading: false });
              }
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_PASSVALID);
            }
          } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_NEED_7_CHARACTER);
          }
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTERPASS);
          this.setState({ loading: false });
        }
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_VALIDEMAIL);
        this.setState({ loading: false });
      }
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTEREMAIL);
      this.setState({ loading: false });
    }
  }

  /**
   * API getresetPass response
   */
  getresetPassResponseData = {
    success: response => {
      if (response.StatusCode == 200) {
        console.log(TAG, 'ResetPasswordAPI response==', response);
        if (response.Result === true) {
          Keyboard.dismiss();
          Alert.alert(globals.appName, response.Message, [
            { text: 'OK', onPress: () => this.props.navigation.navigate('LoginScreen') },
          ]);
          this.setState({ loading: false });
        } else {
          Alert.alert(globals.appName, response.Message, [{ text: 'OK' }]),
            this.setState({ loading: false });
        }
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },
    error: err => {
      console.log(TAG, `ResetPasswordAPI responseERROR== ${JSON.stringify(err.message)}`);
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => {},
  };

  render() {
    const { email, password, confirmPassword, loading } = this.state;
    return (
      <ImageBackground
        source={images.loginAndRegisterScreen.blueBackground}
        style={styles.backgroundStyle}
      >
        <ScrollView style={styles.scrollViewStyle} bounces={false}>
          <View style={styles.resetPass_mainContainer}>
            <View style={styles.resetPAss_titleView}>
              <Text style={styles.resetPass_titleStyle}>
                {globals.MESSAGE.FORGOTSCREEN.RESET_PASSWORD_TITLE}
              </Text>
            </View>
            <View style={styles.resetPass_subContainerStyle}>
              <TextInput
                style={styles.resetPass_currentPassTextInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.EMAIL_ID}
                placeholderTextColor={colors.white}
                autoCapitalize="none"
                value={email}
                returnKeyType="next"
                onSubmitEditing={() => this.passwordRef.focus()}
                onChangeText={text => this.setState({ email: text })}
              />
            </View>
            <View style={styles.resetPass_subContainerStyle}>
              <TextInput
                style={styles.resetPass_currentPassTextInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.PASSWORD}
                placeholderTextColor={colors.white}
                secureTextEntry
                autoCapitalize="none"
                value={password}
                returnKeyType="next"
                ref={passwordRef => (this.passwordRef = passwordRef)}
                onSubmitEditing={() => this.confirmpasswordRef.focus()}
                onChangeText={text => this.setState({ password: text })}
              />
            </View>
            <View style={styles.resetPass_subContainerStyle}>
              <TextInput
                style={styles.resetPass_currentPassTextInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.CONFIRM_PASSWORD}
                placeholderTextColor={colors.white}
                secureTextEntry
                autoCapitalize="none"
                value={confirmPassword}
                returnKeyType="done"
                ref={confirmpasswordRef => (this.confirmpasswordRef = confirmpasswordRef)}
                onChangeText={text => this.setState({ confirmPassword: text })}
              />
            </View>
            <View style={styles.resetPass_touchableViewStyle}>
              <TouchableOpacity onPress={() => this.passwordValidation()}>
                <CustomButton
                  text={globals.BTNTEXT.LOGINSCREEN.RESETPASSLOGINBTNTEXT}
                  backgroundColor={colors.white}
                  color={colors.warmBlue}
                  loadingStatus={loading}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.resetpass_backViewstyle}>
              <TouchableOpacity>
                <Image
                  source={images.loginAndRegisterScreen.backIcon}
                  style={styles.resetpass_backIconStyles}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}
