/* eslint-disable no-underscore-dangle */
/* eslint-disable react/prop-types */
/* eslint-disable no-return-assign */
/* eslint-disable no-useless-constructor */
import React from 'react';
import { Text, View, TouchableOpacity, Image, TextInput, ScrollView, Alert, Platform } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import * as globals from '../../../utils/globals';
import globalStyles from '../../../assets/styles/globleStyles';
import * as colors from '../../../assets/styles/color';
import SectionedMultiSelect from '../../../libs/react-native-sectioned-multi-select';
import MultiSelect from '../../../libs/react-native-multiple-select';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import loginScreen from '../AuthenticationScreens/loginScreen';
import { API } from '../../../utils/api';
import Member from './index';
import KeyboardListener from 'react-native-keyboard-listener';
import Swiper from 'react-native-swiper';


const TAG = '==:== GlobalMemberSearch Screen : ';
const iPad = DeviceInfo.getModel();
class GlobalMemberSearch extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Global Members (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Members ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
        }
    };

    constructor(props) {
        super(props);
        console.log(TAG, "props :---", props);
        this.state = {
            communityId: this.props.navigation.state.params.communityID,
            memberName: '',
            organizationName: '',
            Communities: '',
            profileCategory: '',
            skills: '',
            loading: false,
            sectionListOpen: false,
            businessCategoryName: '',
            tempBizGroupData: [],
            BizGroups: [],
            categoriesData: [],
            selectedItems: [],
            profileCategory: [],
            profileCategoryData: [],
            skillsAndIneterst: [],
            skillsAndInterestData: [],
            CommunitiesData: [],
            communitiesTempArr: [],
            keyboardOpen: false,
            avdertisementData: this.props.navigation.state.params.avdertisementData

        };
    }

    componentDidMount() {
        this._GetMembersIntialData()
    }

    _GetMembersIntialData() {
        if (globals.isInternetConnected === true) {
            this.setState({ loading: true })
            API.getCommunityMembersIntialData(this.GlobalMembersSearchIntialDataResponseData, true)
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    GlobalMembersSearchIntialDataResponseData = {
        success: response => {
            console.log(
                TAG,
                'GlobalMembersSearchIntialDataResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                const bizGroups = response.Data.BizCategories.BizGroups;
                const interestSkill = response.Data.SkillsandInterests;
                let uniqueSkillsInterests = interestSkill.filter((ele, ind) => ind === interestSkill.findIndex(elem => elem.Name === ele.Name && elem.id === ele.id))
                const categoryProfile = response.Data.WhoAmIAll;
                let uniqueProfileCategories = categoryProfile.filter((ele, ind) => ind === categoryProfile.findIndex(elem => elem.Name === ele.Name && elem.id === ele.id))
                const communities_data = response.Data.Communities;
                let uniqueCommunities = communities_data.filter((ele, ind) => ind === communities_data.findIndex(elem => elem.Name === ele.Name && elem.id === ele.id))
                this.setState({ BizGroups: bizGroups })
                this.setState({ skillsAndInterestData: uniqueSkillsInterests })
                this.setState({ profileCategoryData: uniqueProfileCategories, CommunitiesData: uniqueCommunities })
            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message);
                })
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader()
            this.setState({ loading: false })
            console.log(
                TAG,
                'GlobalMembersSearchIntialDataResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403 || err.StatusCode == 500) {
                this.setState({ loading: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader()
            this.setState({ loading: false })
        },
    };


    /**
       *  call when _sessionOnPres
       */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }


    /*
     * method navigate To flatlist screen with paramerters
     */

    navigateToListScreen() {
        const { memberName, organizationName, skillsAndIneterst, profileCategory, tempBizGroupData, communitiesTempArr, CommunitiesData } = this.state;
        var profile_Category = profileCategory.toString()
        var skills_And_Ineterst = skillsAndIneterst.toString()
        var tempBiz_GroupData = tempBizGroupData.toString()
        var communities_TempArr = communitiesTempArr.toString()
        Member.searchMember(memberName, organizationName, skills_And_Ineterst, profile_Category, tempBiz_GroupData, communities_TempArr);
        this.props.navigation.goBack();
    }

    onSelectedprofileCategoryChange = profileCategory => {
        this.setState({ profileCategory });
    };

    onSelectedSkillsDataChange = skillsAndIneterst => {
        if (skillsAndIneterst.length < 11) {
            this.setState({ skillsAndIneterst });
        } else {
            Alert.alert(globals.appName, "You can only select 10 items")
        }
    };

    onSelectedItemsChange = (tempBizGroupData) => {
        if (tempBizGroupData.length < 6) {
            this.setState({ tempBizGroupData });
        } else {
            Alert.alert(globals.appName, "You can only select 5 items")
        }
    };

    onSelectedCommunitiesChange = (communitiesTempArr) => {
        if (communitiesTempArr.length < 4) {
            this.setState({ communitiesTempArr })
        } else {
            Alert.alert(globals.appName, "You can only select 3 items")
        }
    }


    render() {
        const { memberName, organizationName, profileCategoryData, skillsAndInterestData, BizGroups, skillsAndIneterst, profileCategory, tempBizGroupData, CommunitiesData, communitiesTempArr } = this.state;
        return (
            <View style={styles.mainParentStyle}>
                {
                    (this.state.avdertisementData.length > 0) ?
                        <View style={{ height: globals.screenHeight * 0.2 }}>
                            <Swiper
                                autoplay={true}
                                autoplayTimeout={3.5}
                            >
                                {this.state.avdertisementData.map((data, index) => {
                                    return (
                                        <View key={index}>
                                            <Image source={{ uri: data.Image }} style={{ width: '100%', height: globals.screenHeight * 0.2 }} />
                                        </View>
                                    )
                                })}
                            </Swiper>
                        </View> : null
                }

                <KeyboardListener
                    onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                    onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                <ScrollView bounces={false} showsVerticalScrollIndicator={false} style={styles.svStyle}>
                    <View style={[styles.mainParentStyle, {
                        marginBottom: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?
                            globals.screenHeight * 0.0355 : globals.screenHeight * 0.02
                    }]}>
                        <View style={styles.textInputViewContainer}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={'Search by Member Name'}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ memberName: text })}
                                returnKeyType="next"
                                blurOnSubmit={false}
                                value={memberName}
                                onSubmitEditing={() => this.memberNameRef.focus()}
                                autoCapitalize="none"
                            />
                        </View>
                        <View style={styles.textInputViewContainer2}>
                            <TextInput
                                style={styles.textInputStyleContainer}
                                placeholder={"Search by Organization Name"}
                                placeholderTextColor={colors.lightGray}
                                onChangeText={text => this.setState({ organizationName: text })}
                                returnKeyType="done"
                                blurOnSubmit={false}
                                value={organizationName}
                                autoCapitalize="none"
                                ref={memberNameRef => (this.memberNameRef = memberNameRef)}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <MultiSelect
                                hideTags={false}
                                items={profileCategoryData}
                                uniqueKey="Name"
                                ref={(component) => { this.multiSelect = component }}
                                onSelectedItemsChange={this.onSelectedprofileCategoryChange}
                                selectedItems={profileCategory}
                                selectText="Search by Profile Category"
                                searchInputPlaceholderText="Search Profile Category..."
                                onChangeInput={(text) => console.log(text)}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="#CCC"
                                selectedItemTextColor="#CCC"
                                selectedItemIconColor="#CCC"
                                itemTextColor="#000"
                                displayKey="Name"
                                searchInputStyle={{ color: '#CCC' }}
                                submitButtonColor="#CCC"
                                submitButtonText="Submit"
                                hideSubmitButton={true}
                                styleDropdownMenu={styles.multipleSelectDropdownMenu}
                                styleMainWrapper={styles.multipleSelectMainWrapper}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <MultiSelect
                                hideTags={false}
                                items={skillsAndInterestData}
                                uniqueKey="Name"
                                ref={(component) => { this.multiSelect = component }}
                                onSelectedItemsChange={this.onSelectedSkillsDataChange}
                                selectedItems={skillsAndIneterst}
                                selectText="Search by Skills and Interests (Upto 10)"
                                searchInputPlaceholderText="Search Skills and Interests..."
                                onChangeInput={(text) => console.log(text)}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="#CCC"
                                selectedItemTextColor="#CCC"
                                selectedItemIconColor="#CCC"
                                itemTextColor="#000"
                                displayKey="Name"
                                searchInputStyle={{ color: colors.black }}
                                submitButtonColor="#CCC"
                                submitButtonText="Submit"
                                styleDropdownMenu={styles.multipleSelectDropdownMenu}
                                styleMainWrapper={styles.multipleSelectMainWrapper}
                                hideSubmitButton={true}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <MultiSelect
                                hideTags={false}
                                items={CommunitiesData}
                                uniqueKey="ID"
                                ref={(component) => { this.multiSelect = component }}
                                onSelectedItemsChange={this.onSelectedCommunitiesChange}
                                selectedItems={communitiesTempArr}
                                selectText="Search by Communities (Upto 3)"
                                searchInputPlaceholderText="Search Communities..."
                                onChangeInput={(text) => console.log(text)}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="#CCC"
                                selectedItemTextColor="#CCC"
                                selectedItemIconColor="#CCC"
                                itemTextColor="#000"
                                displayKey="Name"
                                searchInputStyle={{ color: colors.black }}
                                submitButtonColor="#CCC"
                                submitButtonText="Submit"
                                styleDropdownMenu={styles.multipleSelectDropdownMenu}
                                styleMainWrapper={styles.multipleSelectMainWrapper}
                                hideSubmitButton={true}
                            />
                        </View>
                        <View>
                            <View style={styles.multipleSelectViewStyle} />
                            <SectionedMultiSelect
                                items={BizGroups}
                                uniqueKey="ID"
                                subKey="BusinessCategories"
                                selectText="Search by Business Categories (Upto 5)"
                                showDropDowns={true}
                                readOnlyHeadings={true}
                                onSelectedItemsChange={this.onSelectedItemsChange}
                                selectedItems={tempBizGroupData}
                                displayKey="GroupName"
                                subDisplayKey="Name"
                                showCancelButton={false}
                                hideConfirm={false}
                            />
                        </View>
                    </View>
                </ScrollView>
                {(Platform.OS == "android") ?
                    (this.state.keyboardOpen == false) ?
                        <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this.navigateToListScreen()}>
                            <Text style={styles.seachTextStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                        </TouchableOpacity>
                        :
                        null
                    :
                    <TouchableOpacity style={styles.searchButtonContainer} onPress={() => this.navigateToListScreen()}>
                        <Text style={styles.seachTextStyle}>{globals.MESSAGE.EVENTDASHBOARD.SEARCH_TITLE}</Text>
                    </TouchableOpacity>
                }
            </View>
        );
    }
}


// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GlobalMemberSearch);