import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, KeyboardAvoidingView, ActivityIndicator, TextInput, Alert, TouchableWithoutFeedback, Platform, Keyboard, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Swiper from 'react-native-swiper';
import { NavigationEvents } from 'react-navigation';
import Modal from "react-native-modal";
import styles from './style';
import * as globals from '../../../utils/globals';
import globalStyles from '../../../assets/styles/globleStyles';
import * as colors from '../../../assets/styles/color';
import * as images from '../../../assets/images/map';
import { API } from '../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import DeviceInfo from 'react-native-device-info';
import loginScreen from '../AuthenticationScreens/loginScreen';
import Nointernet from '../../../components/NoInternet/index'
import ServerError from '../../../components/ServerError/index'

const iPad = DeviceInfo.getModel();
let TAG = "Members Screen ::==="
let _this = null;
class GlobalMember extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const globalEventCount = (params !== undefined) ? params.globalEventCount : ''
        return {
            headerLeft: (globalEventCount != undefined) ?
                globals.ConnectProbackButton(navigation, 'Global Members (' + globalEventCount + ')') : globals.ConnectProbackButton(navigation, 'Members ()'),
            headerStyle: globalStyles.ConnectPropheaderStyle,
            headerRight: (
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('GlobalMemberSearch', { globalEventCount: globalEventCount, avdertisementData: _this.state.avdertisementData })}
                        style={{
                            marginRight: globals.screenWidth * 0.04,
                            alignItems: 'center',
                            marginTop:
                                iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                                    ? globals.screenHeight * 0.03
                                    : null,
                        }}
                    >
                        <Image
                            source={images.headerIcon.searchIcon}
                            resizeMode={"contain"}
                            style={{
                                height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.030 : globals.screenHeight * 0.025,
                                tintColor: colors.warmBlue,
                                alignSelf: 'center',
                            }}
                        />
                    </TouchableOpacity>
                </View>
            ),
        }
    };


    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isInternetFlag: true,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            totalCommunities: 0,
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            visibleModal: false,
            receiverID: 0,
            notes: '',
            avdertisementData: []
        }
    }

    componentDidMount() {
        this.makeAPICall()

    }
    makeAPICall() {
        this.props.showLoader()
        this.apicallGetMembersList()
    }


    static refreshList() {
        _this.makeListFresh(() => {
            _this.apicallGetMembersList();
        });
    }

    static searchMember(memberName, organizationName, skills_And_Ineterst, profile_Category, tempBiz_GroupData, communities_TempArr) {
        _this.setState(
            {
                memberName: memberName,
                businessCategory: tempBiz_GroupData,
                skillsInterest: skills_And_Ineterst,
                whoAmIName: profile_Category,
                companyName: organizationName,
                communityName: communities_TempArr,
                loading: true,
                responseComes: false
            },
            () => {
                _this.makeListFresh(() => {
                    _this.apicallGetMembersList();
                });
            }
        );
    }

    makeListFresh(callback) {
        this.setState(
            {
                pageNumber: 1,
                pageSize: 10,
                totalCommunities: 0,
                membersData: [],
            },
            () => {
                callback();
            }
        );
    }

    /**
     * prepareData when call the API
     */
    prepareData() {
        const {
            pageNumber, pageSize, memberName, companyName, locationSearch, businessCategory, communityName, whoAmIName, skillsInterest, groupname
        } = this.state;

        const data = {};
        data.PageNumber = pageNumber;
        data.PageSize = pageSize;
        data.MemberNameSearch = memberName
        data.CompanyNameSearch = companyName
        data.LocationSearch = locationSearch
        data.BusinessCategoriesSearch = businessCategory
        data.CommunitiesSearch = communityName
        data.UserID = globals.userID
        data.WhoAmISearch = whoAmIName
        data.skillsInterestsSearch = skillsInterest
        data.GroupsSearch = groupname
        return data;
    }

    /**
     * APi call for get members list
     */
    apicallGetMembersList() {
        if (globals.isInternetConnected === true) {
            this.setState({ loadingPagination: true, loading: true, isInternetFlag: true })
            API.getMembersList(this.GetCommunityMembersResponseData, this.prepareData(), true)
            API.getCommunityMembersIntialData(this.GlobalMembersSearchIntialDataResponseData, true)
        } else {
            this.props.hideLoader()
            this.setState({ isInternetFlag: false })
        }
    }

    /**
 * Response of MyCommunity listing
 */
    GetCommunityMembersResponseData = {
        success: response => {
            this.props.hideLoader()
            console.log(
                TAG,
                'GetCommunityMembersResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                total = response.Data.TotalRecords;
                _this.props.navigation.setParams({
                    globalEventCount: response.Data.TotalRecords
                });

                if (this.state.pageNumber == 1) {
                    let members = response.Data.MembersList
                    let isFound = this.checkDuplicateValue(members.UserID, members);
                    if (isFound) {
                        this.setState({
                            loading: false, responseComes: true, loadingPagination: false,
                            membersData: response.Data.MembersList,
                        })
                    } else {
                        this.setState({ loadingPagination: false, loading: false, responseComes: true, })
                    }
                } else {

                    this.setState({
                        loading: false, responseComes: true, loadingPagination: false,
                        membersData: [...this.state.membersData, ...response.Data.MembersList],
                    }, () => {
                        let membersData = this.state.membersData;
                        let uniqueMembers = membersData.filter((ele, ind) => ind === membersData.findIndex(elem => elem.UserID === ele.UserID && elem.id === ele.id))
                        this.setState({ membersData: uniqueMembers })
                    })
                }

            } else {
                Alert.alert(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ serverErr: true, loading: false });
            console.log(
                TAG,
                'GetCommunityMembersResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403) {
                this.setState({ loading: false, loadingPagination: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    /**
    * method for checkDuplicateValue
    */
    checkDuplicateValue(value, tempArray) {
        let found = false;
        for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i].title == value) {
                found = true;
                break;
            }
        }
        return found;
    }

    GlobalMembersSearchIntialDataResponseData = {
        success: response => {
            console.log(
                TAG,
                'GlobalMembersSearchIntialDataResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200 && response.Result == true) {
                const bizGroups = response.Data.BizCategories.BizGroups;
                const interestSkill = response.Data.SkillsandInterests;
                const categoryProfile = response.Data.WhoAmIAll;
                const communities_data = response.Data.Communities;
                const memberAdds = response.Data.MemberAds;
                this.setState({
                    BizGroups: bizGroups, skillsAndInterestData: interestSkill, profileCategoryData: categoryProfile,
                    CommunitiesData: communities_data, avdertisementData: memberAdds
                })

            }
            else {
                this.setState({ loading: false }, () => {
                    Alert.alert(globals.appName, response.Message);
                })
            }
            this.props.hideLoader()
        },
        error: err => {
            this.props.hideLoader()
            this.setState({ loading: false })
            console.log(
                TAG,
                'GlobalMembersSearchIntialDataResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );
            if (err.StatusCode == 401 || err.StatusCode == 403 || err.StatusCode == 500) {
                this.setState({ loading: false })
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }
        },
        complete: () => {
            this.props.hideLoader()
            this.setState({ loading: false })
        },
    };

    /**
  *  call when _sessionOnPres
  */
    _sessionOnPres() {
        AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
        this.props.navigation.navigate('LoginScreen');
        loginScreen.clearTextFields();
    }

    _tryAgain() {
        this.setState({ serverErr: false, responseComes: false }, () => {
            this.props.showLoader();
            this.apicallGetMembersList();
        });

    }

    renderFooter = () => {
        if (this.state.loadingPagination && this.state.responseComes) {
            return (
                
                        <ActivityIndicator style={styles.loaderbottomview} size="large" color={colors.bgColor} />
                   

            )
        } else {
            return (
                <View />
            )
        }
    };

    /**
     * Render FlatList Items
     */
    renderCommunityMembersList(item, index) {
        return (
            <View>
                {
                    this.renderMembersAccordingConditions(item, index)
                }
            </View>
        );
    }

    /**
     * Method of click connect button
     */
    clickConnectBtn(value, id) {
        let data = {
            ID: null,
            Note: '',
            ReceiverID: this.state.receiverID,
            SenderID: globals.userID

        }
        Keyboard.dismiss();
        this.handleConnectModalStatus(value, id)
        if (globals.isInternetConnected === true) {
            this.props.showLoader()
            API.saveMemberConnections(this.saveMemberConnectionsResponseData, data, true)
        } else {
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    handleConnectModalStatus(visible, id) {
        Keyboard.dismiss();
        this.setState({ visibleModal: visible, receiverID: id })
    }

    /**
 * 
 * Upadte TextInput Data
 */
    handleNotes(text) {
        this.setState({
            notes: text,
        });
    }

    /**
    * Render modal of cancel click of waiting for review
    */
    renderConnectModal() {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.visibleModal}
                onRequestClose={() => {
                    this.handleConnectModalStatus(false, this.state.receiverID);
                }}
                onBackdropPress={() => {
                    this.handleConnectModalStatus(false, this.state.receiverID);
                }}
            >
                <KeyboardAvoidingView behavior={(Platform.OS == "ios") ? "padding" : "height"} keyboardVerticalOffset={(Platform.OS == "ios") ? 80 : null}>
                    <View style={[styles.modalMainView]}>
                        <View style={styles.modalInnerMainView}>
                            <Text style={styles.modalUperText}>Your connection request is on it's way. Would you like to add a note to your request.</Text>
                            <View style={styles.underLineView}></View>
                            <Text style={styles.addNoteTxt}>Add Note:</Text>
                            <View style={styles.textInputViewContainer3}>
                                <TextInput
                                    multiline={true}
                                    maxLength={300}
                                    returnKeyType="done"
                                    autoCapitalize="none"
                                    value={this.state.notes}
                                    placeholderTextColor={colors.lightGray}
                                    onChangeText={text => this.handleNotes(text)}
                                    placeholder={'Add Note..'}
                                    style={[styles.textInputStyleContainer]}
                                />
                            </View>
                            <View style={styles.modalEndView}>
                                <TouchableOpacity style={styles.closeView} onPress={() => { this.handleConnectModalStatus(false, this.state.receiverID); this.setState({ notes: '' }) }}>
                                    <View style={styles.closeView}>
                                        <Text style={styles.closeTxt}>Close</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.sendView} onPress={() => this.clickConnectBtn(false, this.state.receiverID)}>
                                    <View style={styles.sendView} >
                                        <Text style={styles.sendTxt}>Send</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </Modal>
        )
    }


    handleConnectModalStatuswithApi(visible, id) {
        Keyboard.dismiss();
        this.setState({ visibleModal: visible, receiverID: id })
        this.clearStates()
        this.props.showLoader()
        this.apicallGetMembersList()
    }

    /**
     * Response of Accept Reject Invitation 
     */
    saveMemberConnectionsResponseData = {
        success: response => {
            console.log(
                TAG,
                'saveMemberConnectionsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {

                Alert.alert(globals.appName, response.Message, [{ text: 'OK', onPress: () => this.handleConnectModalStatuswithApi(false, this.state.receiverID) }]);
            }
            else {
                Alert.alert(globals.appName, response.Message);
                this.props.hideLoader()
            }
        },
        error: err => {
            this.props.hideLoader();
            this.setState({ loading: false })
            console.log(
                TAG,
                'saveMemberConnectionsResponseData -> ERROR : ',
                JSON.stringify(err.message)
            );

            if (err.StatusCode == 401 || err.StatusCode == 403) {
                Alert.alert(
                    globals.appName,
                    'Your session is expired, Please login again',
                    [{ text: 'OK', onPress: () => this._sessionOnPres() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, err.Message)
            }

        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    Capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    clearStates() {
        this.setState({
            membersData: [],
            loading: false,
            loadingPagination: false,
            serverErr: false,
            isShowSearch: false,
            responseComes: false,
            pageNumber: 1,
            pageSize: 10,
            communityName: '',
            totalCommunities: 0,
            memberName: '',
            companyName: '',
            locationSearch: '',
            businessCategory: '',
            whoAmIName: '',
            skillsInterest: '',
            groupname: '',
            visibleModal: false,
            receiverID: 0,
            notes: '',
            avdertisementData: []
        })
    }

    /**
     * Method of render conditions according views in member lisr
     * @param {*} item 
     * @param {*} index 
     */
    renderMembersAccordingConditions(item, index) {
        const { membersData, loading } = this.state;
        const isMemberName = globals.checkObject(item, 'MemberName');
        const isOrganizationName = globals.checkObject(item, 'OrganizationName');
        const isJobTitle = globals.checkObject(item, 'JobTitle');
        const isEmailAddress = globals.checkObject(item, 'EmailAddress');
        const isPhone = globals.checkObject(item, 'Phone')
        const isMemberProImage = globals.checkImageObject(item, 'ProfilePicturePath');

        if (((item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0) && (item.ConnectionStatus == 0 || item.ConnectionStatus == 3)) ||
            (item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0 && item.ConnectionStatus == 1)) {
            return (
                <TouchableWithoutFeedback style={styles.mainParentStyle}>
                    <View style={{ flex: 1 }}>
                        <View
                            style={[styles.boxViewStyle,
                            { backgroundColor: item.isSelected == true ? colors.listSelectColor : colors.white },]}>
                            <View>
                                <TouchableOpacity onPress={() => (item.UserID == globals.userID) ?
                                    this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" })
                                    :
                                    this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "globalMember" })}>

                                    <View style={styles.userinfoStyle}>
                                        <View style={styles.beforeImgViewStyle}>
                                            <Image
                                                source={{ uri: (isMemberProImage) ? item.ProfilePicturePath : globals.User_img }}
                                                style={styles.imgStyle}

                                            ></Image>
                                        </View>
                                        <View style={styles.textViewStyle}>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle, { fontSize: globals.font_14, marginBottom: 3, fontWeight: '500' }]}>
                                                {(isMemberName) ? this.Capitalize(item.MemberName) : '-'}
                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isJobTitle) ?
                                                    (isOrganizationName) ? item.JobTitle + " at " + item.OrganizationName : '' :
                                                    (isOrganizationName) ? item.JobTitle+ item.OrganizationName : ''
                                                }

                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isEmailAddress) ? item.EmailAddress : ''}

                                            </Text>
                                            <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                                {(isPhone) ? item.Phone : ''}
                                            </Text>
                                        </View>{
                                            ((item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0) && (item.ConnectionStatus == 0 || item.ConnectionStatus == 3)) ?
                                                <TouchableOpacity style={styles.lastImgViewEnd} onPress={() => this.handleConnectModalStatus(true, item.UserID)}>
                                                    <View style={styles.lastImgViewEnd}>
                                                        <View style={styles.connectedBtnView}>
                                                            <Text style={styles.connectTextStyle}>Connect</Text>
                                                        </View>
                                                    </View>
                                                </TouchableOpacity>
                                                :
                                                (item.IsConnected == false && item.UserID != globals.userID && globals.userID != 0 && item.ConnectionStatus == 1) ?
                                                    <View style={styles.lastImgViewEnd}>
                                                        <View style={[styles.connectedBtnView, { backgroundColor: colors.darkYellow }]}>
                                                            <Text style={styles.connectTextStyle}>Pending</Text>
                                                        </View>
                                                    </View> : null
                                        }

                                    </View>

                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            )
        } else {
            return (
                <TouchableWithoutFeedback style={styles.mainParentStyle}>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity style={{}} onPress={() => (item.UserID == globals.userID) ?
                            this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "global" })
                            :
                            this.props.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "globalMember" })}>
                            <View style={[styles.userinfoStyleConnected, {}]}>
                                <View style={styles.connectedTwoViews}>
                                    <View style={styles.beforeImgViewStyle}>
                                        <Image
                                            source={{ uri: (isMemberProImage) ? item.ProfilePicturePath : globals.User_img }}
                                            style={styles.imgStyle}
                                        ></Image>
                                    </View>
                                    <View style={styles.textViewStyle}>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle, { fontSize: globals.font_14, marginBottom: 3, fontWeight: '500' }]}>
                                            {(isMemberName) ? this.Capitalize(item.MemberName) : '-'}
                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isJobTitle) ?
                                                (isOrganizationName) ? item.JobTitle + " at " + item.OrganizationName : '' :
                                                (isOrganizationName) ? item.JobTitle+ item.OrganizationName : ''}

                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isEmailAddress) ? item.EmailAddress : ''}

                                        </Text>
                                        <Text numberOfLines={1} style={[styles.proffesionTextStyle]}>
                                            {(isPhone) ? item.Phone : ''}
                                        </Text>
                                    </View>
                                </View>
                                {
                                    (item.UserID != globals.userID) ?
                                        <View style={{ alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                                            <Image source={images.Communities.connectedAttach} style={styles.connectedbtnImgStyle} />
                                        </View> : null
                                }
                            </View>

                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            )
        }

    }

    handleLoadMore = () => {
        if (!this.onEndReachedCalledDuringMomentum) {
            this.setState({
                pageNumber: this.state.pageNumber + 1,
            }, () => {
                _this.apicallGetMembersList();
            })
            this.onEndReachedCalledDuringMomentum = true;

        }
    };


    render() {
        const { serverErr, loading, membersData, BizGroups, CommunitiesData, memberName, responseComes, profileCategoryData, skillsAndInterestData, organizationName, loadingPagination, isInternetFlag } = this.state;
        return (
            <View style={styles.mainParentStyle}>

                <NavigationEvents
                    onWillFocus={() => this.makeAPICall()}
                    onWillBlur={() => this.clearStates()}
                />
                {this.renderConnectModal()}
                {
                    (!isInternetFlag) ?
                        <Nointernet loading={loading} onPress={() => this._tryAgain()} /> :
                        (serverErr === false) ?
                            (
                                (responseComes == false && loadingPagination == true) ?
                                    <View></View> :
                                    (membersData.length == 0 && responseComes) ?
                                        (membersData.length == 0 && (memberName !== '' || organizationName !== '' || profileCategoryData.length !== 0 ||
                                            skillsAndInterestData.length !== 0 || CommunitiesData.length !== 0 || BizGroups.length !== 0)) ?
                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.DATA_NOT_AVAILABLE_IN_SEARCH}</Text>
                                            </View> :

                                            <View style={globalStyles.nodataStyle}>
                                                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.DATA_NOT_AVAILABLE.COMMUNITY_MEMBERS_NOT_AVLB}</Text>
                                            </View> :
                                        <View style={styles.mainParentStyle}>


                                            {
                                                (this.state.avdertisementData.length > 0) ?
                                                    <View style={{ height: globals.screenHeight * 0.2 }}>
                                                        <Swiper
                                                            autoplay={true}
                                                            autoplayTimeout={3.5}
                                                        >
                                                            {this.state.avdertisementData.map((data, index) => {
                                                                return (
                                                                    <View key={index}>
                                                                        <Image source={{ uri: data.Image }} style={{ width: '100%', height: globals.screenHeight * 0.2 }} />
                                                                    </View>
                                                                )
                                                            })}
                                                        </Swiper>
                                                    </View> : null
                                            }

                                            <View style={styles.mainParentStyle}>
                                                <FlatList
                                                    style={{ flex: 1, marginTop: globals.screenHeight * 0.02 }}
                                                    showsVerticalScrollIndicator={false}
                                                    data={membersData}
                                                    renderItem={({ item, index }) => this.renderCommunityMembersList(item, index)}
                                                    extraData={this.state}
                                                    bounces={false}
                                                    keyExtractor={(index, item) => item.toString()}
                                                    ListFooterComponent={this.renderFooter}
                                                    onEndReached={this.handleLoadMore}
                                                    onEndReachedThreshold={0.2}
                                                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                                                />
                                            </View>
                                        </View>
                            )
                            :
                            (
                                <ServerError loading={loading} onPress={() => this._tryAgain()} />
                            )
                }
            </View>
        );
    }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(GlobalMember);