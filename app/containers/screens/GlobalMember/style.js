import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import { getVerticleBaseValue } from '../../../utils/globals';

const ITEM_HEIGHT = globals.screenHeight * 0.1136; // 60;
const OPENITEM_HEIGHT = globals.screenHeight * 0.19; // 110;

const iPad = DeviceInfo.getModel();


module.exports = StyleSheet.create({
  mainParentStyle: {
    flex: 1,
  },
  loaderbottomview: {
    bottom: (Platform.OS == 'android') ?
      globals.screenHeight * 0.028 : globals.screenHeight * 0.041, justifyContent: 'center', alignSelf: 'center'
  },
  loaderWrapper: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 999,
    backgroundColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
},
loaderInner: {
  alignItems: 'center',
  justifyContent: 'center',
},
  userinfoStyle: {
    flexDirection: 'row',
    marginHorizontal: globals.screenWidth * 0.04,
    paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.020 : globals.screenWidth * 0.035,
    alignItems:'flex-start',borderWidth: 0.7,
    borderColor: colors.gray,
    alignItems:'center',
    borderRadius: 5,marginBottom: globals.screenHeight * 0.015,
  },
  userinfoStyleConnected: {
    flex:1,
    flexDirection: 'row',
    marginHorizontal: globals.screenWidth * 0.04,
    //paddingHorizontal: globals.screenWidth * 0.02,
   // paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.025 : globals.screenWidth * 0.035,
    borderWidth: 0.7,
    borderColor: colors.gray,
    //justifyContent:'center',
    alignItems:'flex-start',
    borderRadius: 5,marginBottom: globals.screenHeight * 0.015,
  },
  connectedTwoViews:{
    flex:1,
     flexDirection: 'row',
    paddingHorizontal: globals.screenWidth * 0.02,
    paddingVertical: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.025 : globals.screenWidth * 0.035,
    alignItems:'center', 
    justifyContent:'center',
  },
  connectedbtnImgStyle:{
    height:globals.screenHeight*0.12,
    width:globals.screenHeight*0.12
  },
//   boxViewStyle: {
//     borderWidth: 0.7,
//     borderColor: colors.gray,
//     borderRadius: 5,
//     marginBottom: globals.screenHeight * 0.015,
//     width: globals.screenWidth * 0.90,
//     marginLeft: globals.screenWidth * 0.0595,
//     flex:1,
//   },
  beforeImgViewStyle:{
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 :globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.10 :globals.screenWidth * 0.15,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (globals.screenWidth * 0.10)/2 :(globals.screenWidth * 0.15) / 2,
    borderColor: colors.proUnderline,
    borderWidth: 0.5,
  },
  imgStyle: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.12 :globals.screenWidth * 0.15,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.12 :globals.screenWidth * 0.15,
    borderRadius: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (globals.screenWidth * 0.12)/2 :(globals.screenWidth * 0.15) / 2,
    borderColor: colors.white,
    borderWidth:0.5,
  },
  textViewStyle: {
    // alignContent: 'center',
    marginHorizontal:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?globals.screenWidth * 0.038: globals.screenWidth * 0.035,
    marginVertical: globals.screenWidth * 0.025,
    flex:1,
    justifyContent:'center'
  },
  usernametextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_12,
    color:colors.lightBlack,
},
  proffesionTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 20 : globals.font_12,
    marginRight:5,flex:1,marginTop:2,color:colors.lightBlack,

  },
  lastImgViewEnd:{
    justifyContent:'center', alignItems:'flex-end',
  },
  connectedBtnView:{
      alignItems:'center',
      justifyContent:'center',
      backgroundColor:colors.listSelectColor,
      height:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth*0.07 :globals.screenWidth*0.09,
      width:iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ?globals.screenWidth*0.18 : globals.screenWidth*0.22,
      borderRadius:5
  },
  connectTextStyle:{
      color:colors.white,
      fontSize:globals.font_13
  },

  //connect modal styles
  modalMainView:{
    height: globals.screenHeight * 0.43,
    justifyContent: 'center', alignItems: 'center',
    alignSelf:'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor:colors.proUnderline,
     borderRadius: 6,
     borderWidth:1,
     borderColor:colors.gray,
    marginTop: globals.screenHeight * 0.2
  },
  modalInnerMainView: {
    backgroundColor: colors.proUnderline,
    flex: 1,
    width: '85%',
    margin: 10,
  },
  add_not: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    marginTop:7,
    marginBottom : 5
  },
  underLineView:{
    height:1, backgroundColor:colors.gray, marginVertical:10, marginHorizontal:-10
  },
  addNoteTxt:{
    marginTop: 7, marginBottom:5, fontWeight:'600',fontSize:globals.font_10
  },
  modalUperText:{
    fontSize:globals.font_13,
    fontWeight:'500',
  },
  textInputViewContainer3: {
    flexDirection: 'row',
    height: globals.screenHeight * 0.17,
    borderWidth: 1,
    borderColor: colors.gray,
    // marginHorizontal: 10,
    alignItems: 'flex-start',
    marginTop: globals.screenHeight * 0.01,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginTop: 5,
    marginBottom: 5,
    height:globals.screenHeight * 0.18
  },
  modalEndView:{justifyContent:'flex-end', height:(globals.screenHeight*0.06), marginTop:10, flexDirection:'row'},
  closeView:{
    height: globals.screenHeight * 0.045, width: globals.screenWidth * 0.2, 
     borderWidth:1,  borderColor:colors.gray,
    borderRadius: 3,  alignItems: 'center', justifyContent: 'center'
  },
  sendView: {
    height: globals.screenHeight * 0.045, width: globals.screenWidth * 0.2, 
    backgroundColor: colors.listSelectColor,
    borderRadius: 3, marginLeft: 5, alignItems: 'center', justifyContent: 'center'
  },
  sendTxt: {
    fontSize: globals.font_13,
    color: colors.white
  },
  closeTxt: {
    fontSize: globals.font_13,
    color: colors.black
  },
 //
 //directory screen

 container: {
  flex: 1,
  backgroundColor: colors.white,
  alignItems: 'center',
  justifyContent: 'center',
  paddingVertical: 50,
},
onlyRowStyle: {
  flexDirection: 'row',
},
itemContainer: {
  flex: 1,
  flexDirection: 'column',
  borderColor: colors.lightBorder,
  borderBottomWidth: 1,
  marginHorizontal: globals.screenWidth * 0.09, // 15
},
closedStyle: {
  height: ITEM_HEIGHT,
},
openStyle: {
  height: OPENITEM_HEIGHT,
},
itemTitle: {
  color: colors.lightBlack,
  fontSize:globals.font_13
},
itemSubtitle: {
  color: colors.darkG,
  paddingTop: 0,
  fontSize:globals.font_12

},
vwSelectedCharStyle: {
  height: globals.screenHeight * 0.0757, // 40
  borderColor: colors.lightBorder,
  borderBottomWidth: 1,
  paddingLeft: globals.screenWidth * 0.0468, // 15
  justifyContent: 'center',
  marginBottom: globals.screenHeight * 0.0094, // 5
},
tvSelectedCharStyle: {
  fontSize: globals.font_14,
  color: colors.warmBlue,
},
ivProfileImageStyle: {
  height: globals.screenHeight * 0.0852, // 45
  width: globals.screenHeight * 0.0852, // 45,
  borderRadius: globals.screenHeight * 0.0426, // 22.5,
},
vwInfoStyle: {
  justifyContent: 'center',
  width: '60%',
},
vwOnlyjustifyContentStyle: {
  justifyContent: 'center',
},
vwOnlyAlignedItemStyle: {
  alignItems: 'center',
},
dotStyle: {
  height: globals.screenHeight * 0.0284, // 15
  width: globals.screenHeight * 0.0284, // 15
  margin: globals.screenWidth * 0.0156, //5
  tintColor:colors.lightGray
},
scrollBarContainerStyle: {
  position: 'absolute',
  top: 0,
  height: '79%',
},
scrollBarPointerContainerStyle: {
  height: 0,
  width: 0,
},
vwChildStyle: {
  //backgroundColor:'red',
   paddingTop: globals.screenHeight * 0.0189, // 10
  // paddingBottom: globals.screenHeight * 0.0378, //10
},
toChildButtonStyle: {
  height: globals.screenHeight * 0.0662, // 35
  width: globals.screenHeight * 0.0662, // 35
  alignItems: 'center',
  justifyContent: 'center',
},
reviewView:{
  height: (globals.screenHeight * 0.035),
  width: (globals.screenHeight * 0.035),
  position: 'absolute',
  zIndex: 15,
  bottom: -3,
  right: 3,
  backgroundColor:colors.white,
  borderRadius:7.5,
  borderColor:colors.lightWhite,
  borderWidth:1,
  alignItems:'center',
  justifyContent:'center'
},
lockIconStyle: {
  height: (globals.screenHeight * 0.035),
  width: (globals.screenHeight * 0.035),
  position: 'absolute',
  zIndex: 15,
  bottom: -3,
  right: 3,
  backgroundColor:colors.white,
  borderRadius:(globals.screenHeight * 0.035)/2,
  borderColor:colors.lightWhite,
  borderWidth:1,
  alignItems:'center',
  justifyContent:'center'

},
mailCallIconStyle:{
  height: globals.screenHeight * 0.05, // 35
  width: globals.screenHeight * 0.05, // 35

},

//search screen

  textInputViewContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.05,
    borderRadius: 5,
  },
  textInputViewContainer2: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: colors.proUnderline,
    marginHorizontal: 25,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.025,
    borderRadius: 5,
  },
  textInputStyleContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_14,
    marginTop: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(15),
    marginBottom: Platform.OS === 'android' ? getVerticleBaseValue(1) : getVerticleBaseValue(15),
  },
  searchButtonContainer: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.073 : globals.screenHeight * 0.06, // change in scrollview style also
    width: '100%',
    backgroundColor: colors.bgColor,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0,
  },
  seachTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_20,
    color: colors.white,
  },
  svStyle: {
    marginBottom: getVerticleBaseValue(57),
  },
  datePickerViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 40 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 15 : 20,
  },
  pickerStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.935 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenWidth * 0.84 : globals.screenWidth * 0.87,
  },
  placeHolderTextStyle: {
    color: colors.lightGray,
    fontSize: globals.font_14,
    textAlign: 'left',
    marginRight: iPad.indexOf('iPad') != -1 ? globals.screenWidth * 0.58 : DeviceInfo.isTablet() ? globals.screenWidth * 0.62 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenWidth * 0.41 : globals.screenWidth * 0.4,
  },
  startDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.proUnderline,
    marginTop: 8,
    marginBottom: 8,
    height: iPad.indexOf('iPad') != -1 ? globals.screenHeight * 0.056 : DeviceInfo.isTablet() ? globals.screenHeight * 0.05 : (Platform.OS == 'android') ? globals.screenHeight * 0.065 : (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? globals.screenHeight * 0.07 : globals.screenHeight * 0.06,
  },
  endDateInputStyle: {
    borderRadius: 5,
    borderColor: colors.proUnderline,
    marginTop: 8,
    marginBottom: 8,
    marginLeft: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 5 : 10,
    marginRight: (DeviceInfo.getModel() == 'iPhone 5' || DeviceInfo.getModel() == 'iPhone 5s' || DeviceInfo.getModel() == 'iPhone SE') ? 5 : 20,
  },
  dateIconStyle: {
    position: 'absolute',
    left: 0,
    marginLeft: 0,
  },
  multipleSelectViewStyle: {
    marginTop: globals.screenHeight * 0.025,
  },
  multipleSelectDropdownMenu: {
    alignItems: 'center', 
    alignSelf: 'center',
  },
  multipleSelectMainWrapper: {
    borderWidth: 1, 
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.06 : null,
    borderColor: colors.proUnderline, 
    borderRadius: 5, 
    marginHorizontal: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.026 : (Platform.OS === 'android') ? globals.screenWidth * 0.065 : globals.screenWidth * 0.06,
  },
});