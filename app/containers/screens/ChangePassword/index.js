/* eslint-disable no-return-assign */
/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-sequences */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable react/sort-comp */
/* eslint-disable eqeqeq */
/* eslint-disable no-alert */
/* eslint-disable no-undef */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import {
  View,
  Image,
  StatusBar,
  TextInput,
  Alert,
  ScrollView,
  TouchableOpacity,
  Keyboard,
  Text,
  ImageBackground,
} from 'react-native';
import styles from './style';
import * as colors from '../../../assets/styles/color';
import * as globals from '../../../utils/globals';
import CustomButton from '../../../components/CustomButton';
import Validation from '../../../utils/validation';
import { API } from '../../../utils/api';
import * as images from '../../../assets/images/map';
import loginScreen from "../../../containers/screens/AuthenticationScreens/loginScreen";
import AsyncStorage from '@react-native-community/async-storage';


const TAG = 'ChangePasswordScreen ::=';
let _this;
export default class ChangePassword extends React.Component {


  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      currentPassword: '',
      newPassword: '',
      confirmNewPassword: '',
      loading: false,
    };
  }

  /**
   * method for clear textFileds
   */
  componentWillUnmount() {
    _this.setState({ currentPassword: '', newPassword: '', confirmNewPassword: '' });
  }

  /**
   * method for valid passwords
   */
  passwordValidation() {
    const { currentPassword, newPassword, confirmNewPassword } = this.state;

    if (Validation.isValidPassword(currentPassword)) {
      if (Validation.textInputCheck(newPassword)) {
        if (Validation.passwordLength(newPassword, 7)) {
          if (Validation.isValidPassword(newPassword)) {
            if (Validation.textInputCheck(confirmNewPassword)) {
              if (Validation.passwordLength(confirmNewPassword, 7)) {
                if (Validation.isValidPassword(confirmNewPassword)) {
                  if (newPassword === confirmNewPassword) {
                    if (globals.isInternetConnected === true) {
                      AsyncStorage.getItem(globals.LOGINUSERID).then(token => {
                        globals.userID = token
                        const data = {
                          OldPassword: currentPassword,
                          NewPassword: newPassword,
                          ConfirmPassword: confirmNewPassword,
                          userId: globals.userID,
                        };
                        API.changePasscode(this.getchangePassResponseData, data, true);
                        this.setState({ loading: true });
                      });

                    } else {
                      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
                      this.setState({ loading: false });
                    }
                  } else {
                    Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_SAMEPASS);
                    this.setState({ loading: false });
                  }
                } else {
                  Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CONFIRMTPASS);
                  this.setState({ loading: false });
                }
              } else {
                Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_NEED_7_CHARACTER);
              }
            } else {
              Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTERCONFIRMNEWPASS);
              this.setState({ loading: false });
            }
          } else {
            Alert.alert(globals.appName, globals.MESSAGE.REGISTERSTACK.REG_NEWPASSVALID);
          }
        } else {
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_NEED_7_CHARACTER);
        }
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_ENTERNEWPASS);
        this.setState({ loading: false });
      }
    } else {
      Alert.alert(globals.appName, "Incorrect password. Try again");
      this.setState({ loading: false });
    }
  }

  /**
* API getchangePass response
*/
  getchangePassResponseData = {
    success: response => {
      if (response.StatusCode == 200) {
        console.log(TAG, 'ChangePasswordAPI response==', response);
        if (response.Result === true) {
          Keyboard.dismiss();
          Alert.alert(globals.appName, response.Message, [
            { text: 'OK', onPress: () => this.props.navigation.navigate('LoginScreen') }
          ]);
          this.setState({ loading: false }),
            loginScreen.clearTextFields()
        } else {
          Alert.alert(globals.appName, response.Message, [{ text: 'OK' }]),
            this.setState({ loading: false });
        }
      } else {
        Alert.alert(globals.appName, response.Message);
        this.setState({ loading: false });
      }
    },

    error: err => {
      console.log(TAG, `ChangePasswordAPI responseERROR== ${JSON.stringify(err.message)}`);
      Alert.alert(globals.appName, err.Message);
      this.setState({ loading: false });
    },
    complete: () => { },
  };

  /**
  * method for closed drawer
  */
  drawerClosed() {
    this.setState({ currentPassword: '', newPassword: '', confirmNewPassword: '' });
    this.props.navigation.openDrawer()
  }

  render() {
    const { currentPassword, newPassword, confirmNewPassword, loading } = this.state;
    return (

      <ImageBackground source={images.loginAndRegisterScreen.blueBackground} style={styles.backgroundStyle}>
        <View style={styles.backViewstyle}>
          <TouchableOpacity onPress={() => this.drawerClosed()}>
            <Image source={images.loginAndRegisterScreen.backIcon} style={styles.backIconStyles} />
          </TouchableOpacity>
        </View>
        <ScrollView bounces={false} style={styles.scrollViewStyle}>

          <View style={styles.mainContainer}>
            <View style={styles.titleView}>
              <Text style={styles.titleStyle}>{globals.MESSAGE.FORGOTSCREEN.CHANGE_PASSWORD_TITLE}</Text>
            </View>
            <View style={styles.subContainerStyle}>

              <TextInput
                style={styles.currentPassTextInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.CURRENT_PASSWORD}
                placeholderTextColor={colors.white}
                secureTextEntry
                autoCapitalize="none"
                value={currentPassword}
                returnKeyType="next"
                onSubmitEditing={() => this.newPasswordRef.focus()}
                onChangeText={text => this.setState({ currentPassword: text })}
              />
            </View>
            <View style={styles.subContainerStyle}>

              <TextInput
                style={styles.currentPassTextInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.NEW_PASSWORD}
                placeholderTextColor={colors.white}
                secureTextEntry
                autoCapitalize="none"
                returnKeyType="next"
                value={newPassword}
                ref={newPasswordRef => (this.newPasswordRef = newPasswordRef)}
                onSubmitEditing={() => this.confirmpasswordRef.focus()}
                onChangeText={text => this.setState({ newPassword: text })}
              />
            </View>
            <View style={styles.subContainerStyle}>

              <TextInput
                style={styles.currentPassTextInputStyle}
                placeholder={globals.PLACEHOLDERTXT.LOGIN.CONFIRM_NEW_PASSWORD}
                placeholderTextColor={colors.white}
                secureTextEntry
                autoCapitalize="none"
                returnKeyType="done"
                value={confirmNewPassword}
                ref={confirmpasswordRef => (this.confirmpasswordRef = confirmpasswordRef)}
                onChangeText={text => this.setState({ confirmNewPassword: text })}
              />
            </View>
            <View style={styles.touchableViewStyle}>
              <TouchableOpacity onPress={() => this.passwordValidation()}>
                <CustomButton
                  text={globals.BTNTEXT.LOGINSCREEN.FORGOTPASSLOGINBTNTEXT}
                  backgroundColor={colors.white}
                  color={colors.warmBlue}
                  loadingStatus={loading}
                />
              </TouchableOpacity>
            </View>

          </View>
        </ScrollView>
      </ImageBackground>

    );
  }
}
