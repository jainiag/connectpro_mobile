/* eslint-disable eqeqeq */
import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  changePasssafeViewStyle: {
    flex: 1,
    backgroundColor: colors.warmBlue,
  },
  backgroundStyle: {
    height: '100%',
    width: '100%',
  },
  mainContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.035,
  },
  subContainerStyle: {
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderBottomColor: colors.white,
    alignItems: 'center',
    marginHorizontal: globals.screenWidth * 0.05,
    marginTop: globals.screenHeight * 0.07,
    paddingBottom:
      Platform.OS === 'android' ? globals.screenHeight * 0.005 : globals.screenHeight * 0.015,
  },
  titleStyle: {
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 34 : globals.font_24,
  },
  currentPassTextInputStyle: {
    width: '70%',
    color: colors.white,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_16,
  },
  touchableViewStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: globals.screenHeight * 0.09,
  },
  backIconStyles: {
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.05
        : globals.screenHeight * 0.04,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenHeight * 0.05
        : globals.screenHeight * 0.04,
  },
  backViewstyle: {
    marginTop: globals.screenHeight * 0.11,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginLeft:globals.screenWidth * 0.05,
  },
  titleView: {
    marginTop: globals.screenHeight * 0.05,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
