/* eslint-disable react/no-unused-state */
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import globalStyles from '../../../assets/styles/globleStyles';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import MyProfile from './myProfile';
import CompanyProfile from './companyProfile';
import { ScrollableTabView, ScrollableTabBar } from '../../../libs/react-native-scrollable-tabview'

const TAG = '==:== Profile : ';
let _this;
const iPad = DeviceInfo.getModel();

class Profile extends React.Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    return {
      headerLeft: navigation.getParam('isfrom') === 'global' ?
        globals.ConnectProbackButton(navigation, 'Profile') :
        globals.ConnectProbackButton(navigation, "Profile"),
      headerStyle: globalStyles.ConnectPropheaderStyle,
    }

  };

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: true,
      isSearh: false,
      selectedTab: 1,
      index: 0,
      routes: [
        { key: 'MyProfile', title: 'My Profile' },
        { key: 'CompanyProfile', title: 'Company Profile' },
      ],
    };
  }

  render() {
    return <ScrollableTabView
      style={{ marginTop: (globals.iPhoneX || iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.01 }}
      initialPage={0}
      tabBarTextStyle={{ fontSize: globals.font_16 }}
      tabBarInactiveTextColor={colors.black}
      tabBarActiveTextColor={colors.warmBlue}
      renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
    >
      <MyProfile tabLabel={'My Profile'} navigationProps={this.props} />
      <CompanyProfile tabLabel={'Organization Details'} navigationProps={this.props} />
    </ScrollableTabView>
  }
}

// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
