/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, Linking, Alert, Platform } from 'react-native';
import styles from './style';
import * as globals from '../../../utils/globals';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeviceInfo from 'react-native-device-info';
import ImageLoad from 'react-native-image-placeholder';
import * as colors from '../../../assets/styles/color';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import * as images from '../../../assets/images/map';
import { bindActionCreators } from 'redux';
import { API } from '../../../utils/api';
import AsyncStorage from '@react-native-community/async-storage';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import BusinessCategory from './companyProfileTab/businessCategory';
import BusinessDescription from './companyProfileTab/businessDescription';
import RNFS from 'react-native-fs';
import loginScreen from '../AuthenticationScreens/loginScreen';
import { ScrollableTabView, ScrollableTabBar } from '../../../libs/react-native-scrollable-tabview'


let _this = null;
// let apiImage;
const TAG = '==:== CompanyProfile : ';
const iPad = DeviceInfo.getModel();
class CompanyProfile extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      loading: false,
      getCompanyProfile: [],
      businessCategoryData: [],
      comapnyProfilepicture: '',
      // sourceImage: apiImage,
      isLoading: false,
      isRequestFailed: false,
      groupCategoryData: [],
    }
  }


  static _refreshCompanyData() {
    AsyncStorage.multiGet([globals.LOGINUSERID, globals.ORGANIZATIONID]).then(token => {
      if (token[1][1] !== "null") {
        globals.userID = token[0][1];
        globals.organizationId = token[1][1];
      } else {
        globals.userID = token[0][1];
        globals.organizationId = 0;
      }
      if (globals.isInternetConnected === true) {
        API.getCompanyProfile(_this.getEventDetailsResponseData, true, globals.organizationId, globals.userID);
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  componentDidMount() {
    this.GetCompanyProfile()
  }


  
  /**
   * Api call of get Company Profile
   */
  GetCompanyProfile() {
    this.props.showLoader()
    this.setState({ loading: true })
    AsyncStorage.multiGet([globals.LOGINUSERID, globals.ORGANIZATIONID]).then(token => {
      if (token[1][1] !== "null") {
        globals.userID = token[0][1];
        globals.organizationId = token[1][1];
      } else {
        globals.userID = token[0][1];
        globals.organizationId = 0;
      }
      if (globals.isInternetConnected === true) {
        API.getCompanyProfile(this.getEventDetailsResponseData, true, globals.organizationId, globals.userID);
      } else {
        this.props.hideLoader()
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  /**
     * Response callback of getEventDetails
     */
  getEventDetailsResponseData = {
    success: response => {
      console.log(
        TAG,
        'getEventDetailsResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        const responseData = response.Data.CompanyDetails;
        const businessCategoryresponseData = response.Data.CompanyDetails.BusinessCategories;
        const bizGroups = response.Data.BusinessCategoriesList.BizGroups;
        this.setState({ getCompanyProfile: responseData, businessCategoryData: businessCategoryresponseData, groupCategoryData: bizGroups },()=>{
          BusinessDescription.setDescriptionData(this.state.getCompanyProfile, this.state.groupCategoryData);
          BusinessCategory.updatenewcategories(this.state.getCompanyProfile,this.state.getCompanyProfile)
          console.log("getCompanyProfile api --> ",this.state.getCompanyProfile)
        })
      }
      else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false })
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'getEventDetailsResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.props.hideLoader();
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          globals.appName,
          err.Message
        );
        this.setState({ isRequestFailed: true })
      }
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };


  /**
     * Method called when session expire
     */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigationProps.navigation.navigate('LoginScreen');
  }

  /**
     * method called when change Profile Picture
     */
  changeProfilePicture(res) {
    const data = {
      userid: globals.userID,
      ProfileImageFile: "",
      orgid: globals.organizationId,
      companylogo: 'data:image/png;base64,' + res
    }

    if (globals.isInternetConnected == true) {
      this.setState({ isLoading: true, loading: true })
      API.updateCompanyLogoFromProfile(this.updateCompanyLogoFromProfileResponseData, data, true, globals.userID);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
   * Response callback of updateCompanyLogoFromProfileResponseData
   */
  updateCompanyLogoFromProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'updateCompanyLogoFromProfileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ comapnyProfilepicture: response.Data, isLoading: false, loading: false }, () => {
          this.GetCompanyProfile();
        })
      }
      else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ isLoading: false, loading: false })
      }
    },
    error: err => {
      console.log(
        TAG,
        'updateCompanyLogoFromProfileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.props.hideLoader();
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          globals.appName,
          err.Message
        );
      }
      this.setState({ isLoading: false, loading: false })
    },
    complete: () => {
      this.setState({ isLoading: false, loading: false })
    },
  };

  /**
     * pickSingleImg from gallary
     */
  pickSingleImg() {
    ImagePicker.openPicker({
      width: globals.screenWidth * 0.25,
      height: globals.screenWidth * 0.25,
      borderRadius: (globals.screenWidth * 0.25) / 2,
      mediaType: 'photo',
      includeBase64: true,
    }).then(image => {
      this.setState({ isLoading: !this.state.isLoading }, () => {
        this.forceUpdate()
        this.convert(image.path);
      })

    }).catch((e) => {
      if (e.message !== "User cancelled image selection") {
        if (Platform.OS === "ios") {
          Alert.alert(
            globals.appName,
            e.message,
            [{ text: 'OK', onPress: () => Linking.openSettings() },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],

          );
        } else {
          console.log(e.message ? "ERROR" + e.message : "ERROR" + e);
        }
      } else {
        console.log(TAG, e.message);
      }
    });
  }

  /**
  * convert image to base64 formate
  */
  convert(path) {
    RNFS.readFile(path, 'base64')
      .then(res => {
        this.setState({ isLoading: true, loading: true })
        this.changeProfilePicture(res)
      });
  }


  /*
  * this method for Linkedin open in default browser
  */
  _opneLinkedin = (LinkedinURl) => {
    let URL = LinkedinURl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        return Linking.openURL(URL);
      } else {
        return Linking.openURL(URL)
      }
    });
  };


  /*
   * this method for Twitter open in default browser
   */
  _opneTwitter = (TwitterURl) => {
    let URL = TwitterURl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        Linking.openURL(URL);
      } else {
        Linking.openURL(URL)
      }
    });
  };



  /*
  * this method for _openFacebook open in default browser
  */
  _openFacebook = (FacebookURl) => {
    let URL = FacebookURl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        Linking.openURL(URL);
      } else {
        Linking.openURL(URL)
      }
    });
  };


  render() {
    const { getCompanyProfile, comapnyProfilepicture, isLoading } = this.state;
    const isCompanyLogoPath = globals.checkImageObject(getCompanyProfile, 'CompanyLogoPath');
    const isName = globals.checkObject(getCompanyProfile, 'Name');
    const isEmail = globals.checkObject(getCompanyProfile, 'Email');
    const isLinkedInUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'LinkedInUrl')
    const isFacebookUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'FacebookUrl')
    const isTwitterUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'TwitterUrl')
    const LinkedinURl = (isLinkedInUrl) ? getCompanyProfile.SocialLinksobj.LinkedInUrl : '';
    const FacebookURl = (isFacebookUrl) ? getCompanyProfile.SocialLinksobj.FacebookUrl : '';
    const TwitterURl = (isTwitterUrl) ? getCompanyProfile.SocialLinksobj.TwitterUrl : '';
    console.log("getCompanyProfile render --> ",this.state.getCompanyProfile)

    return (
      <View style={styles.mainContainer}>
        <View style={styles.profileViewContainer}>
          <View style={styles.imgAndIconViewContainer}>
            {
              (isLoading) ?
                <View style={styles.before_imageView}>
                  <ImageLoad
                    style={[styles.profile_ImageStyle,]}
                    isShowActivity={true}
                    placeholderSource={images.MyProfile.placeHolder}
                    borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 140 / 2 : (globals.screenWidth * 0.26) / 2}
                  />
                </View>
                :
                (isCompanyLogoPath) ?
                  <TouchableOpacity onPress={() => this.props.navigationProps.navigation.navigate("ViewCompanyProfilePicture", { sourceImage: comapnyProfilepicture, ProfileImage: getCompanyProfile.CompanyLogoPath })}>
                    <ImageLoad
                      source={{ uri: (this.state.comapnyProfilepicture == '' || this.state.comapnyProfilepicture == undefined) ? getCompanyProfile.CompanyLogoPath : this.state.comapnyProfilepicture }}
                      style={styles.profile_ImageStyle}
                      placeholderSource={images.MyProfile.placeHolder}
                      placeholderStyle={styles.profile_ImageStyle}
                      isShowActivity={true}
                      borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 140 / 2 : (globals.screenWidth * 0.26) / 2}
                    />
                  </TouchableOpacity>
                  :
                  <TouchableOpacity onPress={() => this.props.navigationProps.navigation.navigate("ViewCompanyProfilePicture", { placeHolder: images.MyProfile.placeHolder })}>
                    <ImageLoad
                      source={images.MyProfile.placeHolder}
                      style={styles.profile_ImageStyle}
                      placeholderSource={images.MyProfile.placeHolder}
                      placeholderStyle={styles.profile_ImageStyle}
                      isShowActivity={false}
                      borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 140 / 2 : (globals.screenWidth * 0.26) / 2}
                    />
                  </TouchableOpacity>
            }

            <TouchableOpacity style={styles.settingOpacityContainer}
              onPress={() => this.pickSingleImg()}>
              <AntDesign
                name="setting"
                size={
                  iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                    ? 30
                    : globals.screenWidth * 0.055
                }
                color={colors.white}
                style={styles.settingIconStyle}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.userNameAndEmailTextViewContainer}>
            <Text style={styles.userNameTextStyle} numberOfLines={1}>
              {(isName) ? getCompanyProfile.Name : '-'}
            </Text>
            <Text style={styles.userEmailTextStyle}>{(isEmail) ? getCompanyProfile.Email : '-'}</Text>
          </View>
        </View>
        <View style={styles.IconsViewStyle}>
          {(LinkedinURl == "") ? null
            :
            <View style={styles.linkedinAndTwitterIconViewStyle}>
              <TouchableOpacity onPress={() => this._opneLinkedin(LinkedinURl)}>
                <Image
                  source={images.MyProfile.linkedIn}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(FacebookURl == "") ? null
            :
            <View style={styles.fbIconStyle}>
              <TouchableOpacity onPress={() => this._openFacebook(FacebookURl)}>
                <Image
                  source={images.MyProfile.fbBlue}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(TwitterURl == "") ? null
            :
            <View style={styles.linkedinAndTwitterIconViewStyle}>
              <TouchableOpacity onPress={() => this._opneTwitter(TwitterURl)}>
                <Image
                  source={images.MyProfile.twitter}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }

        </View>
        {/* <TouchableOpacity style={styles.applyBtnViewStyle} onPress={() => this.props.navigationProps.navigation.navigate('EditLinkedinProfile',{getCompanyProfile:this.state.getCompanyProfile})}>
          <Text style={styles.applyTextStyle}>Apply LinkedIn Profile</Text>
        </TouchableOpacity> */}
        <ScrollableTabView
          initialPage={0}
          tabBarTextStyle={{ fontSize: globals.font_16 }}
          tabBarInactiveTextColor={colors.black}
          tabBarActiveTextColor={colors.warmBlue}
          renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
        >
          <BusinessDescription tabLabel={'Business Description'} navigationProps={this.props} isRequest={this.state.isRequestFailed} Org_ID={globals.organizationId} groupCategoryData={this.state.groupCategoryData} />
          <BusinessCategory tabLabel={'Business Categories'} navigationProps={this.props} groupCategoryData={this.state.groupCategoryData} businessDescriptionDataList={this.state.getCompanyProfile} />
        </ScrollableTabView>
      </View>
    );
  
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyProfile);
