/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  SafeAreaView,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import styles from './styles';
import * as colors from '../../../../assets/styles/color';

export default class EditBiusnessCategory extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Business Category'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      businessCategoryDataList: this.props.navigation.state.params.businessCategoryDataList,
      addTag: '',
    };
  }

  /**
      * Remove particulat Data
      */
  _addTags() {
    const { businessCategoryDataList, addTag } = this.state;
    const add = businessCategoryDataList.concat(addTag);
    if (this.state.addTag != '') {
      this.setState({ businessCategoryDataList: add, addTag: '' });
    }
  }

  render() {
    return (
      <SafeAreaView style={[styles.container, (globals.iPhoneX) ?
        { marginTop: 35 }
        :
        { paddingTop: 35 }]}>

        <ScrollView
          style={styles.scrollViewStyle}
          bounces={false}
          showsVerticalScrollIndicator={false}
        >
          <TagSelect
            isFrom="ProfileCategory"
            data={this.state.businessCategoryDataList}
            itemStyle={styles.item}
            itemLabelStyle={styles.label}
            itemStyleSelected={styles.itemSelected}
            itemLabelStyleSelected={styles.labelSelected}
          />
        </ScrollView>

        <View style={styles.textMainView}>
          <View style={styles.textInputView}>
            <TextInput
              value={this.state.addTag}
              autoCapitalize="none"
              onFocus={false}
              returnKeyType="done"
              onChangeText={text => this.setState({ addTag: text })}
              placeholderTextColor={colors.darkgrey}
              placeholder="Add Profile Categories like Mentor, Entrepreneur etc.."
              style={styles.textInputStyle}
            />
          </View>
        </View>
        <View
          style={styles.touchableViewStyle}
        >
          <TouchableOpacity
            onPress={() => this._addTags()}
            style={[styles.touchablebtnStyle, { backgroundColor: colors.warmBlue, }]}
          >
            <Text style={styles.canclebtnStyle}>Save</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => alert('Cancel')}
            style={[styles.touchablebtnStyle, { backgroundColor: colors.gray, }]}
          >
            <Text style={styles.canclebtnStyle}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    );
  }
}
