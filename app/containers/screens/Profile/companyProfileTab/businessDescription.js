/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import * as globals from '../../../../utils/globals';
import HTML from 'react-native-render-html';
import * as images from '../../../../assets/images/map';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import * as colors from '../../../../assets/styles/color';


let _this = null;
let businessDescriptionDataList = [];
let groupData = []

class BusinessDescription extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      finalData: [],
      isRequest: this.props.isRequest,
      groupAllData: [],
    }
  }

  /**
  * setDescriptionData called from above page tp get businessDescriptionData
  */
  static setDescriptionData(businessDescriptionData, groupDataCategory) {
    let tempData = [];
    let tempGroupData = [];
    _this && _this.setState({ finalData: businessDescriptionData, groupAllData: groupDataCategory })
    tempData = businessDescriptionData;
    businessDescriptionDataList = tempData;
    tempGroupData = groupDataCategory;
    groupData = tempGroupData;
  }



  render() {
    const { finalData, groupAllData } = this.state;
    const isPhone = globals.checkObject(finalData, 'Phone');
    const isBusinessDescription = globals.checkObject(finalData, 'BusinessDescription');
    const isEmail = globals.checkObject(finalData, 'Email');
    const isExtension = globals.checkObject(finalData, 'Extension');
    const isFax = globals.checkObject(finalData, 'Fax')
    const atNumber = (isPhone) ? finalData.Phone : '-';
    const finalPhone = (atNumber.charAt(0) == "+") ? atNumber : (isPhone) ? "+"+atNumber : "-"
    
    return (
      <View style={styles.container}>
        {
          (finalData != null ) ?
            <>
              <ScrollView bounces={false}>
                <View style={styles.mainView}>
                  <View style={styles.ViewStyle}>
                    <Image style={styles.imgStyle} source={images.MyProfile.emailBlack}>
                    </Image>
                    <Text style={styles.textStyle}>
                      {(isEmail) ? finalData.Email : '-'}
                    </Text>
                  </View>
                  <View style={styles.ViewStyle}>
                    <Image style={styles.imgStyle} source={images.MyProfile.phone}>
                    </Image>
                    <Text style={styles.textStyle}>
                      {finalPhone}
                    </Text>
                    {(isExtension) ?
                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.textStyle}>
                          {"Extension:"}
                        </Text>
                        <Text style={styles.textStyle}>
                          {(isExtension) ? finalData.Extension : '-'}
                        </Text>
                      </View>
                      :
                      null
                    }
                  </View>
                  <View style={styles.ViewStyle}>
                    <FontAwesome name="fax" size={globals.screenHeight * 0.03}/>
                    <Text style={styles.textStyle}>{(isFax) ? finalData.Fax : ''}</Text>
                  </View>
                  <View style={styles.descriptionViewStyle}>
                    <HTML html={isBusinessDescription ? finalData.BusinessDescription : '-'} baseFontStyle={{ fontSize: globals.font_15, color: colors.lightBlack }} />
                  </View>
                </View>
              </ScrollView>
                <TouchableOpacity style={styles.iconViewStyle}
                  onPress={() =>
                    this.props.navigationProps.navigationProps.navigation.navigate('EditcompanyProfile', {
                      businessDescriptionDataList: finalData,
                      groupDataCategory: groupAllData
                    })
                  }
                >
                  <Image
                    source={images.MyProfile.blueEdit}
                    style={styles.IconsStyle}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
            </>
            :
            <View style={styles.notAvialableViewStyle}>
            <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.BUSINESS_DESCRIPTION}</Text>
          </View>
      }

      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessDescription);