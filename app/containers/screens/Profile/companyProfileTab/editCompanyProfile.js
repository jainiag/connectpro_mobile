/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import styles from './styles';
import * as colors from '../../../../assets/styles/color';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Validation from '../../../../utils/validation';
import CompanyProfile from '../companyProfile';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import CountryPicker, { getAllCountries } from '../../../../libs/react-native-country-picker-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import SectionedMultiSelect from '../../../../libs/react-native-sectioned-multi-select';
import MyProfile from '../myProfile';

let _this = null;
let arrCategory;
let arrCategoryName = [];
let arrCategoryID = [];

const TAG = '==:== EditCompanyProfile : ';
const iPad = DeviceInfo.getModel();
class EditCompanyProfile extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Edit Organization details'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    const userLocaleCountryCode = DeviceInfo.getDeviceCountry();
    const userCountryData = getAllCountries()
      .filter(country => globals.COUNTRY_LIST.includes(country.cca2))
      .filter(country => country.cca2 === userLocaleCountryCode)
      .pop();
    let callingCode = null;
    let cca2 = userLocaleCountryCode;
    if (!cca2 || !userCountryData) {
      cca2 = 'IN';
      callingCode = '91';
    } else {
      cca2 = 'IN';
      callingCode = '91';
    }

    const phoneNumber = this.props.navigation.state.params.businessDescriptionDataList.Phone
    const lengthBase = (phoneNumber == null) ? 0 : phoneNumber.length
    const iscode = (phoneNumber == null) ? +91 : (lengthBase < 13) ? phoneNumber.slice(0,2) : (lengthBase > 13) ? phoneNumber.slice(0,3) : phoneNumber.slice(0,2)
    const checkCode = (phoneNumber == null) ? +91 : iscode.charAt(0);
    const newCode = iscode
    const cntryCode = (phoneNumber !== null ) ? (lengthBase == 12) ? phoneNumber.split(phoneNumber.slice(-11))[0] :  (lengthBase == 8) ? phoneNumber.split(phoneNumber.slice(-8))[0] : phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (phoneNumber !== null ) ? (phoneNumber.length === 10) ? phoneNumber : (lengthBase == 12) ? phoneNumber.slice(2): (lengthBase == 8) ? phoneNumber.slice(1) : phoneNumber.slice(3) : ""
    const getNumber = (phoneNumber !== null) ? (phoneNumber.length === 10) ? phoneNumber : isNumber.replace(/ /g, '') : "";
    const finalCode = (checkCode == "+") ? iscode.substring(1) : iscode

    this.state = {
      businessCategoryData: this.props.navigation.state.params.businessCategoryData,
      businessDataList: this.props.navigation.state.params.businessDescriptionDataList,
      fullName: '',
      email: '',
      fax: '',
      professionalSummary: '',
      Phone: '',
      Ext: '',
      business_category: '',
      LinkedIn: '',
      Facebook: '',
      Twitter: '',
      loading: false,
      updatedData: [],
      cca2: cca2,
      callingCode: finalCode,
      mobileNum: '',
      validMob: false,
      isLoading: false,
      groupCategoryData: [],
      tempBizGroupData: [],
      businessCategoryIDData: [],
    };
  }

  componentDidMount(){
    let {groupCategoryData,tempBizGroupData} = this.state;
    var tempBizGroupDataCopy;
    const phoneNumber = this.props.navigation.state.params.businessDescriptionDataList.Phone
    const lengthBase = (phoneNumber == null) ? 0 : phoneNumber.length
    const iscode = (phoneNumber == null) ? +91 : (lengthBase == 11 || lengthBase == 10 ) ? phoneNumber.slice(0,3) : (lengthBase < 13) ? phoneNumber.slice(0,2) : (lengthBase > 13) ? phoneNumber.slice(0,3) : phoneNumber.slice(0,2)
    const checkCode = (phoneNumber == null) ? +91 : iscode.charAt(0);
    const newCode = iscode
    const cntryCode = (phoneNumber !== null ) ? (lengthBase == 12) ? phoneNumber.split(phoneNumber.slice(-11))[0] : (lengthBase == 11 || lengthBase == 10) ? phoneNumber.split(phoneNumber.slice(-10))[0] : (lengthBase == 8) ? phoneNumber.split(phoneNumber.slice(-8))[0] : phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (phoneNumber !== null ) ? (phoneNumber.length === 10) ? phoneNumber.slice(3) : (phoneNumber.length === 11) ? phoneNumber.slice(3) : (lengthBase == 12) ? phoneNumber.slice(2): (lengthBase == 8) ? phoneNumber.slice(1) : phoneNumber.slice(3) : ""
    const getNumber = (phoneNumber !== null) ? (phoneNumber.length === 10) ? phoneNumber.slice(3) : (phoneNumber.length === 11) ? phoneNumber.slice(3) : isNumber.replace(/ /g, '') : "";
    const finalCode = (checkCode == "+") ? iscode.substring(1) : iscode
   
    this.setState({
      businessCategoryData: this.props.navigation.state.params.businessCategoryData,
      businessDataList: this.props.navigation.state.params.businessDescriptionDataList,
      fullName: this.props.navigation.state.params.businessDescriptionDataList.Name,
      email: this.props.navigation.state.params.businessDescriptionDataList.Email,
      fax: this.props.navigation.state.params.businessDescriptionDataList.Fax,
      professionalSummary: '',
      Phone: (phoneNumber !== null) ? getNumber : '',
      Ext: this.props.navigation.state.params.businessDescriptionDataList.Extension,
      business_category: '',
      LinkedIn: this.props.navigation.state.params.businessDescriptionDataList.SocialLinksobj.LinkedInUrl,
      Facebook: this.props.navigation.state.params.businessDescriptionDataList.SocialLinksobj.FacebookUrl,
      Twitter: this.props.navigation.state.params.businessDescriptionDataList.SocialLinksobj.TwitterUrl,
      loading: false,
      updatedData: [],
      callingCode: finalCode,
      mobileNum: '',
      validMob: false,
      isLoading: false,
      groupCategoryData: this.props.navigation.state.params.groupDataCategory,
      tempBizGroupData: [],
      businessCategoryIDData: [],
    },()=>{
        let businessDataList=this.state.businessDataList
        arrCategory = businessDataList.BusinessCategories
        console.log("didmount arrCategory -->",arrCategory)
        arrCategoryID=[]
        for (i = 0; i < arrCategory.length; i++) {
          // var categoryID = arrCategory[i].ID
          //  arrCategoryID.push(categoryID)
          // console.log("concatedId-->",arrCategoryID)

          // tempBizGroupDataCopy = arrCategoryID.concat(tempBizGroupData);
          // console.log("tempBizGroupDataCopy-->",tempBizGroupDataCopy)
          // tempBizGroupDataCopy = tempBizGroupDataCopy.filter( function( item, index, inputArray ) {
          //   return inputArray.indexOf(item) == index;
          // });
          var selectedcategoryID = arrCategory[i].ID
          arrCategoryID.push(selectedcategoryID)
          arrCategoryID = arrCategoryID.filter( function( item, index, inputArray ) {
            return inputArray.indexOf(item) == index;
          });
          this.setState({tempBizGroupData:arrCategoryID},()=>{
            console.log("didmount tempBizGroupData -->",this.state.tempBizGroupData)

          })

        }
     
    })
  }

  componentWillUnmount() {
    CompanyProfile._refreshCompanyData();
    this.setState({
      businessCategoryData: [],
      businessDataList: [],
      fullName: '',
      email: '',
      fax: '',
      professionalSummary: '',
      Phone: '',
      Ext: '',
      business_category: '',
      LinkedIn: '',
      Facebook: '',
      Twitter: '',
      updatedData: [],
      mobileNum: '',
      groupCategoryData: [],
      tempBizGroupData: [],
      businessCategoryIDData: [],
    })
  }

  /**
   * API call of updateCompanyProfile
   */
  makeApiCall() {
    const { businessDataList, Phone, tempBizGroupData, businessCategoryIDData, fullName, fax, email, LinkedIn, Facebook, Twitter, professionalSummary, Ext } = this.state;
    this.props.showLoader();
    this.setState({ loading: true })
    const checkPhoneNo = businessDataList.Phone;
    const isPhone = globals.checkObject(businessDataList, 'Phone');
    const phoneNumber = (isPhone) ? (checkPhoneNo !== null) ? businessDataList.Phone : Phone : Phone;
    const cntryCode = (checkPhoneNo !== null) ? phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (checkPhoneNo !== null) ? (phoneNumber.length === 10) ? phoneNumber : phoneNumber.substring(4) : "";
    const getNumber = (checkPhoneNo !== null) ? "" + this.state.callingCode +" "+ this.state.Phone : this.state.Phone;
    const isCode_Number = (this.state.callingCode == "") ? "91"+getNumber : getNumber

    for (i = 0; i < arrCategory.length; i++) {
      var categoryName = arrCategory[i].CategoryName
      var categoryID = arrCategory[i].ID
      var categoryDescription = arrCategory[i].Description
      var categoryParentID = arrCategory[i].ParentID
      var categoryCreatedDate = arrCategory[i].CreatedDate
      var categoryModifiedDate = arrCategory[i].ModifiedDate
      var categoryModifiedBy = arrCategory[i].ModifiedBy
      var categoryCreatedBy = arrCategory[i].CreatedBy
      var categoryIsActive = arrCategory[i].IsActive

    }
    const data = {
      companyPoco: {
        ID: businessDataList.ID,
        Name: fullName,
        BusinessCategoryIDs: businessDataList.BusinessCategoryIDs,
        BusinessCategoryNames: businessDataList.BusinessCategoryNames,
        NumberOfEmployeesID: businessDataList.NumberOfEmployeesID,
        NoOfEmployeesText: businessDataList.NoOfEmployeesText,
        BusinessDescription: businessDataList.BusinessDescription,
        Website: businessDataList.Website,
        Phone: isCode_Number,
        Fax: fax,
        CompanyLogoPath: businessDataList.CompanyLogoPath,
        CurrencyID: businessDataList.CurrencyID,
        ReceiverProfile: businessDataList.ReceiverOrganization,
        CreatedDate: businessDataList.CreatedDate,
        StartDate: businessDataList.StartDate,
        EndDate: businessDataList.EndDate,
        LicenceStartDate: businessDataList.LicenceStartDate,
        LicenceEndDate: businessDataList.LicenceEndDate,
        CreatedBy: globals.userID,
        ModifiedDate: categoryModifiedDate,
        ModifiedBy: globals.userID,
        MembershipPlan: businessDataList.MembershipPlan,
        IsActive: businessDataList.IsActive,
        BusinessCategories: [
          {
            ID: categoryID,
            CategoryName: categoryName,
            Description: categoryDescription,
            ParentID: categoryParentID,
            CreatedDate: categoryCreatedDate,
            CreatedBy: categoryCreatedBy,
            ModifiedDate: categoryModifiedDate,
            ModifiedBy: categoryModifiedBy,
            IsActive: categoryIsActive
          }
        ],
        EntityStatusID: businessDataList.EntityStatusID,
        StatusText: businessDataList.StatusText,
        Extension: Ext,
        SocialLinks: businessDataList.SocialLinks,
        SocialLinksobj: {
          LinkedInUrl: LinkedIn,
          FacebookUrl: Facebook,
          TwitterUrl: Twitter,
          GooglePlusUrl: businessDataList.SocialLinksobj.GooglePlusUrl
        },
        Email: email,
        ShowRenewalLink: businessDataList.ShowRenewalLink,
        ValidLicense: businessDataList.ValidLicense,
        RejectReason: businessDataList.RejectReason,
        SubmitterNote: businessDataList.SubmitterNote,
        EmailVerificationStatus: businessDataList.EmailVerificationStatus,
        RowIndex: businessDataList.RowIndex,
        DT_RowId: businessDataList.DT_RowId,
        DT_RowClass: businessDataList.DT_RowClass,
        DT_RowData: businessDataList.DT_RowData,
      },
      businessCategoryIDs: businessCategoryIDData
    }
    if (globals.isInternetConnected == true) {
      API.updateCompanyProfile(this.updateCompanyProfileResponseData, data, true, globals.userID);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      this.props.hideLoader();
      this.setState({ loading: false })
    }
  }

  /**
    * Response callback of updateCompanyProfileResponseData
    */
  updateCompanyProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'updateCompanyProfileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        MyProfile.refreshMyProfile();
        CompanyProfile._refreshCompanyData();
        Alert.alert(globals.appName, response.Message,
          [{ text: 'OK', onPress: () => {this.props.showLoader(); this.props.navigation.goBack()}}],
          { cancelable: false }
      );
      }
      else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message)
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'updateCompanyProfileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      Alert.alert(globals.appName, err.Message)
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
    * method for _validation For EditCompany Profile
    */
  _validationForEditCompanyProfile() {
    const { fullName, email, Phone, fax } = this.state;
    const phoneLength = Phone.length;

    if (Validation.textInputCheck(fullName) && fullName !== "") {
      if (Validation.isValidEmail(email) || (email === "") || (email == null)) {
        if (Validation.connectProValidMobileNub(phoneLength)|| (Phone == "") || (Phone == null)) {
          this._urlValidation();
        } else {
          Alert.alert(globals.appName, "The phone number is not valid");
        }
      } else {
        Alert.alert(globals.appName, "Please enter valid Email Address");
      }
    } else {
      Alert.alert(globals.appName, 'Please enter Organization name');
    }
  }

  _urlValidation(){
    const { LinkedIn, Facebook, Twitter } = this.state;
    if ((LinkedIn == "") || (LinkedIn == null) || Validation.linkedInUrlValidation(LinkedIn)) {
      if ((Facebook == "") || (Facebook == null) || Validation.fbUrlValidation(Facebook)) {
        if ((Twitter == "") || (Twitter == null) || Validation.twitterUrlValidation(Twitter)) {
          this.makeApiCall();
        } else {
          Alert.alert(globals.appName, "Please enter valid Twitter URL")
        }
      } else {
        Alert.alert(globals.appName, "Please enter valid Facebook URL")
      }
    } else {
      Alert.alert(globals.appName, "Please enter valid LinkedIn URL")
    }
  }

  _cancelOnClick() {
    CompanyProfile._refreshCompanyData()
    this.props.navigation.goBack()
  }

  fullNameChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ fullName: text, }, () => {
      user_data.Name = this.state.fullName,
        this.setState({
          businessDataList: user_data
        });
    })
  }
  emailChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ email: text, }, () => {
      user_data.Email = this.state.email,
        this.setState({
          businessDataList: user_data
        });
    })
  }

  professionalSummaryChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ professionalSummary: text, }, () => {
      user_data.BusinessDescription = this.state.professionalSummary,
        this.setState({
          businessDataList: user_data
        });
    })
  }
  phoneChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ Phone: text, }, () => {
      user_data.Phone = this.state.Phone,
        this.setState({
          businessDataList: user_data
        });
    })
  }
  extensionChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ Ext: text.replace(/[^0-9]/g, ''), }, () => {
      user_data.Extension = this.state.Ext,
        this.setState({
          businessDataList: user_data
        });
    })
  }

  linkedInChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ LinkedIn: text, }, () => {
      user_data.SocialLinksobj.LinkedInUrl = this.state.LinkedIn,
        this.setState({
          businessDataList: user_data
        });
    })
  }
  facebookChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ Facebook: text, }, () => {
      user_data.SocialLinksobj.FacebookUrl = this.state.Facebook,
        this.setState({
          businessDataList: user_data
        });
    })
  }
  faxChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ fax: text.replace(/[^0-9]/g, ''),}, () => {
      user_data.Fax = this.state.fax,
        this.setState({
          businessDataList: user_data
        });
    })
  }
  twitterChange(text) {
    let user_data = this.state.businessDataList;
    this.setState({ Twitter: text, }, () => {
      user_data.SocialLinksobj.TwitterUrl = this.state.Twitter,
        this.setState({
          businessDataList: user_data
        });
    })
  }

  onSelectedItemsChange = (tempBizGroupData) => {

    if (tempBizGroupData.length < 6) {
      let data = [];
      tempBizGroupData.map((i) => {
        data.push(JSON.stringify(i))
      })
      this.setState({ tempBizGroupData });
      this.setState({ businessCategoryIDData: data })
    } else {
      Alert.alert(globals.appName, "You can only select 5 items")
    }
  };

  getCode(value){
    let CodeArray = globals.CountryCodeArray
    for(let i = 0; i < CodeArray.length; i++){
      let name = CodeArray[i].name
      if(name == value.name){
        const finalValue = (CodeArray[i].dial_code) ? CodeArray[i].dial_code : '69';
        this.setState({ cca2: value.cca2, callingCode: (!value.callingCode) ? finalValue : value.callingCode });  
      }
    }
  }

  render() {
    const { businessDataList, LinkedIn, Facebook, Twitter, fax, fullName, email, professionalSummary, Phone, Ext, cca2, callingCode, country } = this.state;
    let {groupCategoryData,tempBizGroupData} = this.state;
    const isFullName = globals.checkObject(businessDataList, 'Name');
    const isEmail = globals.checkObject(businessDataList, 'Email');
    const isPhone = globals.checkObject(businessDataList, 'Phone');
    const isExtension = globals.checkObject(businessDataList, 'Extension');
    const isFax = globals.checkObject(businessDataList, 'Fax');
    const isBusinessCategories = globals.checkObject(businessDataList, 'BusinessCategories');
    const isCategoryName = (isBusinessCategories) ? globals.checkObject(businessDataList.isBusinessCategories, 'CategoryName') : '-';
    const isBusinessDescription = globals.checkObject(businessDataList, 'BusinessDescription');
    const isFacebookUrl = globals.checkObject(businessDataList.SocialLinksobj, 'FacebookUrl');
    const isLinkedInUrl = globals.checkObject(businessDataList.SocialLinksobj, 'LinkedInUrl');
    const isTwitterUrl = globals.checkObject(businessDataList.SocialLinksobj, 'TwitterUrl');
    const checkPhoneNo = businessDataList.Phone;
    const phoneNumber = (isPhone) ? (checkPhoneNo !== null) ? businessDataList.Phone : Phone : Phone;
    const cntryCode = (checkPhoneNo !== null) ? phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (checkPhoneNo !== null) ? (phoneNumber.length === 10) ? phoneNumber : phoneNumber.substring(4) : Phone;
    const getNumber = (checkPhoneNo !== null) ? (phoneNumber.length === 10) ? phoneNumber : isNumber.replace(/ /g, '') : Phone;
    const getMyNumber = (checkPhoneNo !== null) ? "+" + this.state.callingCode + " " + this.state.Phone : this.state.Phone
    const isCallingCode = this.state.callingCode;
    if (isBusinessCategories) {
      arrCategory = businessDataList.BusinessCategories
      for (i = 0; i < arrCategory.length; i++) {
        var categoryName = arrCategory[i].CategoryName
        let object = {
          "value": categoryName
        }
        arrCategoryName.push(object);
        var categoryID = arrCategory[i].ID
       console.log("rendertempBizGroupData-->",tempBizGroupData)
      }
    }

    return (
      <View style={styles.ep_container}>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }}>
            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Name</Text>
                <TextInput
                  style={styles.ep_contentText}
                  maxLength={70}
                  placeholder="Organization Name"
                  placeholderTextColor={colors.lightGray}
                  onChangeText={(text) => this.fullNameChange(text)}
                  returnKeyType="next"
                  value={(isFullName) ? businessDataList.Name : fullName}
                  onSubmitEditing={() => this.EmailRef.focus()}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>
            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Email</Text>
                <TextInput
                  style={styles.ep_contentText}
                  placeholder="Email"
                  placeholderTextColor={colors.lightGray}
                  onChangeText={(text) => this.emailChange(text)}
                  returnKeyType="next"
                  value={(isEmail) ? businessDataList.Email : email}
                  ref={EmailRef => (this.EmailRef = EmailRef)}
                  onSubmitEditing={() => this.PhoneRef.focus()}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>
            <View style={[{ flexDirection: 'row' }]}>
              <View style={[styles.ep_mainView, { width: '50%', paddingRight: globals.screenWidth * 0.04 }]}>
                <Text style={styles.ep_titleStyle}>Phone</Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                  <CountryPicker
                    isNumber={isCallingCode}
                    isFrom={'editCompanyPrfoile'}
                    PhoneNumber={Phone}
                    countryList={globals.COUNTRY_LIST}
                    onChange={value => {
                      this.getCode(value)
                    }}
                    cca2={cca2}
                    translation="eng"
                  />
                  <TextInput
                    style={[styles.ep_contentText2,{paddingBottom:globals.screenHeight * 0.0129}]}
                    placeholderTextColor={colors.lightGray}
                    placeholder="Phone Number"
                    onChangeText={(text) => this.phoneChange(text)}
                    returnKeyType="done"
                    keyboardType="number-pad"
                    maxLength={15}
                    value={Phone}
                    ref={PhoneRef => (this.PhoneRef = PhoneRef)}
                    onSubmitEditing={() => this.ExtRef.focus()}
                    autoCapitalize="none"></TextInput>
                </View>
              </View>
              <View style={[styles.ep_spacebetweeenView, { width: '50%' }]}>
                <Text style={styles.ep_spacebetweenTitleStyle}>Extension</Text>
                <TextInput
                  style={[styles.ep_spacebetweenContentStyle, { width: '70%' }]}
                  placeholderTextColor={colors.lightGray}
                  placeholder="Extension"
                  onChangeText={(text) => this.extensionChange(text)}
                  returnKeyType="done"
                  keyboardType={"numeric"}
                  maxLength={4}
                  value={(isExtension) ? businessDataList.Extension : Ext}
                  ref={ExtRef => (this.ExtRef = ExtRef)}
                  onSubmitEditing={() => this.FaxRef.focus()}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>
            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Fax</Text>
                <TextInput
                  style={styles.ep_contentText}
                  placeholderTextColor={colors.lightGray}
                  placeholder="Fax"
                  onChangeText={(text) => this.faxChange(text)}
                  returnKeyType="done"
                  keyboardType={"numeric"}
                  maxLength={15}
                  value={(isFax) ? businessDataList.Fax : fax}
                  ref={FaxRef => (this.FaxRef = FaxRef)}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>

            <View style={styles.ep_mainView}>
              <SectionedMultiSelect
                items={groupCategoryData}
                uniqueKey="ID"
                subKey="BusinessCategories"
                selectText="Select Business Categories with max limit of 5"
                showDropDowns={true}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedItemsChange}
                selectedItems={tempBizGroupData}
                displayKey="GroupName"
                subDisplayKey="Name"
                isfrom={"editOrganizationDetails"}
                showCancelButton={false}
                hideConfirm={false}
              />
            </View>
            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Business Description</Text>
                <TextInput style={styles.ep_contentText}
                  multiline={true}
                  maxLength={300}
                  style={styles.ep_contentText}
                  placeholderTextColor={colors.lightGray}
                  placeholder="Business description"
                  onChangeText={(text) => this.professionalSummaryChange(text)}
                  returnKeyType="default"
                  value={(isBusinessDescription) ? businessDataList.BusinessDescription : professionalSummary}
                  ref={ProfessionalSummaryRef => (this.ProfessionalSummaryRef = ProfessionalSummaryRef)}
                  autoCapitalize="none">

                </TextInput>
              </View>
            </View>


            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Linkedin Profile</Text>
                <TextInput
                  style={styles.ep_contentText}
                  placeholderTextColor={colors.lightGray}
                  placeholder="https://www.linkedin.com/profile/"
                  onChangeText={(text) => this.linkedInChange(text)}
                  returnKeyType="next"
                  value={(isLinkedInUrl) ? businessDataList.SocialLinksobj.LinkedInUrl : LinkedIn}
                  onSubmitEditing={() => this.FacebookRef.focus()}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>
            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Facebook Profile</Text>
                <TextInput
                  style={styles.ep_contentText}
                  placeholderTextColor={colors.lightGray}
                  placeholder="https://www.facebook.com/profile/"
                  onChangeText={(text) => this.facebookChange(text)}
                  returnKeyType="next"
                  value={(isFacebookUrl) ? businessDataList.SocialLinksobj.FacebookUrl : Facebook}
                  ref={FacebookRef => (this.FacebookRef = FacebookRef)}
                  onSubmitEditing={() => this.TwitterRef.focus()}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>
            <View style={styles.ep_mainView}>
              <View>
                <Text style={styles.ep_titleStyle}>Twitter</Text>
                <TextInput
                  style={styles.ep_contentText}
                  placeholderTextColor={colors.lightGray}
                  placeholder="https://www.twitter.com/profile/"
                  onChangeText={(text) => this.twitterChange(text)}
                  returnKeyType="done"
                  value={(isTwitterUrl) ? businessDataList.SocialLinksobj.TwitterUrl : Twitter}
                  ref={TwitterRef => (this.TwitterRef = TwitterRef)}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>


            <View style={styles.ep_buttonStyle}>
              <TouchableOpacity style={[styles.touchableStyle, { backgroundColor: colors.warmBlue }]} onPress={() => this._validationForEditCompanyProfile()}>
                <View>
                  <Text style={[styles.btntextStyless, { color: colors.white }]}>{globals.BTNTEXT.PAYMENTSCREENS.SAVE}</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this._cancelOnClick()}
                style={[styles.touchableStyle, { marginLeft: globals.screenWidth * 0.02, backgroundColor: colors.gray }]}
              >
                <View>
                  <Text style={styles.btntextStyless}>{globals.BTNTEXT.PAYMENTSCREENS.CANCLE}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>
        </ScrollView>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditCompanyProfile);