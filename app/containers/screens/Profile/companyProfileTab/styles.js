import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();
module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    marginTop: 15,
    // marginLeft: 15,
  },
  ViewStyle: {
    flexDirection: 'row',
    marginBottom: globals.screenHeight * 0.02
  },
  buttonContainer: {
    padding: 15,
  },
  textStyle: {
    marginLeft: globals.screenWidth * 0.022,
    fontSize: globals.font_14,
    color: colors.lightBlack,
    textAlign: 'center',
    alignSelf: 'center'
  },
  descriptionViewStyle: {
    width: "90%",
    //backgroundColor:"red",
    // marginHorizontal:globals.screenHeight * 0.01,
    marginVertical: globals.screenHeight * 0.015,

  },
  iconStyle: {
    width: 27,
    height: 27,
    backgroundColor: colors.darkSkyBlue,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    marginBottom: 15,
  },
  mainView: {
    marginLeft: globals.screenWidth * 0.04,
  },
  iconViewStyle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.025,
    marginRight: globals.screenWidth * 0.03,
  },
  iconimgStyle: {
    height: 22, width: 22, tintColor: colors.white
  },
  imgStyle: {
    height: globals.screenHeight * 0.03,
    width: globals.screenHeight * 0.03
  },
  buttonInner: {
    marginBottom: 15,
  },
  labelText: {
    // color: '#333',
    fontSize: 15,
    fontWeight: '500',
    marginBottom: 15,
  },
  item: {
    // borderWidth: 1,
    // borderColor: '#333',
    backgroundColor: 'lightgrey',
  },
  label: {
    // color: '#333',
  },
  itemSelected: {
    // backgroundColor: '#333',
  },
  labelSelected: {
    // color: '#FFF',
  },
  canclebtnStyle: {
    color: colors.white, fontSize: globals.font_16
  },
  touchablebtnStyle: {
   
    height: 30,
    width: 80,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 7,
  },
  touchableViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: globals.iPhoneX ? 0 : 15,
  },
  textInputStyle: {
    paddingBottom: globals.screenHeight * 0.015,
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.04,
    color: colors.black,
    fontSize: globals.font_13,
  },
  textInputView:{
    width: '100%', borderWidth: 0.5, borderColor: colors.proUnderline
  },
  textMainView:{
    justifyContent: 'flex-end', alignItems: 'flex-start'
  },
  scrollViewStyle:{
    flex: 1, marginLeft: globals.screenWidth * 0.04
  },
  settingStyle:{
    height: globals.screenHeight * 0.035,
    width: globals.screenHeight * 0.035
  },
  notAvialableTextStyle: {
    fontSize: globals.font_14,
    textAlign: 'center'
  },
  notAvialableViewStyle: {
    justifyContent: 'center', 
    alignItems: 'center', 
    marginTop: globals.screenHeight * 0.09
  },
  EditButtonStyle: {
    justifyContent: 'flex-end', 
    alignItems: 'flex-end', 
    marginRight: globals.screenWidth * 0.03,
  },
  IconsStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08, 
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
  },
  /////// edit profile //////////
  ep_container: {
    flex: 1,
    marginTop: globals.screenHeight * 0.018,
  },
  ep_mainView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  ep_spacebetweeenView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
    width: globals.screenWidth,
    marginLeft: globals.screenWidth * 0.04,
  },
  ep_spacebetweenTitleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.darkgrey,
  },
  ep_spacebetweenContentStyle: {
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.0065,
    paddingBottom: globals.screenHeight * 0.01,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
  },
  ep_titleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.darkgrey,
    marginLeft: globals.screenWidth * 0.05,
  },
  ep_contentText: {
    marginTop: (Platform.OS === 'ios') ?  globals.screenHeight * 0.0065 : 0,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginHorizontal: globals.screenWidth * 0.05,
  },
  ep_contentText2: {
    marginTop: (Platform.OS === 'ios') ?  globals.screenHeight * 0.0065 : 0,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginHorizontal: globals.screenWidth * 0.015,
  },
  roundViewStyle: {
    marginTop: globals.screenHeight * 0.0065,
    height: globals.screenHeight * 0.025,
    width: globals.screenHeight * 0.025,
    borderColor: colors.gray,
    borderWidth: 1,
    marginLeft: globals.screenWidth * 0.05,
    borderRadius: (globals.screenHeight * 0.025) / 2,
  },
  directionView: {
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.001,
  },
  ep_textStyle: {
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.0065,
    // marginLeft: globals.screenWidth * 0.015,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 19 : globals.font_14,
  },
  squareView: {
    marginTop: globals.screenHeight * 0.0065,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  ep_buttonStyle: {
    marginTop: globals.screenHeight * 0.015,
    marginHorizontal: globals.screenWidth * 0.25,
    // margin:25,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.03,
  },
  btntextStyless: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    //  color:colors.white
  },
  bottomStyle: {
    paddingBottom: globals.screenHeight * 0.01,
  },
  buttonsView: {
    marginTop: globals.screenHeight * 0.01,
  },
  touchableStyle: {
    paddingHorizontal: 22,
    paddingVertical: 5,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  scrollStyle:{
    flex:1
  },
  viewcontainer:{
    height:globals.screenHeight * 0.20,
    width:globals.screenWidth,
    alignItems:'center',
    justifyContent:'center',
    
  },
  beforeimgviewStyle:{
    alignSelf:'center',
    width:globals.screenWidth * 0.25,
    height:globals.screenWidth * 0.25,
    borderColor:colors.lightgray,
    borderWidth:0.2,
    borderRadius:(globals.screenWidth * 0.25) / 2
  },
  editimgStyle:{
    alignSelf:'center',
    width:globals.screenWidth * 0.25,
    height:globals.screenWidth * 0.25,
    borderColor:colors.white,
    borderWidth:0.2,
    borderRadius:(globals.screenWidth * 0.25) / 2
  }
});
