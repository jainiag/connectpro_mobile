/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import { Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import styles from './styles';
import { connect } from 'react-redux';
import * as globals from '../../../../utils/globals';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import * as images from '../../../../assets/images/map';
import globalStyles from '../../../../assets/styles/globleStyles';

let _this = null;
class BusinessCategory extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      businessCategoryData: this.props.businessDescriptionDataList,
      renderData: this.props.businessDescriptionDataList.BusinessCategories,
      groupCategoryData: this.props.groupCategoryData,
      categoryArrar: [],

    }
  }


  static updatenewcategories(companyProfiledata, newdata) {
   _this && _this.setState({businessCategoryData: companyProfiledata},()=>{
      let updateddata = newdata.BusinessCategories;
      if (updateddata == '' || updateddata == []) {
      } else {
        _this && _this.setState({ renderData: updateddata }, () => {
          console.log("business categories updatenewcategories===>", _this.state.renderData)
          let arrTempUpdateCat = [];
          let mainDataUpdateCat = _this.state.renderData
          for (let i = 0; i < mainDataUpdateCat.length; i++) {
            var CategoryName = mainDataUpdateCat[i].CategoryName
            let obj = {
              Name: CategoryName
            };
            arrTempUpdateCat.push(obj);
            _this.setState({ categoryArrar: arrTempUpdateCat })
         }
        })
      }
    })
  }

  componentDidMount() {
    let arrTemp = [];
    let mainData = this.state.renderData
    for (let i = 0; i < mainData.length; i++) {
      var CategoryName = mainData[i].CategoryName
      let obj = {
        Name: CategoryName
      };
      arrTemp.push(obj);
      this.setState({ categoryArrar: arrTemp })
    }
  }

  render() {
    const { businessCategoryData, categoryArrar,groupCategoryData } = this.state;
    console.log("business categories render===>",businessCategoryData)
    return (
      <View style={styles.container}>
        {
          (categoryArrar != null && categoryArrar.length > 0) ?
            <>
              <ScrollView bounces={false}>
                <View style={styles.mainView}>
                  <TagSelect
                    isFrom="Business_Categories"
                    data={categoryArrar}
                    itemStyle={styles.item}
                    itemLabelStyle={styles.label}
                    itemStyleSelected={styles.itemSelected}
                    itemLabelStyleSelected={styles.labelSelected}
                  />
                </View>
              </ScrollView>
              <TouchableOpacity style={styles.iconViewStyle}
                onPress={() =>
                  this.props.navigationProps.navigationProps.navigation.navigate('EditcompanyProfile', {
                    businessDescriptionDataList: businessCategoryData,
                    groupDataCategory: groupCategoryData
                  })
                }
              >
                <Image
                  source={images.MyProfile.blueEdit}
                  style={styles.IconsStyle}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </>
            :
            <View style={globalStyles.nodataStyle}>
              <Text style={[globalStyles.nodataTextStyle]}>{globals.ERROR_MESSAGE.MY_PROFILE.BUSINESS_CATEGORY}</Text>
            </View>
        }

      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BusinessCategory);