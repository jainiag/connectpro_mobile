import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  ScrollView,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import styles from './styles';
import * as colors from '../../../../assets/styles/color';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import { bindActionCreators } from 'redux';
import ImagePicker from 'react-native-image-crop-picker';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';

let _this = null;
let apiImage;

const TAG = '==:== LinkedinProfile : ';
const iPad = DeviceInfo.getModel();
class EditLinkedinProfile extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Linkedin Profile'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      getCompanyProfile: this.props.navigation.state.params.getCompanyProfile,
      fullName: '',
      email: '',
      jobtitle: '',
      fax: '',
      professionalSummary: '',
      Phone: '',
      Ext: '',
      business_category: '',
      LinkedIn: '',
      Facebook: '',
      Twitter: '',
      loading: false,
      sourceImage: apiImage,
    };
  }

  /**
     * pickSingleImg from gallary
     */
  pickSingleImg() {
    ImagePicker.openPicker({
      width: globals.screenWidth * 0.25,
      height: globals.screenWidth * 0.25,
      borderRadius: (globals.screenWidth * 0.25) / 2,
      mediaType: 'photo',
      includeBase64: true,
    }).then(image => {  
      this.setState({ sourceImage: image.path })
      AsyncStorage.setItem(globals.LINKEDINCOMPANYPROFILE, this.state.sourceImage); 
    }).catch((e) => {
      alert(e.message ? e.message : e);
    });
  }

 
  /**
   * API call of updateLinkdinProfilePicture
   */
  makeApiCall(sourceImage) {
    this.props.showLoader();
    this.setState({ loading: true })

    if (globals.isInternetConnected == true) {
      API.updateLinkdinProfilePicture(this.updateLinkdinProfilePictureResponseData, true, sourceImage, globals.userID);
    } else {
      this.props.hideLoader();
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }


  /**
    * Response callback of updateLinkdinProfilePictureResponseData
    */
   updateLinkdinProfilePictureResponseData = {
    success: response => {
      console.log(
        TAG,
        'updateLinkdinProfilePictureResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        Alert.alert(globals.appName, globals.MESSAGE.PROFILE.LINKEDINPROFILE,
          [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],);
      }
      else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message)
      }
      this.props.hideLoader()
    },
    error: err => {
      this.props.hideLoader();
      this.setState({ loading: false })
      console.log(
        TAG,
        'updateLinkdinProfilePictureResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      Alert.alert(globals.appName, err.Message)
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  render() {
    const { sourceImage } = this.state;
    const { getCompanyProfile, LinkedIn, fullName, email, jobtitle, professionalSummary } = this.state;
    const isFullName = globals.checkObject(getCompanyProfile, 'Name');
    const isEmail = globals.checkObject(getCompanyProfile, 'Email');
    const isBusinessDescription = globals.checkObject(getCompanyProfile, 'BusinessDescription');
    const isSociallinks = globals.checkObject(getCompanyProfile, 'SocialLinks')
    const isLinkedInUrl = globals.checkObject(getCompanyProfile.SocialLinksobj, 'LinkedInUrl');
    const isCompanyLogoPath = globals.checkImageObject(getCompanyProfile, 'CompanyLogoPath');
    apiImage = getCompanyProfile.CompanyLogoPath;
    AsyncStorage.getItem(globals.LINKEDINCOMPANYPROFILE).then(token => {
      globals.linkedin_company_profile  = token;
    });

    return (
      <ScrollView style={styles.scrollStyle}>
        <View style={styles.scrollStyle}>
          <View style={styles.viewcontainer}>
            <TouchableOpacity onPress={() => this.pickSingleImg()}>
              <View style={[styles.beforeimgviewStyle, { backgroundColor: colors.proUnderline }]}>
                <Image
                  source={{ uri: (isCompanyLogoPath) ? (globals.linkedin_company_profile != "") ? globals.linkedin_company_profile : apiImage : globals.User_img }}
                  style={styles.editimgStyle} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Full Name</Text>
              <TextInput
                style={styles.ep_contentText}
                placeholder={(isFullName) ? getCompanyProfile.Name : '-'}
                placeholderTextColor={colors.darkBlack}
                onChangeText={text => this.setState({ fullName: text })}
                returnKeyType="next"
                value={fullName}
                onSubmitEditing={() => this.EmailRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Email</Text>
              <TextInput
                style={styles.ep_contentText}
                placeholder={(isEmail) ? getCompanyProfile.Email : '-'}
                placeholderTextColor={colors.darkBlack}
                onChangeText={text => this.setState({ email: text })}
                returnKeyType="next"
                value={email}
                ref={EmailRef => (this.EmailRef = EmailRef)}
                onSubmitEditing={() => this.jobRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Job Title</Text>
              <TextInput
                style={styles.ep_contentText}
                placeholder="-"
                placeholderTextColor={colors.darkBlack}
                onChangeText={text => this.setState({ jobtitle: text })}
                returnKeyType="next"
                value={jobtitle}
                ref={jobRef => (this.jobRef = jobRef)}
                onSubmitEditing={() => this.ProfessionalSummaryRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Professional Summary</Text>
              <TextInput style={styles.ep_contentText}
                multiline={true}
                maxLength={100}
                style={styles.ep_contentText}
                placeholderTextColor={colors.darkBlack}
                placeholder={(isBusinessDescription) ? getCompanyProfile.BusinessDescription : '-'}
                onChangeText={text => this.setState({ professionalSummary: text })}
                returnKeyType="next"
                value={professionalSummary}
                ref={ProfessionalSummaryRef => (this.ProfessionalSummaryRef = ProfessionalSummaryRef)}
                onSubmitEditing={() => this.LinkedInRef.focus()}
                autoCapitalize="none">

              </TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>LinkedIn</Text>
              <TextInput
                style={styles.ep_contentText}
                placeholderTextColor={colors.darkBlack}
                placeholder={(isLinkedInUrl) ? getCompanyProfile.LinkedInUrl : '-'}
                onChangeText={text => this.setState({ LinkedIn: text })}
                returnKeyType="done"
                value={LinkedIn}
                ref={LinkedInRef => (this.LinkedInRef = LinkedInRef)}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={[styles.ep_buttonStyle, { marginTop: globals.screenHeight * 0.0355, }]}>
            <TouchableOpacity onPress={() => this.makeApiCall(sourceImage)}
              style={[styles.touchableStyle, { backgroundColor: colors.warmBlue }]}>
              <View>
                <Text style={[styles.btntextStyless, { color: colors.white, }]}>{globals.BTNTEXT.PAYMENTSCREENS.SAVE}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.touchableStyle, { marginLeft: globals.screenWidth * 0.02, backgroundColor: colors.gray }]}
            >
              <View>
                <Text style={[styles.btntextStyless, { color: colors.white }]}>{globals.BTNTEXT.PAYMENTSCREENS.CANCLE}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

      </ScrollView>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditLinkedinProfile);