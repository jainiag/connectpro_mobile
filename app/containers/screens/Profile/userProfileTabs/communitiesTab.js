/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, ScrollView, ActivityIndicator,Alert } from 'react-native';
import * as globals from '../../../../utils/globals';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import * as colors from '../../../../assets/styles/color';

let TAG = "Profile Communities::== "
class CommunitiesTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      communitiesData: [],
      loading: false,
      serverErr: false,
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    this.makeApiCall()
  }

  /**
   * API call of render my communitiesList
   */
  makeApiCall() {
    const data = {
      UserID: globals.userID,
      CommunityName: "",
      IsCommunityAdmin: true,
      PageNumber: 1,
      PageSize: 100,
      IsAccessToGlobalCommunities: true,
      UserAccessToken: JSON.parse(globals.tokenValue)
    }

    if (globals.isInternetConnected === true) {
      API.getMyCommunities(this.GetMyCommunityResponseData, data, true)
    } else {
      this.setState({ loading: false })
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }


  /**
   *  call when _sessionOnPres
   */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }


  /**
   * Response of MyCommunity listing
   */
  GetMyCommunityResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetMyCommunityResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        let communityData = response.Data.UserCommunities
        let finalCommunityData = [];
        for (let index = 0; index < communityData.length; index++) {
          const communityDataObject = communityData[index];
          if (communityDataObject.Isactive && communityDataObject.CommunityStatusID == 1) { }
          else if ((communityDataObject.Isactive || communityDataObject.CommunityStatusID != 1 || communityDataObject.CommunityStatusID != 5) && communityDataObject.CommunityUserStatus == 1) { }
          else {
            finalCommunityData.push(communityDataObject);
            this.setState({ communitiesData: finalCommunityData }, () => {
            })
          }
        }
      } else {
        Alert.alert(globals.appName, response.Message)
      }
      this.setState({ loading: false })
    },
    error: err => {
      this.setState({ serverErr: true, loading: false });
      console.log(
        TAG,
        'GetMyCommunityResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.setState({ loading: false })
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };


  render() {
    const { communitiesData } = this.state;
    return (
      <View style={styles.container2}>
        {(this.state.loading === true) ?
          <View style={styles.activity_IndicatorViewStyle}>
            <ActivityIndicator
              style={styles.actvity_indicatorStyle}
              size="small"
              color={colors.bgColor}
            />
          </View>
          :
          <View style={styles.pro_tagViewContainer}>
            <ScrollView bounces={false}>
              {
                (this.state.serverErr == true) ?
                  <View style={styles.notAvialableViewStyle}>
                    <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_SMTHING_WENT_WRNG}</Text>
                  </View>
                  :
                  (communitiesData.length > 0 && communitiesData != null) ?
                    <TagSelect
                      isFrom="ProfileCommunities"
                      data={communitiesData}
                      itemStyle={styles.item}
                      itemLabelStyle={styles.label}
                      itemStyleSelected={styles.itemSelected}
                      itemLabelStyleSelected={styles.labelSelected}
                    />
                    :
                    <View style={styles.notAvialableViewStyle}>
                      <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.PORFILE_COMMUNITY_NOT_AVLBL}</Text>
                    </View>
              }
            </ScrollView>
          </View>
        }
      </View>
    );
  }
}
// ********************** Model mapping method **********************

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CommunitiesTab);