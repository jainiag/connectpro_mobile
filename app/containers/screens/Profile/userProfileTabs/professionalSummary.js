/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import * as images from '../../../../assets/images/map';
import styles from './style';
import * as globals from '../../../../utils/globals';
import AsyncStorage from '@react-native-community/async-storage'

var _this = null;
let MemberProfile = [];
let MyProfile = [];
let email;
let organizationName;
let ProfileAllData = [];
let TAG = "ProfessionalSummary Tab ::===="
export default class ProfessionalSummary extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    AsyncStorage.getItem(globals.ORGANIZATIONID).then(org_Id => {
      globals.organizationId = org_Id;
    })
  }

  static getMemberProfileData(memberProfile, myProfileData, EmailID, OrganizationName,profileAllData) {
    MemberProfile = memberProfile;
    MyProfile = myProfileData;
    email = EmailID;
    organizationName = OrganizationName;
    ProfileAllData = profileAllData;
  }
  render() {
    const isBiography = globals.checkObject(MemberProfile, 'Biography')
    const isExtension = globals.checkObject(MemberProfile, 'Extension')
    const isPhone = globals.checkObject(MemberProfile, 'Phone')
    const isJobTitle = globals.checkObject(MemberProfile, 'JobTitle')
    const isEmailId = globals.checkObject(MyProfile, 'EmailID')
    const isOrganizationName = globals.checkObject(MyProfile, 'OrganizationName')
    const atNumber = (isPhone) ? MemberProfile.Phone : '-';
    const finalPhone = (atNumber.charAt(0) == "+") ? atNumber : (isPhone) ? "+"+atNumber : "-"

    return (
      <View style={styles.summary_MainContainer}>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
          <View style={styles.summary_ViewContainer}>
            <Image source={images.MyProfile.bag}
              resizeMode='contain'
              style={styles.summary_IconsStyle}
            />
            <Text style={[styles.summary_TextStyles,{width:globals.screenWidth * 0.80}]}>{(isJobTitle) ? MemberProfile.JobTitle : '-'}</Text>
          </View>
          <View style={styles.summary_ViewContainer}>
            <Image source={images.MyProfile.buildings}
              resizeMode='contain'
              style={styles.summary_IconsStyle}
            />
            <Text style={[styles.summary_TextStyles,{width:globals.screenWidth * 0.80}]}>{(isOrganizationName) ? MyProfile.OrganizationName : '-'}</Text>
          </View>
          <View style={styles.summary_ViewContainer}>
            <Image source={images.MyProfile.emailBlack}
              resizeMode='contain'
              style={styles.summary_IconsStyle}
            />
            <Text style={[styles.summary_TextStyles,{width:globals.screenWidth * 0.80}]}>{(isEmailId) ? MyProfile.EmailID : '-'}</Text>
          </View>
          <View style={styles.summary_ViewContainer}>
            <Image source={images.MyProfile.phone}
              resizeMode='contain'
              style={styles.summary_IconsStyle}
            />
            <View style={styles.summary_TextViewContainer}>
              <Text style={[styles.summary_TextStyles]}>{finalPhone}</Text>
              {(isExtension) ?
                <Text style={[styles.summary_TextStyles, { width: globals.screenWidth * 0.30 }]}>Extension: {(isExtension) ? MemberProfile.Extension : '-'}</Text>
                :
                null
              }
            </View>
          </View>
          <Text style={[styles.summary_TextStyles,{marginLeft: globals.screenWidth * 0.02, paddingBottom: globals.screenHeight * 0.02,}]}>{(isBiography) ? MemberProfile.Biography : '-'}</Text>

        </ScrollView>
        <TouchableOpacity style={styles.iconViewStyle}
          onPress={() => this.props.navigationProps.navigationProps.navigation.navigate("EDIT_PROFILE", 
          { 
            MemberProfile: MemberProfile, 
            MyProfile: MyProfile, 
            profileAllData: ProfileAllData,
            sourceImage: this.props.sourceImage,
            })}>
          <Image source={images.MyProfile.blueEdit}
            resizeMode='contain'
            style={styles.IconsStyle}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
