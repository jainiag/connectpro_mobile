import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';
import * as colors from '../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.045 : (globals.iPhoneX) ? globals.screenHeight * 0.018 : (Platform.OS == 'android') ? globals.screenHeight * 0.005 : globals.screenHeight * 0.03,
  },
  container2: {
    flex: 1,
    backgroundColor:  colors.white,
    marginTop: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.028 : (globals.iPhoneX) ? globals.screenHeight * 0.018 : globals.screenHeight * 0.022,
  },
  buttonContainer: {
    padding: globals.screenHeight * 0.018,
  },
  buttonInner: {
    marginBottom: globals.screenHeight * 0.018,
  },
  labelText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 27 : globals.font_15,
    fontWeight: '500',
    marginBottom: globals.screenHeight * 0.018,
  },
  item: {
    backgroundColor: 'lightgrey',
  },
  label: {
    // color: '#333',
    fontSize: globals.font_13
  },
  itemSelected: {
    // backgroundColor: '#333',
  },
  labelSelected: {
    // color:colors.white,
  },
  activity_IndicatorViewStyle: {
    flex: 1, 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  actvity_indicatorStyle: {
    justifyContent: 'center', 
    alignItems: 'center'
  },

  // ========: profileSummary Style :=======
  summary_MainContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
    marginTop: globals.screenHeight * 0.018,
  },
  summary_ViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: globals.screenHeight * 0.013,
    marginHorizontal:globals.screenWidth * 0.012
  },
  summary_TextStyles: {
    marginLeft: globals.screenWidth * 0.03,
    // backgroundColor:'red',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
  },
  iconViewStyle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.025,
    marginRight: globals.screenWidth * 0.03,
  },
  summary_IconsStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.06,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.04 : globals.screenWidth * 0.06,
  },
  IconsStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08, 
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
  },
  summary_TextViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  summary_EditButtonStyle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.025,
    marginRight: globals.screenWidth * 0.03,
  },
  // ======: profileCategory tab :=====
  pro_tagViewContainer: {
    marginLeft: globals.screenWidth * 0.04,
  },
  pro_imageStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.055 : globals.screenWidth * 0.08,
  },
  notAvialableTextStyle: {
    fontSize: globals.font_14,
    textAlign: 'center'
  },
  notAvialableViewStyle: {
    justifyContent: 'center', 
    alignItems: 'center', 
    marginTop: globals.screenHeight * 0.185
  },
  // ======: edit profileCategory :=====
  scrollViewContainer: {
    flex: 1,
    marginLeft: globals.screenWidth * 0.04,
  },
  mainInputViewContaier: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  inputViewContainer: {
    width: '100%',
    borderWidth: 0.5,
    borderColor: colors.proUnderline,
  },
  textInputContainer: {
    paddingBottom: globals.screenHeight * 0.015,
    paddingTop: globals.screenHeight * 0.015,
    marginLeft: globals.screenWidth * 0.02,
    marginRight: globals.screenWidth * 0.02,
    color: colors.black,
    // width: globals.screenWidth * 0.65,
    fontSize: globals.font_13,
  },
  buttonViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: globals.iPhoneX ? 0 : 15,
  },
  saveButtonContariner: {
    backgroundColor: colors.warmBlue,
    height: globals.screenHeight * 0.036,
    width: globals.screenWidth * 0.19,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: globals.screenWidth * 0.018,
  },
  saveAndCancelTextStyle: {
    color: colors.white,
    fontSize: globals.font_16,
  },
  cancelButtonContainer: {
    backgroundColor: colors.gray,
    height: globals.screenHeight * 0.036,
    width: globals.screenWidth * 0.19,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: globals.screenWidth * 0.018,
  },
  addCategory_ViewStyle:{
    borderColor: colors.gray, 
    borderWidth: 1,
    width: "95%", 
    height: (Platform.OS == "ios") ? globals.screenHeight * 0.137 : globals.screenHeight * 0.15, 
    alignSelf: "center", 
    justifyContent: "flex-end"
  },
  addCategory_childViewStyle: {
    borderColor: colors.proUnderline,
    // borderTopWidth: 0.7,
    height: globals.screenHeight * 0.08,
    width: "100%"
  },
  //----------------------------similar interest----------------------------------------
  mainParentStyle: {
    flex: 1,
  },
  mainRenderItemView: {
    height: globals.screenHeight * 0.11, borderColor: colors.proUnderline, borderWidth: 1, marginBottom: globals.screenHeight * 0.01, borderRadius: 3, flex: 1,
    marginHorizontal: 10
  },
  mainViewSimilarInterst: {
    flex: 1, marginTop: globals.screenHeight * 0.018
  },
  horizontalItemView: {
    flexDirection: 'row', marginHorizontal: globals.screenHeight * 0.02, marginVertical: globals.screenHeight * 0.015, flex: 1
  },
  middleViewTexts: {
    justifyContent: 'center', marginHorizontal: globals.screenHeight * 0.02, flex: 1,
  },
  lastImgViewEnd: {
    justifyContent: 'center', alignItems: 'flex-end'
  },
  attendesFlatlist: {
    marginTop: globals.screenHeight * 0.05,
    paddingHorizontal: globals.screenWidth * 0.05
  },
  beforeimgView: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946) / 2,
    borderColor: colors.lightGray,
    borderWidth: 0.5
  },
  ivItemImageStyle: {
    height: globals.screenHeight * 0.0757, //40
    width: globals.screenHeight * 0.0757, //40
    borderRadius: (globals.screenHeight * 0.0946) / 2,
    marginTop: -1,
    marginLeft: -1,
  },
  attendeeTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_12,
  },
  attende_shareIconStyle: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 30 : globals.screenHeight * 0.03,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 40 : globals.screenWidth * 0.07
  },

  // ========:  Portfolio Tab :=========
  mainViewContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.01,
  },

  PO_TAB_MainContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.018,
  },
  PO_TAB_AddButtonContainer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: (globals.iPhoneX) ? globals.screenWidth * 0.04 : globals.screenWidth * 0.02,
    right: globals.screenWidth * 0.002,
    marginRight: globals.screenWidth * 0.03,
  },
  PO_TAB_addIconStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.08,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.06 : globals.screenWidth * 0.08,
  },
  PO_TAB_flatListMainView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: globals.screenHeight * 0.01,
  },
  PO_TAB_titleViewContainer: {
    marginLeft: globals.screenHeight * 0.018,
  },
  PO_TAB_titleTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
    marginBottom: globals.screenHeight * 0.01,
  },
  PO_TAB_linkTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
    color: colors.darkSkyBlue,
  },
  PO_TAB_EditDeleteBtnViewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  PO_TAB_editIconStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    marginRight: globals.screenHeight * 0.01
  },
  PO_TAB_deleteIconStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.05 : globals.screenWidth * 0.07,
    marginRight: globals.screenHeight * 0.04,
  },
  PO_TAB_underLineStyle: {
    height: 1,
    width: '100%',
    backgroundColor: colors.gray,
  },
  //:==================================Skills & Interests---------------------------------
  horizontalView:{
    flexDirection: 'row', marginBottom: globals.screenHeight * 0.015, marginHorizontal: globals.screenWidth * 0.04
  },
  findPersonView:{
    justifyContent: 'center', alignItems: 'flex-start', flex:1 
  },
  searchViewRight:{
    justifyContent: 'flex-end', alignItems: 'flex-end',flex:1
  },
  findPersonTextStyle:{
      fontSize:globals.font_14,
      color:colors.lightgray,
      fontWeight:'600'
  },
  grayBorderStyle:{
    width: globals.screenWidth, height: 1, bottom: 2, backgroundColor: colors.gray, opacity: 0.5,
    marginVertical:5, marginTop:10
  },
});


