/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, Image, FlatList, TouchableWithoutFeedback, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import styles from './style';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import * as images from '../../../../assets/images/map';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import AsyncStorage from '@react-native-community/async-storage';
import { API } from '../../../../utils/api';
import globalStyles from '../../../../assets/styles/globleStyles';

let TAG = "My profile SimilarInterests :==="
let _this = null;
class SimilarInterests extends React.Component {
  constructor(props) {
    super(props);
    _this = this
    this.state = {
      loading: false,
      matchesData: [],
      serverErr: false,
    }
  }

  static refreshMatchesInterestList(){
    _this.makeGetSimilarMatchesInterestApiCall();
  }

  componentDidMount() {
   this.makeGetSimilarMatchesInterestApiCall();
  }

/**
 * Api call GetSilmilarMatchesInterest
 */
  makeGetSimilarMatchesInterestApiCall(){
    if (globals.isInternetConnected === true) {
      this.setState({ loading: true })
      API.getSilmilarMatchesInterest(this.GetSilmilarMatchesInterestResponseData, true, globals.userID, 5);
    } else {
      this.setState({ loading: false })
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }


  /**
 * Response callback of GetSilmilarMatchesInterestResponseData
 */
  GetSilmilarMatchesInterestResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetSilmilarMatchesInterestResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.setState({ matchesData: response.Data.SimilarInterestUsers })
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message)
      }
      this.setState({ loading: false })
    
    },
    error: err => {
      this.setState({ serverErr: true })
      this.setState({ loading: false })
      console.log(
        TAG,
        'GetSilmilarMatchesInterestResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
      if (err.StatusCode == 403 || err.StatusCode == 401) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(globals.appName, err.Message)
      }
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };
  /**
     *  call when _sessionOnPres
     */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    this.props.navigationProps.navigation.navigate('LoginScreen');
    loginScreen.clearTextFields();
  }


  /**
   * click of forward image, which shows user interests in Alert
   * @param {*} userName 
   * @param {*} userInterests 
   */
  clickForward(userName, userInterests) {
    Alert.alert(
      globals.appName,
      "Hobbies of " + userName + ": " + userInterests,
      [{ text: 'OK' }],
      { cancelable: false }
    );
  }

  /**
     * Render item list event attendee
     * @param {*} item 
     * @param {*} index 
     */
  renderItemList(item, index) {
    const isProffesion = globals.checkObject(item, 'OrganizationName');
    const isUserName = globals.checkObject(item, 'UserName')
    const isImage = globals.checkImageObject(item, 'ProfilePicture');
    const isHobby = globals.checkObject(item, 'UserInterests')
    const isJobTitle = globals.checkObject(item, "JobTitle")
    const JobTitle = (isJobTitle) ? item.JobTitle : '';
    const OrganizationName = (isProffesion) ? item.OrganizationName : ''
    const atText = (JobTitle.length > 15) ? "at " : " at ";

    return (
      <TouchableWithoutFeedback style={styles.mainParentStyle}>
        <View style={[styles.mainParentStyle]}>
          <TouchableOpacity 
          onPress={() => this.props.navigationProps.navigationProps.navigation.navigate("OtherProfile", { User_ID: item.UserID, item: item, isfrom: "SimilarInterests" })}
          style={[styles.mainRenderItemView, { backgroundColor: colors.white }]}>
            <View style={styles.horizontalItemView}>
              <View style={styles.beforeimgView}>
                <Image
                  source={{ uri: isImage ? item.ProfilePicture : globals.User_img }}
                  style={[styles.ivItemImageStyle]}
                />
              </View>
              <View style={styles.middleViewTexts}>
                <Text style={[styles.attendeeTextStyle, { color: colors.black }]}>{(isUserName) ? item.UserName : ''}</Text>
                <View style={{ flexDirection: 'row' }}>
                  <Text numberOfLines={1} style={[styles.attendeeTextStyle, { marginTop: 2, color: colors.black, width: (JobTitle.length > 15) ? globals.screenWidth * 0.26 : null }]}>{JobTitle}</Text>
                  <Text numberOfLines={1} style={[styles.attendeeTextStyle,{color: colors.black, marginTop: 2, width: (JobTitle.length > 19) ? globals.screenWidth * 0.33 : null}]}>{atText + OrganizationName}</Text>
                </View>
              </View>
              <TouchableWithoutFeedback style={styles.lastImgViewEnd} onPress={() => this.clickForward(item.UserName, item.UserInterests)}>
                <View style={styles.lastImgViewEnd}>
                  <Image
                    source={images.MyProfile.forwardArrow}
                    style={styles.attende_shareIconStyle}
                    resizeMode="contain"
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  render() {
    return (
      <View style={styles.mainViewSimilarInterst}>
        {(this.state.loading === true) ?
          <View style={styles.activity_IndicatorViewStyle}>
            <ActivityIndicator
              style={styles.actvity_indicatorStyle}
              size="small"
              color={colors.bgColor}
            />
          </View>
          :
          (this.state.matchesData.length > 0) && (this.state.matchesData !== null) ?
            <FlatList
              data={this.state.matchesData}
              renderItem={({ item, index }) => this.renderItemList(item, index)}
              keyExtractor={(index, item) => item.toString()}
              extraData={this.state}
              bounces={false}
              showsVerticalScrollIndicator={false}
            />
            :
            <View style={globalStyles.nodataStyle}>
              <Text style={globalStyles.nodataTextStyle}>{"Currently you do not have any similar interests with other members"}</Text>
            </View>
        }
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SimilarInterests);
