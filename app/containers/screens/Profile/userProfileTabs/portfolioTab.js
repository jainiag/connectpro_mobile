/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, FlatList, ActivityIndicator, Alert, Linking } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as images from '../../../../assets/images/map';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import styles from './style';
import AsyncStorage from '@react-native-community/async-storage';
import loginScreen from '../../AuthenticationScreens/loginScreen';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as colors from '../../../../assets/styles/color';


let TAG = 'Portfolio Tab ::==='
let _this;
class PortfolioTab extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      portfolioData: [],
      loading: false,
      serverErr: false,
      isDelete: false,
    }
  }


/**
   * static method for portfolio Tab refresh 
   */
  static refreshPortfolio() {
    if (globals.isInternetConnected === true) {
      API.getPortfolioInfo(_this.GetPortfolioResponseData, true, globals.userID)
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  componentDidMount() {
    if (globals.isInternetConnected === true) {
      this.setState({ loading: true })
      API.getPortfolioInfo(this.GetPortfolioResponseData, true, globals.userID)
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      this.setState({ loading: false })
    }
  }

  /**
    * Response callback of GetPortfolio ResponseData
    */
  GetPortfolioResponseData = {
    success: response => {
      console.log(
        TAG,
        'GetPortfolioResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        const responseData = response.Data.Links;
        this.setState({ portfolioData: responseData })
      } else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false })
      }
      this.setState({ loading: false })
    },
    error: err => {
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        ); 
      }
      else{
        Alert.alert(globals.appName, err.Message)
      }
      console.log(
        TAG,
        'GetMyConnectionResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ loading: false });
      this.setState({ serverErr: true })
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };

  

  /**
   *  this method for Delete portfolio
   */
  _deletePortfolioClick(index) {
    let objectId = this.state.portfolioData[index].Id
    this.setState({isDelete: true})
    let filteredArray = this.state.portfolioData.filter(item => item.Id !== objectId)
    this.setState({ portfolioData: filteredArray });
    const data = {
      UserId: globals.userID,
      Links: filteredArray
    }
    if (globals.isInternetConnected === true) {
      API.savePortfolioInfo(this.SavePortfolioResponseData, data, true)
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
    * Response callback of SavePortfolio ResponseData
    */
  SavePortfolioResponseData = {
    success: response => {
      console.log(
        TAG,
        'SavePortfolioResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        Alert.alert(globals.appName , "Portfolio deleted",
          [{ text: 'OK', onPress: () => this.setState({isDelete: false})}],
          { cancelable: false }
      );
      } else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false })
      }
    },
    error: err => {
      console.log(
        TAG,
        'SavePortfolioResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      Alert.alert(globals.appName, err.Message)
      this.setState({ loading: false });
      this.setState({ serverErr: true })
    },
    complete: () => {
      this.setState({ loading: false })
    },
  };

/**
 * When session is expired then this method called
 */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigationProps.navigationProps.navigation.navigate('LoginScreen');
  }


  _openPortfolio = (Url) => {
    Linking.canOpenURL(Url).then(supported => {
      if (supported) {
        return Linking.openURL(Url);
      } else {
        return Linking.openURL(Url)
      }
    });
  };


  renderItemList(item, index) {
    return (
      <View>
        <View style={[styles.PO_TAB_flatListMainView,{marginHorizontal:globals.screenWidth * 0.01 }]}>
          <View style={[styles.PO_TAB_titleViewContainer,{marginHorizontal:globals.screenWidth * 0.01}]}>
            <Text style={[styles.PO_TAB_titleTextStyle,{width:globals.screenWidth * 0.65,}]}>{item.Title}</Text>
            <Text onPress={() => this._openPortfolio(item.Link)} style={styles.PO_TAB_linkTextStyle}>{item.Link}</Text>
          </View>
          <View style={styles.PO_TAB_EditDeleteBtnViewStyle}>
            <TouchableOpacity
              onPress={() => this.props.navigationProps.navigationProps.navigation.navigate('AddPortfolio',
                {
                  PortfolioData: item,
                  PortfolioAllData: this.state.portfolioData,
                  fillData: true,
                  index: index
                })}
            >
              <Image
                source={images.MyProfile.blueEdit}
                style={styles.PO_TAB_editIconStyle}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this._deletePortfolioClick(index)}
            >
              <Image
                source={images.MyProfile.blueDelete}
                style={styles.PO_TAB_deleteIconStyle}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.PO_TAB_underLineStyle} />
      </View>
    );
  }

  render() {
    return (
      <View style={styles.mainViewContainer}>
        {(this.state.loading == true) ?
          <View style={styles.activity_IndicatorViewStyle}>
            <ActivityIndicator
              style={styles.actvity_indicatorStyle}
              size="small"
              color={colors.bgColor}
            />
          </View>
          :
          (this.state.isDelete == true) ?
          null
            :
            (this.state.serverErr == true) ?
              <View style={styles.notAvialableViewStyle}>
                <Text style={styles.notAvialableTextStyle}>{globals.ERROR_MESSAGE.SERVER_ERR.SERVER_ERR_SMTHING_WENT_WRNG}</Text>
              </View>
            :
            (this.state.portfolioData.length > 0 && this.state.portfolioData != null) ?
              <FlatList
                data={this.state.portfolioData}
                renderItem={({ item, index }) => this.renderItemList(item, index)}
                keyExtractor={(index, item) => item.toString()}
                extraData={this.state}
                bounces={false}
                showsVerticalScrollIndicator={false}
                style={{marginBottom: globals.screenHeight * 0.045}}
              /> :
              <View style={globalStyles.nodataStyle}>
                <Text style={globalStyles.nodataTextStyle}>{globals.ERROR_MESSAGE.MY_PROFILE.PORFILE_PORTFOLIO_NOT_AVLBL}</Text>
              </View>
        }
        {this.state.loading == true ? null :
          <TouchableOpacity
            onPress={() => this.props.navigationProps.navigationProps.navigation.navigate('AddPortfolio',
              {
                fillData: false,
                PortfolioData: this.state.portfolioData
              })}
            style={styles.PO_TAB_AddButtonContainer}>
            <Image
              source={images.MyProfile.blueAdd}
              style={styles.PO_TAB_addIconStyle}
            />
          </TouchableOpacity>
        }
      </View>
    );
  }
}


// ********************** Model mapping method **********************

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PortfolioTab);