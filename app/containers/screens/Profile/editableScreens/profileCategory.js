/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  SafeAreaView,
  TextInput,
  Alert,
  ScrollView,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import styles from '../userProfileTabs/style';
import * as colors from '../../../../assets/styles/color';
import { API } from '../../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ProfileCategoryTab from '../userProfileTabs/profileCategoryTab';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import tagStyle from '../userProfileTabs/style';
import DeviceInfo from 'react-native-device-info';
import KeyboardListener from 'react-native-keyboard-listener';

const iPad = DeviceInfo.getModel();
let TAG = "Myprofile ProfileCategory:=="
let _this;
class ProfileCategory extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Profile Category'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      CategoryData: this.props.navigation.state.params.ProfileCategoryData,
      addTag: '',
      profileCategoryData: [],
      keyboardOpen: false,
      deleteStatus: false,
    };
  }

  _cancelOnClick(){
    ProfileCategoryTab._profileCategoryApiCall()
    this.props.navigation.goBack()
  }

  componentWillUnmount(){
    ProfileCategoryTab._profileCategoryApiCall()
  }

  static deleteProfileItem() {
    _this.setState({
      deleteStatus: true,
    })
  }
  /**
   * click on save button
   */
  saveProfileCategoriesAPI() {

    if (this.state.profileCategoryData.length > 0 || this.state.deleteStatus == true ) {
    if (this.state.CategoryData.length !== this.state.profileCategoryData.length || this.state.deleteStatus == true || this.state.addTag == "") {
      let arrPortfolioData = [];
      if ((this.state.profileCategoryData.length > 0) || (this.state.addTag == "")) {
        let categoryArray = this.state.profileCategoryData
        arrPortfolioData = this.props.navigation.state.params.ProfileCategoryData

        var merge = (a, b, p) => a.filter(aa => !b.find(bb => aa[p] === bb[p])).concat(b);
        let final_Arr = merge(arrPortfolioData, categoryArray, "Name");
          let nameValues = final_Arr;
          let namesArray = [];
          let uNamesValue = new Map(nameValues.map(s => [s.Name.toLowerCase(), s.Name]));
          let finalValues = [...uNamesValue.values()]
          for (let i = 0; i < finalValues.length; i++) {
            let newObj = {
              Name: finalValues[i]
            }
            namesArray.push(newObj)
          }

        const data = {
          UserId: globals.userID,
          WhoAmIAll: namesArray
        }
        this.props.showLoader()
        if (globals.isInternetConnected === true) {
          API.saveProfileCategories(this.SaveProfileCategoriesResponseData, data, true)
        } else {
          this.props.hideLoader()
          Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
      } else {
        Alert.alert(globals.appName, "Please add profile category")
      }
    } else {
      Alert.alert(globals.appName, "Please add profile category")
    }
  } else {
    Alert.alert(globals.appName, "Please add profile category")
  }
  }

  /**
   * Response callback of SavePortfolio ResponseData
   */
  SaveProfileCategoriesResponseData = {
    success: response => {
      console.log(
        TAG,
        'SaveProfileCategoriesResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({deleteStatus: false})
        Alert.alert(globals.appName , response.Message,
          [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
          { cancelable: false }
      );
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message)
      }
      this.props.hideLoader()
    },
    error: err => {
      console.log(
        TAG,
        'SaveProfileCategoriesResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ loading: false });
      Alert.alert(globals.appName, err.Message)
      this.props.hideLoader();
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  _changeTag(text) {
    if (this.state.addTag == '') {
        this.setState({ addTag: text })
    } else {
        Alert.alert(globals.appName, 'Please add profile category')
    }
}


_addCategory() {
    const { profileCategoryData, addTag } = this.state;
    let regex = /^[^\s]+(\s+[^\s]+)*$/;
    if (!regex.test(addTag)) {
      if (profileCategoryData.length < 0) {
        Alert.alert(globals.appName, "Please add profile category",
          [{ text: 'OK', onPress: () => this.setState({ addTag: '' }) }])
      }
    } else {
      let tempArr = profileCategoryData;
      tempArr.push({ Name: addTag });
      this.setState({ profileCategoryData: tempArr});
    }
    this.setState({addTag: ""})
  } 

onSubmitt() {
  this.setState({ addTag: '' })
}

  render() {
    const { CategoryData } = this.state;
    return (
      <SafeAreaView style={[styles.container, (globals.iPhoneX) ?
        { marginTop: 35 }
        :
        { paddingTop: 35 }]}>
          <KeyboardListener
          onDidShow={() => { this.setState({ keyboardOpen: true }, () => {console.log('onDidShow')}); }}
          onDidHide={() => { this.setState({ keyboardOpen: false },() => {console.log('onDidHide')}); }} />
        <ScrollView
          style={styles.scrollViewContainer}
          bounces={false}
          showsVerticalScrollIndicator={false}
        >
          {
            (CategoryData != null) ?
              <TagSelect
                isFrom="ProfileCategory"
                isDelete="Profile"
                data={CategoryData}
                itemStyle={styles.item}
                itemLabelStyle={styles.label}
                itemStyleSelected={styles.itemSelected}
                itemLabelStyleSelected={styles.labelSelected}
              /> : null
          }

        </ScrollView>
        <KeyboardAvoidingView behavior={(Platform.OS == "ios") ? "padding" : "height"} keyboardVerticalOffset={(Platform.OS == "ios") ? 100 : 0}>
        <View style={styles.addCategory_ViewStyle}>
          <View style={styles.addCategory_childViewStyle}>
            <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
              <TagSelect
                isFrom="ProfileCategory"
                data={this.state.profileCategoryData}
                itemStyle={tagStyle.item}
                itemLabelStyle={tagStyle.label}
                itemStyleSelected={tagStyle.itemSelected}
                itemLabelStyleSelected={tagStyle.labelSelected}
              />
            </ScrollView>
          </View>
          <View style={styles.mainInputViewContaier}>
            <View style={styles.inputViewContainer}>
              <TextInput
                value={this.state.addTag}
                autoCapitalize="none"
                onFocus={false}
                blurOnSubmit={(this.state.addTag == '') ? true : false}
                returnKeyType="done"
                onChangeText={text => this.setState({ addTag: text })}
                placeholderTextColor={colors.darkgrey}
                placeholder="Add Profile Categories like Mentor, Entrepreneur etc.."
                style={styles.textInputContainer}
                onSubmitEditing={() => (this.state.addTag == '') ? this.onSubmitt() : this._addCategory()}
              />
            </View>
          </View>
        </View>
        </KeyboardAvoidingView>
        {(Platform.OS == "android") ?
          (this.state.keyboardOpen == false) ?
            <View
              style={styles.buttonViewContainer}
            >
              <TouchableOpacity
                onPress={() => this.saveProfileCategoriesAPI()}
                style={styles.saveButtonContariner}
              >
                <Text style={styles.saveAndCancelTextStyle}>Save</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this._cancelOnClick()}
                style={styles.cancelButtonContainer}
              >
                <Text style={styles.saveAndCancelTextStyle}>Cancel</Text>
              </TouchableOpacity>
            </View>
            :
            <View style={{marginTop: 20, marginBottom: globals.iPhoneX ? 0 : globals.screenHeight * 0.01,}} />
          :
          <View
            style={styles.buttonViewContainer}
          >
            <TouchableOpacity
              onPress={() => this.saveProfileCategoriesAPI()}
              style={styles.saveButtonContariner}
            >
              <Text style={styles.saveAndCancelTextStyle}>Save</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this._cancelOnClick()}
              style={styles.cancelButtonContainer}
            >
              <Text style={styles.saveAndCancelTextStyle}>Cancel</Text>
            </TouchableOpacity>
          </View>
        }
      </SafeAreaView>
    );
  }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileCategory);
