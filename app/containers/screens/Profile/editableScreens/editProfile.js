import React from 'react';
import { Text, View, TouchableOpacity, TextInput, ScrollView, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import styles from './style';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import * as colors from '../../../../assets/styles/color';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import { API } from '../../../../utils/api';
import MyProfile from '../myProfile';
import Validation from '../../../../utils/validation';
import CountryPicker, { getAllCountries } from '../../../../libs/react-native-country-picker-modal';
import DeviceInfo from 'react-native-device-info';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

let TAG = ' EditProfile :===: '
class EditProfile extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Edit Profile'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    console.log("props :: :: ", this.props);
    
    const userLocaleCountryCode = DeviceInfo.getDeviceCountry();
    const userCountryData = getAllCountries()
      .filter(country => globals.COUNTRY_LIST.includes(country.cca2))
      .filter(country => country.cca2 === userLocaleCountryCode)
      .pop();
    let callingCode = null;
    let cca2 = userLocaleCountryCode;
    if (!cca2 || !userCountryData) {
      cca2 = 'IN';
      callingCode = '91';
    } else {
      cca2 = 'IN';
      callingCode = '91';
    }

    const phoneNumber = this.props.navigation.state.params.MemberProfile.Phone
    const lengthBase = (phoneNumber == null) ? 0 : phoneNumber.length
    const iscode = (phoneNumber == null) ? +91 : (lengthBase < 13) ? phoneNumber.slice(0,2) : (lengthBase > 13) ? phoneNumber.slice(0,3) : phoneNumber.slice(0,2)
    const checkCode = (phoneNumber == null) ? +91 : iscode.charAt(0);
    const newCode = iscode
    const cntryCode = (phoneNumber !== null ) ? (lengthBase == 12) ? phoneNumber.split(phoneNumber.slice(-11))[0] : (lengthBase == 8) ? phoneNumber.split(phoneNumber.slice(-8))[0] : phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (phoneNumber !== null ) ? (phoneNumber.length === 10) ? phoneNumber : (lengthBase == 12) ? phoneNumber.slice(2): (lengthBase == 8) ? phoneNumber.slice(1) : phoneNumber.slice(3) : ""
    const getNumber = (phoneNumber !== null ) ? (phoneNumber.length === 10) ? phoneNumber : isNumber.replace(/ /g, '') : "";
    const finalCode = (checkCode == "+") ? iscode.substring(1) : iscode

    this.state = {
      isCheck: false,
      isUncheck: false,
      firstName: '',
      lastName: '',
      jobTitle: '',
      professionalSummary: '',
      Phone: '',
      Ext: '',
      profileVideo: '',
      LinkedIn: '',
      Facebook: '',
      Twitter: '',
      googlePage: '',
      myProfileData: [],
      memberProfile: [],
      profileAllData: [],
      redStarforFirstName: false,
      redStarforLastName: false,
      isPrivate: false,
      isPrimary: false,
      cca2: cca2,
      callingCode: finalCode,
      mobileNum: '',
      validMob: false,
      isLoading: false,
      profileImage: this.props.navigation.state.params.sourceImage,
    };
  }

  componentDidMount(){
    const phoneNumber = this.props.navigation.state.params.MemberProfile.Phone
    const lengthBase = (phoneNumber == null) ? 0 : phoneNumber.length
    const iscode = (phoneNumber == null) ? +91 : (lengthBase == 11 || lengthBase == 10 ) ? phoneNumber.slice(0,3) : (lengthBase < 13) ? phoneNumber.slice(0,2) : (lengthBase > 13) ? phoneNumber.slice(0,3) : phoneNumber.slice(0,2)
    const checkCode = (phoneNumber == null) ? +91 : iscode.charAt(0);
    const newCode = iscode
    const cntryCode = (phoneNumber !== null ) ? (lengthBase == 12) ? phoneNumber.split(phoneNumber.slice(-11))[0] : (lengthBase == 11 || lengthBase == 10) ? phoneNumber.split(phoneNumber.slice(-10))[0] : (lengthBase == 8) ? phoneNumber.split(phoneNumber.slice(-8))[0] : phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (phoneNumber !== null ) ? (phoneNumber.length === 10) ? phoneNumber.slice(3) : (phoneNumber.length === 11) ? phoneNumber.slice(3) : (lengthBase == 12) ? phoneNumber.slice(2): (lengthBase == 8) ? phoneNumber.slice(1) : phoneNumber.slice(3) : ""
    const getNumber = (phoneNumber !== null) ? (phoneNumber.length === 10) ? phoneNumber.slice(3) : (phoneNumber.length === 11) ? phoneNumber.slice(3) : isNumber.replace(/ /g, '') : "";
    const finalCode = (checkCode == "+") ? iscode.substring(1) : iscode

    this.setState({
      isCheck: this.props.navigation.state.params.MemberProfile.IsPhonePrivate,
      isUncheck: this.props.navigation.state.params.MemberProfile.IsEmailPrivate,
      firstName: this.props.navigation.state.params.MyProfile.FirstName,
      lastName: this.props.navigation.state.params.MyProfile.LastName,
      jobTitle: this.props.navigation.state.params.MemberProfile.JobTitle,
      professionalSummary: this.props.navigation.state.params.MemberProfile.Biography,
      Phone: (phoneNumber !== null) ?  getNumber : '',
      Ext: this.props.navigation.state.params.MemberProfile.Extension,
      profileVideo: this.props.navigation.state.params.MemberProfile.YouTubeUrl,
      LinkedIn: this.props.navigation.state.params.MemberProfile.LinkedInUrl,
      Facebook: this.props.navigation.state.params.MemberProfile.FacebookUrl,
      Twitter: this.props.navigation.state.params.MemberProfile.TwitterUrl,
      googlePage: this.props.navigation.state.params.MemberProfile.GooglePlusUrl,
      myProfileData: this.props.navigation.state.params.MyProfile,
      memberProfile: this.props.navigation.state.params.MemberProfile,
      profileAllData: this.props.navigation.state.params.profileAllData,
      redStarforFirstName: false,
      redStarforLastName: false,
      isPrivate: this.props.navigation.state.params.MemberProfile.IsPrivate,
      isPrimary: this.props.navigation.state.params.MemberProfile.IsPrimary,
      callingCode: finalCode,
      mobileNum: '',
      validMob: false,
      isLoading: false,
    })
  }

 
  componentWillUnmount(){
    MyProfile.refreshMyProfile();
    this.setState({
      firstName: "",
      lastName: "",
      jobTitle: "",
      professionalSummary: "",
      Phone: '',
      Ext: "",
      profileVideo: "",
      LinkedIn: "",
      Facebook: "",
      Twitter: "",
      myProfileData: [],
      memberProfile: [],
      profileAllData: [],
      mobileNum: '',
    })
  }

  /**
   *  this method for update profile data
   */
  _saveProfileData() {
    const {
      jobTitle,
      firstName,
      lastName,
      professionalSummary,
      Phone,
      Ext,
      profileVideo,
      LinkedIn,
      Facebook,
      Twitter,
      googlePage,
      isUncheck,
      isCheck,
      isPrimary,
      isPrivate,
      memberProfile
    } = this.state;

    const checkPhoneNo = memberProfile.Phone;
    const isPhone = globals.checkObject(memberProfile, 'Phone');
    const phoneNumber = (isPhone) ? (checkPhoneNo !== null ) ? memberProfile.Phone : Phone : Phone;
    const cntryCode =  (checkPhoneNo !== null ) ? phoneNumber.split(phoneNumber.slice(-11))[0] : +91
    const isNumber = (checkPhoneNo !== null ) ? (phoneNumber.length === 10) ? phoneNumber : phoneNumber.substring(4,) : "";
    const getNumber = (checkPhoneNo !== null ) ? ""+this.state.callingCode +" "+ this.state.Phone : this.state.Phone
    const isCode_Number = (this.state.callingCode == "") ? "1"+getNumber : getNumber

    const profileData =
    {
      "MemberProfile": {
        "ID": 0,
        "AppUserID": globals.userID,
        "JobTitle": jobTitle,
        "Biography": professionalSummary,
        "Phone": isCode_Number,
        "Extension": Ext,
        "LinkedInUrl": LinkedIn,
        "FacebookUrl": Facebook,
        "TwitterUrl": Twitter,
        "GooglePlusUrl": "",
        "YouTubeUrl": profileVideo,
        "ProfilePicturePath": this.state.profileImage,
        "CreatedDate": "2019-11-04T07:05:58.259Z",
        "CreatedBy": 0,
        "ModifiedDate": "2019-11-04T07:05:58.259Z",
        "ModifiedBy": 0,
        "IsPrimary": isPrimary,
        "DoesUserHasValidLicense": true,
        "IsPrivate": isPrivate,
        "IsEmailPrivate": isUncheck,
        "IsPhonePrivate": isCheck,
        "PasswordChangeRequired": true,
        "LastPasswordChangeDate": "2019-11-04T07:05:58.259Z"
      },
      "MemberInfo": {
        "UserID": globals.userID,
        "Username": "string",
        "FirstName": firstName,
        "LastName": lastName,
        "MI": "string",
        "EmailID": "string",
        "LocationDetailID": 0,
        "OrganizationID": 0,
        "OrganizationName": "string",
        "UserSince": "2019-11-04T07:05:58.259Z",
        "LastLoginDate": "2019-11-04T07:05:58.259Z",
        "LastLoginDateStr": "string",
        "RoleName": "string",
        "UserStatusID": 0,
        "UserStatus": "string",
        "MemberSince": "2019-11-04T07:05:58.259Z",
        "MemberSinceStr": "string",
        "CurrentLicenseStartDate": "2019-11-04T07:05:58.259Z",
        "CurrentLicenseEndDate": "2019-11-04T07:05:58.259Z",
        "CurrentMembershipStatus": true,
        "MembershipPlanID": 0,
        "MembershipPlanName": "string",
        "IsPrimary": true,
        "ProfilePicture": "string",
        "IsSpotlight": true,
        "OrgStatusID": 0,
        "CurrentLicenseStartDateStr": "string",
        "CurrentLicenseEndDateStr": "string",
        "FullName": "string",
        "Address": "string",
        "RowIndex": 0,
        "DT_RowId": "string",
        "DT_RowClass": "string",
        "DT_RowData": "string"
      },
      "Endorsements": [
        {
          "ID": 0,
          "SenderID": 0,
          "SenderName": "string",
          "ReceiverFirstName": "string",
          "SenderEmail": "string",
          "SenderJobTitle": "string",
          "SenderOrganization": "string",
          "ReceiverID": 0,
          "ReceiverName": "string",
          "ReceiverEmail": "string",
          "ReceiverJobTitle": "string",
          "ReceiverOrganization": "string",
          "Content": "string",
          "CreatedDate": "2019-11-04T07:05:58.259Z",
          "ModifiedDate": "2019-11-04T07:05:58.259Z",
          "IsActive": true,
          "StatusID": 0,
          "StatusText": "string",
          "SenderProfile": "string",
          "ReceiverProfile": "string",
          "CreatedDateStr": "string"
        }
      ],
      "BusinessCategoriesList": {
        "BizGroups": [
          {
            "ID": 0,
            "GroupName": "string",
            "BusinessCategories": [
              {
                "ID": 0,
                "Name": "string"
              }
            ]
          }
        ],
        "BizCategories": [
          {
            "ID": 0,
            "Name": "string"
          }
        ]
      },
      "Groups": [
        {
          "ID": 0,
          "Name": "string",
          "Description": "string",
          "ImagePath": "string",
          "GroupTypeID": 0,
          "CreatedDate": "2019-11-04T07:05:58.259Z",
          "CreatedBy": 0,
          "ModifiedDate": "2019-11-04T07:05:58.259Z",
          "ModifiedBy": 0,
          "IsActive": true,
          "Note": "string",
          "SubmitterNote": "string",
          "OwnerName": "string",
          "GroupTypeName": "string",
          "MembersCount": 0,
          "IsForEvent": true,
          "EventId": 0,
          "Address": "string",
          "City": "string",
          "State": "string",
          "ZipCode": "string",
          "Country": "string",
          "GroupCategoryId": 0,
          "GroupCategoryName": "string",
          "LinkedInUrl": "string",
          "FacebookUrl": "string",
          "TwitterUrl": "string",
          "GooglePlusUrl": "string",
          "YouTubeUrl": "string",
          "EmailID": "string",
          "GroupCreatorPicPath": "string",
          "LogoPath": "string",
          "PrivateToPublicIds": "string",
          "CommunityID": 0,
          "CommunityName": "string",
          "RowIndex": 0,
          "DT_RowId": "string",
          "DT_RowClass": "string",
          "DT_RowData": "string"
        }
      ],
      "Communities": [
        {
          "ID": 0,
          "Name": "string",
          "Description": "string",
          "TermsAndConditions": "string",
          "ImagePath": "string",
          "CommunityTypeID": 0,
          "CreatedDate": "2019-11-04T07:05:58.259Z",
          "CreatedBy": 0,
          "ModifiedDate": "2019-11-04T07:05:58.259Z",
          "ModifiedBy": 0,
          "IsActive": true,
          "Note": "string",
          "SubmitterNote": "string",
          "OwnerName": "string",
          "CommunityTypeName": "string",
          "MembersCount": 0,
          "Address": "string",
          "City": "string",
          "State": "string",
          "CommunityCategoryId": 0,
          "CommunityCategoryName": "string",
          "LinkedInUrl": "string",
          "FacebookUrl": "string",
          "TwitterUrl": "string",
          "GooglePlusUrl": "string",
          "EmailID": "string",
          "GroupCreatorPicPath": "string",
          "LogoPath": "string",
          "PrivateToPublicIds": "string",
          "CreatedDateStr": "string",
          "CommunityPurposeTypeId": "string",
          "OtherPurpose": "string",
          "CommunityThemeName": "string",
          "CommunityThemeId": 0,
          "CommunityStatus": 0,
          "IsaccesstoGlobalCommunities": true,
          "CommunityUpdate": "string",
          "Website": "string",
          "AccessToken": "string",
          "RowIndex": 0,
          "DT_RowId": "string",
          "DT_RowClass": "string",
          "DT_RowData": "string"
        }
      ],
      "CommunityInfo": {
        "ID": 0,
        "Name": "string",
        "Description": "string",
        "TermsAndConditions": "string",
        "ImagePath": "string",
        "CommunityTypeID": 0,
        "CreatedDate": "2019-11-04T07:05:58.260Z",
        "CreatedBy": 0,
        "ModifiedDate": "2019-11-04T07:05:58.260Z",
        "ModifiedBy": 0,
        "IsActive": true,
        "Note": "string",
        "SubmitterNote": "string",
        "OwnerName": "string",
        "CommunityTypeName": "string",
        "MembersCount": 0,
        "Address": "string",
        "City": "string",
        "State": "string",
        "CommunityCategoryId": 0,
        "CommunityCategoryName": "string",
        "LinkedInUrl": "string",
        "FacebookUrl": "string",
        "TwitterUrl": "string",
        "GooglePlusUrl": "string",
        "EmailID": "string",
        "GroupCreatorPicPath": "string",
        "LogoPath": "string",
        "PrivateToPublicIds": "string",
        "CreatedDateStr": "string",
        "CommunityPurposeTypeId": "string",
        "OtherPurpose": "string",
        "CommunityThemeName": "string",
        "CommunityThemeId": 0,
        "CommunityStatus": 0,
        "IsaccesstoGlobalCommunities": true,
        "CommunityUpdate": "string",
        "Website": "string",
        "AccessToken": "string",
        "RowIndex": 0,
        "DT_RowId": "string",
        "DT_RowClass": "string",
        "DT_RowData": "string"
      },
      "IsCRMConnectedMember": true
    }

    this.props.showLoader()
    if (globals.isInternetConnected === true) {
      API.UpdateProfile(this.UpateProfileResponseData, profileData, true)
    } else {
      this.props.hideLoader()
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }


  /**
   * Response callback of UpateProfileResponseData
   */
  UpateProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'UpateProfileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        MyProfile.refreshMyProfile();
        Alert.alert(globals.appName , response.Message,
          [{ text: 'OK', onPress: () => {this.props.showLoader(); this.props.navigation.goBack()}}],
          { cancelable: false }
      );
      } else {
        this.setState({ loading: false })
        Alert.alert(globals.appName, response.Message)
      }
      this.props.hideLoader()
    },
    error: err => {
      console.log(
        TAG,
        'UpateProfileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ loading: false });
      Alert.alert(globals.appName, err.Message)
      this.props.hideLoader();
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  _urlValidation(){
    const {profileVideo, LinkedIn, Facebook, Twitter} = this.state;
    if ((profileVideo == "") || (profileVideo == null) || Validation.youtubeUrlValidation(profileVideo)) {
      if ((LinkedIn == "") || (LinkedIn == null) || Validation.linkedInUrlValidation(LinkedIn)) {
        if ((Facebook == "") || (Facebook == null) || Validation.fbUrlValidation(Facebook)) {
          if ((Twitter == "") || (Twitter == null) || Validation.twitterUrlValidation(Twitter)) {
            this._saveProfileData();
          } else {
            Alert.alert(globals.appName, "Please enter valid Twitter URL")
          }
        } else {
          Alert.alert(globals.appName, "Please enter valid Facebook URL")
        }
      } else {
        Alert.alert(globals.appName, "Please enter valid LinkedIn URL")
      }
    } else {
      Alert.alert(globals.appName, "Please enter valid Youtube URL")
    }
  }

/**
*  this method for validation for firstName and lastName
*/
  _validationForEditProfile() {
    const { firstName, lastName, Phone } = this.state;
    const phoneLength = Phone.length;
    if (Validation.textInputCheck(firstName) && firstName !== "") {
      this.setState({ redStarforFirstName: true });
      if (Validation.textInputCheck(lastName) && lastName !== "") {
        if ((Phone == "") || (Phone == null) || Validation.connectProValidMobileNub(phoneLength)) {
          this.setState({ redStarforLastName: true });
          this._urlValidation();
        } else {
          Alert.alert(globals.appName, "The phone number is not valid");
        }
      } else {
        Alert.alert(globals.appName, "Last Name is Required");
        this.setState({ redStarforLastName: false });
      }
    } else {
      Alert.alert(globals.appName, "First Name is Required");
      this.setState({ redStarforFirstName: false });
    }
  }

  _cancelOnClick(){
    MyProfile.refreshMyProfile();
    this.props.navigation.goBack(null)
  }

  _isCheckClick() {
    let user_data = this.state.memberProfile;
    this.setState({ isCheck: true })
    if (this.state.isCheck == true) {
      user_data.IsPhonePrivate = true
    } else {
      user_data.IsPhonePrivate = false
    }
    this.setState({
      memberProfile: user_data
    });
  }
  _isCheckClickForCheck() {
    let user_data = this.state.memberProfile;
    this.setState({ isCheck: false })
    if (this.state.isCheck == false) {
      user_data.IsPhonePrivate = false
    } else {
      user_data.IsPhonePrivate = true
    }
    this.setState({
      memberProfile: user_data
    });
  }

  _isCheckClick2() {
    let user_data = this.state.memberProfile;
    this.setState({ isUncheck: true })
    if (this.state.isUncheck == true) {
      user_data.IsEmailPrivate = true
    } else {
      user_data.IsEmailPrivate = false
    }
    this.setState({
      memberProfile: user_data
    });
  }
  _isCheckClickForUncheck() {
    let user_data = this.state.memberProfile;
    this.setState({ isUncheck: false })
    if (this.state.isUncheck == false) {
      user_data.IsEmailPrivate = false
    } else {
      user_data.IsEmailPrivate = true
    }
    this.setState({
      memberProfile: user_data
    });
  }

/**
*  this method for firstName onChangeText
*/
  firstNameChange(text) {
    let user_data = this.state.myProfileData;
    this.setState({ firstName: text, }, () => {
      user_data.FirstName = this.state.firstName,
        this.setState({
          myProfileData: user_data
        });
    })
  }

/**
*  this method for lastName onChangeText
*/
  lastNameChange(text) {
    let user_data = this.state.myProfileData;
    this.setState({ lastName: text, }, () => {
      user_data.LastName = this.state.lastName,
        this.setState({
          myProfileData: user_data
        });
    })
  }

/**
*  this method for job title onChangeText
*/
  jobTitleChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ jobTitle: text, }, () => {
      user_data.JobTitle = this.state.jobTitle,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for professional Summary onChangeText
*/
  professionalSummaryChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ professionalSummary: text, }, () => {
      user_data.Biography = this.state.professionalSummary,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for phoneNumber onChangeText
*/
  phoneChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ Phone: text, }, () => {
      user_data.Phone = this.state.Phone,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for extension onChangeText
*/
  extensionChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ Ext: text.replace(/[^0-9]/g, ''), }, () => {
      user_data.Extension = this.state.Ext,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for googlePlus url onChangeText
*/
  profileVideoChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ profileVideo: text, }, () => {
      user_data.YouTubeUrl = this.state.profileVideo,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for linkedIn url onChangeText
*/
  linkedInChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ LinkedIn: text, }, () => {
      user_data.LinkedInUrl = this.state.LinkedIn,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for facebook url onChangeText
*/
  facebookChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ Facebook: text, }, () => {
      user_data.FacebookUrl = this.state.Facebook,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
*  this method for twitter url onChangeText
*/

  twitterChange(text) {
    let user_data = this.state.memberProfile;
    this.setState({ Twitter: text, }, () => {
      user_data.TwitterUrl = this.state.Twitter,
        this.setState({
          memberProfile: user_data
        });
    })
  }

/**
  *  this method for visible private view
*/

  toggleButton1() {
    let user_data = this.state.memberProfile;
    this.setState({ isPrivate: true, isPrimary: false }, () => {
      user_data.IsPrivate = true,
        user_data.IsPrimary = false,
        this.setState({
          memberProfile: user_data
        });
    })
  }

 /**
  *  this method for visible public view
  */
  toggleButton2() {
    let user_data = this.state.memberProfile;
    this.setState({ isPrimary: true, isPrivate: false, }, () => {
      user_data.IsPrimary = true,
        user_data.IsPrivate = false,
        this.setState({
          memberProfile: user_data
        });
    })
  }

  getCode(value){
    let CodeArray = globals.CountryCodeArray
    for(let i = 0; i < CodeArray.length; i++){
      let name = CodeArray[i].name
      if(name == value.name){
        const finalValue = (CodeArray[i].dial_code) ? CodeArray[i].dial_code : '69';
        console.log("finalValue ::", finalValue); 
        this.setState({ cca2: value.cca2, callingCode: (!value.callingCode) ? finalValue : value.callingCode });  
      }
    }
  }

  render() {
    const { LinkedIn, Facebook, Twitter, isCheck, firstName, lastName, isUncheck, jobTitle, professionalSummary, Phone, profileVideo, Ext, cca2, callingCode, country, memberProfile } = this.state;
    const isFirstName = globals.checkObject(this.state.myProfileData, 'FirstName');
    const isLastName = globals.checkObject(this.state.myProfileData, 'LastName');
    const isJobTitle = globals.checkObject(this.state.memberProfile, 'JobTitle');
    const isBiography = globals.checkObject(this.state.memberProfile, 'Biography');
    const isPhone = globals.checkObject(this.state.memberProfile, 'Phone');
    const isExtension = globals.checkObject(this.state.memberProfile, 'Extension');
    const isProfileVideo = globals.checkObject(this.state.memberProfile, 'YouTubeUrl');
    const isLinkedIn = globals.checkObject(this.state.memberProfile, 'LinkedInUrl');
    const isFacebook = globals.checkObject(this.state.memberProfile, 'FacebookUrl');
    const isTwitter = globals.checkObject(this.state.memberProfile, 'TwitterUrl');
    const isPrivate = globals.checkObject(this.state.memberProfile, 'IsPrivate');
    const isPrimary = globals.checkObject(this.state.memberProfile, 'IsPrimary');
    const isPhonePrivate = globals.checkObject(this.state.memberProfile, 'IsPhonePrivate');
    const isEmailPrivate = globals.checkObject(this.state.memberProfile, 'IsEmailPrivate');
    const checkPhoneNo = memberProfile.Phone;
    const phoneNumber = (isPhone) ?  (checkPhoneNo !== null ) ? this.state.memberProfile.Phone : Phone : Phone;
    const cntryCode = (checkPhoneNo !== null ) ? phoneNumber.split(phoneNumber.slice(-11))[0] : +91;
    const isNumber = (checkPhoneNo !== null ) ? (phoneNumber.length === 10) ? phoneNumber : phoneNumber.substring(4) : Phone;
    const getNumber = (checkPhoneNo !== null ) ? (phoneNumber.length === 10) ? phoneNumber : isNumber.replace(/ /g, '') : Phone;
    const getMyNumber =  (checkPhoneNo !== null ) ?  "+" + this.state.callingCode + " " + this.state.Phone : this.state.Phone
    const isCallingCode = this.state.callingCode;
  

    return (
      <View style={styles.ep_container}>
        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
        <KeyboardAwareScrollView contentContainerStyle={{flex: 1}}>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>
                First Name
              {this.state.redStarforFirstName === false &&
                  this.state.firstName === '' ? (
                    <Text style={styles.ep_redStarStyle}>*</Text>
                  ) : null}
              </Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholder="First Name"
                placeholderTextColor={colors.lightGray}
                onChangeText={(text) => this.firstNameChange(text)}
                returnKeyType="next"
                value={(isFirstName) ? this.state.myProfileData.FirstName : firstName}
                onSubmitEditing={() => this.LastNameRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>
                Last Name
                {this.state.redStarforLastName === false &&
                  this.state.lastName === '' ? (
                    <Text style={styles.ep_redStarStyle}>*</Text>
                  ) : null}
              </Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholder="Last Name"
                placeholderTextColor={colors.lightGray}
                onChangeText={(text) => this.lastNameChange(text)}
                returnKeyType="next"
                value={(isLastName) ? this.state.myProfileData.LastName : lastName}
                ref={LastNameRef => (this.LastNameRef = LastNameRef)}
                onSubmitEditing={() => this.JobTitleRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Job Title</Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholderTextColor={colors.lightGray}
                placeholder="Job Title"
                onChangeText={(text) => this.jobTitleChange(text)}
                returnKeyType="default"
                value={(isJobTitle) ? this.state.memberProfile.JobTitle : jobTitle}
                ref={JobTitleRef => (this.JobTitleRef = JobTitleRef)}
                autoCapitalize="none">

              </TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Professional Summary</Text>
              <TextInput style={styles.ep_contentText_H}
                multiline={true}
                maxLength={300}
                placeholderTextColor={colors.lightGray}
                placeholder="Professional Summary Details"
                onChangeText={(text) => this.professionalSummaryChange(text)}
                 returnKeyType="default"
                value={(isBiography) ? this.state.memberProfile.Biography : professionalSummary}
                autoCapitalize="none">

              </TextInput>
            </View>
          </View>
          <View style={[{ flexDirection: 'row' }]}>
            <View style={[styles.ep_mainView, {width:'50%', paddingRight: globals.screenWidth * 0.04 }]}>
              <Text style={styles.ep_titleStyle}>Phone</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <CountryPicker
                  isFrom={'editMyProfile'}
                  isNumber={isCallingCode}
                  countryList={globals.COUNTRY_LIST}
                  onChange={value => {
                    console.log("value :----", value);
                    this.getCode(value)
                    // this.setState({ cca2: value.cca2, callingCode: (!value.callingCode) ? "246" : value.callingCode });
                  }}
                  cca2={cca2}
                  translation="eng"
                />
                <TextInput
                  style={[styles.ep_contentText2,{paddingBottom:globals.screenHeight * 0.0129}]}
                  placeholderTextColor={colors.lightGray}
                  placeholder="Mobile Number"
                  onChangeText={(text) => this.phoneChange(text)}
                  returnKeyType="done"
                  keyboardType="number-pad"
                  value={Phone}
                  maxLength={15}
                  onSubmitEditing={() => this.ExtRef.focus()}
                  autoCapitalize="none"></TextInput>
              </View>
            </View>
            <View style={[styles.ep_spacebetweeenView,{width: '50%'}]}>
              <Text style={styles.ep_spacebetweenTitleStyle}>Extension</Text>
              <TextInput
                style={[styles.ep_spacebetweenContentStyle,{width:'70%'}]}
                placeholderTextColor={colors.lightGray}
                placeholder="Extension"
                onChangeText={(text) => this.extensionChange(text)}
                returnKeyType="done"
                maxLength={4}
                keyboardType={"numeric"}
                value={(isExtension) ? this.state.memberProfile.Extension : Ext}
                ref={ExtRef => (this.ExtRef = ExtRef)}
                onSubmitEditing={() => this.ProfileVideoRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Profile Video</Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholderTextColor={colors.lightGray}
                placeholder="https://www.youtube.com/watch?"
                onChangeText={(text) => this.profileVideoChange(text)}
                returnKeyType="next"
                value={(isProfileVideo) ? this.state.memberProfile.YouTubeUrl : profileVideo}
                ref={ProfileVideoRef => (this.ProfileVideoRef = ProfileVideoRef)}
                onSubmitEditing={() => this.LinkedInRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Linkedin Profile</Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholderTextColor={colors.lightGray}
                placeholder="https://www.linkedin.com/profile/"
                onChangeText={(text) => this.linkedInChange(text)}
                returnKeyType="next"
                value={(isLinkedIn) ? this.state.memberProfile.LinkedInUrl : LinkedIn}
                ref={LinkedInRef => (this.LinkedInRef = LinkedInRef)}
                onSubmitEditing={() => this.FacebookRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Facebook Profile</Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholderTextColor={colors.lightGray}
                placeholder="https://www.facebook.com/profile/"
                onChangeText={(text) => this.facebookChange(text)}
                returnKeyType="next"
                value={(isFacebook) ? this.state.memberProfile.FacebookUrl : Facebook}
                ref={FacebookRef => (this.FacebookRef = FacebookRef)}
                onSubmitEditing={() => this.TwitterRef.focus()}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.ep_mainView}>
            <View>
              <Text style={styles.ep_titleStyle}>Twitter</Text>
              <TextInput
                style={styles.ep_contentText_H}
                placeholderTextColor={colors.lightGray}
                placeholder="https://www.twitter.com/profile/"
                onChangeText={(text) => this.twitterChange(text)}
                returnKeyType="done"
                value={(isTwitter) ? this.state.memberProfile.TwitterUrl : Twitter}
                ref={TwitterRef => (this.TwitterRef = TwitterRef)}
                autoCapitalize="none"></TextInput>
            </View>
          </View>
          <View style={styles.buttonsView}>
            <View style={styles.bottomStyle}>
              <Text style={styles.ep_titleStyle}>Profile Visibility</Text>
              <View style={[styles.directionView]}>
                <TouchableOpacity
                  style={[
                    styles.roundViewStyle,
                    {
                      backgroundColor: this.state.isPrimary === true ? colors.warmBlue : null,
                    },
                  ]}
                  onPress={() => this.toggleButton2()}
                />
                <Text style={styles.ep_textStyle2}>Public</Text>
                <TouchableOpacity
                  style={[
                    styles.roundViewStyle,
                    {
                      backgroundColor: this.state.isPrivate === true ? colors.warmBlue : null,
                    },
                  ]}
                  onPress={() => this.toggleButton1()}
                />
                <Text style={styles.ep_textStyle2}>Private</Text>
              </View>
              {(this.state.isPrimary == true) ?
                <View style={[styles.directionView]}>
                  <TouchableOpacity
                    onPress={() => (isUncheck == false) ? this._isCheckClick2() : this._isCheckClickForUncheck()}
                    style={[styles.squareView, { marginLeft: globals.screenWidth * 0.055 }]}
                  >
                    {this.state.isUncheck === true ? (
                      <Icon name="check" color={colors.black} size={15} />
                    ) : null}
                  </TouchableOpacity>
                  <Text style={styles.ep_textStyle2}>Email Is Private</Text>
                  <TouchableOpacity
                    onPress={() => (isCheck == false) ? this._isCheckClick() : this._isCheckClickForCheck()}
                    style={[styles.squareView, { marginLeft: globals.screenWidth * 0.025 }]}
                  >
                    {this.state.isCheck === true ? (
                      <Icon name="check" color={colors.black} size={15} />
                    ) : null}
                  </TouchableOpacity>
                  <Text style={styles.ep_textStyle2}>Phone Is Private</Text>
                </View>
                :
                null
              }
            </View>
          </View>
          <View style={styles.ep_buttonStyle}>
            <TouchableOpacity
              onPress={() => this._validationForEditProfile()}
              style={[styles.ep_touchableStyle]}>
              <View>
                <Text style={styles.ep_btntextStyless}>{globals.BTNTEXT.PAYMENTSCREENS.SAVE}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._cancelOnClick()}
              style={[styles.touchableStyle2, { marginLeft: globals.screenWidth * 0.02 }]}
            >
              <View>
                <Text style={styles.btntextStyless}>{globals.BTNTEXT.PAYMENTSCREENS.CANCLE}</Text>
              </View>
            </TouchableOpacity>
          </View>
          </KeyboardAwareScrollView>
        </ScrollView>
      </View>
    );
  }
}
// ********************** Model mapping method **********************
const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfile);