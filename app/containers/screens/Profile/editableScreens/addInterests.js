/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    SafeAreaView,
    TextInput,
    Alert,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
} from 'react-native';
import * as globals from '../../../../utils/globals';
import globalStyles from '../../../../assets/styles/globleStyles';
import { TagSelect } from '../../../../libs/react-native-tag-select';
import styles from '../userProfileTabs/style';
import * as colors from '../../../../assets/styles/color';
import { API } from '../../../../utils/api';
import SkillsAndInterest from '../userProfileTabs/skillsAndInterests';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import tagStyle from '../userProfileTabs/style';
import KeyboardListener from 'react-native-keyboard-listener';

let TAG = "Myprofile AddInterests:=="
let _this;
class AddInterests extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Interests'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            interestsData: this.props.navigation.state.params.interestsData,
            skillsData : this.props.navigation.state.params.skillsData,
            addTag: '',
            interest_Data: [],
            keyboardOpen: false,
            deleteStatus: false,
        };
    }

    _cancelOnClick(){
        SkillsAndInterest._skillAndInterestApiCall()
        this.props.navigation.goBack()
      }

      componentWillUnmount(){
        SkillsAndInterest._skillAndInterestApiCall()
      }

      static deleteInterestItem() {
        _this.setState({ deleteStatus: true })
      }
    /**
     * click on save button
     */
    saveProfileCategoriesAPI() {
        if (this.state.interest_Data.length > 0 || this.state.deleteStatus == true ) {
        if (this.state.interestsData.length !== this.state.interest_Data.length || this.state.deleteStatus == true || this.state.addTag == "") {
            if ((this.state.interest_Data.length > 0) || (this.state.addTag == "")) {
                let arrInterestsData = [];
                let arrSkillsData = [];
                let interestsDataArr = this.state.interest_Data
                arrInterestsData = this.props.navigation.state.params.interestsData
                arrSkillsData = this.props.navigation.state.params.skillsData
                var merge = (a, b, p) => a.filter(aa => !b.find(bb => aa[p] === bb[p])).concat(b);
                let final_Arr = merge(arrInterestsData, interestsDataArr, "Name");
                let nameValues = final_Arr;
                let namesArray = [];
                let uNamesValue = new Map(nameValues.map(s => [s.Name.toLowerCase(), s.Name]));
                let finalValues = [...uNamesValue.values()]
                for (let i = 0; i < finalValues.length; i++) {
                    let newObj = {
                        Name: finalValues[i]
                    }
                    namesArray.push(newObj)
                }
                const data = {
                    UserId: globals.userID,
                    Skills: arrSkillsData,
                    Interests: namesArray
                }
                this.props.showLoader()
                if (globals.isInternetConnected === true) {
                    API.saveSkillsInterests(this.SaveSkillsResponseData, data, true)
                } else {
                    this.props.hideLoader()
                    Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
                }
            } else {
                Alert.alert(globals.appName, "Please add interests")
            }
        } else {
            Alert.alert(globals.appName, "Please add interests")
        }
    } else {
        Alert.alert(globals.appName, "Please add interests")
    }
    }

    /**
       * Response callback of SavePortfolio ResponseData
       */
    SaveSkillsResponseData = {
        success: response => {
            console.log(
                TAG,
                'SaveSkillsResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                this.setState({deleteStatus: false})
                Alert.alert(globals.appName , response.Message,
                    [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
                    { cancelable: false }
                );
            } else {
                this.setState({ loading: false })
                console.log(globals.appName, response.Message)
            }
            this.props.hideLoader()
        },
        error: err => {
            console.log(
                TAG,
                'SaveSkillsInterestsResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            this.setState({ loading: false });
            Alert.alert(globals.appName, err.Message)
            this.props.hideLoader();
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    _addInterest() {
        const { interest_Data, addTag } = this.state;
          let regex = /^[^\s]+(\s+[^\s]+)*$/;
            if (!regex.test(addTag) || (addTag == "")) {
                if (interest_Data.length < 0) {
                    Alert.alert(globals.appName, "Please add interests",
                        [{ text: 'OK', onPress: () => this.setState({ addTag: '' }) }])
                }
            } else {
                let tempArr = interest_Data;
                tempArr.push({ Name: addTag });
                this.setState({ interest_Data: tempArr });
            }
            this.setState({ addTag: ""})
        } 
      
      onSubmitt() {
        this.setState({ addTag: '' })
      }


    render() {
        const { interestsData } = this.state;
        return (
            <SafeAreaView style={[styles.container, (globals.iPhoneX) ?
                { marginTop: 35 }
                :
                { paddingTop: 35 }]}>
                <KeyboardListener
                    onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                    onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                <ScrollView
                    style={styles.scrollViewContainer}
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                >
                    {
                        (interestsData != null) ?
                            <TagSelect
                                isFrom="ProfileCategory"
                                isDelete="Ineterest"
                                data={interestsData}
                                itemStyle={styles.item}
                                itemLabelStyle={styles.label}
                                itemStyleSelected={styles.itemSelected}
                                itemLabelStyleSelected={styles.labelSelected}
                            /> : null
                    }

                </ScrollView>
                <KeyboardAvoidingView behavior={(Platform.OS == "ios") ? "padding" : "height"} keyboardVerticalOffset={(Platform.OS == "ios") ? 100 : 0}>
                <View style={styles.addCategory_ViewStyle}>
                    <View style={styles.addCategory_childViewStyle}>
                        <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
                            <TagSelect
                                isFrom="ProfileCategory"
                                data={this.state.interest_Data}
                                itemStyle={tagStyle.item}
                                itemLabelStyle={tagStyle.label}
                                itemStyleSelected={tagStyle.itemSelected}
                                itemLabelStyleSelected={tagStyle.labelSelected}
                            />
                        </ScrollView>
                    </View>
                    <View style={styles.mainInputViewContaier}>
                        <View style={styles.inputViewContainer}>
                            <TextInput
                                value={this.state.addTag}
                                autoCapitalize="none"
                                blurOnSubmit={(this.state.addTag == '') ? true : false}
                                returnKeyType="done"
                                onChangeText={text => this.setState({ addTag: text })}
                                placeholderTextColor={colors.darkgrey}
                                placeholder="Add your interests"
                                style={styles.textInputContainer}
                                onSubmitEditing={() => (this.state.addTag == '') ? this.onSubmitt() : this._addInterest()}
                            />
                        </View>
                    </View>
                </View>
                </KeyboardAvoidingView>
                {(Platform.OS == "android") ?
                    (this.state.keyboardOpen == false) ?
                        <View
                            style={styles.buttonViewContainer}
                        >
                            <TouchableOpacity
                                onPress={() => this.saveProfileCategoriesAPI()}
                                style={styles.saveButtonContariner}
                            >
                                <Text style={styles.saveAndCancelTextStyle}>Save</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this._cancelOnClick()}
                                style={styles.cancelButtonContainer}
                            >
                                <Text style={styles.saveAndCancelTextStyle}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <View style={{ marginTop: 20, marginBottom: globals.iPhoneX ? 0 : globals.screenHeight * 0.01, }} />
                    :
                    <View
                        style={styles.buttonViewContainer}
                    >
                        <TouchableOpacity
                            onPress={() => this.saveProfileCategoriesAPI()}
                            style={styles.saveButtonContariner}
                        >
                            <Text style={styles.saveAndCancelTextStyle}>Save</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this._cancelOnClick()}
                            style={styles.cancelButtonContainer}
                        >
                            <Text style={styles.saveAndCancelTextStyle}>Cancel</Text>
                        </TouchableOpacity>
                    </View>}
            </SafeAreaView>
        );
    }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddInterests);
