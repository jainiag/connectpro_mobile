import { StyleSheet, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({

  //=========: Add Portfolio :=======
  portfolio_mainContainer: {
    flex: 1,
    marginTop: globals.screenHeight * 0.018,
  },
  portfolio_mainView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  redStarViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  redStarStyle: {
    color: colors.redColor,
    fontSize: globals.font_10
  },
  portfolio_TitleinputTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    color: colors.darkgrey,
    marginLeft: globals.screenWidth * 0.05,
  },
  portfolio_inputContainer: {
    marginTop: (Platform.OS == 'android') ? -globals.screenHeight * 0.0065 : globals.screenHeight * 0.0065,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginLeft:(Platform.OS == 'android') ? globals.screenWidth * 0.04 : globals.screenWidth * 0.05,
  },
  portfolio_buttonViewContainer: {
    marginTop: globals.screenHeight * 0.015,
    marginHorizontal: globals.screenWidth * 0.25,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.03,
    position: 'absolute',
    bottom: globals.screenHeight * 0.02,
    right: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.09 : 0,
  },
  portfolio_SaveButton: {
    paddingHorizontal: 22,
    paddingVertical: 5,
    backgroundColor: colors.warmBlue,
  },
  portfolio_CancelButton: {
    marginLeft: globals.screenWidth * 0.02,
    paddingHorizontal: 22,
    paddingVertical: 5,
    backgroundColor: colors.gray,
  },
  portfolio_buttonTextStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    color: colors.white
  },
  scrollStyle: {
    flex: 1
  },
  viewcontainer: {
    height: globals.screenHeight * 0.20,
    width: globals.screenWidth,
    alignItems: 'center',
    justifyContent: 'center',

  },
  beforeimgviewStyle: {
    alignSelf: 'center',
    width: globals.screenWidth * 0.25,
    height: globals.screenWidth * 0.25,
    borderColor: colors.lightgray,
    borderWidth: 0.2,
    borderRadius: (globals.screenWidth * 0.25) / 2
  },
  editimgStyle: {
    alignSelf: 'center',
    width: globals.screenWidth * 0.25,
    height: globals.screenWidth * 0.25,
    borderColor: colors.white,
    borderWidth: 0.2,
    borderRadius: (globals.screenWidth * 0.25) / 2
  },
  ep_container: {
    flex: 1,
    marginTop: globals.screenHeight * 0.018,
  },
  ep_mainView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  ep_spacebetweeenView: {
    marginTop: globals.screenHeight * 0.01,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
    width: globals.screenWidth,
    marginLeft: globals.screenWidth * 0.04,
  },
  ep_spacebetweenTitleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.darkgrey,
  },
  ep_spacebetweenContentStyle: {
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.0065,
    paddingBottom: globals.screenHeight * 0.01,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
  },
  ep_titleStyle: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_10,
    color: colors.darkgrey,
    marginLeft: globals.screenWidth * 0.05,
  },
  ep_contentText: {
    marginTop: (Platform.OS === 'ios') ? globals.screenHeight * 0.0065 : 0,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginHorizontal: globals.screenWidth * 0.05,
  },
  roundViewStyle: {
    marginTop: globals.screenHeight * 0.0065,
    height: globals.screenHeight * 0.025,
    width: globals.screenHeight * 0.025,
    borderColor: colors.gray,
    borderWidth: 1,
    marginLeft: globals.screenWidth * 0.05,
    borderRadius: (globals.screenHeight * 0.025) / 2,
  },
  directionView: {
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.001,
  },
  ep_textStyle: {
    marginTop: globals.screenHeight * 0.0065,
    marginLeft: globals.screenWidth * 0.015,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 19 : globals.font_14,
  },
  squareView: {
    marginTop: globals.screenHeight * 0.0065,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  ep_buttonStyle: {
    marginTop: globals.screenHeight * 0.015,
    marginHorizontal: globals.screenWidth * 0.25,
    // margin:25,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    paddingBottom: globals.screenHeight * 0.03,
  },
  btntextStyless: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    // color:colors.white
  },
  bottomStyle: {
    paddingBottom: globals.screenHeight * 0.01,
  },
  buttonsView: {
    marginTop: globals.screenHeight * 0.01,
  },
  touchableStyle: {
    paddingHorizontal: 22,
    paddingVertical: 5,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  ep_contentText_H: {
    marginTop: globals.screenHeight * 0.0065,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginLeft: globals.screenWidth * 0.05,
  },
  ep_contentText2: {
    marginTop: globals.screenHeight * 0.0065,
    color: colors.lightBlack,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 22 : globals.font_14,
    paddingBottom: globals.screenHeight * 0.01,
    marginLeft: globals.screenWidth * 0.015,
  },
  ep_textStyle2: {
    color: colors.lightBlack,
    marginTop: globals.screenHeight * 0.0065,
    marginLeft: globals.screenWidth * 0.015,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 19 : globals.font_14,
  },
  ep_btntextStyless: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 18 : globals.font_14,
    color: colors.white
  },
  ep_touchableStyle: {
    paddingHorizontal: 22,
    paddingVertical: 5,
    backgroundColor: colors.warmBlue,
  },
  touchableStyle2: {
    paddingHorizontal: 22,
    paddingVertical: 5,
    backgroundColor: colors.gray,
  },
  ep_redStarStyle: {
    fontSize: globals.font_15,
    color: colors.redColor,
  },
})
