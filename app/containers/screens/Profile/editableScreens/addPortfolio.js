import React from 'react';
import { Text, View, TouchableOpacity , TextInput, Alert } from 'react-native';
import * as globals from '../../../../utils/globals';
import * as colors from '../../../../assets/styles/color';
import globalStyles from '../../../../assets/styles/globleStyles';
import styles from './style';
import { API } from '../../../../utils/api';
import PortfolioTab from '../userProfileTabs/portfolioTab';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { showLoader, hideLoader } from '../../../../redux/acrions/showLoader';
import Validation from '../../../../utils/validation';
import KeyboardListener from 'react-native-keyboard-listener';

let TAG = 'Add Portfolio ::====';

class AddPortfolio extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params } = navigation.state;
        const fillValue = params.fillData;
        return {
        headerLeft: globals.ConnectProbackButton(navigation, (fillValue == true) ? 'Edit Portfolio' : 'Add Portfolio'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
        }
    };
    constructor(props) {
        super(props);
        this.state = {
            title: '',
            link: '',
            _Id: 0,
            fillData: this.props.navigation.state.params.fillData,
            loading: false,
            redStarforTitle: false,
            redStarforlink: false,
            keyboardOpen: false,
        }
    }

    componentDidMount() {
        /**
         * update state when fillData will true
         */
        if (this.props.navigation.state.params.fillData == true) {
            this.setState({ title: this.props.navigation.state.params.PortfolioData.Title })
            this.setState({ link: this.props.navigation.state.params.PortfolioData.Link })
            this.setState({ _Id: this.props.navigation.state.params.PortfolioData.Id })
        }
    }


    /**
         * _validation For AddPortfolio
         */
    _validationForAddPortfolio() {
        const { title, link } = this.state;
        if (Validation.textInputCheck(title) && title !== "") {
            this.setState({ redStarforTitle: true });
            if (Validation.textInputCheck(link) && link !== "") {
                this.setState({ redStarforlink: true });
                if (Validation.validateURL(link)) {
                    this.setState({ redStarforlink: true });
                    this._savePortfolioClick();
                } else {
                    Alert.alert(globals.appName, "Please enter valid link");
                    this.setState({ redStarforlink: false });
                }
            } else {
                Alert.alert(globals.appName, "Portfolio Link is Required");
                this.setState({ redStarforlink: false });
            }
        } else {
            Alert.alert(globals.appName, "Portfolio Title is Required");
            this.setState({ redStarforTitle: false });
        }
    }

    /**
     *  this method for Add portfolio and Edit Portfolio
     */

    _savePortfolioClick() {
        const { title, link, _Id } = this.state;
        let arrLinks = []
        if (this.props.navigation.state.params.fillData == true) {

            arrLinks = this.props.navigation.state.params.PortfolioAllData
            let index = this.props.navigation.state.params.index
            let object = this.props.navigation.state.params.PortfolioAllData[index]
            object['Title'] = title
            object['Link'] = link
            arrLinks[index] = object
        } else {
            arrLinks = this.props.navigation.state.params.PortfolioData
            arrLinks.push({
                Id: this.props.navigation.state.params.PortfolioData.length + 1,
                Title: title,
                Link: link,
            });
        }

        const data = {
            UserId: globals.userID,
            Links: arrLinks
        }
        this.props.showLoader()
        if (globals.isInternetConnected === true) {
            API.savePortfolioInfo(this.SavePortfolioResponseData, data, true)
        } else {
            this.props.hideLoader()
            Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
        }
    }

    /**
    * Response callback of SavePortfolio ResponseData
    */
    SavePortfolioResponseData = {
        success: response => {
            console.log(
                TAG,
                'SavePortfolioResponseData -> success : ',
                JSON.stringify(response)
            );
            if (response.StatusCode == 200) {
                PortfolioTab.refreshPortfolio()
                Alert.alert(globals.appName , (this.state.fillData == true) ? "Portfolio updated" : response.Message,
                    [{ text: 'OK', onPress: () => this.props.navigation.goBack() }],
                    { cancelable: false }
                );
            } else {
                Alert.alert(globals.appName, response.Message)
                this.setState({ loading: false })
            }
            this.props.hideLoader()
        },
        error: err => {
            console.log(
                TAG,
                'SavePortfolioResponseData -> ERROR : ',
                JSON.stringify(err.Message)
            );
            this.setState({ loading: false });
            Alert.alert(globals.appName, err.Message)
            this.props.hideLoader();
        },
        complete: () => {
            this.props.hideLoader();
            this.setState({ loading: false })
        },
    };

    render() {
        const buttonText = (this.state.fillData == true) ? "Update" : globals.BTNTEXT.PAYMENTSCREENS.SAVE ;
        return (
            <View style={styles.portfolio_mainContainer}>
                <KeyboardListener
                    onDidShow={() => { this.setState({ keyboardOpen: true }, () => { console.log('onDidShow') }); }}
                    onDidHide={() => { this.setState({ keyboardOpen: false }, () => { console.log('onDidHide') }); }} />
                <View style={styles.portfolio_mainView}>
                    <View>
                        <View style={styles.redStarViewContainer}>
                            <Text style={styles.portfolio_TitleinputTextStyle}>Title</Text>
                            {(this.state.redStarforTitle === false && this.state.title === '') ?
                                <Text style={styles.redStarStyle}>*</Text>
                                :
                                null
                            }
                        </View>
                        <TextInput
                            style={styles.portfolio_inputContainer}
                            placeholder="Title"
                            placeholderTextColor={colors.lightGray}
                            onChangeText={(text) => this.setState({ title: text })}
                            returnKeyType="next"
                            value={this.state.title}
                            onSubmitEditing={() => this.linkRef.focus()}
                            autoCapitalize="none"></TextInput>
                    </View>
                </View>
                <View style={styles.portfolio_mainView}>
                    <View>
                        <View style={styles.redStarViewContainer}>
                            <Text style={styles.portfolio_TitleinputTextStyle}>Link</Text>
                            {(this.state.redStarforlink === false && this.state.link === '') ?
                                <Text style={styles.redStarStyle}>*</Text>
                                :
                                null
                            }
                        </View>
                        <TextInput
                            style={styles.portfolio_inputContainer}
                            placeholder="http://example.com/"
                            placeholderTextColor={colors.lightGray}
                            onChangeText={(text) => this.setState({ link: text })}
                            returnKeyType="done"
                            value={this.state.link}
                            ref={linkRef => (this.linkRef = linkRef)}
                            autoCapitalize="none"></TextInput>
                    </View>
                </View>
                {(Platform.OS == "android") ?
                    (this.state.keyboardOpen == false) ?
                        <View style={styles.portfolio_buttonViewContainer}>
                            <TouchableOpacity style={styles.portfolio_SaveButton} onPress={() => this._validationForAddPortfolio()}>
                                <View>
                                    <Text style={styles.portfolio_buttonTextStyle}>{buttonText}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                                style={styles.portfolio_CancelButton}>
                                <View>
                                    <Text style={styles.portfolio_buttonTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.CANCLE}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        :
                        null
                    :
                    <View style={styles.portfolio_buttonViewContainer}>
                        <TouchableOpacity style={styles.portfolio_SaveButton} onPress={() => this._validationForAddPortfolio()}>
                            <View>
                                <Text style={styles.portfolio_buttonTextStyle}>{buttonText}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}
                            style={styles.portfolio_CancelButton}>
                            <View>
                                <Text style={styles.portfolio_buttonTextStyle}>{globals.BTNTEXT.PAYMENTSCREENS.CANCLE}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                }
            </View>
        );
    }
}


// ********************** Model mapping method **********************

const mapStateToProps = state => {
    return {
        loader: state.loaderRed.loader,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            showLoader,
            hideLoader,
        },
        dispatch
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddPortfolio);