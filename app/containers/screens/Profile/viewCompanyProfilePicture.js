/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { View,Image } from 'react-native';
import * as globals from '../../../utils/globals';
import globalStyles from '../../../assets/styles/globleStyles';
import ImageViewer from '../../../libs/react-native-image-zoom-viewer';
import FastImage from 'react-native-fast-image'


export default class ViewCompanyProfilePicture extends React.Component {

  static navigationOptions = ({ navigation }) => ({
    headerLeft: globals.ConnectProbackButton(navigation, 'Organization logo'),
    headerStyle: globalStyles.ConnectPropheaderStyle,
  });

  constructor(props) {
    super(props);
    this.state = {
      sourceImage: this.props.navigation.state.params.sourceImage,
      ProfileImage: this.props.navigation.state.params.ProfileImage,
      placeHolder: this.props.navigation.state.params.placeHolder,
      index: 0,
    }
  }


  render() {
    const images = [{
      url: (this.state.placeHolder == undefined) ?
        (this.state.sourceImage != undefined) ? this.state.sourceImage : this.state.ProfileImage
        : this.state.placeHolder,
    },]
    return (
      <View style={{ flex: 1 }}>
        <ImageViewer renderImage={(props) => {
          return (
            <FastImage
              style={{ flex: 1, width: globals.screenWidth, height: 220 }}
              source={{
                uri: (this.state.placeHolder == undefined) ?
                  (this.state.sourceImage != undefined) ? this.state.sourceImage : this.state.ProfileImage
                  : this.state.placeHolder,
                priority: FastImage.priority.normal,
              }}
            />
          )
        }} loadingRender={() => {
          return (
            <Image
              style={{ flex: 1, width: globals.screenWidth, height: 220 }}
              source={{
                uri: (this.state.placeHolder == undefined) ?
                  (this.state.sourceImage != undefined) ? this.state.sourceImage : this.state.ProfileImage
                  : this.state.placeHolder,

              }}
            />
          )
        }}
          imageUrls={images} index={this.state.index}
        />
      </View>
    );
  }
}
