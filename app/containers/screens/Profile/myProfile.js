/* eslint-disable no-duplicate-case */
/* eslint-disable no-useless-constructor */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import { Text, View, TouchableOpacity, Image, Alert, Linking, Platform, } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import DeviceInfo from 'react-native-device-info';
import styles from './style';
import * as globals from '../../../utils/globals';
import * as images from '../../../assets/images/map';
import * as colors from '../../../assets/styles/color';
import PortfolioTab from './userProfileTabs/portfolioTab';
import CommunitiesTab from './userProfileTabs/communitiesTab';
import ProfileCategoryTab from './userProfileTabs/profileCategoryTab';
import ProfessionalSummary from './userProfileTabs/professionalSummary';
import SkillsAndInterests from './userProfileTabs/skillsAndInterests';
import SimilarInterests from './userProfileTabs/similarInterests';
import { ScrollableTabView, ScrollableTabBar } from '../../../libs/react-native-scrollable-tabview'
import { API } from '../../../utils/api';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RNFS from 'react-native-fs';
import ImageLoad from 'react-native-image-placeholder';
import ImagePicker from 'react-native-image-crop-picker';
import { showLoader, hideLoader } from '../../../redux/acrions/showLoader';
import AsyncStorage from '@react-native-community/async-storage';
import SideMenu from '../../screens/SideMenu/index';
import loginScreen from '../AuthenticationScreens/loginScreen';

const iPad = DeviceInfo.getModel();
let TAG = 'MyProfile :==='
let apiImage;

let _this;
class MyProfile extends React.Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      myProfileData: [],
      memberProfile: [],
      loading: false,
      sourceImage: apiImage,
      profileAllData: [],
      isLoading: false,
      pickerOpen: false,
    }
  }


  clearstates(){
     this.setState({sourceImage:'', apiImage:''})
  }

/**
   * static method for clear source images
   */
  static clearsourceImages =()=>{
    _this && _this.clearstates()
  }

  /**
   * static method for refresh My Profile
   */
  static refreshMyProfile() {
    if (globals.isInternetConnected === true) {
      API.getMyProfileDetails(_this.getMyProfileResponseData, true, globals.userID);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  componentDidMount() {
    this.GetMyProfile()
  }


  /**
 * Method called when session expire
 */
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigationProps.navigation.navigate('LoginScreen');
  }

  /**
   * Api call of get my Profile
   */
  GetMyProfile() {
    this.props.showLoader()
    this.setState({ loading: true })
    AsyncStorage.multiGet([globals.LOGINUSERID, globals.ORGANIZATIONID]).then(token => {
      globals.userID = token[0][1];
      globals.organizationId = token[1][1];
      if (globals.isInternetConnected === true) {
        API.getMyProfileDetails(this.getMyProfileResponseData, true, globals.userID);
      } else {
        this.props.hideLoader()
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  /**
   * Response callback of getMyProfile
   */
  getMyProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'getMyProfileResponseData -> success : ',
        response
      );
      if (response.StatusCode == 200) {
        const responseData = response.Data.MemberInfo;
        const responseMemberProfile = response.Data.MemberProfile
        AsyncStorage.setItem(globals.ORGANIZATIONID, JSON.stringify(response.Data.MemberInfo.OrganizationID));
        this.setState({ myProfileData: responseData })
        this.setState({ memberProfile: responseMemberProfile , sourceImage: responseMemberProfile.ProfilePicturePath})
        this.setState({ profileAllData: response.Data })
        ProfessionalSummary.getMemberProfileData(
          this.state.memberProfile,
          this.state.myProfileData,
          this.state.myProfileData.EmailID,
          this.state.myProfileData.OrganizationName,
          this.state.profileAllData,
        );
        SideMenu.profilepicture(this.state.sourceImage)
      } else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false })
      }
      this.props.hideLoader()
    },
    error: err => {
      this.setState({ loading: false })
      if (err.StatusCode == 401 || err.StatusCode == 403) {
        this.props.hideLoader();
        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          globals.appName,
          err.Message
        );
      }
      console.log(
        TAG,
        'getMyProfileResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );

      this.setState({ loading: false });
      // this.props.hideLoader();
    },
    complete: () => {
      this.props.hideLoader();
      this.setState({ loading: false })
    },
  };

  /**
       * pickSingleImg from gallary
       */
  pickSingleImg() {
    ImagePicker.openPicker({
      width: globals.screenWidth * 0.25,
      height: globals.screenWidth * 0.25,
      borderRadius: (globals.screenWidth * 0.25) / 2,
      mediaType: 'photo',
      includeBase64: true,
    }).then(image => {
      this.setState({ isLoading: !this.state.isLoading }, () => {
        this.forceUpdate()
        this.convert(image.path);
      })
    }).catch((e) => {
      if (e.message !== "User cancelled image selection") {
        if (Platform.OS === "ios") {
          Alert.alert(
            globals.appName,
            e.message,
            [{ text: 'OK', onPress: () => Linking.openSettings() },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],

          );
        } else {
          console.log(e.message ? "ERROR" + e.message : "ERROR" + e);
        }
      } else {
        console.log(TAG, e.message);
      }
    });
  }

  /**
* convert image to base64 formate
*/
  convert(image) {
    RNFS.readFile(image, 'base64')
      .then(res => {
        this.setState({ isLoading: true, loading: true })
        this.changeProfilePicture(res)
      });
  }

  /**
    * method called when change Profile Picture
    */
  changeProfilePicture(res) {
    const data = {
      userid: globals.userID,
      ProfileImageFile: 'data:image/png;base64,' + res,
      orgid: globals.organizationId,
      companylogo: "",
    }

    if (globals.isInternetConnected == true) {
      this.setState({ isLoading: true, loading: true })
      API.uploadProfilePictureFile(this.uploadProfilePictureFileResponseData, data, true, globals.userID);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
  * Response callback of uploadProfilePictureFileResponseData
  */
  uploadProfilePictureFileResponseData = {
    success: response => {
      console.log(
        TAG,
        'uploadProfilePictureFileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ sourceImage: response.Data, isLoading: false, loading: false }, () => {
          this.forceUpdate()
          // AsyncStorage.setItem(globals.MYPROFILEPICTURE, this.state.sourceImage);

          var picture = response.Data
          SideMenu.profilepicture(picture)
          this.GetMyProfile()
        })

      }
      else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ isLoading: false, loading: false })
      }
    },
    error: err => {
      console.log(
        TAG,
        'uploadProfilePictureFileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ isLoading: false, loading: false })
      Alert.alert(globals.appName, err.Message)
    },
    complete: () => {
      this.setState({ isLoading: false, loading: false })
    },
  };

  /**
 * Method for open Facebook Url
 */
  _opneFacebook = (FacebookUrl) => {
    let URL = FacebookUrl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        return Linking.openURL(URL);
      } else {
        return Linking.openURL(URL)
      }
    });
  };

  /*
  * this method for Linkedin open in default browser
  */
  _opneLinkedin = (LinkedInUrl) => {
    let URL = LinkedInUrl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        return Linking.openURL(URL);
      } else {
        return Linking.openURL(URL)
      }
    });
  };


  /*
   * this method for Twitter open in default browser
   */
  _opneTwitter = (TwitterUrl) => {
    let URL = TwitterUrl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        return Linking.openURL(URL);
      } else {
        return Linking.openURL(URL)
      }
    });
  };

  /*
   * this method for Twitter open in default browser
   */
  _opneYoutube = (YouTubeUrl) => {
    let URL = YouTubeUrl
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        return Linking.openURL(URL);
      } else {
        return Linking.openURL(URL)
      }
    });
  };



  static refreshMyProfile() {
    if (globals.isInternetConnected === true) {
      API.getMyProfileDetails(_this.getMyProfileResponseData, true, globals.userID);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  static updateProfilePic(img){
    SideMenu.profilepicture(img)
    _this && _this.setState({sourceImage: img})
  }

  render() {
    const { myProfileData, sourceImage, memberProfile, isLoading } = this.state;
    const isProfileImage = globals.checkImageObject(memberProfile, 'ProfilePicturePath');
    const isFullName = globals.checkObject(myProfileData, 'FullName');
    const isEmailId = globals.checkObject(myProfileData, 'EmailID');
    const EmailID = (isEmailId) ? myProfileData.EmailID : '-';
    const isyouTubeUrl = globals.checkObject(memberProfile, 'YouTubeUrl')
    const islinkedInUrl = globals.checkObject(memberProfile, 'LinkedInUrl')
    const isfacebookUrl = globals.checkObject(memberProfile, 'FacebookUrl')
    const istwitterUrl = globals.checkObject(memberProfile, 'TwitterUrl')
    const YouTubeUrl = (isyouTubeUrl) ? memberProfile.YouTubeUrl : '';
    const LinkedInUrl = (islinkedInUrl) ? memberProfile.LinkedInUrl : '';
    const FacebookUrl = (isfacebookUrl) ? memberProfile.FacebookUrl : '';
    const TwitterUrl = (istwitterUrl) ? memberProfile.TwitterUrl : '';
    apiImage = memberProfile.ProfilePicturePath;
    
   
    

    return (
      <View style={styles.mainContainer}>
        <View style={styles.profileViewContainer}>
          <View style={styles.imgAndIconViewContainer} >
            {
              (isLoading) ?
                <View style={styles.before_imageView}>
                  {/* <TouchableOpacity onPress={() => this.props.navigationProps.navigation.navigate("viewMyProfilePicture")}> */}
                    <ImageLoad
                      style={[styles.profile_ImageStyle,]}
                      isShowActivity={true}
                      placeholderSource={images.MyProfile.placeHolder}
                      borderRadius={((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140/2 :  (globals.screenWidth * 0.26) / 2}
                    />
                  {/* </TouchableOpacity> */}
                </View>
                :
                (isProfileImage) ?
                  <TouchableOpacity onPress={() => this.props.navigationProps.navigation.navigate("viewMyProfilePicture",{sourceImage:sourceImage, ProfileImage:memberProfile.ProfilePicturePath })}>
                    <ImageLoad
                      source={{ uri:(sourceImage == null || sourceImage == undefined) ? memberProfile.ProfilePicturePath : sourceImage }}
                      style={styles.profile_ImageStyle}
                      placeholderSource={images.MyProfile.placeHolder}
                      placeholderStyle={styles.profile_ImageStyle}
                      isShowActivity={true}
                      borderRadius={ ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140/2 :  (globals.screenWidth * 0.26) / 2}
                    />
                  </TouchableOpacity>
                  :
                  <TouchableOpacity onPress={() => this.props.navigationProps.navigation.navigate("viewMyProfilePicture",{placeHolder:images.MyProfile.placeHolder})}>
                    <ImageLoad
                      source={images.MyProfile.placeHolder}
                      style={styles.profile_ImageStyle}
                      placeholderSource={images.MyProfile.placeHolder}
                      placeholderStyle={styles.profile_ImageStyle}
                      isShowActivity={false}
                      borderRadius={ ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? 140/2 :  (globals.screenWidth * 0.26) / 2}
                    />
                  </TouchableOpacity>
            }

            <TouchableOpacity style={styles.settingOpacityContainer} onPress={() => this.pickSingleImg()}>
              <AntDesign
                name="setting"
                size={
                  iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
                    ? 30
                    : globals.screenWidth * 0.055
                }
                color={colors.white}
                style={styles.settingIconStyle}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.userNameAndEmailTextViewContainer}>
            <Text style={styles.userNameTextStyle} numberOfLines={1}>
              {(isFullName) ? myProfileData.FullName : '-'}
            </Text>
            <Text numberOfLines={1} style={styles.userEmailTextStyle}>{EmailID}</Text>
          </View>
        </View>
        <View style={styles.IconsViewStyle}>
          {(YouTubeUrl == "") ?
            null
            :
            <View style={[styles.playIconViewStyle,{marginRight: (LinkedInUrl == "") ? null : globals.screenWidth * 0.026,}]}>
              <TouchableOpacity onPress={() => this._opneYoutube(YouTubeUrl)}>
                <Image
                  source={images.MyProfile.play}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(LinkedInUrl == "") ? null
            :
            <View style={styles.linkedinAndTwitterIconViewStyle}>
              <TouchableOpacity onPress={() => this._opneLinkedin(LinkedInUrl)}>
                <Image
                  source={images.MyProfile.linkedIn}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(FacebookUrl == "") ? null
            :
            <View style={styles.fbIconStyle}>
              <TouchableOpacity onPress={() => this._opneFacebook(FacebookUrl)}>
                <Image
                  source={images.MyProfile.fbBlue}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
          {(TwitterUrl == "") ? null
            :
            <View style={styles.linkedinAndTwitterIconViewStyle}>
              <TouchableOpacity onPress={() => this._opneTwitter(TwitterUrl)}>
                <Image
                  source={images.MyProfile.twitter}
                  resizeMode="contain"
                  style={styles.iconsStyles}
                />
              </TouchableOpacity>
            </View>
          }
        </View>
        <ScrollableTabView
          initialPage={0}
          tabBarTextStyle={{fontSize:globals.font_16}}
          tabBarInactiveTextColor={colors.black} 
          tabBarActiveTextColor={colors.warmBlue}
          renderTabBar={() => <ScrollableTabBar isFrom={'Profile'} />}
        >
          <ProfessionalSummary tabLabel={'Professional Summary'} navigationProps={this.props} sourceImage={sourceImage} />
          <PortfolioTab tabLabel={'Portfolio'} navigationProps={this.props} />
          <ProfileCategoryTab tabLabel={'Profile Category'} navigationProps={this.props} />
          <CommunitiesTab tabLabel={'Communities'} navigationProps={this.props} />
          <SkillsAndInterests tabLabel={'Skills & Interests'} navigationProps={this.props} />
          <SimilarInterests tabLabel={'People With Similar Interests'+'  '} navigationProps={this.props} />
        </ScrollableTabView>
      </View>
    );
  }
}

// ********************** Model mapping method **********************

const mapStateToProps = state => {
  return {
    loader: state.loaderRed.loader,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      showLoader,
      hideLoader,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyProfile);