/* eslint-disable no-console */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/sort-comp */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */
import React, { Component } from 'react';
import { NavigationActions, StackActions } from 'react-navigation';
import { SafeAreaView, Text, View, TouchableOpacity, Image, Alert, Linking } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './style';
import ImageLoad from 'react-native-image-placeholder';
import * as colors from '../../../assets/styles/color';
import * as globals from '../../../utils/globals';
import loginScreen from "../AuthenticationScreens/loginScreen";
import DeviceInfo from 'react-native-device-info';
import * as images from '../../../assets/images/map';
import myProfile from '../Profile/myProfile';
import ImagePicker from 'react-native-image-crop-picker';
import RNFS from 'react-native-fs';
import MyProfile from '../Profile/myProfile';
import CompanyProfile from '../Profile/companyProfile';
import GlobalEvents from '../GlobalEvents/globalEvents';
import { API } from '../../../utils/api';

var iPad = DeviceInfo.getModel()
let _this = null;
const TAG = 'SideMenu ::=';
let apiImage;

class SideMenu extends Component {
  constructor(props) {
    super(props);
    _this = this;
    this.state = {
      memberProfile: [],
      tabId: 1,
      userName: '',
      userEmail: '',
      userProfilePhoto: '',
      isFblogin: true,
      isLoading: false,
      loading: false,
    };
  }

  static onbackPressChange() {
    _this.setState({ tabId: 1 })
  }
  /**
   * method for set profile picture
   */
  static profilepicture(picture) {
    console.log("static profilepicture")
    AsyncStorage.setItem(globals.MYPROFILEPICTURE, picture);
    _this.setState({ userProfilePhoto: picture },()=>{
      AsyncStorage.getItem(globals.MYPROFILEPICTURE).then(token => {
        globals.my_profile_picture = token;
        _this.setState({ userProfilePhoto: token })
      });
    })
  
  }

  clearValues() {


// export var tokenValue = '';
// export var userID = '';
// export var loginFullName = '';
// export var loginEmail = '';
// export var loginProfileImg = '';
// export var payerInfoData = {};
// export var att_Email = '';
// export var att_Fname = '';
// export var att_Lname = '';
// export var att_MobileNo = '';
// export var att_Comp_name = '';
// export var att_Change_icon = 0;
// export var att_userId = [];
// export var linkedin_company_profile = '';
// export var linkedin_my_profile = '';
// export var my_profile_picture = '';
// export var organizationId = '';
// export var allItems = '';
// export var fbLoginKey = true;
// export var other_org_id = "";
// export var allItems_msg = '';
// export var allItems_endorsement = '';
// export var allItems_connection = true;
// export var PasswordChangeRequired = false;


    globals.my_profile_picture = ''
    globals.tokenValue=''
    globals.userID=''
    globals.loginFullName=''
    this.setState({ userProfilePhoto: '' })
  }

  componentDidMount() {
    console.log("componentDidMount sidemenu")
    if (globals.PasswordChangeRequired === false) {
      this.GetMyProfile()
    }
    AsyncStorage.multiGet([globals.LOGINUSERNAME, globals.LOGINUSEREMAIL, globals.LOGINUSERPROFILEIMAGE, globals.FB_LOGINKEY]).then(profileInfo => {
      this.setState({
        userEmail: profileInfo[1][1],
        userName: profileInfo[0][1],
        isFblogin: JSON.parse(profileInfo[3][1]),
      }, () => {
        console.log("componentDidMount sidemen emailu-->",profileInfo[1][1])
        globals.loginFullName = profileInfo[0][1];
        globals.loginEmail = profileInfo[1][1];
        globals.loginProfileImg = profileInfo[2][1];
        globals.fbLoginKey = JSON.parse(profileInfo[3][1])
      });
    });
    AsyncStorage.getItem(globals.FB_LOGINKEY).then(fb_id => {
      globals.fbLoginKey = fb_id;
    })
    this.setState({ isFblogin: globals.fbLoginKey })
    AsyncStorage.getItem(globals.MYPROFILEPICTURE).then(token => {
      globals.my_profile_picture = token;
      _this.setState({ userProfilePhoto: token })
    });
  }


  /**
   * Api call of get my Profile
   */
  GetMyProfile() {
    this.setState({ loading: true })
    AsyncStorage.multiGet([globals.LOGINUSERID, globals.ORGANIZATIONID]).then(token => {
      globals.userID = token[0][1];
      globals.organizationId = token[1][1];
      if (globals.isInternetConnected === true) {
        API.getMyProfileDetails(this.getMyProfileResponseData, true, globals.userID);
      } else {
        Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
      }
    });
  }

  /**
* Method called when session expire
*/
  _sessionOnPres() {
    AsyncStorage.multiRemove([globals.LOGINRESPONSEKEY]);
    loginScreen.clearTextFields();
    this.props.navigation.navigate('LoginScreen');
  }


  /**
   * Response callback of getMyProfile
   */
  getMyProfileResponseData = {
    success: response => {
      console.log(
        TAG,
        'getMyProfileResponseData -> success : ',
        response
      );
      if (response.StatusCode == 200) {
        const responseData = response.Data.MemberInfo;
        const responseMemberProfile = response.Data.MemberProfile
        AsyncStorage.setItem(globals.ORGANIZATIONID, JSON.stringify(response.Data.MemberInfo.OrganizationID));
        this.setState({ memberProfile: responseMemberProfile, userProfilePhoto: responseMemberProfile.ProfilePicturePath })
        // MyProfile.updateProfilePic(this.state.userProfilePhoto)
      } else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ loading: false })
      }
    },
    error: err => {
      this.setState({ loading: false })
      if (err.StatusCode == 401 || err.StatusCode == 403) {

        Alert.alert(
          globals.appName,
          'Your session is expired, Please login again',
          [{ text: 'OK', onPress: () => this._sessionOnPres() }],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          globals.appName,
          err.Message
        );
      }
      console.log(
        TAG,
        'getMyProfileResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );

      this.setState({ loading: false });

    },
    complete: () => {
      this.setState({ loading: false })
    },
  };


  

  /**
 * method for navigateTo Other Screen 
 */
  navigateToScreen = (route, currentTabId) => () => {
    if (currentTabId === 3) {
      GlobalEvents.updateselecedindex()
    }
    const { navigation } = this.props;
    const navigateAction = NavigationActions.navigate({
      index:0,
      routeName: route,
    });
    this.setState({ tabId: currentTabId });
    navigation.dispatch(navigateAction);
  };


  logoutFinal() {
    this.clearValues()
    myProfile.clearsourceImages();
    if (this != null && this != undefined) {
      if (this.props != null && this.props != undefined) {
        if (this.props.navigation != null && this.props.navigation != undefined) {
          if (this.props.navigation.reset != undefined) {
            this.props.navigation.reset([NavigationActions.navigate({ routeName: 'LoginScreen' })], 0)
          } else {
            this.props.navigation.navigate('LoginScreen')
          }
        }
      }
    }
    AsyncStorage.clear()
    loginScreen.clearTextFields();
  }

  /**
   * logOut method
   */
  onPressLogout() {
    Alert.alert(globals.appName, "Are you sure you want to logout?", [{ text: 'OK', onPress: () => this.logoutFinal() },
    { text: 'Cancel', onPress: () => console.log('Cancel Pressed') }])
  }

  /**
* convert image to base64 formate
*/
  convert(image) {
    RNFS.readFile(image, 'base64')
      .then(res => {
        this.setState({ isLoading: true, loading: true })
        this.changeProfilePicture(res)
      });
  }

  /**
      * method called when change Profile Picture
      */
  changeProfilePicture(res) {
    const data = {
      userid: globals.userID,
      ProfileImageFile: 'data:image/png;base64,' + res,
      orgid: globals.organizationId,
      companylogo: "",
    }

    if (globals.isInternetConnected == true) {
      this.setState({ isLoading: true, loading: true })
      API.uploadProfilePictureFile(this.uploadProfilePictureFileResponseData, data, true, globals.userID);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
  * Response callback of uploadProfilePictureFileResponseData
  */
  uploadProfilePictureFileResponseData = {
    success: response => {
      console.log(
        TAG,
        'uploadProfilePictureFileResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200) {
        this.setState({ userProfilePhoto: response.Data, isLoading: false, loading: false }, () => {
          this.forceUpdate()
          var picture = response.Data
          MyProfile.updateProfilePic(picture)
          this.setState({ userProfilePhoto: picture })
        })

      }
      else {
        Alert.alert(globals.appName, response.Message)
        this.setState({ isLoading: false, loading: false })
      }
    },
    error: err => {
      console.log(
        TAG,
        'uploadProfilePictureFileResponseData -> ERROR : ',
        JSON.stringify(err.Message)
      );
      this.setState({ isLoading: false, loading: false })
      Alert.alert(globals.appName, err.Message)
    },
    complete: () => {
      this.setState({ isLoading: false, loading: false })
    },
  };



  /**
   * pickSingleImg from gallary
   */
  pickSingleImg() {
    ImagePicker.openPicker({
      width: globals.screenWidth * 0.25,
      height: globals.screenWidth * 0.25,
      borderRadius: (globals.screenWidth * 0.25) / 2,
      mediaType: 'photo',
      includeBase64: true,
    }).then(image => {
      this.setState({ isLoading: !this.state.isLoading }, () => {
        this.forceUpdate()
        this.convert(image.path);
      })
    }).catch((e) => {
      if (e.message !== "User cancelled image selection") {
        if (Platform.OS === "ios") {
          Alert.alert(
            globals.appName,
            e.message,
            [{ text: 'OK', onPress: () => Linking.openSettings() },
            { text: 'Cancel', onPress: () => console.log('Cancel Pressed') },],

          );
        } else {
          console.log(e.message ? "ERROR" + e.message : "ERROR" + e);
        }
      } else {
        console.log(TAG, e.message);
      }
    });
  }

  render() {
    const { tabId, isFblogin, isLoading, loading, memberProfile } = this.state;
    // console.log("this.state.userProfilePhoto render--",this.state.userProfilePhoto)

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <View style={styles.profileView}>
            <TouchableOpacity onPress={() => this.props.navigation.closeDrawer()}>
              <Image source={images.drawerMenu.menuClose} style={styles.closeMenuBtnStyle} />
            </TouchableOpacity>
            <Text style={styles.menuTextStyle}>{globals.MESSAGE.SIDEMENU_OPTIONS.MENU_HAEDER}</Text>
          </View>
          <View style={styles.drawerSaperator} />
          <View style={styles.imgAndIconViewContainer}>
            <View style={styles.profileViewContainer}>
              <View style={styles.beforeimgview}>
                {
                  (isLoading) ?
                    <View style={styles.before_imageView}>
                      <ImageLoad
                        style={[styles.profile_ImageStyle,]}
                        isShowActivity={true}
                        placeholderSource={images.MyProfile.placeHolder}
                        borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : (globals.screenWidth * 0.26) / 2}
                      />
                    </View>
                    :
                    (this.state.userProfilePhoto || globals.my_profile_picture) ?
                      <ImageLoad
                        style={styles.profile_ImageStyle}
                        source={{ uri: this.state.userProfilePhoto }}
                        isShowActivity={true}
                        placeholderSource={images.MyProfile.placeHolder}
                        placeholderStyle={styles.profile_ImageStyle}
                        borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : (globals.screenWidth * 0.26) / 2}
                      />
                      :
                      <ImageLoad
                        style={styles.profile_ImageStyle}
                        source={images.MyProfile.placeHolder}
                        isShowActivity={false}
                        placeholderSource={images.MyProfile.placeHolder}
                        placeholderStyle={styles.profile_ImageStyle}
                        borderRadius={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : (globals.screenWidth * 0.26) / 2}
                      />
                }

              </View>

              <TouchableOpacity style={styles.settingOpacityContainer} onPress={() => this.pickSingleImg()}>
                <AntDesign
                  name="setting"
                  size={((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 30 : globals.screenWidth * 0.055}
                  color={colors.white}
                  style={styles.settingIconStyle}
                />
              </TouchableOpacity>
            </View>

            <TouchableOpacity style={[styles.profileViewContainer, { marginLeft: 0 }]} onPress={() => this.props.navigation.navigate("MY_PROFILEDEMO", { isfrom: "myProfile" })}>

              <View style={styles.userNameAndEmailTextViewContainer}>
                <Text style={styles.userNameTextStyle} numberOfLines={1}>{this.state.userName}</Text>
                <Text style={styles.userEmailTextStyle} numberOfLines={1}>{this.state.userEmail}</Text>

              </View>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity
              style={[
                styles.dashBoardButtonViewStyle,
                { backgroundColor: tabId === 1 ? colors.bgColor : colors.white },
              ]}
              onPress={this.navigateToScreen(globals.DASHBOARD_SCREEN, 1)}
            >
              <Text
                style={[
                  styles.dashBoardTextStyle,
                  { color: tabId === 1 ? colors.white : colors.black },
                ]}
              >
                {' '}
                {globals.MESSAGE.SIDEMENU_OPTIONS.DASHBOARD}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={[
                styles.dashBoardButtonViewStyle,
                { backgroundColor: tabId === 2 ? colors.bgColor : colors.white },
              ]}
              onPress={this.navigateToScreen(globals.GLOBAL_MEMBER_SCREEN, 2)}
            >
              <Text
                style={[
                  styles.dashBoardTextStyle,
                  { color: tabId === 2 ? colors.white : colors.black },
                ]}
              >
                {globals.MESSAGE.SIDEMENU_OPTIONS.GLOBAL_MEMBERS}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.dashBoardButtonViewStyle,
                { backgroundColor: tabId === 3 ? colors.bgColor : colors.white },
              ]}
              onPress={this.navigateToScreen(globals.GLOBAL_EVENT_SCREEN, 3)}
            >
              <Text
                style={[
                  styles.dashBoardTextStyle,
                  { color: tabId === 3 ? colors.white : colors.black },
                ]}
              >

                {globals.MESSAGE.SIDEMENU_OPTIONS.GLOBAL_EVENTS}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.dashBoardButtonViewStyle,
                { backgroundColor: tabId === 4 ? colors.bgColor : colors.white },
              ]}
              onPress={this.navigateToScreen(globals.GLOBAL_COMMUNITIES_SCREEN, 4)}
            >
              <Text
                numberOfLines={1}
                style={[
                  styles.dashBoardTextStyle,
                  { color: tabId === 4 ? colors.white : colors.black },
                ]}
              >
                {globals.MESSAGE.SIDEMENU_OPTIONS.GLOBAL_COMMUNITIES}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.dashBoardButtonViewStyle,
                { backgroundColor: tabId === 5 ? colors.bgColor : colors.white },
              ]}
              onPress={this.navigateToScreen(globals.NOTIFICATION_SCREEN, 5)}
            >
              <Text
                numberOfLines={1}
                style={[
                  styles.dashBoardTextStyle,
                  { color: tabId === 5 ? colors.white : colors.black },
                ]}
              >
                {globals.MESSAGE.SIDEMENU_OPTIONS.NOTIFICATIONS}
              </Text>
            </TouchableOpacity>
            {(isFblogin === true) || (isFblogin == null) ?
              <TouchableOpacity
                style={[
                  styles.dashBoardButtonViewStyle,
                  { backgroundColor: tabId === 6 ? colors.bgColor : colors.white },
                ]}
                onPress={this.navigateToScreen(globals.CHANGE_PASSWORD_SCREEN, 6)}
              >
                <Text
                  style={[
                    styles.dashBoardTextStyle,
                    { color: tabId === 6 ? colors.white : colors.black },
                  ]}
                >
                  {globals.MESSAGE.SIDEMENU_OPTIONS.CHANGE_PASSWORD}
                </Text>
              </TouchableOpacity>
              :
              null
            }
            <TouchableOpacity
              onPress={() => this.onPressLogout()}
              style={[styles.dashBoardButtonViewStyle, { backgroundColor: colors.white }]}
            >
              <Text style={[styles.dashBoardTextStyle, { color: colors.black }]}>
                {globals.MESSAGE.SIDEMENU_OPTIONS.LOG_OUT}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

export default SideMenu;
