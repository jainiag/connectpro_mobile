import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

var iPad = DeviceInfo.getModel()

module.exports = StyleSheet.create({
  container: {
    flex: 1,
  },
  profileView: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 10,
    marginLeft: 10,
    alignItems: 'center',
  },
  beforeimgview: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : globals.screenWidth * 0.26,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : globals.screenWidth * 0.26,
    borderRadius: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : (globals.screenWidth * 0.26) / 2,
    borderColor: colors.proUnderline,
    borderWidth: 0.3
  },
  userInfoView: {
    justifyContent: 'center',
    marginLeft: 10,
  },
  drawerSaperator: {
    width: '100%',
    height: 1,
    backgroundColor: colors.gray,
  },
  drawerImageView: {
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  drawerImageLogoView: {
    width: 30,
    height: 30,
  },
  profilePicImg: {
    width: globals.screenWidth * 0.16,
    height: globals.screenWidth * 0.16,
    borderRadius: (globals.screenWidth * 0.16) / 2,
  },
  userNameText: {
    fontSize: globals.font_15,
    marginBottom: 3,
  },
  contactNumText: {
    opacity: Platform.OS === 'android' ? 0.7 : 0.7,
  },
  sliderMenuUpperSafeareaView: {
    flex: 0,
  },
  headerContainer: {
    backgroundColor: colors.HeaderColor,
    alignItems: 'center',
    justifyContent: 'center',
    height: globals.screenHeight * 0.08,
    marginBottom: 30,
  },
  headerTextStyle: {
    fontWeight: '500',
    fontSize: globals.font_20,
    color: colors.white,
  },
  navItemStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 10,
    marginVertical: 5,
    height: globals.screenHeight * 0.05,
    alignItems: 'center',
  },
  navItemContainStyle: {
    fontSize: globals.font_15,
    flex: 1,
    marginLeft: 20,
  },
  iconContainer: {
    alignItems: 'flex-end',
  },
  lineStyle: {
    borderBottomWidth: 1,
    borderColor: colors.separate,
  },
  footerSupportView: {
    width: '100%',
    borderTopWidth: 1,
    borderTopColor: colors.gray,
    flexDirection: 'row',
    bottom: 0,
    position: 'absolute',
  },
  iconCallEmail: {
    flex: 1,
    width: 50,
    height: 50,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerCallEmailIcon: {
    width: 50,
    height: 50,
  },
  supportText: {
    marginTop: 8,
    fontSize: globals.font_11,
    color: colors.purple,
  },
  supportBoxView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderRightColor: colors.gray,
    padding: 10,
  },
  headerStyle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.black,
    elevation: 0,
    shadowColor: 'transparent',
    textAlign: 'center',
  },
  headerGroupStyle: {
    flex: 2,
    textAlign: 'center',
    // fontFamily: globals.SFProTextRegular,
    fontSize: globals.font_17,
    color: colors.darkgrey,
  },
  menuTextStyle: {
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 27 : globals.font_20,
    color: colors.black,
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 15,
    marginBottom: (DeviceInfo.getModel() === ' iPhone 6s Plus') ? 10 : 0,
    fontWeight: '500',
    marginTop: ((iPad.indexOf('iPad') != -1)) ? 4 : 0,
  },
  profileViewContainer: {
    flexDirection: 'row',
    marginLeft: globals.screenWidth * 0.06,
    marginTop: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.07 : globals.screenWidth * 0.1,
    marginBottom: globals.screenHeight * 0.04,
  },
  imgAndIconViewContainer: {
    flexDirection: 'row',
  },
  profile_ImageStyle: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : globals.screenWidth * 0.26,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : globals.screenWidth * 0.26,
    borderRadius: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : (globals.screenWidth * 0.26) / 2,
    borderWidth: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 0.1 : (Platform.OS == 'android') ? 0.3 : 0.2,
    borderColor: (Platform.OS == 'android') ? colors.lightBlack: colors.proUnderline
  },
  settingOpacityContainer: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 40 : globals.screenWidth * 0.085,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 40 : globals.screenWidth * 0.085,
    borderRadius: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 40 : (globals.screenWidth * 0.085) / 2,
    backgroundColor: colors.darkSkyBlue,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    marginLeft: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? Platform.OS === 'ios' ? globals.screenWidth * 0.11 : globals.screenWidth * 0.095 : globals.screenWidth * 0.17,
    bottom: 0,
  },
  settingIconStyle: {
    alignSelf: 'center',
  },
  userNameAndEmailTextViewContainer: {
    flexDirection: 'column',
    marginLeft: 20,
    justifyContent: 'center',
  },
  userNameTextStyle: {
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 30 : globals.font_20,
    color: colors.black,
    marginBottom: 5,
    width: globals.screenWidth * 0.39,
  },
  userEmailTextStyle: {
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 24 : globals.font_16,
    color: colors.lightgray,
    width: globals.screenWidth * 0.37,
  },
  dashBoardButtonViewStyle: {
    height: globals.screenHeight * 0.055,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? globals.screenWidth * 0.37 : globals.screenWidth * 0.6,
    justifyContent: 'center',
    borderRadius: (globals.screenHeight * 0.055 + globals.screenWidth * 0.61) / 2,
    marginLeft: globals.screenWidth * 0.06,
    marginBottom: globals.screenHeight * 0.02,
  },
  dashBoardTextStyle: {
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 27 : globals.font_18,
    marginLeft: 20,
    opacity: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 0.8 : 0.7 : 0.7,
  },
  closeMenuBtnStyle: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 27 : 20,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 27 : 20,
    marginLeft: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 7 : 0,
    marginTop: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 4 : (DeviceInfo.getModel() === ' iPhone 6s Plus') ? 8 : 0,
  },
  before_imageView: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : globals.screenWidth * 0.26,
    width: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : globals.screenWidth * 0.26,
    borderRadius: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 130 : (globals.screenWidth * 0.26) / 2,
    borderWidth: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 0.1 : (Platform.OS == 'android') ? 0.3 : 0.2,
    borderColor: (Platform.OS == 'android') ? colors.lightBlack: colors.proUnderline,
  },
});
