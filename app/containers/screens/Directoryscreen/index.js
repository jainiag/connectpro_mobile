/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import styles from './style'
import * as globals from '../../../utils/globals';
import globalStyles from '../../../assets/styles/globleStyles'
import AlphaScrollFlatList from '../../../libs/alpha-scroll-flat-list';
import people from './data';
import Icon from 'react-native-vector-icons/EvilIcons';

let TAG = "==:== Directory : "
let _this = undefined;
const ITEM_HEIGHT = globals.screenHeight * 0.1136; //60;

export default class Directory extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerLeft: globals.ConnectProbackButton(navigation, 'Directory'),
        headerStyle: globalStyles.ConnectPropheaderStyle,
    });

    constructor(props) {
        super(props);
        _this = this;
        this.state = {
            selectedChar: '#',
            peopleData: people
        }
    }

    componentDidMount() {
        let { peopleData } = this.state;
        let updatedPeopleData = [];
        peopleData.map((item) => {
            item.isOpen = false;
            updatedPeopleData.push(item);
        })

        this.setState({ peopleData: updatedPeopleData })
    }

    onClickItem(item, index) {
        console.log(TAG, "onClickItem : " + JSON.stringify(item) + " on -> " + index);
        let indexedObj = Object.assign({}, this.state.peopleData[index]);
        indexedObj.isOpen = !indexedObj.isOpen;
        let peopleDataUpdated = Object.assign([], this.state.peopleData);
        peopleDataUpdated[index] = indexedObj;

        this.setState({ peopleData: peopleDataUpdated })
    }

    renderItem({ item, index }) {
        return (
            <View style={[styles.itemContainer, styles.vwOnlyjustifyContentStyle, (!item.isOpen ? styles.closedStyle : styles.openStyle)]}>
                <View style={[styles.onlyRowStyle, styles.vwOnlyAlignedItemStyle]}>
                    <Image style={styles.ivProfileImageStyle} source={{ uri: 'https://dummyimage.com/300' }} />
                    <View style={styles.vwInfoStyle}>
                        <Text numberOfLines={1} style={styles.itemTitle}>{item.name}</Text>
                        <Text numberOfLines={1} style={styles.itemSubtitle}>{item.company}</Text>
                    </View>
                    <TouchableOpacity onPress={() => this.onClickItem(item, index)}>
                        <Image resizeMode={'stretch'} style={styles.dotStyle} source={{ uri: 'https://img.icons8.com/material-outlined/24/000000/menu-2.png' }} />
                    </TouchableOpacity>
                </View>
                {(item.isOpen) ? <View style={styles.vwChildStyle}>
                    <View style={{ flexDirection: 'row', }}>
                        <TouchableOpacity style={styles.toChildButtonStyle}>
                            <Icon name="envelope" size={30} color="blue" />
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.toChildButtonStyle}>
                            <Icon name="envelope" size={30} color="blue" />
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.toChildButtonStyle}>
                            <Icon name="comment" size={30} color="blue" />
                        </TouchableOpacity>
                    </View>
                </View> : null}
            </View>
        );
    }

    keyExtractor(item) {
        return item.id;
    }

    onCharacterSelect(char) {
        this.setState({ selectedChar: char });
    }


    /**
*  Method for pagination to load More data
*/
    handleLoadMore = () => {
        // if (!this.onEndReachedCalledDuringMomentum) {
        //     this.setState({
        //         pageNumber: this.state.pageNumber + 1,
        //     }, () => {
        //         _this.getCommunityList();
        //     })
        //     this.onEndReachedCalledDuringMomentum = true;
        // }
    };


    render() {
        let { selectedChar, peopleData } = this.state;


        return (
            <View style={styles.mainParentStyle}>

                <View style={styles.vwSelectedCharStyle}>
                    <Text style={styles.tvSelectedCharStyle}>{selectedChar}</Text>
                </View>

                <AlphaScrollFlatList
                    extraData={this.state}
                    keyExtractor={this.keyExtractor.bind(this)}
                    data={peopleData.sort((prev, next) => prev.name.localeCompare(next.name))}
                    renderItem={this.renderItem.bind(this)}
                    scrollKey={'name'}
                    reverse={false}
                    itemHeight={ITEM_HEIGHT}
                    scrollBarPointerContainerStyle={styles.scrollBarPointerContainerStyle}
                    scrollBarContainerStyle={styles.scrollBarContainerStyle}
                    onCharacterSelect={(char) => this.onCharacterSelect(char)}
                    activeColor={'blue'}
                    scrollBarColor={'#dfdfdf'}
                    onEndReached={this.handleLoadMore}
                    onEndReachedThreshold={0.2}
                    onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
                />

            </View>
        );
    }
}
