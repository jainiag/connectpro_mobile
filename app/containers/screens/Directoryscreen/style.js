import { StyleSheet } from 'react-native';
import * as globals from '../../../utils/globals';
import * as colors from '../../../assets/styles/color';

const { screenHeight, screenWidth } = globals;
const WIDTH = screenWidth;
const ITEM_HEIGHT = globals.screenHeight * 0.1136; // 60;
const OPENITEM_HEIGHT = globals.screenHeight * 0.2272; // 110;

module.exports = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 50,
  },
  onlyRowStyle: {
    flexDirection: 'row',
  },
  itemContainer: {
    width: WIDTH,
    flex: 1,
    flexDirection: 'column',
    borderColor: colors.lightBorder,
    borderBottomWidth: 1,
    marginHorizontal: screenWidth * 0.0468, // 15
  },
  closedStyle: {
    height: ITEM_HEIGHT,
  },
  openStyle: {
    height: OPENITEM_HEIGHT,
  },
  itemTitle: {
    fontWeight: 'bold',
    color: colors.shadeGray,
  },
  itemSubtitle: {
    color: colors.lightShadeGray,
    paddingTop: 0,
  },
  vwSelectedCharStyle: {
    height: screenHeight * 0.0757, // 40
    borderColor: colors.lightBorder,
    borderBottomWidth: 1,
    paddingLeft: screenWidth * 0.0468, // 15
    justifyContent: 'center',
    marginBottom: screenHeight * 0.0094, // 5
  },
  tvSelectedCharStyle: {
    fontSize: globals.font_14,
    color: colors.blue,
  },
  ivProfileImageStyle: {
    height: screenHeight * 0.0852, // 45
    width: screenHeight * 0.0852, // 45,
    borderRadius: screenHeight * 0.0426, // 22.5,
    marginRight: screenWidth * 0.0375, // 12
  },
  vwInfoStyle: {
    justifyContent: 'center',
    width: '60%',
  },
  vwOnlyjustifyContentStyle: {
    justifyContent: 'center',
  },
  vwOnlyAlignedItemStyle: {
    alignItems: 'center',
  },
  dotStyle: {
    height: screenHeight * 0.0284, // 15
    width: screenHeight * 0.0284, // 15
    margin: screenWidth * 0.0156, //5
  },
  scrollBarContainerStyle: {
    position: 'absolute',
    top: 0,
    height: '79%',
  },
  scrollBarPointerContainerStyle: {
    height: 0,
    width: 0,
  },
  vwChildStyle: {
    paddingTop: screenHeight * 0.0189, // 10
    paddingBottom: screenHeight * 0.0378, //10
  },
  toChildButtonStyle: {
    height: screenHeight * 0.0662, // 35
    width: screenHeight * 0.0662, // 35
    borderRadius: (screenHeight * 0.0662) / 2,
    borderColor: colors.darkSkyBlue,
    borderWidth: 1,
    marginLeft: screenWidth * 0.0312, //10
    alignItems: 'center',
    justifyContent: 'center',
  },
});
