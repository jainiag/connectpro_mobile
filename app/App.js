/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Platform,
  StatusBar,
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import * as globals from "./utils/globals";
import { Provider } from 'react-redux';
import { name as appName } from '../app.json';
import Loader from './components/Loader';
import RootNav from './containers/navigators/_RootNavigator';
import LoginStackNavigator from './containers/navigators/_LoginNavigator';
import store from './redux/store';
import * as colors from './assets/styles/color';

export default class App extends Component {


  async componentDidMount() {
    console.disableYellowBox = true
    Platform.OS == 'android' ? StatusBar.setBackgroundColor(colors.bgColor, true) : null;
    NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      globals.isInternetConnected = state.isConnected;
      console.log("isInternetConnected==", globals.isInternetConnected);
    });
  }


  componentWillUnmount() {
    NetInfo.addEventListener(state => {
      console.log("componentWillUnmount Connection type", state.type);
      console.log("componentWillUnmount Is connected?", state.isConnected);
      globals.isInternetConnected = state.isConnected;
      console.log("componentWillUnmount isInternetConnected==", globals.isInternetConnected);
    });
  }


  render() {
    return (
      <Provider store={store}>
        <RootNav />
      </Provider>
    );
  }
}

AppRegistry.registerComponent(appName, () => App);
