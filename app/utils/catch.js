import AsyncStorage from '@react-native-community/async-storage';

// const KEY = 'Async_Key';

class Catch {
  storeData = async (key, value) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (e) {
      // saving error
    }
  };

  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log('Async Value --------->', value);
      }
    } catch (e) {
      // error reading value
    }
  };
}

module.exports = new Catch();
