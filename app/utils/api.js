/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
import { Alert, Platform } from 'react-native';
import * as globals from './globals';

const timeoutObj = {
  success: 'fail',
  Message: 'We are unable to fetch data at this time. Please check back in a few minutes.',
};

const internetErrorObj = {
  success: 'fail',
  Message: 'Please check your internet connectivity',
};

export const buildHeaderNormal = (headerParams = {}) => {
  const header = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };
  Object.assign(header, headerParams);
  return header;
};

export const buildHeader = (headerParams = {}) => {
  const header = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  };
  Object.assign(header, headerParams);
  return header;
};

export const buildHeaderWithToken = (headerParams = {}) => {
  let header = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    AccessToken: (globals.tokenValue) ? (globals.tokenValue != null || globals.tokenValue != undefined)  ? JSON.parse(globals.tokenValue) : null : null,
  };
  Object.assign(header, headerParams);
  console.log("header", header)
  return header;
};


function timeout(ms, promise) {
  return new Promise(function (resolve, reject) {
    setTimeout(function () {
      reject(JSON.stringify(timeoutObj));
    }, ms);
    promise.then(resolve, reject);
  });
}

export const buildHeaderWithMultiPartToken = (headerParams = {}) => {

  const header = {
    Accept: 'application/json',
    'Content-Type': 'multipart/form-data; charset=utf-8',
    AccessToken: JSON.parse(globals.tokenValue),
  };
  Object.assign(header, headerParams);
  return header;
};

const createMediaData = (media, body) => {
  const data = new FormData();

  Object.keys(body).forEach(key => {
    data.append(key, body[key]);
  });

  for (let i = 0; i < media.length; i++) {
    data.append(media[i].name, {
      name: media[i].fileName,
      filename: media[i].fileName,
      type: media[i].type,
      uri: Platform.OS === 'android' ? media[i].uri : media[i].uri.replace('file://', ''),
    });
  }
  return data;
};


async function mediaRequest(
  onResponse,
  data,
  arrMedia,
  type,
  returnType,
  isHeaderRequired,
  featureURL,
  secureRequest
) {
  let response = '';
  let responseJSON;
  console.log(`featureURL >>> ${featureURL}`);
  console.log(`secureRequest ${JSON.stringify(secureRequest)}`);
  console.log(`data >>> ${JSON.stringify(data)}`);
  console.log(`arrMedia >>> ${JSON.stringify(arrMedia)}`);
  console.log(`returnType ${returnType}`);
  console.log(`isHeaderRequired ${isHeaderRequired}`);
  console.log(`type ${type}`);
  try {
    response = await timeout(
      globals.timeoutDuration,
      // eslint-disable-next-line no-undef
      fetch(featureURL, {
        method: type,
        headers: secureRequest,
        body: createMediaData(arrMedia, data),
      })
    );
    console.log(`response status ${response.status}`);
    if (returnType === 'TEXT') {
      responseJSON = await response.text();
    } else if (returnType === 'HTML') {
      responseJSON = await response.html();
    } else {
      responseJSON = await response.json();
    }
    console.log(`responseJSON ${JSON.stringify(responseJSON)}`);

    if (response.status === 200) {
      console.log('onResponse success ');
      onResponse.success(responseJSON);
    } else if (response.status === 440) {
      console.log('onResponse 440 ');
      if (Platform.OS === 'ios') {
        const errorMsg = globals.checkObject(responseJSON, 'message')
          ? responseJSON.message
          : JSON.stringify(responseJSON);
        Alert.alert(globals.appName, errorMsg, { cancelable: false });
      } else {
        const isErrorMsg = globals.checkObject(responseJSON, 'message');
        // eslint-disable-next-line no-unused-expressions
        isErrorMsg
          ? Alert.alert(globals.appName, responseJSON.message)
          : Alert.alert(globals.appName, JSON.stringify(responseJSON));
      }
    } else {
      console.log('onResponse error');
      onResponse.error(responseJSON);
    }
    if (onResponse.complete) {
      console.log('onResponse complete');
      onResponse.complete();
    }
  } catch (error) {
    console.log(`onResponse catch error ${error}`);

    if (globals.isInternetConnected && onResponse.error) {
      console.log('Catch error Nointernet');
      onResponse.error(timeoutObj);
    } else if (!globals.isInternetConnected && onResponse.error) {
      onResponse.error(internetErrorObj);
      console.log('Catch error internal');
    } else if (onResponse.error) {
      // onResponse.error(JSON.parse(error));
      onResponse.error(JSON.stringify(error));

      console.log('Catch error server');
    } else {
      console.log('Catch error unkonwn');
      onResponse.error(response.status);
    }
    if (onResponse.complete) {
      console.log('onResponse catch complete');
      onResponse.complete();
    }
  }
}



async function request(
  onResponse,
  data,
  type,
  returnType,
  isHeaderRequired,
  featureURL,
  secureRequest
) {
  let response = '';
  let responseJSON;
  console.log(`featureURL >>> ${featureURL}`);
  console.log(`secureRequest ${JSON.stringify(secureRequest)}`);
  console.log(`data >>> ${JSON.stringify(data)}`);
  console.log(`returnType ${returnType}`);
  console.log(`isHeaderRequired ${isHeaderRequired}`);
  console.log(`type ${type}`);
  try {
    if (type === 'GET') {
      if (isHeaderRequired) {
        console.log('Request Call get with Header');
        response = await timeout(
          globals.timeoutDuration,
          fetch(featureURL, {
            method: type,
            headers: secureRequest,
          })
        );
      } else {
        console.log('Request Call get without header');
        response = await timeout(
          globals.timeoutDuration,
          fetch(featureURL, {
            method: type,
          })
        );
      }
    } else if (secureRequest['Content-Type'] === 'multipart/form-data') {
      console.log('MULTIPART Request Call');
      response = await timeout(
        globals.timeoutDuration,
        fetch(featureURL, {
          method: type,
          headers: secureRequest,
          body: data,
        })
      );
    } else {
      console.log('Request Call post with header');
      response = await timeout(
        globals.timeoutDuration,
        fetch(featureURL, {
          method: type,
          headers: secureRequest,
          body: JSON.stringify(data),
        })
      );
    }
    // console.log("response " + JSON.stringify(response));
    console.log(`response status ${response.status}`);
    if (returnType === 'TEXT') {
      responseJSON = await response.text();
    } else if (returnType === 'HTML') {
      responseJSON = await response.html();
    } else {
      responseJSON = await response.json();
    }
    console.log(`responseJSON ${JSON.stringify(responseJSON)}`);

    if (response.status === 200) {
      console.log('onResponse success ');
      onResponse.success(responseJSON);
    } else if (response.status === 440) {
      console.log('onResponse 440 ');
      // onResponse.error(responseJSON);
      if (Platform.OS === 'ios') {
        const errorMsg = globals.checkObject(responseJSON, 'message')
          ? responseJSON.message
          : JSON.stringify(responseJSON);
        // AsyncStorage.clear()
        Alert.alert(
          globals.appName,
          errorMsg,
          { cancelable: false }
        );
      } else {
        const isErrorMsg = globals.checkObject(responseJSON, 'message');
        isErrorMsg
          ? Alert.alert(globals.appName, responseJSON.message)
          : Alert.alert(globals.appName, JSON.stringify(responseJSON));
      }
    } else {
      console.log('onResponse error');
      onResponse.error(responseJSON);
    }
    if (onResponse.complete) {
      console.log('onResponse complete');
      onResponse.complete();
    }
  } catch (error) {
    console.log(`onResponse catch error ${error}`);
    if (globals.isInternetConnected && onResponse.error) {
      console.log('If --> Network request failed');
      onResponse.error(timeoutObj)
    }
    else if (!globals.isInternetConnected && onResponse.error) {
      onResponse.error(internetErrorObj);
      console.log('Catch error internal');
    } else if (onResponse.error) {
      onResponse.error(JSON.parse(error));
      console.log('Catch error server');
    } else {
      console.log('Catch error unkonwn');
      onResponse.error(response.status);
    }
    if (onResponse.complete) {
      console.log('onResponse catch complete');
      onResponse.complete();
    }
  }
}

export const API = {
  loginPasscode: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.Authenticate, buildHeader());
  },
  resendClickHereReq: (onResponse,useremail, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.resendEmailClickHere + "?email=" + useremail, buildHeader());
  },
  forgotPasscode: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.ForgotPassword, buildHeader());
  },
  changePasscode: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.ChangePassword, buildHeader());
  },
  userRegister: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.Register, buildHeader());
  },
  resetPasscode: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.ResetPassword, buildHeader());
  },
  getGlobalUpcomingEvents: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetEventsSearchResults, buildHeader());
  },
  getMyEvents: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetMyEvents + "?userId=" + globals.userID, buildHeaderWithToken());
  },
  getMyProfileDetails: (onResponse, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetMyProfileData + "?userId=" + globals.userID, buildHeaderWithToken());
  },
  getEventAttendes: (onResponse, isHeaderRequired, eventId) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetEventsAttendes + eventId, buildHeader());
  },
  getEventDetails: (onResponse, isHeaderRequired, eventId, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetEventDetails + "/" + eventId + "/" + userid, buildHeader());
  },
  getSilmilarMatchesInterest: (onResponse, isHeaderRequired, userid, totalRecoards) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetSilmilarMatchesInterest + "/" + userid + "/" + totalRecoards, buildHeaderWithToken());
  },
  getMyConnection: (onResponse, isHeaderRequired, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetMyConnection + "/" + userid, buildHeaderWithToken());
  },
  getNewConnection: (onResponse, isHeaderRequired, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetNewConnection + "/" + userid, buildHeaderWithToken());
  },
  acceptOrRejectConnection: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.AcceptOrRejectConnection, buildHeaderWithToken());
  },
  cancelNewConnectionRequest: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.CancelNewConnectionRequest, buildHeaderWithToken());
  },
  getAvailableTickets: (onResponse, isHeaderRequired, eventId) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetAvailableTickets + eventId, buildHeaderWithToken());
  },
  resendEmailConfirmation: (onResponse, isHeaderRequired, eventID, email, ) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.ResendEmailConfirmation + "?id=" + eventID + "&confirmationEmail=" + email, buildHeader());
  },
  getCompanyProfile: (onResponse, isHeaderRequired, orgid, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetCompanyProfile + "/" + orgid + "/" + userid, buildHeaderWithToken());
  },
  updateCompanyProfile: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.UpdateCompanyProfile, buildHeaderWithToken());
  },
  getGlobalCommunities: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetGlobalCommunities, buildHeaderWithToken());
  },
  getMyCommunities: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetMyCommunities, buildHeaderWithToken());
  },
  getPortfolioInfo: (onResponse, isHeaderRequired, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetPortfolioInfo + "?userId=" + userid, buildHeaderWithToken());
  },
  savePortfolioInfo: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SavePortfolioInfo, buildHeaderWithToken());
  },
  getProfileCategories: (onResponse, isHeaderRequired, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetProfileCategories + userid, buildHeaderWithToken());
  },
  saveProfileCategories: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveProfileCategories, buildHeaderWithToken());
  },
  getSkillsInterests: (onResponse, isHeaderRequired, userid) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetSkillsInterests + "?UserId=" + userid, buildHeaderWithToken());
  },
  saveSkillsInterests: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveSkillsInterests, buildHeaderWithToken());
  },
  updateCompanyLogoFromProfile: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.UpdateCompanyLogoFromProfile, buildHeaderWithToken());
  },
  updateLinkdinProfilePicture: (onResponse, isHeaderRequired, linkdinProfilePath, userId) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.UpdateLinkdinProfilePicture + "?linkdinProfilePath=" + linkdinProfilePath + "&userId=" + userId, buildHeaderWithToken());
  },
  uploadProfilePictureFile: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.UploadProfilePictureFile, buildHeaderWithToken());
  },
  UpdateProfile: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.UpdateProfile, buildHeaderWithToken());
  },
  communityInfo: (onResponse, isHeaderRequired, CommunityID, userId) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.CommunityInfo + "?CommunityID=" + CommunityID + "&UserID=" + userId, buildHeaderWithToken());
  },
  getSpotLightEvents: (onResponse, isHeaderRequired, CommunityID) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetSpotLightEvent + "?CommunityID=" + CommunityID, buildHeaderWithToken());
  },
  inviteMembers: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.InviteMembers, buildHeaderWithToken());
  },
  saveUserStatusInCommunities: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveUserStatusInCommunities, buildHeaderWithToken());
  },
  saveCommunityStatus: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveCommunityStatus, buildHeaderWithToken());
  },
  acceptRejectReqInviteMember: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.acceptRejectReqInviteMember, buildHeaderWithToken());
  },
  activateCommunity: (onResponse, CommunityId, userId, isHeaderRequired) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.activateCommunity + "?communityId=" + CommunityId + "&isActive=true&userId=" + userId, buildHeaderWithToken());
  },
  discussionsList: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.DiscussionsList, buildHeader());
  },
  saveEventAttendees: (onResponse, data, isHeaderRequired, userid) => {
    request(onResponse, data, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.SaveEventAttendees + userid, buildHeaderWithToken());
  },
  getPaymentMethods: (onResponse, communityId, paymentMethodId, isHeaderRequired ) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetPaymentMethods + communityId+"/"+ paymentMethodId, buildHeaderWithToken());
  },
  getPaymentMethodConfigInfo: (onResponse, communityId, paymentMethodId, isHeaderRequired ) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.GetPaymentMethodConfigInfo + "?CommunityID="+communityId+"&PaymentMethodID="+ paymentMethodId, buildHeaderWithToken());
  },
  saveEventTicketPaymentInformation: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveEventTicketPaymentInformation, buildHeaderWithToken());
  },
  getMembersList: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getmemberList, buildHeaderWithToken());
  },
  saveMemberConnections: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.saveMemberConnection, buildHeaderWithToken());
  },
  getPaymentSummary: (onResponse, paymentId, transactionId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getPaymentSummary + paymentId + "/" + transactionId, buildHeaderWithToken());
  },
  deleteDiscussion: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.DeleteDiscussion, buildHeaderWithToken());
  },
  getCommunityPhotos: (onResponse, communityId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getCommunityPhotoes + "?communityId=" + communityId, buildHeaderWithToken());
  },
  saveDiscussionsComments: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveDiscussionsComments, buildHeaderWithToken());
  },
  getCommunityFiles: (onResponse, communityId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getCommunityFiles + "?communityId=" + communityId, buildHeaderWithToken());
  },
  getCommunityAdmins: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getCommunityAdmins, buildHeaderWithToken());
  },
  getCommunityMembersIntialDataWithId: (onResponse, communityId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetCommunityMembersIntialData + "?communityId=" + communityId, buildHeaderWithToken());
  },
  getCommunityEvents: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getCommunityEvents, buildHeaderWithToken());
  },
  discussionComments: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.DiscussionComments, buildHeaderWithToken());
  },
  saveDiscussionsLikes: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.SaveDiscussionsLikes, buildHeaderWithToken());
  },
  createCommunityDetails: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.createCommunityDetails, buildHeaderWithToken());
  },
  getEndrosments: (onResponse, endrosmentType, userId, isHeaderRequired) => {
    request(onResponse, {}, 'GET', 'JSON', isHeaderRequired, globals.mainUrl + globals.getEndrosments + endrosmentType + "/" + userId, buildHeaderWithToken());
  },
  allGroupsData: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.AllGroupsData, buildHeaderWithToken());
  },
  acceptRejectEndrosement: (onResponse, endorsementId, userId, actionType, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.acceptRejectEndorsement + endorsementId + "/" + userId + "/" + actionType, buildHeaderWithToken());
  },
  joinOrLeaveGroup: (onResponse, isHeaderRequired, groupId, action, userId) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.JoinOrLeaveGroup + "?groupId=" + groupId + "&action=" + action + "&userId=" + userId, buildHeaderWithToken());
  },
  saveCommunityAsync: (onResponse, data, isHeaderRequired, userid) => {
    request(onResponse, data, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.SaveCommunityAsync + "?userId=" + userid, buildHeaderWithToken());
  },
  getMemberDashboardCount: (onResponse, userId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getMemberDashboardCount + "?userId=" + userId, buildHeaderWithToken());
  },
  getMyGroupData: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getMyGroups, buildHeaderWithToken());
  },
  getUserInfoForAutofill: (onResponse, emailId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getUserInfoForAutofill + "?emailId=" + emailId, buildHeaderWithToken());
  },
  groupDashboardInfo: (onResponse, isHeaderRequired, communityId, groupId, userId) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.getGroupDashboardDetails + "?communityId=" + communityId + "&groupId=" + groupId + "&userID=" + userId, buildHeaderWithToken());
  },
  getGroupAdmins: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getGroupAdmins, buildHeaderWithToken());
  },
  getGroupPhotos: (onResponse, groupId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getGroupPhotoes + "?groupId=" + groupId, buildHeaderWithToken());
  },
  getGroupFiles: (onResponse, groupId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getGroupFiles + "?groupId=" + groupId, buildHeaderWithToken());
  },
  deleteComments: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.deleteComments, buildHeaderWithToken());
  },
  saveCommunityDiscussion: (onResponse, data, arrMedia, isHeaderRequired) => {
    mediaRequest(onResponse, data, arrMedia, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.saveCommunityDiscussion, buildHeaderWithMultiPartToken());
  },
  savegroupdiscussions: (onResponse, data, arrMedia, isHeaderRequired) => {
    mediaRequest(onResponse, data, arrMedia, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.savegroupdiscussions, buildHeaderWithMultiPartToken());
  },
  externalLoginCallbackAPI: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', '', isHeaderRequired, globals.mainUrl + globals.ExternalLoginCallbackAPI, buildHeader());
  },
  getGroupDiscussion: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getGroupDiscussion, buildHeaderWithToken());
  },
  deleteGroupDiscussion: (onResponse, groupDiscussionId, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.deleteGroupDiscussion + "?groupDiscussionId=" + groupDiscussionId, buildHeaderWithToken());
  },
  getGroupDiscussionComment: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getGroupDiscussionComment, buildHeaderWithToken());
  },
  saveDiscussionsLike: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.saveDiscussionsLike, buildHeaderWithToken());
  },
  deletDiscussionsComment: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.deletDiscussionsComment, buildHeaderWithToken());
  },
  saveGroupDiscussionComment: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.saveGroupDiscussionComment, buildHeaderWithToken());
  },
  getCommunityMembersIntialData: (onResponse, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.globalMembersSearchInitialData, buildHeaderWithToken());
  },
  SendUserEndorsements: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.senduserEndorsements, buildHeaderWithToken());
  },
  CancelNewconnectionRequest: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.cancelnewConnectionRequest, buildHeaderWithToken());
  },
  getOtherProfileDetails: (onResponse, {}, UserID, isHeaderRequired) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetMyProfileData + "?userId=" + UserID, buildHeaderWithToken());
  },
  GetMailSentInformation: (onResponse, data, UserID, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetMailSentInformation + UserID, buildHeaderWithToken());
  },
  TriggerMailsinQueueStatus: (onResponse, isHeaderRequired) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.triggermailsinqueuestatus, buildHeaderWithToken());
  },
  getProfileData: (onResponse, isHeaderRequired, userID) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.getprofilesData + userID, buildHeaderWithToken());
  },
  GetAttachmentPoco: (onResponse, isHeaderRequired, userId) => {
    request(onResponse, {}, 'GET', 'JSON', isHeaderRequired, globals.mainUrl + globals.getAttachmentPoco + "?mailQueueId=" + userId, buildHeaderWithToken());
  },
  getDraftmailsInformation: (onResponse, isHeaderRequired, userId) => {
    request(onResponse, {}, 'GET', 'JSON', isHeaderRequired, globals.mainUrl + globals.getdraftmailsinformation + "?userId=" + userId, buildHeaderWithToken());
  },
  GetDraftEmailDataToCompose: (onResponse, isHeaderRequired, batchId, userId) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.getDraftEmailDataToCompose + "?BatchId=" + batchId + "&userID=" + userId, buildHeaderWithToken());
  },
  getbulkcontacts: (onResponse, isHeaderRequired, KeyName, KeyId, ) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.getbulkcontacts + "?KeyName=" + KeyName + "&KeyId=" + KeyId, buildHeaderWithToken());
  },
  getbulkcontactswithoutCommunity: (onResponse, isHeaderRequired, KeyName, KeyId, CommunityId) => {
    request(onResponse, {}, "POST", "JSON", isHeaderRequired, globals.mainUrl + globals.getsystemcontacts + "?KeyName=" + KeyName + "&KeyId=" + KeyId +"&CommunityId="+ CommunityId, buildHeaderWithToken());
  },
  savebulkmailrequest: (onResponse, data, arrMedia,  isHeaderRequired) => {
    mediaRequest(onResponse, data, arrMedia,'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.savebulkmailrequest, buildHeaderWithMultiPartToken());
  },
  getChatMessages: (onResponse,userId, memberId, isHeaderRequired) => {
    request(onResponse, {}, 'GET', 'JSON', isHeaderRequired, globals.mainUrl + globals.getChatMessages + "?userId=" + userId +"&memberId="+memberId, buildHeaderWithToken());
  },
  getDraftattachments: (onResponse, isHeaderRequired, batchId) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getdraftattachments + "?batchId=" + batchId, buildHeaderWithToken());
  },
  getMessageThreads: (onResponse, isHeaderRequired, userId) => {
    request(onResponse, {}, 'GET', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetMessageThreads + "?userId=" + userId, buildHeaderWithToken());
  },
  getChatMembers: (onResponse, isHeaderRequired, userId) => {
    request(onResponse, {}, 'GET', 'JSON', isHeaderRequired, globals.mainUrl + globals.GetChatMembers + "?userId=" + userId, buildHeaderWithToken());
  },
  postChatMessage: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.postMessage, buildHeaderWithToken());
  },
  getEmailactivityreport: (onResponse, isHeaderRequired, userId, startDate, endDate) => {
    request(onResponse, {}, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getemailactivityreport + "?UserId=" + userId + "&startDate=" + startDate + "&endDate=" + endDate, buildHeaderWithToken());
  },
  getMailEvents: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getMailEvents, buildHeaderWithToken());
  },
  getUsersInCommunity: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getUsersInCommunity, buildHeaderWithToken());
  },
  getUsersInGroup: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.getUsersInGroup, buildHeaderWithToken());
  },
  onlyrspved: (onResponse, data, isHeaderRequired) => {
    request(onResponse, data, 'POST', 'JSON', isHeaderRequired, globals.mainUrl + globals.onlyrspved, buildHeaderWithToken());
  },
};
