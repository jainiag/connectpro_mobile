import { Platform } from 'react-native';
import * as globals from './globals';
// import * as colors from '../assets/styles/color';

export const registerScreenShadow = {
  width: globals.screenWidth * 0.864,
  height: globals.screenHeight * 0.23,
  color: '#556fef',
  border: 4,
  radius: 10,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    margin: 4,
    alignSelf: 'center',
  },
  
};

export const SetOtpScreen = {
  width: globals.screenWidth * 0.864,
  height: globals.screenHeight * 0.25,
  color: '#556fef',
  border: 4,
  radius: 10,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    margin: 4,
    alignSelf: 'center',
  },
};

export const SetPinScreen = {
  width: globals.screenWidth * 0.9173,
  height: globals.screenHeight * 0.25,
  color: '#556fef',
  border: 4,
  radius: 10,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    margin: 4,
    alignSelf: 'center',
  },
};

export const myAccountScreenUserShadow = {
  width: globals.screenWidth * 0.93,
  height: globals.screenWidth * 0.216,
  color: '#51545c',
  border: 4,
  radius: 4,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    alignSelf: 'center',
    margin: globals.screenWidth * 0.03,
    width: globals.screenWidth * 0.93,
    height: globals.screenWidth * 0.216,
  },
};

export const myAccountScreenTransactionShadow = {
  width: globals.screenWidth * 0.93,
  height: globals.screenWidth * 0.43,
  color: '#51545c',
  border: 4,
  radius: 4,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    alignSelf: 'center',
    margin: globals.screenWidth * 0.03,
    width: globals.screenWidth * 0.93,
    height: globals.screenWidth * 0.43,
  },
};

export const myAccountScreenAboutViewShadow = {
  width: globals.screenWidth * 0.93,
  height: globals.screenWidth * 0.58,
  color: '#51545c',
  border: 4,
  radius: 4,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    alignSelf: 'center',
    margin: globals.screenWidth * 0.03,
    width: globals.screenWidth * 0.93,
    height: globals.screenWidth * 0.58,
  },
};

export const myCardAddScreenShadow = {
  width: globals.screenWidth * 0.9,
  height: globals.screenWidth * 0.5,
  color: '#51545c',
  border: 4,
  radius: 10,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    alignSelf: 'center',
    margin: globals.screenWidth * 0.08,
    width: globals.screenWidth * 0.9,
    height: globals.screenWidth * 0.5,
  },
};

export const myCardScreenShadow = {
  width: globals.screenWidth * 0.93,
  height: globals.screenHeight * 0.19,
  color: '#51545c',
  border: 4,
  radius: 10,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    alignSelf: 'center',
    margin: globals.screenWidth * 0.02,
    width: globals.screenWidth * 0.93,
    height: globals.screenHeight * 0.19,
  },
};

export const myProfileScreenShadow = {
  width: globals.screenWidth * 0.933,
  height: Platform.OS === 'android' ? globals.screenWidth * 0.55 : globals.screenWidth * 0.47,
  color: '#51545C',
  border: 4,
  radius: 0,
  opacity: 0.1,
  x: 0,
  y: 0,
  style: {
    alignSelf: 'center',
    margin: globals.screenWidth * 0.02,
    width: globals.screenWidth * 0.933,
    height:Platform.OS === 'android' ? globals.screenWidth * 0.55 : globals.screenWidth * 0.47,
  },
};
