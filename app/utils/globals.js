import React from 'react';
import { Dimensions, Platform, TouchableOpacity, Text, View, Image, SafeAreaView } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import * as color from '../assets/styles/color';
import * as images from '../assets/images/map';

let iPad = DeviceInfo.getModel();

// export const mainUrl = 'https://cpcommunitydemo.azurewebsites.net/'; //demo
export const mainUrl = 'https://cpcommunityqa.azurewebsites.net/'; // QA
// export const mainUrl = 'https://cpcommunity.azurewebsites.net/'; //Production
export const timeoutDuration = 60000;
export const appName = 'ConnectPro';

export const isInternetConnected = false;
export const WINDOW = Dimensions.get('window');

export var globalEventCount = 120;
export const youtubeUrl = "https://www.youtube.com/watch?";
export const facebookURL = "https://www.facebook.com/profile";
export const linkedinURL = "https://www.linkedin.com/";
export const twitterURL = "https://www.twitter.com/profile";
// terms&conditions and privacyPolicy Url

export const termsCondUrl = 'https://cpcommunityqa.azurewebsites.net/pages/termsandconditions/2';
export const privacyUrl = 'https://cpcommunityqa.azurewebsites.net/pages/privacypolicy/3';
export const authorizeNetPaymentUrl = "https://cpcommunityqa.azurewebsites.net/PaymentGateways/BuyEventTicketsPaymentWithAuthorizeHostedCheckout"

// API endpoints
export const Authenticate = 'api/account/Authenticate'; // Login
export const resendEmailClickHere = 'api/Account/ResendRegistrationEmail'; // Resend email
export const ForgotPassword = 'api/account/forgotpassword'; // forgot pass
export const ChangePassword = 'api/account/ChangePassword'; // change pass
export const ResetPassword = 'api/account/resetpassword'; // Login
export const Register = 'api/account/Register'; // Register
export const GetEventsSearchResults = 'api/event/GetEventsSearchResults'; // Global events
export const GetMyEvents = 'api/member/GetEventsSearchResultsByMember'; // My Events
export const GetMyProfileData = 'api/member/getmyprofiledata'; // My Events
export const GetEventsAttendes = 'api/event/GetAttendeesInfo/';//Get event attendes
export const GetEventDetails = 'api/event/details'; // Get Event Details
export const GetSilmilarMatchesInterest = 'api/member/getsimilarinterestusers'; //Get Silmilar Matches Interest
export const GetMyConnection = 'api/member/GetConnections'; // Get My Connections
export const GetNewConnection = 'api/member/GetNewConnections'; // Get New Connections
export const AcceptOrRejectConnection = 'api/member/AcceptOrRejectConnection'; // AcceptOrRejectConnection
export const CancelNewConnectionRequest = 'api/member/CancelNewConnectionRequest'; // CancelNewConnectionRequest
export const GetAvailableTickets = 'api/event/GetAvailableTickets/'; //Get available tickets
export const ResendEmailConfirmation = 'api/event/ResendConfirmationEmailAsync';//resend email for event registration
export const GetCompanyProfile = 'api/member/GetCompanyProfile'; // Get Company Profile
export const UpdateCompanyProfile = 'api/member/UpdateCompanyInfo'; // Update Company Profile
export const GetPortfolioInfo = 'api/member/GetPortfolioInfo'; // Get Portfolio 
export const SavePortfolioInfo = 'api/member/SavePortfolioInfo'; // Save Portfolio
export const GetProfileCategories = 'api/member/getwhoami/'; // Get profileCategory 
export const SaveProfileCategories = 'api/member/SaveWhoAmI'; // Save profileCategory
export const GetSkillsInterests = 'api/member/getskillsandinterests'; // Get profileCategory 
export const SaveSkillsInterests = 'api/member/saveskillsandinterest'; // Save Portfolio
export const UploadProfilePictureFile = 'api/member/UploadProfilePictureFile';// Upload Profile PictureFile
export const UpdateProfile = 'api/member/UpdateProfileData'; // Update Profile Data
export const GetMessageThreads = 'api/member/getmessagethreads';// Get message threads
export const GetChatMembers = 'api/member/getchatmembers';// Get message threads
export const GetGlobalCommunities = 'api/communities/AllCommunities';//Get global community
export const GetMyCommunities = 'api/communities/GetUserCommunitiesInfo';//Get my community list
export const UpdateCompanyLogoFromProfile = 'api/member/UpdateCompanyLogoFromProfile';//Upload Profile Picture File
export const UpdateLinkdinProfilePicture = 'api/member/UpdateLinkdinProfilePicture'; //Update Linkdin Profile Picture
export const CommunityInfo = "api/communities/CommunityInfo"; // CommunityInfo
export const GetSpotLightEvent = "api/communities/GetSpotLightEvents"; // Get SpotLight Events
export const SaveUserStatusInCommunities = "api/communities/SaveUserStatusInCommunities" //Save User Status In Communities
export const SaveCommunityStatus = "api/communities/SaveCommunityStatus" //Save community status
export const acceptRejectReqInviteMember = "api/communities/AcceptOrRejectPendingRequestsForInviteMember" //Accept or Reject pending requests
export const activateCommunity = "api/communities/ActivateOrInactivateCommunity" //Accept or Reject pending requests
export const InviteMembers = "api/communities/invitemembers" // communities invite members
export const DiscussionsList = "api/communities/DiscussionsList" // DiscussionsList
export const SaveEventAttendees = "api/event/SaveEventAttendees/"; // SaveEventAttendees 
export const GetPaymentMethods = "api/event/GetPaymentMethods/"; // Get payment methods 
export const GetPaymentMethodConfigInfo = "api/paymentgateways/GetPaymentMethodConfigInfo"; // Get payment method information
export const SaveEventTicketPaymentInformation= "api/paymentgateways/SaveEventTicketPaymentInformation"; //Save event ticket payment information
export const getmemberList = "api/member/getmemberdirectory" //Get community members list
export const saveMemberConnection = "api/member/saveconnections" //save community member connection
export const getPaymentSummary = "api/paymentgateways/GetPaymentSummary/" //GetPaymentSummary for booking receipt
export const DeleteDiscussion = "api/communities/DeleteDiscussion" // DeleteDiscussion
export const deleteComments = "api/communities/DeleteDiscussionComments" // deleteComments
export const getCommunityPhotoes = "api/communities/CommunityImages" //Get community images
export const SaveDiscussionsComments = "api/communities/SaveDiscussionsComments" // Save Discussions Comments
export const DiscussionComments = "api/communities/DiscussionComments"//DiscussionComments
export const getCommunityAdmins = "api/communities/GetCommunityUsers" //Get community admins
export const GetCommunityMembersIntialData = "api/communities/GetCommunityMembersIntialData" // GetCommunityMembersIntialData
export const SaveDiscussionsLikes = "api/communities/SaveDiscussionsLikes"//SaveDiscussionsLikes
export const getCommunityFiles = "api/communities/CommunityAttachments" //Get community files
export const getCommunityEvents = "api/event/GetCommunityEvents" //Get community admins
export const createCommunityDetails = "api/communities/CreateCommunityDetails" // CreateCommunityDetails
export const AllGroupsData = "api/groups/AllGroupsData" // AllGroupsData
export const getEndrosments = "api/member/getuserendorsements/" //Get community admins
export const acceptRejectEndorsement = "api/member/AcceptOrRejectEndorsement/" //Accept or Reject Endorsement
export const JoinOrLeaveGroup = "api/groups/JoinOrLeaveGroup" //JoinOrLeaveGroup
export const SaveCommunityAsync = "api/communities/SaveCommunityAsync" // SaveCommunityAsync
export const getMemberDashboardCount = "api/member/MemberDashboardCount"//Get member dashboard count
export const getMyGroups = "api/groups/GetMyGroupsInfo"//Get member dashboard count
export const getUserInfoForAutofill = "api/event/GetUserInfoForAutofill" //Get UserInfo For Autofill
export const getGroupDashboardDetails = "api/groups/GroupInfoData" //Get Group dashboard info data
export const getGroupAdmins = "api/groups/GroupMembers" //Get Group Admins
export const getGroupPhotoes = "api/groups/GetImagesInGroup" //Get Group Photoes
export const getGroupFiles = "api/groups/GetAttachmentsInGroup" //Get group files
export const ExternalLoginCallbackAPI = "api/account/ExternalLoginCallbackAPI" //ExternalLoginCallbackAPI
export const globalMembersSearchInitialData = "api/member/GetMemberDirectorySearchByData" //Global member search initial data
export const senduserEndorsements = "api/member/senduserendorsements" //send user endorsements
export const cancelnewConnectionRequest = "api/member/cancelnewconnectionrequest" // cancel newconnection request
export const saveCommunityDiscussion = "api/communities/SaveCommunityDiscussions" //saveCommunityDiscussion
export const getGroupDiscussion = "api/groups/GetGroupDiscussion" // GetGroupDiscussion
export const deleteGroupDiscussion = "api/groups/DeleteGroupDiscussion" // DeleteGroupDiscussion
export const getGroupDiscussionComment = "api/groups/GetGroupDiscussionComment" // GetGroupDiscussionComment
export const saveDiscussionsLike = "api/groups/SaveDiscussionsLike" // SaveDiscussionsLike
export const deletDiscussionsComment = "api/groups/DeletDiscussionsComment" // DeletDiscussionsComment
export const saveGroupDiscussionComment = "api/groups/SaveGroupDiscussionComment" // SaveGroupDiscussionComment
export const GetMailSentInformation = "api/bulkmail/GetMailSentInformation/" //GetMail Sent Information
export const triggermailsinqueuestatus = "api/bulkmail/triggermailsinqueuestatus" //triggermailsinqueuestatus
export const savegroupdiscussions = "api/groups/savegroupdiscussions" //savegroupdiscussions
export const getprofilesData = "api/member/getprofilesData/" // getprofilesData
export const getAttachmentPoco = "api/bulkmail/GetAttachmentPoco" // GetAttachmentPoco
export const getdraftmailsinformation = "api/bulkmail/getdraftmailsinformation" // getdraftmailsinformation
export const getDraftEmailDataToCompose = "api/bulkmail/GetDraftEmailDataToCompose" // GetDraftEmailDataToCompose
export const getbulkcontacts = "api/bulkmail/getbulkcontacts" // getbulkcontacts
export const getsystemcontacts = "api/bulkmail/getsystemcontacts" // getsystemcontacts
export const savebulkmailrequest = "api/bulkmail/savebulkmailrequest" // savebulkmailrequest
export const getChatMessages = "api/member/getmessages" // Get chat messages
export const getdraftattachments = "api/bulkmail/getdraftattachments" // get draft attachments
export const postMessage = "api/member/postmessage" // get draft attachments
export const getemailactivityreport = "api/bulkmail/getemailactivityreport" //get email activity report
export const getMailEvents = "api/BulkMail/GetMailEvents" // Get Mail Events
export const getUsersInCommunity = "api/bulkmail/GetUsersInCommunity" // GetUsersInCommunity
export const getUsersInGroup = "api/bulkmail/GetUsersInGroup" // GetUsersInGroup
export const onlyrspved = "api/bulkmail/GetEventAttendeesForAnEvent/onlyrspved" // onlyrspved

// AsyncStorage Keys
export const APITOKENVALUE = 'APITOKENVALUE';
export const ISLOGINVALUE = 'ISLOGINVALUE';
export const LOGINRESPONSEKEY = 'LOGINRESPONSEKEY';
export const USERID = 'USERID';
export const COUNTRYID = 'COUNTRYID';
export const LANGUAGEKEY = 'LANGUAGEKEY';
export const USERIMAGEURL = '';
export const userProfileImage = '';
export const IS_FIRST_OPEN = 'IS_FIRST_OPEN';
export const FCMTOKEN = 'FCMTOKEN';
export const USERFIRSTNAME = 'USERFIRSTNAME';
export const USERLASTNAME = 'USERLASTNAME';
export const LOGINUSERNAME = 'LOGINUSERNAME';
export const LOGINUSERPROFILEIMAGE = 'LOGINUSERPROFILEIMAGE';
export const LOGINUSEREMAIL = 'LOGINUSEREMAIL';
export const USEREMAIL = 'USEREMAIL';
export const USERPASS = 'USERPASS';
export const LOGINACCESSTOKEN = 'ACCESSTOKEN';
export const LOGINUSERID = 'LOGINID';
export const LOGINUSERDATA = 'LOGINUSERDATA';
export const PAYERINFODATA = 'PAYERINFODATA';
export const ATTENDEEDETAIL = 'ATTENDEEDETAIL';
export const ATT_EMAIL = 'ATT_EMAIL';
export const ATT_FNAME = 'ATT_FNAME';
export const ATT_LNAME = 'ATT_LNAME';
export const ATT_MOBILENO = 'ATT_MOBILENO';
export const ATT_COMP_NAME = 'ATT_COMP_NAME';
export const ATT_CHANGE_ICON = 'ATT_CHANGE_ICON';
export const ATT_USERID = 'ATT_USERID';
export const LINKEDINCOMPANYPROFILE = 'LINKEDINCOMPANYPROFILE';
export const LINKEDINMYPROFILE = 'LINKEDINMYPROFILE';
export const MYPROFILEPICTURE = 'MYPROFILEPICTURE';
export const ORGANIZATIONID = 'ORGANIZATIONID';
export const MESSAGESNOTIFICATIONSTATUS = 'MESSAGESNOTIFICATIONSTATUS';
export const ENDORSENOTIFICATIONSTATUS = 'ENDORSENOTIFICATIONSTATUS';
export const CONNECTIONNOTIFICATIONSTATUS = 'CONNECTIONNOTIFICATIONSTATUS';
export const ALLTIFICATIONSTATUS = 'ALLTIFICATIONSTATUS';
export const DASHBOARDMODULESCOUNT = 'DASHBOARDMODULESCOUNT';
export const FB_LOGINKEY = "FB_LOGINKEY";
export const OTHER_ORG_ID = "OTHER_ORG_ID";
export const MESSAGESNOTIFICATIONSTATUSN = false;
export const ENDORSENOTIFICATIONSTATUSN = false;
export const CONNECTIONNOTIFICATIONSTATUSN = false;

export var tokenValue = '';
export var userID = '';
export var loginFullName = '';
export var loginEmail = '';
export var loginProfileImg = '';
export var payerInfoData = {};
export var att_Email = '';
export var att_Fname = '';
export var att_Lname = '';
export var att_MobileNo = '';
export var att_Comp_name = '';
export var att_Change_icon = 0;
export var att_userId = [];
export var linkedin_company_profile = '';
export var linkedin_my_profile = '';
export var my_profile_picture = '';
export var organizationId = '';
export var allItems = '';
export var fbLoginKey = true;
export var other_org_id = "";
export var allItems_msg = '';
export var allItems_endorsement = '';
export var allItems_connection = true;
export var PasswordChangeRequired = false;


// Global values
export const RESETPASSWORDCODE = 'RESETPASSWORDCODE';
export const screenWidth = Dimensions.get('window').width;
export const screenHeight = Dimensions.get('window').height;
export const iPhoneX =
  Platform.OS === 'ios' &&
  Dimensions.get('window').height === 812 &&
  Dimensions.get('window').width === 375;


// --------------------------------- FontSizes For Whole App -------------------------------------

// More big new
export const font_8 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 9 : Platform.OS === 'ios' ? screenWidth * 0.025 : screenWidth * 0.0225; // 8
export const font_9 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 10 : Platform.OS === 'ios' ? screenWidth * 0.0275 : screenWidth * 0.025; // 9
export const font_10 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 12 : Platform.OS === 'ios' ? screenWidth * 0.03 : screenWidth * 0.0275; // 10
export const font_11 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 14 : Platform.OS === 'ios' ? screenWidth * 0.0325 : screenWidth * 0.03; // 11
export const font_12 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 16 : screenWidth * 0.032; // 12
export const font_13 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 20 : screenWidth * 0.0346; // 13
export const font_14 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 23 : Platform.OS === 'ios' ? screenWidth * 0.04 : screenWidth * 0.0375; // 14
// export const font_14 = Platform.OS === 'ios' ? screenHeight * 0.02 : screenHeight * 0.0172; // 14
export const font_15 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 25 : Platform.OS === 'ios' ? screenWidth * 0.0435 : screenWidth * 0.04; // 14
export const font_16 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 27 : Platform.OS === 'ios' ? screenWidth * 0.0475 : screenWidth * 0.0425; // 16
// export const font_16 = Platform.OS === 'ios' ? screenHeight * 0.02 : screenHeight * 0.02; // 16
export const font_17 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 29 : Platform.OS === 'ios' ? screenWidth * 0.05 : screenWidth * 0.045; // 17
export const font_18 = (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 31 : Platform.OS === 'ios' ? screenWidth * 0.048 : screenWidth * 0.0475; // 18
export const font_19 = screenWidth * 0.0475; // 19
export const font_20 = screenWidth * 0.053; // 20
export const font_22 = screenWidth * 0.055; // 22
export const font_24 = screenWidth * 0.064; // 24
export const font_26 = screenWidth * 0.0693; // 26
export const font_28 = screenWidth * 0.07; // 28
export const font_29 = screenWidth * 0.075; // 28
export const font_32 = screenWidth * 0.08; // 32
export const font_36 = screenWidth * 0.09; // 36
export const font_40 = screenWidth * 0.106; // 40
export const font_53 = 53; // 53

// --------------------------------- Later spacing For Whole App -------------------------------------

export const ltr_sp_013 = screenWidth * 0.000346; // 0.13 pt
export const ltr_sp_014 = screenWidth * 0.000373; // 0.14 pt
export const ltr_sp_015 = screenWidth * 0.0004; // 0.15 pt
export const ltr_sp_016 = screenWidth * 0.000426; // 0.16 pt
export const ltr_sp_024 = screenWidth * 0.00064; // 0.24 pt
export const ltr_sp_044 = screenWidth * 0.00117; // 0.44pt


export const secretKey = 'Aijoh2A+tohW8ov7leeyai0Equ9a*G9d';
export const sandboxClientId = 'AX78CDQXkf1Tfq2XpACRkMw9Q9JCH88MuWZxtw8VOeZLAmeme1AfK1e1SRwtva-4NHMhz4spcDNUzdX8';
export const productionClientId = 'AeUJBKnCGijGNzwLR52OMBa4roJkA4h8xH4v_5RUktbpOG-HfZ7eAYTwiYDChkLDk8SJCOgBhyFp49HY';

/**
 * convert given value as per device width
 * @function getHorizontalBaseValue
 * @param {*} value give value to convet based on width
 */
export function getHorizontalBaseValue(value) {
  return screenWidth * (value / screenWidth);
}

/**
 * convert given value as per device height
 * @function getVerticleBaseValue
 * @param {*} value give value to convet based on height
 */
export function getVerticleBaseValue(value) {
  return screenHeight * (value / screenHeight);
}

export const ConnectProbackButton = (navigation, title, key) => (
  (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
    <SafeAreaView style={{ backgroundColor: color.white }}>
      <View style={{ marginTop: screenWidth * 0.02, width: screenWidth, backgroundColor: color.white, height: screenHeight * 0.07, flexDirection: 'row', borderBottomColor: color.gray, borderBottomWidth: 1 }}>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <TouchableOpacity
              onPress={() => (key == "Success") || (key == "Receipt") ? navigation.navigate("GLOBAL_EVENT_SCREEN_DASHBOARD") : navigation.goBack(null, { pathName: "Dashboard" })}
              style={{ flexDirection: 'row', marginLeft: 10, alignItems: 'center', justifyContent: 'center', }}>
              <Image source={images.loginAndRegisterScreen.backIcon}
                resizeMode={"contain"}
                style={{ height: screenHeight * 0.035, width: screenHeight * 0.035, tintColor: color.warmBlue, alignSelf: 'center' }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 2, justifyContent: 'center', }}>
          <Text numberOfLines={1} style={{ fontSize: font_16, color: color.black, textAlign: 'left', fontWeight: '500', marginLeft: screenWidth * 0.03 }}>
            {title}
          </Text>
        </View>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
        </View>
      </View>
    </SafeAreaView>
    :
    <SafeAreaView style={{ backgroundColor: color.white }}>
      <View style={{ width: screenWidth, backgroundColor: color.white, height: screenHeight * 0.08, flexDirection: 'row', borderBottomColor: color.gray, borderBottomWidth: 1 }}>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <TouchableOpacity
              onPress={() => (key == "Success") || (key == "Receipt") ? navigation.navigate("GLOBAL_EVENT_SCREEN_DASHBOARD") : navigation.goBack(null)}
              style={{ flexDirection: 'row', marginLeft: 10, alignItems: 'center', justifyContent: 'center', }}>
              <Image source={images.loginAndRegisterScreen.backIcon}
                resizeMode={"contain"}
                style={{ height: screenHeight * 0.035, width: screenHeight * 0.035, tintColor: color.warmBlue, alignSelf: 'center' }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 2, justifyContent: 'center', }}>
          <Text numberOfLines={1} style={{ fontSize: font_20, color: color.black, textAlign: 'left', fontWeight: '500', marginLeft: screenWidth * 0.03 }}>
            {title}
          </Text>
        </View>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
        </View>
      </View>
    </SafeAreaView>
);

export const ConnectProDrawerButton = (navigation, title) => (
  (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?
    <SafeAreaView style={{ backgroundColor: color.white }}>
      <View style={{ marginTop: screenWidth * 0.02, width: screenWidth, backgroundColor: color.white, height: screenHeight * 0.09, flexDirection: 'row', borderBottomColor: color.gray, borderBottomWidth: 1 }}>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <TouchableOpacity
              onPress={() => (title == "Member Connections") ? navigation.goBack(null) : navigation.openDrawer()}
              style={{ flexDirection: 'row', marginLeft: 10, alignItems: 'center', justifyContent: 'center', }}>
              <Image source={images.navigationIcon.navIcon}
                resizeMode={"contain"}
                style={{ height: screenHeight * 0.035, width: screenHeight * 0.035, tintColor: color.warmBlue, alignSelf: 'center' }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 2, justifyContent: 'center', }}>
          <Text style={{ fontSize: font_16, color: color.black, textAlign: 'left', fontWeight: '500', marginLeft: screenWidth * 0.03 }}>
            {title}
          </Text>
        </View>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
        </View>
      </View>
    </SafeAreaView>
    :
    <SafeAreaView style={{ backgroundColor: color.white }}>
      <View style={{ width: screenWidth, backgroundColor: color.white, height: screenHeight * 0.08, flexDirection: 'row', borderBottomColor: color.gray, borderBottomWidth: 1 }}>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', }}>
            <TouchableOpacity
              onPress={() => (title == "Member Connections") ? navigation.goBack(null) : navigation.openDrawer()}
              style={{ flexDirection: 'row', marginLeft: 10, alignItems: 'center', justifyContent: 'center', }}>
              <Image source={images.navigationIcon.navIcon}
                resizeMode={"contain"}
                style={{ height: screenHeight * 0.035, width: screenHeight * 0.035, tintColor: color.warmBlue, alignSelf: 'center' }} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 2, justifyContent: 'center', }}>
          <Text style={{ fontSize: font_20, color: color.black, textAlign: 'left', fontWeight: '500', marginLeft: screenWidth * 0.03 }}>
            {title}
          </Text>
        </View>
        <View style={{ flex: 0.3, justifyContent: 'center', alignItems: 'center' }}>
        </View>
      </View>
    </SafeAreaView>
);


export const righButton = (iconName, onRightClick) => (
  <TouchableOpacity
    onPress={() => onRightClick()}
    style={{
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: screenWidth * 0.0312,
      paddingBottom: iPhoneX ? 4 : 0,
    }}
  >
    <Image
      source={images.headerIcon.searchIcon}
      style={{
        height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 34 : 21,
        width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 32 : 20,
        marginTop: 7,
      }}
    />
  </TouchableOpacity>
);

export const headerStyle = {
  // backgroundColor: color.HeaderColor,
  backgroundColor: color.white,
  height: screenHeight * 0.08,
};

export const headerTitleStyle = {
  // fontFamily: fontRobotoTextMedium,
  fontSize:
    Platform.OS === 'ios'
      ? Dimensions.get('window').width * 0.052
      : Dimensions.get('window').width * 0.0452,
  color: color.black,
  textAlign: 'center',
  flex: 2,
  fontWeight: 'normal',
};




export const checkObject = function (responesData, key) {
  const responseData = responesData;
  let checkObject;
  if (typeof responseData === 'string') {
    checkObject = true;
  } else if (typeof responseData === 'object') {
    if (responesData !== undefined) {
      if (responesData.hasOwnProperty(key)) {
        if (responesData[key] !== undefined) {
          if (responesData[key] !== '') {
            if (responesData[key] !== null) {
              checkObject = true;
            } else {
              checkObject = false;
            }
          } else {
            checkObject = false;
          }
        } else {
          checkObject = false;
        }
      } else {
        checkObject = false;
      }
    } else {
      console.log('No Data-->', responesData);
    }
  } else if (typeof responseData === 'array') {
    console.log('data-->', responseData);
    if (responesData.key !== undefined) {
      return responesData;
    }
  }
  return checkObject;
};

export const checkImageObject = function (responesData, key) {
  const responseData = responesData;
  let checkObject;
  if (typeof responseData === 'string') {
    if (responesData.startsWith('http://') || responesData.startsWith('https://')) {
      checkObject = true;
    } else {
      checkObject = false;
    }
  } else if (typeof responseData === 'object') {
    if (responesData !== undefined) {
      if (responesData.hasOwnProperty(key)) {
        if (responesData[key] != undefined) {
          if (responesData[key] != '') {
            if (responesData[key] != null) {
              if (
                typeof (responesData[key] === 'string') &&
                (responesData[key].startsWith('http://') ||
                  responesData[key].startsWith('https://'))
              ) {
                checkObject = true;
              } else {
                checkObject = false;
              }
            } else {
              checkObject = false;
            }
          } else {
            checkObject = false;
          }
        } else {
          checkObject = false;
        }
      } else {
        checkObject = false;
      }
    } else {
      checkObject = false;
    }
  } else if (typeof responseData === 'array') {
    if (responesData.key !== undefined) {
      return responesData;
    }
  }
  return checkObject;
};

// placeholder
export const PLACEHOLDERTXT = {
  LOGIN: {
    ENTER_EMAIL: ' Enter Your Email Id',
    EMAIL_ID: ' Email Id',
    PASSWORD: ' Password',
    CONFIRM_PASSWORD: ' Confirm Password',
    CURRENT_PASSWORD: ' Current Password',
    NEW_PASSWORD: ' New Password',
    CONFIRM_NEW_PASSWORD: ' Confirm New Password',
    LOGIN: 'LOG IN',
  },
  REGISTER: {
    FIRST_NAME: 'First Name',
    LAST_NAME: 'Last Name',
    SIGN_UP: 'SIGN UP',
  },
};

// button text
export const BTNTEXT = {
  LOGINSCREEN: {
    FORGOTPASSLOGINBTNTEXT: 'SUBMIT',
    CHANGEPASSLOGINBTNTEXT: 'CHANGE PASSWORD',
    RESETPASSLOGINBTNTEXT: 'RESET',
    LOGIN_EMAIL_VERFIY: 'Your email Address is Not Verified.',
    LOGIN_PLZ: 'Please',
    LOGIN_CLICK: 'Click Here',
    LOGIN_VERIFY: 'to Verify',
    LOGIN_DONT_HAVE_ACC: "Don't Have an Account? ",
    LOGIN_SIGN_UP: 'Sign Up',
    LOGIN_BY_CREATING_TEXT: 'By creating an account you agree to ConnectPro Global',
    LOGIN_CONDITIONS: 'Terms of Conditions',
    LOGIN_SLASHLINE: '|',
    LOGIN_PRIVACY: 'Privacy Policy',
    LOGIN_OR: 'OR',
    CUSTOM_BTN_TEXT: 'Try again',
  },
  REGISTERSCREEN: {
    REG_CREATE_ACC: 'Create a New Account',
    REG_BTNTEXT: 'REGISTER',
  },
  PAYMENTSCREENS: {
    TOTAL: '(Total Price)',
    CONTINUE: 'CONTINUE',
    ADD: 'ADD',
    DONE: 'DONE',
    SAVE: 'Save',
    CANCLE: 'Cancel',
    BOOKING_INFO: 'Booking Information',
    ADD_PLAYER_INFO: 'Add Payer Information',
    ATTENDEE_DETAILS: 'Attendees Details',
    GOLD_TICKETS: 'Gold Tickets',
    ATTENDEE_DETAILS2: 'Attendee 2 Details',
    ATTENDEE2: 'Attendee 2',
    SILVER_TICKETS: 'Silver Ticket',
    BOOKING_RECEIPT: 'Booking Receipt',
    MAKE_PAYMENT: 'Make Payment',
    PAYMENT_METHOD: 'Payment Method',
    ALREADY_REGISTRED: 'Already Registered?',
    RESEND: 'Resend Confirmation',
    BOOK_TICKETS: 'Book Tickets',
    STAR: 'Star Ticket',
    PLATINUM: 'Platinum Ticket',
    DIMOND: 'Diamond Ticket',
    PREMIUM: 'Premium Ticket',
    BOOKING_CLOSED: 'Booking closed',
    SOLD_OUT: 'Sold Out',
  }
};

// alert messeges
export const MESSAGE = {
  LOGINSTACK: {
    AUTH_ENTEREMAIL: 'Please enter your email',
    AUTH_REGEMAIL: 'Please enter registered email ID',
    AUTH_VALIDEMAIL: 'Please enter a valid email',
    AUTH_VALIDPASS: 'Please enter valid password',
    AUTH_VALIDCONFIRMPASS: 'Please enter valid confirm password',
    AUTH_SUCCESS: 'success',
    AUTH_FAILED: 'failed',
    AUTH_PASSWORD_LENGTH: 'Please enter more than 6 character',
    AUTH_PASSWORD: 'Please enter a password',
    AUTH_SAMEPASS: 'Make sure your New Password & Confirm Password are the same',
    AUTH_RESETSAMEPASS: 'Make sure your Password & Confirm Password are same',
    AUTH_NEWPASS:
      'Please enter at least one special character, one alphabetic, one numeric, one capital in new password',
    AUTH_CURRENTPASS:
      'Please enter at least one special character, one alphabetic, one numeric, one capital in current password',
    AUTH_CONFIRMTPASS:
      'Please enter at least one special character, one alphabetic, one numeric, one capital in Confirm New password',
    AUTH_FILLDETAILS: 'Please enter details',
    AUTH_PASSVALID:
      'Please enter at least one special character, one alphabetic, one numeric, one capital in password',
    AUTH_ENTERCURRENTPASS: 'Please enter Current Password',
    AUTH_ENTERNEWPASS: 'Please enter New Password',
    AUTH_ENTERCONFIRMPASS: 'Please enter Confirm Password',
    AUTH_ENTERCONFIRMNEWPASS: 'Please enter Confirm New Password',
    AUTH_ENTERPASS: 'Please enter a password',
    AUTH_PASSCONFIRM:
      'Please enter at least one special charater, one alphabetic, one numeric, one Capital in confirm password',
    EMAIL_VERIFY: 'Your email address is not verified. Please check your email to verify.',
    // EMAIL_VERIFY: 'Your email address is not verified. Please',
    AUTH_CHECK_INTERNET: 'Please check your internet connection',
    AUTH_NEED_7_CHARACTER: 'Password needs minimum 7 characters',
    VALIDATE_ALERT: 'Please at least fill one field',
  },
  REGISTERSTACK: {
    REG_FIRSTNAME: 'Please enter first name',
    REG_LASTNAME: 'Please enter last name',
    REG_EMAILVALID: "Please enter a valid email",
    REG_NEWPASSVALID:
      'Please enter at least one special charater, one alphabetic, one numeric, one capital in New password',
    REG_CURRENTPASSVALID:
      'Please enter at least one special charater, one alphabetic, one numeric, one capital in Current password',
  },
  FORGOTSCREEN: {
    FRGT_TITLE: 'Forgot Password?',
    FRGT_REG_EMAIL_ADD: 'Enter your registered email address to',
    FRGT_RECEIVE_PASSWORD_RESET_LINK: 'receive the password reset link in an email.',
    FRGT_DO_YOU_HAVE_ACC: 'Do You Have An Account?',
    RESET_PASSWORD_TITLE: 'Reset Password',
    CHANGE_PASSWORD_TITLE: 'Change Password',
  },
  EVENTDASHBOARD: {
    SEARCH_EVENT_TITLE: 'Search Events',
    SEARCH_BY_EVENT_DATE: 'Search By Event Date',
    START_DATE: 'Start Date',
    END_DATE: 'End Date',
    SEARCH_TITLE: 'SEARCH',
  },
  SIDEMENU_OPTIONS: {
    MENU_HAEDER: 'Menu',
    DASHBOARD: 'Dashboard',
    GLOBAL_MEMBERS: 'Global Members',
    GLOBAL_EVENTS: 'Global Events',
    GLOBAL_COMMUNITIES: 'Global Communities',
    NOTIFICATIONS: 'Notifications',
    CHANGE_PASSWORD: 'Change Password',
    LOG_OUT: 'Logout',
  },
  SEARCH_SCREEN: {
    SEARCH_BY_EMAIL_DATE: 'Select the Schedule Date & Time',
    SEARCH_BY_EVENT_DATE: 'Search by Event Date',
    SEARCH_BY_LOCATION: 'Search by Location',
    SEARCH_BY_EVENT_NAME: 'Search by Event Name',
    SEARCH_CONFIRM_DATE: 'Confirm Date',
    SEARCH_CANCEL_DATE: 'Cancel Date',
    SEARCH_COMMUNITYNAME: 'Search by Community Name',
    SEARCH_ADMIN: 'Search Admins..',
    SEARCH_EVENTS: 'Search by event name',
    SEARCH_GROUPS: 'Search by group name',
    SEARCH_GROUPS_MEMBERS: 'Search by member name',
  },
  GLOBAL_EVENT: {
    GLOBAL_EVENT_UPCOMING: 'Upcoming',
    GLOBAL_EVENT_PAST: 'Past',
  },
  CONNECTIONS: {
    MY_CONNECTION: 'My Connections',
    NEW_CONNECTION_REQUEST: 'New connection Requests',
  },
  ATTENDEE_DETAILS: {
    VALID_COMPANY_NAME: 'Please enter Organization name',
    VALID_LAST_NAME: 'Please enter valid last name',
    ENTER_LAST_NAME: 'Please enter last name',
    ENTER_FIRST_NAME: 'Please enter first name',
    VALID_FIRST_NAME: 'Please enter valid first name',
    VALID_MOB_NO: 'Please enter valid mobile number',
    VALID_EMAIL: 'Please enter a valid email',
    ENTER_EMAIL_ID: 'Please enter your email',
    SUCCESS: 'done',
  },
  ADD_PLAYER_INFO: {
    VALID_NAME: 'Please enter valid name',
    ENTER_MOBNO: 'Please enter mobile number',
    VALID_MOB_NO: 'Please enter valid mobile number',
    ENTER_LAST_NAME: 'Please enter last name',
    ENTER_FIRST_NAME: 'Please enter first name',
    VALID_FIRST_NAME: 'Please enter valid first name',
    VALID_LAST_NAME: 'Please enter valid last name',
    VALID_EMAIL: 'Please enter a valid email',
    ENTER_EMAIL_ID: 'Please enter your email',
    SUCCESS: 'done',
    PROFFESION: 'Please enter profession',
    ENTER_NOTE: 'Please enter note',
    ORGANIZATION_NAME: 'Organization name',
  },
  EVENTREGISTRATIONMODULE: {
    SELECTTICKET: "Please select at least one ticket for booking"
  },
  PROFILE: {
    LINKEDINPROFILE: 'Linkedin profile updated successfully',
    ENTERFULLNAME: 'Please enter Name'
  },
  COMMUNITIES: {
    NOTE_TEXT: "Note: Invitations will not be sent to the recipients who are already members of the community",
    CREATE_COMM_NAME: 'Please enter Community Name',
    CREATE_COMM_MAXNAME: 'Please enter name of your community between 5 and 1000 characters long',
    CREATE_COMM_MAXABOUTCOMMUNITY: 'Please enter about community value between 5 and 1000 characters long',
    CREATE_COMM_PRUPOSECATEGORY: 'Please select Purpose of your Community',
    CREATE_COMM_OTHERPRUPOSECATEGORY: 'Please enter other purpose of your community',
    CREATE_COMM_SELECTCATEGORY: 'Please select Category of your Community',
    CREATE_COMM_SELECTTYPE: 'Please select Type of your Community',
    CREATE_COMM_ABOUTCOMMUNITY: 'Please enter description about your Community',
    CREATE_DISSCOMM_NAME: 'The post content is required and cannot be empty',
    CREATE_DISSCOMM_LINK:'Hyperlink URL should not be empty',

    CREATE_DISSCOMM_COMMENTNAME: 'The comment content is required and cannot be empty'
  },
  MANAGE_EMAILS: {
    SUBJECT_NEEDED: 'Subject is required',
    TOADDRESS_NEEDED: 'To email required',
    EMAILDID_COLUMN_NEEDED: 'Email Id column is missing',
    ONLY_CSV_FILE: 'Please select only CSV file',
    ONLYTHREEFILES: 'Only 3 files can be uploaded',
  }
};



// Imgs Placeholder URL

export const Square_img = "https://i2.wp.com/svartisenrana.no/wp-content/uploads/2019/05/placeholder-image.png?fit=1280%2C960"; //  suare_light color imgPlaceholder
export const User_img = "https://ethioexplorer.com/wp-content/uploads/2019/07/Author__Placeholder.png";// User imgPlaceholder
export const Event_img = "https://via.placeholder.com/150"; //  Event(150*150)  imgPlaceholder
export const DarkSquare_img = "https://www.parkerici.org/wp-content/uploads/2019/07/3x2-dark-placeholder.jpg";//  suare_dark color imgPlaceholder


// Error message
export const SEARCHNODATAFOUND = {
  MYCOMMUNITY_SEARCH: "No results found matching your search criteria"
}

export const ERROR_MESSAGE = {
  INTERNET_NOT_AVAILABLE: 'Internet not available. Please try again',
  SERVER_ERR: {
    SERVER_ERR_PLZ_TRY_AGAIN: 'Something went wrong. Please try again',
    SERVER_ERR_SMTHING_WENT_WRNG: 'Something went wrong',
  },
  DATA_NOT_AVAILABLE: {
    UPCOMING_DATA_NOT_AVLB: 'Currently there are no upcoming events for you',
    PAST_DATA_NOT_AVLB: 'There are no any past events for you',
    ATTENDEE_NOT_AVAILABLE: 'Currently there are no attendees for this event',
    EVENT_RESOURCE_ALBUM_NOT_AVAILABLE: 'Currently there are no albums for this event',
    EVENT_RESOURCE_DOCUMENT_NOT_AVAILABLE: 'Currently there are no documents for this event',
    EVENT_RESOURCE_VIDEOS_NOT_AVAILABLE: 'Currently there are no videos for this event',
    SPOSORS_DATA_NOT_AVLB: 'Currently  there are no Sponsors for this event',
    SPEAKERS_DATA_NOT_AVLB: 'Currently there are no Speakers for this event',
    AGENDA_DATA_NOT_AVLB: 'Currently there is no Agenda for this event',
    CANNOT_SHAR_EVENT: "Currently can not share this event",
    NOTHING_FOR_EVENT: 'Nothing For this event',
    LOCATION_DATA_NOT_AVLB: "location is not available for this event",
    MATCHES_DATA_NOT_AVLB: 'Currently you do not have any matches',
    PENDING_REQUEST: 'Request Pending',
    NEWREQUEST_DATA_NOT_AVLB: "Currently you don't have any connections",
    MYCONNECTION_DATA_NOT_AVLB: "Currently you don't have any connections",
    MESSAGETHREAD_DATA_NOT_AVLB: "You don't have any recent chats. Select a member to start a new chat",
    MEMBER_DATA_NOT_AVLB: 'There are no members available for a chat',
    MYCOMMUNITIES_DATA_NOT_AVLB: 'Currently there are not any communities available for you to join',
    GLOBALCOMMUNITIES_DATA_NOT_AVLB: 'Currently there are not any global communities available',
    DISCUSSION_POST_DATA_NOT_AVLB: 'Currently there are no posts for this community',
    DISCUSSION_POST_DATA_NOT_AVLB_SEARCH: 'No results found matching your search criteria',
    DISCUSSION_GROUP_POST_DATA_NOT_AVLB: 'Currently there is no post for this group',
    COMMUNITY_RESOURCES_NOT_AVLB: 'There are no resources at this time',
    COMMUNITY_ADMIN_NOT_AVLB: 'There are not any admin available for this community',
    COMMUNITY_ADMIN_NOT_AVLB_SEARCH: 'No results found matching your search criteria',
    COMMUNITY_GROUP_NOT_AVLB: 'There are no groups for this community',
    COMMUNITY_GROUP_NOT_AVLB_SEARCH: 'There are no groups for this community',
    COMMUNITY_MEMBERS_NOT_AVLB: 'There are no members available',
    COMMUNITY_MEMBERS_NOT_AVLB_SEARCH: 'No results found matching your search criteria',
    COMMUNITY_EVENTS_NOT_AVLB: 'There are no events for this community',
    COMMUNITY_EVENTS_NOT_AVLB_SEARCH: 'No results found matching your search criteria',
    ADDCONTACT_DATA_NOT_AVLB: 'At least one Group or Event or Member should be selected',
    ENDORSEMENTSENT_DATA_NOT_AVLB: 'You haven’t sent any endorsements',
    ENDORSEMENTRECEIVED_DATA_NOT_AVLB: 'You haven’t received any endorsements',
    DATA_NOT_AVAILABLE_IN_SEARCH: "No results found matching your search criteria",

  },
  EVENT_ERR: {
    MYEVENT_DATA_NOT_AVLB: 'Currently there are no upcoming events for you to attend',
    MYEVENT_PDFURL_ERROR: 'Sorry, Could not open pdf. Please try again later'
  },
  MY_PROFILE: {
    PORFILE_CATEGORY_NOT_AVLBL: 'Currently you have not created a profile category',
    PORFILE_PORTFOLIO_NOT_AVLBL: 'Currently you have not updated your portfolio',
    PORFILE_COMMUNITY_NOT_AVLBL: 'currently you have not joined any communities',
    PORFILE_SKILLS_NOT_AVLBL: 'You have not included any skills',
    PORFILE_INTERESTS_NOT_AVLBL: 'You have not included any interests',
    BUSINESS_CATEGORY: 'Currently you have not included any business categories',
    BUSINESS_DESCRIPTION: 'Currently you do not have any business description',

  },
  COMMUNITIES: {
    SPOTLIGHT_EVNT_NOT_AVLBL: 'Currently you do not have any spotlight events',
  },
  MANAGE_EMAILS: {
    SENT_EMAIL_NOT_AVLBL: "There are no mails available",
    DRAFT_EMAIL_NOT_AVLBL: "No results found matching your search criteria",
    DRAFT_EMAIL_NOT_AVLBL_DATA: "You don't have any draft emails"
  }
};

export const CountryCodeArray =
  [
    {
      "name": "Afghanistan",
      "dial_code": "93",
      "code": "AF"
    },
    {
      "name": "Albania",
      "dial_code": "355",
      "code": "AL"
    },
    {
      "name": "Algeria",
      "dial_code": "213",
      "code": "DZ"
    },
    {
      "name": "American Samoa",
      "dial_code": "1",
      "code": "AS"
    },
    {
      "name": "Andorra",
      "dial_code": "376",
      "code": "AD"
    },
    {
      "name": "Angola",
      "dial_code": "244",
      "code": "AO"
    },
    {
      "name": "Anguilla",
      "dial_code": "1",
      "code": "AI"
    },
    {
      "name": "Antarctica",
      "dial_code": "672",
      "code": "AQ"
    },
    {
      "name": "Antigua and Barbuda",
      "dial_code": "1268",
      "code": "AG"
    },

    {
      "name": "Argentina",
      "dial_code": "54",
      "code": "AR"
    },
    {
      "name": "Armenia",
      "dial_code": "374",
      "code": "AM"
    },
    {
      "name": "Aruba",
      "dial_code": "297",
      "code": "AW"
    },
    {
      "name": "Australia",
      "dial_code": "61",
      "code": "AU"
    },
    {
      "name": "Austria",
      "dial_code": "43",
      "code": "AT"
    },
    {
      "name": "Azerbaijan",
      "dial_code": "994",
      "code": "AZ"
    },
    {
      "name": "Bahamas",
      "dial_code": "1",
      "code": "BS"
    },
    {
      "name": "Bahrain",
      "dial_code": "973",
      "code": "BH"
    },
    {
      "name": "Bangladesh",
      "dial_code": "880",
      "code": "BD"
    },
    {
      "name": "Barbados",
      "dial_code": "1",
      "code": "BB"
    },
    {
      "name": "Belarus",
      "dial_code": "375",
      "code": "BY"
    },
    {
      "name": "Belgium",
      "dial_code": "32",
      "code": "BE"
    },
    {
      "name": "Belize",
      "dial_code": "501",
      "code": "BZ"
    },
    {
      "name": "Benin",
      "dial_code": "229",
      "code": "BJ"
    },
    {
      "name": "Bermuda",
      "dial_code": "1",
      "code": "BM"
    },
    {
      "name": "Bhutan",
      "dial_code": "975",
      "code": "BT"
    },
    {
      "name": "Bolivia",
      "dial_code": "591",
      "code": "BO"
    },
    {
      "name": "Bosnia and Herzegovina",
      "dial_code": "387",
      "code": "BA"
    },
    {
      "name": "Botswana",
      "dial_code": "267",
      "code": "BW"
    },
    {
      "name": "Bouvet Island",
      "dial_code": "47",
      "code": "BV"
    },
    {
      "name": "Brazil",
      "dial_code": "55",
      "code": "BR"
    },
    {
      "name": "British Indian Ocean Territory",
      "dial_code": "246",
      "code": "IO"
    },
    {
      "name": "British Virgin Islands",
      "dial_code": "1",
      "code": "VG"
    },
    {
      "name": "Brunei",
      "dial_code": "673",
      "code": "BN"
    },
    {
      "name": "Bulgaria",
      "dial_code": "359",
      "code": "BG"
    },
    {
      "name": "Burkina Faso",
      "dial_code": "226",
      "code": "BF"
    },
    {
      "name": "Burundi",
      "dial_code": "257",
      "code": "BI"
    },
    {
      "name": "Cambodia",
      "dial_code": "855",
      "code": "KH"
    },
    {
      "name": "Cameroon",
      "dial_code": "237",
      "code": "CM"
    },
    {
      "name": "Canada",
      "dial_code": "1",
      "code": "CA"
    },
    {
      "name": "Cape Verde",
      "dial_code": "238",
      "code": "CV"
    },
    {
      "name": "Cayman Islands",
      "dial_code": "1",
      "code": "KY"
    },
    {
      "name": "Central African Republic",
      "dial_code": "236",
      "code": "CF"
    },
    {
      "name": "Chad",
      "dial_code": "235",
      "code": "TD"
    },
    {
      "name": "Chile",
      "dial_code": "56",
      "code": "CL"
    },
    {
      "name": "China",
      "dial_code": "86",
      "code": "CN"
    },
    {
      "name": "Christmas Island",
      "dial_code": "61",
      "code": "CX"
    },
    {
      "name": "Cocos (Keeling) Islands",
      "dial_code": "61",
      "code": "CC"
    },
    {
      "name": "Colombia",
      "dial_code": "57",
      "code": "CO"
    },
    {
      "name": "Comoros",
      "dial_code": "269",
      "code": "KM"
    },
    {
      "name": "Congo- Brazzaville",
      "dial_code": "242",
      "code": "CG"
    },
    {
      "name": "Congo- Kinshasa",
      "dial_code": "243",
      "code": "CD"
    },
    {
      "name": "Cook Islands",
      "dial_code": "682",
      "code": "CK"
    },
    {
      "name": "Costa Rica",
      "dial_code": "506",
      "code": "CR"
    },
    {
      "name": "Cote d'Ivoire",
      "dial_code": "225",
      "code": "CI"
    },
    {
      "name": "Croatia",
      "dial_code": "385",
      "code": "HR"
    },
    {
      "name": "Cuba",
      "dial_code": "53",
      "code": "CU"
    },
    {
      "name": "Curacao",
      "dial_code": "599",
      "code": "CW"
    },
    {
      "name": "Cyprus",
      "dial_code": "357",
      "code": "CY"
    },
    {
      "name": "Czech Republic",
      "dial_code": "420",
      "code": "CZ"
    },
    {
      "name": "Denmark",
      "dial_code": "45",
      "code": "DK"
    },
    {
      "name": "Djibouti",
      "dial_code": "253",
      "code": "DJ"
    },
    {
      "name": "Dominica",
      "dial_code": "1",
      "code": "DM"
    },
    {
      "name": "Dominican Republic",
      "dial_code": "1",
      "code": "DO"
    },
    {
      "name": "Ecuador",
      "dial_code": "593",
      "code": "EC"
    },
    {
      "name": "Egypt",
      "dial_code": "20",
      "code": "EG"
    },
    {
      "name": "El Salvador",
      "dial_code": "503",
      "code": "SV"
    },
    {
      "name": "Equatorial Guinea",
      "dial_code": "240",
      "code": "GQ"
    },
    {
      "name": "Eritrea",
      "dial_code": "291",
      "code": "ER"
    },
    {
      "name": "Estonia",
      "dial_code": "372",
      "code": "EE"
    },
    {
      "name": "Ethiopia",
      "dial_code": "251",
      "code": "ET"
    },
    {
      "name": "Falkland Islands",
      "dial_code": "500",
      "code": "FK"
    },
    {
      "name": "Faroe Islands",
      "dial_code": "298",
      "code": "FO"
    },
    {
      "name": "Fiji",
      "dial_code": "679",
      "code": "FJ"
    },
    {
      "name": "Finland",
      "dial_code": "358",
      "code": "FI"
    },
    {
      "name": "France",
      "dial_code": "33",
      "code": "FR"
    },
    {
      "name": "French Polynesia",
      "dial_code": "689",
      "code": "PF"
    },
    {
      "name": "Gabon",
      "dial_code": "241",
      "code": "GA"
    },
    {
      "name": "Gambia",
      "dial_code": "220",
      "code": "GM"
    },
    {
      "name": "Georgia",
      "dial_code": "995",
      "code": "GE"
    },
    {
      "name": "Germany",
      "dial_code": "49",
      "code": "DE"
    },
    {
      "name": "Ghana",
      "dial_code": "233",
      "code": "GH"
    },
    {
      "name": "Gibraltar",
      "dial_code": "350",
      "code": "GI"
    },
    {
      "name": "Greece",
      "dial_code": "30",
      "code": "GR"
    },
    {
      "name": "Greenland",
      "dial_code": "299",
      "code": "GL"
    },
    {
      "name": "Grenada",
      "dial_code": "1",
      "code": "GD"
    },
    {
      "name": "Guam",
      "dial_code": "1",
      "code": "GU"
    },
    {
      "name": "Guatemala",
      "dial_code": "502",
      "code": "GT"
    },
    {
      "name": "Guernsey",
      "dial_code": "44",
      "code": "GG"
    },
    {
      "name": "Guinea",
      "dial_code": "224",
      "code": "GN"
    },
    {
      "name": "Guinea-Bissau",
      "dial_code": "245",
      "code": "GW"
    },
    {
      "name": "Guyana",
      "dial_code": "592",
      "code": "GY"
    },
    {
      "name": "Haiti",
      "dial_code": "509",
      "code": "HT"
    },
    {
      "name": "Heard Island and McDonald Islands",
      "dial_code": "672",
      "code": "HM"
    },
    {
      "name": "Honduras",
      "dial_code": "504",
      "code": "HN"
    },
    {
      "name": "Hong Kong SAR China",
      "dial_code": "652",
      "code": "HK"
    },
    {
      "name": "Hungary",
      "dial_code": "36",
      "code": "HU"
    },
    {
      "name": "Iceland",
      "dial_code": "354",
      "code": "IS"
    },
    {
      "name": "India",
      "dial_code": "91",
      "code": "IN"
    },
    {
      "name": "Indonesia",
      "dial_code": "62",
      "code": "ID"
    },
    {
      "name": "Iran",
      "dial_code": "98",
      "code": "IR"
    },
    {
      "name": "Iraq",
      "dial_code": "964",
      "code": "IQ"
    },
    {
      "name": "Ireland",
      "dial_code": "353",
      "code": "IE"
    },
    {
      "name": "Isle of Man",
      "dial_code": "44",
      "code": "IM"
    },
    {
      "name": "Israel",
      "dial_code": "972",
      "code": "IL"
    },
    {
      "name": "Italy",
      "dial_code": "39",
      "code": "IT"
    },
    {
      "name": "Jamaica",
      "dial_code": "1",
      "code": "JM"
    },
    {
      "name": "Japan",
      "dial_code": "81",
      "code": "JP"
    },
    {
      "name": "Jersey",
      "dial_code": "44",
      "code": "JE"
    },
    {
      "name": "Jordan",
      "dial_code": "962",
      "code": "JO"
    },
    {
      "name": "Kazakhstan",
      "dial_code": "7",
      "code": "KZ"
    },
    {
      "name": "Kenya",
      "dial_code": "254",
      "code": "KE"
    },
    {
      "name": "Kiribati",
      "dial_code": "686",
      "code": "KI"
    },
    {
      "name": "Kosovo",
      "dial_code": "383",
      "code": "XK"
    },
    {
      "name": "Kuwait",
      "dial_code": "965",
      "code": "KW"
    },
    {
      "name": "Kyrgyzstan",
      "dial_code": "996",
      "code": "KG"
    },
    {
      "name": "Laos",
      "dial_code": "856",
      "code": "LA"
    },
    {
      "name": "Latvia",
      "dial_code": "371",
      "code": "LV"
    },
    {
      "name": "Lebanon",
      "dial_code": "961",
      "code": "LB"
    },
    {
      "name": "Lesotho",
      "dial_code": "266",
      "code": "LS"
    },
    {
      "name": "Liberia",
      "dial_code": "231",
      "code": "LR"
    },
    {
      "name": "Libya",
      "dial_code": "218",
      "code": "LY"
    },
    {
      "name": "Liechtenstein",
      "dial_code": "423",
      "code": "LI"
    },
    {
      "name": "Lithuania",
      "dial_code": "370",
      "code": "LT"
    },
    {
      "name": "Luxembourg",
      "dial_code": "352",
      "code": "LU"
    },
    {
      "name": "Macau SAR China",
      "dial_code": "853",
      "code": "MO"
    },
    {
      "name": "Macedonia",
      "dial_code": "389",
      "code": "MK"
    },
    {
      "name": "Madagascar",
      "dial_code": "261",
      "code": "MG"
    },
    {
      "name": "Malawi",
      "dial_code": "265",
      "code": "MW"
    },
    {
      "name": "Malaysia",
      "dial_code": "60",
      "code": "MY"
    },
    {
      "name": "Maldives",
      "dial_code": "960",
      "code": "MV"
    },
    {
      "name": "Mali",
      "dial_code": "223",
      "code": "ML"
    },
    {
      "name": "Malta",
      "dial_code": "356",
      "code": "MT"
    },
    {
      "name": "Marshall Islands",
      "dial_code": "692",
      "code": "MH"
    },
    {
      "name": "Martinique",
      "dial_code": "596",
      "code": "MQ"
    },
    {
      "name": "Mauritania",
      "dial_code": "222",
      "code": "MR"
    },
    {
      "name": "Mauritius",
      "dial_code": "230",
      "code": "MU"
    },
    {
      "name": "Mayotte",
      "dial_code": "262",
      "code": "YT"
    },
    {
      "name": "Mexico",
      "dial_code": "52",
      "code": "MX"
    },
    {
      "name": "Micronesia",
      "dial_code": "691",
      "code": "FM"
    },
    {
      "name": "Moldova",
      "dial_code": "373",
      "code": "MD"
    },
    {
      "name": "Monaco",
      "dial_code": "377",
      "code": "MC"
    },
    {
      "name": "Mongolia",
      "dial_code": "976",
      "code": "MN"
    },
    {
      "name": "Montenegro",
      "dial_code": "382",
      "code": "ME"
    },
    {
      "name": "Montserrat",
      "dial_code": "1",
      "code": "MS"
    },
    {
      "name": "Morocco",
      "dial_code": "212",
      "code": "MA"
    },
    {
      "name": "Mozambique",
      "dial_code": "258",
      "code": "MZ"
    },
    {
      "name": "Myanmar (Burma)",
      "dial_code": "95",
      "code": "MM"
    },
    {
      "name": "Namibia",
      "dial_code": "264",
      "code": "NA"
    },
    {
      "name": "Nauru",
      "dial_code": "674",
      "code": "NR"
    },
    {
      "name": "Nepal",
      "dial_code": "977",
      "code": "NP"
    },
    {
      "name": "Netherlands",
      "dial_code": "31",
      "code": "NL"
    },
    {
      "name": "Netherlands Antilles",
      "dial_code": "599",
      "code": "AN"
    },
    {
      "name": "New Caledonia",
      "dial_code": "687",
      "code": "NC"
    },
    {
      "name": "New Zealand",
      "dial_code": "64",
      "code": "NZ"
    },
    {
      "name": "Nicaragua",
      "dial_code": "505",
      "code": "NI"
    },
    {
      "name": "Niger",
      "dial_code": "227",
      "code": "NE"
    },
    {
      "name": "Nigeria",
      "dial_code": "234",
      "code": "NG"
    },
    {
      "name": "Niue",
      "dial_code": "683",
      "code": "NU"
    },
    {
      "name": "North Korea",
      "dial_code": "850",
      "code": "KP"
    },
    {
      "name": "Northern Mariana Islands",
      "dial_code": "1",
      "code": "MP"
    },
    {
      "name": "Norway",
      "dial_code": "47",
      "code": "NO"
    },
    {
      "name": "Oman",
      "dial_code": "968",
      "code": "OM"
    },
    {
      "name": "Pakistan",
      "dial_code": "92",
      "code": "PK"
    },
    {
      "name": "Palau",
      "dial_code": "680",
      "code": "PW"
    },
    {
      "name": "Palestinian Territory",
      "dial_code": "970",
      "code": "PS"
    },
    {
      "name": "Panama",
      "dial_code": "507",
      "code": "PA"
    },
    {
      "name": "Papua New Guinea",
      "dial_code": "675",
      "code": "PG"
    },
    {
      "name": "Paraguay",
      "dial_code": "595",
      "code": "PY"
    },
    {
      "name": "Peru",
      "dial_code": "51",
      "code": "PE"
    },
    {
      "name": "Philippines",
      "dial_code": "63",
      "code": "PH"
    },
    {
      "name": "Pitcairn Islands",
      "dial_code": "64",
      "code": "PN"
    },
    {
      "name": "Poland",
      "dial_code": "48",
      "code": "PL"
    },
    {
      "name": "Portugal",
      "dial_code": "351",
      "code": "PT"
    },
    {
      "name": "Puerto Rico",
      "dial_code": "1",
      "code": "PR"
    },
    {
      "name": "Qatar",
      "dial_code": "974",
      "code": "QA"
    },
    {
      "name": "Réunion",
      "dial_code": "262",
      "code": "RE"
    },
    {
      "name": "Romania",
      "dial_code": "40",
      "code": "RO"
    },
    {
      "name": "Russia",
      "dial_code": "7",
      "code": "RU"
    },
    {
      "name": "Rwanda",
      "dial_code": "250",
      "code": "RW"
    },
    {
      "name": "Samoa",
      "dial_code": "685",
      "code": "WS"
    },
    {
      "name": "San Marino",
      "dial_code": "378",
      "code": "SM"
    },
    {
      "name": "São Tomé and Príncipe",
      "dial_code": "239",
      "code": "ST"
    },
    {
      "name": "Saudi Arabia",
      "dial_code": "966",
      "code": "SA"
    },
    {
      "name": "Senegal",
      "dial_code": "221",
      "code": "SN"
    },
    {
      "name": "Serbia",
      "dial_code": "381",
      "code": "RS"
    },
    {
      "name": "Seychelles",
      "dial_code": "248",
      "code": "SC"
    },
    {
      "name": "Sierra Leone",
      "dial_code": "232",
      "code": "SL"
    },
    {
      "name": "Singapore",
      "dial_code": "65",
      "code": "SG"
    },
    {
      "name": "Sint Maarten",
      "dial_code": "1",
      "code": "SX"
    },
    {
      "name": "Slovakia",
      "dial_code": "421",
      "code": "SK"
    },
    {
      "name": "Slovenia",
      "dial_code": "386",
      "code": "SI"
    },
    {
      "name": "Solomon Islands",
      "dial_code": "677",
      "code": "SB"
    },
    {
      "name": "Somalia",
      "dial_code": "252",
      "code": "SO"
    },
    {
      "name": "South Africa",
      "dial_code": "27",
      "code": "ZA"
    },
    {
      "name": "South Georgia",
      "dial_code": "500",
      "code": "GS"
    },
    {
      "name": "South Korea",
      "dial_code": "82",
      "code": "KR"
    },
    {
      "name": "South Sudan",
      "dial_code": "211",
      "code": "SS"
    },
    {
      "name": "Spain",
      "dial_code": "34",
      "code": "ES"
    },
    {
      "name": "Sri Lanka",
      "dial_code": "94",
      "code": "LK"
    },
    {
      "name": "St.Barthélemy",
      "dial_code": "590",
      "code": "BL"
    },
    {
      "name": "St.Helena",
      "dial_code": "290",
      "code": "SH"
    },
    {
      "name": "St.Kitts and Nevis",
      "dial_code": "1",
      "code": "KN"
    },
    {
      "name": "St.Lucia",
      "dial_code": "1",
      "code": "LC"
    },
    {
      "name": "St.Martin",
      "dial_code": "590",
      "code": "MF"
    },
    {
      "name": "St.Pierre and Miquelon",
      "dial_code": "508",
      "code": "PM"
    },
    {
      "name": "St.Vincent and Grenadines",
      "dial_code": "1",
      "code": "VC"
    },
    {
      "name": "Sudan",
      "dial_code": "249",
      "code": "SD"
    },
    {
      "name": "Suriname",
      "dial_code": "597",
      "code": "SR"
    },
    {
      "name": "Svalbard and Jan Mayen",
      "dial_code": "47",
      "code": "SJ"
    },
    {
      "name": "Swaziland",
      "dial_code": "268",
      "code": "SZ"
    },
    {
      "name": "Sweden",
      "dial_code": "46",
      "code": "SE"
    },
    {
      "name": "Switzerland",
      "dial_code": "41",
      "code": "CH"
    },
    {
      "name": "Syria",
      "dial_code": "963",
      "code": "SY"
    },
    {
      "name": "Taiwan",
      "dial_code": "886",
      "code": "TW"
    },
    {
      "name": "Tajikistan",
      "dial_code": "992",
      "code": "TJ"
    },
    {
      "name": "Tanzania",
      "dial_code": "255",
      "code": "TZ"
    },
    {
      "name": "Thailand",
      "dial_code": "66",
      "code": "TH"
    },
    {
      "name": "Timor-Leste",
      "dial_code": "670",
      "code": "TL"
    },
    {
      "name": "Togo",
      "dial_code": "228",
      "code": "TG"
    },
    {
      "name": "Tokelau",
      "dial_code": "690",
      "code": "TK"
    },
    {
      "name": "Tonga",
      "dial_code": "676",
      "code": "TO"
    },
    {
      "name": "Trinidad and Tobago",
      "dial_code": "1",
      "code": "TT"
    },
    {
      "name": "Tunisia",
      "dial_code": "216",
      "code": "TN"
    },
    {
      "name": "Turkey",
      "dial_code": "90",
      "code": "TR"
    },
    {
      "name": "Turkmenistan",
      "dial_code": "993",
      "code": "TM"
    },
    {
      "name": "Turks and Caicos Islands",
      "dial_code": "1649",
      "code": "TC"
    },
    {
      "name": "Tuvalu",
      "dial_code": "688",
      "code": "TV"
    },
    {
      "name": "U.S Virgin Islands",
      "dial_code": "1",
      "code": "VI"
    },
    {
      "name": "Uganda",
      "dial_code": "256",
      "code": "UG"
    },
    {
      "name": "Ukraine",
      "dial_code": "380",
      "code": "UA"
    },
    {
      "name": "United Arab Emirates",
      "dial_code": "971",
      "code": "AE"
    },
    {
      "name": "United Kingdom",
      "dial_code": "44",
      "code": "GB"
    },
    {
      "name": "United States",
      "dial_code": "1",
      "code": "US"
    },
    {
      "name": "United States Minor Outlying Islands",
      "dial_code": "246",
      "code": "UM"
    },
    {
      "name": "United States Virgin Islands",
      "dial_code": "1340",
      "code": "VI"
    },
    {
      "name": "Uruguay",
      "dial_code": "598",
      "code": "UY"
    },
    {
      "name": "Uzbekistan",
      "dial_code": "998",
      "code": "UZ"
    },
    {
      "name": "Vanuatu",
      "dial_code": "678",
      "code": "VU"
    },
    {
      "name": "Vatican City",
      "dial_code": "379",
      "code": "VC"
    },
    {
      "name": "Venezuela",
      "dial_code": "58",
      "code": "VE"
    },
    {
      "name": "VietNam",
      "dial_code": "84",
      "code": "VN"
    },
    {
      "name": "Wallis and Futuna",
      "dial_code": "681",
      "code": "WF"
    },
    {
      "name": "Western Sahara",
      "dial_code": "212",
      "code": "EH"
    },
    {
      "name": "Yemen",
      "dial_code": "967",
      "code": "YE"
    },
    {
      "name": "Zambia",
      "dial_code": "260",
      "code": "ZM"
    },
    {
      "name": "Zimbabwe",
      "dial_code": "263",
      "code": "ZW"
    },
    {
      "name": "Åland Islands",
      "dial_code": "358",
      "code": "AX"
    }
  ]

// country list
export const COUNTRY_LIST = [
  'AF',
  'AL',
  'DZ',
  'AS',
  'AD',
  'AO',
  'AI',
  'AQ',
  'AG',
  'AR',
  'AM',
  'AW',
  'AU',
  'AT',
  'AZ',
  'BS',
  'BH',
  'BD',
  'BB',
  'BY',
  'BE',
  'BZ',
  'BJ',
  'BM',
  'BT',
  'BO',
  'BA',
  'BW',
  'BV',
  'BR',
  'IO',
  'VG',
  'BN',
  'BG',
  'BF',
  'BI',
  'KH',
  'CM',
  'CA',
  'CV',
  'KY',
  'CF',
  'TD',
  'CL',
  'CN',
  'CX',
  'CC',
  'CO',
  'KM',
  'CK',
  'CR',
  'HR',
  'CU',
  'CW',
  'CY',
  'CZ',
  'CD',
  'DK',
  'DJ',
  'DM',
  'DO',
  'EC',
  'EG',
  'SV',
  'GQ',
  'ER',
  'EE',
  'ET',
  'FK',
  'FO',
  'FJ',
  'FI',
  'FR',
  'GF',
  'PF',

  'GA',
  'GM',
  'GE',
  'DE',
  'GH',
  'GI',
  'GR',
  'GL',
  'GD',
  'GP',
  'GU',
  'GT',
  'GG',
  'GN',
  'GW',
  'GY',
  'HT',
  'HM',
  'HN',
  'HK',
  'HU',
  'IS',
  'IN',
  'ID',
  'IR',
  'IQ',
  'IE',
  'IM',
  'IL',
  'IT',
  'CI',
  'JM',
  'JP',
  'JE',
  'JO',
  'KZ',
  'KE',
  'KI',
  'XK',
  'KW',
  'KG',
  'LA',
  'LV',
  'LB',
  'LS',
  'LR',
  'LY',
  'LI',
  'LT',
  'LU',
  'MO',
  'MK',
  'MG',
  'MW',
  'MY',
  'MV',
  'ML',
  'MT',
  'MH',
  'MQ',
  'MR',
  'MU',
  'YT',
  'MX',
  'FM',
  'MD',
  'MC',
  'MN',
  'ME',
  'MS',
  'MA',
  'MZ',
  'MM',
  'NA',
  'NR',
  'NP',
  'NL',
  'NC',
  'NZ',
  'NI',
  'NE',
  'NG',
  'NU',
  'NF',
  'KP',
  'MP',
  'NO',
  'OM',
  'PK',
  'PW',
  'PS',
  'PA',
  'PG',
  'PY',
  'PE',
  'PH',
  'PN',
  'PL',
  'PT',
  'PR',
  'QA',
  'CG',
  'RO',
  'RU',
  'RW',
  'RE',
  'BL',
  'KN',
  'LC',
  'MF',
  'PM',
  'VC',
  'WS',
  'SM',
  'SA',
  'SN',
  'RS',
  'SC',
  'SL',
  'SG',
  'SX',
  'SK',
  'SI',
  'SB',
  'SO',
  'ZA',
  'GS',
  'KR',
  'SS',
  'ES',
  'LK',
  'SD',
  'SR',
  'SJ',
  'SZ',
  'SE',
  'CH',
  'SY',
  'ST',
  'TW',
  'TJ',
  'TZ',
  'TH',
  'TL',
  'TG',
  'TK',
  'TO',
  'TT',
  'TN',
  'TR',
  'TM',
  'TC',
  'TV',
  'UG',
  'UA',
  'AE',
  'GB',
  'US',
  'UM',
  'VI',
  'UY',
  'UZ',
  'VU',
  'VA',
  'VE',
  'VN',
  'WF',
  'EH',
  'YE',
  'ZM',
  'ZW',
  'AX',
];

// drawer screen name
export const DASHBOARD_SCREEN = 'DASHBOARD_SCREEN';
export const GLOBAL_MEMBER_SCREEN = 'GLOBAL_MEMBER_SCREEN';
export const GLOBAL_EVENT_SCREEN = 'GLOBAL_EVENT_SCREEN';
export const GLOBAL_COMMUNITIES_SCREEN = 'GLOBAL_COMMUNITIES_SCREEN';
export const NOTIFICATION_SCREEN = 'NOTIFICATION_SCREEN';
export const CHANGE_PASSWORD_SCREEN = 'CHANGE_PASSWORD_SCREEN';
export const MYEVENTS_SCREEN = 'MYEVENTS_SCREEN';

export const LOCATION_SCREEN = 'LOCATION_SCREEN';
export const VIEW_MAP = 'VIEW_MAP';

// used in directory screen, do not remove
export const selectedCharacter = '';
