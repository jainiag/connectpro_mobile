

import { Alert } from 'react-native';
import * as globals from '../utils/globals';

//---------------------------------------------------
// Class: Validation
//---------------------------------------------------

class Validation {

  /**
   * Regular Expression Method for url
   */
  validateURL(textval) {
    var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
    return urlregex.test(textval);
  }

  youtubeUrlValidation(url){
    var regex = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/
    return regex.test(url)
  }

  fbUrlValidation(url){
    var regex = /http(?:s)?:\/\/(?:www\.)?facebook\.com\/([a-zA-Z0-9_]+)/ ;
    return regex.test(url);
  }

  twitterUrlValidation(url){
    var regex = /http(?:s)?:\/\/(?:www\.)?twitter\.com\/([a-zA-Z0-9_]+)/ ;
    return regex.test(url);
  }

  linkedInUrlValidation(url){
    var regex = /(ftp|http|https):\/\/?(?:www\.)?linkedin.com/ ;
    return regex.test(url)
  }
  /**
     * Regular Expression Method for url
     */
  validateURLWithText(textval) {
    var urlregex = /((?:(http|https|Http|Https|rtsp|Rtsp):\/\/(?:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,64}(?:\:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,25})?\@)?)?((?:(?:[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnrwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eouw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\:\d{1,5})?)(\/(?:(?:[a-zA-Z0-9\;\/\?\:\@\&\=\#\~\-\.\+\!\*\'\(\)\,\_])|(?:\%[a-fA-F0-9]{2}))*)?(?:\b|$)/gi;
    return urlregex.test(textval);
  }

  /**
     * Regular Expression Method for TextInput Empty
     */
  textInputCheck(txtInput) {
    // var regex = /^[a-zA-Z ]+$/;
    // console.log("TXTInput-->" + txtInput);

    if (txtInput !== null && txtInput.trim().length > 0) {
      return true;
    }
    return false;
  }

  /**
   * Regular Expression Method for TextInput Special Character
   */
  specialCharacterCheck(txtInput) {
    const regex = /^[a-zA-Z ]+$/;
    // console.log("TXTInput-->" + txtInput);

    if (regex.test(txtInput)) {
      return true;
    }
    // Alert.alert(globals.appName, emptyMessage);
    return false;
  }

  /**
   * Regular Expression Method for TextInput allSpecialCharacter
   */
  allSpecialCharacter(txtInput) {
    const regex = /[`~,.<>;':"\/\[\]\|{}()=_+-]/;

    if (!regex.test(txtInput)) {
      return true;
    }
    // Alert.alert(globals.appName, emptyMessage);
    return false;
  }

  /**
   * Regular Expression Method for Date compare
   */
  dateCheck(date1, date2, message) {
    if (date1 > date2) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for valid Month
   */
  validMonth(month) {
    // const regex ='/^\d{2}$/';
    if (month > 12 || month <= 0) {
      return false;
      // Alert.alert(globals.appName, I18n.t('AUTH_ALERT_VALID_MONTH'));
    }
    return true;
  }

  /**
   * Regular Expression Method for is Valid Register Mobile
   */
  isValidRegisterMobile(mobileNum) {
    if (mobileNum !== 10) {
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for connectPro Valid Mobile Nub
   */
  connectProValidMobileNub(mobileNum) {
    if (mobileNum == 0) {
      return true;
    } else if (mobileNum > 6 && mobileNum < 13) {
      return true;
    }
    return false;
  }

  /**
   * Regular Expression Method for valid OTP
   */
  validOTP(otp) {
    if (otp !== 4) {
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for valid Pin
   */
  validPin(pin) {
    if (pin !== 6) {
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for Textinput Empty
   */
  // Check empty TextInput
  emptyTextInput(txtInput, message) {
    if (txtInput == null || txtInput.length <= 0) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for is Valid Email
   */
  isValidEmail(email, message) {
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regex.test(email)) {
      // Alert.alert(globals.appName, message)
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for is Valid CardNo
   */
  isValidCardNo(cardno, message) {
    if (cardno === 19) {
      return true;
    }
    return false;
    // Alert.alert(globals.appName, I18n.t('AUTH_ALERT_VALID_CARDNO'));
  }

  /**
   * Regular Expression Method for Valid card id
   */
  isValidCardID(number, message) {
    const regex = /^\d{16}$/;
    if (!regex.test(number)) {
      // Alert.alert(globals.appName, message)
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for Valid mobile number
   */
  isValidMobileNumber(number, message) {
    const regex = /^[0-9]{5,15}$/;
    if (!regex.test(number)) {
      // Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  /**
  * Regular Expression Method for is Valid Cheque Number
  */
  isValidChequeNumber(number, message) {
    const regex = /^[0-9]{5,15}$/;
    if (!regex.test(number)) {
      // Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for Valid Phone number
   */
  isValidPhoneNumber(number, message) {
    const regex = /^[0-9()+ -]+$/;
    if (!regex.test(number)) {
      // Alert.alert(globals.appName, message)
      return false;
    }
    return true;
  }

  /**
  * Regular Expression Method for is Valid Password
  */
  isValidPassword(password, message) {
    const regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{7,}$/;
    // const regex = /^(?=.*[A-Za-z])(?=.*d)[A-Za-zd]{7,}$/;
    if (!regex.test(password)) {
      // Alert.alert(globals.appName, message)
      return false;
    }
    return true;
  }

  /**
   * Regular Expression Method for Check Password length greater than minimum
   */
  passwordLength(password, min, message) {
    if (password.length < min) {
      // Alert.alert(globals.appName, message + " " + min + " characters")
      return false;
    }
    return true;
  }


  /**
   * Regular Expression Method for Check Password length greater than minimum
   */
  changePasswordLength(password, min, message) {
    if (password.length < min) {
      // Alert.alert(globals.appName, message)
      return false;
    }
    return true;
  }

  /**
    * Regular Expression Method for Check Password length greater than minimum
    */
  passwordLength2(password, min, message) {
    if (password.length < min) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  // Check both password matches
  matchPasswordPIN(pwd, confrmPwd, message) {
    if (pwd != confrmPwd) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  // Check if password is same
  samePassword(currentpwd, nwpwd, message) {
    if (currentpwd === nwpwd) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  // Check valid pin
  isValidPin(pin, message) {
    if (pin.length < 6) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  // Check valid zip code
  isValidZipCode(pin, message) {
    const regex = /^[a-zA-Z0-9]+$/;
    if (pin !== null && pin.length >= 0) {
      if (regex.test(pin) && pin.length > 6) {
        return true;
      }
      Alert.alert(globals.appName, 'Pin not valid');
    } else {
      Alert.alert(globals.appName, message);
    }
    return false;
  }

  // Check valid miles code
  isValidMiles(radius, message) {
    const regex = /^[0-9]+$/;
    if (radius !== null && radius.length >= 0 && regex.test(radius)) {
      return true;
    }
    Alert.alert(globals.appName, message);

    return false;
  }

  // Check point validation
  pointValidation(point, message) {
    const regex = /^[0-9.]+$/;
    if (point !== null && point.length >= 0 && regex.test(point)) {
      return true;
    }
    Alert.alert(globals.appName, message);

    return false;
  }

  // Check for alphanumeric zip code validation
  alphaNumericZipCodeValidation(value, message) {
    const regex = /^[a-zA-Z0-9]+$/;
    if (value !== null && value.length >= 0 && regex.test(value)) {
      return true;
    }
    Alert.alert(globals.appName, message);

    return false;
  }

  // check for umber validation
  numberValidation(number, message) {
    const regex = /^[0-9]+$/;
    if (number !== null && number.length >= 0 && regex.test(number)) {
      return true;
    }
    Alert.alert(globals.appName, message);

    return false;
  }

  // chekc for point compare validation
  pointCompareValidation(point1, point2, message) {
    if (point1 > point2) {
      Alert.alert(globals.appName, message);
      return false;
    }
    return true;
  }

  RegularExpressionEmail = function (txt) {
    let isValid = false;
    const reg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    if (txt !== '' && txt !== null) {
      if (reg.test(txt) === false) {
        isValid = false;
      } else {
        isValid = true;
      }
    } else {
      isValid = false;
    }
    return isValid;
  };

  /**
   * Regular Expression for Password
   */
  RegularExpressionPassword = function (txt) {
    let isValid = false;
    const str = txt.toString().trim();
    const letter = /[a-zA-Z]/;
    const number = /[0-9]/;
    if (str.length >= 7) {
      const _valid = number.test(str) && letter.test(str);
      if (_valid === false) {
        isValid = false;
      } else {
        isValid = true;
      }
    } else {
      isValid = false;
    }
    return isValid;
  };

  /**
   * Regular Expression for Mobile number
   */
  RegularExpressionMobileNumber = function (txt) {
    let isValid = false;
    if (isNaN(txt) === false && txt !== null) {
      if (txt.length === 10) {
        if (txt.trim() !== '') {
          isValid = true;
        }
      }
    }
    return isValid;
  };
}

module.exports = new Validation();
