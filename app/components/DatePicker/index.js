import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { Calendar, defaultStyle } from '../../libs/react-native-calendars'

const XDate = require('xdate');

type Props = {
  initialRange: React.PropTypes.array.isRequired,
  onSuccess: React.PropTypes.func.isRequired,
  onStartDayPress : React.PropTypes.func.isRequired,
};
export default class DateRangePicker extends Component<Props> {

  state = { isFromDatePicked: false, isToDatePicked: false, markedDates: {} }

  componentDidMount() { this.setupInitialRange() }

  onDayPress = (day) => {    
    if (!this.state.isFromDatePicked || (this.state.isFromDatePicked && this.state.isToDatePicked)) {          
      this.setupStartMarker(day, 'second');      
      this.props.onStartDayPress(day);
    } else if (!this.state.isToDatePicked) {          
      const mFromDate = new XDate(this.state.fromDate);
      const mToDate = new XDate(day.dateString);

      let range;
      if (mFromDate >= mToDate) {
        range = mToDate.diffDays(mFromDate);
      } else {
        range = mFromDate.diffDays(mToDate);
      }
      
      if (range != 0) {
        const markedDates = { ...this.state.markedDates };
        let [mMarkedDates, range] = [,]
        if (mFromDate >= mToDate) {
          // [mMarkedDates, range] = this.setupMarkedDates(day.dateString, this.state.fromDate, markedDates, 'reverse'); //reverse
        } else {
          [mMarkedDates, range] = this.setupMarkedDates(this.state.fromDate, day.dateString, markedDates, ''); //streight
        }
        //let markedDates = { ...this.state.markedDates }
        //let [mMarkedDates, range] = this.setupMarkedDates(this.state.fromDate, day.dateString, markedDates)
        if (range >= 0) {          
          if (mFromDate >= mToDate) {
            // this.setState({ isFromDatePicked: true, isToDatePicked: true, markedDates: mMarkedDates }, () => {
            //   console.log("MARKED DATES reverse::: ", (this.state.markedDates))
            // });
            // this.props.onSuccess(this.state.fromDate, day.dateString);
          } else {
            this.setState({ isFromDatePicked: true, isToDatePicked: true, markedDates: mMarkedDates }, () => {
              console.log("MARKED DATES norml::: ", (this.state.markedDates))
            });            
            
            this.props.onSuccess(this.state.fromDate,day.dateString);
          }

        } else {          
          this.props.onStartDayPress(day);
          this.setupStartMarker(day, '')
        }
      }
    }
  }

  setupStartMarker = (day, second) => {
    
    let markedDates;
    if (second == 'second') {
      markedDates = { [day.dateString]: { startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor, endingDay: true } };
    } else {
      markedDates = { [day.dateString]: { startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor,endingDay: true } };
    }
    this.setState({ isFromDatePicked: true, isToDatePicked: false, fromDate: day.dateString, markedDates: markedDates })
  }

  setupMarkedDates = (fromDate, toDate, markedDates, reverse) => {
    let mFromDate = new XDate(fromDate)
    let mToDate = new XDate(toDate)
    let range = mFromDate.diffDays(mToDate)
    if (range >= 0) {
      if (range == 0) {
        markedDates = { [toDate]: { color: this.props.theme.markColor, textColor: this.props.theme.markTextColor, endingDay: true } }
      } else {
        for (var i = 1; i <= range; i++) {
          let tempDate = mFromDate.addDays(1).toString('yyyy-MM-dd')
          const tempFromDate = mToDate.addDays(-1).toString("yyyy-MM-dd");
          if (i < range) {
            markedDates[tempDate] = { color: this.props.theme.markColor, textColor: this.props.theme.markTextColor }
          } else {
            if (reverse == 'reverse') {
              // markedDates[tempFromDate] = { startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor };
              // markedDates[tempDate] = { endingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor, startingDay: false };
              // markedDates[tempDate] = Object.assign({}, markedDates[tempFromDate], markedDates[tempDate])
            }
            else {
              markedDates[tempFromDate] = { endingDay: false, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor, startingDay: true, };
              markedDates[tempDate] = { endingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor, startingDay: false };
              markedDates[tempDate] = Object.assign({}, markedDates[tempFromDate], markedDates[tempDate])
            }

          }
        }
      }
    }
    return [markedDates, range]
  }

  setupInitialRange = () => {

    if (!this.props.initialRange) return
    let [fromDate, toDate] = this.props.initialRange
    let markedDates;
    
    if (fromDate == toDate) {
      markedDates = { [fromDate]: { startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor, endingDay: true } };
    } else {
      markedDates = { [fromDate]: { startingDay: true, color: this.props.theme.markColor, textColor: this.props.theme.markTextColor } };

    }
    const [mMarkedDates, range] = this.setupMarkedDates(fromDate, toDate, markedDates, '');
    
    this.setState({ markedDates: mMarkedDates, fromDate: fromDate })
  }

  render() {

    return (
      <Calendar {...this.props}
        style={{ width: '100%' }}
        markingType={'period'}
        current={this.state.fromDate}
        markedDates={this.state.markedDates}
        onDayPress={(day) => { this.onDayPress(day) }} />
    )
  }
}

DateRangePicker.defaultProps = {
  theme: { markColor: '#00adf5', markTextColor: '#ffffff' }
};
