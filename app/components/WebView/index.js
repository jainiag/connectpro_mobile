/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import { Text, View,  TouchableOpacity, Image } from 'react-native';
import React from 'react';
import * as images from '../../assets/images/map';
import { WebView } from 'react-native-webview';
import LoginScreem from '../../containers/screens/AuthenticationScreens/loginScreen'
import RegisterScreen from '../../containers/screens/AuthenticationScreens/registerScreen'
import styles from './style';

const TAG = '==:== WebView component : ';
export default class WebViewComponent extends React.Component {

  constructor(props) {
    super(props);
    console.log(TAG, props);

    this.state = {
      url: this.props.url,
    }
  }

  closeWebView(){
    if(this.props.isOpenLogin === true){
      LoginScreem.closeWebView()
    } else {
      RegisterScreen.closeWebView()
    }
  }


  render() {
    const { url } = this.state;
    return (
      <View style={styles.mainViewStyle}>
        <View style={styles.headerViewStyle}>
          <TouchableOpacity
            onPress={() => this.closeWebView()}
            style={styles.backBtnStyle}
          >
            <Image source={images.loginAndRegisterScreen.backIcon}
              resizeMode={"contain"}
              style={styles.imageIconStyle} /></TouchableOpacity>
          {(this.props.isterm === true) ?
            <Text style={styles.titleStyle}>
              {'Terms And Condition'}
            </Text>
            :
            (this.props.isPrivacy === true) ?
            <Text style={styles.titleStyle}>
              {'Privacy Policy'}
            </Text>
            :  
            <Text style={styles.titleStyle}>
              {'Terms And Condition'}
            </Text>
          }
          <View style={styles.bottomViewStyle}>
          </View>
        </View>
        <WebView
          source={{ uri: url }}
        />
      </View>
    )

  }
}

