
/* eslint-disable eqeqeq */
import { StyleSheet } from 'react-native';
import * as globals from '../../utils/globals';
import * as color from '../../assets/styles/color';


module.exports = StyleSheet.create({
  mainViewStyle: {
    flex:1,
  },
  headerViewStyle: {
    flexDirection: "row", 
    alignItems: 'center', 
    paddingVertical: globals.screenHeight * 0.01, 
    marginTop: globals.screenHeight * 0.012,
  },
  backBtnStyle: {
    marginLeft: globals.screenWidth * 0.04, 
    alignItems: 'center', 
    paddingVertical: globals.screenHeight * 0.02
  },
  imageIconStyle: {
    height: globals.screenHeight * 0.035, 
    width: globals.screenHeight * 0.035, 
    tintColor: color.warmBlue, 
    alignSelf: 'center',
    marginTop: globals.screenHeight * 0.01 
  },
  titleStyle: {
    marginLeft: globals.screenWidth * 0.07, 
    fontSize: globals.font_20, 
    fontWeight: '500', 
    alignSelf: 'center', 
    marginTop: globals.screenHeight * 0.01
  },
  bottomViewStyle: {
    width: globals.screenWidth, 
    position: 'absolute', 
    bottom: 2, 
    height: 1, 
    backgroundColor: color.gray, 
    opacity: 0.5, 
  },
});
