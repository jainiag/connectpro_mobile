import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../utils/globals';
import * as colors from '../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

var iPad = DeviceInfo.getModel()

module.exports = StyleSheet.create({
  Container: {
    backgroundColor: 'transparent',
    height: globals.screenHeight * 0.08,
    shadowColor: colors.black,
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    position: 'relative',
    elevation: 2,
  },
  titleViewStyle: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    marginRight: 60,
  },
  titleStyle: {
    fontSize: globals.font_17,
    color: colors.darkgrey,
  },
  arrobackTouchStyle: {
    flexDirection: 'row',
    marginLeft: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 20 : 30 : 15,
    
    paddingTop:(globals.iPhoneX) ? 42 : (Platform.OS == 'ios') ? 30 : 25,
  },
  headerBackTextStyle: {
    marginLeft: 5,
    fontSize: globals.font_17,
    color: colors.darkgrey,
    marginBottom: 3,
  },
  backIconStyle: {
    height: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.getHorizontalBaseValue(40) : globals.getHorizontalBaseValue(25),
    width: ((iPad.indexOf('iPad')!=-1) || (DeviceInfo.isTablet())) ? globals.getHorizontalBaseValue(40) : globals.getHorizontalBaseValue(25),
    tintColor: colors.white,
  },
});
