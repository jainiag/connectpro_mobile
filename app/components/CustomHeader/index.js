/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import * as images from '../../assets/images/map';
import styles from './style';

export default class Header extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.Container}>
        <TouchableOpacity onPress={this.props.onPress} style={styles.arrobackTouchStyle}>
          <Image style={styles.backIconStyle} source={images.loginAndRegisterScreen.backIcon} />
        </TouchableOpacity>
      </View>
    );
  }
}
