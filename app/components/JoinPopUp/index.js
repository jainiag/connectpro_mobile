/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import { Text, View, TouchableOpacity, Image, Linking, Alert } from 'react-native';
import React from 'react';
import styles from './style';
import Modal from "react-native-modal";
import * as globals from '../../utils/globals';
import * as images from '../../assets/images/map';
import * as colors from '../../assets/styles/color';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/AntDesign';
import DeviceInfo from 'react-native-device-info';
import { API } from '../../utils/api';
import AboutCommunity from '../../containers/screens/Dashboard/Communities/Community_Dashboard/AboutCommunity/index';
import GlobalCommunities from '../../containers/screens/GlobalCommunities/index'
import CommunityDashboard from '../../containers/screens/Dashboard/Communities/Community_Dashboard/communityDashboard';

const iPad = DeviceInfo.getModel();
let visible;
const TAG = '==:== JoinPopUp : ';
export default class JoinPopUp extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      Communitystatus:this.props.Communitystatus,
      isCheck: false,
      CategoryName: this.props.CommunityCategoryName,
      communityID: this.props.communityID,
      isOpen: this.props.isOpen,
      TermsAndConditions: this.props.TermsAndConditions,
      globalCommunityScreen: this.props.isFrom,
      isVisible: false,
      navigationProps: this.props.navigationProps,
    }
  }

  componentDidMount() {
    this.setState({ navigationProps: this.props.navigationProps, TermsAndConditions: this.props.TermsAndConditions, isOpen: this.props.isOpen, communityID: this.props.communityID, isCheck: false, globalCommunityScreen: this.props.isFrom })
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.isOpen) {
      this.setState({ isOpen: true, isCheck: false });
    }
  }

  /**
 * Method of setModalVisible false
 * 
 */
  setModalVisible(visible) {
    this.setState({ isOpen: false }, () => {
      if (this.state.globalCommunityScreen == "globalCommunityScreen") {
        GlobalCommunities.joinPopUpClose()
      }
      else if (this.state.globalCommunityScreen == "CommunityDashboard") {
        CommunityDashboard.joinPopUpClose()

      }
      else if (this.state.globalCommunityScreen == "AboutCommunity") {
        AboutCommunity.joinPopUpClose()
        CommunityDashboard.joinPopUpClose()
      }
    })

  }

  /**
* Method of API on yesclick
* 
*/
  setModalVisibleAPI(visible) {
    this.setState({ isOpen: false })
    this.makeJoinApiCall()
  }

  /**
* Method of navigateToWeb
* 
*/
  navigateToWeb() {
    const URL = globals.mainUrl + "community/termsandconditions/" + this.state.communityID;
    Linking.canOpenURL(URL).then(supported => {
      if (supported) {
        console.log("supported" + URL);
        Linking.openURL(URL);
      } else {
        console.log("not supported " + URL);
        Linking.openURL(URL)
      }
    });
  }


  /**
 * Method of Checked accept btn
 * 
 */
  onChecked() {
    this.setState({ isCheck: true })
    if (this.state.isCheck == true) {
      this.setState({ isCheck: false, isVisible: false })
    }
    else {
      this.setState({ isCheck: true, isVisible: false })
    }
  }

  callbackmethod() {
    if (this.state.globalCommunityScreen == "globalCommunityScreen") {
      GlobalCommunities.clearStatesJoinPopup()

    }
    else if (this.state.globalCommunityScreen == "CommunityDashboard") {
      CommunityDashboard.clearStatesJoinPopup()

    }
    else if (this.state.globalCommunityScreen == "AboutCommunity") {
      AboutCommunity.clearStatesJoinPopup()
      CommunityDashboard.clearStatesJoinPopup()
    }
  }

  /**
     * API call of Join communityInfo
     */
  makeJoinApiCall() {
    const data = {
      communityIds: [
        this.state.communityID
      ],
      statusId: 1,
      userId: globals.userID,
      rejectReason: "",
      AccessToken: JSON.parse(globals.tokenValue)
    }
    if (globals.isInternetConnected == true) {
      API.saveUserStatusInCommunities(this.saveUserStatusInCommunitiesResponseData, data, true);
    } else {
      Alert.alert(globals.appName, globals.MESSAGE.LOGINSTACK.AUTH_CHECK_INTERNET);
    }
  }

  /**
      * Response callback of saveUserStatusInCommunitiesResponseData
      */
  saveUserStatusInCommunitiesResponseData = {
    success: response => {
      console.log(
        TAG,
        'saveUserStatusInCommunitiesResponseData -> success : ',
        JSON.stringify(response)
      );
      if (response.StatusCode == 200 && response.Result == true) {
        this.callbackmethod()
        
        if (this.state.Communitystatus == 'public' || this.state.Communitystatus == 'Public') {
          Alert.alert(
            globals.appName,
            "You have successfully joined the community",
            [{ text: 'OK' }],
            { cancelable: false }
          );
        } else {
          Alert.alert(
            globals.appName,
            "Your request to join this community is waiting for approval.",
            [{ text: 'OK' }],
            { cancelable: false }
          );
        }
      }
      else {
        Alert.alert(globals.appName, response.Message)
      }

    },
    error: err => {
      console.log(
        TAG,
        'saveUserStatusInCommunitiesResponseData -> ERROR : ',
        JSON.stringify(err.message)
      );
      Alert.alert(
        globals.appName,
        err.Message
      );
    },
    complete: () => {
      //this.setModalVisible(false)
    },
  };


  /**
        * View for alert when not press on check buton
        */
  renderAlertView() {
    this.setState({ isVisible: true })
    return (
      (this.state.isVisible == true && this.state.isCheck == false) ?
        <View style={styles.notcheckbtn}>
          <Image style={styles.notcheckbtnImg} source={images.EventDashboard.aboutIcon} />
          <Text numberOfLines={2} style={styles.notcheckbtnText}>{"Please accept the Terms & Conditions to proceed."}</Text>
        </View>
        :
        <View />
    )

  }



  render() {
    const { CategoryName, isOpen, isCheck, isVisible } = this.state;
    return (
      <View>

        <Modal
          animationType="slide"
          transparent={true}
          visible={isOpen}
          onRequestClose={() => { this.setModalVisible() }}
        >
          <View style={[styles.modalMainView, {
            height: (this.state.TermsAndConditions == null || this.state.TermsAndConditions == "") ? globals.screenHeight * 0.3 :
              (globals.iPhoneX) ? globals.screenHeight * 0.40 : globals.screenHeight * 0.45,
          }]}>
            <View style={styles.modalInnerMainView}>
              <View style={styles.popupinnerViewStyle}>
                <Text style={styles.popupheaderStyle}>{"Do you want to join " + CategoryName + " " + "?"}</Text>
                <Text style={styles.popupsubTextStyle}>{"Note : if the community is private, then the request needs to be approved by community Admin."}</Text>
              </View>
            </View>
            {
              (this.state.TermsAndConditions == null || this.state.TermsAndConditions == "") ?
                <View /> :
                <View style={[styles.termconditionView, {
                  marginBottom: (this.state.isCheck == true) ?
                    (globals.iPhoneX) ? globals.screenHeight * 0.075 : globals.screenHeight * 0.075 :
                    (this.state.isVisible == true) ? (globals.iPhoneX) ? globals.screenHeight * 0.01 :
                      globals.screenHeight * 0.018 : globals.screenHeight * 0.075
                }]}>
                  <TouchableOpacity onPress={() => this.onChecked()} style={styles.squareView}>
                    {(isCheck === true) ? (
                      <Icon name="check" color={colors.black} size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? 20 : globals.screenWidth * 0.04} />
                    ) : null}
                  </TouchableOpacity>
                  <Text style={styles.scceptText}>{"  " + "I accept"} </Text>

                  <TouchableOpacity onPress={() => this.navigateToWeb()}>
                    <Text style={styles.linkText}>{" " + "Terms and Conditions"}</Text>
                  </TouchableOpacity>

                </View>

            }

            {
              (isCheck === false && isVisible == true) ?
                <View style={styles.notcheckbtn}>
                  <Image style={styles.notcheckbtnImg} source={images.EventDashboard.aboutIcon} />
                  <Text numberOfLines={2} style={styles.notcheckbtnText}>{"Please accept the Terms & Conditions to proceed."}</Text>
                </View>
                :
                <View />
            }



            <View style={[styles.btnViewStyle, {
              marginVertical: (this.state.TermsAndConditions == null || this.state.TermsAndConditions == "") ?
                globals.screenHeight * 0.045 : globals.screenHeight * 0.02,
            }]}>
              <TouchableOpacity onPress={() => this.setModalVisible(false)}>
                <View style={[styles.innerbtnViewStyles, { backgroundColor: colors.orange, borderColor: colors.orange }]}>
                  <Entypo
                    size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.035 : globals.screenHeight * 0.025}
                    name={"cross"} color={colors.white} style={[styles.vectorStyle, { marginTop: globals.screenHeight * 0.001 }]} />
                  <Text style={[styles.btnTextsStyle, {}]}>{"No"}
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => (isCheck === true || this.state.TermsAndConditions == null || this.state.TermsAndConditions == "") ?
                this.setModalVisibleAPI(false) : this.renderAlertView()}>
                <View style={[styles.innerbtnViewStyles, { marginLeft: globals.screenWidth * 0.03, backgroundColor: colors.green, borderColor: colors.green }]}>
                  <Entypo
                    size={(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ? globals.screenHeight * 0.03 : globals.screenHeight * 0.02}
                    name={"check"} color={colors.white} style={styles.vectorStyle} />
                  <Text style={[styles.btnTextsStyle,]}>{"Yes, Proceed"}</Text>
                </View>
              </TouchableOpacity>
            </View>


          </View>
        </Modal>
      </View>
    )

  }
}

