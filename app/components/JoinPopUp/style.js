/* eslint-disable eqeqeq */
import { StyleSheet, Platform } from 'react-native';
import * as globals from '../../utils/globals';
import * as colors from '../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({

  headerViewStyle: {
    backgroundColor: colors.white,
    height: globals.screenHeight * 0.08,
    width: globals.screenWidth,
    flexDirection: 'row',
    marginTop: 3,
    paddingHorizontal: globals.screenHeight * 0.01,
    paddingVertical: globals.screenHeight * 0.02,
    justifyContent: 'space-between',
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
  },
  modalMainView: {
   
    borderColor: colors.gray,
    borderWidth: 4,
    width: globals.screenWidth * 0.9,
    backgroundColor: colors.white,
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: globals.screenHeight * 0.05
  },
  modalInnerMainView: {
    backgroundColor: colors.white,
    //  margin: globals.screenWidth * 0.035,
    width: globals.screenWidth * 0.8,
    // borderRadius: 4,
    flex: 1,

  },
  modalSecondView: {
    height: (globals.iPhoneX) ? globals.screenHeight * 0.37 : globals.screenHeight * 0.5,
    justifyContent: 'center',
    alignItems: 'center',
    width: globals.screenWidth * 0.9,
    marginHorizontal: (globals.screenWidth * 0.05),
    backgroundColor: colors.gray,
    marginTop: globals.screenHeight * 0.6
  },
  modalAlreadyRegisteredView: {
    flexDirection: 'row', padding: globals.screenHeight * 0.01
  },
  modalResendTextView: {
    flex: 1, marginRight: 10
  },
  modalCancelImg: {
    height: globals.screenHeight * 0.03, width: globals.screenHeight * 0.03, tintColor: colors.gray
  },
  modalHorizontalLine: {
    height: 2, backgroundColor: colors.proUnderline, width: '100%', marginTop: (globals.screenHeight * 0.03), marginBottom: (globals.screenHeight * 0.03)
  },
  modalTextInputView: {
    borderColor: colors.gray, borderWidth: 2, marginHorizontal: 10
  },
  modalSubmitView: {
    justifyContent: 'center', backgroundColor: colors.listSelectColor, width: globals.screenWidth * 0.22, alignItems: 'center', height: globals.screenHeight * 0.05, right: 0, position: 'absolute', bottom: (globals.screenHeight * 0.03), marginRight: globals.screenHeight * 0.01, borderRadius: 3
  },
  modalSubmitText: {
    color: colors.white, fontSize: globals.font_13, fontWeight: '600'
  },
  popupinnerViewStyle: {
    flex: 1, marginTop: globals.screenHeight * 0.02,
    marginHorizontal: globals.screenHeight * 0.015,
    // marginBottom: globals.screenHeight * 0.02,
  },
  popupheaderStyle: {
    fontSize: globals.font_16,
    color: colors.Gray,
    fontWeight: "500",
  },
  popupsubTextStyle: {
    marginTop: globals.screenHeight * 0.01,
    fontSize: globals.font_12,
    color: colors.Gray
  },
  termconditionView: { 
    marginHorizontal: globals.screenWidth * 0.02,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'center'
  },
  squareView: {
    // marginTop: globals.screenHeight * 0.0065,
    height:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    width:
      iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()
        ? globals.screenWidth * 0.028
        : globals.screenWidth * 0.045,
    borderColor: colors.gray,
    borderWidth: 1,
  },
  scceptText: {
    fontSize: globals.font_14,
    color: colors.black
  },
  linkText: {
    fontSize: globals.font_14,
    color: colors.warmBlue,
    textDecorationLine: "underline",
    fontWeight: "500",

  },
  btnViewStyle: {
   
    marginTop:globals.screenHeight * 0.01,
    marginBottom:globals.screenHeight * 0.035,
    marginHorizontal: globals.screenWidth * 0.02,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'
  },
  notcheckbtn: {
    width: globals.screenWidth * 0.9,
    backgroundColor: colors.grayBG,
    flexDirection: 'row',
    paddingTop: globals.screenHeight * 0.018,
    marginBottom:globals.screenHeight * 0.015,
  },
  notcheckbtnImg: {
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.04 : globals.screenHeight * 0.028,
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenHeight * 0.04 : globals.screenHeight * 0.028,
    tintColor: colors.redColor,
   alignSelf:'center',
   marginBottom:globals.screenHeight * 0.012,
    marginLeft: globals.screenWidth * 0.035
  },
  notcheckbtnText: {
    width:globals.screenWidth * 0.75,
    fontSize: globals.font_14,
    color: colors.Gray,
    alignSelf:'center',
    marginHorizontal: globals.screenWidth * 0.02,
    paddingBottom:globals.screenHeight * 0.015
    // marginRight: globals.screenWidth * 0.02
  },
  innerbtnViewStyles: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderWidth: 0.2,
    borderRadius: 3,
    // marginHorizontal:globals.screenWidth * 0.03
  },
  closeimgStyle: {
    height: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.04,
    tintColor: colors.white,
    marginLeft: globals.screenWidth * 0.02
  },
  vectorStyle: {
    marginLeft: globals.screenWidth * 0.01,
    alignItems: 'center'
  },
  btnTextsStyle: {
    //  paddingLeft:globals.screenWidth * 0.025,
    paddingRight: globals.screenWidth * 0.025,
    paddingVertical: globals.screenWidth * 0.015,
    fontSize: globals.font_16,
    color: colors.white,
    fontWeight: "600",
    textAlign: 'center'
  },

});
