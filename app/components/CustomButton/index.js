/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import { Text, View, ActivityIndicator } from 'react-native';
import React from 'react';
import styles from './styles';

function CustomButton(props) {
 
  return (
    <View style={[styles.buttonViewStyle, { backgroundColor: props.backgroundColor }]}>
      {props.loadingStatus === false ? (
        <Text style={[styles.btnTextStyle, { color: props.color }]}>{props.text}</Text>
      ) : (
        <ActivityIndicator
          size="large"
          color={props.color}
        />
      )}
    </View>
  );
}

export default CustomButton;
