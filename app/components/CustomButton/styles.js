/* eslint-disable eqeqeq */
import { StyleSheet,Platform } from 'react-native';
import * as globals from '../../utils/globals';
import * as colors from '../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';

const iPad = DeviceInfo.getModel();

module.exports = StyleSheet.create({
  buttonViewStyle: {
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: globals.screenWidth * 0.019,
    paddingHorizontal: globals.screenWidth * 0.04,
    width: globals.screenWidth * 0.85,
    height: globals.screenHeight * 0.0785,
    borderRadius: globals.iPhoneX ? globals.screenHeight * 0.03 : globals.screenHeight * 0.03,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnTextStyle: {
    alignSelf: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (Platform.OS == 'ios') ? 28 : 32 : globals.font_20,
    fontWeight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? (Platform.OS === 'android') ? 'bold' : '700' : '700',
  },
  activityindicatorStyle: {},
});
