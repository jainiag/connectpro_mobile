/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
import { Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import * as globals from '../../utils/globals';
import globalStyles from '../../assets/styles/globleStyles';
import * as colors from '../../assets/styles/color';
import CustomButton from '../CustomButton/index';

const TAG = '==:== Nointernet component : ';
export default class Nointernet extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isCheck: false,
      loading:this.props.loading
    }
  }


  render() {
      const {loading} = this.state;
    return (
        <View style={globalStyles.serverErrViewContainer}>
        <Text style={globalStyles.serverTextStyle}>
            {globals.ERROR_MESSAGE.INTERNET_NOT_AVAILABLE}
        </Text>
        <TouchableOpacity style={globalStyles.serverButtonStyle} onPress={this.props.onPress}>
            <CustomButton
                text={globals.BTNTEXT.LOGINSCREEN.CUSTOM_BTN_TEXT}
                backgroundColor={colors.bgColor}
                color={colors.white}
                loadingStatus={loading}
            />
        </TouchableOpacity>
    </View>
    )

  }
}

