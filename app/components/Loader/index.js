/* eslint-disable no-use-before-define */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/prop-types */
/* eslint-disable no-console */
import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';
import * as globals from '../../utils/globals';
import * as colors from '../../assets/styles/color';

class Loader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: this.props.loader,
    };
  }

  /**
   * Method for receive props
   * @param {*} newProps
   */
  componentWillReceiveProps(newProps) {
    this.setState(
      {
        loading: newProps.loading,
      }
    );
  }

  render() {
    if (this.state.loading) {
      return (
        <View style={styles.loaderWrapper} pointerEvents="box-only">
          <View style={[[styles.loaderInner]]}>
            <ActivityIndicator size="large" color={colors.bgColor} />
          </View>
        </View>
      );
    }
    return <View />;
  }
}

const styles = StyleSheet.create({
  loaderWrapper: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 999,
    backgroundColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderInner: {
    alignItems: 'center',
    justifyContent: 'center',
    // height: 100,
    // width: 100,
    // borderRadius: 10,
    // backgroundColor:'rgba(0,0,0,0.6)'
  },
  lottieView: {
    height: globals.screenWidth * 0.3,
    width: globals.screenWidth * 0.3,
  },
});

export default Loader;
