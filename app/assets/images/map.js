/* eslint-disable global-require */
/* eslint-disable import/prefer-default-export */
export const loginAndRegisterScreen = {
  logo: require('./logoIcon/logo.png'),
  blueBackground: require('./blueBackground/bluebg.png'),
  fbIcon: require('./fbIcon/fb.png'),
  infoIcon: require('./infoIcon/info.png'),
  password: require('./passwordIcon/password.png'),
  userIcon: require('./userIcon/user.png'),
  backIcon: require('./backIcon/back.png'),
  alertIcon: require('./alertIcon/alert.png'),
  closeBtn: require('./closeBtn/close-blue.png'),
};

export const drawerMenu = {
  menuClose: require('./menuCloseIcon/menu-close.png'),
};

export const headerIcon = {
  searchIcon: require('./seacrhIcon/search.png'),
};

export const InnerBg = {
  InnerBg: require('./innerbg/innerbg.png'),
};

export const Authorised = {
  AuthorisedLogo: require('./authorisednet/authorize.png'),
};

export const Paypal = {
  PaypalLogo: require('./paypallogo/paypal.png'),
};

export const colse = {
  closebtn: require('./closeButton/white-cancel.png'),
};

export const done = {
  donebtn: require('./donebutton/done.png'),
};
export const EventDashboard = {
  aboutIcon: require('./aboutIcon/about.png'),
  agenda: require('./agenda/agenda.png'),
  shareBtn: require('./shareBtn/share-round.png'),
  speakers: require('./speakers/speakers.png'),
  attendees: require('./attendees/attendees.png'),
  calendarBlue: require('./calendarBlue/calendar-blue.png'),
  location: require('./location/location.png'),
  locationBlue: require('./locationBlue/location-blue.png'),
  shareBlack: require('./ShareBlack/share-round-white.png'),
  sponsors: require('./sponsors/sponsors.png'),
  resources: require('./Resources/resources.png'),
  registration: require('./registration/registration.png'),
  chatIcon: require('./chatIcon/chat-icon.png'),
  notificationWhite: require('./notificationWhite/notification-white.png'),
  qrScanner: require('./qrScanner/qrscanner.png'),
  upArrow: require('./upArrow/uparrow.png'),
  downArrow: require('./downArrow/downarrow.png'),
  whitepdf: require('./whitepdf/pdf-white.png'),
  eventRegistration: {
    resendEventMail: {
      close: require('./cancelBtn/cancel.png'),
    }
  },
  imagePlaceHolder: require('./imagePlaceHolder/image-placeholder.png'),
};

export const messages = {
  chatAct: require('./chatAct/chat-act.png'),
};

export const calenderImage = {
  calenderIcon: require('./calenderImage/calendar-white.png'),
};

export const navigationIcon = {
  navIcon: require('./navigationIcon/nav.png'),
};

export const MemberDashboard = {
  Communities: require('./communities/communities.png'),
  Groups: require('./groupBlue/group-blue.png'),
  Events: require('./eventsBlue/events-blue.png'),
  Connections: require('./connectionBlue/connections-blue.png'),
  Endorsements: require('./endorsmentBlue/endorsements.png'),
  Messages: require('./messagesBlue/messages-blue.png'),
  Matches: require('./matchesBlue/matches-blue.png'),
  Manage_Emails: require('./emailBlue/email-blue.png'),
};

export const EventRegistration = {
  calendarGrey: require('./calendarGrey/calendar.png'),
  successIcon: require('./success/success.png'),
  deleteIcon: require('./deleteIcon/delete.png'),
  editIcon: require('./editIcon/edit.png'),
  addIcon: require('./addIcon/add.png'),
};

export const MyProfile = {
  bag: require('./bag/bag.png'),
  blueAdd: require('./blueAdd/blueadd.png'),
  blueEdit: require('./blueEdit/blueedit.png'),
  buildings: require('./buildings/buildings.png'),
  closeRed: require('./closeRed/close.png'),
  emailBlack: require('./emailBlack/email.png'),
  googlePlus: require('./googlePlus/googleplus.png'),
  linkedIn: require('./linkedIn/linkedin.png'),
  phone: require('./phone/phone.png'),
  placeHolder: require('./placeHolder/placeholder.png'),
  play: require('./play/play.png'),
  twitter: require('./twitter/twitter.png'),
  fbBlue: require('./fbBlue/fb.png'),
  setting: require('./setting/setting.png'),
  blueDelete: require('./bluedelete/bluedelete.png'),
  forwardArrow: require('./myProfileImages/forward/forward.png'),
}

export const Communities = {
  lock: require('./community/communityListing/lock/lock.png'),
  waitingApproval: require('./community/communityListing/waitingApproval/outoftime.png'),
  waitingReview: require('./community/communityListing/waitingReview/orange-outoftime.png'),
  acceptCommunity: require('./community/communityListing/accept/accept.png'),
  activeCommunity: require('./community/communityListing/active/active.png'),
  cancelApproval: require('./community/communityListing/CancelApproval/cancel.png'),
  LeaveCommunity: require('./community/communityListing/leave/leave.png'),
  member: require('./community/communityListing/member/use.png'),
  admins: require('./community/admins/admin.png'),
  directory: require('./community/directory/directory.png'),
  dicussion: require('./community/discussion/discussion.png'),
  event: require('./community/events/events.png'),
  memberss: require('./community/members/members.png'),
  setting: require('./community/settings/setting.png'),
  globaletting: require('./community/globalSetting/settings.png'),
  connectedAttach: require('./attachment/attachment.png'),
  joinPlus: require('./community/communitygroup/joinPlus/Iconfeather-plus-circle.png'),
  joinMinus: require('./community/communitygroup/cancleMinus/Iconfeather-minus-circle.png'),
}

export const memberDirectory = {
  bigCall:require('../images/community/MemberDirectory/bigcall.png'),
  bigChat:require('../images/community/MemberDirectory/bigchat.png'),
  bigemail:require('../images/community/MemberDirectory/bigemail.png'),
  bigshare:require('../images/community/MemberDirectory/bigshare.png'),
  cancel:require('../images/community/MemberDirectory/cancel.png'),
  more:require('../images/community/MemberDirectory/more.png'),
  connected:require('../images/community/MemberDirectory/share.png'),
  time:require('../images/community/MemberDirectory/time.png'),
}

export const About_Community = {
  shareIcon: require('./community/shareIcon/share.png'),
  innerFbicon: require('./community/innerFbIcon/inner-fb.png'),
  innerGooglePlus: require('./community/innerGooglePlus/inner-google.png'),
  innerlinkedIn: require('./community/innerLinkedin/inner-linkdin.png'),
  innerTwitter: require('./community/innerTwitter/inner-twitter.png'),
};

export const Discussion = {
  chatBtn: require("./community/discussionImages/chatButton/chat-btn.png"),
  smile: require("./community/discussionImages/smile/smile.png"),
  blueAttach: require("./community/discussionImages/blueAttach/attach.png"),
  blueGallery: require("./community/discussionImages/blueGallery/gallery.png"),
  bluePdf: require("./community/discussionImages/bluePdf/blue-pdf.png"),
  pdf: require("./community/discussionImages/pdf/pdf.png"),
  like: require("./community/discussionImages/like/like.png"),
  comment: require("./community/discussionImages/comment/comment.png"),
  send: require("./community/discussionImages/send/send-btn.png"),
  filledLike: require("./community/discussionImages/filledLike/like.png"),
  attchment: require("./attch-white/attch-white.png"),
}

export const Endorsement = {
  clock: require("./clockIcon/clock.png"),
}

export const Group = {
  GroupBG: require("./group/groupBG/group-bg.png"),
}

export const ManageEmails = {
  Attch_white: require("./attch-white/attch-white.png"),
}

export const Messages = {
  leftArr: require("./chat/left/left.png"),
  rightArr: require("./chat/right/right.png")
}
