import { StyleSheet, Platform, Dimensions } from 'react-native';
import * as colors from './color';
import * as globals from '../../utils/globals';
import DeviceInfo from 'react-native-device-info';

var iPad = DeviceInfo.getModel()

export const styles = StyleSheet.create({
  mainSafeAreaView: {
    flex: 1,
    backgroundColor: colors.HeaderColor,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerBack: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerLabel: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1,
  },
  headerLabelStyle: {
    color: colors.white,
    fontSize: 20,
    marginLeft: 15,
  },
  RightIcon: {
    tintColor: 'white',
    marginRight: 15,
  },
  headerBacktitle: {
    marginLeft: 25,
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 28 : (DeviceInfo.getModel() === 'iPhone X') ? 20 : 17,
    fontWeight: '500',
    marginTop: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 8 : (globals.iPhoneX) ? 3 : 7,
  },
  reg_backButton: {
  },
  headerContainer2: {
    flexDirection: 'row',
    alignItems: 'center',
 
    height: 150,
    width: '100%',
    flex: 1,
  },
  homeScreenHeaderStyle: {
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 70 : 80 : 45,
    width: '100%',
    alignItems: 'center',
  },
  headerBacktitle2: {
    marginLeft: 25,
    fontSize: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 28 : 32 : 23,
    fontWeight: '400',
    marginTop: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? (Platform.OS == 'ios') ? 8 : 12 : (globals.iPhoneX) ? 3 : (DeviceInfo.getModel() == 'iPhone X') ? 1 : (DeviceInfo.getModel() === ' iPhone 6s Plus') ? 2 : 7,
    paddingBottom: (DeviceInfo.getModel() == 'iPhone X') ? 0 : 4,
  },
  headerContainerforNewBackButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 175 : 150,
    width: '100%',
    flex: 1,
  },
  newbackButton: {
    flexDirection: 'row',
    marginLeft: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 20 : 10,
    marginTop: ((iPad.indexOf('iPad') != -1) || (DeviceInfo.isTablet())) ? 20 : (DeviceInfo.getModel() === ' iPhone 6s Plus') ? 15 : 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ConnectPropheaderStyle: {
    borderBottomWidth: 0,
    elevation: 0,
    shadowColor: "transparent",
  },
  ConnectProheaderTitleStyle: {
    fontSize: (Platform.OS == "ios") ? Dimensions.get("window").width * 0.0500 : Dimensions.get("window").width * 0.0450,
    color: colors.black,
    textAlign: "left",
    flex: 2,
    fontWeight: "normal",
  },
  ipadViewStyle:{
    flexDirection: "row",
    alignItems: 'center',
    height: 100,
    width: globals.screenWidth,
    paddingVertical: globals.screenHeight * 0.01
  },
  ipadtouchableStyle:{
    marginLeft: globals.screenWidth * 0.03,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.02,
    paddingVertical: globals.screenHeight * 0.02,
  },
  ipadImageStyle:{
    height: globals.screenHeight * 0.04,
    width: globals.screenHeight * 0.04,
    tintColor: colors.warmBlue,
  },
  ipadTextstyle:{
    marginLeft: globals.screenWidth * 0.05,
    alignItems: 'center',
    marginTop: globals.screenHeight * 0.02,
    fontWeight: (DeviceInfo.isTablet()) ? 'bold' : null,
    fontSize: 27,
  },
  tabLineViewStyle:{
    width: globals.screenWidth,
    position: 'absolute',
    height: 1,
    bottom: 2,
    backgroundColor: colors.gray,
    opacity: 0.5,
  },
  deviceViewStyle:{
    flexDirection: "row",
    alignItems: 'center',
    paddingVertical: globals.screenHeight * 0.01
  },
  devicetouchableStyle:{
    marginLeft: globals.screenWidth * 0.04,
    alignItems: 'center',
    paddingVertical: globals.screenHeight * 0.02,
  },
  deviceImageStyle:{
    height: globals.screenHeight * 0.04,
    width: globals.screenHeight * 0.04,
    tintColor: colors.warmBlue,
    alignSelf: 'center'
  },
  deviceTextStyle:{
    marginLeft: globals.screenWidth * 0.07,
     fontSize: globals.font_20, 
     fontWeight: '600', 
     alignSelf:'center',
  },
  deviceLineViewStyle:{
    width:globals.screenWidth,
    position: 'absolute',
    bottom: 2,
    height: 1,
    backgroundColor: colors.gray,
    opacity: 0.5,
    // marginHorizontal: screenWidth * 0.0468,
  },
  serverErrViewContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  serverTextStyle: {
    textAlign: 'center',
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 30 : globals.font_18,
    marginBottom: 15,
  },
  serverButtonStyle: {
    marginTop: 15,
  },
  nodataStyle:{
    alignItems:'center',
    justifyContent:'center',
    flex:1,
  },
  nodataTextStyle:{
    fontSize : globals.font_15,
    marginHorizontal:globals.screenWidth * 0.02,
    textAlign:'center'
  },
  //ActivityIndicator footer for pagination

  loaderWrapper: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 999,
    backgroundColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderInner: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  

});

export default styles;
