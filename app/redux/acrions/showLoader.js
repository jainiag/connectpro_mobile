export const GET_SHOWLOADER = 'GET_SHOWLOADER';

export const showLoader = () => ({
  type: GET_SHOWLOADER,
  loader: true,
});


export const hideLoader = () => ({
  type: GET_SHOWLOADER,
  loader: false,
});