const defaultState = {
    loader: false,
  };
  
  export default function loaderRed(state = defaultState, action) {
    console.log("in reducer : " + JSON.stringify(action));
    switch (action.type) {
      case "GET_SHOWLOADER":
        return Object.assign({}, state, {
          loader: action.loader,
        });
  
      default:
        return state;
    }
  }