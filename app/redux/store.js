import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import loaderRed from "./reducer/loaderRed";


const appReducer = combineReducers({
  loaderRed: loaderRed,
});

const middleware = applyMiddleware(thunk);

const store = createStore(
  appReducer,
  compose(middleware, window.devToolsExtension ? window.devToolsExtension() : f => f),
);

export default store;
