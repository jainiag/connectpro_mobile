import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableHighlight, TouchableOpacity, StyleSheet, Image, Text, ScrollView } from 'react-native';
import Collapsible from './Collapsible';
import { ViewPropTypes } from './config';
import * as globals from '../../utils/globals';
import * as colors from '../../assets/styles/color';
import DeviceInfo from 'react-native-device-info';



const iPad = DeviceInfo.getModel();

const COLLAPSIBLE_PROPS = Object.keys(Collapsible.propTypes);
const VIEW_PROPS = Object.keys(ViewPropTypes);

export default class Accordion extends Component {
  static propTypes = {
    sections: PropTypes.array.isRequired,
    renderHeader: PropTypes.func.isRequired,
    renderContent: PropTypes.func.isRequired,
    renderFooter: PropTypes.func,
    renderSectionTitle: PropTypes.func,
    activeSections: PropTypes.arrayOf(PropTypes.number).isRequired,
    onChange: PropTypes.func.isRequired,
    align: PropTypes.oneOf(['top', 'center', 'bottom']),
    duration: PropTypes.number,
    easing: PropTypes.string,
    underlayColor: PropTypes.string,
    touchableComponent: PropTypes.func,
    touchableProps: PropTypes.object,
    disabled: PropTypes.bool,
    expandFromBottom: PropTypes.bool,
    expandMultiple: PropTypes.bool,
    onAnimationEnd: PropTypes.func,
    sectionContainerStyle: ViewPropTypes.style,
    containerStyle: ViewPropTypes.style,
  };

  static defaultProps = {
    underlayColor: 'black',
    disabled: false,
    expandFromBottom: false,
    expandMultiple: false,
    touchableComponent: TouchableOpacity,
    renderSectionTitle: () => null,
    onAnimationEnd: () => null,
    sectionContainerStyle: {},
  };

  _toggleSection(section) {
    if (!this.props.disabled) {
      const { activeSections, expandMultiple, onChange } = this.props;

      let updatedSections = [];

      if (activeSections.includes(section)) {
        updatedSections = activeSections.filter(a => a !== section);
      } else if (expandMultiple) {
        updatedSections = [...activeSections, section];
      } else {
        updatedSections = [section];
      }

      onChange && onChange(updatedSections);
    }
  }

  render() {
    let viewProps = {};
    let collapsibleProps = {};

    Object.keys(this.props).forEach(key => {
      if (COLLAPSIBLE_PROPS.includes(key)) {
        collapsibleProps[key] = this.props[key];
      } else if (VIEW_PROPS.includes(key)) {
        viewProps[key] = this.props[key];
      }
    });

    const {
      activeSections,
      containerStyle,
      sectionContainerStyle,
      expandFromBottom,
      sections,
      underlayColor,
      touchableProps,
      touchableComponent: Touchable,
      onAnimationEnd,
      renderContent,
      renderHeader,
      renderFooter,
      renderSectionTitle,
    } = this.props;

    const renderCollapsible = (section, key) => (
      <Collapsible
        collapsed={!activeSections.includes(key)}
        {...collapsibleProps}
        onAnimationEnd={() => onAnimationEnd(section, key)}
      >
        {renderContent(section, key, activeSections.includes(key), sections)}
      </Collapsible>
    );

  

    return (
      <View style={containerStyle} {...viewProps}>
        {sections.map((section, key) => (
           <ScrollView style={{ flex: 1,}} bounces={false} showsVerticalScrollIndicator={false}>
            <View key={key} style={[sectionContainerStyle, styles.mainContainer]}>
              {renderSectionTitle(section, key, activeSections.includes(key))}

              {expandFromBottom && renderCollapsible(section, key)}
              <View style={{ flexDirection: 'row', flex: 4, }}>

                <View style={styles.renderItemView}>
                  <View style={styles.beforeimgStyle}>
                    <Image
                      source={{ uri: (section.ImagePath == '' || section.ImagePath == undefined) ? globals.User_img:section.ImagePath }}
                      style={styles.profileImage}
                      resizeMode="contain"
                    />
                  </View>
                  <View style={styles.userNameAndTitleViewStyle}>
                    <Text style={styles.userNameText}>{(section.Name =='' || section.Name == undefined) ? '-':section.Name}</Text>
                    {/* <Text style={styles.userTitleText}>{section.title}</Text> */}
                    {/* <Text style={styles.userTitleText}>{'Tech Mentor'}</Text> */}
                  </View>

                </View>
                <Touchable style={{ flex: 1, marginRight: globals.screenWidth * 0.03 }}
                  onPress={() => this._toggleSection(key)}
                  underlayColor={underlayColor}
                  {...touchableProps}
                >
                  {renderHeader(
                    section,
                    key,
                    activeSections.includes(key),
                    sections
                  )}
                </Touchable>

              </View>
              {!expandFromBottom && renderCollapsible(section, key)}

              {renderFooter &&
                renderFooter(
                  section,
                  key,
                  activeSections.includes(key),
                  sections
                )}
            </View>
           </ScrollView> 

        ))}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "white",
    marginVertical: globals.screenHeight * 0.0073,
    marginHorizontal: globals.screenWidth * 0.053,
    borderWidth: 1,
    borderColor: 'lightgray',
    borderRadius: 5,
  },
  renderItemView: {
    padding: globals.screenWidth * 0.026,
    margin: globals.screenWidth * 0.013,
    flexDirection: 'row',
    alignItems: 'center',
    flex: 10,
   
  },
  profileImage: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.12 : globals.screenWidth * 0.16,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.12 : globals.screenWidth * 0.16,
    borderRadius: globals.screenWidth * 0.16 / 2,
    marginLeft: -1,
    marginTop: -1
  },
  userNameAndTitleViewStyle: {
    marginRight: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.26 : globals.screenWidth * 0.18,
  },
  userNameText: {
    marginBottom: globals.screenHeight * 0.03,
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 23 : globals.font_14,
    marginLeft: globals.screenWidth * 0.03,
    color: 'black',
  },
  userTitleText: {
    fontSize: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? 25 : globals.font_15,
    color: 'black',
    opacity: 0.7,
  },
  beforeimgStyle: {
    width: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.12 : globals.screenWidth * 0.16,
    height: iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet() ? globals.screenWidth * 0.12 : globals.screenWidth * 0.16,
    borderRadius: globals.screenWidth * 0.16 / 2,
    borderColor: colors.proUnderline,
    borderWidth: 0.5
  }
});