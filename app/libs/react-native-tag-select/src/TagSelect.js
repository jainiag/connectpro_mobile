import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  ViewPropTypes,
  StyleSheet
} from 'react-native'

import TagSelectItem from './TagSelectItem'
import profileCategory from '../../../containers/screens/Profile/editableScreens/profileCategory'
import addInterests from '../../../containers/screens/Profile/editableScreens/addInterests'
import addSkill from '../../../containers/screens/Profile/editableScreens/addSkill'

let TAG = "TagSelect library ::===="
class TagSelect extends React.Component {
  static propTypes = {
    // Pre-selected values
    value: PropTypes.array,

    // Objet keys
    labelAttr: PropTypes.string,
    keyAttr: PropTypes.string,

    // Available data
    data: PropTypes.array,

    // validations
    max: PropTypes.number,

    // Callbacks
    onMaxError: PropTypes.func,
    onItemPress: PropTypes.func,

    // Styles
    containerStyle: ViewPropTypes.style
  }

  static defaultProps = {
    value: [],

    labelAttr: 'Name',
    keyAttr: 'id',
    labelAttr2: 'CommunityName',
    lableCategoryName: 'CategoryName',

    data: [],
    max: null,

    onMaxError: null,
    onItemPress: null,

    containerStyle: {}
  }

  state = {
    value: {}
  }

  componentDidMount() {
    console.log(TAG,this.props.data)
    const value = {}
    this.props.value.forEach((val) => {
      value[val[[this.props.keyAttr]] || val] = val
    })

    this.setState({ value })
  }

  /**
   * @description Return the number of items selected
   * @return {Number}
   */
  get totalSelected() {
    return Object.keys(this.state.value).length
  }

  /**
   * @description Return the items selected
   * @return {Array}
   */
  get itemsSelected() {
    const items = []

    Object.entries(this.state.value).forEach(([key]) => {
      items.push(this.state.value[key])
    })

    return items
  }

  /**
   * @description Callback after select an item
   * @param {Object} item
   * @return {Void}
   */
  handleSelectItem = (item) => {
    const key = item[this.props.keyAttr] || item

    const value = { ...this.state.value }
    const found = this.state.value[key]

    // Item is on array, so user is removing the selection
    if (found) {
      delete value[key]
    } else {
      // User is adding but has reached the max number permitted
      if (this.props.max && this.totalSelected >= this.props.max) {
        if (this.props.onMaxError) {
          return this.props.onMaxError()
        }
      }

      value[key] = item
    }

    return this.setState({ value }, () => {
      if (this.props.onItemPress) {
        this.props.onItemPress(item)
      }
    })
  }

  deleteListData = (index) => {
    let checked = this.props.data;
    let values = checked.indexOf(index)
    checked.splice(values, 1);
    this.setState({ checked_Array: checked });
    if(this.props.isDelete == "Profile"){
      profileCategory.deleteProfileItem();
    } else if (this.props.isDelete == "Ineterest") {
      addInterests.deleteInterestItem();
    } else if (this.props.isDelete == "Skills") {
      addSkill.deleteSkillItem();
    } else {
      console.log("Not deleted");
    }
  }

  render() {
    return (
      <View
        style={[
          styles.container,
          this.props.containerStyle
        ]}
      >
        {this.props.data.map((i) => {
          return (
            (this.props.isFrom === 'ProfileCommunities') ?
              <TagSelectItem
                {...this.props}
                CommunityName={i[this.props.labelAttr2] ? i[this.props.labelAttr2] : i}
                key={i[this.props.keyAttr] ? i[this.props.keyAttr] : i}
                onPress={this.deleteListData.bind(this, i)}
                selected={(this.state.value[i[this.props.keyAttr]] || this.state.value[i]) && true}
              />
              :
            (this.props.isFromForLable === 'Business_Categories') ?
            <TagSelectItem
              {...this.props}
              Name={i[this.props.lableCategoryName] ? i[this.props.lableCategoryName] : i}
              key={i[this.props.keyAttr] ? i[this.props.keyAttr] : i}
              onPress={this.deleteListData.bind(this, i)}
              selected={(this.state.value[i[this.props.keyAttr]] || this.state.value[i]) && true}
            />
            :
            <TagSelectItem
              {...this.props}
              Name={i[this.props.labelAttr] ? i[this.props.labelAttr] : i}
              key={i[this.props.keyAttr] ? i[this.props.keyAttr] : i}
              onPress={this.deleteListData.bind(this, i)}
              selected={(this.state.value[i[this.props.keyAttr]] || this.state.value[i]) && true}
            />
        )
        })}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  }
})

export default TagSelect
