import {StyleSheet, Platform} from 'react-native';
import * as defaultStyle from '../../style';
import * as globals from '../../../../../utils/globals';
import DeviceInfo from 'react-native-device-info';

const STYLESHEET_ID = 'stylesheet.calendar.header';
var iPad = DeviceInfo.getModel();

export default function(theme={}) {
  const appStyle = {...defaultStyle, ...theme};
  return StyleSheet.create({
    header: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: 10,
      paddingRight: 10,
      marginTop: 6,
      alignItems: 'center'
    },
    monthText: {
      fontSize: (iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?  28 :globals.font_18,//appStyle.textMonthFontSize,
      fontFamily: appStyle.textMonthFontFamily,
      fontWeight: 'bold',//appStyle.textMonthFontWeight,
      color: appStyle.monthTextColor,
      margin: 10
    },
    arrow: {
      padding: 10,
      ...appStyle.arrowStyle
    },
    arrowImage: {
      ...Platform.select({
        ios: {
          tintColor: appStyle.arrowColor
        },
        android: {
          tintColor: appStyle.arrowColor
        }
      })
    },
    week: {
      marginTop: 7,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    dayHeader: {
      marginTop: 2,
      marginBottom: 7,
      width: 32,
      textAlign: 'center',
      fontSize:(iPad.indexOf('iPad') != -1 || DeviceInfo.isTablet()) ?  20 : appStyle.textDayHeaderFontSize,
      fontFamily: appStyle.textDayHeaderFontFamily,
      fontWeight: 'bold',//appStyle.textDayHeaderFontWeight,
      color: 'white',//appStyle.textSectionTitleColor
    },
    ...(theme[STYLESHEET_ID] || {})
  });
}
